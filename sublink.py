#!/usr/bin/env python2
# -*- coding: utf-8 -*-
""" Illustris Simulation: Public Data Release.
sublink.py: File I/O related to the Sublink merger tree files. """

import numpy as np
import h5py
import glob
import six
import os


def gcPath(basePath, snapNum, chunkNum=0):
    """ Return absolute path to a group catalog HDF5 file. """
    gcPath = basePath + '/groups_%03d/' % snapNum
    filePath1 = gcPath + 'groups_%03d.%d.hdf5' % (snapNum, chunkNum)
    filePath2 = gcPath + 'fof_subhalo_tab_%03d.%d.hdf5' % (snapNum, chunkNum)

    if os.path.isfile(filePath1):
        return filePath1
    return filePath2


def offsetPath(basePath, snapNum):
    """ Return absolute path to a separate offset file (modify as needed). """
    offsetPath = (basePath +
                  '/offsets/offsets_%03d.hdf5' % snapNum)

    return offsetPath


def partTypeNum(partType):
    """ Mapping between common names and numeric particle types. """
    if str(partType).isdigit():
        return int(partType)

    if str(partType).lower() in ['gas', 'cells']:
        return 0
    if str(partType).lower() in ['dm', 'darkmatter']:
        return 1
    if str(partType).lower() in ['tracer', 'tracers', 'tracermc', 'trmc']:
        return 3
    if str(partType).lower() in ['star', 'stars', 'stellar']:
        return 4  # only those with GFM_StellarFormationTime>0
    if str(partType).lower() in ['wind']:
        return 4  # only those with GFM_StellarFormationTime<0
    if str(partType).lower() in ['bh', 'bhs', 'blackhole', 'blackholes']:
        return 5

    raise Exception("Unknown particle type name.")


def treePath(basePath, chunkNum=0):
    """ Return absolute path to a SubLink HDF5 file (modify as needed). """
    tree_path = os.path.join('tree_extended.' + str(chunkNum) + '.hdf5')

    _path = os.path.join(basePath, tree_path)
    if len(glob.glob(_path)):
        return _path

    # new path scheme
    _path = os.path.join(basePath, os.path.pardir, tree_path)
    if len(glob.glob(_path)):
        return _path

    # try one or more alternative path schemes before failing
    _path = os.path.join(basePath, tree_path)
    if len(glob.glob(_path)):
        return _path

    raise ValueError("Could not construct treePath "
                     "from basePath = '{}'".format(basePath))


def treeOffsets(basePath, snapNum, subnum):
    """ Handle offset loading for a SubLink merger tree cutout. """
    # old or new format
    if 'fof_subhalo' in gcPath(basePath, snapNum):
        # load groupcat chunk offsets from separate 'offsets_nnn.hdf5' files
        with h5py.File(offsetPath(basePath, snapNum), 'r') as f:
            groupFileOffsets = f['FileOffsets'][()]

        offsetFile = offsetPath(basePath, snapNum)

        groupOffset = subnum
    else:
        # load groupcat chunk offsets from header of first file
        with h5py.File(gcPath(basePath, snapNum), 'r') as f:
            groupFileOffsets = f['Header'].attrs['FileOffsets_Subhalo']

        # calculate target groups file chunk which contains this subnum
        groupFileOffsets = int(subnum) - groupFileOffsets
        fileNum = np.max(np.where(groupFileOffsets >= 0))
        groupOffset = groupFileOffsets[fileNum]

        offsetFile = gcPath(basePath, snapNum, fileNum)

    with h5py.File(offsetFile, 'r') as f:
        # load the merger tree offsets of this subgroup
        RowNum = f['RowNum'][groupOffset]
        LastProgID = f['LastProgenitorID'][groupOffset]
        SubhaloID = f['SubhaloID'][groupOffset]
        return RowNum, LastProgID, SubhaloID


offsetCache = dict()


def subLinkOffsets(basePath, cache=True):
    # create quick offset table for rows in the SubLink files
    if cache is True:
        cache = offsetCache

    if type(cache) is dict:
        try:
            return cache[basePath]
        except KeyError:
            pass

    search_path = treePath(basePath, '*')
    numTreeFiles = len(glob.glob(search_path))
    if numTreeFiles == 0:
        raise ValueError("No tree files found "
                         "for path '{}'".format(search_path))
    offsets = np.zeros(numTreeFiles, dtype='int64')

    for i in range(numTreeFiles-1):
        with h5py.File(treePath(basePath, i), 'r') as f:
            offsets[i+1] = offsets[i] + f['SubhaloID'].shape[0]

    if type(cache) is dict:
        cache[basePath] = offsets

    return offsets


def loadTree(basePath, snapNum, subnum, fields=None,
             onlyMPB=False, onlyMDB=False, cache=True):
    """ Load portion of Sublink tree for a given subhalo.

    Arguments:
        basePath: directory containing sublink merger trees.
        snapNum: snapshot number.
        subnum: subhalo ID number.
        field: optionally restrict to a subset of fields.
        onlyMPB: Load only the main progenitor branch.
        onlyMDB: load only main descendant branch (e.g. from z=0 descendant
                 to current subhalo)
    """
    # the tree is all subhalos between SubhaloID and LastProgenitorID
    RowNum, LastProgID, SubhaloID = treeOffsets(basePath, snapNum, subnum)

    if RowNum == -1:
        print('Warning, empty return. Subhalo [%d] at snapNum [%d] '
              'not in tree.' % (subnum, snapNum))
        return None

    rowStart = RowNum
    rowEnd = RowNum + (LastProgID - SubhaloID)
    nRows = rowEnd - rowStart + 1

    # make sure fields is not a single element
    if isinstance(fields, six.string_types):
        fields = [fields]

    offsets = subLinkOffsets(basePath, cache)

    # find the tree file chunk containing this row
    rowOffsets = rowStart - offsets

    try:
        fileNum = np.max(np.where(rowOffsets >= 0))
    except ValueError as err:
        print("ERROR: ", err)
        print("rowStart = {}, offsets = {}, rowOffsets = {}".format(
                rowStart, offsets, rowOffsets))
        print(np.where(rowOffsets >= 0))
        raise
    fileOff = rowOffsets[fileNum]

    # load only main progenitor branch - get MainLeafProgenitorID now
    if onlyMPB:
        with h5py.File(treePath(basePath, fileNum), 'r') as f:
            MainLeafProgenitorID = f['MainLeafProgenitorID'][fileOff]

        # re-calculate nRows
        rowEnd = RowNum + (MainLeafProgenitorID - SubhaloID)
        nRows = rowEnd - rowStart + 1

    # load only main descendant branch
    if onlyMDB:
        with h5py.File(treePath(basePath, fileNum), 'r') as f:
            RootDescendantID = f['RootDescendantID'][fileOff]

        # re-calculate tree subset
        rowStart = RowNum - (SubhaloID - RootDescendantID) + 1
        rowEnd = RowNum
        nRows = rowEnd - rowStart + 1
        fileOff -= nRows

    # read
    result = {'count': nRows}

    with h5py.File(treePath(basePath, fileNum), 'r') as f:
        # if no fields requested, return all fields
        if not fields:
            fields = list(f.keys())

        if fileOff + nRows > f['SubfindID'].shape[0]:
            raise Exception('Should not occur. Each tree is contained '
                            'within a single file.')

        # loop over each requested field
        for field in fields:
            if field not in f.keys():
                raise Exception("SubLink tree does not have field ["+field+"]")

            # read
            result[field] = f[field][fileOff:fileOff+nRows]

    # only a single field? then return the array instead of a single item dict
    if len(fields) == 1:
        return result[fields[0]]

    return result


def maxPastMass(tree, index, partType='stars'):
    """ Get maximum past mass (of the given partType) along the main branch
    of a subhalo specified by index within this tree. """
    ptNum = partTypeNum(partType)

    branchSize = (tree['MainLeafProgenitorID'][index] -
                  tree['SubhaloID'][index] + 1)
    masses = tree['SubhaloMassType'][index: index + branchSize, ptNum]
    return np.max(masses)


def numMergers(tree, minMassRatio=1e-10, massPartType='stars', index=0):
    """ Calculate the number of mergers in this sub-tree
    (optionally above some mass ratio threshold). """
    # verify the input sub-tree has the required fields
    reqFields = ['SubhaloID', 'NextProgenitorID', 'MainLeafProgenitorID',
                 'FirstProgenitorID', 'SubhaloMassType']

    if not set(reqFields).issubset(tree.keys()):
        raise Exception('Error: Input tree needs to have loaded fields: ' +
                        ', '.join(reqFields))

    numMergers = 0
    invMassRatio = 1.0 / minMassRatio

    # walk back main progenitor branch
    rootID = tree['SubhaloID'][index]
    fpID = tree['FirstProgenitorID'][index]

    while fpID != -1:
        fpIndex = index + (fpID - rootID)
        fpMass = maxPastMass(tree, fpIndex, massPartType)

        # explore breadth
        npID = tree['NextProgenitorID'][fpIndex]

        while npID != -1:
            npIndex = index + (npID - rootID)
            npMass = maxPastMass(tree, npIndex, massPartType)

            # count if both masses are non-zero, and ratio exceeds threshold
            if fpMass > 0.0 and npMass > 0.0:
                ratio = npMass / fpMass

                if ratio >= minMassRatio and ratio <= invMassRatio:
                    numMergers += 1

            npID = tree['NextProgenitorID'][npIndex]

        fpID = tree['FirstProgenitorID'][fpIndex]

    return numMergers
