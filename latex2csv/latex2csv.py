""" This script is intended for converting latex tables from latex source code
to a csv table for easy importing """


def main():
    import sys
    import os
    import argparse
    import re

    parser = argparse.ArgumentParser(
                description="Convert a Latex table to csv format.",
                formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("infilename", metavar="infilename", type=str,
                        help='File containing a single latex table')
    parser.add_argument('--outfile', '--out', type=str,
                        help='Filename of output csv')
    parser.add_argument('--empty', type=str, default=" ",
                        help='String to replace empty cells')
    parser.add_argument('--head', '--header', type=int, default=0,
                        help='Number of header rows (column titles etc). '
                             'Applies to all tables if parsing multiple.')
    parser.add_argument('--latex', type=bool, default=False,
                        help='Try to parse multiple tables from file. '
                             'Default False.')
    parser.add_argument('--table', type=int, default=None,
                        help='Table number to parse if multiple tables. '
                             'Default None parses all tables.')
    parser.add_argument('--verbose', '--v', type=int, default=0,
                        help='0 (default) to print output only, '
                             'higher integers print more info')
    args = parser.parse_args()

    if os.path.exists(args.infilename):
        infile = open(args.infilename, "r")
    else:
        print args.infilename, "does not exist."
        sys.exit(1)
    toScreen = True
    if args.outfile is not None:
        args.outfile = os.path.splitext(args.outfile)[0]
        toScreen = False
    else:
        args.outfile = os.path.splitext(args.infilename)[0]

    empty = args.empty  # String to replace fields with no value
    headers = args.head
    verbose = args.verbose

    tables = []
    if args.latex or os.path.splitext(args.infilename)[1] == ".tex":
        if args.table is None:
            print "Parsing all tables in file."
        else:
            print "Parsing table "+str(args.table)+" from file."
        copy = False
        tablelines = []
        ntables = 0  # number of tables found
        for line in infile:
            # match lines containing the start of a table, unless that line
            # starts with a '%', i.e. it is a comment
            buf = re.sub("\s+", "", line)
            if line[0] is "%":
                # skip commented lines
                continue
            elif (re.match(r".*\\begin\{tabular\}\{.*\}",
                           buf) or
                  re.match(r".*\\begin\{tabularx\}\{.*\}",
                           buf) or
                  re.match(r".*\\begin\{deluxetable\*?\}\{.*\}",
                           buf)):
                ntables += 1
                # copy table into list if desired
                if args.table is None or ntables == args.table:
                    copy = True
                # print "line = ", line
            elif copy and (re.match(r".*\\end\{tabular\}.*", buf) or
                           re.match(r".*\\end\{deluxetable\*?\}.*", buf)):
                copy = False
                tables.append("\n".join(tablelines))
                # print "table =","\n".join(tablelines)
                tablelines = []
            elif copy:
                # print "appending ", line
                tablelines.append(line)
    else:
        tables.append(infile.read())

    if len(tables) == 0:
        if args.table is None:
            print "ERROR: No tables found."
        else:
            print "ERROR: Could not find table "+str(args.table)
        sys.exit(1)
    if len(tables[0]) == 0:  # sometimes the first element is empty
        del tables[0]
    outname = args.outfile
    for i, table in enumerate(tables):
        if args.table is None:
            n = i+1  # table number
        else:
            n = args.table
        if toScreen:
            print "\nTable "+str(n)+": "
        else:
            if len(tables) > 1:
                outname = args.outfile+"_t"+str(i+1)
            print "\nTable "+str(n)+" output to", outname+".csv"
        parse_table(table, outname=outname, empty=empty, headers=headers,
                    toScreen=toScreen, verbose=verbose)
    infile.close()


def parse_table(intab, outname=None, empty=" ", headers=0, toScreen=True,
                verbose=False):
    """ Take in a latex table (string) and output in csv format

    Args:
        intab: string containing latex table
        outname: name of output csv file (excluding .csv ext) if not toScreen
        empty: string to replace non-existent cells with
        headers: no. of lines in table that contain text and not data
        toScreen: if true will print to screen instead of outname file
    """

    import re
    import numpy as np
    import sys

    if not toScreen and outname is None:
        print "ERROR: Output filename is None. Printing to screen."
        toScreen = True

    # Latex code to remove
    remove = re.compile(r"\\ensuremath|\\mathrm|\\rm|\\texttt|\\textemdash|"
                        r"\\text|\\overline|\\dagger|\\dotfill|\\ldots|"
                        r"\\nodata|\\pz|\\quad|\\left|\\right|\\br|"
                        r"\\startdata|\\enddata",
                        re.IGNORECASE)
    # for finding multicol commands
    multicol = re.compile(r"\\multicolumn\{\d+\}\{.*\}\{.*\}", re.IGNORECASE)
    N = 0  # Number of columns
    discarded = 0  # Number of discarded lines
    multicolinsert = -1  # index where \multicol is found
    multicollength = None  # how many columns to insert at multicolinsert
    table = []
    concat_lines = False

    # split table by line
    lines = (intab).splitlines()
    for k, line in enumerate(lines):
        if not re.search(r'\\\\', line) and k != len(lines)-1:
            # no \\ in line and is not the last line
            if verbose > 1:
                print "line {:d}: no \\\\ in line".format(k)
            if not concat_lines:
                concat_lines = True
                line_concat = line.replace("\n", "")
            else:
                line_concat += line.replace("\n", "")
            discarded += 1
            continue  # skip to next line and concatenate
        elif concat_lines:
            line = line_concat+line
            concat_lines = False
            if verbose > 1:
                print "Concatenated line:"
                print line

        row = []
        line = re.sub('\n', "", line)
        line = re.sub(r'\\\\.*$', "", line)  # Remove \\ and rest of line
        # line = re.sub(r'\\footnote\{.*\}',"", line) ## DON'T USE - removes
        # everything between \footnote and the last instance of \} in the line
        # could try footnote\{[^\}]*\} instead, where [^\}] matches all
        # characters except \}
        # Remove \footnotetext
        line = re.sub(r'\\footnotetext\[?[a-zA-Z0-9]?\]?\{[^\}]*\}', "", line)
        # Remove \footnotmark
        line = re.sub(r'\\footnotemark\[[a-zA-Z0-9]?\]', "", line)

        # remove latex code defined above
        line = remove.sub("", line)
        line = re.sub(r"\\&", "and", line)  # Replace literal ampersands
        line = re.sub(r"\\,|\\;", "", line)  # remove \, and \; and spaces
        line = re.sub(r",|\*", "", line)  # Remove , and *
        line = re.sub("\\\hline", "", line)  # Remove horiz lines

        fields = line.split("&")
        # Discard row if no "&"s or is comment
        if len(fields) == 1 or re.match("^%", line.strip()):
            discarded += 1
            continue
        if len(fields) >= N or multicol.search(line):
            N = len(fields)
        elif multicolinsert >= 0:
            for multicolidx in range(multicollength - 1):
                fields.insert(multicolinsert + multicollength - 1, "")
        if len(fields) < N:
            print ("ERROR: Row " + str(k) + " (zero index) has fewer columns "
                   "than one of the previous rows.")
            if verbose > 0:
                print "The row is\n", lines[k]
            return 1
        # Number of new columns each column is split into
        colsplits = np.full((len(fields)), 1, dtype='int64')
        if verbose > 0 and k == headers + discarded:
            print "First line:"
            print line+"\n"
        for j, field in enumerate(fields):
            field = field.strip()

            if verbose > 0 and k == headers + discarded:
                sys.stdout.write(field+" --> ")

            # Handle case with \multicolumn{n}{}{} where n is no of columns
            if multicol.match(field):
                if multicolinsert >= 0:
                    # if multicol has already occurred in table
                    print ("ERROR: Multiple \multicol{}{}{} found. "
                           "Not currently supported.")
                    return 1
                multicolstring = multicol.findall(field)[0]
                # remove multicol
                field = re.sub(r"\\multicolumn\{\d+\}\{.*\}\{", "", field)
                field = re.sub("\}", "", field)
                fields[j] = field
                # get n
                multicollength = int(re.findall(r"\d+", multicolstring)[0])
                if multicollength > 1:
                    multicolinsert = j+1
                    for multicolidx in range(multicollength - 1):
                        fields.insert(multicolinsert, field)
                        N = len(fields)
                        colsplits = np.insert(colsplits, multicolinsert, 1)
            elif multicol.search(field):
                print ("\multicolumn{}{}{} found but field contains "
                       "other characters besides whitespace. "
                       "The field looks like this:\n" + str(field))
                return 1

            # Remove brackets if 1st char
            if re.match("\([-+]?\d*\.\d+|\(\d+", field):
                field = re.sub("\(|\)", "", field)
            # Replace notes of form '(1)' where the number is one or two digits
            field = re.sub("\(\d\d?\)", "", field)
            # Remove notes of form '$^{a}$'
            field = re.sub("\$\^\{[a-zA-Z*]+\}\$", "", field)
            # Remove notes of form '$^{\a}$'
            field = re.sub("\$\^\{\\[a-zA-Z*]+\}\$", "", field)
            # Remove notes of form '$^{}$'
            field = re.sub("\$\^\{\}\$", "", field)
            # Remove notes of form '^{}'
            field = re.sub("\^\{\}", "", field)
            # Remove notes of form '^{a}'
            field = re.sub("\^\{[a-zA-Z*]+\}", "", field)
            # Remove notes of form '^{{a}}'
            field = re.sub("\^\{\{[a-zA-Z*]+\}\}", "", field)
            # Remove notes of form ^a
            field = re.sub("\^[a-zA-Z*]+", "", field)
            # replace infinity with large integer
            field = re.sub(r"\\infty", "999999999", field)
            # remove any uncovered whitespace
            field = field.strip()

            if verbose > 0 and k == headers + discarded:
                sys.stdout.write(field)
                sys.stdout.flush()

            if k <= discarded + headers - 1:  # Header rows
                # Remove $ and {} and colhead
                field = re.sub("\$|\{|\}|colhead", "", field)
                # replace whitespace with _
                field = re.sub("\s+", "_", field)
                if j == 0:  # Add comment marker at beginning of line
                    field = "#"+field
                row.append(field)
                if verbose > 0 and k == headers + discarded:
                    print " -->", field, note("(header)")
            elif "$" in field:
                row_idx = len(row)

                # if no numbers, or if the first character is
                # a letter, just append field as text
                if (re.match("[^0-9]*\$[^0-9]+\$[^0-9]*", field) or
                        re.match("^[a-zA-Z].*", field)):
                    # remove $, { or } before appending
                    field = re.sub("\$|\{|\}", "", field)
                    row.append(field)
                    if verbose > 0 and k == headers + discarded:
                        print " -->", field, note("(text)")

#==============================================================================
#                 # if text in math-mode
#                 if re.match("\$[a-zA-z. ]+\$", field):
#                     row.append(field)
#                     if verbose > 0 and k == headers + discarded:
#                         print " -->", field, note("(text)")
#                 # if math-mode hyphen with just text
#                 elif re.match("[a-zA-z. ]+\s*\$-\$\s*[a-zA-z. ]+", field):
#                     row.append(field.replace("$", ""))
#                     if verbose > 0 and k == headers + discarded:
#                         print " -->", field, note("(math-mode hyphen)")
#==============================================================================
                else:
                    field = re.sub("\(|\)", "", field)  # remove brackets
                    subfields = field.split("$")
                    errors = False
                    # Set flag for when + and - errors are the same
                    skip_next_subfield = False
                    if verbose > 0 and k == headers + discarded:
                        print " -->", subfields, note("($ split)")

                    multiply = False
                    # Remove whitespace and deal with factors of 10
                    for i, subfield in enumerate(subfields):
                        # remove whitespace
                        subfields[i] = subfields[i].strip()
                        subfields[i] = re.sub("\s+", "", subfields[i])
                        if re.search(r"\\times10\^\{[-+]?\d+\}", subfields[i]):
                            # currently doesn't handle negative exponents
                            multiply = True
                            # get string containing \times10
                            multiplystring = re.findall(
                                                r"\\times10\^\{[-+]?\d+\}",
                                                subfields[i])[0]
                            # get exponent
                            multiplyexp = re.findall(r"[-+]?\d+",
                                                     multiplystring)[1]
                            # remove \times
                            subfields[i] = re.sub(r"\\times10\^\{[-+]?\d+\}",
                                                  "", subfields[i])
                    if verbose > 0 and k == headers + discarded:
                        print " -->", subfields, note("(whitespace removed)")

                    for i, subfield in enumerate(subfields):
                        if verbose > 1:
                            sys.stdout.write(subfield)
                            sys.stdout.flush()
                        if subfields[i] == "":
                            if verbose > 1:
                                print " --->", note("(blank)")
                            continue

                        # If decimal or integer
                        elif re.match("^[-+]?\d*\.\d+$|^\d+$", subfields[i]):
                            # only add if not already done,
                            # as if previous subfield was \pm
                            if not skip_next_subfield:
                                row.append(subfields[i])
                            if verbose > 1:
                                print " --->", subfields[i], note("(number only)")

                        # just "\pm"
                        elif re.match(r"^\\pm$", subfields[i]):
                            errors = True
                            skip_next_subfield = True  # + and - errors same
                            row.append(subfields[i+1].strip())
                            row.append("-"+subfields[i+1].strip())
                            if verbose > 1: print " ---> +/-", subfields[i+1].strip(), note("(just \pm)")

                        # "i \pm j"
                        elif re.match("[-+]?\d*\.\d+\s*\\pm\s*\d*\.\d+|\d+\s*\\pm\s*\d*\.\d+|\d*\.\d+\s*\\pm\s*\d+|\d+\s*\\pm\s*\d+", subfields[i]):
                            errors = True
                            skip_next_subfield = True ## + and - errors same
                            buf = (re.sub("\s+","",subfields[i])).split("pm")
                            row.append(buf[0])
                            row.append(buf[1])
                            row.append("-"+buf[1])
                            if verbose > 1: print " --->", row[-3:], note("(i \pm j)")

                        ## "i\pmj"
                        elif re.match("[-+]?\d*\.\d+\s*\\\pm\s*\d*\.\d+|\d+\s*\\\pm\s*\d*\.\d+|\d*\.\d+\s*\\\pm\s*\d+|\d+\s*\\\pm\s*\d+", subfields[i]):
                            errors = True
                            skip_next_subfield = True ## + and - errors same
                            buf = (re.sub("\s+","",subfields[i])).split("\pm")
                            row.append(buf[0])
                            row.append(buf[1])
                            row.append("-"+buf[1])
                            if verbose > 1: print " --->", row[-3:], note("(i\pmj)")
                            
                        ## "\pmj"
                        elif re.match("\\\pm\s*\d*\.\d+|\d+\s*\\\pm\s*\d*\.\d+|\\\pm\s*\d+|\\\pm\s*\d+", subfields[i]):
                            errors = True
                            skip_next_subfield = True ## + and - errors same
                            buf = (re.sub("\s+","",subfields[i])).split("\pm")
                            row.append(buf[1])
                            row.append("-"+buf[1])
                            if verbose > 1: print " --->", row[-2:], note("(\pmj)")

                        ## "i^{j}_{k}"
                        elif re.match("^[-+]?\d*\.?\d*\^\{[-+]?\d*\.?\d*\}_\{[-+]?\d*\.?\d*\}", subfields[i]):
                            errors = True
                            subfields[i] = re.sub("\^|\+|_", "", subfields[i])
                            buf = subfields[i].split("{")
                            row.append(buf[0])
                            row.append(buf[1].split("}")[0]) ## take bracket contents only
                            row.append(buf[2].split("}")[0])
                            if verbose > 1: print " --->", row[-3:], note("(i^{j}_{k})")

                        ## "i_{j}^{k}"
                        elif re.match("^[-+]?\d+\.?\d*_\{[-+]?\d*\.?\d*\}\^\{[-+]?\d*\.?\d*\}", subfields[i]):
                            errors = True
                            subfields[i] = re.sub("\^|\+|_", "", subfields[i])
                            buf = subfields[i].split("{")
                            row.append(buf[0])
                            row.append(buf[2].split("}")[0]) ## take bracket contents only
                            row.append(buf[1].split("}")[0])
                            if verbose > 1: print " --->", row[-3:], note("(i_{j}^{k})")
                            
                        ## "_{j}^{k}" (like above but without i)
                        elif re.match("^_\{[-+]?\d*\.?\d*\}\^\{[-+]?\d*\.?\d*\}", subfields[i]):
                            errors = True
                            subfields[i] = re.sub("\^|\+|_", "", subfields[i])
                            buf = subfields[i].split("{")
                            row.append(buf[2].split("}")[0]) ## take bracket contents only
                            row.append(buf[1].split("}")[0])
                            if verbose > 1: print " --->", row[-2:], note("(_{j}^{k})")

                        ## "< i" upper limit so +ve error is zero, -ve error is same as number
                        elif re.match("^<\s*\d+\.?\d*\s*$", subfields[i]):
                            errors = True
                            subfields[i] = re.sub("<", "", subfields[i])
                            row.append(subfields[i].strip())
                            row.append("0.0")
                            row.append("-"+subfields[i].strip())
                            if verbose > 1: print " ---> +/-", row[-3:], note("(upper limit)")
                            
                        ## "<" and "i" (upper limit)
                        elif re.match("\s*<\s*", subfields[i]) and re.match("\s*\d+\.?\d*\s*$", subfields[i+1]):
                            errors = True
                            skip_next_subfield = True
                            row.append(subfields[i+1].strip())
                            row.append("0.0") ## +ve error is zero
                            row.append("-"+subfields[i+1].strip()) ## -ve error is same as number
                            if verbose > 1: print " ---> +/-", row[-3:], note("(upper limit)")

                        elif re.match("^\\pm\d*\.\d+|\\pm\d+$", subfields[i]):
                            errors = True
                            row.append(re.sub("\\pm","",subfields[i]))
                            row.append(re.sub("\\pm","","-"+subfields[i]))
                            if verbose > 1: print " --->", row[-2:], note("(\pm i)")

                        elif re.match("\\pm\^\{", subfields[i]):
                            errors = True
                            subfields[i] = re.sub("\\pm|\^|\{|\+|\}", "", subfields[i])
                            row.append(subfields[i].split("_")[0])
                            row.append(subfields[i].split("_")[1])
                            if verbose > 1: print " --->", row[-2:], note("(\pm^{})")

                        elif re.match("\^\{", subfields[i]): ## plus/minus errors
                            errors = True
                            subfields[i] = re.sub("\^|\{|\+|\}", "", subfields[i])
                            if "_" in subfields[i]:
                                row.append(subfields[i].split("_")[0])
                                row.append(subfields[i].split("_")[1])
                            else:
                                row.append(subfields[i])
                            if verbose > 1: print " --->", row[-2:], note("(^{)")

                        # no match so just add subfields as separate columns
                        else:
                            row.append(subfield)
                            if verbose > 1: print " --->", row[-1], note("(no match)")
                    if errors:
                        colsplits[j] = 3
                    if multiply:
                        for i in range(row_idx, len(row)):
                            row[i] += "e"+multiplyexp
                    if verbose > 0 and k == headers + discarded:
                        print " -->", row[row_idx:]
            elif re.match("^\s+$", field) or field == "":
                if verbose > 0 and k == headers + discarded:
                    print " -->", empty, note("(empty)")
                row.append(empty)
            elif re.match("\d+\.?\d*-\d+\.?\d*", re.sub("\s+", "", field)):
                colsplits[j] = 2
                field  = re.sub("\s+", "", field)
                if verbose > 0 and k == headers + discarded:
                    print " -->", field, note("(range)")
                row.append(field.split("-")[0])
                row.append(field.split("-")[1])
            else:
                if verbose > 0 and k == headers + discarded:
                    print " -->", field, note("(string)")
                row.append(field)
            if verbose > 0 and k == headers + discarded:
                print ""

        # Create array 'colsplits_all' containing
        # all of the colsplits for each row
        if k == discarded:  # initialize colsplits_all
            colsplits_all = colsplits
        else:  # stack current colsplits with colsplits_all
            try:
                colsplits_all = np.vstack((colsplits_all, colsplits))
            except ValueError:
                print "ERROR: colsplits shape doesn't match colsplits_all"
                print "line =", line
                print "fields =", fields
                print np.shape(colsplits), "colsplits = ", colsplits
                print np.shape(colsplits_all), "colsplits_all = ", colsplits_all
                sys.exit(1)
        table.append(row)
    if len(lines) == 0:
        print "Table is empty."
        if verbose > 1:
            print "The input table was"
            print intab
    else:
        if verbose > 0: print "\n"
        ## For each column get the maximum no. of columns which it needs to be split into
        colsplits = np.amax(colsplits_all, axis=0)
        assert isinstance(colsplits, np.ndarray), "colsplits_all is not an array? Does table have \\?"
    
        if not toScreen:
            try:
                outfile = open(outname+".csv", "w")
                #print "Outputting to "+args.outfile+".csv"
            except IOError:
                print "ERROR: Could not open "+outname+".csv"
                sys.exit(1)
            except:
                print "ERROR: Error occurred opening output file "+outname+".csv"
                sys.exit(1)
        for i, row in enumerate(table):
            if len(row) != sum(colsplits):
                n=0
                idx = 0
                #print colsplits_all[i]
                for j in range(0,len(colsplits)):
                    #print "j = ", j
                    idx += colsplits_all[i,j] - 1
                    #print "idx = ", idx
                    if colsplits_all[i,j] < colsplits[j]:
                        #print "n =", n
                        for k in range(0, colsplits[j] - colsplits_all[i,j]):
                            table[i].insert(j+idx+1+n, empty)
                            #print table[i]
                        n += colsplits[j] - colsplits_all[i,j]
            if toScreen:
                print ",".join(row)
            else:
                print >> outfile, ",".join(row)
    if not toScreen:
        outfile.close()

def note(text):
    BLUE = '\033[94m'
    ENDC = '\033[0m'
    
    return BLUE+text+ENDC

if __name__ == "__main__":
    main()
