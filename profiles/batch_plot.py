import radprof as rp
import os

dir = "/data/curie4/nh444/project1/profiles/"


## names of directories containing the snapshot files
runs = ["L40_512_DutyInt1RadioWeak_Thermal_FixEta",
        "L40_512_DutyIntRadioWeak_Thermal",
        "L40_512_LowDutyRadioWeak_HalfThermal_FixEta",
        "L40_512_LowDutyRadioWeak_HalfThermal_FixEta_BHChangeVel",
        "L40_512_NoDutyRadioInt_HalfThermal_FixEta",
        "L40_512_NoDutyRadioIntLow_HalfThermal_FixEta",
        "L40_512_NoDutyRadioIntLow_HalfThermal_FixEta_BHChangeVel",
        "L40_512_NoDutyRadioWeak_HalfThermal_FixEta",
        "L40_512_NoDutyRadioWeak_HalfThermal_FixEta_BHChange",
        "L40_512_NoDutyRadioWeak_HalfThermal_FixEta_BHChangeVel",
        "L40_512_NoDutyRadioWeak_HalfThermal_FixEtaRadEff",
        "L40_512_Temp7DutyRadioWeak_HalfThermal_FixEta_BHChangeVel",
        "L40_512_TempDutyRadioWeak_HalfThermal_FixEta_BHChangeVel",
        "L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff",
        "L40_512_NoDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_Prex"]

runs = ["L40_512_NoDutyRadioWeak_HalfThermal_FixEta",
            "L40_512_NoDutyRadioWeak_HalfThermal_FixEtaRadEff",
            "L40_512_NoDutyRadioWeak_HalfThermal_FixEta_BHChangeVel",
            "L40_512_NoDutyRadioWeak_HalfThermal_FixEta_BHChange",
            "L40_512_LowDutyRadioWeak_HalfThermal_FixEta",
            "L40_512_LowDutyRadioWeak_HalfThermal_FixEta_BHChangeVel",
            "L40_512_TempDutyRadioWeak_HalfThermal_FixEta_BHChangeVel",
            "L40_512_NoDutyRadioIntLow_HalfThermal_FixEta",
            "L40_512_NoDutyRadioIntLow_HalfThermal_FixEta_BHChangeVel",
            "L40_512_NoDutyRadioInt_HalfThermal_FixEta",
            "L40_512_NoDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_Prex",
            "L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff"]
labels = [r"NDRW",
          r"NDRWRadEff",
          r"NDRWBHVel",
          r"NDRWBHAcc",
          r"LDRW",
          r"LDRWBHVel",
          r"TDRWBHVel",
          r"NDRIntLow",
          r"NDRIntLowBHVel",
          r"NDRInt",
          r"NDRIntRadioEffBHVelPre",
          r"LDRIntRadioEffBHVel"]
#runs = ["L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff"]
#labels = [r"LDRIntRadioEffBHVel"]
## snapshot numbers
#steps = [5, 8, 15, 25]
#steps = [5, 6, 8, 10, 15, 20, 25]
steps = [25]
list = []

plot_only='all' ## all, density, temperature or entropy
plot_T2500 = False
outfile = plot_only+"_profiles.pdf"
compare="Sun"
nplot=0
M500_min=678.0*2

for i, run in enumerate(runs):
    for step in steps:
        print "Plotting run", run, "snapshot", str(step).zfill(3)
        outname = rp.radprof_plot(dir+run+"/snap_"+str(step).zfill(3)+"_profiles.hdf5", outdir=dir+run, title=labels[i], plotmean=True, nplot=nplot, M500_min=M500_min, compare=compare, plot_only=plot_only, plot_T2500=plot_T2500, for_paper=False)
        list.append(outname)
    #list.append(dir+run+"/SMF/SMF_zeven_"+run+".pdf")
    #list.append(dir+run+"/SMF/SMF_zodd_"+run+".pdf")

liststr = " ".join(list)
os.system("gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile="+outfile+" "+liststr)
print "Output to", outfile
