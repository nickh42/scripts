"""
Created on Wed Oct 19 17:22:19 2016

This script takes an HDF5 file containing profiles output by the module
'radprof' or 'radprof2' and plots them. 
"""
def main():
    indirs = ["/data/curie4/nh444/project1/profiles/LDRIntRadioEffBHVel/c96_box_Arepo_new/",
              #"/data/curie4/nh444/project1/profiles/",## For comparing eg. zooms use concat_profiles.py to make combined hdf5 file
              ]
    if len(indirs) == 1:
        outdir=indirs[0]
    else:
        outdir = "/data/curie4/nh444/project1/profiles/"
    filename = "snap_025_profiles_mass_weighted_Rasia.hdf5"
    file_labels = ["Ref", ## same order as indirs (its not sorted)
                   "Stronger radio",
                   "Quasar duty cycle",
                   "Best"]
    prof_labels=[r"$\alpha = 1$ M$_{\mathrm{seed}}=8 \times 10^5$",
                 r"$\alpha = 1$",
                 r"$\alpha \propto n_H^2$",
                 r"$\alpha = 100$ M$_{\mathrm{seed}}=10^4$",#M_{\odot}/h
                 r"$\alpha = 100$",
                 "fiducial",
                 ]
    radprof_plot([indir+filename for indir in indirs], outdir=outdir,
                 #title="LDRIntRadioEffBHVel",
                 #title="no pressure criterion",
                 #title="LDRIntRadioEffBHVel  T $<$ 2.0 T200",
                 #title ="c128 snap 20 (z=1.0)",
                 #title=" ", redshift=1.0,
                 plot_only="all", plot_colour_scale=True, for_paper=True,
                 add_prof_labels=True, prof_labels=None,
                 add_file_labels=True, file_labels=file_labels,
                 sort_by_M500=True,
                 M500_min=1.0, M500_max=-1,## (1e13 Msun) Use negative for inf
                 #nplot=1,
                 rmin=0.01, rmax=-1, ##-1 for infinity
                 plot_mean=False, plot_median=False,
                 compare="Sun")
    
def radprof_plot (infiles, filetype="radprof", outdir="default",
                  z_label=False, h_scale=0.679, b=0.0, title="", fout_suffix="",
                  nplot=0, M500_min=-1, M500_max=-1, compare="", scaled=True, scaledbyR=False, #REX_match_med=False,
                  filtered=True, comp_unfilt=False, rmin=0, rmax=-1, ylims=None,
                  plot_mean=False, plot_median=True, medians_only=False, scat_ind=[0],
                  plot_only='all', for_paper=False, blkwht=False, transparent=False,
                  plot_vertlines=False, add_prof_labels=False, prof_labels=None,
                  add_file_labels=False, file_labels=None, frameon=False,
                  sort_by_M500=True, plot_colour_scale=True, discrete_colours=True,
                  colours=None, ls=None, lw=2.5, leglabelsize=None,
                  plot_residual=True):
    """
    Arguments are:
    Infiles = list of hdf5 filenames containing profile data produced by either radprof2.py or xray_profiles.py
    filetype = "radprof" or "xray" depending if files were produced by radprof2.py or xray_profiles.py
    outdir = directory where .pdf file is output
    stack = If true all profiles are plotted on same graph
    h_scale = value of H0 / 100.0 to scale observations to
    b = X-ray hydrostatic mass bias b = 1 - (M_X / M_True)
    title = title of plot
    nplot = no. of profiles to plot (beginning from the first in hdf5 list)
    compare = "Sun", "Rasmussen", "Croston" etc. for different comparison observational data
    scaled = if True, quantity is normalised by an appropriate global quantity e.g. T500, P500
    scaledbyR = if True, scale density by radius-squared
    REX_match_med - if True REXCESS comparisons use subsample with similar median mass, otherwise subsample in given mass range
    plot_only = "all", "density", "temperature", "entropy", "pressure", "mass" (gas) or "gasfrac" (gas mass / total mass)
    for_paper = publication style plot
    blkwht = black and white plot (transparent png)
    plot_median = plot the median profile for each hdf5 file
    scat_ind = indices for which to plot the scatter in the median relation
    M500_min = minimum mass (M500) of objects to plot (1e13 Msun)
    M500_max = max mass (M500) of objects to plot (1e13 Msun), -1 for no max limit
    add_prof_labels = labels each profile
    prof_labels = labels for add_prof_labels. If None, uses hdf5 group names
    add_file_labels = labels the median profile of each input file
    file_labels = labels for add_file_labels. If None, uses part of hdf5 group name
    plot_colour_scale = colour scale at bottom of plot - currently not working if more than one file given
    discrete_colours = colour scale will show discrete colours rather than a gradient
    colours = list of colours to use to plot each individual profile (applies only to profiles that are actually plotted)
    ls = list of linestyles e.g. ['solid', 'dashed', 'dotted'] (applies only to profiles that are actually plotted)
    leglabelsize = fontsize of legend labels
    rmin, rmax = radial range to plot, in units of r500
    ylims = y limits of plot as list [ymin, ymax] - only used if only one subplot
    frameon = if True, have border around legend
    """
    
    import h5py
    import numpy as np
    import matplotlib.pyplot as plt
    import matplotlib.ticker as ticker
    from matplotlib import gridspec
    from matplotlib.patches import Polygon
    import sys, os
    if filetype=="xray":
        from sim_python.cosmo import cosmology

    ## assumed cosmology
    omega_m = 0.3065 ## for cosmo box
    omega_b = 0.0483 ## for cosmo box
    omega_l = 0.6935 ## for cosmo box
    
    Msun_in_g = 1.9884430e33
    kpc_cm = 3.085678e21
    obs_dir = "/data/vault/nh444/ObsData/profiles/" ## directory containing observational data
    
    ag_hydrogen = 0.70683 # Anders & Grevesse solar mass fractions 
    ag_helium = 0.27431
    ag_metals = 0.01886

    assert filetype in ["radprof", "xray"], "filetype must be either 'radprof' or 'xray'"
    assert plot_only in ['all', 'density', 'temperature', 'entropy', 'pressure', 'mass', 'gasfrac']

    assert isinstance(infiles, (list, tuple)), "infiles argument must be list or tuple"
    for infile in infiles:
        assert os.path.exists(infile), "Could not find "+infile
    nfiles = len(infiles)
    if nfiles > 1:
        plot_median = True ## Otherwise plot is blank for some reason...
        medians_only=True ## only plot median for each input file

    if (outdir == "default"):
        ## Check write permissions for directory of input file
        if (os.access(os.path.dirname(infiles[0]), os.W_OK | os.X_OK)):
            plotdir = os.path.dirname(infiles[0])+"/"
        ## Check write permissions for current directory
        elif (os.access("./", os.W_OK | os.X_OK)):
            plotdir = "./"
            print "WARNING: No write permission in snapshot directory. Outputting to current directory."
        else:
            print "Do not have write permission for directory of input file or the current directory."
            sys.exit()
    else:
        plotdir = os.path.normpath(outdir)+"/"

    if not (os.path.exists(plotdir)):
        os.makedirs(plotdir)

    plt.rc('text', usetex=True)
    #plt.rc("font", family="serif", serif="Times")

    if not "$" in title: ## escape any underscores unless in math mode
        title = title.replace('_','\_')
    if len(fout_suffix) > 0: ## suffix appended to output filename
        if fout_suffix[0] != "_": ## add underscore to suffix if not one already
            fout_suffix = "_"+fout_suffix

    axislabelsize = 20 ## size of the axis labels in pts
    ticklabelsize = 15 ## size of the tick labels in pts
    plt.rcParams['xtick.labelsize'] = ticklabelsize
    plt.rcParams['ytick.labelsize'] = ticklabelsize

    ## Define the output filename
    ## Note: the name of the output file is based on the first input filename
    if plot_only=='all':
        outname = plotdir+os.path.splitext(os.path.basename(infiles[0]))[0]+fout_suffix+".pdf"
    else:
        if plot_only=='density': outname = plotdir+os.path.splitext(os.path.basename(infiles[0]))[0]+"_density"+fout_suffix+".pdf"
        if plot_only=='temperature': outname = plotdir+os.path.splitext(os.path.basename(infiles[0]))[0]+"_temperature"+fout_suffix+".pdf"
        if plot_only=='entropy': outname = plotdir+os.path.splitext(os.path.basename(infiles[0]))[0]+"_entropy"+fout_suffix+".pdf"
        if plot_only=='pressure': outname = plotdir+os.path.splitext(os.path.basename(infiles[0]))[0]+"_pressure"+fout_suffix+".pdf"
        if plot_only=='mass': outname = plotdir+os.path.splitext(os.path.basename(infiles[0]))[0]+"_mass"+fout_suffix+".pdf"
        if plot_only=='gasfrac': outname = plotdir+os.path.splitext(os.path.basename(infiles[0]))[0]+"_gas_frac"+fout_suffix+".pdf"
        
    ## Set up figure and subplots with gridspec
    if plot_only=='all':
        fig = plt.figure(figsize=(7,15.5))
        outer = gridspec.GridSpec(2, 1, height_ratios=[3,0.05], hspace=0.12)
        gs = gridspec.GridSpecFromSubplotSpec(3, 1, subplot_spec = outer[0], hspace=0)
        gs2 = gridspec.GridSpecFromSubplotSpec(1, 1, subplot_spec = outer[1])
    elif plot_only=='pressure' and plot_residual:
        fig = plt.figure(figsize=(7,12))
        gs = gridspec.GridSpec(3,1, height_ratios=[1,0.5,0.05])
    else:
        fig = plt.figure(figsize=(6,7))
        gs = gridspec.GridSpec(2,1, height_ratios=[1,0.05])
        
    if plot_only=='density' or plot_only=='all':
        d_plt = fig.add_subplot(gs[0], xscale='log', yscale='log')
        if plot_only=='density':
            d_plt.set_xlabel(r"$\mathrm{r}/\mathrm{r}_{500}$", fontsize=axislabelsize)
        #if compare=="none": labelpad=5
        #else: labelpad=-5
        d_plt.set_ylabel(r"${\rho}_{\mathrm{gas}} / {\rho}_{\mathrm{crit}}$", fontsize=axislabelsize)#, labelpad=labelpad)
        if ylims is not None:
            d_plt.set_ylim(ylims[0], ylims[1])
        elif compare=="Croston": d_plt.set_ylim(1e-5, 0.1)
        elif compare=="Arnaud": d_plt.set_ylim(0.1, 1e3)
        #elif compare=="Sun": d_plt.set_ylim(10, 1e4)
        elif compare=="none": d_plt.set_ylim(1e-29, 1e-23)
        #if for_paper and (compare=="Sun" or compare=="Arnaud"): d_plt.yaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))

        if for_paper: d_plt.xaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))
    if plot_only=='temperature' or plot_only=='all':
        if plot_only=='all':
            T_plt = fig.add_subplot(gs[1], xscale='log', yscale='linear')
            T_plt.set_ylabel(r"$\mathrm{T}/\mathrm{T}_{500}$", fontsize=axislabelsize)#, labelpad=25)
        if plot_only=='temperature':
            T_plt = fig.add_subplot(gs[0], xscale='log', yscale='linear')
            T_plt.set_xlabel(r"$\mathrm{r}/\mathrm{r}_{500}$", fontsize=axislabelsize)
            T_plt.set_ylabel(r"$\mathrm{T}/\mathrm{T}_{500}$", fontsize=axislabelsize, labelpad=15) ## label might be changed further down
        #if compare=="Arnaud": T_plt.set_ylim(0.4, 3.0)
        #if compare=="Sun": T_plt.set_ylim(0, 2.0)
        #if compare=="none": T_plt.set_ylim(0.0, 1.2)
        if plot_only=='temperature' and ylims is not None:
            T_plt.set_ylim(ylims[0], ylims[1])
        #T_plt.yaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))
        if for_paper: T_plt.xaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))
    if plot_only=='entropy' or plot_only=='all':
        if plot_only=='all':
            S_plt = fig.add_subplot(gs[2], xscale='log', yscale='log')
            S_plt.set_ylabel(r"$\mathrm{K}/\mathrm{K}_{500\mathrm{, adi}}$", fontsize=axislabelsize)#, labelpad=5)
            d_plt.set_xticklabels([])
            T_plt.set_xticklabels([])
        if plot_only=='entropy':
            S_plt = fig.add_subplot(gs[0], xscale='log', yscale='log')
            S_plt.set_ylabel(r"$\mathrm{K}/\mathrm{K}_{500\mathrm{, adi}}$", fontsize=axislabelsize)
        S_plt.set_xlabel(r"$\mathrm{r}/\mathrm{r}_{500}$", fontsize=axislabelsize)
        #if compare=="none": S_plt.set_ylim(1.0, 1000.0)
        if compare=="Sun": S_plt.set_ylim(0.01, 4.0)
        if plot_only=='entropy' and ylims is not None:
            S_plt.set_ylim(ylims[0], ylims[1])
        if for_paper: S_plt.yaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))
        if for_paper: S_plt.xaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))
    if plot_only=='pressure':
        P_plt = fig.add_subplot(gs[0], xscale='log', yscale='log')
        if plot_residual:
            P_plt_res = fig.add_subplot(gs[1], xscale='log', yscale='linear')
            P_plt_res.set_xlabel(r"$\mathrm{r}/\mathrm{r}_{500}$", fontsize=axislabelsize)
            P_plt_res.set_ylabel(r"$(\mathrm{P}/\mathrm{P}_{500})/(\mathrm{P}/\mathrm{P}_{500})_{\mathrm{A10}}$", fontsize=axislabelsize, labelpad=10)
        if scaled:
            P_plt.set_ylabel(r"$\mathrm{P}/\mathrm{P}_{500}$", fontsize=axislabelsize)
            P_plt.set_ylim(0.03, 100)
        else:
            P_plt.set_ylabel(r"$\mathrm{P}$ [keV cm$^{-3}$]", fontsize=axislabelsize)
            P_plt.set_ylim(1e-6, 1)
        P_plt.set_xlabel(r"$\mathrm{r}/\mathrm{r}_{500}$", fontsize=axislabelsize)
        if ylims is not None:
            P_plt.set_ylim(ylims[0], ylims[1])
        if for_paper: P_plt.xaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))
        if for_paper and scaled: P_plt.yaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))
        ## Pressure profiles from REXCESS and Planck: P(x) = P(r)/P500 with x=r/r500
        #planck13 = lambda x: 6.41 * bias**(2.0/3.0) / ((1.81 * x / bias**(1.0/3.0))**0.31 * (1 + (1.81 * x)**1.33)**((4.13-0.31)/1.33))
        ## this arnaud fit is pos dodgy coz it doesn't include the dependence of the
        ## normalisation on M500 - I should plot the individal profiles or take the median myself
        arnaud10 = lambda x: 8.403 * (0.7/h_scale)**(3.0/2.0) / ((1.177 * x)**0.3081 * (1 + (1.177 * x)**1.051)**((5.4905-0.3081)/1.051))
    if plot_only=='mass':
        m_plt = fig.add_subplot(gs[0], xscale='log', yscale='log')
        m_plt.set_ylabel(r"$\mathrm{M}_{\mathrm{gas}}$ ($M_{\odot}$)", fontsize=axislabelsize)
        m_plt.set_xlabel(r"$\mathrm{r}/\mathrm{r}_{500}$", fontsize=axislabelsize)
        if plot_only=='mass' and ylims is not None:
            m_plt.set_ylim(ylims[0], ylims[1])
        if for_paper: m_plt.xaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))
    if plot_only=='gasfrac':
        f_plt = fig.add_subplot(gs[0], xscale='log', yscale='linear')
        f_plt.set_ylabel(r"$\mathrm{f}_{\mathrm{gas}}$", fontsize=axislabelsize)
        f_plt.set_xlabel(r"$\mathrm{r}/\mathrm{r}_{500}$", fontsize=axislabelsize)
        if plot_only=='gasfrac' and ylims is not None:
            f_plt.set_ylim(ylims[0], ylims[1])
            f_plt.yaxis.set_ticks(np.linspace(ylims[0], ylims[1], num=(ylims[1]-ylims[0])/0.04, endpoint=True))
        if for_paper: f_plt.xaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))

    if title is not "":
        ## Make room at top and bottom for title
        if plot_only=='all':
            outer.tight_layout(fig, rect=[0, 0.05, 1, 0.96])  ## rect is bounding box for subplots in normalised coords (x1, y1, x2, y2)
        else:
            gs.tight_layout(fig, rect=[0, 0.1, 1, 0.97])
        if not z_label: ## otherwise title is added later once we have the redshift
            fig.suptitle(title, fontsize=axislabelsize)
    else:
        if plot_only=='all':
            outer.tight_layout(fig, rect=[0, 0.04, 1, 1])
        else:
            gs.tight_layout(fig, rect=[0, 0.1, 1, 1])
    ## Colour bar
    col_given = True ## whether or not colours was specified in function call
    if colours is None: ## then set default colours
        col_given = False
        if not add_prof_labels:## use gradient of colours, else use discrete colours (see below)
            #from palettable.colorbrewer.sequential import RdPu_9_r as pallette
            #cmax = 0.5 ## increase to include lighter colours
            if blkwht:
                from palettable.colorbrewer.sequential import YlGnBu_7 as pallette
                cmax = 0.75 ## increase to include darker colours
            else:
                from palettable.colorbrewer.sequential import YlGnBu_9_r as pallette
                cmax = 0.6 ## increase to include lighter colours
            cmap=pallette.mpl_colormap
        if nplot == 1:
            colour_idx = [0]
    if plot_colour_scale:
        if plot_only=='all': c_plt = fig.add_subplot(gs2[0])
        elif plot_only=='pressure' and plot_residual: c_plt = fig.add_subplot(gs[2])
        else: c_plt = fig.add_subplot(gs[1])
        labelsM500 = []
    if plot_median:
        if blkwht:
            med_col=["#56b4e9",#9fcf49
                      "#cb484e",
                      "#b153cd",
                      "#e2a82c"]
            med_ls=['solid']*(nfiles)
#==============================================================================
#         elif for_paper:
#             if nfiles <= 4:
#                 from palettable.colorbrewer.sequential import YlGnBu_5 as pallette
#                 med_col=pallette.hex_colors[-4:]
#             else:
#                 from palettable.colorbrewer.qualitative import Dark2_8 as pallette
#                 med_col=pallette.hex_colors
#             med_ls=['solid']*nfiles
#==============================================================================
        else:
            med_col = ["#c7673e", "#78a44f", "#6b87c8", "#934fce", "#e6ab02",
                       "#be558f", "#47af8d"]
            med_ls = ['solid'] * nfiles
        if col_given:
            med_col = colours

    if M500_max < 0:
        M500_max = np.inf

    for filenum, infile in enumerate(infiles):
        # File checking
        if not os.path.exists(infile):
            print "File", infile, "not found."
            continue

        f = h5py.File(infile, 'r')
        grp_names = f.keys()
        grp_list = np.asarray(grp_names)
        N = len(grp_names)
        Nend = N
        if nplot == 0:
            Nstart = 0  # Plot all by default, but may restrict to M500 limits below
        else:
            assert (M500_min < 0), "Cannot define both nplot and M500_min"
            assert np.isinf(M500_max), "Cannot define both nplot and M500_max"
            Nstart = N - nplot
            
        if z_label and plot_only=='all':
            d_plt.set_title(title+" (z = "+"{:.1f})".format(f.attrs['redshift']), fontsize=axislabelsize)
            d_plt.title.set_position([0.5, 1.04])
        elif z_label:## this is broken - title is added to colour scale if one exists
            plt.gca().set_title(title+" (z = "+"{:.1f})".format(f.attrs['redshift']), fontsize=axislabelsize)
            plt.gca().title.set_position([0.5, 1.02])
    
        M500_list = []
        for i in range(0, N):
            if ('M500' in f[grp_list[i]].attrs):
                M500_list.append(f[grp_list[i]].attrs['M500']/h_scale/1e13) # 1e13 Msun
        M500_list = np.asarray(M500_list, dtype=np.float64)
        assert sum(M500_list >= M500_min) > 0, "No objects in given mass range."
        M500expon = int(np.log10(np.min(M500_list[M500_list >= M500_min]*1e13)))
        if sort_by_M500 and len(M500_list) > 0:
            ## Sort groups lowest M500 to highest
            sorter = np.argsort(M500_list)
            M500_list = M500_list[sorter]
            grp_list = grp_list[sorter]
            ## Plot those with M500 above M500_min (1e13 Msun)
            if M500_min >= 0: Nstart = np.min(np.where(M500_list >= M500_min))
            if M500_max >= 0: Nend = np.max(np.where(M500_list <= M500_max)) + 1
        #else:
            ## Sort groups in (reverse) order of group name e.g. ID-19, ID-18... ID-01, ID-00
            #grp_list = np.sort(grp_list)[::-1]
            
        if add_prof_labels and not col_given:
            #cmap = plt.get_cmap('rainbow')
            #from palettable.colorbrewer.qualitative import Dark2_7_r as pallette
            import palettable.colorbrewer.qualitative as pallette
            cmap=getattr(pallette, "Dark2_"+str(max(3,min(8,Nend-Nstart)))+"_r").mpl_colormap
            #from matplotlib import colors
            #cmap = colors.ListedColormap(["#b987d2","#bd60e5","#6999d8","#787aeb"])
            if Nend-Nstart==2: cmax=0.5
            else: cmax=1.0
                
        if not col_given:
            colour_idx = np.linspace(cmax, 0, Nend-Nstart)
            colours = cmap(colour_idx)
        if Nend-Nstart <=1: plot_colour_scale = False
        if not medians_only:
            assert len(colours) >= Nend-Nstart, "Need more colours."
        if ls is None:
            ls = ['solid']*(Nend-Nstart)
        if not medians_only:
            assert len(ls) >= Nend-Nstart, "Need more linestyles."
        #lw=1.0+(2.0/(Nend-Nstart))

        nbins = None
        for i in range(Nstart, Nend):
            print "Plotting", grp_list[i], "(M500 = {:.1e})".format(f[grp_list[i]].attrs['M500'] / h_scale)
            group = f[grp_list[i]]
            z = group.attrs['redshift']
            if filetype=="radprof": ## haven't added units to xray profiles yet
                assert 'LengthUnits' in group.attrs, "These profiles are in an old format and need updating."
                assert (group.attrs['LengthUnits'] == "pkpc/h"), "Incorrect length units"
                assert (group.attrs['MassUnits'] == "Msun/h"), "Incorrect mass units"
            M500 = group.attrs['M500'] / h_scale ## Msun
            R500 = group.attrs['R500'] / h_scale ## pkpc
            
            if filetype=="radprof":
                if filtered:
                    T_suffix = "Filt"
                    d_suffix = "Filt"
                else:
                    T_suffix = "UnFilt"
                    d_suffix = "UnFilt"
                    
                if comp_unfilt and not filtered:
                    print "Setting comp_unfilt to False"
                    comp_unfilt = False
    
                assert group['bins'].attrs['units'] == "pkpc/h"
                bins = np.asarray(group['bins']) / h_scale ## pkpc
                if nbins is not None:
                    assert len(bins[:,0]) == nbins, "Profiles must have same number of bins. nbins = "+str(nbins)+" but len(bins) = "+str(len(bins[:,0]))
                nbins = len(bins[:,0])
                
                assert group['density'+d_suffix].attrs['units'] == "Msun h2 pkpc-3"
                density = np.asarray(group['density'+d_suffix]) * h_scale**2 * Msun_in_g / kpc_cm**3 ## g cm^-3
                #dens_unit = "g cm$^{-3}$"
                if comp_unfilt and filtered:
                    densityUF = np.asarray(group['density'+"UnFilt"]) * h_scale**2 * Msun_in_g / kpc_cm**3 ## g cm^-3
                    
                assert group['n_e_'+d_suffix].attrs['units'] == "h2 cm-3"
                n_e = np.asarray(group['n_e_'+d_suffix]) * h_scale**2 ## cm^-3
                ne500 = group.attrs['ne500'+d_suffix] * h_scale**2 ## cm^-3
                
                assert group['temperature'+T_suffix].attrs['units'] == "keV"
                temp = np.asarray(group['temperature'+T_suffix]) ## keV
                #temp_unit = "keV"
                #T500 = group.attrs['T500'+T_suffix]
                #T2500_Sun = group.attrs['T2500_Sun_'+T_suffix+"_sph"] ## add "_sph" to use T2500 within a core-excised spherical aperture rather than projected
                #T2500_Sun = group.attrs['T2500'+T_suffix] ## T2500_Sun doesn't seem to work so use T2500 in 3D aperture
                Tmean = group.attrs['Tmean'+T_suffix] ## mean temperature
                TX = group.attrs['TX_'+T_suffix]
                
                assert group['entropy'+T_suffix].attrs['units'] == "keV h-4/3 cm2"
                S = np.asarray(group['entropy'+T_suffix]) / h_scale**(4.0/3.0) ## keV cm^2
                ## rederive entropy if temp and density filters are different
                #S = temp / n_e**(2.0/3.0) ## keV cm^2
                #S_unit = "keV cm$^2$"
                
                #assert (group['pressure'+d_suffix].attrs['units'] == "keV h2 cm-3") or (group['pressure'+d_suffix].attrs['units'] == "keV h3 cm-3") ## is actually h^2 but i screwed up units before
                #P = np.asarray(group['pressure'+d_suffix]) * h_scale**2 ## keV cm^-3
                P = temp * n_e
                
                assert group['gasmass'+d_suffix].attrs['units'] == "Msun/h"
                gasmass = np.asarray(group['gasmass'+d_suffix]) / h_scale ## Msun (converting in g causes infs)
                if comp_unfilt and filtered:
                    gasmassUF = np.asarray(group['gasmass'+"UnFilt"]) / h_scale ## Msun (converting in g causes infs)
                    
                assert group['mass_all'].attrs['units'] == "Msun/h"
                totmass = np.asarray(group['mass_all']) / h_scale ## Msun (converting in g causes infs)
                
                T_number = group['number_'+T_suffix]
                d_number = group['number_'+d_suffix] ## will be same as T_number unless temp uses a different filter to density
            elif filetype=="xray":
                Rin = np.asarray(group['Rin'][:,0]) / h_scale ## pkpc
                Rout = np.asarray(group['Rout'][:,0]) / h_scale ## pkpc
                Rcent = (Rin * Rout)**0.5
                bins = np.c_[Rin, Rcent, Rout]
                if nbins is not None:
                    assert len(bins[:,0]) == nbins, "Profiles must have same number of bins. nbins = "+str(nbins)+" but len(bins) = "+str(len(bins[:,0]))
                nbins = len(bins[:,0])
                
                ## Get norm (emission measure) and calculate corresponding density using abundance
                norm = np.asarray(group['norm'][:,0]) * (h_scale / group.attrs['HubbleParam'])**3 ## rescale to h_scale
                abund = np.asarray(group['abundance'][:,0]) ## metal abundance
                binvol = 4.0/3.0 * np.pi * (Rout**3 - Rin**3) * kpc_cm**3 ## cm^3
                ## Estimate n_e*n_H (cm^-6) from the best-fit norm (emission measure of gas scaled by DA)
                da_cm = cosmology(group.attrs['HubbleParam'], omega_m).da(max(z, 0.001))*1000.0*kpc_cm / h_scale # angular diameter distance in cm, uses z=0.001 if z<0.001 consistent with xray.py
                nenH = norm * 1e14 / binvol * (4*np.pi * da_cm**2 * (1+z)**2) ## assumes constant density in bin
                nelec = 1.0 + 0.5*ag_helium/ag_hydrogen + 0.5*abund*ag_metals/ag_hydrogen ## ne/nH
                n_e = np.sqrt(nenH * nelec) ## spectrally-derived electron number density 
                density = np.zeros_like(n_e) ## set mass density to zero for now
                gasmass = np.zeros_like(n_e)
                totmass = np.zeros_like(n_e)
                
                temp = np.asarray(group['temp_spec'][:,0]) ## keV
                S = temp / n_e**(2.0/3.0) ## keV cm^2
                P = temp * n_e
                
                T_number = np.asarray(group['number'][:,0]) ## number of gas cells in each bin
                d_number = np.copy(T_number)
                
            if add_prof_labels:
                if prof_labels is not None:
                    label = prof_labels[i]
                else:
                    label = grp_list[i].replace("_", "\_")
            else:
                label = None
            
            vertlines = [] ## Positions of vertical lines
            if ('SofteningGas' in group.attrs):
                vertlines.append(group.attrs['SofteningGas'])
    
            if plot_colour_scale:
                labelsM500.append(M500 / 10**M500expon)

            Rvir = R500 ## scale radius

            ## SCALING factors ##
            Ez = pow(omega_m * pow(1+z, 3.0) + omega_l, 0.5)
            #rho_crit = 3.0/8.0 / pi / 6.67408e-11 * (1.0 * 100 / 3.086e+19)**2.0 * (3.086e+19)**3.0 / 1.989e+30 / 1e+10 ## Critical density in 10^10 Msun h^2 kpc^-3 (h = 1.0)
            #rho_crit = 2.77492421e-8 ## Critical density in 10^10 Msun h^2 kpc^-3 (same as above)
            rho_crit_today = (3.0/8.0 / np.pi / 6.67408e-8 * (h_scale * 100 / 3.086e+19)**2.0) ## Critical density today in g cm^-3
            rho_crit = (3.0/8.0 / np.pi / 6.67408e-8 * (h_scale * 100 / 3.086e+19)**2.0) * Ez**2 ## Critical density at redshift z in g cm^-3
            
            ## Self-similar scaling factors - see Arnaud+2010 Appendix A or Barnes+2017
            ## WARNING: If you change the following three parameters then the comparison with REXCESS will be wrong - I need to code this possibility into REXCESS.py
            mu = 0.59 ## as used in Arnaud+2010 and Nagai+2007 - doesn't matter exactly what this is as long as it's consistent between mine and the observed profiles
            mu_e = 1.14 ## mean atomic weight per free electron as used in Arnaud+2010 and Nagai+2007
            fB = omega_b / omega_m #=0.1576 - Arnaud+2010 use 0.175 but I rescale their P500 to my fB in REXCESS.py
            mp = 1.6726231e-24 ## proton mass in g
            G = 6.67259e-8 ## gravitational constant in cgs
            T500_ss = G * mu * mp * M500*1.989e33 / (2.0 * R500 * 3.085678e21) / 1.6021772e-9
            ne500_ss = 500 * fB * rho_crit / (mu_e * mp) ## cm^-3
            P500_ss = ne500_ss * T500_ss ## keV cm^-3
            S500_ss = T500_ss / ne500_ss**(2.0/3.0) ## keV cm^2
            ## Direct calculation without intermediate steps:
            #K500 = mu * mp**(5.0/3.0) * (mu_e * 2.0 * np.pi / 3.0 / fB)**(2.0/3.0) / (500.0 * (h_scale*Ez * 100 / 3.086e+19)**2 / G**4)**(1.0/3.0) / 1.6021772e-9 * (M500*1.989e33)**(2.0/3.0)
            
            # other scalings
            ## Sun+2009
            #Svir = 342.0 * pow(M500 / 1e14 / Ez, 2.0/3.0) / pow(h_scale/0.73, 4.0/3.0) ## Sun et al 2009 (h0 = 0.73 fb=0.165)
            ## ^technically there is a fb^-2/3 dependence in the entropy scale, but this is only a ~3% (0.01 dex) effect for Planck fb=0.157 as used in the boxes
            ## Arnaud+2010 and Nagai+2007 (same as P500_ss if fB=0.175, mu=0.59 and mu_e=1.14)
            #P500 = (1.65e-3 * pow(Ez, 8.0/3.0) * pow(M500 / 3.0e14 * (h_scale/0.7), 2.0/3.0) * (h_scale/0.7)**2) ## keV cm^-3
            
            ## Mask radial range
            bins_rel = bins / Rvir
            if plot_only=="mass": bin_idx = 2 ## plot right-most edge of bin
            else: bin_idx = 1 ## plot centre of bin
            if rmax < 0: rmax = np.inf ## this causes problems when making bins for observed profiles
            #if rmax < 0: rmax = np.max(bins_rel[:,2]) ## should be the same for all profiles since they're all scaled
            vertlines /= Rvir
            ## I should really use masked arrays...
            mask = (bins_rel[:,0] >= rmin) & (bins_rel[:,2] <= rmax)
            bins_rel = bins_rel[mask, :]
            density = np.asarray(density)[mask]
            cumgasmass = np.cumsum(gasmass, axis=0)[mask]
            gasmass = np.asarray(gasmass)[mask]
            if comp_unfilt:
                densityUF = np.asarray(densityUF)[mask]
                gasmassUF = np.asarray(gasmassUF)[mask]
            totmass = np.asarray(totmass)[mask]
            n_e = np.asarray(n_e)[mask]
            temp = np.asarray(temp)[mask]
            S = np.asarray(S)[mask]
            P = np.asarray(P)[mask]
            d_number = np.asarray(d_number)[mask]
            T_number = np.asarray(T_number)[mask]
            if plot_median:
                d_numbermask = (d_number == 0) ## Exclude bins with zero cells
                T_numbermask = (T_number == 0)
            else:
                X = 3
                d_numbermask = (d_number <= X) ## Exclude bins with less than X cells
                T_numbermask = (T_number <= X)
            
            ## Scaling ##
            ## Scale density
            if plot_only=='all' or plot_only=='density':
                if compare=="Sun" or compare=="Croston" or compare=="REXCESS":
                    if scaledbyR:
                        yd = np.ma.masked_where(d_numbermask[:,0], n_e[:,0] * bins_rel[:, bin_idx]**2.0 / Ez**2)
                        d_plt.set_ylabel(r"$(\mathrm{E(z)}^{-2}$ $\mathrm{n}_{\mathrm{e}}) (r/r_{500})^2$ [cm$^{-3}$]", labelpad=1)
                    else:
                        yd = np.ma.masked_where(d_numbermask, n_e / Ez**2)
                        d_plt.set_ylabel(r"$\mathrm{E(z)}^{-2}$ $\mathrm{n}_{\mathrm{e}}$ [cm$^{-3}$]", labelpad=1)
                elif compare=="Arnaud":
                    yd = np.ma.masked_where(d_numbermask, n_e / float(ne500))
                    d_plt.set_ylabel(r"$\mathrm{n}_{\mathrm{e}} / \langle\mathrm{n}_{\mathrm{e}}\rangle_{R_{500}}$")
                elif compare=="Borgani":
                    yd = np.ma.masked_where(d_numbermask, density / 100.0/float(rho_crit_today))
                    d_plt.set_ylabel(r"${\rho}_{\mathrm{gas}} / {\rho}_{\mathrm{vir}}$")
                elif compare=="TNG":
                    yd = np.ma.masked_where(d_numbermask, n_e)
                    d_plt.set_ylabel(r"$\mathrm{n}_{\mathrm{e}}$ [cm$^{-3}$]", labelpad=1)
                elif compare=="none":
                    yd = np.ma.masked_where(d_numbermask, density)
                    d_plt.set_ylabel(r"${\rho}_{\mathrm{gas}}$ (g cm$^{-3}$)")
                else:##default to rho_crit scaling
                    yd = np.ma.masked_where(d_numbermask, density / float(rho_crit))
                    if comp_unfilt:
                        yUF = np.ma.masked_where(d_numbermask, densityUF / float(rho_crit))

            ## Scale temperature
            if plot_only=='all' or plot_only=='temperature':
                if compare=="Rasmussen":
                    T_plt.set_ylabel(r"$\mathrm{T}/<\mathrm{T}>$")
                    yT = np.ma.masked_where(T_numbermask, np.asarray(temp) / Tmean)
                elif compare=="Sun":
                    if scaled: ## T/T500
                        T_plt.set_ylabel(r"$\mathrm{T}/\mathrm{T}_{500}$")
                        yT = np.ma.masked_where(T_numbermask, np.asarray(temp) / T500_ss)
                    else:
                        T_plt.set_ylabel(r"$\mathrm{T}$ [keV]")
                        yT = np.ma.masked_where(T_numbermask, np.asarray(temp))
                elif compare=="REXCESS":
                    if scaled:
                        yT = np.ma.masked_where(T_numbermask, np.asarray(temp) / T500_ss) ## self-similar T500
                    else:
                        yT = np.ma.masked_where(T_numbermask, np.asarray(temp))
                        T_plt.set_ylabel(r"$\mathrm{T}$ [keV]")
                    #yT = np.ma.masked_where(T_numbermask, np.asarray(temp)) ## keV
                    #T_plt.set_ylabel(r"$\mathrm{T}$ [keV]")
                    #yT = np.ma.masked_where(T_numbermask, np.asarray(temp) / T500) ## T500 measured in 3D aperture
                    #T_plt.set_ylabel(r"$\mathrm{T}/\mathrm{T}_{500}$")
                elif compare=="Arnaud":
                    T_plt.set_ylabel(r"$\mathrm{T}/\mathrm{T}_{\mathrm X}$")
                    yT = np.ma.masked_where(T_numbermask, np.asarray(temp) / TX)
                elif compare=="TNG":
                    T_plt.set_ylabel(r"$\mathrm{T}$ [keV]")
                    yT = np.ma.masked_where(T_numbermask, np.asarray(temp))
                elif compare=="none":
                    T_plt.set_ylabel(r"$\mathrm{T}$ [keV]")
                    yT = np.ma.masked_where(T_numbermask, np.asarray(temp))
                else:
                    #print "Using self-similar temperature scaling"
                    yT = np.ma.masked_where(T_numbermask, np.asarray(temp) / T500_ss)
                    #bin_vir = (bins_rel[:,0] <= 1.0) & (bins_rel[:,2] >= 1.0)
                    #Tvir = np.asarray(temp)[bin_vir]
                    #yT = np.ma.masked_where(numbermask, np.asarray(temp) / float(Tvir[0]))

            ## Scale entropy etc
            if plot_only=='all' or plot_only=='entropy':
                if compare=="Sun" or compare=="Sun2D":
                    yS = np.ma.masked_where(T_numbermask, S / float(S500_ss))
                    S_plt.set_ylabel(r"$\mathrm{K}/\mathrm{K}_{500}$", fontsize=axislabelsize)
                elif compare=="REXCESS" and scaled:
                    yS = np.ma.masked_where(T_numbermask, S / float(S500_ss))
                    S_plt.set_ylabel(r"$\mathrm{K}/\mathrm{K}_{500}$", fontsize=axislabelsize)
                else:## REXCESS, TNG, none
                    yS = np.ma.masked_where(T_numbermask, S)
                    S_plt.set_ylabel(r"K [keV cm$^2$]")
            if plot_only=='pressure':
                if scaled:
                    yP = np.ma.masked_where(T_numbermask, np.asarray(P) / float(P500_ss))
                else:
                    yP = np.ma.masked_where(T_numbermask, np.asarray(P))
                if plot_residual:
                    yP_res = yP[:,0] / arnaud10(bins_rel[:,1])
                    print "yP[:,0] = ", yP[:,0]
                    print "arnaud10(bins_rel[:,1]) =", arnaud10(bins_rel[:,1])
                    print "yP_res =", yP_res
            if plot_only=='mass':
                if scaled:
                    ym = np.ma.masked_where(d_numbermask, cumgasmass / cumgasmass[-1])
                    m_plt.set_ylabel(r"$\mathrm{M}_{\mathrm{gas}}(r) / \mathrm{M}_{\mathrm{gas}}(r_{\mathrm{max}})$", fontsize=axislabelsize)
                else:
                    ym = np.ma.masked_where(d_numbermask, cumgasmass)
            if plot_only=='gasfrac':
                yf = np.ma.masked_where(d_numbermask, gasmass / totmass)
                if comp_unfilt:
                    yUF = np.ma.masked_where(d_numbermask, gasmassUF / totmass)
                
            ## PLOTTING ##
            if plot_only=='all':
                y = (yd, yT, yS)
                axes = (d_plt, T_plt, S_plt)
            if plot_only=='density':
                y = [yd]
                axes = [d_plt]
            if plot_only=='temperature':
                y = [yT]
                axes = [T_plt]
            if plot_only=='entropy':
                y = [yS]
                axes = [S_plt]
            if plot_only=='pressure':
                if plot_residual:
                    y = [yP, yP_res]
                    axes = [P_plt, P_plt_res]
                else:
                    y = [yP]
                    axes = [P_plt]
            if plot_only=='mass':
                y = [ym]
                axes = [m_plt]
            if plot_only=='gasfrac':
                y = [yf]
                axes = [f_plt]
            if comp_unfilt:
                yUF = [yUF]
            y1 = [y1[np.ma.flatnotmasked_edges(y1)] for y1 in y] ## Plot points at either end of lines...
            ## ...since the first point will not be visible if it cannot join with the second point
            if rmin > 0: xmin = rmin
            else: xmin = min(bins_rel[:,0])
            if np.isfinite(rmax): xmax = rmax
            else: xmax = max(bins_rel[:,2])
            for j, ax in enumerate(axes):
                if not medians_only:
                    ax.plot(bins_rel[:,bin_idx], y[j], '-', lw=lw,
                        color=colours[i-Nstart], label=label, ls=ls[i-Nstart], dash_capstyle='round')
                    ax.plot(bins_rel[np.ma.flatnotmasked_edges(y[j]), bin_idx], y1[j], 'o',
                                 ms=5, mec='none', mfc=colours[i-Nstart])
                    if comp_unfilt:
                        ax.plot(bins_rel[:,bin_idx], yUF[0], '--', lw=lw,
                        color=colours[i-Nstart])#, dashes=(0.5,6), dash_capstyle='round')
                if i==Nstart:
                    plt.sca(ax)
                    plt.xlim(xmin, xmax)
                    if plot_vertlines:
                        for vl in vertlines: ## Plot vertical dashed lines for softening length
                            plt.axvline(x=vl, color=colours[i-Nstart], linestyle='dashed', linewidth=0.5)

            ## Calculate median
            if plot_median:
                if i==Nstart:
                    y_array = y[0]
                    ax = axes[0]
                    if comp_unfilt:
                        y_arrayUF = yUF[0]
                else:
                    y_array = np.ma.concatenate((y_array, y[0]), axis=1)
                    if comp_unfilt:
                        y_arrayUF = np.ma.concatenate((y_arrayUF, yUF[0]), axis=1)
                
            ## Calculate Means 
            if plot_mean:
                if i==Nstart:
                    if plot_only=='all': y_mean = np.zeros((len(temp), 5)) ## Array to hold means
                    else: y_mean = np.zeros((len(temp), 3)) ## Array to hold means
                y_mean[:,0] += bins_rel[:,1] ## NEED TO FIX THIS - average bin centre radii
                y_mean[:,1] += np.logical_not(yd[:,0].mask) ## Count number of non-zero bins in sum
                if plot_only=='all':
                    y_mean[:,2] += yd[:,0] ## sum of density in each bin
                    y_mean[:,3] += yT[:,0] ## sum of temperature in each bin
                    y_mean[:,4] += yS[:,0] ## sum of entropy in each bin
                if plot_only=='density': y_mean[:,2] += yd[:,0]
                if plot_only=='temperature': y_mean[:,2] += yT[:,0]
                if plot_only=='entropy': y_mean[:,2] += yS[:,0]
                if plot_only=='pressure': y_mean[:,2] += yP[:,0]
    
        if plot_median:
            med_bins = np.concatenate((bins_rel[:,1],bins_rel[::-1,1]))
            if add_file_labels:
                if file_labels is not None:
                    label_median = file_labels[filenum]
                else:
                    label_median = infile.replace("_", "\_")
            else: label_median = None
            # print "y_array =", y_array[np.isnan(y_array)]
            med = np.median(y_array, axis=1) ## median of each column
            err1 = np.ma.asarray([np.percentile(y_array[k,:], 16.0) for k in range(len(y_array[:,0]))])
            err2 = np.ma.asarray([np.percentile(y_array[k,:], 84.0) for k in range(len(y_array[:,0]))])
            if not medians_only:
                ax.plot(bins_rel[:,1], med, lw=lw, c='0.5', label="Median", zorder=-1)
                #ax.plot(bins_rel[:,1], err1, linestyle='--', lw=lw, c='black')
                #ax.plot(bins_rel[:,1], err2, linestyle='--', lw=lw, c='black')
                err = np.ma.concatenate((err1,err2[::-1]))
                patch = np.ma.column_stack((med_bins, err))
                ax.axes.add_patch(Polygon(patch[patch[:,1]>0], alpha=0.2, color='0.5', ec='none', zorder=-2))
            else:
                ax.plot(bins_rel[:,1], med, ls=med_ls[filenum], dash_capstyle='round', lw=lw, c=med_col[filenum], label=label_median)#med_cmap(line_colour_idx[filenum])
                if filenum in scat_ind:
                    err = np.ma.concatenate((err1,err2[::-1]))
                    patch = np.ma.column_stack((med_bins, err))
                    ax.axes.add_patch(Polygon(patch[patch[:,1]>0], alpha=0.2, color=med_col[filenum], ec='none'))
            if comp_unfilt:
                medUF = np.median(y_arrayUF, axis=1)
                ax.plot(bins_rel[:,1], medUF, ls='dashed', dash_capstyle='round', lw=lw, c=med_col[filenum])

        if plot_mean:
            nonzero = np.where(y_mean[:,1] > 0) ## Avoid dividing by zero
            y_mean[:, 0] = y_mean[:, 0] / len(y_mean[:, 0]) ## average bin radius
            j = 2
            for ax in axes:
                y_mean[nonzero, j] = y_mean[nonzero, j] / y_mean[nonzero, 1] ## arithmetic mean of density, temp, entropy
                average, = ax.plot(bins_rel[:,1], np.ma.masked_where(y_mean[:,1] < 1, y_mean[:, j]), linestyle='--', lw=1, marker='D', ms=3, mec='black', mfc='black', color='black', label="Average")
                j += 1
        
        if filenum==0:
            Xh=0.76
            ne2nh = 1.0 + 2.0*(1.0-Xh)/(4.0*Xh) # rough approximation assuming full ionization and neglecting metallicity
            PROTONMASS = 1.6726e-24 # g
            ELECTRONMASS = 9.1094e-28 # g
            ne2rho = ne2nh * (PROTONMASS + ELECTRONMASS) / Xh

            #ACCEPTnames = ["NGC_5846", "HCG_42", "HCG_0062",
                           #"NGC_5044", "NGC_0507", "MKW_04", "ESO_5520200"]
            #ACCEPTz = [0.0057, 0.0133, 0.0146, 0.009, 0.0164, 0.0198, 0.0314]
            #ACCEPTr500 = [309, 324, 403, 430, 467, 690, 580]
            #ACCEPT_temp = [0.64, 0.7, 1.1, 1.22, 1.4, 2.16, 2.34]
            REXM500max = M500_max * 1e13#9.0e14 ## max mass to plot (largest REXCESS mass is 8.41e14 Msun)
            if blkwht:
                REXalpha = 0.3
                REXcol = ['0.65']*3## normal, CC, disturbed
                REXlabels = ["relaxed", "cool-core", "disturbed"]
                REXmedcol = '0.8'
                REXmedlabel = "median"
            else:
                REXalpha = 0.6
                #REXcol = ['grey','blue', 'green']## normal, CC, disturbed
                REXcol = ['0.8', '0.8', '0.8']## normal, CC, disturbed
                REXlabels = [None]*3
                REXmedcol = '0.5'
                REXmedlabel = "REXCESS"
            REXls = ['solid', 'dotted', 'dashed']
            REXlw = 2
            REXmedlw = 3
            REXbias = 1.0 - b ## halo mass bias 1-b e.g. 0.7
            if REXbias != 1.0 and plot_only=='temperature': REXmedlabel+="\n(1-b)="+str(REXbias)
            if plot_only=="mass" or compare=="Croston" or compare=="REXCESS":
                rexcesstable = np.genfromtxt(obs_dir+"REXCESS_profiles/REXCESS_table_Pratt2009.csv", delimiter=",", dtype=None)
                CC = rexcesstable['f22']
                disturbed = rexcesstable['f23']
                rex_mass = rexcesstable['f24'] ## 1e14 Msun
                zrex = rexcesstable['f1']
                E_zrex = np.sqrt(omega_m*(1+zrex)**3 + omega_l)
                #rhocrit = (3.0/8.0/np.pi/6.67259e-8 * (70.0*1e3/3.0857e22)**2) * (omega_m*(1+zrex)**3 + omega_l) ## g / cm^3
                
            ## Colours for Sun+2009 sample
            if blkwht:
                Suncol = '0.6'
                Sunmedcol = '0.8'
                Sunalpha = 0.2
                Sunlw = 2
                Sunmedlw = 3
            else:
                Suncol = '#0C0C33'#'0.2'#'#000099'
                Sunmedcol = '0.5'
                Sunalpha = 0.5
                Sunlw = 2
                Sunmedlw = 3
            
            if plot_only=='all' or plot_only=='density':
                if compare=="Arnaud":
                    d_obs = np.loadtxt(obs_dir+"Arnaud2010_density_profile.txt")
                    d_plt.plot(d_obs[:,0], d_obs[:,1], c='0.8', lw=3, zorder=1,
                               label="Arnaud et al. 2010")
                    d_obs2 = np.loadtxt(obs_dir+"Arnaud2010_density_profile_range.txt")
                    poly_d_obs2 = Polygon(d_obs2, color='0.9', ec='none')
                    d_plt.axes.add_patch(poly_d_obs2)
                elif compare=="Croston" or compare=="REXCESS":
                    #REXmedlabel = "REXCESS"#"Croston+ 2008"
                    rexcess_ids = range(len(CC))
                    #rexcess_ids = [1,3,9,10,15,18,19,21]## zero indexing
                    #rexcess_ids = [25,15,14,19,9,23,27,1]
                    
                    for rexid in rexcess_ids:
                        data = np.loadtxt(obs_dir+"REXCESS_profiles/density_profiles/REXCESS_density_profile_t"+str(rexid+1)+".csv", delimiter=",")
                        rexdens = data[:,2] / E_zrex[rexid]**2 * (h_scale/0.7)**2
                        #rexdens = data[:,2] * ne2rho / rhocrit[rexid]
                        if scaledbyR:
                            rexdens *= (data[:,1]*(h_scale/0.7))**2.0
                        if rex_mass[rexid]*1e14 < REXM500max:## mass limit
                            if CC[rexid] == 1:
                                d_plt.plot(data[:,1]*(h_scale/0.7), rexdens, c=REXcol[1], alpha=REXalpha, ls=REXls[1], lw=REXlw, dash_capstyle='round', zorder=1, label=REXlabels[1])
                                REXlabels[1] = ""
                            elif disturbed[rexid] == 1:
                                d_plt.plot(data[:,1]*(h_scale/0.7), rexdens, c=REXcol[2], alpha=REXalpha, ls=REXls[2], lw=REXlw, dash_capstyle='round', zorder=1, label=REXlabels[2])
                                REXlabels[2] = ""
                            else:
                                d_plt.plot(data[:,1]*(h_scale/0.7), rexdens, c=REXcol[0], alpha=REXalpha, ls=REXls[0], lw=REXlw, dash_capstyle='round', zorder=1, label=REXlabels[0])
                                REXlabels[0] = ""
#==============================================================================
#                         import profiles.REXCESS as REXCESS
#                         if REX_match_med:
#                             med_mass = np.median(labelsM500) * 10**M500expon
#                         else:
#                             med_mass = None
#                         x, med, err1, err2 = REXCESS.get_density_prof(rmin=rmin, rmax=2.0, target_med=med_mass, M500min=0.0, M500max=REXM500max, h=h_scale)
#                         d_plt.errorbar(x, med, yerr=[err1,err2], marker='s', ls='none', c='#000099', mec='none', label=REXmedlabel, zorder=3)
#                         
#==============================================================================
                    ## median of all REXCESS clusters:
                    import profiles.REXCESS as REXCESS
                    x, med, err1, err2 = REXCESS.get_density_prof(rmin=rmin*0.1, rmax=rmax*10, dlogr=0.1, M500min=0.0, M500max=np.inf, h=h_scale)
                    if scaledbyR:
                        med *= x**2.0
                    #d_plt.errorbar(x, med, yerr=[err1,err2], alpha=0.5, marker='s', ls='none', c='#000099', mec='none', label=REXmedlabel, zorder=3)
                    d_plt.errorbar(x, med, c=REXmedcol, lw=REXmedlw, dashes=(14,8), alpha=1, zorder=1, label=REXmedlabel)
                    '''
                    ## Colour code by mass (r500^3)
                    from palettable.colorbrewer.sequential import YlGnBu_9 as rex_pallette
                    cmap_rex=rex_pallette.mpl_colormap
                    rex_r500 = rexcesstable['f2']
                    rex_col_idx = 0.3 + (rex_r500**3 - np.min(rex_r500**3)) / (np.max(rex_r500**3) - np.min(rex_r500**3))
                    for rexid in range(len(CC)):
                        data = np.loadtxt(obs_dir+"REXCESS_profiles/density_profiles/REXCESS_density_profile_t"+str(rexid+1)+".csv", delimiter=",")
                        rexdens = data[:,2] / E_zrex[rexid]**2 * (h_scale/0.7)**2
                        d_plt.plot(data[:,1], rexdens, c=cmap_rex(rex_col_idx[rexid]), alpha=0.6, lw=2, zorder=3)
                    '''
#==============================================================================
#                         Hicks = np.loadtxt(obs_dir+"Hicks2013_density_profile.txt")
#                         Hicks_dens = Hicks[:,1]# * ne2rho / ((3.0/8.0/np.pi/6.67259e-8 * (70.0*1e3/3.0857e22)**2))
#                         d_plt.plot(Hicks[:,0], Hicks_dens, c='orange', lw=3, label="Hicks et al. 2013")
#==============================================================================
                    
                    #data = np.loadtxt(obs_dir+"Croston2008_density_h-2.txt")
                    #d_plt.axes.add_patch(Polygon(data, color='red', alpha=0.5, ec='none'))
                    
#==============================================================================
#                         ## Sun 2009
#                         data = np.loadtxt("/data/vault/nh444/ObsData/profiles/Sun2009_density_new.txt")
#                         n = 3
#                         d_plt.errorbar(10**data[::n,0], 10**data[::n,1], yerr=[10**data[::n,1]-10**data[::n,3], 10**data[::n,2]-10**data[::n,1]],
#                                        zorder=3, fmt='o', mew=1.2, lw=1.2, ms=5, mec='none',
#                                        mfc='#000099', color='#000099', ecolor='#000099',
#                                        label="Sun et al. (2009)")
#                         d_plt.plot(10**data[:,0], 10**data[:,1], zorder=3, lw=2, c='#000099', label="Sun et al. (2009)")
#                         patch = np.column_stack((np.concatenate((10**data[:,0], 10**data[::-1, 0])), np.concatenate((10**data[:,3], 10**data[::-1,2]))))
#                         d_plt.axes.add_patch(Polygon(patch, alpha=0.2, color='#000099', ec='none', zorder=2))
#                         
#==============================================================================
                elif compare=="Borgani":
                    data = np.loadtxt(obs_dir+"Borgani_2004_density.txt")
                    d_plt.axes.add_patch(Polygon(data, color='red', alpha=0.5, ec='none', label="Borgani et al. (2004)"))
                    for rexid in range(len(CC)):
                        data = np.loadtxt(obs_dir+"REXCESS_profiles/density_profiles/REXCESS_density_profile_t"+str(rexid+1)+".csv", delimiter=",")
                        rexdens = data[:,2] / E_zrex[rexid]**2 * (h_scale/0.7)**2 * ne2rho / (100.0*rho_crit_today)
                        if CC[rexid] == 1:
                            d_plt.plot(data[:,1], rexdens, c='blue', alpha=0.3, lw=2, zorder=1)
                        elif disturbed[rexid] == 1:
                            d_plt.plot(data[:,1], rexdens, c='green', alpha=0.3, lw=2, zorder=1)
                        else:
                            d_plt.plot(data[:,1], rexdens, c='grey', alpha=0.3, lw=2, zorder=1)
                elif compare=="Sun":
                    xbins = np.logspace(np.log10(max(0.01, xmin)), np.log10(min(xmax, 1.0)), num=(np.log10(xmax) - np.log10(xmin))/0.08)
                    from glob import glob
                    d_files = glob(obs_dir+"Sun_2009_density_profiles/*_den")
                    Suncat = np.genfromtxt(obs_dir+"Sun2009.csv", delimiter=",", dtype=None) ## dtype=None guesses data type (can be slow)
                    import os
                    all_values = np.zeros((len(xbins), len(d_files)))
                    print d_files[17:18]
                    for d_idx, d_file in enumerate(d_files):
                        dat = np.loadtxt(d_file) ## log10(r/kpc)   T_3d (keV)   lower_boundary  upper_boundary
                        ## Find r500 from catalogue of sun+2009 objects
                        idx = np.where(Suncat['f2'] == os.path.basename(d_file).replace("_den",""))[0]
                        assert len(idx)>0, "Could not find R500 for "+d_file
                        Sun_R500 = Suncat['f10'][idx] * (0.73/h_scale) ## R500 in kpc
                        Sun_z = Suncat['f3'][idx] ## redshift
                        Sun_Ez = (omega_m*(1.0 + Sun_z)**3 + omega_l)**0.5
                        d_x = 10**dat[:,0] * (0.73/h_scale) / Sun_R500 * (1.0-b)**(1.0/3.0) ## r/r500
                        d_y = dat[:,1] * (h_scale/0.73)**2 / Sun_Ez**2 ## n_e in cm^-3
                        #d_y_errlo = (dat[:,1] - dat[:,2]) * (h_scale/0.73)**2 / Sun_Ez**2 ## 1 sigma
                        #d_y_errhi = (dat[:,3] - dat[:,1]) * (h_scale/0.73)**2 / Sun_Ez**2
                        if scaledbyR:
                            d_y *= d_x**2.0
                        ## plot individual profiles
                        d_plt.plot(d_x[d_x > xmin], d_y[d_x > xmin], c=Suncol, alpha=Sunalpha, lw=Sunlw, zorder=1)
                        ## Interpolate at bin position in logspace, returning np.inf when outside range
                        values = np.interp(np.log10(xbins), np.log10(d_x), np.log10(d_y), left=np.inf, right=np.inf)
                        all_values[np.isfinite(values), d_idx] = 10.0**values[np.isfinite(values)] ## mask out the np.inf to avoid an error, leaving them as zero
                    ## Find the median profile:
                    all_values = np.ma.masked_values(all_values, 0.0) ## masked array - mask out remaining zeros
                    d_med = np.ma.median(all_values, axis=1)
                    all_values_nan = np.ma.filled(all_values, np.nan) ## np.ma has no percentile so fill masked values with nans and use nanpercentile
                    #d_err1 = d_med - np.nanpercentile(all_values_nan, 16.0, axis=1)
                    #d_err2 = np.nanpercentile(all_values_nan, 84.0, axis=1) - d_med
                    if b > 0.0:
                        Sunlabel = "Sun+ 2009 [b = "+str(b)+"]"
                    else:
                        Sunlabel = "Sun+ 2009"
                    #d_plt.errorbar(xbins, d_med, yerr=[d_err1, d_err2], zorder=3, fmt='o', mew=1.2, lw=1.2, ms=6, mec='none', mfc=suncol, color=suncol, ecolor=suncol, label=Sunlabel)
                    d_plt.errorbar(xbins, d_med, c=Sunmedcol, lw=Sunmedlw, dashes=(14,8), zorder=1, label=Sunlabel)

#==============================================================================
#                     ## Old Sun+09 median density data
#                     #rho_crit_Sun = (3.0/8.0 / np.pi / 6.67408e-8 * (0.73 * 100 / 3.086e+19)**2.0) * (0.3*(1+0.032)**3+0.7) ## Critical density at median z=0.032 in g cm^-3
#                     data = np.loadtxt("/data/vault/nh444/ObsData/profiles/Sun2009_density_new.txt") ## log(r/r500), log(n_e/E(z)^2), 1 sigma scatter
#                     hfac = 0.73 / h_scale
#                     
#                     ### Median line with 1-sigma shaded region - it's quite noisy
#                     ## rho / rho_crit
#                     #d_plt.plot(10**data[:,0]*hfac**(2.0/3.0), 10**data[:,1] / hfac**2 * ne2rho / rho_crit_Sun, zorder=3, lw=2, c='#000099', label="Sun et al. (2009)")
#                     #patch = np.column_stack((np.concatenate((10**data[:,0]*hfac**(2.0/3.0), 10**data[::-1, 0]*hfac**(2.0/3.0))), np.concatenate((10**data[:,3], 10**data[::-1,2])) / hfac**2 * ne2rho / rho_crit_Sun))
#                     #d_plt.axes.add_patch(Polygon(patch, alpha=0.2, color='#000099', ec='none', zorder=2))
#                     ## n_e E(z)^-2
#                     #d_plt.plot(10**data[:,0]*hfac**(2.0/3.0), 10**data[:,1] / hfac**2, zorder=3, lw=2, c='#000099', label="Sun et al. (2009)")
#                     #patch = np.column_stack((np.concatenate((10**data[:,0]*hfac**(2.0/3.0), 10**data[::-1, 0]*hfac**(2.0/3.0))), np.concatenate((10**data[:,3], 10**data[::-1,2])) / hfac**2))
#                     #d_plt.axes.add_patch(Polygon(patch, alpha=0.2, color='#000099', ec='none', zorder=2))
# 
#                     ### Points with 1-sigma error bars
#                     SunInd = np.arange(2, len(data[:,0]), step=4) ## spacing between radial bins is 0.02 dex
#                     ## rho / rho_crit
#                     #d_plt.errorbar(10**data[SunInd,0]*hfac**(2.0/3.0), 10**data[SunInd,1] / hfac**2 * ne2rho / rho_crit_Sun, yerr=[(10**data[SunInd,1] - 10**data[SunInd,2]) / hfac**2 * ne2rho / rho_crit_Sun, (10**data[SunInd,3] - 10**data[SunInd,1]) / hfac**2 * ne2rho / rho_crit_Sun],
#                                    #zorder=3, fmt='o', mew=1.2, lw=1.2, ms=6, mec='none', mfc=suncol, color=suncol, ecolor=suncol, label="Sun+ 2009")
#                     ## n_e E(z)^-2
#                     d_plt.errorbar(10**data[SunInd,0]*hfac**(2.0/3.0), 10**data[SunInd,1] / hfac**2, yerr=[(10**data[SunInd,1] - 10**data[SunInd,2]) / hfac**2, (10**data[SunInd,3] - 10**data[SunInd,1]) / hfac**2],
#                                    zorder=3, fmt='o', mew=1.2, lw=1.2, ms=6, mec='none', mfc='0.5', color='0.5', ecolor='0.5', label="Sun+ 2009")
#                     
#==============================================================================
#==============================================================================
#                         ## Density Le Brun et al (<-- Sun et al 2009)
#                         d_obs1 = np.loadtxt(obs_dir+"Sun2009_density.txt")
#                         mask = (d_obs1[:,0] > xmin) & (d_obs1[:,0] < xmax)
#                         d_plt.errorbar(d_obs1[mask,0], d_obs1[mask,1], yerr=d_obs1[mask,2:3],
#                                        zorder=3, fmt='o', mew=1.2, lw=1.2, ms=5, mec='none',
#                                        mfc=suncol, color=suncol, ecolor=suncol,
#                                        label="Sun et al. (2009)")
#==============================================================================
#==============================================================================
#                         ## ACCEPT profiles for Sun 2009 groups
#                         names = ["ABELL_0160","ABELL_0262","ABELL_0744",
#                                  "ABELL_1991","ABELL_2029","ABELL_2462",
#                                  "ABELL_2717","ABELL_3581","ESO_3060170",
#                                  "ESO_5520200","MKW_04","RBS_0461","RX_J1022.1+3830"]
#                         names = ["ABELL_0160","ABELL_3581","ESO_5520200","MKW_04","RX_J1022.1+3830"]
#                         #for suni, name in enumerate(names):
#                             #d_obs2 = np.genfromtxt(obs_dir+"Sun_ACCEPT_profiles/"+name+"_derived_profiles.dat")
#                             #d_plt.plot(d_obs2[:,3], d_obs2[:,10], c='0.5')
#==============================================================================
                elif compare=="TNG":
                    TNG = np.loadtxt(obs_dir+"IllustrisTNG/TNG_density.csv", delimiter=",")
                    d_plt.plot(TNG[:,0], TNG[:,1], c='orange', lw=2.5, label="IllustrisTNG")

                ## Vikhlinin+2006
                #for k in range(1,5):
                    #d_obs2 = np.loadtxt(obs_dir+"Vikh2006_density_profile_"+str(k)+".txt")
                    #if k==1: d_plt.plot(d_obs2[:,0], d_obs2[:,1], color='0.7', lw=1, label="Vikhlinin et al. (2006) ($T < 2.5$ keV)")
                    #else: d_plt.plot(d_obs2[:,0], d_obs2[:,1], color='0.7', lw=1)
                ## ACCEPT cluster profiles
                #for ACCEPTidx, ACCEPTname in enumerate(ACCEPTnames):
                    #d_obs3 = np.loadtxt(obs_dir+"ACCEPT_profiles/"+ACCEPTname+"_derived_profiles.dat")
                    #d_plt.plot(d_obs3[:,2]/ACCEPTr500[ACCEPTidx], d_obs3[:,6]/(rho_crit*0.7*0.7), lw=1, label=ACCEPTname.replace("_","\_")+" ($T_{X}= $ "+str(ACCEPT_temp[ACCEPTidx])+" keV)")
                    
                ## Group 0 density profile from C code output
                #data = np.loadtxt("/data/curie4/nh444/project1/L-T_relations/L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_025/group_0/"+"seperate_profiles.txt")
                #gasrho = data[:,6] * 1e10 * 1.9884430e33 * h_scale*h_scale / 3.085678e21**3 ## g / cm^3
                #d_plt.plot(data[:,2]/500.63605, gasrho/rho_crit, 'o', mfc='black')

            if plot_only=='all' or plot_only=='temperature':
                ## Temperature
                if compare=="Sun":
                    xbins = np.logspace(np.log10(max(0.01, xmin)), np.log10(min(xmax, 1.0)), num=(np.log10(xmax) - np.log10(xmin))/0.08) ## 0.08 dex spacing to be consistent with Sun09 density profile (0.02 dex spacing but plotting every 4 bins)
                    from glob import glob
                    T_files = glob(obs_dir+"Sun_2009_temp_profiles/T_3D/*_T")
                    Suncat = np.genfromtxt(obs_dir+"Sun2009.csv", delimiter=",", dtype=None) ## dtype=None guesses data type (can be slow)
                    import os
                    all_values = np.zeros((len(xbins), len(T_files)))
                    for T_idx, T_file in enumerate(T_files):
                        dat = np.loadtxt(T_file) ## log10(r/kpc)   T_3d (keV)   lower_boundary  upper_boundary
                        ## Find r500 from catalogue of sun+2009 objects
                        idx = np.where(Suncat['f2'] == os.path.basename(T_file).replace("_T",""))[0]
                        assert len(idx)>0, "Could not find R500 for file "+T_file
                        Sun_R500 = Suncat['f10'][idx] * (0.73/h_scale) ## R500 in kpc
                        T_x = 10**dat[:,0] * (0.73/h_scale) / Sun_R500 * (1.0-b)**(1.0/3.0) ## r/r500
                        T_y = dat[:,1] ## T_3D in keV
                        T_y_errlo = dat[:,1] - dat[:,2]
                        T_y_errhi = dat[:,3] - dat[:,1]
                        if scaled: ## scale to T/T500
                            Sun_T500 = G * mu * mp * Suncat['f19'][idx]*1e13*1.989e33 / (2.0 * Suncat['f10'][idx] * 3.085678e21) / 1.6021772e-9 / (1.0-b)**(2.0/3.0)
                            T_y /= Sun_T500
                            T_y_errlo /= Sun_T500
                            T_y_errhi /= Sun_T500
                        #T_plt.errorbar(T_x, T_y, yerr=[T_y_errlo, T_y_errhi], c='grey', alpha=0.4, ls='none', zorder=1)
                        T_plt.plot(T_x, T_y, c=Suncol, alpha=Sunalpha, lw=Sunlw, zorder=1)
                        ## Interpolate at bin position in logspace, returning np.inf when outside range
                        values = np.interp(np.log10(xbins), np.log10(T_x), np.log10(T_y), left=np.inf, right=np.inf)
                        all_values[np.isfinite(values), T_idx] = 10.0**values[np.isfinite(values)] ## mask out the np.inf to avoid an error, leaving them as zero
                    ## Get median profile:
                    all_values = np.ma.masked_values(all_values, 0.0) ## masked array - mask out remaining zeros
                    T_med = np.ma.median(all_values, axis=1)
                    all_values_nan = np.ma.filled(all_values, np.nan) ## np.ma has no percentile so fill masked values with nans and use nanpercentile
                    T_err1 = T_med - np.nanpercentile(all_values_nan, 16.0, axis=1)
                    T_err2 = np.nanpercentile(all_values_nan, 84.0, axis=1) - T_med
                    if b > 0.0:
                        Sunlabel = "Sun+ 2009 [b = "+str(b)+"]"
                    else:
                        Sunlabel = "Sun+ 2009"
                    #T_plt.errorbar(xbins, T_med, yerr=[T_err1, T_err2], zorder=3, fmt='o', mew=1.2, lw=1.2, ms=6, mec='none', mfc=suncol, color=suncol, ecolor=suncol, label=Sunlabel)
                    T_plt.errorbar(xbins, T_med, c=Sunmedcol, lw=Sunmedlw, dashes=(14,8), zorder=1, label=Sunlabel)
                    
                    ## scaled temperature profile from C-EAGLE paper Barnes 2017
                    #data = np.loadtxt("/data/vault/nh444/ObsData/profiles/Arnaud2010_temp_prof_Barnes2017.csv", delimiter=",")
                    #T_plt.errorbar(data[:,0],data[:,1], yerr=[data[:,2], data[:,3]], marker='s', c='0.5', mec='none', ls='none', label="Arnaud+ 2010")
                    
                elif compare=="Sun2D": ## projected 2D temperature profiles
                    xbins = np.logspace(np.log10(xmin), np.log10(xmax), num=(np.log10(xmax) - np.log10(xmin))/0.08) ## 0.08 dex spacing to be consistent with Sun09 density profile (0.02 dex spacing but plotting every 4 bins)
                    from glob import glob
                    T_files = glob(obs_dir+"Sun_2009_temp_profiles/T_2D/*.dat")
                    Suncat = np.genfromtxt(obs_dir+"Sun2009.csv", delimiter=",", dtype=None) ## dtype=None guesses data type (can be slow)
                    import os
                    all_values = np.zeros((len(xbins), len(T_files)))
                    for T_idx, T_file in enumerate(T_files):
                        dat = np.loadtxt(T_file) ## r/r500 and T/T2500 with errors on each
                        T_x = dat[:,0] ## r/r500 - no h factor
                        ### Convert T/T2500 to T in keV 
                        ## Get T2500 (keV) from table:
                        idx = np.where(Suncat['f2'] == os.path.basename(T_file).replace("_T_lin.dat",""))[0]
                        assert len(idx)>0, "Could not find T_2500 for file "+T_file
                        Sun_T2500 = Suncat['f7'][idx]
                        T_y = dat[:,3] * Sun_T2500 ## multiply T/T2500 by T2500 to get T in keV
                        T_y_errlo = dat[:,5] * Sun_T2500
                        T_y_errhi = dat[:,4] * Sun_T2500
                        if scaled: ## T/T500
                            Sun_T500 = G * mu * mp * Suncat['f19'][idx]*1e13*1.989e33 / (2.0 * Suncat['f10'][idx] * 3.085678e21) / 1.6021772e-9
                            T_y /= Sun_T500
                            T_y_errlo /= Sun_T500
                            T_y_errhi /= Sun_T500
                        T_plt.errorbar(T_x, T_y, xerr=[dat[:,2], dat[:,1]], yerr=[T_y_errlo, T_y_errhi], c='grey', alpha=0.4, ls='none', zorder=1) 
                        ## Interpolate at bin position in logspace, returning np.inf when outside range
                        values = np.interp(np.log10(xbins), np.log10(T_x), np.log10(T_y), left=np.inf, right=np.inf)
                        all_values[np.isfinite(values), T_idx] = 10.0**values[np.isfinite(values)] ## mask out the np.inf to avoid an error, leaving them as zero
                    all_values = np.ma.masked_values(all_values, 0.0) ## masked array - mask out remaining zeros
                    T_med = np.ma.median(all_values, axis=1)
                    all_values_nan = np.ma.filled(all_values, np.nan) ## np.ma has no percentile so fill masked values with nans and use nanpercentile
                    T_err1 = T_med - np.nanpercentile(all_values_nan, 16.0, axis=1)
                    T_err2 = np.nanpercentile(all_values_nan, 84.0, axis=1) - T_med
                    T_plt.errorbar(xbins, T_med, yerr=[T_err1, T_err2], zorder=3, fmt='o', mew=1.2, lw=1.2, ms=6, mec='none', mfc=Suncol, color=Suncol, ecolor=Suncol, label="Sun+ 2009")
                elif compare=="Rasmussen":
                    ## Temperature Rasmussen et al. 2007
                    T_obs = np.loadtxt(obs_dir+"Rasmussen_temp-prof.txt")
                    T_plt.errorbar(T_obs[:,0], T_obs[:,1], yerr=T_obs[:,4:5], fmt='D',
                                               ms=3, mfc='0.6', mec='0.6', ecolor='0.6',
                                               zorder=1, label="Rasmussen et al. (2007)")
                elif compare=="Arnaud":
                    T_obs1 = np.loadtxt(obs_dir+"Arnaud2010_temp_profile.txt")
                    T_plt.plot(T_obs1[:,0], T_obs1[:,1], c='0.8', lw=3, zorder=1)
                    T_obs2 = np.loadtxt(obs_dir+"Arnaud2010_temp_profile_range.txt")
                    poly_T_obs2 = Polygon(T_obs2, color='0.9', ec='none', label="Arnaud et al. (2010)")
                    T_plt.axes.add_patch(poly_T_obs2)
                elif compare=="TNG":
                    TNG = np.loadtxt(obs_dir+"IllustrisTNG/TNG_temperature.csv", delimiter=",")
                    T_plt.plot(TNG[:,0], TNG[:,1], c='orange', lw=2.5, label="IllustrisTNG")
                    dat = np.loadtxt(obs_dir+"IllustrisTNG/TNG_temperature_sigma.csv", delimiter=",")
                    poly = Polygon(dat, color='orange', alpha=0.2, ec='none')
                    T_plt.axes.add_patch(poly)
                elif compare=="REXCESS":
#==============================================================================
#                     ### REXCESS profiles -- median
#                     import profiles.REXCESS as REXCESS
#                     xbins = np.logspace(np.log10(xmin), np.log10(2.0), num=1000)
#                     profiles = REXCESS.get_temp_prof(xbins, scaled=False, M500min=0.0, M500max=REXM500max, non_CC=True, h=h_scale)
#                     #for i in range(len(profiles[:,0])):
#                         #T_plt.plot(xbins, profiles[i,:], c='k', alpha=0.2)
#                     b = 0.0
#                     profiles *= (1-b)
#                     med, patch = REXCESS.get_median_range(xbins, profiles)
#                     T_plt.plot(xbins, med, c='k', lw=2, label="Arnaud+ 2010")#$b="+str(b)+"$"
#                     T_plt.axes.add_patch(Polygon(patch[patch[:,1]>0], alpha=0.2, color='k', ec='none', zorder=2))
#                     ## binned points
#                     #x, med, err1, err2 = REXCESS.get_median_bins(xbins, profiles, rmin=xmin, rmax=2.0)
#                     #T_plt.errorbar(x, med, yerr=[err1,err2], marker='s', c='k', ls='none', label="Arnaud+ 2010")
#                     
#==============================================================================
                    ### REXCESS temperature profiles
                    xlims = T_plt.axes.get_xlim()
                    xbins = np.logspace(np.log10(xlims[0]/2.0), np.log10(xlims[1]*2.0), num=1000)
                    import profiles.REXCESS as REXCESS
                    profiles, cls = REXCESS.get_temp_prof(xbins, bias=REXbias, T500_scaling=False, ss_scaling=scaled, mu=mu, h=h_scale, M500min=0.0, M500max=REXM500max)
                    ## Individual REXCESS profiles:
                    for i in range(len(profiles[:,0])):
                        T_plt.plot(xbins * REXbias**(1.0/3.0), profiles[i,:], c=REXcol[cls[i]], ls=REXls[cls[i]], alpha=REXalpha, lw=REXlw, zorder=1, label=REXlabels[cls[i]])
                        REXlabels[cls[i]] = ""
                    ## Median in bins:
                    x, med, err1, err2 = REXCESS.get_median_bins(xbins * REXbias**(1.0/3.0), profiles, dlogr=0.1, rmin=xlims[0]*0.1, rmax=xlims[1]*10)
                    T_plt.errorbar(x, med, c=REXmedcol, lw=REXmedlw, dashes=(14,8), alpha=1, zorder=1, label=REXmedlabel)
                    
                    ## scaled temperature profile from C-EAGLE paper Barnes 2017
                    #data = np.loadtxt("/data/vault/nh444/ObsData/profiles/Arnaud2010_temp_prof_Barnes2017.csv", delimiter=",")
                    #T_plt.errorbar(data[:,0],data[:,1], yerr=[data[:,2], data[:,3]], marker='s', c='0.5', mec='none', ls='none', label="Arnaud+ 2010")

                                        
                #elif not compare=="none": ## compare=="Sun":
#==============================================================================
#                         ## Temperature Sun et al. 2009
#                         if plot_T2500 == True:
#                             norm = 1.0
#                         else:
#                             norm = 0.89#0.54 ## normalisation T_500 / T_2500 according to Sun et al. best fit
#                         T_obs1 = np.loadtxt(obs_dir+"temp_Sun_Etal_dist.txt")
#                         T_obs1[:,1] /= norm
#                         poly_T_obs1 = Polygon(T_obs1, color='0.9', ec='none', label="Sun et al. (2009)")
#                         T_plt.axes.add_patch(poly_T_obs1)
#==============================================================================
                    
                    #for name in ["A262", "A2550", "A744", "MKW4", "A3581"]:
#==============================================================================
#                         for name in ["A262", "A2550", "A744", "MKW4", "A3581", "UGC5088", "HCG51", "NGC4325", "NGC1132", "NGC5129", "NGC533"]:
#                             T_obs2 = np.loadtxt(obs_dir+"Sun_temp_profiles/"+name+"_temp_prof.txt")
#                             T_plt.errorbar(T_obs2[:,1], T_obs2[:,3], c='0.5', lw=2)
#==============================================================================
                        
#==============================================================================
#                         for name in ["RXCJ0003", "RXCJ0020", "RXCJ0547", "RXCJ0605", "RXCJ1044", "RXCJ1516.5", "RXCJ1302", "RXCJ1311"]:
#                             T_obs1 = np.loadtxt("/data/vault/nh444/ObsData/profiles/REXCESS_profiles/temp_profiles/temp_profile_"+name+".txt")
#                             T_plt.plot(T_obs1[:,3], T_obs1[:,5], lw=2, c='black', alpha=0.3)
#==============================================================================
                    
                    ## scaled temperature profiles from MACSIS paper Barnes 2016
                    #data = np.loadtxt("/data/vault/nh444/ObsData/profiles/REXCESS_profiles/Arnaud_2010_temp_profile_from_MACSIS.txt")
                    #T_plt.errorbar(data[:,0],data[:,1], yerr=[data[:,2], data[:,3]], marker='s', c='black', ls='none', label="REXCESS")
                    #data = np.loadtxt("/data/vault/nh444/ObsData/profiles/Giles2015_temp_prof_from_Barnes2015.txt")
                    #T_plt.errorbar(data[:,0],data[:,1], yerr=[data[:,2], data[:,3]], marker='<', c='black', ls='none', label="Giles+2015")
                    
                    
                    ## 3D temperature profile fits from Giles et al. 2015
#==============================================================================
#                         props = np.genfromtxt("/data/vault/nh444/ObsData/profiles/Giles2015_props.csv", delimiter=",")
#                         from vikhprof import temp_profile
#                         dat = np.genfromtxt("/data/vault/nh444/ObsData/profiles/Giles2015_temp_fits.csv", delimiter=",")
#                         r = np.logspace(-3, 1, num=100)
#                         Gilprof = []
#                         for i in range(len(dat[:,1])):
#                             T_plt.plot(r, temp_profile(r*props[i,1]*1000.0, dat[i,4], dat[i,6], dat[i,7], dat[i,8], dat[i,2], dat[i,5], dat[i,3], dat[i,1])/(1.392e-11*props[i,9]*1e14/(props[i,1]*1000.0)), c='blue', alpha=0.2)
#                             Gilprof.append(temp_profile(r*props[i,1]*1000.0, dat[i,4], dat[i,6], dat[i,7], dat[i,8], dat[i,2], dat[i,5], dat[i,3], dat[i,1])/(1.392e-11*props[i,9]*1e14/(props[i,1]*1000.0)))
#                         Gilprof = np.asarray(Gilprof)
#                         T_plt.plot(r, np.median(Gilprof, axis=0), c='blue', lw=2.5)
#                         T_plt.set_ylim(0.0, 2.0)
#==============================================================================
                    
                    ## Compare to temperature profile of c256 and c384 output by C code
                    #data = np.loadtxt("/data/curie4/nh444/project1/L-T_relations/LDRIntRadioEffBHVel/c256_025/group_0/temperature_entropy_profiles.txt")
                    #T_plt.errorbar(data[:,2]/457.3069, data[:,4]/(1.392e-11*5.558e13/457.3069), lw=2, c=cmap(colour_idx[0]))#cmap(colour_idx[0])
                    #data = np.loadtxt("/data/curie4/nh444/project1/L-T_relations/LDRIntRadioEffBHVel/c384_025/group_0/temperature_entropy_profiles.txt")
                    #T_plt.errorbar(data[:,2]/828.772, data[:,4]/(1.392e-11*3.30816e14/828.772), lw=2, c=cmap(colour_idx[2]))
#==============================================================================
#                         ## Same but for Gadget runs
#                         data = np.loadtxt("/data/curie4/nh444/project1/L-T_relations/gadget/m_33617_h_146_0_z2_csfbh_062/group_0/temperature_entropy_profiles.txt")
#                         T_plt.errorbar(data[:,2]/731.7972, data[:,4]/(1.392e-11*21964.6*1e10/731.797), lw=2, c='black', label=r"csfbh (M$_{500}\sim2.2\times10^{14}$)")
#                         data = np.loadtxt("/data/curie4/nh444/project1/L-T_relations/gadget/m_33617_h_146_0_z2_csf_062/group_0/temperature_entropy_profiles.txt")
#                         T_plt.errorbar(data[:,2]/746.049, data[:,4]/(1.392e-11*23253.2*1e10/746.049), lw=2, c='black', ls='dashed', label=r"csf (M$_{500}\sim2.3\times10^{14}$)")
#                         data = np.loadtxt("/data/curie4/nh444/project1/L-T_relations/gadget/m_14999_h_355_1278_z3_csfbh_062/group_0/temperature_entropy_profiles.txt")
#                         T_plt.errorbar(data[:,2]/554.479, data[:,4]/(1.392e-11*9554.22*1e10/554.479), lw=2, c='blue', label=r"csfbh (M$_{500}\sim0.95\times10^{14}$)")
#                         data = np.loadtxt("/data/curie4/nh444/project1/L-T_relations/gadget/m_14999_h_355_1278_z3_csf_062/group_0/temperature_entropy_profiles.txt")
#                         T_plt.errorbar(data[:,2]/568.164, data[:,4]/(1.392e-11*10279.3*1e10/568.164), lw=2, c='blue', ls='dashed', label=r"csf (M$_{500}\sim1.0\times10^{14}$)")
#                         data = np.loadtxt("/data/curie4/nh444/project1/L-T_relations/gadget/m_22498_h_197_0_z2_csfbh_062/group_0/temperature_entropy_profiles.txt")
#                         T_plt.errorbar(data[:,2]/634.314, data[:,4]/(1.392e-11*14287.8*1e10/634.314), lw=2, c='green', label=r"csfbh (M$_{500}\sim1.4\times10^{14}$)")
#                         data = np.loadtxt("/data/curie4/nh444/project1/L-T_relations/gadget/m_22498_h_197_0_z2_csf_062/group_0/temperature_entropy_profiles.txt")
#                         T_plt.errorbar(data[:,2]/653.885, data[:,4]/(1.392e-11*15651.7*1e10/653.885), lw=2, c='green', ls='dashed', label=r"csf (M$_{500}\sim1.6\times10^{14}$)")
#                         T_plt.set_ylim(0.0, 2.5)
#==============================================================================

                    #T_obs3 = np.loadtxt("/home/nh444/vault/ObsData/profiles/temp_Sun_Etal_1sig.txt")
                    #T_obs3[:,1] /= norm
                    #poly_T_obs3 = Polygon(T_obs3, color='0.8', ec='none', label="Sun et al. (2009) (1-$\sigma$ range)")
                    #T_plt.axes.add_patch(poly_T_obs3)
                    
                    #T_obs2 = np.loadtxt("/home/nh444/vault/ObsData/profiles/temp_Sun_Etal.txt")
                    #T_obs2[:,1:2] /= norm
                    #T_plt.errorbar(T_obs2[:,0], T_obs2[:,1], yerr=T_obs2[:,2], fmt='D', ms=3, mfc='black', ecolor='black')
                

            if plot_only=='all' or plot_only=='entropy':
                if compare=="Sun":
                    xbins = np.logspace(np.log10(max(0.01, xmin)), np.log10(min(xmax, 1.0)), num=(np.log10(xmax) - np.log10(xmin))/0.08)
                    Suncat = np.genfromtxt(obs_dir+"Sun2009.csv", delimiter=",", dtype=None) ## dtype=None guesses data type (can be slow)
                    import os
                    all_values = np.zeros((len(xbins), len(Suncat['f2'])))
                    for f_idx, name in enumerate(Suncat['f2']):
                        d_dat = np.loadtxt(obs_dir+"Sun_2009_density_profiles/"+name+"_den") ## log10(r/kpc)   ne  lower_boundary  upper_boundary
                        T_dat = np.loadtxt(obs_dir+"Sun_2009_temp_profiles/T_3D/"+name+"_T") ## log10(r/kpc)   T_3d (keV)   lower_boundary  upper_boundary
                        #T_dat = np.loadtxt(obs_dir+"Sun_2009_temp_profiles/T_2D/"+name+"_T_lin.dat") ## log10(r/kpc)   T_3d (keV)   lower_boundary  upper_boundary
                        Sun_R500 = Suncat['f10'][f_idx] * (0.73/h_scale) ## R500 in kpc
                        Sun_z = Suncat['f3'][f_idx] ## redshift
                        d_x = 10**d_dat[:,0] * (0.73/h_scale) / Sun_R500 * (1.0-b)**(1.0/3.0) ## r/r500
                        T_x = 10**T_dat[:,0] * (0.73/h_scale) / Sun_R500 * (1.0-b)**(1.0/3.0) ## for T_3D r/r500
                        #T_x = T_dat[:,0] * (0.73/h_scale) * (1.0-b)**(1.0/3.0) ## for T_2D r/r500
                        d_y = d_dat[:,1] * (h_scale/0.73)**2 ## n_e in cm^-3
                        T_y = T_dat[:,1] ## T_3D in keV
                        #Sun_T2500 = Suncat['f7'][f_idx] ## for T_2D
                        #T_y = T_dat[:,3] * Sun_T2500 ## for T_2D: multiply T/T2500 by T2500 to get T in keV
                        ## Interpolate the density at each temperature datapoint
                        ## Interpolate in log space, returning np.inf when outside range
                        d_y2 = 10.0**np.interp(np.log10(T_x), np.log10(d_x), np.log10(d_y), left=np.inf, right=np.inf)
                        S_y = T_y / d_y2**(2.0/3.0) ## keV cm^2
                        S_y[np.isinf(S_y)] = 0.0 ## replace infs with zeroes
                        S_y = np.ma.masked_values(S_y, 0.0) ## mask out infs
                        if scaled: ## scale to P/P500
                            Sun_T500 = G * mu * mp * Suncat['f19'][f_idx]*1e13*1.989e33 / (2.0 * Suncat['f10'][f_idx] * 3.085678e21) / 1.6021772e-9 / (1.0-b)**(2.0/3.0)
                            Sun_ne500 = 500.0 * fB * (3.0 * (h_scale * 100 / 3.086e+19)**2 / (8.0 * np.pi * G)) / (mu_e * mp)
                            Sun_K500 = Sun_T500 / Sun_ne500**(2.0/3.0)
                            S_y /= Sun_K500
                        ## plot individual profiles
                        S_plt.plot(T_x[T_x > xmin], S_y[T_x > xmin], c=Suncol, alpha=Sunalpha, lw=Sunlw, zorder=1)
                        ## Interpolate at bin position in logspace, returning np.inf when outside range
                        values = np.interp(np.log10(xbins), np.log10(T_x), np.log10(S_y), left=np.inf, right=np.inf)
                        all_values[np.isfinite(values), f_idx] = 10.0**values[np.isfinite(values)] ## mask out the np.inf to avoid an error, leaving them as zero
                    all_values = np.ma.masked_values(all_values, 0.0) ## masked array - mask out remaining zeros
                    S_med = np.ma.median(all_values, axis=1)
                    S_count = np.ma.count(all_values, axis=1)
                    S_mask = (S_count > 10) ## don't plot bins with fewer than X datapoints
                    all_values_nan = np.ma.filled(all_values, np.nan) ## np.ma has no percentile so fill masked values with nans and use nanpercentile
                    #S_err1 = S_med - np.nanpercentile(all_values_nan, 16.0, axis=1)
                    #S_err2 = np.nanpercentile(all_values_nan, 84.0, axis=1) - S_med
                    if b > 0.0:
                        Sunlabel = "Sun+ 2009 [b = "+str(b)+"]"
                    else:
                        Sunlabel = "Sun+ 2009"
                    #S_plt.errorbar(xbins, S_med, yerr=[S_err1, S_err2], zorder=3, fmt='o', mew=1.2, lw=1.2, ms=6, mec='none', mfc=suncol, color=suncol, ecolor=suncol, label=Sunlabel)
                    #S_plt.errorbar(xbins, S_med, yerr=[S_err1, S_err2], zorder=3, fmt='o', mew=1.2, lw=1.2, ms=6, mec='none', mfc='r', color='r', ecolor='r', alpha=0.4, label=Sunlabel)
                    S_plt.errorbar(xbins[S_mask], S_med[S_mask], c=Sunmedcol, lw=Sunmedlw, dashes=(14,8), zorder=1, label=Sunlabel)
                elif compare=="Sun2D":
                    ## Entropy Sun et al. 2009
#==============================================================================
#                         if blkwht: alpha=0.4
#                         else: alpha=None
#                         S_obs1 = np.loadtxt(obs_dir+"Sun_etal_entropy.txt")
#                         poly_S_obs1 = Polygon(S_obs1, color='0.9', ec='none', alpha=alpha, label="Sun+ 2009")
#                         S_plt.axes.add_patch(poly_S_obs1)
#==============================================================================
                    xbins = np.logspace(np.log10(xmin), np.log10(xmax), num=(np.log10(xmax) - np.log10(xmin))/0.08) ## 0.08 dex spacing to be consistent with Sun09 density profile (0.02 dex spacing but plotting every 4 bins)
                    from glob import glob
                    K_files = glob(obs_dir+"Sun_2009_entropy_profiles/*.dat")
                    all_values = np.zeros((len(xbins), len(K_files)))
                    for K_idx, K_file in enumerate(K_files):
                        dat = np.loadtxt(K_file)
                        K_x = 10**dat[:,0] * (0.73/h_scale)**(2.0/3.0) ## r/r500 - h^2/3 factor since r (h^-1) and r500 (h^-1/3)
                        #K_y = 10**dat[:,1] * (h_scale / 0.73) ## K/K500 has h^+1 units since K500 is h^-4/3 and K is h^-1/3
                        ## rescale to self-similar K500 definition used above
                        ## first get K / M500^(2/3) / E(z)^2/3 for M500 in g (don't know M500 but doesn't matter coz our K500 is also proportional to it)
                        K = 10**dat[:,1] * 342 / (1e14 * 1.989e33)**(2.0/3.0) * (h_scale / 0.73)
                        ## this is the self-similar entropy I use to scale the simulated profiles without M500 and E(z):
                        K500 = mu * mp**(5.0/3.0) * (mu_e * 2.0 * np.pi / 3.0 / fB)**(2.0/3.0) / (500.0 * (h_scale * 100 / 3.086e+19)**2 / G**4)**(1.0/3.0) / 1.6021772e-9 ## / 1.6021772e-9 is to get temp into keV
                        #print "K500 =", K500
                        #print "K500 =", K500 * (1e14 * 1.989e33)**(2.0/3.0) ## for a 1e14 Msun cluster
                        K_y = K / K500
                        #S_plt.errorbar(K_x, K_y, lw=2, c='grey', alpha=0.4, zorder=1) 
                        S_plt.plot(K_x, K_y, c='0.8', alpha=0.5, lw=2, zorder=1)
                        ## Interpolate entropy at bin position in logspace, returning np.inf when outside range
                        values = np.interp(np.log10(xbins), np.log10(K_x), np.log10(K_y), left=np.inf, right=np.inf)
                        all_values[np.isfinite(values), K_idx] = 10.0**values[np.isfinite(values)] ## mask out the np.inf to avoid an error, leaving them as zero
                    all_values = np.ma.masked_values(all_values, 0.0) ## masked array - mask out remaining zeros
                    K_med = np.ma.median(all_values, axis=1)
                    K_count = np.ma.count(all_values, axis=1)
                    K_mask = (K_count > 10) ## don't plot bins with fewer than X datapoints
                    all_values_nan = np.ma.filled(all_values, np.nan) ## np.ma has no percentile so fill masked values with nans and use nanpercentile
                    #K_err1 = K_med - np.nanpercentile(all_values_nan, 16.0, axis=1)
                    #K_err2 = np.nanpercentile(all_values_nan, 84.0, axis=1) - K_med
                    #S_plt.errorbar(xbins[K_mask], K_med[K_mask], yerr=[K_err1[K_mask], K_err2[K_mask]], zorder=3, fmt='o', mew=1.2, lw=1.2, ms=6, mec='none', mfc=suncol, color=suncol, ecolor=suncol, label="Sun+ 2009")
                    S_plt.errorbar(xbins[K_mask], K_med[K_mask], c='0.5', lw=3, dashes=(14,8), zorder=1, label="Sun+ 2009")
                    
                    ## ACCEPT cluster profiles
                    #for ACCEPTidx, ACCEPTname in enumerate(ACCEPTnames):
                        #S_obs2 = np.loadtxt(obs_dir+"ACCEPT_profiles/"+ACCEPTname+"_derived_profiles.dat")
                        #ACCEPTEz = pow(omega_m * pow(1.0+ACCEPTz[ACCEPTidx], 3.0) + omega_l, 0.5)
                        #SvirACC = 342.0 * pow(ACCEPTr500[ACCEPTidx]/690.0, 2.0) / pow(ACCEPTEz, 2.0/3.0) / pow(0.7/0.73, 4.0/3.0) ## Sun et al 2009 with M500 --> r500 scaling
                        #S_plt.plot(S_obs2[:,2]/ACCEPTr500[ACCEPTidx], S_obs2[:,5]/SvirACC, lw=1, label=ACCEPTname.replace("_","\_")+" ($T_{X}= $ "+str(ACCEPT_temp[ACCEPTidx])+" keV)")
                    ## Entropy - Mean points
                    #S_obs2 = np.loadtxt("/home/nh444/vault/ObsData/profiles/entropy_LeBrun.txt")
                    #S_plt.errorbar(S_obs2[:,0], S_obs2[:,1], yerr=S_obs2[:,2:3], fmt='D', ms=4, mfc='black', ecolor='black')
                    ## Entropy - resample 1.4 < M500 (1e13 Msun) < 4.0
                    #S_obs3 = np.loadtxt("/data/vault/nh444/ObsData/profiles/Sun2009_entropy_5lowmass.txt") ## Resampled Sun 2009
                    #for k in range(0, len(S_obs3[0,:])-1, 2):
                        #S_plt.plot(S_obs3[:,k], S_obs3[:,k+1], 'black')
                    ## Entropy Johnson et al. 2009
                    #S_obs4 = np.loadtxt("/data/vault/nh444/ObsData/profiles/Johnson_entropy_resample.txt") ## Resampled Johnson
                    #johnson = S_plt.plot(S_obs4[:,0], S_obs4[:,1], 'D-', color='green', lw=0.5, ms=4, mec='none', label="Johnson et al. (2009) 1.1 $\leq$ M$_{500}$ ($10^{13}$ $M_{\odot}$) $\leq$ 3.4")
                    #johnson = S_plt.plot(S_obs4[:,0], S_obs4[:,2:], 'D-', color='green', lw=0.5, ms=4, mec='none')
                elif compare=="TNG":
                    TNG = np.loadtxt(obs_dir+"IllustrisTNG/TNG_entropy.csv", delimiter=",")
                    S_plt.plot(TNG[:,0], TNG[:,1], c='orange', lw=2.5, label="IllustrisTNG")
                elif compare=="REXCESS":
#==============================================================================
#                         ### REXCESS profiles -- median
#                         import profiles.REXCESS as REXCESS
#                         xbins = np.logspace(np.log10(xmin), np.log10(2.0), num=1000)
#                         profiles = REXCESS.get_entropy_prof(xbins, scaled=True, M500min=0.0, M500max=REXM500max, h=h_scale)
#                         for i in range(len(profiles[:,0])):
#                             S_plt.plot(xbins, profiles[i,:], c='k', lw=2, alpha=0.25)
#                         med, patch = REXCESS.get_median_range(xbins, profiles)
#                         S_plt.plot(xbins, med, c='k', lw=2, label="Pratt+ 2010")#$b="+str(b)+"$"
#                         S_plt.axes.add_patch(Polygon(patch[patch[:,1]>0], alpha=0.2, color='k', ec='none', zorder=2))
#==============================================================================
                    
                    ### REXCESS profiles -- individual and colour coded
#==============================================================================
#                     import profiles.REXCESS as REXCESS
#                     Sdat = np.genfromtxt("/data/vault/nh444/ObsData/profiles/REXCESS_profiles/Pratt2010_Entropy_param.csv", delimiter=",")
#                     R500 = Sdat[:,1] * 1000.0 ## kpc
#                     K0 = Sdat[:,3]
#                     K100 = Sdat[:,6]
#                     Salpha = Sdat[:,9]
#                     alpha = 0.6
#                     c1, c2, c3 = '0.8', '0.8', '0.8' ## CC, disturbed, relaxed
#                     REXmedlabel = "REXCESS"#"Pratt+ 2010"
#                     rexcess_ids = range(len(CC))
#                     xbins = np.logspace(np.log10(xmin), np.log10(xmax), num=1000)
#                     profiles = []
#                     for rexid in rexcess_ids:
#                         K = REXCESS.entropy_prof_model(xbins * R500[rexid], K0[rexid], K100[rexid], Salpha[rexid], h=h_scale)
#                         profiles.append(K)
#                         if rex_mass[rexid]*1e14 < REXM500max:## mass limit
#                             if CC[rexid] == 1:
#                                 S_plt.plot(xbins, K, c=c1, alpha=alpha, ls='dotted', lw=2, dash_capstyle='round', zorder=1)
#                             elif disturbed[rexid] == 1:
#                                 S_plt.plot(xbins, K, c=c2, alpha=alpha, ls='dashed', lw=2, dash_capstyle='round', zorder=1)
#                             else:
#                                 S_plt.plot(xbins, K, c=c3, alpha=alpha, lw=2, zorder=1, label=REXmedlabel)
#                                 REXmedlabel = None
#==============================================================================
                    xlims = S_plt.axes.get_xlim()
                    xbins = np.logspace(np.log10(xlims[0]), np.log10(xlims[1]), num=1000)
                    import profiles.REXCESS as REXCESS
                    profiles, cls = REXCESS.get_entropy_prof(xbins, scaled=scaled, fB=fB, h=h_scale, M500min=0.0, M500max=REXM500max)
                    ## Individual REXCESS profiles:
                    for i in range(len(profiles[:,0])):
                        S_plt.loglog(xbins, profiles[i,:], c=REXcol[cls[i]], ls=REXls[cls[i]], lw=REXlw, alpha=REXalpha, dash_capstyle='round', zorder=1, label=REXlabels[cls[i]])
                        REXlabels[cls[i]] = ""
                    ## Median in bins:
                    x, med, err1, err2 = REXCESS.get_median_bins(xbins, profiles, dlogr=0.1, rmin=xlims[0]*0.1, rmax=xlims[1]*10)
                    S_plt.errorbar(x, med, c=REXmedcol, lw=REXmedlw, dashes=(14,8), zorder=1, label=REXmedlabel)
                    ## need to reset the axis labels because they're overwritten for some reason
                    if for_paper: S_plt.xaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))
                    if for_paper and scaled: S_plt.yaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))
                    
                if scaled: ## Voit 2005
                    ## Baseline relation (self-similar scaling, no non-gravitational processes) from Voit et al 2005
                    ## The original relation is K/K200 = 1.32 (r/R200)^1.1 but I rescale it to K/K500 = X (r/R500)^1.1
                    ## where the normalisation X assumes some ratio R500/R200 and rescales it to my baryon fraction
                    ## Here I use the form given in Sun+2009 where it has been rescaled to R500 instead of R200 using the average R500/R200 ratio of the Sun+09 group sample
                    x_base = np.asarray(S_plt.axes.get_xlim())
                    if x_base[0] < 0.01: x_base[0] = 0.01 ## approx limit of baseline approximation
                    y_base = 1.32 * 0.4**(1.0/3.0) * (1.0/0.655)**0.9 * (0.15/fB)**(2.0/3.0) * x_base**1.1 ## the ratio R500/R200 = 0.655 is the mean value for all simulated haloes with M500>=3.8e13 Msun (i.e. the low and high mass sample)
                    S_plt.errorbar(x_base, y_base, c='0.5',
                                       label="Voit+ 2005", lw=2, dashes=(2,6,6,6), zorder=0)
            if plot_only=="pressure":
                if compare=="Sun":
#==============================================================================
#                     #SunPmed = np.loadtxt(obs_dir+"Sun2010_pressure.txt", delimiter=",")
#                     #P_plt.errorbar(SunPmed[5:,0], SunPmed[5:,1], c='0.5', lw=3, dashes=(14,8), alpha=1, zorder=1, label="Sun+ 2010")
#                     SunPmed = np.loadtxt(obs_dir+"Sun2010_pressure_prof.csv", delimiter=",")
#                     SunInd = np.arange(1, len(SunPmed[:,0]), step=4)
#                     SunPmed[:,[1,4,5]] = SunPmed[:,[1,4,5]] + np.log10(0.175 / fB) ## rescale P500 to my fB, assuming they use the same fB as Arnaud+2010
#                     P_plt.errorbar(10**SunPmed[SunInd,0]*(h_scale/0.73), 10**SunPmed[SunInd,1], yerr=[10**SunPmed[SunInd,1] - 10**SunPmed[SunInd,4], 10**SunPmed[SunInd,5] - 10**SunPmed[SunInd,1]], fmt='o', mew=1.2, lw=1.2, ms=6, mec='none', mfc=suncol, color=suncol, ecolor=suncol, label="Sun+ 2010")
#==============================================================================
                    
                    xbins = np.logspace(np.log10(max(0.01, xmin)), np.log10(min(xmax, 1.0)), num=(np.log10(xmax) - np.log10(xmin))/0.08)
                    Suncat = np.genfromtxt(obs_dir+"Sun2009.csv", delimiter=",", dtype=None) ## dtype=None guesses data type (can be slow)
                    import os
                    all_values = np.zeros((len(xbins), len(Suncat['f2'])))
                    for f_idx, name in enumerate(Suncat['f2']):
                        d_dat = np.loadtxt(obs_dir+"Sun_2009_density_profiles/"+name+"_den") ## log10(r/kpc)   ne  lower_boundary  upper_boundary
                        T_dat = np.loadtxt(obs_dir+"Sun_2009_temp_profiles/T_3D/"+name+"_T") ## log10(r/kpc)   T_3d (keV)   lower_boundary  upper_boundary
                        #T_dat = np.loadtxt(obs_dir+"Sun_2009_temp_profiles/T_2D/"+name+"_T_lin.dat") ## log10(r/kpc)   T_3d (keV)   lower_boundary  upper_boundary
                        Sun_R500 = Suncat['f10'][f_idx] * (0.73/h_scale) ## R500 in kpc
                        Sun_z = Suncat['f3'][f_idx] ## redshift
                        d_x = 10**d_dat[:,0] * (0.73/h_scale) / Sun_R500 * (1.0-b)**(1.0/3.0) ## r/r500
                        T_x = 10**T_dat[:,0] * (0.73/h_scale) / Sun_R500 * (1.0-b)**(1.0/3.0) ## for T_3D r/r500
                        #T_x = T_dat[:,0] * (0.73/h_scale) * (1.0-b)**(1.0/3.0) ## for T_2D r/r500
                        d_y = d_dat[:,1] * (h_scale/0.73)**2 ## n_e in cm^-3
                        T_y = T_dat[:,1] ## T_3D in keV
                        #Sun_T2500 = Suncat['f7'][f_idx] ## for T_2D
                        #T_y = T_dat[:,3] * Sun_T2500 ## for T_2D: multiply T/T2500 by T2500 to get T in keV
                        ## Interpolate the density at each temperature datapoint
                        ## Interpolate in log space, returning np.inf when outside range
                        d_y2 = 10.0**np.interp(np.log10(T_x), np.log10(d_x), np.log10(d_y), left=np.inf, right=np.inf)
                        P_y = d_y2 * T_y ## keV cm^-3
                        P_y[np.isinf(P_y)] = 0.0 ## replace infs with zeroes
                        P_y = np.ma.masked_values(P_y, 0.0) ## mask out infs
                        if scaled: ## scale to P/P500
                            Sun_T500 = G * mu * mp * Suncat['f19'][f_idx]*1e13*1.989e33 / (2.0 * Suncat['f10'][f_idx] * 3.085678e21) / 1.6021772e-9 / (1.0-b)**(2.0/3.0)
                            Sun_ne500 = 500.0 * fB * (3.0 * (h_scale * 100 / 3.086e+19)**2 / (8.0 * np.pi * G)) / (mu_e * mp)
                            Sun_P500 = Sun_ne500 * Sun_T500
                            P_y /= Sun_P500
                        ## plot individual profiles
                        P_plt.plot(T_x[T_x > xmin], P_y[T_x > xmin], c=Suncol, alpha=Sunalpha, lw=Sunlw, zorder=1)
                        ## Interpolate at bin position in logspace, returning np.inf when outside range
                        values = np.interp(np.log10(xbins), np.log10(T_x), np.log10(P_y), left=np.inf, right=np.inf)
                        all_values[np.isfinite(values), f_idx] = 10.0**values[np.isfinite(values)] ## mask out the np.inf to avoid an error, leaving them as zero
                    all_values = np.ma.masked_values(all_values, 0.0) ## masked array - mask out remaining zeros
                    P_med = np.ma.median(all_values, axis=1)
                    all_values_nan = np.ma.filled(all_values, np.nan) ## np.ma has no percentile so fill masked values with nans and use nanpercentile
                    #P_err1 = P_med - np.nanpercentile(all_values_nan, 16.0, axis=1)
                    #P_err2 = np.nanpercentile(all_values_nan, 84.0, axis=1) - P_med
                    if b > 0.0:
                        Sunlabel = "Sun+ 2009 [b = "+str(b)+"]"
                    else:
                        Sunlabel = "Sun+ 2009"
                    #P_plt.errorbar(xbins, P_med, yerr=[P_err1, P_err2], zorder=3, fmt='o', mew=1.2, lw=1.2, ms=6, mec='none', mfc=suncol, color=suncol, ecolor=suncol, label=Sunlabel)
                    #P_plt.errorbar(xbins, P_med, yerr=[P_err1, P_err2], zorder=3, fmt='o', mew=1.2, lw=1.2, ms=6, mec='none', mfc='r', color='r', ecolor='r', alpha=0.4, label=Sunlabel)
                    P_plt.errorbar(xbins, P_med, c=Sunmedcol, lw=Sunmedlw, dashes=(14,8), zorder=1, label=Sunlabel)

                elif compare=="REXCESS":
                    xlims = P_plt.axes.get_xlim()
                    xbins = np.logspace(np.log10(xlims[0]), np.log10(xlims[1]), num=1000)
                    import profiles.REXCESS as REXCESS
                    profiles, cls = REXCESS.get_pressure_prof(xbins, scaled=scaled, fB=fB, h=h_scale, M500min=0.0, M500max=REXM500max)
                    print "cls =", cls
                    ## Individual REXCESS profiles:
                    for i in range(len(profiles[:,0])):
                        P_plt.loglog(xbins, profiles[i,:], c=REXcol[cls[i]], ls=REXls[cls[i]], lw=REXlw, alpha=REXalpha, dash_capstyle='round', zorder=1, label=REXlabels[cls[i]])
                        REXlabels[cls[i]] = ""
                    ## need to reset the axis labels because they're overwritten for some reason
                    if for_paper: P_plt.xaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))
                    if for_paper and scaled: P_plt.yaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))
                    #med, patch = REXCESS.get_median_range(xbins, profiles)
                    #P_plt.loglog(xbins, med, c='0.5', ls='solid', dash_capstyle='round', lw=3, zorder=1, label="Arnaud et al. 2010")
                    #P_plt.axes.add_patch(Polygon(patch[patch[:,1]>0], alpha=0.3, color='0.5', ec='none', zorder=1))
                    #if plot_residual:
                        #P_plt_res.plot(xbins, med / arnaud10(xbins), c='0.5', lw=3)
                    
                    ## Median in bins:
                    x, med, err1, err2 = REXCESS.get_median_bins(xbins, profiles, dlogr=0.1, rmin=xlims[0]*0.1, rmax=xlims[1]*10)
                    #P_plt.errorbar(x, med, yerr=[err1,err2], marker='s', ls='none', c='r', mec='none')
                    P_plt.errorbar(x, med, c=REXmedcol, lw=REXmedlw, dashes=(14,8), zorder=1, label=REXmedlabel)
    
    #==============================================================================
    #                     P_obs1 = np.loadtxt(obs_dir+"Arnaud2010_pressure_profile_range.txt")
    #                     P_obs1[:,0] = P_obs1[:,0] * bias**(1.0/3.0)
    #                     P_obs1[:,1] = P_obs1[:,1] * bias**(2.0/3.0)
    #                     #poly_P_obs1 = Polygon(P_obs1, color='#ffe4b3', ec='none')
    #                     #P_plt.axes.add_patch(poly_P_obs1)
    #                     if bias < 1.0: suffix=" (1-b)={:.1f}".format(bias)
    #                     else: suffix=""
    #                     #P_plt.plot(xbins, arnaud10(xbins), label="Arnaud et al. 2010 (best fit)"+suffix, dash_capstyle='round', c='orange', lw=2.0)
    #                     P_plt.plot(xbins, planck13(xbins), label="Planck Collab. 2013"+suffix, c='0.5', ls='dashed', dash_capstyle='round', lw=3, zorder=1)
    #                     if plot_residual:
    #                         P_plt_res.axhline(1.0, c='orange', lw=3)
    #                         P_plt_res.plot(xbins, planck13(xbins) / arnaud10(xbins), c='green', ls='dashed', lw=3)
    #                     
    #==============================================================================
            if plot_only=="mass":
                #data = np.loadtxt("/data/curie4/nh444/project1/L-T_relations/L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_025/group_0/"+"seperate_profiles.txt")
                #gasmasscum = data[:,5] * 1e10 / h_scale ## Msun
                #m_plt.plot(data[(data[:,2]/500.636>xmin),2]/500.63605,
                                #gasmasscum[(data[:,2]/500.636>xmin)]-np.min(gasmasscum[(data[:,2]/500.636>xmin)]),
                                #'o', mfc='black')
                if compare=="REXCESS":
                    rexcesstable = np.genfromtxt(obs_dir+"REXCESS_profiles/REXCESS_table_Pratt2009.csv", delimiter=",", dtype=None)
                    CC = rexcesstable['f22']
                    disturbed = rexcesstable['f23']
                    z = rexcesstable['f1']
    
                    for rexid in range(len(z)):
                        data = np.loadtxt(obs_dir+"REXCESS_profiles/density_profiles/REXCESS_density_profile_t"+str(rexid+1)+".csv", delimiter=",")
                        #data = data[data[:,1] > min(bins_rel[:,0])]
                        rexdens = data[:,2] * ne2rho / E_zrex[rexid]**2 * (h_scale/0.7)**2# g / cm^3
                        rexvol = 4.0/3.0*np.pi * (data[1:,0]**3 - data[:-1,0]**3) * E_zrex[rexid]**3 * (0.7/h_scale)**3 ## kpc^3
                        rexmass = rexdens[1:] * rexvol * 3.085678e21**3 / 1.989e33 # assuming bin radius is the outside edge rather than inside (Msun)
                        if scaled:
                            rexmass = np.cumsum(rexmass) / np.sum(rexmass)
                        else:
                            rexmass = np.cumsum(rexmass)
                        #rexrad = 10**((np.log10(data[1:,1])+np.log10(data[:-1,1]))/2.0)
                        rexrad = data[1:,1] ## RHS of bin
                        #print rexid+2, rexrad[np.argmax(rexrad)-1], rexmass[np.argmax(rexrad)-1]
                        if CC[rexid] == 1:
                            m_plt.plot(rexrad, rexmass, c='blue', alpha=0.2, lw=2, zorder=1)
                        elif disturbed[rexid] == 1:
                            m_plt.plot(rexrad, rexmass, c='green', alpha=0.2, lw=2, zorder=1)
                        else:
                            m_plt.plot(rexrad, rexmass, c='grey', alpha=0.2, lw=2, zorder=1)
            if plot_only=="gasfrac" and compare is not "none":
                ## I haven't scaled r/r500 by h 
                data = np.loadtxt("/data/vault/nh444/ObsData/profiles/gas_fractions/gas_frac_Planck13.txt")
                f_plt.plot(data[:,0], data[:,1], c='0.5', lw=2.5, label="Planck 2013")##hypth. (1)
                data = np.loadtxt("/data/vault/nh444/ObsData/profiles/gas_fractions/gas_frac_Arnaud2010.txt")
                f_plt.plot(data[:,0], data[:,1], c='0.5', lw=2.5, ls='dashed', label="Arnaud+2010")##hypth. (1)
                data = np.loadtxt("/data/vault/nh444/ObsData/profiles/gas_fractions/IllustrisTNG_gas_frac_profile.csv", delimiter=",")
                f_plt.plot(data[:,0], data[:,1], c='orange', lw=2.5, ls='solid', label="IllustrisTNG")
                data = np.loadtxt("/data/vault/nh444/ObsData/profiles/gas_fractions/IllustrisTNG_gas_frac_median_patch.csv", delimiter=",")
                poly = Polygon(data, color='orange', alpha=0.2, ec='none')
                f_plt.axes.add_patch(poly)
                data = np.loadtxt("/data/vault/nh444/ObsData/profiles/gas_fractions/Pratt2010_gas_frac_profile.csv", delimiter=",")
                f_plt.plot(data[:,0], data[:,1], c='0.5', ls='dotted', lw=2.5, label="Pratt+2010")
                
        if leglabelsize is None:
            if add_prof_labels:
                leglabelsize = 18-(Nend-Nstart) ## 18 if no prof_labels, 10 for 8 prof_labels etc
            elif add_file_labels and file_labels is not None:
                leglabelsize = 20 - nfiles
            else:
                leglabelsize = axislabelsize-2 ## Note: legend labels are (axislabelsize-2) because they are horizontal
        if add_file_labels:
            baxespad = 0.8
        else:
            baxespad = 2
        if plot_only=='all' or plot_only=='density':
            d_handles, d_labels = d_plt.axes.get_legend_handles_labels()
            leg = d_plt.legend(d_handles, d_labels, loc='lower left', frameon=frameon, borderpad=0.5,
                         title="", borderaxespad=baxespad, fontsize=leglabelsize, numpoints=1)
            if transparent:
                leg.get_frame().set_facecolor('none')
        if plot_only=='all' or plot_only=='temperature':
            T_handles, T_labels = T_plt.axes.get_legend_handles_labels()
            leg = T_plt.legend(T_handles, T_labels, frameon=frameon, borderpad=0.5, loc='best',
                         title="", borderaxespad=baxespad, fontsize=leglabelsize, ncol=1)#loc=(0.4, 0.05),
            if transparent:
                leg.get_frame().set_facecolor('none')
        if plot_only=='all' or plot_only=='entropy':
            S_handles, S_labels = S_plt.axes.get_legend_handles_labels()
            leg = S_plt.legend(S_handles, S_labels, loc='lower right', frameon=frameon, borderpad=0.5,
                         title="", borderaxespad=baxespad, fontsize=leglabelsize)
            if transparent:
                leg.get_frame().set_facecolor('none')
        if plot_only=='pressure':
            P_handles, P_labels = P_plt.axes.get_legend_handles_labels()
            leg = P_plt.legend(P_handles, P_labels, loc='lower left', frameon=frameon, borderpad=0.5,
                         title="", borderaxespad=0.9, fontsize=leglabelsize)
            if transparent:
                leg.get_frame().set_facecolor('none')
        if plot_only=='mass':
            m_handles, m_labels = m_plt.axes.get_legend_handles_labels()
            leg = m_plt.legend(m_handles, m_labels, loc='best', frameon=frameon, borderpad=0.5,
                         title="", borderaxespad=0.9, fontsize=leglabelsize)
            if transparent:
                leg.get_frame().set_facecolor('none')
        if plot_only=='gasfrac':
            f_handles, f_labels = f_plt.axes.get_legend_handles_labels()
            leg = f_plt.legend(f_handles, f_labels, loc='best', frameon=frameon, borderpad=0.5,
                         title="", borderaxespad=0.9, fontsize=leglabelsize)
            if transparent:
                leg.get_frame().set_facecolor('none')
    
    ## Colour scale
#==============================================================================
#         if plot_colour_scale and col_given:
#             print '\033[31m'+"ERROR: Cannot currently plot colour scale for colours specified in function argument"+'\033[0m'
#             plot_colour_scale = False
#==============================================================================
    if (M500_min > 0 or M500_max < np.inf) and plot_colour_scale:
        print "Average, median M500 = ", np.mean(labelsM500), np.median(labelsM500)
    if plot_colour_scale:
        for j in range(0, len(labelsM500)):
            labelsM500[j] = '{:.1f}'.format(labelsM500[j])
        if discrete_colours:
            if col_given:
                ## create colourmap from given colours
                from matplotlib.colors import ListedColormap
                c = colours[:Nend-Nstart]
                cmap = ListedColormap(c[::-1]) ## have to reverse list because I defined colour_index the wrong way round, hence why the M500labels list is reversed below when setting the x tick labels
                cmax = 1.0
        
            ##colour scale will show discrete colours rather than a gradient
            bounds = np.linspace(0, Nend-Nstart, Nend-Nstart+1)
            from matplotlib.colors import BoundaryNorm
            norm = BoundaryNorm(bounds, int(cmap.N*cmax))
            import matplotlib.colorbar
            ticks = np.linspace(0, Nend-Nstart, Nend-Nstart+1)
            ticks = ticks + ticks[1]/2.0
            cbar = matplotlib.colorbar.ColorbarBase(c_plt, cmap=cmap, norm=norm, spacing='proportional', ticks=ticks, boundaries=bounds, orientation='horizontal')
            c_plt.tick_params(length=0)
        else:
            gradient = np.linspace(0, 1, 256)
            gradient = np.vstack((gradient, gradient))
            norm = matplotlib.colors.Normalize(vmin=0.0, vmax=1.0/cmax)
            c_plt.axes.imshow(gradient, aspect='auto', cmap=cmap, norm=norm)
            c_plt.axes.get_yaxis().set_visible(False)
            #outer.tight_layout(fig, rect=[0, 0.05, 1, 0.95])
        c_plt.axes.set_xticklabels(labelsM500[::-1], rotation='horizontal', fontsize=ticklabelsize)
        
        c_plt.axes.set_xlabel(r"\mathrm{M}$_{500}$ [$10^{"+str(M500expon)+"}$ $M_{\odot}$]", fontsize=axislabelsize) ## $h^{-1}$
        
    if blkwht:
        import matplotlib
        fig.patch.set_facecolor('0.2')
        for ax in fig.axes:
            ax = plt.gca()
            ax.set_axis_bgcolor('0.2')
            ax.xaxis.label.set_color('white')
            ax.tick_params(axis='x', which='both', colors='white')
            ax.yaxis.label.set_color('white')
            ax.tick_params(axis='y', which='both', colors='white')
            for child in ax.get_children():
                if isinstance(child, matplotlib.spines.Spine):
                    child.set_color('white')
        if plot_colour_scale:
            c_plt.xaxis.label.set_color('white')
            c_plt.tick_params(which='both', colors='white')
            if discrete_colours:
                cbar.outline.set_edgecolor('white')
            for child in c_plt.get_children():
                if isinstance(child, matplotlib.spines.Spine):
                    child.set_color('white')
        for text in leg.get_texts():
            text.set_color("white")
        
        fig.savefig(outname.replace(".pdf",".png"), bbox_inches='tight', transparent=True)
        print "Plot(s) saved to", outname.replace(".pdf",".png")
    else:
        fig.savefig(outname, transparent=transparent, bbox_inches='tight')
        print "Plot(s) saved to", outname
    f.close()
    return outname

if __name__ == "__main__":
    main()
