#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 17 15:29:26 2017
This script contains functions for plotting the radial profiles of the REXCESS
cluster sample. The density profiles are tabulated whilst the pressure and 
entropy profiles are taken from the best-fit parameters. Temperature profiles
are derived from the pressure and entropy profiles.
@author: Nick Henden
"""

## Path of directory containing tabulated data
datadir = "/data/vault/nh444/ObsData/profiles/REXCESS_profiles/"

def main():
    """
    The following plots the density, pressure, entropy and temperature profiles
    of the REXCESS clusters within a specified halo mass range.
    """
    import numpy as np
    import matplotlib.pyplot as plt
    from matplotlib.patches import Polygon

    xlims = [0.01, 2.0] ## r/r500
    M500min = 0.0 ## minimum M500 in Msun
    M500max = 9e14 ## max M500 (for REXCESS max is 8.41e14 Msun)
    ## define radial bins for plotting the best-fit profiles and the medians
    xbins = np.logspace(np.log10(xlims[0]), np.log10(xlims[1]), num=1000)
    h = 0.7 ## the value of h to scale to
    
    fig, (d_plt, P_plt, S_plt, T_plt) = plt.subplots(ncols=4, figsize=(20,8))
    for ax in (P_plt, S_plt, T_plt, d_plt):
        ax.set_xlabel("r/r$_{500}$")
        ax.set_xlim(xlims[0], xlims[1])
        ax.set_xscale('log')
    P_plt.set_ylabel("P/P$_{500}$")
    S_plt.set_ylabel("K [keV cm$^2$]")
    T_plt.set_ylabel("T/T$_{500}$ [keV]")
    d_plt.set_ylabel(r"$\mathrm{E(z)}^{-2}$ $\mathrm{n}_{\mathrm{e}}$ [cm$^{-3}$]")
    d_plt.set_yscale('log')
    plt.tight_layout()
    
    ## plot electron density profiles from tables
    rexcesstable = np.genfromtxt(datadir+"REXCESS_table_Pratt2009.csv", delimiter=",", dtype=None)
    zrex = rexcesstable['f1']
    M500 = rexcesstable['f24']
    E_zrex = np.sqrt(0.3*(1+zrex)**3 + 0.7)
    for i in range(len(zrex)):
        if (M500[i] * 1e14 > M500min) & (M500[i] * 1e14 < M500max):
            dat = np.loadtxt(datadir+"density_profiles/REXCESS_density_profile_t"+str(i+1)+".csv", delimiter=",")
            dens = dat[:,2] / E_zrex[i]**2 * (h/0.7)**2
            d_plt.loglog(dat[:,1], dens, c='k', alpha=0.2)
    ## Plot the median density profile
    x, med, err1, err2 = get_density_prof(rmin=xlims[0], rmax=xlims[1],
                                          M500min=M500min, M500max=M500max, h=h)
    d_plt.errorbar(x, med, yerr=[err1,err2], marker='s', ls='none', c='r', mec='none')

    ## pressure profiles from model
    profiles, cls = get_pressure_prof(xbins, scaled=True, M500min=M500min, M500max=M500max, h=h)
    for i in range(len(profiles[:,0])):
        P_plt.loglog(xbins, profiles[i,:], c='k', alpha=0.2)
    med, patch = get_median_range(xbins, profiles)
    P_plt.loglog(xbins, med, c='k', lw=2)
    P_plt.axes.add_patch(Polygon(patch[patch[:,1]>0], alpha=0.2, color='r', ec='none', zorder=2))
    x, med, err1, err2 = get_median_bins(xbins, profiles, rmin=xlims[0], rmax=xlims[1])
    P_plt.errorbar(x, med, yerr=[err1,err2], marker='s', ls='none', c='r', mec='none')

    ## compare to pressure profiles from plots in papers
    P_obs1 = np.loadtxt(datadir+"Arnaud2010_pressure_profile_range.txt")
    poly_P_obs1 = Polygon(P_obs1, color='#ffe4b3', ec='none')
    P_plt.axes.add_patch(poly_P_obs1)
    planck13 = lambda x: 6.41 / ((1.81 * x)**0.31 * (1 + (1.81 * x)**1.33)**((4.13-0.31)/1.33))
    ## below doesn't include mass dependence of normalisation \propto M500^0.12
    arnaud10 = lambda x: 8.403 * (0.7/h)**(3.0/2.0) / ((1.177 * x )**0.3081 * (1 + (1.177 * x)**1.051)**((5.4905-0.3081)/1.051))
    P_plt.loglog(xbins, arnaud10(xbins), label="Arnaud et al. 2010", c='orange', lw=2.0)
    P_plt.loglog(xbins, planck13(xbins), label="Planck Collab. 2013", c='green', ls='dashed', lw=2.0)

    ## compare to Sun et al. 2010 pressure profiles
    SunPmed = np.loadtxt("/data/vault/nh444/ObsData/profiles/"+"Sun2010_pressure_prof.csv", delimiter=",")
    SunInd = np.arange(1, len(SunPmed[:,0]), step=4)
    #SunPmed[:,[1,4,5]] = SunPmed[:,[1,4,5]] + np.log10(0.175 / fB) ## rescale P500 to my fB, assuming they use the same fB as Arnaud+2010
    suncol = 'darkblue'
    P_plt.errorbar(10**SunPmed[SunInd,0]*(0.7/0.73), 10**SunPmed[SunInd,1], yerr=[10**SunPmed[SunInd,1] - 10**SunPmed[SunInd,4], 10**SunPmed[SunInd,5] - 10**SunPmed[SunInd,1]], fmt='o', mew=1.2, lw=1.2, ms=6, mec='none', mfc=suncol, color=suncol, ecolor=suncol, label="Sun+ 2010")
       
    ## entropy profiles from model
    profiles, cls = get_entropy_prof(xbins, M500min=M500min, M500max=M500max, h=0.7)
    for i in range(len(profiles[:,0])):
        S_plt.loglog(xbins, profiles[i,:], c='k', alpha=0.2)
    med, patch = get_median_range(xbins, profiles)
    S_plt.loglog(xbins, med, c='k', lw=2)
    S_plt.axes.add_patch(Polygon(patch[patch[:,1]>0], alpha=0.2, color='r', ec='none', zorder=2))
    x, med, err1, err2 = get_median_bins(xbins, profiles, rmin=xlims[0], rmax=xlims[1])
    S_plt.errorbar(x, med, yerr=[err1,err2], marker='s', ls='none', c='r', mec='none')
    
    ## temperature profiles from pressure and entropy
    profiles, cls = get_temp_prof(xbins, M500min=M500min, M500max=M500max, non_CC=True, h=h)
    for i in range(len(profiles[:,0])):
        T_plt.plot(xbins, profiles[i,:], c='k', alpha=0.2)
    med, patch = get_median_range(xbins, profiles)
    T_plt.plot(xbins, med, c='k', lw=2)
    T_plt.axes.add_patch(Polygon(patch[patch[:,1]>0], alpha=0.2, color='r', ec='none', zorder=2))
    x, med, err1, err2 = get_median_bins(xbins, profiles, rmin=xlims[0], rmax=xlims[1])
    T_plt.errorbar(x, med, yerr=[err1,err2], marker='s', ls='none', c='r', mec='none')
    
    Barnes = np.loadtxt(datadir+"Arnaud2010_temp_prof_Barnes2017.csv", delimiter=",")
    T_plt.errorbar(Barnes[:,0],Barnes[:,1], yerr=[Barnes[:,2], Barnes[:,3]], marker='s', c='black', ls='none', label="Arnaud+ 2010")

def get_density_prof(rmin=0.0, rmax=None, dlogr=0.1, M500min=0.0, M500max=None,
                     target_med=None, h=0.7):
    """
    Returns the median REXCESS density profile in range r/r500=[rmin,rmax] in bins of width dlogr
    If target_med is not None, it will use a cluster sample with median mass as close as possible to target_med (Msun)
    Otherwise it will use all clusters in the given mass range M500min-M500max (Msun)
    Args:
        rmin: minimum radius of profile in units r500
        rmax: maximum radius of profile in units r500
        dlogr: spacing of logarithmic radial bins
        M500min, M500max: min and max M500 of REXCESS clusters to plot in Msun
        target_med: If not None, the target median mass of the REXCESS sample
        h: value of little h to scale profiles to
    """
    import numpy as np
    if rmax is None:
        rmax = np.inf
    if M500max is None:
        M500max = np.inf
        
    bin_edges = np.logspace(np.log10(rmin), np.log10(rmax), num=(np.log10(rmax)-np.log10(rmin))/dlogr)
    xbins = bin_edges[:-1]**0.5 * bin_edges[1:]**0.5

    rexcesstable = np.genfromtxt(datadir+"REXCESS_table_Pratt2009.csv", delimiter=",", dtype=None)
    zrex = rexcesstable['f1']
    M500 = rexcesstable['f24'] * (0.7/h) * 1e14
    if target_med is not None:
        ind = find_sample_with_med_mass(M500, target_med)
    else:
        ind = np.where((M500 > M500min) & (M500 < M500max))[0] ## no. in mass range
    N = len(ind)
    assert N > 0, "No clusters in mass range ({:.2e}, {:.2e}) Msun".format(M500min, M500max)
    print "REXCESS sample: N =", N, "of", len(zrex), "Median mass =", np.median(M500[ind]), "("+str(np.median(M500))+" for full sample)"
    print "REXCESS sample: Min =", np.min(M500[ind]), "Max =", np.max(M500[ind])
    E_zrex = np.sqrt(0.3*(1+zrex)**3 + 0.7)
                    
    all_values = np.zeros((len(xbins), N))
    j=0
    for idx in ind:
        dat = np.loadtxt(datadir+"density_profiles/REXCESS_density_profile_t"+str(idx+1)+".csv", delimiter=",")
        x = dat[:,1] * (0.7/h)**(2.0/3.0) ## r/r500 - h^2/3 factor since r (h^-1) and r500 (h^-1/3)
        assert np.all(np.diff(x) > 0), "r/r500 not increasing"
        dens = dat[:,2] / E_zrex[idx]**2 * (h/0.7)**2
        ## Interpolate density at bin position, returning np.inf when outside range
        values = np.interp(np.log10(xbins), np.log10(x), np.log10(dens), left=np.inf, right=np.inf)
        all_values[np.isfinite(values), j] = 10.0**values[np.isfinite(values)]
        j += 1
    all_values = np.ma.masked_values(all_values, 0.0) ## mask out zeros
    med = np.ma.median(all_values, axis=1)
    ## np.ma has no percentile so fill masked values with nans and use nanpercentile
    all_values_nan = np.ma.filled(all_values, np.nan)
    err1 = med - np.nanpercentile(all_values_nan, 16.0, axis=1)
    err2 = np.nanpercentile(all_values_nan, 84.0, axis=1) - med
    return xbins, med, err1, err2
    
def get_temp_prof(xbins, ss_scaling=False, mu=0.59, T500_scaling=True, M500min=0.0, M500max=None, bias=1.0, non_CC=False, h=0.7):
    """
    Calculates temperature profiles for xbins=r/r500 for REXCESS clusters in
    given mass range (Msun) from their entropy and pressure profile fits
    """
    import numpy as np
    if M500max is None:
        M500max = np.inf
    assert ss_scaling*T500_scaling == 0, "Can't define both scalings"
    
    mp = 1.6726231e-24 ## proton mass in g
    G = 6.67259e-8 ## gravitational constant in cgs
    
    Pdat = np.genfromtxt("/data/vault/nh444/ObsData/profiles/REXCESS_profiles/Arnaud2010.csv", delimiter=",")
    Sdat = np.genfromtxt("/data/vault/nh444/ObsData/profiles/REXCESS_profiles/Pratt2010_Entropy_param.csv", delimiter=",")
    
    ind = np.where((Pdat[:,2] * 1e14 / bias > M500min) & (Pdat[:,2] * 1e14 / bias < M500max))[0]
    if non_CC:
        ind = np.intersect1d(ind, np.where(Sdat[:,12] < 1)[0]) ## non-cool cores only
    assert len(ind) > 0, "No clusters in mass range ({:.2e}, {:.2e}) Msun".format(M500min, M500max)
    Pnames = Pdat[ind,0]
    R500 = Pdat[ind,1] * 1000.0 ## kpc
    M500 = Pdat[ind,2] * 1e14 
    P500 = Pdat[ind,12] * 1e-3
    P0 = Pdat[ind,13]
    c500 = Pdat[ind,14]
    Palpha = Pdat[ind,15]
    gamma = Pdat[ind,16]
    K0 = Sdat[ind,3]
    K100 = Sdat[ind,6]
    Salpha = Sdat[ind,9]
    CC = Sdat[ind,12].astype(np.int8) ## =1 if cool-core
    dist = Sdat[ind,13].astype(np.int8) ##=1 if disturbed
    cls = np.zeros(len(CC), dtype=np.int8) ## 1 if CC, 2 if disturbed, 0 otherwise
    cls[(CC==1)] = 1
    cls[(dist==1)] = 2
    profiles = []
    for i, Pname in enumerate(Pnames):
        assert Pdat[i,1]==Sdat[i,1], "Cluster R500 "+str(i)+" doesn't match."
        P = pres_prof_model(xbins, P500=P500[i], M500=M500[i], P0=P0[i], c500=c500[i], gamma=gamma[i], alpha=Palpha[i], h=h)
        K = entropy_prof_model(xbins * R500[i], K0[i], K100[i], Salpha[i], h=h)
        kT = np.asarray(K)**(3.0/5.0) * np.asarray(P)**(2.0/5.0)
        if ss_scaling: ## self-similar scaling \propto M500/R500
            #T500 = 6.67259e-8 * 0.59 * 1.6726231e-24 * M500[i]*1.989e33 / (2.0 * R500[i] * 3.085678e21) / 1.6021772e-9
            T500 = G * mu * mp * M500[i]*1.989e33 / bias / (2.0 * R500[i] / bias**(1.0/3.0) * 3.085678e21) / 1.6021772e-9
            profiles.append(kT / T500)
        elif T500_scaling:
            dat = np.genfromtxt("/data/vault/nh444/ObsData/M-T/REXCESS_M-T.csv", delimiter=",")
            ## should have clusters in same order as Pdat and Sdat
            T500 = dat[ind[i],4] ## T(r500) in keV
            profiles.append(kT / T500)
        else:
            profiles.append(kT)
    return np.array(profiles), cls
    
def get_entropy_prof(xbins, scaled=True, fB=0.175, M500min=0.0, M500max=None, h=0.7):
    """
    returns the entropy (keV cm^2) at xbins=r/r500 for REXCESS clusters in
    given mass range (Msun)
    Args:
        fB: universal baryon fraction. default is 0.175 as used in Arnaud+2010
    """
    import numpy as np
    if M500max is None:
        M500max = np.inf
    
    Sdat = np.genfromtxt(datadir+"Pratt2010_Entropy_param.csv", delimiter=",")
    ind = np.where((Sdat[:,2] * 1e14 > M500min) & (Sdat[:,2] * 1e14 < M500max))[0]
    assert len(ind) > 0, "No clusters in mass range ({:.2e}, {:.2e}) Msun".format(M500min, M500max)
    Snames = Sdat[ind,0]
    R500 = Sdat[ind,1] * 1000.0 ## kpc
    M500 = Sdat[ind,2] * 1e14
    z = Sdat[ind,14]
    K0 = Sdat[ind,3]
    K100 = Sdat[ind,6]
    Salpha = Sdat[ind,9]
    CC = Sdat[ind,12].astype(np.int8) ## =1 if cool-core
    dist = Sdat[ind,13].astype(np.int8) ##=1 if disturbed
    cls = np.zeros(len(CC), dtype=np.int8) ## 1 if CC, 2 if disturbed, 0 otherwise
    cls[(CC==1)] = 1
    cls[(dist==1)] = 2
    
    ## Self-similar scaling
    ## for self-similar scaling factors see Arnaud+2010 Appendix A or Barnes+2017
    
    mu = 0.59 ## mean molecular weight as used in Arnaud+2010 and Nagai+2007 - doesn't matter exactly what this is as long as it's used consistently
    mu_e = 1.14 ## mean atomic weight per free electron as used in Arnaud+2010 and Nagai+2007
    mp = 1.6726231e-24 ## proton mass in g
    G = 6.67259e-8 ## gravitational constant in cgs
    Ez = np.sqrt(0.3*(1+z)**3 + 0.7)
    rho_crit = (3.0/8.0 / np.pi / 6.67408e-8 * (h * 100 / 3.086e+19)**2.0) * Ez**2 ## Critical density at redshift z in g cm^-3
    T500_ss = G * mu * mp * M500*1.989e33 / (2.0 * R500 * 3.085678e21) / 1.6021772e-9
    ne500_ss = 500 * fB * rho_crit / (mu_e * mp) ## cm^-3
    K500_ss = T500_ss / ne500_ss**(2.0/3.0) ## keV cm^2
    
    profiles = []
    for i, name in enumerate(Snames):
        K = entropy_prof_model(xbins * R500[i], K0[i], K100[i], Salpha[i], h=0.7)
        if scaled:
            profiles.append(K / K500_ss[i])
        else:
            profiles.append(K)
    return np.array(profiles), cls
    
def get_pressure_prof(xbins, scaled=True, fB=0.175, M500min=0.0, M500max=None, h=0.7):
    """
    returns the pressure (keV cm^-3) at xbins=r/r500 for REXCESS clusters in
    given mass range (Msun)
    Args:
        fB: universal baryon fraction. default is 0.175 as used in Arnaud+2010
    """
    import numpy as np
    if M500max is None:
        M500max = np.inf
        
    Pdat = np.genfromtxt(datadir+"Arnaud2010.csv", delimiter=",")
    ind = np.where((Pdat[:,2] * 1e14 > M500min) & (Pdat[:,2] * 1e14 < M500max))[0]
    assert len(ind) > 0, "No clusters in mass range ({:.2e}, {:.2e}) Msun".format(M500min, M500max)
    Pnames = Pdat[ind,0]
    M500 = Pdat[ind,2] * 1e14
    print "REXCESS sample: N =", len(ind), "of", len(Pdat[:,2]), "Median mass =", np.median(M500), "("+str(np.median(Pdat[:,2] * 1e14))+" for full sample)"
    P500 = Pdat[ind,12] * 1e-3 * (fB / 0.175) ## rescale P500 to fB
    P0 = Pdat[ind,13]
    c500 = Pdat[ind,14]
    Palpha = Pdat[ind,15]
    gamma = Pdat[ind,16]
    CC = Pdat[ind,17].astype(np.int8) ## =1 if cool-core
    dist = Pdat[ind,18].astype(np.int8) ##=1 if disturbed
    cls = np.zeros(len(CC), dtype=np.int8) ## 1 if CC, 2 if disturbed, 0 otherwise
    cls[(CC==1)] = 1
    cls[(dist==1)] = 2 ## there are two clusters that are both CC and disturbed - they will be classified as disturbed
    profiles = []
    for i, name in enumerate(Pnames):
        P = pres_prof_model(xbins, P500=P500[i], M500=M500[i], P0=P0[i], c500=c500[i], gamma=gamma[i], alpha=Palpha[i], h=h)
        if scaled:
            profiles.append(P / P500[i])
        else:
            profiles.append(P)
    return np.array(profiles), cls
    
def find_sample_with_med_mass(M, target_med, verbose=False):
    ## find the indices of the subsample whose median is closest to the target median (same units as M e.g. Msun)
    import numpy as np
    
    if verbose: print "target_med =", target_med
    sortInd = np.argsort(M)
    Msort = M[sortInd]
    idx = np.argmin(np.abs(target_med - Msort)) ## closest actual mass
    mid = (Msort[1:] + Msort[:-1])/2.0
    mididx = np.argmin(np.abs(target_med - mid)) ## closest middle value
    if verbose:
        print "idx =", idx
        print "Msort[idx] =", Msort[idx]
        print "mididx = ", mididx
        print "mid[mididx] =", mid[mididx]
    if np.abs(target_med - Msort[idx]) < np.abs(target_med - mid[mididx]):## idx is closer than mididx
        length = min([idx, len(Msort) - idx])## number of indices to take either side of idx
        ind = sortInd[range(idx - length + 1, idx+length)]
        if verbose: print "length1 =", length
        if verbose: print "ind1 =", ind
    else:
        length = min([mididx, len(Msort) - (mididx + 1)]) ## corresponding bounding indices in M are mididx and mididx+1
        ind = sortInd[range(mididx - length + 1, mididx+1 + length)]
        if verbose: print "length2 =", length
        if verbose: print "ind2 =", ind
    return ind

def get_median_bins(r, profiles, rmin=0.01, rmax=2.0, dlogr=0.1):
    """
    Takes 2D array of profiles and returns median and 1-sigma errors in bins
    between rmin and rmax with log-spacing dlogr
    Note: profiles must be a 2D array of values with the first axis
    corresponding to the radius given in r
    (because the median is taken off all points in the bin, which will be
    biased by profile with more points in that bin)
    """
    import numpy as np
    bin_edges = np.logspace(np.log10(rmin), np.log10(rmax), num=(np.log10(rmax)-np.log10(rmin))/dlogr)
    x = bin_edges[:-1]**0.5 * bin_edges[1:]**0.5 ## bin centre
    profiles = np.array(profiles)
    if profiles.ndim == 1:
        print "\033[93m"+"Only one profile given."+"\033[0m"
        return x, profiles, np.zeros_like(profiles), np.zeros_like(profiles)
    med = []
    err1 = []
    err2 = []
    for i in range(len(bin_edges) - 1):
        binind = np.where((r >= bin_edges[i]) & (r < bin_edges[i+1]))[0]
        if len(binind) > 0:
            med.append(np.median(profiles[:,binind]))
            err1.append(med[-1] - np.percentile(profiles[:,binind], 16.0))
            err2.append(np.percentile(profiles[:,binind], 84.0) - med[-1])
        else:
            med.append(0.0)
            err1.append(0.0)
            err2.append(0.0)
        #print np.median(profiles[:,ind]), np.median(profiles[:,np.argmin(np.absolute(xbins - bin_edges[i]**0.5 * bin_edges[i+1]**0.5))])
    return x, med, err1, err2
    
def get_median_range(xbins, profiles):
    """
    Takes 2D array of profiles and returns the median and a 'patch' array
    which can be plotted as a Polygon in matplotlib
    e.g. ax.axes.add_patch(Polygon(patch))
    """
    import numpy as np
    profiles = np.array(profiles)
    
    med_bins = np.concatenate((xbins, xbins[::-1])) ## for patch
    
    if profiles.ndim == 1:
        print "\033[93m"+"Only one profile given."+"\033[0m"
        return profiles, np.column_stack((med_bins, np.concatenate((np.zeros_like(profiles), np.zeros_like(profiles)))))
    
    med = np.median(profiles, axis=0)
    err1 = [np.percentile(profiles[:,k], 16.0) for k in range(len(profiles[0]))]
    err2 = [np.percentile(profiles[:,k], 84.0) for k in range(len(profiles[0]))]
    err = np.concatenate((err1,err2[::-1]))
    patch = np.column_stack((med_bins, err))
    return med, patch
    
def entropy_prof_model(R, K0, K100, alpha, h=0.7):
    """
    Entropy profile model of Pratt+ 2010
    K0, K100 - keV cm^2
    R - kpc
    """
    return K0 + K100 * (R / (100.0 * 0.7/h))**alpha


def pres_prof_model(x, P500=None, M500=None, z=None, P0=8.403, c500=1.177,
                    gamma=0.3081, alpha=1.051, beta=5.4905,
                    h=0.6774, omega_m=0.3089):
    """
    Physical pressure profile (keV cm^-3 for given h) from eq 13 Arnaud+ 2010.
    Arguments:
        x: r/r500
        P500: keV cm^-3 (eq 5 Arnaud+ 2010)
        M500: M500 in Msun.
    """
    import numpy as np
    import sim_python.cosmo as cosmo
    cm = cosmo.cosmology(h, omega_m)

    if M500 is None:
        assert P500 is not None, "Need P500 or M500"
        assert z is not None, "Need redshift if M500 not given"
        Ez = cm.E_z(z)
        M500 = (P500 / 1.65e-3 / Ez**(8.0/3.0))**(3.0/2.0) * (3e14 * (0.7/h))
    if P500 is None:
        assert z is not None, "Must have P500 or redshift (z)"
        Ez = cm.E_z(z)
        P500 = 1.65e-3 * Ez**(8.0/3.0) * (M500 / (3e14 * (0.7/h)))**(2.0/3.0) * (h/0.7)**2
    else:
        P500 *= (h/0.7)**2

    gnfw = GNFW(x, P0=P0, c500=c500, gamma=gamma, alpha=alpha, beta=beta, h=h)
    ap1 = 0.12  # eq 7 Arnaud+ 2010
    x = np.array(x)
    ap2 = 0.10 - (ap1 + 0.1)*((x/0.5)**3.0 / (1.0 + (x / 0.5)**3.0))
    P = P500 * gnfw * (M500 / (3e14 * (0.7/h)))**(ap2 + ap1)
    return P


def GNFW(x, P0=8.403, c500=1.177, gamma=0.3081, alpha=1.051, beta=5.4905, h=0.6774):
    """
    Generalised NFW profile (eq 11 of Arnaud+ 2010)
    Default parameters are the best-fit to REXCESS sample (Arnaud+ 2010)
    x = r/r500
    """
    P0 *= (0.7/h)**1.5
    p = P0 / ((c500 * x)**gamma * (1 + (c500 * x)**alpha)**((beta - gamma)/alpha))
    return p
    
if __name__ == "__main__":
    main()
