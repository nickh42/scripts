#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 24 13:35:51 2016
This module produces logarithmic radial profiles of mass, temperature or 
entropy for a given radial range, centre, number of bins etc.

@author: nh444
"""

from profiles.radprof_plot import radprof_plot

def radprof_calc (centre, minrad, maxrad, group_id=None,
                  snapshot=None, snapfile=None, masstab=False, mpc_units=False,
                  kdtree=None, bin_ratio=None, nbins=0,
                  T_weighting="mass", rho_weighting="mass",
                  filter_type="Rasia", temp_thresh=-1, main_halo_only=False,
                  R500=None, M500=None, R2500=None, T200=None, mean_bins=None,
                  get_tot_mass_prof=False, incl_wind=True,
                  verbose=True):
    
    """ Function for calculating density, temp and entropy in radial bins 
    --------------------------- FUNCTION ARGUMENTS ---------------------------
    snapshot = snap.py class object
    snapfile - snapshot file path (string)
    centre - centre from which radii are calculated (comoving ckpc/h)
    minrad/maxrad - radial range (comoving kpc/h)
    nbins - no. of logarithmic bins between minrad and maxrad
    bin_ratio - overrides nbins = Ratio of outer to inner radius for each bin e.g. 1.4
    group_id - index of fof group in snapshot object
    main_halo_only - If true, only consider particles bound to main halo of group 'group_id'
    R500, M500, R2500, T200 - these are required if group_id is not given (kpc, Msun) but will also override group_id
    mean_bins - inner and outer radii of bin in which to calculate a 'mean' temperature
    temp_thresh - upper temperature limit as a factor of (estimated) T200
    T_weighting - temperature weighting = "mass", "EM" or "spec"
    rho_weighting - temperature weighting = "mass" or "EM"
    filter_type - filter out cooling branch: "none", "default" or "Rasia" - see xray.py
    """
    
    ##### MINRAD AND MAXRAD MUST BE COMOVING KPC/H #####
    ## This function works in code units and returns quantities in units
    ## of Msun/h and proper kpc/h
    
    import numpy as np
    import collections
    import snap
    
    Xh=0.76 # hydrogen mass fraction, 0.76 as used in snap.snapshot.get_temp()
    ##physical constants in cgs units
    PROTONMASS = 1.6726e-24
    ELECTRONMASS = 9.1094e-28
    eV = 1.6021766208e-19
    UnitLength_in_cm = 3.085678e21 ## 1.0 kpc, as used in snap.snapshot object s
    UnitMass_in_g = 1.9884430e43 ## 1.0e10 solar masses, as used in snap.snapshot object s
    UnitMass_in_Msun = 1e10
    
    ## factors of little h for each quantity
    hfac = collections.namedtuple('hfac', 'dist, mass, bins, rho, ne, T, S, P')
    hfac.dist = hfac.bins = hfac.mass = -1
    hfac.T = 0

    ## Assertions
    if group_id is None:
        assert R500 is not None, "Must provide R500 or group id"
        if temp_thresh > 0: assert (T200 is not None), "Must provide T200 or group id to use temp_thresh>0"
    if bin_ratio is None:
        assert nbins > 0, "Must define either bin_ratio or nbins > 0"

    if snapshot is None:
        if snapfile is not None:
            ## Read in snapshot
            basedir, snapnum = split_filename(snapfile)
            s = snap.snapshot(basedir, snapnum, masstab=masstab,
                      mpc_units=mpc_units)
        else:
            assert False, "Must have either snapshot object or snapshot filename"
    else:
        s = snapshot
        
    #h = s.header.hubble
    z = s.header.redshift
    time = s.header.time
    omega_m = s.header.omega_m
    
    ## Get halo properties
    if group_id is not None:
        if R500 is None: R500 = s.cat.group_r_crit500[group_id]
        if M500 is None: M500 = s.cat.group_m_crit500[group_id]## used in temperature-density filtering
        if T200 is None: T200 = s.cat.group_T_crit200[group_id]
        #R2500_est = 0.47 * R500 ## 0.47 factor is average of Sun+2009 sample
        if R2500 is None:
            if verbose: print "Calculating R2500c..."
            M2500, R2500 = s.get_so_mass_rad(group_id, 2500, ref="crit", verbose=verbose)
    assert maxrad > R500, "Need maxrad > R500 else cannot (currently) compute values within R500"
    
    ## Define logarithmic radial bins
    if bin_ratio is not None:
        nbins = int((np.log10(maxrad) - np.log10(minrad)) / np.log10(bin_ratio))
    binleft = np.logspace(np.log10(minrad), np.log10(maxrad), num=nbins, base=10.0)
    bincentre = binleft*10.0**((np.log10(maxrad) - np.log10(minrad))/nbins/2)  
    binvolTot = 4.0/3.0*np.pi*(binleft[1:]**3 - binleft[:-1]**3) ## (ckpc/h)^3

    if kdtree is not None:
        assert False, "kdtree not currently working"
    else:
        ## Load snapshot data
        if get_tot_mass_prof:
            print "Starting calculation of total mass profile (slow)..."
            if not hasattr(s, "pos"):## might be loaded from a previous profile
                if verbose: print "Loading positions..."
                s.load_pos()
                if verbose: print "All positions loaded."
            gaspos = s.pos[s.species_start[0]:s.species_end[0]]
            if verbose: print "Calculating distance..."
            ## Split into two to save memory in distance computation:
            n = len(s.pos[:,0]) / 2
            r_all = s.nearest_dist_3D(s.pos[:n], centre)
            r_all = np.concatenate((r_all, s.nearest_dist_3D(s.pos[n:], centre)))
            if not hasattr(s, "mass"):
                if verbose: print "Loading masses..."
                s.load_mass()
                if verbose: print "All masses loaded."
            gasmass = s.mass[s.species_start[0]:s.species_end[0]]
            if main_halo_only:
                m_ind = np.where((r_all < maxrad) & (s.hostsubhalos == s.cat.group_firstsub[group_id]))[0]
            else:
                m_ind = np.where(r_all < maxrad)[0]
            if verbose: print "Calculating total mass profile..."
            totmbinsum, bin_edges = np.histogram(r_all[m_ind], bins=binleft, weights=s.mass[m_ind])## 1e10 Mpc/h
            del r_all
            del m_ind
        elif hasattr(s, "pos") and hasattr(s, "mass"):
            totmbinsum = np.zeros(len(binleft) - 1)
            gaspos = s.pos[s.species_start[0]:s.species_end[0]]
            gasmass = s.mass[s.species_start[0]:s.species_end[0]]
        else:
            totmbinsum = np.zeros(len(binleft) - 1)
            if not hasattr(s, "gaspos"):
                print "Loading gas positions..."
                s.load_gaspos()
            gaspos = s.gaspos
            if not hasattr(s, "gasmass"):
                print "Loading gas masses..."
                s.load_gasmass()
            gasmass = s.gasmass
        if not hasattr(s, "gasrho"):
            s.load_gasrho()
        if not hasattr(s, "temp"):
            s.get_temp(del_ne=False, del_u=False)
        if incl_wind and not hasattr(s, "windpos"):
            if verbose: print "Getting wind properties..."
            s.load_stellar_ages()## technically this is the stellar formation time (code units), not exactly the age
            ageind = np.where(s.age <= 0)[0] ## winds have formation time <=0 (=0 if only just recoupled)
            s.load_starpos()
            s.windpos = s.starpos[ageind]
            del s.starpos
            s.load_starmass()
            s.windmass = s.starmass[ageind]
            del s.starmass
            #maxidx = len(mass)
            #pos = np.concatenate((pos, s.windpos), axis=0) ## all positions incl winds (winds must be append to the end)
            #mass = np.concatenate((mass, s.windmass), axis=0)
        
        if verbose: print "Calculating distances..."
        r = s.nearest_dist_3D(gaspos, centre)
        if incl_wind:
            rwind = s.nearest_dist_3D(s.windpos, centre)
            indwind = np.where(rwind < maxrad)[0]
        
        ## Get indices of gas cells which satisfy given criteria
        ## Note that numpy index arrays (size=len*8 bytes) will use less memory than
        ## a boolean mask (size=len*1) if the index array is >8 times shorter than
        ## the original array, which is true for r<~50*R500 in a 512^3 40-Mpc box
#==============================================================================
#         if main_halo_only:
#             if incl_wind:
#                 print "\033[31m", "WARNING: main_halo_only and incl_wind currently not compatible, including ALL winds", "\033[0m"
#             assert (group_id >= 0), "Group id "+str(group_id)+" not valid."
#             if not hasattr(s, "hostsubhalos"):
#                 if verbose: print "Loading hostsubhalos..."
#                 s.get_hostsubhalos()
#         if temp_thresh > 0.0:
#             assert T200 > 0.0, "Need T200 > 0 but have T200={:.1f} keV".format(T200)
#             if verbose: print "Applying temperature threshold..."
#             if main_halo_only:
#                 indFilt = np.where((r < maxrad) & (s.temp < temp_thresh*T200) & (s.hostsubhalos[s.species_start[0]:s.species_end[0]] == s.cat.group_firstsub[group_id]))[0]
#                 indUF = np.where((r < maxrad) & (s.hostsubhalos[s.species_start[0]:s.species_end[0]] == s.cat.group_firstsub[group_id]))[0]
#             else:
#                 indFilt = np.where((r < maxrad) & (s.temp < temp_thresh*T200))[0]
#                 indUF = np.where(r < maxrad)[0]
#         elif main_halo_only:
#             indFilt = np.where((r < maxrad) & (s.hostsubhalos[s.species_start[0]:s.species_end[0]] == s.cat.group_firstsub[group_id]))[0]
#             indUF = indFilt ## a copy should only be made if indFilt is reassigned, like below
#         else:
#             indFilt = np.where(r < maxrad)[0]
#             indUF = indFilt
#         if filter_type is not "none":
#             if verbose: print "Filtering..."
#             import scaling.xray_python.xray as xray
#             xray_ind = xray.xray_source(z, s.header.hubble, omega_m).filter_phase_space(s.temp, s.gasrho, filter_type=filter_type, M500=M500, R500=R500)
#             indFilt = np.intersect1d(indFilt, xray_ind, assume_unique=True)
#==============================================================================
            
        indFilt = np.where(r < maxrad)[0]
        indUF = indFilt
        if main_halo_only:
            if incl_wind:
                print "\033[31m", "WARNING: main_halo_only and incl_wind currently not compatible, including ALL winds", "\033[0m"
            assert (group_id >= 0), "Group id "+str(group_id)+" not valid."
            if not hasattr(s, "hostsubhalos"):
                if verbose: print "Loading hostsubhalos..."
                s.get_hostsubhalos()
            curind = np.where(s.hostsubhalos[s.species_start[0] + indFilt] == s.cat.group_firstsub[group_id])[0]
            indFilt = indFilt[curind]
            #indUF = indUF[curind] ## If I do this, I need to do the volume correction for binvol
        if filter_type is not "none":
            if verbose: print "Filtering..."
            import scaling.xray_python.xray as xray
            curind = xray.xray_source(z, s.header.hubble, omega_m).filter_phase_space(s.temp[indFilt], s.gasrho[indFilt], filter_type=filter_type, M500=M500, R500=R500)
            indFilt = indFilt[curind]
        if temp_thresh > 0.0:
            if verbose: print "Applying temperature threshold..."
            assert T200 > 0.0, "Need T200 > 0 but have T200={:.1f} keV".format(T200)
            curind = np.where(s.temp[indFilt] < temp_thresh*T200)[0]
            indFilt = indFilt[curind]
        
        if verbose: print "Calculating gas profiles..."
        binnumsFilt, bin_edges = np.histogram(r[indFilt], bins=binleft)
        binnumsUF, bin_edges = np.histogram(r[indUF], bins=binleft)
        Filt_nonzero = (binnumsFilt > 0.0)
        UF_nonzero = (binnumsUF > 0.0)

        if incl_wind:
            gasmbinsumFilt, bin_edges = np.histogram(np.concatenate((r[indFilt], rwind[indwind])), bins=binleft, weights=np.concatenate((gasmass[indFilt], s.windmass[indwind]))) ## 1e10 Msun/h
            gasmbinsumUF, bin_edges = np.histogram(np.concatenate((r[indUF], rwind[indwind])), bins=binleft, weights=np.concatenate((gasmass[indUF], s.windmass[indwind]))) ## 1e10 Msun/h
            windmbinsum, bin_edges = np.histogram(rwind[indwind], bins=binleft, weights=s.windmass[indwind]) ## 1e10 Msun/h
        else:
            gasmbinsumFilt, bin_edges = np.histogram(r[indFilt], bins=binleft, weights=gasmass[indFilt]) ## 1e10 Msun/h
            gasmbinsumUF, bin_edges = np.histogram(r[indUF], bins=binleft, weights=gasmass[indUF]) ## 1e10 Msun/h
            windmbinsum = np.zeros_like(gasmbinsumFilt)
            
        ### Correct bin volumes by the volume excluded by filters
        volTot = np.histogram(r, bins=binleft, weights = gasmass / s.gasrho)[0]## sum of volumes of all cells in each bin
        volFilt = np.histogram(r[indFilt], bins=binleft, weights = gasmass[indFilt] / s.gasrho[indFilt])[0]## sum of volumes of filtered cells
        volmask = ((volFilt > 0.0) & (volTot > 0.0)) ## to avoid multiplying vol by zeroes or nans
        binvolFilt = np.copy(binvolTot)
        binvolFilt[volmask] = binvolTot[volmask] * volFilt[volmask] / volTot[volmask] ## correct actual bin vol by ratio filtered/all
        
        if rho_weighting=="mass":
            ## this is actually volume-weighted density because i'm simply summing the mass in each bin and dividing by the bin volume, hence cells with a larger volume contribute more to the density in a given bin
            ## Instead I could average the density of cells in the bin but cells with v.small volume (high rho) greatly bias the average
            mbindensFilt = gasmbinsumFilt / binvolFilt ## 1e10 Msun/h (ckpc/h)^-3 = 1e10 h^2 Msun ckpc^-3
            mbindensUF = gasmbinsumUF / binvolTot ## 1e10 Msun/h (ckpc/h)^-3 = 1e10 h^2 Msun ckpc^-3
            hfac.rho = 2

            nebinmeanFilt, bin_edges = np.histogram(r[indFilt], bins=binleft, weights=gasmass[indFilt] * s.ne[indFilt]) ## mass * no. electrons per H
            nebinmeanFilt *= Xh * UnitMass_in_g / (PROTONMASS + ELECTRONMASS) / binvolFilt / UnitLength_in_cm**3 ## electron number density, h^2 cm^-3 comoving
            nebinmeanUF, bin_edges = np.histogram(r[indUF], bins=binleft, weights=gasmass[indUF] * s.ne[indUF]) ## mass * no. electrons per H
            nebinmeanUF *= Xh * UnitMass_in_g / (PROTONMASS + ELECTRONMASS) / binvolTot / UnitLength_in_cm**3 ## electron number density, h^2 cm^-3 comoving
            hfac.ne = 2
            
            assert maxrad > R500
            ind = np.intersect1d(indFilt, np.where(r < R500)[0], assume_unique=True)
            rho500Filt = np.sum(gasmass[ind]) / (4.0/3.0*np.pi*R500**3) ## avg gas density in R500, 1e10 Msun/h (ckpc/h)^-3
            ne500Filt = np.sum(gasmass[ind] * s.ne[ind]) / (4.0/3.0*np.pi*R500**3) * Xh * UnitMass_in_g / (PROTONMASS + ELECTRONMASS) / UnitLength_in_cm**3 ## electron number density, 1.0/h (cm/h)^-3 comoving
            ind = np.intersect1d(indUF, np.where(r < R500)[0], assume_unique=True)
            rho500UF = np.sum(gasmass[ind]) / (4.0/3.0*np.pi*R500**3) ## avg gas density in R500, 1e10 Msun/h (ckpc/h)^-3
            ne500UF = np.sum(gasmass[ind] * s.ne[ind]) / (4.0/3.0*np.pi*R500**3) * Xh * UnitMass_in_g / (PROTONMASS + ELECTRONMASS) / UnitLength_in_cm**3 ## electron number density, 1.0/h (cm/h)^-3 comoving
        elif rho_weighting=="EM":
            ## I guess this is actually mass-weighting not EM-weighted since i'm doing the same as above (volume-weighted) but weighting cells by their density
            if incl_wind:
                print "\033[31m", "WARNING: Wind not included for EM weighting", "\033[0m" ## since wind particles have no 'gas density'
            mbindensFilt, bin_edges = np.histogram(r[indFilt], bins=binleft, weights=gasmass[indFilt] * s.gasrho[indFilt])## (1e10 Msun/h)^2 (ckpc/h)^-3
            mbindensFilt = (mbindensFilt / binvolFilt)**0.5 ## 1e10 h^2 Msun ckpc^-3
            mbindensUF, bin_edges = np.histogram(r[indUF], bins=binleft, weights=gasmass[indUF] * s.gasrho[indUF])## (1e10 Msun/h)^2 (ckpc/h)^-3
            mbindensUF = (mbindensUF / binvolTot)**0.5 ## 1e10 h^2 Msun ckpc^-3
            hfac.rho = 2
            
            nebinmeanFilt, bin_edges = np.histogram(r[indFilt], bins=binleft, weights=gasmass[indFilt] * s.ne[indFilt] * s.gasrho[indFilt]) ## mass * no. electrons per H
            nebinmeanFilt = (nebinmeanFilt / binvolFilt)**0.5 ## 1e10 h^2 Msun ckpc^-3
            nebinmeanFilt *= Xh * UnitMass_in_g / (PROTONMASS + ELECTRONMASS) / UnitLength_in_cm**3 ## electron number density, h^2 cm^-3 comoving
            nebinmeanUF, bin_edges = np.histogram(r[indUF], bins=binleft, weights=gasmass[indUF] * s.ne[indUF] * s.gasrho[indUF]) ## mass * no. electrons per H
            nebinmeanUF = (nebinmeanUF / binvolTot)**0.5 ## 1e10 h^2 Msun ckpc^-3
            nebinmeanUF *= Xh * UnitMass_in_g / (PROTONMASS + ELECTRONMASS) / UnitLength_in_cm**3 ## electron number density, h^2 cm^-3 comoving
            hfac.ne = 2
            
            assert maxrad > R500
            ind = np.intersect1d(indFilt, np.where(r < R500)[0], assume_unique=True)
            rho500Filt = (np.sum(gasmass[ind] * s.gasrho[ind]) / (4.0/3.0*np.pi*R500**3))**0.5 ## avg gas density in R500, 1e10 h^2 Msun ckpc^-3
            ne500Filt = (np.sum(gasmass[ind] * s.ne[ind] * s.gasrho[ind]) / (4.0/3.0*np.pi*R500**3))**0.5 * Xh * UnitMass_in_g / (PROTONMASS + ELECTRONMASS) / UnitLength_in_cm**3 ## electron number density, 1.0/h (cm/h)^-3
            ind = np.intersect1d(indUF, np.where(r < R500)[0], assume_unique=True)
            rho500UF = (np.sum(gasmass[ind] * s.gasrho[ind]) / (4.0/3.0*np.pi*R500**3))**0.5 ## avg gas density in R500, 1e10 h^2 Msun ckpc^-3
            ne500UF = (np.sum(gasmass[ind] * s.ne[ind] * s.gasrho[ind]) / (4.0/3.0*np.pi*R500**3))**0.5 * Xh * UnitMass_in_g / (PROTONMASS + ELECTRONMASS) / UnitLength_in_cm**3 ## electron number density, 1.0/h (cm/h)^-3
        else:
            assert False, "rho_weighting must be 'mass' or 'EM'"
            
        ## Calculate mean weighted temperature profile
        if T_weighting == "EM":
            w = s.gasrho * gasmass ## Emission-measure weighted (volume-averaged with weight w = rho^2)
        elif T_weighting == "brem":
            w = s.gasrho * gasmass * np.sqrt(s.temp) ## Bremsstrahlung emission-measure weighted (volume-averaged with weight w = rho^2 * sqrt(T))
        elif T_weighting == "mass": ## Mass weighted
            w = np.copy(gasmass)
        elif T_weighting == "spec": ## Mazzotta et al 2004 spectral like temperature
            w = s.gasrho * gasmass / s.temp**0.75
        elif T_weighting == "E": ## emission-weighted using flux extracted from MEKAL tables so includes line emission
            abund = np.full_like(s.temp, 0.3) ## Assuming 0.3 solar metallicity
            ag_hydrogen = 0.70683 # Anders & Grevesse solar mass fractions 
            ag_helium = 0.27431
            ag_metals = 0.01886
            f = 0.76
            fH = ag_hydrogen / (ag_hydrogen + ag_helium + abund * ag_metals) #Hydrogen mass fraction = 0.716 for abund=0.3
            nelec = 1.0 + 0.5*ag_helium/ag_hydrogen + 0.5*abund*ag_metals/ag_hydrogen
            nelec *= s.ne / (1.0+(1.0-f)/(2.0*f))
            
            fluxdir="/home/nh444/scripts/scaling/flux_tables_mekal_63/"
            zdata = np.loadtxt(fluxdir+"redshifts.txt")
            fluxnum = zdata[np.argmin(np.absolute(zdata[:,1] - z)), 0].astype(int)
            fluxfilename = fluxdir+"fluxTable_mekal_"+str(fluxnum).zfill(3)+".d"
            with open(fluxfilename) as f:
                for i, line in enumerate(f):
                    if "abund. num. =" in line:
                        abundnum = int(line.split()[-1])
                    if "min. abund. =" in line:
                        minabund = float(line.split()[-1])
                    if "max. abund. =" in line:
                        maxabund = float(line.split()[-1])
                    if "temp. num. =" in line:
                        tempnum = int(line.split()[-1])
                    if "min. temp. =" in line:
                        mintemp = float(line.split()[-1])
                    if "max. temp. =" in line:
                        maxtemp = float(line.split()[-1])
                    if i>28: break ## no need to read past here
            fluxdata = np.loadtxt(fluxfilename, skiprows=30, usecols=[7])
            lower_temp = np.floor((s.temp-mintemp)/((maxtemp-mintemp)/(tempnum-1))).astype(int)
            lower_temp[lower_temp <= 0] = 0
            lower_temp[lower_temp >= tempnum-1] = tempnum-1
            d_temp = (s.temp-mintemp)/((maxtemp-mintemp)/(tempnum-1)) - lower_temp
            d_temp[(lower_temp <= 0) | (lower_temp >= tempnum-1)] = 0
            upper_temp = np.copy(lower_temp)
            upper_temp[(lower_temp<tempnum-1)] += 1
            lower_abund = np.floor((abund-minabund)/((maxabund-minabund)/(abundnum-1))).astype(int)
            lower_abund[lower_abund <= 0] = 0
            lower_abund[lower_abund >= abundnum-1] = abundnum-1
            d_abund = ((abund-minabund)/((maxabund-minabund)/(abundnum-1))) - lower_abund
            d_abund[(lower_abund <= 0) | (lower_abund >= abundnum-1)] = 0
            upper_abund = np.copy(lower_abund)
            upper_abund[(lower_abund<abundnum-1)] += 1
            flux = fluxdata[lower_abund*tempnum+lower_temp]*(1.0-d_abund)*(1.0-d_temp) + \
                    fluxdata[upper_abund*tempnum+lower_temp]*(d_abund)*(1.0-d_temp) + \
                    fluxdata[lower_abund*tempnum+upper_temp]*(1.0-d_abund)*(d_temp) + \
                    fluxdata[upper_abund*tempnum+upper_temp]*(d_abund)*(d_temp)
            w = gasmass*s.gasrho*flux*fH*fH*nelec ## flux from table (norm=1) times norm (i.e. emission measure)
            print "kT =", s.temp
            print "lower_temp =", lower_temp
            print "upper_temp =", upper_temp
            print "d_temp =", d_temp
            print "lower_abund =", lower_abund
            print "upper_abund =", upper_abund
            print "d_abund =", d_abund
            print "fluxdata[lower_abund*1024+lower_temp] =", fluxdata[lower_abund*1024+lower_temp]
            print "flux =", flux
        else:
            assert False, "T_weighting must be one of 'mass', 'EM' (emission measure), 'brem' (bremsstrahlung), 'E' (total emission) or 'spec' (Mazzotta+2004 spec-like)"
        
        wbinsumFilt, bin_edges = np.histogram(r[indFilt], bins=binleft, weights=w[indFilt])
        TbinmeanFilt, bin_edges = np.histogram(r[indFilt], bins=binleft, weights=s.temp[indFilt] * w[indFilt])        
        TbinmeanFilt[Filt_nonzero] /= wbinsumFilt[Filt_nonzero]
        wbinsumUF, bin_edges = np.histogram(r[indUF], bins=binleft, weights=w[indUF])
        TbinmeanUF, bin_edges = np.histogram(r[indUF], bins=binleft, weights=s.temp[indUF] * w[indUF])        
        TbinmeanUF[UF_nonzero] /= wbinsumUF[UF_nonzero]
                
        ## Calculate pressure profile
        UbinmeanFilt, bin_edges = np.histogram(r[indFilt], bins=binleft, weights=gasmass[indFilt] * s.u[indFilt]) ## total internal energy, keV
        PbinmeanFilt = (2.0/3.0 * UbinmeanFilt * UnitMass_in_g * 1.0e3 /
                    (binvolFilt * UnitLength_in_cm**3) / (1000.0 * eV))  # in keV h^2 cm^-3 (u, and thus T, have no h dependence so U=u*m is h^-1)
        UbinmeanUF, bin_edges = np.histogram(r[indUF], bins=binleft, weights=gasmass[indUF] * s.u[indUF]) ## total internal energy, keV
        PbinmeanUF = (2.0/3.0 * UbinmeanUF * UnitMass_in_g * 1.0e3 /
                    (binvolTot * UnitLength_in_cm**3) / (1000.0 * eV))  # in keV h^2 cm^-3 (u, and thus T, have no h dependence so U=u*m is h^-1)
        hfac.P = 2
        
        ##### Calculate temperatures in various apertures #####
        assert maxrad > R500
        ind = np.intersect1d(indFilt, np.where(r < R500)[0], assume_unique=True)
        T500Filt = np.sum(s.temp[ind] * w[ind]) / np.sum(w[ind])
        ind = np.intersect1d(indUF, np.where(r < R500)[0], assume_unique=True)
        T500UF = np.sum(s.temp[ind] * w[ind]) / np.sum(w[ind])
        ind = np.intersect1d(indFilt, np.where(r < R2500)[0], assume_unique=True)
        T2500Filt = np.sum(s.temp[ind] * w[ind]) / np.sum(w[ind])
        ind = np.intersect1d(indUF, np.where(r < R2500)[0], assume_unique=True)
        T2500UF = np.sum(s.temp[ind] * w[ind]) / np.sum(w[ind])
        
        ## Calculate T2500 in cylindrical aperture with projected radius from 0.15*R500 (not R2500) to R2500 along the LOS between 0.15*R500 to 1.6*R500 as in Sun+2009
        ## WARNING: The outer radius along LOS (1.6*R500) is unimportant for emission weighting (similar to R2500)
        ## but for mass weighting T2500 is biased too low so I also calculate T2500 within a spherical aperture of radius 0.15*R500 to R2500
        r2D = ((s.rel_pos(gaspos[:,0], centre[0]))**2 + (s.rel_pos(gaspos[:,1], centre[1]))**2)**0.5
        rz = np.absolute(s.rel_pos(gaspos[:,2], centre[2]))
        ind = np.intersect1d(indFilt, np.where((r2D >= 0.15*R500) & (r2D <= R2500) & (rz >= 0.15*R500) & (rz <= 1.6*R500))[0], assume_unique=True)
        T2500_Sun_Filt = np.sum(s.temp[ind] * w[ind]) / np.sum(w[ind])
        ind = np.intersect1d(indUF, np.where((r2D >= 0.15*R500) & (r2D <= R2500) & (rz >= 0.15*R500) & (rz <= 1.6*R500))[0], assume_unique=True)
        T2500_Sun_UF = np.sum(s.temp[ind] * w[ind]) / np.sum(w[ind])
        ## spherical aperture:
        ind = np.intersect1d(indFilt, np.where((r >= 0.15*R500) & (r <= R2500))[0], assume_unique=True)
        T2500_Sun_Filt_sph = np.sum(s.temp[ind] * w[ind]) / np.sum(w[ind])
        ind = np.intersect1d(indUF, np.where((r >= 0.15*R500) & (r <= R2500))[0], assume_unique=True)
        T2500_Sun_UF_sph = np.sum(s.temp[ind] * w[ind]) / np.sum(w[ind])
        ## Similarly calculate T500 as in Sun+2009
        ind = np.intersect1d(indFilt, np.where((r2D >= 0.15*R500) & (r2D <= R500) & (rz >= 0.15*R500) & (rz <= 1.6*R500))[0], assume_unique=True)
        T500_Sun_Filt = np.sum(s.temp[ind] * w[ind]) / np.sum(w[ind])
        ind = np.intersect1d(indUF, np.where((r2D >= 0.15*R500) & (r2D <= R500) & (rz >= 0.15*R500) & (rz <= 1.6*R500))[0], assume_unique=True)
        T500_Sun_UF = np.sum(s.temp[ind] * w[ind]) / np.sum(w[ind])
        
        ## Mean temperature in specified aperture
        assert minrad <= mean_bins[0]
        assert maxrad >= mean_bins[1]
        ind = np.intersect1d(indFilt, np.where((r > mean_bins[0]) & (r < mean_bins[1]))[0], assume_unique=True)
        TmeanFilt = np.sum(s.temp[ind] * w[ind]) / np.sum(w[ind])
        ind = np.intersect1d(indUF, np.where((r > mean_bins[0]) & (r < mean_bins[1]))[0], assume_unique=True)
        TmeanUF = np.sum(s.temp[ind] * w[ind]) / np.sum(w[ind])

        ## Calculate TX in aperture of projected radius 0.15-0.75*R500 as in Arnaud+2010
        ind = np.intersect1d(indFilt, np.where((r2D >= 0.15*R500) & (r2D <= 0.75*R500))[0], assume_unique=True)
        TX_Filt = np.sum(s.temp[ind] * w[ind]) / np.sum(w[ind])
        ind = np.intersect1d(indUF, np.where((r2D >= 0.15*R500) & (r2D <= 0.75*R500))[0], assume_unique=True)
        TX_UF = np.sum(s.temp[ind] * w[ind]) / np.sum(w[ind])
        
        ## Calculate entropy profile
        SbinmeanFilt = np.zeros_like(TbinmeanFilt)
        SbinmeanFilt[Filt_nonzero] = TbinmeanFilt[Filt_nonzero] / pow(nebinmeanFilt[Filt_nonzero], 2.0/3.0) ## keV / (h^2 cm^-3)**2/3 (comoving)
        SbinmeanUF = np.zeros_like(TbinmeanUF)
        SbinmeanUF[UF_nonzero] = TbinmeanUF[UF_nonzero] / pow(nebinmeanUF[UF_nonzero], 2.0/3.0) ## keV / (h^2 cm^-3)**2/3 (comoving)
        hfac.S = hfac.T + pow(hfac.ne, -2.0/3.0)
        
        ### Convert to units of Msun/h and proper kpc/h
        units = collections.namedtuple('units', 'dist, mass, bins, vol, \
                              gasmbinsum, mbindens, Tbinmean, \
                              Sbinmean, nebinmean, Pbinmean, totmbinsum, windmbinsum')
        
        R500 *= time ## time = scale factor = 1/(1+z) to convert comoving to physical coords
        M500 *= UnitMass_in_Msun ## Msun/h
        R2500 *= time 
        M2500 *= UnitMass_in_Msun ## Msun/h
        units.dist = "pkpc/h"
        units.mass = "Msun/h"
        
        binleft *= time ## pkpc/h
        bincentre *= time ## pkpc/h
        units.bins = "pkpc/h"
        binvolFilt *= time**3 ## (pkpc/h)^3
        binvolTot *= time**3 ## (pkpc/h)^3
        units.vol = "h-3 pkpc3"
        
        gasmbinsumFilt *= UnitMass_in_Msun ## Msun/h
        gasmbinsumUF *= UnitMass_in_Msun ## Msun/h
        units.gasmbinsum = "Msun/h"
        totmbinsum *= UnitMass_in_Msun ## Msun/h
        units.totmbinsum = "Msun/h"
        windmbinsum *= UnitMass_in_Msun ## Msun/h
        units.windmbinsum = "Msun/h"
        
        mbindensFilt *= UnitMass_in_Msun / time**3 ## Msun h^2 pkpc^-3
        mbindensUF *= UnitMass_in_Msun / time**3 ## Msun h^2 pkpc^-3
        rho500Filt *= UnitMass_in_Msun / time**3 ## Msun h^2 pkpc^-3
        rho500UF *= UnitMass_in_Msun / time**3 ## Msun h^2 pkpc^-3
        units.mbindens = "Msun h2 pkpc-3"
        
        units.Tbinmean = "keV"
        
        nebinmeanFilt /= time**3 ## h^2 cm^-3
        nebinmeanUF /= time**3 ## h^2 cm^-3
        ne500Filt /= time**3 ## h^2 cm^-3
        ne500UF /= time**3 ## h^2 cm^-3
        units.nebinmean = "h2 cm-3"
        
        SbinmeanFilt *= time**2 ## keV h^-4/3 cm^2
        SbinmeanUF *= time**2 ## keV h^-4/3 cm^2
        units.Sbinmean = "keV h-4/3 cm2"
        PbinmeanFilt /= time**3 ## keV h^2 cm^-3
        PbinmeanUF /= time**3 ## keV h^2 cm^-3
        units.Pbinmean = "keV h2 cm-3"
        
        Data = collections.namedtuple('Data', 'binleft, bincentre, binright, \
                                      binvolFilt, binvolTot\
                                      binnumsFilt, binnumsUF, gasmbinsumFilt, gasmbinsumUF, \
                                      mbindensFilt, mbindensUF, nebinmeanFilt, nebinmeanUF, \
                                      TbinmeanFilt, TbinmeanUF, SbinmeanFilt, SbinmeanUF, \
                                      PbinmeanFilt, PbinmeanUF, totmbinsum, windmbinsum')
        data = Data(binleft[:-1], bincentre[:-1], binleft[1:], binvolFilt, binvolTot,
                    binnumsFilt, binnumsUF, gasmbinsumFilt, gasmbinsumUF,
                    mbindensFilt, mbindensUF, nebinmeanFilt, nebinmeanUF,
                    TbinmeanFilt, TbinmeanUF, SbinmeanFilt, SbinmeanUF,
                    PbinmeanFilt, PbinmeanUF, totmbinsum, windmbinsum)

        #return (data, units, hfac, R500, M500, R2500, M2500, T500, T2500, T500_Sun, T2500_Sun, Tmean, TX, rho500Filt, rho500UF, ne500Filt, ne500UF)
        Globval = collections.namedtuple('Globval', 'R500, M500, R2500, M2500, \
                                         T500Filt, T2500Filt, T500_Sun_Filt, T2500_Sun_Filt, T2500_Sun_Filt_sph, TmeanFilt, TX_Filt, \
                                         T500UF, T2500UF, T500_Sun_UF, T2500_Sun_UF, T2500_Sun_UF_sph, TmeanUF, TX_UF, \
                                         rho500Filt, rho500UF, ne500Filt, ne500UF')
        globval = Globval(R500, M500, R2500, M2500, \
                                         T500Filt, T2500Filt, T500_Sun_Filt, T2500_Sun_Filt, T2500_Sun_Filt_sph, TmeanFilt, TX_Filt,
                                         T500UF, T2500UF, T500_Sun_UF, T2500_Sun_UF, T2500_Sun_UF_sph, TmeanUF, TX_UF,
                                         rho500Filt, rho500UF, ne500Filt, ne500UF)
        return (data, units, hfac, globval)
            

## --------------------------- DATA OUTPUT --------------------------- ##
def radprof_targets (target_list, snapshot=None, snapfile=None, bin_ratio=1.6,
                     nbins=0, outdir="./", suffix="",
                     main_halo_only=False, temp_thresh=-1,
                     plot=False, stack=True, title="",
                     masstab=False, mpc_units=None,
                     filter_type="Rasia", incl_wind=True,
                     T_weighting="mass", rho_weighting="mass",
                     use_kdtree=False):
    """
    This function accepts a list of group targets (group_id, xcentre, ycentre, zcentre, minrad, maxrad) in ckpc/h
    calculates the radial profiles for them and saves the result in a HDF5 file.
    """

    ## snapshot - snap.py snapshot class object
    ## snapfile - name of snapshot file if snapshot=None
    ## target_list - txt file or array with [group_id, xcentre, ycentre, zcentre, minrad, maxrad] for each target
    ## nbins - no. of logarithmic bins
    ## outdir - path to directory to contain output files
    ## plot - if True plots will be made in output directory
    ## metadata - list of dictionaries containing {Length, Mass, M500, R500, M200, R200, SofteningGas} for each object
    ## stack - If true all plots are put on the same figure
    ## title - title of stacked plot, if plotting
    
    import h5py
    import numpy as np
    import sys, os
    import snap

    if (type(target_list) is str):#if name of file
        if os.path.exists(target_list):
            targets = np.genfromtxt(target_list)
        else:
            print "Target file", target_list, "not found."
            sys.exit()
    else:#if array
        targets = np.transpose(np.asarray(target_list))
        
    if snapshot is not None:
        snapnum = snapshot.snapnum
    elif snapfile is not None:
        basedir, snapnum = split_filename(snapfile)
    else:
        assert False, "Must have snapshot or snapfile."
        
    if suffix:
        filename = "snap_"+str(snapnum).zfill(3)+"_profiles_"+suffix+".hdf5"
    else:
        filename = "snap_"+str(snapnum).zfill(3)+"_profiles.hdf5"
    if outdir != "./":
        outdir = os.path.normpath(outdir)+"/"
        if not (os.path.exists(outdir)):
            os.makedirs(outdir)
    
    if snapshot is None:
        if snapfile is not None:
            if mpc_units is None:
                basedir, snapnum = split_filename(snapfile)
                UnitLength_in_cm, UnitMass_in_g, UnitVelocity_in_cm_per_s = read_params(basedir, mpc_units)
                if 999 < UnitLength_in_cm/3.085678e21 < 1001:
                    mpc_units=True
                elif 0.99 < UnitLength_in_cm/3.085678e21 < 1.01:
                    mpc_units=False
                else:
                    assert False, "UnitLength_in_cm "+str(UnitLength_in_cm)+" from parameter file is neither kpc or Mpc"
            basedir, snapnum = split_filename(snapfile)
            sim = snap.snapshot(basedir, snapnum,
                              masstab=masstab, mpc_units=mpc_units)
        else:
            assert False, "Must give either snapshot object or snapshot file name"
    else:
        sim = snapshot
            
    h = sim.header.hubble
    boxsize = sim.header.boxsize
    z = sim.header.redshift
    
    ids = np.array(targets[:,0], dtype=int) ## Group ids
    centre = targets[:,1:4] ## raprof_calc takes ckpc/h
    minrad = targets[:,4] ## ckpc/h
    maxrad = targets[:,5] ## ckpc/h
    
    if use_kdtree:
        from scipy.spatial import cKDTree
        sim.load_pos()
        assert sim.header.massarr[1] > 0.0
        leafsize = np.min(sim.cat.group_m_crit500[ids]) / sim.header.massarr[1]
        ## set leafsize to approx number of particles in the lowest mass halo of interest
        kdtree = cKDTree(sim.pos, leafsize=leafsize, boxsize=sim.box_sidelength)
    else:
        kdtree = None
    
    with h5py.File(outdir+filename, "w") as outfile:
        outfile.attrs['boxsize'] = boxsize
        outfile.attrs['omega_m'] = sim.header.omega_m
        outfile.attrs['omega_l'] = sim.header.omega_l
        outfile.attrs['HubbleParam'] = h
        outfile.attrs['redshift'] = z
        outfile.attrs['filter_type'] = filter_type
        outfile.attrs['main_halo_only'] = main_halo_only
        outfile.attrs['temp_thresh'] = temp_thresh
        outfile.attrs['incl_wind'] = incl_wind
            
        for i in range (0, len(ids)):
            mean_bins = [0.1*sim.cat.group_r_crit500[ids[i]], 0.3*sim.cat.group_r_crit500[ids[i]]] ## inner and outer radii of bin for mean temperature
            
            d, units, hfac, gv = radprof_calc(centre[i],
                                                    minrad[i], maxrad[i], 
                                                    snapshot=sim, kdtree=kdtree,
                                                    nbins=nbins, bin_ratio=bin_ratio, mean_bins=mean_bins,
                                                    group_id=ids[i],
                                                    masstab=masstab, mpc_units=mpc_units,
                                                    filter_type=filter_type, incl_wind=incl_wind,
                                                    main_halo_only=main_halo_only, temp_thresh=temp_thresh,
                                                    T_weighting=T_weighting, rho_weighting=rho_weighting)
            

            group = outfile.create_group("Group_"+str(int(ids[i])).zfill(2))
            bins = group.create_dataset('bins', data = np.c_[d.binleft, d.bincentre, d.binright])
            binvolFilt = group.create_dataset('binvolFilt', data=np.c_[d.binvolFilt])
            binvolUF = group.create_dataset('binvolUF', data=np.c_[d.binvolTot]) ## atm this is just the total volume, not taking into account "main_halo_only"
            number_Filt = group.create_dataset('number_Filt', data = np.c_[d.binnumsFilt])
            number_UnFilt = group.create_dataset('number_UnFilt', data = np.c_[d.binnumsUF])
            gasmassFilt = group.create_dataset('gasmassFilt', data = np.c_[d.gasmbinsumFilt])
            gasmassUnFilt = group.create_dataset('gasmassUnFilt', data = np.c_[d.gasmbinsumUF])
            wind_mass = group.create_dataset('wind_mass', data=np.c_[d.windmbinsum])
            densityFilt = group.create_dataset('densityFilt', data = np.c_[d.mbindensFilt])
            densityUnFilt = group.create_dataset('densityUnFilt', data = np.c_[d.mbindensUF])
            temperatureFilt = group.create_dataset('temperatureFilt', data = np.c_[d.TbinmeanFilt])
            temperatureUnFilt = group.create_dataset('temperatureUnFilt', data = np.c_[d.TbinmeanUF])
            entropyFilt = group.create_dataset('entropyFilt', data = np.c_[d.SbinmeanFilt])
            entropyUnFilt = group.create_dataset('entropyUnFilt', data = np.c_[d.SbinmeanUF])
            n_e_Filt = group.create_dataset('n_e_Filt', data = np.c_[d.nebinmeanFilt])
            n_e_UnFilt = group.create_dataset('n_e_UnFilt', data = np.c_[d.nebinmeanUF])
            pressureFilt = group.create_dataset('pressureFilt', data = np.c_[d.PbinmeanFilt])
            pressureUnFilt = group.create_dataset('pressureUnFilt', data = np.c_[d.PbinmeanUF])
            tot_mass = group.create_dataset('mass_all', data = np.c_[d.totmbinsum])
            
            group.attrs['MassUnits'] = units.mass
            group.attrs['LengthUnits'] = units.dist
            group.attrs['main_halo_only'] = main_halo_only
            group.attrs['filter_type'] = filter_type
            group.attrs['temp_thresh'] = temp_thresh
            group.attrs['incl_wind'] = incl_wind
            
            bins.attrs['units'] = units.bins
            bins.attrs['hfac'] = hfac.dist
            binvolFilt.attrs['units'] = units.vol
            binvolUF.attrs['units'] = units.vol
            number_UnFilt.attrs['main_halo_only'] = main_halo_only
            number_Filt.attrs['main_halo_only'] = main_halo_only
            number_Filt.attrs['filter_type'] = filter_type
            number_Filt.attrs['temp_thresh'] = temp_thresh
            
            gasmassFilt.attrs['units'] = units.gasmbinsum
            gasmassFilt.attrs['hfac'] = hfac.mass
            gasmassUnFilt.attrs['units'] = units.gasmbinsum
            gasmassUnFilt.attrs['hfac'] = hfac.mass
            tot_mass.attrs['units'] = units.totmbinsum
            tot_mass.attrs['hfac'] = hfac.mass
            wind_mass.attrs['units'] = units.windmbinsum
            wind_mass.attrs['hfac'] = hfac.mass
            
            densityFilt.attrs['weighting'] = rho_weighting
            densityFilt.attrs['units'] = units.mbindens
            densityFilt.attrs['hfac'] = hfac.rho
            densityUnFilt.attrs['weighting'] = rho_weighting
            densityUnFilt.attrs['units'] = units.mbindens
            densityUnFilt.attrs['hfac'] = hfac.rho
            
            temperatureFilt.attrs['weighting'] = T_weighting
            temperatureFilt.attrs['units'] = units.Tbinmean
            temperatureUnFilt.attrs['weighting'] = T_weighting
            temperatureUnFilt.attrs['units'] = units.Tbinmean
            entropyFilt.attrs['units'] = units.Sbinmean
            entropyFilt.attrs['hfac'] = hfac.S
            entropyUnFilt.attrs['units'] = units.Sbinmean
            entropyUnFilt.attrs['hfac'] = hfac.S
            
            n_e_Filt.attrs['weighting'] = rho_weighting
            n_e_Filt.attrs['units'] = units.nebinmean
            n_e_Filt.attrs['hfac'] = hfac.ne
            n_e_UnFilt.attrs['weighting'] = rho_weighting
            n_e_UnFilt.attrs['units'] = units.nebinmean
            n_e_UnFilt.attrs['hfac'] = hfac.ne
            
            pressureFilt.attrs['units'] = units.Pbinmean
            pressureFilt.attrs['hfac'] = hfac.P
            pressureUnFilt.attrs['units'] = units.Pbinmean
            pressureUnFilt.attrs['hfac'] = hfac.P
            
            group.attrs['boxsize'] = sim.header.boxsize
            group.attrs['omega_m'] = sim.header.omega_m
            group.attrs['omega_l'] = sim.header.omega_l
            group.attrs['HubbleParam'] = h
            group.attrs['redshift'] = z
            group.attrs['centre'] = centre[i]
            group.attrs['minrad'] = minrad[i]
            group.attrs['maxrad'] = maxrad[i]

            group.attrs['Length'] = sim.cat.group_len[ids[i]]
            group.attrs['Mass'] = sim.cat.group_mass[ids[i]] * 1e10
            group.attrs['M500'] = gv.M500
            group.attrs['R500'] = gv.R500
            group.attrs['M2500'] = gv.M2500
            group.attrs['R2500'] = gv.R2500
            if hasattr(sim.cat, "group_m_crit200"):
                group.attrs['M200'] = sim.cat.group_m_crit200[ids[i]] * 1e10
                group.attrs['R200'] = sim.cat.group_r_crit200[ids[i]] / (1+z) ## pkpc/h
                
            
            group.attrs['T500Filt'] = gv.T500Filt
            group.attrs['T2500Filt'] = gv.T2500Filt
            group.attrs['T500_Sun_Filt'] = gv.T500_Sun_Filt
            group.attrs['T2500_Sun_Filt'] = gv.T2500_Sun_Filt
            group.attrs['T2500_Sun_Filt_sph'] = gv.T2500_Sun_Filt_sph
            group.attrs['TmeanFilt'] = gv.TmeanFilt
            group.attrs['TX_Filt'] = gv.TX_Filt
            
            group.attrs['T500UnFilt'] = gv.T500UF
            group.attrs['T2500UnFilt'] = gv.T2500UF
            group.attrs['T500_Sun_UnFilt'] = gv.T500_Sun_UF
            group.attrs['T2500_Sun_UnFilt'] = gv.T2500_Sun_UF
            group.attrs['T2500_Sun_UF_sph'] = gv.T2500_Sun_UF_sph
            group.attrs['TmeanUnFilt'] = gv.TmeanUF
            group.attrs['TX_UnFilt'] = gv.TX_UF
            
            group.attrs['rho500Filt'] = gv.rho500Filt
            group.attrs['rho500UnFilt'] = gv.rho500UF
            group.attrs['ne500Filt'] = gv.ne500Filt
            group.attrs['ne500UnFilt'] = gv.ne500UF
    
            print "Profile "+str(i+1)+" of "+str(len(ids))+" (Group "+str(int(ids[i]))+") done."
    
        if plot:
            print "Plotting..."
            radprof_plot(str(outfile.filename), outdir=outdir, stack=stack,
                         redshift=z, title=title)
            
        print "Data saved as", outfile.filename

def radprof_halos (snapfile, N=0, bin_ratio=1.4, nbins=0, rmin=1e-3, rmax=2.0,
                   outdir="./", suffix="",
                   plot=False, stack=True, title="",
                   main_halo_only=False, temp_thresh=-1,
                   M500_min=1e3, M500_max=None,
                   mpc_units=None, masstab=False,
                   filter_type="Rasia", incl_wind=True,
                   T_weighting="mass", rho_weighting="mass"):

    ### This function will calculate density, temperature and entropy profiles
    ### for a) the N most massive halos in a given snapshot or b) if N=0 those within a given range of M500
    ## snapfile - path of snapshot file (string)
    ## fof_file - name of corresponding fof file - if 'default' will search same directory as snapshot
    ## N - N most massive halos to consider
    ## bin_ratio = ratio of r_out to _rin for each radial bin
    ## rmin, rmax = min. and max. radius to calculate profile in units of R500
    ## nbins - number of bins across radial range
    ## outdir - path of directory to place output files
    ## plot - if True, will run func radprof_plot and output pdf plot(s)
    ## stack - If stack, profiles are calculated in terms of normalised R/R_500
    ##        - If plot and stack, one stacked plot is produced, otherwise individual plots for each halo
    ## title - title of plot(s)
    ## mpc_units - In case the parameters-usedvalues file cannot be accessed this will be used to set UnitLength_in_cm to Mpc (True) or kpc (False), otherwise user is prompted
    ## M500_min - minimum M_500_crit for halo sample (1e10 Msun/h)
    
    import snap
    import numpy as np
    
    if M500_max==None:
        M500_max = np.inf
    
    basedir, snapnum = split_filename(snapfile)

    if mpc_units is None:
        UnitLength_in_cm, UnitMass_in_g, UnitVelocity_in_cm_per_s = read_params(basedir, mpc_units)
        if 0.99 < UnitLength_in_cm/3.085678e24 < 1.01:
            mpc_units=True
        elif 0.99 < UnitLength_in_cm/3.085678e21 < 1.01:
            mpc_units=False
        else:
            assert False, "UnitLength_in_cm "+str(UnitLength_in_cm)+" from parameter file is neither kpc or Mpc"
                    
    print "Loading snapshot..."
    sim = snap.snapshot(basedir, snapnum, masstab=masstab, mpc_units=mpc_units)
    
    ## If M_500 and R_500 not in snapshot object then calculate them
    if (hasattr(sim.cat, 'group_m_crit500') and hasattr(sim.cat, 'group_r_crit500')):
        M500c = sim.cat.group_m_crit500
        R500c = sim.cat.group_r_crit500
    else:
        print "WARNING: No M_500 or R_500 in halo file. Calculating..."
        M500c = np.zeros(sim.cat.ngroups)
        R500c = np.zeros(sim.cat.ngroups)
        ## Calculate M500 and R500 down to M500_min - assumes halo file is in mass order
        ## I should really correct this so that I only need to calculate it for
        ## the halos I'm using (as defined below)
        for groupnum in range(0, sim.cat.ngroups):
            m500c, r500c = sim.get_so_mass_rad(groupnum, 500, ref="crit")
            if N==0 and m500c < M500_min / 2: ## Calculate down to M500_min / 2
                break
            if N > 0 and groupnum > N + 5: ## Calculate down to N + 5
                break
            M500c[groupnum] = m500c
            R500c[groupnum] = r500c
        ## Save to snapshot object
        sim.cat.group_m_crit500 = M500c
        sim.cat.group_r_crit500 = R500c
          
    if N == 0:
        print "Filtering halos by", M500_min, "< M500 <", M500_max, "(1e10 Msun/h)"
        ## Sometimes a FoF group has abnormally high M500 and zero M200 - exclude them
        if hasattr(sim.cat, 'group_m_crit200'):
            ind = np.where((M500c > M500_min) & (M500c <= M500_max) & (sim.cat.group_m_crit200 > 0.0))
        else:
            ind = np.where((M500c > M500_min) & (M500c <= M500_max))
        print len(ind[0]), "halos found."
    elif N > 0:
        print "\nFiltering halos to", N, "most massive (M500)"
        if hasattr(sim.cat, 'group_m_crit200'):
            ind = M500c.argsort()[::-1] ## group indices in descending mass order
            ind = ind[sim.cat.group_m_crit200[ind] > 0.0] ## exclude those with M200=0
            ind = ind[:N] ## take N most massive
        else:
            ind = M500c.argsort()[-N:][::-1]
    else:
        assert False, "Invalid value of N = "+str(N)
               
    centre = sim.cat.group_pos[ind]
    maxrad = rmax * R500c[ind] ## in ckpc/h
    minrad = rmin * R500c[ind] ## in ckpc/h

    targets = np.vstack((np.array(ind, dtype=float), centre[:,0], centre[:,1], centre[:,2], minrad, maxrad))

    radprof_targets(targets, snapshot=sim, bin_ratio=bin_ratio, nbins=nbins,
                    outdir=outdir, suffix=suffix,
                    plot=plot, stack=stack, title=title,
                    masstab=masstab, mpc_units=mpc_units,
                    filter_type=filter_type, incl_wind=incl_wind,
                    main_halo_only=main_halo_only, temp_thresh=temp_thresh,
                    T_weighting=T_weighting, rho_weighting=rho_weighting)
    
    return

### Read parameter file to get units
def read_params(dirname, mpc_units):
    import os, re

    UnitLength_in_cm = 3.085678e21 # 1.0 kpc
    UnitMass_in_g = 1.989e43 # 1.0e10 solar masses
    UnitVelocity_in_cm_per_s = 100000
    
    prompt = False
    if os.path.exists(dirname+"/parameters-usedvalues"):
        if os.access(dirname+"/parameters-usedvalues", os.R_OK):
            params = open(dirname+'/parameters-usedvalues')
            for line in params:
                if re.match('UnitLength_in_cm', line):
                    UnitLength_in_cm = float(line.strip().split()[1])
                if re.match('UnitMass_in_g', line):
                    UnitMass_in_g = float(line.strip().split()[1])
                if re.match('UnitVelocity_in_cm_per_s', line):
                    UnitVelocity_in_cm_per_s = float(line.strip().split()[1])
            params.close()
        else:
            print "WARNING: No read permission on "+dirname+"/parameters-usedvalues. Using default mass and velocity units."
            prompt = True
    else:
        print "WARNING: Could not find "+dirname+"/parameters-usedvalues. Using default mass and velocity units."
        prompt = True

    if mpc_units and prompt: ## If not params not read and mpc_units=True
        UnitLength_in_cm = 3.085678e24 # 1.0 Mpc
        prompt = False
    if not mpc_units:
        prompt = False
        
    while prompt:
        response = raw_input('Using Mpc units? [y/n]: ')
        if response == 'y':
            UnitLength_in_cm = 3.085678e24 # 1.0 Mpc
            prompt = False
        elif response == 'n':
            prompt = False
        else:
            print "Incorrect input format - Please enter 'y' or 'n'"

    return UnitLength_in_cm, UnitMass_in_g, UnitVelocity_in_cm_per_s
    
def split_filename(snapfile):
    import os
    
    basedir = os.path.dirname(snapfile)
    if (basedir == ""):
        basedir = "."
    snapnum = int(str(os.path.splitext(snapfile)[0])[-3:])
    
    return basedir, snapnum