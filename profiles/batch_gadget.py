import radprof as rp
import os
import h5py as h5
import numpy as np

#nbins = 20
bin_ratio=1.4
N = 1 #number of groups
mpc_units = True
masstab = True
combine = True
topdir = "/data/deboras/ewald/"
outdir = "/home/nh444/data/project1/L-T_relations/gadget/"
outfile_suffix = "_Massive_csf"

save_old = False

## names of directories containing the snapshot files
runs = ["m_585_h_486_2210_z3/csf", "m_585_h_486_2210_z3/csfbh",
	"m_717_h_497_3136_z3/csf", "m_717_h_497_3136_z3/csfbh",
	"m_878_h_466_1998_z3/csf", "m_878_h_466_1998_z3/csfbh",
	"m_1075_h_442_550_z3/csf", "m_1075_h_442_550_z3/csfbh",
	"m_1317_h_379_2174_z3/csf", "m_1317_h_379_2174_z3/csfbh",
	"m_1613_h_390_2864_z3/csf", "m_1613_h_390_2864_z3/csfbh",
	"m_1975_h_506_1096_z3/csf", "m_1975_h_506_1096_z3/csfbh",
	"m_2186_h_56_1789_z3/csf", "m_2186_h_56_1789_z3/csfbh",
	"m_2419_h_436_2408_z3/csf", "m_2419_h_436_2408_z3/csfbh",
	"m_2963_h_73_1513_z3/csf", "m_2963_h_73_1513_z3/csfbh",
	"m_3279_h_103_2600_z3/csf", "m_3279_h_103_2600_z3/csfbh",
	"m_3629_h_259_610_z3/csf", "m_3629_h_259_610_z3/csfbh",
	"m_4445_h_183_1161_z3/csf", "m_4445_h_183_1161_z3/csfbh"]

runs = ["m_2963_h_73_1513_z3/csf",
        "m_3279_h_103_2600_z3/csf",
        "m_3629_h_259_610_z3/csf",
        "m_4445_h_183_1161_z3/csf",
        "m_6664_h_48_194_z3/csf",
        "m_10002_h_94_501_z3/csf",
        "m_14999_h_355_1278_z3/csf"]
        #"m_22498_h_197_0_z2/csf",
        #"m_33617_h_146_0_z2/csf"]
'''
runs = ["m_2963_h_73_1513_z3/csfbh",
        "m_3279_h_103_2600_z3/csfbh",
        "m_3629_h_259_610_z3/csfbh",
        "m_4445_h_183_1161_z3/csfbh",
        "m_6664_h_48_194_z3/csfbh",
        "m_10002_h_94_501_z3/csfbh",
        "m_14999_h_355_1278_z3/csfbh",
        "m_22498_h_197_0_z2/csfbh",
        "m_33617_h_146_0_z2/csfbh"]


runs = ["m_585_h_486_2210_z3/csf",
	"m_717_h_497_3136_z3/csf",
	"m_878_h_466_1998_z3/csf",
	"m_1075_h_442_550_z3/csf",
	"m_1317_h_379_2174_z3/csf",
	"m_1613_h_390_2864_z3/csf",
	"m_1975_h_506_1096_z3/csf",
	"m_2186_h_56_1789_z3/csf",
	"m_2419_h_436_2408_z3/csf",
	"m_2963_h_73_1513_z3/csf",
	"m_3279_h_103_2600_z3/csf",
	"m_3629_h_259_610_z3/csf",
	"m_4445_h_183_1161_z3/csf"]

runs = ["m_585_h_486_2210_z3/csfbh",
	"m_717_h_497_3136_z3/csfbh",
	"m_878_h_466_1998_z3/csfbh",
	"m_1075_h_442_550_z3/csfbh",
	"m_1317_h_379_2174_z3/csfbh",
	"m_1613_h_390_2864_z3/csfbh",
	"m_1975_h_506_1096_z3/csfbh",
	"m_2186_h_56_1789_z3/csfbh",
	"m_2419_h_436_2408_z3/csfbh",
	"m_2963_h_73_1513_z3/csfbh",
	"m_3279_h_103_2600_z3/csfbh",
	"m_3629_h_259_610_z3/csfbh",
	"m_4445_h_183_1161_z3/csfbh"]
'''
## snapshot numbers
steps = [62]

for step in steps:
    if combine:
        outfile_name = outdir+"snap_"+str(step).zfill(3)+"_profiles"+outfile_suffix+".hdf5"
        if os.path.exists(outfile_name):
            os.remove(outfile_name)
        outfile = h5.File(outfile_name)
    for run in runs:
        if save_old:
            if os.path.exists(outdir+run+"/snap_"+str(step).zfill(3)+"_profiles.hdf5"):
                os.system("cp "+outdir+run+"/snap_"+str(step).zfill(3)+"_profiles.hdf5 "+outdir+run+"/snap_"+str(step).zfill(3)+"_profiles_old.hdf5")
        print "Doing run", run, "snapshot", str(step)
        #rp.radprof_halos(topdir+run+"/snap_"+str(step).zfill(3), N=N, bin_ratio=bin_ratio, plot=False, stack=True, title=run, outdir=outdir+run, mpc_units=mpc_units, masstab=masstab)

        if combine:
            infile = h5.File(outdir+run+"/snap_"+str(step).zfill(3)+"_profiles.hdf5", 'r')
            grp_name = run.replace("/","_")
            infile.copy("ID-00", outfile['/'], name=grp_name)

            if not outfile.attrs.__contains__('HubbleParam'):
                outfile.attrs['HubbleParam'] = infile.attrs['HubbleParam']
                outfile.attrs['boxsize'] = infile.attrs['boxsize']
                outfile.attrs['redshift'] = infile.attrs['redshift']
            infile.close()
    if combine:
        outfile.close()
                
print "Combined data saved to", outfile_name

#rp.radprof_plot(outfile_name, outdir=outdir, stack=True, title="Gadget zoom-ins with AGN feedback", plotmean=True)
