#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 11 18:50:39 2017
This module simply contains functions describing the density and temperature
profiles described in Vikhlinin et al. 2006
@author: nh444
"""

def temp_profile(r, Rt, a, b, c, Rcool, acool, Tmin, T0):
    ## Vikhlinin+2006 temperature profile model
    if b == 0.0: t = pow(r/Rt, -a)
    else: t = pow(r/Rt, -a) / pow(1+pow(r/Rt, b), c/b)
    x = pow(r/Rcool, acool)
    tcool = (x + Tmin/T0) / (x+1)
    T = T0 * t * tcool
    return T