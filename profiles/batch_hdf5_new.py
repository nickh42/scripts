import radprof2 as rp

#nbins = 20
bin_ratio=1.3
N = 0 #number of clusters, set to 0 to use M500_min instead
M500_min = 1e2 * 0.6 ## 1e10 Msun/h
topdir = "/data/emergence3/deboras/Elliptical_ZoomIns/"
outdir = "/home/nh444/data/project1/profiles/"
## names of directories containing the snapshot files
runs = [#"L40_512_DutyInt1RadioWeak_Thermal_FixEta",
        #"L40_512_DutyIntRadioWeak_Thermal",
        #"L40_512_LowDutyRadioWeak_HalfThermal_FixEta",
        #"L40_512_LowDutyRadioWeak_HalfThermal_FixEta_BHChangeVel",
        #"L40_512_NoDutyRadioInt_HalfThermal_FixEta",
        #"L40_512_NoDutyRadioIntLow_HalfThermal_FixEta",
        #"L40_512_NoDutyRadioIntLow_HalfThermal_FixEta_BHChangeVel",
        #"L40_512_NoDutyRadioWeak_HalfThermal_FixEta",
        #"L40_512_NoDutyRadioWeak_HalfThermal_FixEta_BHChange",
        #"L40_512_NoDutyRadioWeak_HalfThermal_FixEta_BHChangeVel",
        #"L40_512_NoDutyRadioWeak_HalfThermal_FixEtaRadEff",
        #"L40_512_Temp7DutyRadioWeak_HalfThermal_FixEta_BHChangeVel",
        #"L40_512_TempDutyRadioWeak_HalfThermal_FixEta_BHChangeVel",
        "L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff",
        #"L40_512_NoDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_Prex",
        ]
runs = [#"L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff",
        #"L40_512_NoDutyRadioWeak_HalfThermal_FixEta",
        "L40_512_NoDutyRadioInt_HalfThermal_FixEta",
        "L40_512_LowDutyRadioWeak_HalfThermal_FixEta",
]

topdir = "/data/curie4/nh444/project1/boxes/"
runs = [#"L40_512_LDRIntRadioEffBHVel/",
        #"L40_512_NoDutyRadioWeak/",
        #"L40_512_NoDutyRadioInt/",
        #"L40_512_DutyRadioWeak/",
        #"L40_512_LDRIntRadioEff_noBH/",
        #"L40_512_LDRHighRadioEff/",
        "L40_512_MDRIntRadioEff/",
        ]

mpc_units=False

## snapshot numbers
#steps = [5, 6, 8, 10, 15, 20, 25]
steps = [19]
#steps = [20,21,22,23,24]
T_weighting="mass"
rho_weighting="mass"
temp_thresh = 4.0
filter_type="default"
suffix = "mass_weighted_default_Tvir4.0"
#filter_type="Rasia2012"
#suffix = "mass_weighted_Rasia2012_Tvir4.0"
#temp_thresh = -1
#filter_type="default"
#suffix = "mass_weighted_default"

for run in runs:
    for step in steps:
        #if os.path.exists(outdir+run+"/snap_"+str(step).zfill(3)+"_profiles.hdf5"):
            #os.system("cp "+outdir+run+"/snap_"+str(step).zfill(3)+"_profiles.hdf5 "+outdir+run+"/snap_"+str(step).zfill(3)+"_profiles_old.hdf5")
        print "Doing run", run, "snapshot", str(step)
        rp.radprof_halos(topdir+run+"/snap_"+str(step).zfill(3)+".hdf5", N=N, bin_ratio=bin_ratio, rmax=5.0,
                         plot=False, stack=True,
                         outdir=outdir+run, M500_min=M500_min, mpc_units=mpc_units,
                         T_weighting=T_weighting, rho_weighting=rho_weighting,
                         suffix=suffix, filter_type=filter_type, temp_thresh=temp_thresh)
