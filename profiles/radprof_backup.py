## Produces logarithmic radial profiles of mass, temperature or entropy for a gadget snapshot file (1st argument) for a given radial range, centre, number of bins
## Data is output to a .txt file in the same directory as the input file unless no write permission in which case it's saved to /home/nh444/scripts/profile/output/:
''' e.g. 1
import radprof as rp
rp.radprof("snap_069.hdf5","/home/nh444/scripts/profile/target_test.txt")
e.g. 2
from radprof import radprof
data = radprof("snap_069.hdf5", 95.229874, 99.87104, 103.63534, 0.0004537959, 1.956541504474, nbins=84)
'''

##  Function for calculating density, temp and entropy in radial bins
def radprof_calc (snapshot, xcentre, ycentre, zcentre, minrad, maxrad, units, bin_ratio = 1.6, nbins=0, R500=None, R2500=None, mean_bins=None, volumeweight=False, HubbleParam=None, boxsize=None, masstab=False, mpc_units=False):
    ## --------------------------- FUNCTION ARGUMENTS --------------------------- ##
    ## snapshot - snapshot file path (string)
    ## xcentre, ycentre, zcentre - centre from which radii are calculated (code units)
    ## minrad/maxrad - min/max radius to plot (in code units from centre position)
    ## units = (UnitLength_in_cm, UnitMass_in_g, UnitVelocity_in_cm_per_s)
    ## bin_ratio - Ratio of outer to inner radius for each bin
    ## nbins - no. of logarithmic bins between minrad and maxrad
    ## volumeweight - option to volume weight the entropy rather than mass weight it
    ## mean_bins - inner and outer radii of bin in which to calculate mean temperature
    
    import h5py
    import numpy as np
    import pylab as plt
    #import readsnap as rs
    import sys, os, math
    import collections
    from scipy import stats
    import snap

    kT_lim = 0.02585501872659 ## 3e5 Kelvin in keV - typical temp limit of observations
    #kT_lim = 0.008617 ## 1e5 K
    #kT_lim = 0.04308665 ## 5e5 K
    
    type = 0 # Particle type, currently gas only (type 0)
    centre = (xcentre, ycentre, zcentre)

    Xh=0.76 # hydrogen mass fraction
    gamma=5.0/3.0 # adiabatic index
    
    ##physical constants in cgs units
    GRAVITY = 6.672e-8
    BOLTZMANN = 1.3806e-16
    PROTONMASS = 1.6726e-24
    ELECTRONMASS = 9.1094e-28
    ELECTRONCHARGE = 4.8032e-10 #in cgs ESU/Gaussian units (statC/Fr = 10^-1 * c Coulombs)
    ELECTRONVOLT = 1.60218e-12 #in ergs (electron charge times 1V = 10^8 * c^-1 statV)
    kpc_in_cm = 3.085678e21 # 1.0 kpc
    Msun1e10_in_g = 1.989e43 # 1.0e10 solar masses
    km_per_s_in_cm_per_s = 1.0e5 # 1 km/sec

    UnitLength_in_cm = units[0]
    UnitMass_in_g = units[1]
    UnitVelocity_in_cm_per_s = units[2]

    UnitTime_in_s = UnitLength_in_cm / UnitVelocity_in_cm_per_s
    UnitEnergy_in_cgs = UnitMass_in_g * UnitLength_in_cm**2.0 / UnitTime_in_s**2.0

    ## --------------------------- DATA HANDLING --------------------------- ##
    ## Read in snapshot
    basedir = os.path.dirname(snapshot)
    if (basedir == ""):
        basedir = "."
    snapnum = int(str(os.path.splitext(snapshot)[0])[-3:])

    s = snap.snapshot(basedir, snapnum, masstab=masstab, mpc_units=False, read_params=False)
    ## Note I have set mpc_units to False since the following code assumes all quantities are in the original code units (should change this at some point)
    if (HubbleParam is None or boxsize is None):
        HubbleParam = s.header.hubble
        boxsize = s.header.boxsize

    s.load_gaspos()
    s.load_u()
    s.load_gasmass()
    s.load_gasrho()
    s.load_ne()

    pos = s.gaspos
    u = s.u
    MASS = s.gasmass
    rho = s.gasrho
    NE = s.ne ## Electron Abundance = no of free electrons per H nucleus (HI + HII)
    #pos = rs.read_block(snapshot, "POS ", parttype=type)
    #u = rs.read_block(snapshot, "U   ", parttype=type) ## internal energy per unit mass
    #MASS = rs.read_block(snapshot, "MASS", parttype=type)
    #rho = rs.read_block(snapshot, "RHO ", parttype=type)
    #NE =  rs.read_block(snapshot, "NE  ", parttype=type) ## Electron Abundance = no of free electrons per H nucleus (HI + HII)

    ## Compute relative distances from centre, taking periodicity into account
    dist = pos
    for i in range(0, len(centre)):
        dist[:,i] -= centre[i]
        if (centre[i] < maxrad):
            filter = np.where(dist[:,i] > (boxsize - maxrad))
            dist[filter,i] -= boxsize
        if (centre[i] + maxrad > boxsize):
            filter = np.where(dist[:,i] < (maxrad - boxsize))
            dist[filter,i] += boxsize

    
    ## Mask particles outside of maxrad
    mask = np.where((dist[:,0] < maxrad) | (dist[:,1] < maxrad) | (dist[:,2] < maxrad))
    pos = pos[mask]
    dist = dist[mask]
    u = u[mask]
    MASS = MASS[mask]
    rho = rho[mask]
    NE = NE[mask]
    
    ## Compute radial distances from centre for each particle
    r = ((dist[:,0])**2 + (dist[:,1])**2 + (dist[:,2])**2)**0.5

    '''
    ## If first bin would be empty, recalculate nbins so that it is not
    rmin = min(r)
    if rmin > minrad:
        nbins = int((math.log10(maxrad) - math.log10(minrad)) / (math.log10(rmin) - math.log10(minrad))) - 1
    '''
    
    ##Define logarithmic radial bins
    if nbins > 0:
        nbins = int(nbins) + 1
    else:
        nbins = int((math.log10(maxrad) - math.log10(minrad)) / math.log10(bin_ratio))
    binleft = np.logspace(math.log10(minrad), math.log10(maxrad), num=nbins, base=10.0)
    bincentre = binleft*10.0**((math.log10(maxrad) - math.log10(minrad))/nbins/2)
    ##label each particle with corresponding bin index (0 if below lowest bin)
    binlabels = np.digitize(r, binleft, right=False)
    
    ## ---------- CALCULATE TEMP ---------- ##
    ## Convert internal energy to cgs units
    u *= UnitEnergy_in_cgs / UnitMass_in_g
    ## mean molecular weight for electron abundance NE
    meanweight = PROTONMASS * (1.0+(1.0-Xh)/Xh)/(1.0+NE+(1.0-Xh)/(4.0*Xh))
    ## Calculate kT from ideal gas law (0.5KT per d.o.f.) in keV
    kT = u * meanweight * (gamma - 1.0) / ELECTRONVOLT / 1000.0
    
    ## Electron number density ne from gas density and electron abundance
    ne = rho * UnitMass_in_g / UnitLength_in_cm**3.0 * NE * Xh / (PROTONMASS + ELECTRONMASS) * HubbleParam**2

    ## ---------------------- BINNING --------------------- ##
    ## Mask temperatures below kT_lim ~ 3e5 K (observational limit)
    filt = np.where(kT > kT_lim)
    kT = kT[filt]
    ne = ne[filt]
    m = MASS[filt]
    
    ## Count number of particles (with kT>kT_lim) in each bin
    binnums = np.bincount(binlabels[filt], minlength=nbins+1)
    ## delete first element since np.digitize returns a zero index for particles at r<minrad and 1 for particles in the first bin (Whose left edge is binleft[0])
    binnums = np.delete(binnums, [0])
    ## delete last element since np.digitize returns index len(bins) for particles beyond maxrad
    binnums = np.delete(binnums, [len(binnums)-1])
    nonzero = (binnums[:] > 0) # mask for non-zero bins

    v = MASS / rho
    binvol = np.asarray([v[binlabels == i].sum() for i in range(1, len(binleft))])
    
    ## Total mass in each bin
    mbinsum = np.asarray([m[binlabels[filt] == i].sum() for i in range(1, len(binleft))])
    ## 'Mass-weighted' density
    mbindens = np.zeros_like(mbinsum)
    for i in range(0, len(binleft)-1):
        mbindens[i] = mbinsum[i] / (4.0/3.0*math.pi * (binleft[i+1]**3 - binleft[i]**3))

    ## Emission-measure weighted density
    #rho_i = MASS[filt] * rho[filt]
    #mbindens = np.asarray([rho_i[binlabels[filt] == i].sum() for i in range(1, len(binleft))])
    #mbindens[nonzero] = (mbindens[nonzero] / binvol[nonzero])**0.5
    
    method = 5
    T_method = 1
    if method == 5: ## Sum over electron number density
        if T_method == 0: ## EM weighted
            w = rho[filt] * MASS[filt] ## Emission-measure weighted (volume-averaged with weight w = rho^2)
        if T_method == 1: ## Mass weighted
            w = MASS[filt]
        if T_method == 2: ## Mazzotta et al 2004 spectral like temperature
            w = rho[filt] * MASS[filt] / kT**0.75

        #w_spec = rho[filt] * MASS[filt] / kT**0.75 ## Mazzotta et al 2004 spectral like temperature
        #kT_spec = kT * w_spec
        ## Calculate temperature for entropy calculation
        kT *= w
        wbinsum = np.asarray([w[binlabels[filt] == i].sum() for i in range(1, len(binleft))])
        Tbinmean = np.asarray([kT[binlabels[filt] == i].sum() for i in range(1, len(binleft))])
        Tbinmean[nonzero] = np.asarray(Tbinmean[nonzero]) / np.asarray(wbinsum[nonzero])

        ## Calculate mean temperature
        if mean_bins is not None:
            binlabels_mean = np.digitize(r, mean_bins)
            Tmean = np.asarray(kT[binlabels_mean[filt] == 1].sum())
            wmeansum = np.asarray(w[binlabels_mean[filt] == 1].sum())
            Tmean = Tmean / wmeansum
        else:
            Tmean = None

        ## Calculate T500 and T2500 in aperture of projected radius 0.15-1*R500
        if R500 is not None:
            r2D = ((dist[:,0])**2 + (dist[:,1])**2)**0.5
            binlabels_r500 = np.digitize(r2D, [0.15*R500, R500])
            T500 = np.asarray(kT[binlabels_r500[filt] == 1].sum())
            wr500sum = np.asarray(w[binlabels_r500[filt] == 1].sum())
            T500 = T500 / wr500sum
        else:
            T500 = None
        if R2500 is not None:
            r2D = ((dist[:,0])**2 + (dist[:,1])**2)**0.5
            binlabels_r2500 = np.digitize(r2D, [0.15*R2500, R2500])
            T2500 = np.asarray(kT[binlabels_r2500[filt] == 1].sum())
            wr2500sum = np.asarray(w[binlabels_r2500[filt] == 1].sum())
            T2500 = T2500 / wr2500sum
        else:
            T2500 = None

        ne_i = MASS[filt] * NE[filt] * rho[filt]
        nebinmean = np.asarray([ne_i[binlabels[filt] == i].sum() for i in range(1, len(binleft))])
        nebinmean *=  UnitMass_in_g * UnitMass_in_g / pow(UnitLength_in_cm, 6) * Xh**2 / (PROTONMASS + ELECTRONMASS)**2
        nebinmean[nonzero] = (nebinmean[nonzero] / binvol[nonzero])**0.5 * HubbleParam**2
        Sbinmean = np.zeros_like(Tbinmean)
        Sbinmean[nonzero] = Tbinmean[nonzero] / pow(nebinmean[nonzero], 2.0/3.0)

    if method == 6: ## (Deprecated) Sum over mass density, then average electron density
        w = rho[filt] * MASS[filt] ## weighting
        wbinsum = np.asarray([w[binlabels[filt] == i].sum() for i in range(1, len(binleft))])
        kT *= w
        Tbinmean = np.asarray([kT[binlabels[filt] == i].sum() for i in range(1, len(binleft))])
        Tbinmean[nonzero] = np.asarray(Tbinmean[nonzero]) / np.asarray(wbinsum[nonzero])

        v = MASS / rho
        binvol = np.asarray([v[binlabels == i].sum() for i in range(1, len(binleft))])

        ne_i = MASS[filt] * rho[filt]
        nebinmean = np.asarray([ne_i[binlabels[filt] == i].sum() for i in range(1, len(binleft))])
        nebinmean = (nebinmean / binvol)**0.5
        NE = NE[filt]
        NEbinmean = np.asarray([NE[binlabels[filt] == i].mean() for i in range(1, len(binleft))])
        nebinmean *= Xh / (PROTONMASS + ELECTRONMASS) * NEbinmean * HubbleParam**2
        Sbinmean = Tbinmean / pow(nebinmean, 2.0/3.0)
        
    ## Convert to units of kpc and 10^10 Msun
    lengthfac = UnitLength_in_cm / kpc_in_cm
    massfac = UnitMass_in_g / Msun1e10_in_g
    binleft *= lengthfac
    bincentre *= lengthfac
    mbinsum *= massfac
    mbindens *= massfac / (lengthfac)**3
    
    Data = collections.namedtuple('Data', 'binleft, bincentre, binright, binnums, mbinsum, mbindens, Tbinmean, Sbinmean')
    data = Data(binleft[:-1], bincentre[:-1], binleft[1:], binnums, mbinsum, mbindens, Tbinmean, Sbinmean)

    return (data, Tmean, T500, T2500)

## --------------------------- PLOTTING --------------------------- ##
def radprof_plot (infile, outdir="default", stack=True, redshift=None, title="", nplot=0, M500_min=-1, plotmean=True, compare="Sun", plot_only='all', plot_T2500=False, for_paper=False, plot_vertlines=False, add_labels=False):

    ## Infile = hdf5 file containing profile data
    ## outdir = directory where .pdf file is output
    ## stack = If true all profiles are plotted on same graph
    ## redshift = redshift of object
    ## title = title of plot
    ## nplot = no. of profiles to plot (beginning from the first in hdf5 list)
    ## compare = "Sun" or "Rasmussen" for different comparison observational data
    ## plot_only = "all", "density", "temperature" or "entropy"
    ## plot_T2500 = normalise temp by T2500
    ## add_labels = labels each profile with its hdf5 group name
    
    import h5py
    import numpy as np
    from numpy import ma
    import matplotlib.pyplot as plt
    from matplotlib import gridspec
    import sys, os

    pi = 3.1415926535897932384626
    obs_dir = "/data/vault/nh444/ObsData/profiles/" ##directory containing observational data

    assert plot_only in ['all', 'density', 'temperature', 'entropy']

    ## File checking
    if not os.path.exists(infile):
        print "File", infile, "not found."
        sys.exit()

    if (outdir == "default"):
        ## Check write permissions for directory of input file
        if (os.access(os.path.dirname(infile), os.W_OK | os.X_OK)):
            if not stack:
                plotdir = os.path.dirname(infile)+"/"+os.path.splitext(os.path.basename(infile))[0]+"/"
            else:
                plotdir = os.path.dirname(infile)+"/"
        elif (os.access("./", os.W_OK | os.X_OK)):
            if not stack:
                plotdir = "./"+os.path.splitext(os.path.basename(infile))[0]+"/"
            else:
                plotdir = "./"
            print "WARNING: No write permission in snapshot directory. Outputting to current directory."
        else:
            if not stack:
                plotdir = "/home/nh444/scripts/profile/output/"+os.path.splitext(os.path.basename(infile))[0]+"/"
            else:
                plotdir = "/home/nh444/scripts/profile/output/"
            print "WARNING: No write permission in directory. Outputting to"+plotdir
    else:
        if not stack:
            plotdir = os.path.normpath(outdir)+"/"+os.path.splitext(os.path.basename(infile))[0]+"/"
        else:
            plotdir = os.path.normpath(outdir)+"/"

    if not (os.path.exists(plotdir)):
        os.makedirs(plotdir)

    plt.rc('text', usetex=True)
    plt.rc('font', family='sans-serif')

    title = title.replace('_','\_')

    if plot_only=='all':
        #fig = plt.figure(figsize=(8.27,11.69)) # A4 size
        fig = plt.figure(figsize=(10,13))
    else:
        fig = plt.figure(figsize=(10,7))

    dens_unit = "$10^{10}$ $M_{\odot}$ h$^2$ kpc$^{-3}$"
    temp_unit = "keV"
    S_unit = "keV cm$^2$"

    f = h5py.File(infile, 'r')
    grp_names = f.keys()
    grp_list = np.asarray(grp_names)
    h = f.attrs['HubbleParam']
    N = len(grp_names)
    if nplot == 0:
        Nstart = 0 ## Plot all
    else:
        assert (M500_min < 0), "Cannot define both nplot and M500_min"
        Nstart = N - nplot


    if stack:

        M500_list = []
        for i in range(0, N):
            if ('M500' in f[grp_list[i]].attrs):
                M500_list.append(f[grp_list[i]].attrs['M500'])
        if len(M500_list) > 0:
            M500_list = np.asarray(M500_list, dtype=np.float64)
            ## Sort groups lowest M500 to highest
            sorter = np.argsort(M500_list)
            M500_list = M500_list[sorter]
            grp_list = grp_list[sorter]
            ## Plot those with M500 (1e10 Msun/h) above M500_min (1e10 Msun/h)
            if M500_min >= 0: Nstart = np.argmax(M500_list >= M500_min)
        else:
            ## Sort groups in (reverse) order of group name e.g. ID-19, ID-18... ID-01, ID-00
            grp_list = np.sort(grp_list)[::-1]
        if plot_only=='all':
            gs = gridspec.GridSpec(4, 1, height_ratios=[1,1,1,0.08])
            outname = plotdir+os.path.splitext(os.path.basename(infile))[0]+".pdf"
        else:
            gs = gridspec.GridSpec(2,1, height_ratios=[1,0.05])
            if plot_only=='density': outname = plotdir+os.path.splitext(os.path.basename(infile))[0]+"_density.pdf"
            if plot_only=='temperature': outname = plotdir+os.path.splitext(os.path.basename(infile))[0]+"_temperature.pdf"
            if plot_only=='entropy': outname = plotdir+os.path.splitext(os.path.basename(infile))[0]+"_entropy.pdf"
            
        if plot_only=='density' or plot_only=='all':
            d_plt = fig.add_subplot(gs[0], xscale='log', yscale='log')
            if plot_only=='density': plt.xlabel(r"$r/r_{500}$", fontsize=18)
            plt.ylabel(r"${\rho}_{gas} / {\rho}_{crit}$", fontsize=16)
            plt.ylim(3, 1e4)
        if plot_only=='temperature' or plot_only=='all':
            if plot_only=='all': T_plt = fig.add_subplot(gs[1], xscale='log', yscale='log')
            if plot_only=='temperature':
                T_plt = fig.add_subplot(gs[0], xscale='log', yscale='log')
                plt.xlabel(r"$r/r_{500}$", fontsize=18)
            plt.ylabel(r"$T/T_{500}$", fontsize=16)
            plt.ylim(0.1, 10.0)
        if plot_only=='entropy' or plot_only=='all':
            if plot_only=='all': S_plt = fig.add_subplot(gs[2], xscale='log', yscale='log')
            if plot_only=='entropy': S_plt = fig.add_subplot(gs[0], xscale='log', yscale='log')
            plt.ylabel(r"$S/S_{500\mathrm{, adi}}$", fontsize=16)
            plt.xlabel(r"$r/r_{500}$", fontsize=18)
            plt.ylim(0.003, 10.0)

        ## Colour bar
        #cmap = plt.get_cmap('rainbow')
        cmap = plt.get_cmap('cool')
        colour_idx = np.linspace(1, 0, N-Nstart)
        if nplot == 1:
            colour_idx = [0]
        gradient = np.linspace(0, 1, 256)
        gradient = np.vstack((gradient, gradient))
        if plot_only=='all': c_plt = fig.add_subplot(gs[3])
        else: c_plt = fig.add_subplot(gs[1])
        c_plt.axes.imshow(gradient, aspect='auto', cmap=cmap)
        c_plt.axes.get_yaxis().set_visible(False)
        gs.tight_layout(fig, rect=[0, 0.05, 1, 0.95])
        labels = []

    for i in range(Nstart, N):
        print "Plotting", grp_list[i]
        group = f[grp_list[i]]
        bins = group['bins']
        density = group['density']
        temp = group['temperature']
        S = group['entropy']
        number = group['number']
        if add_labels:
            label = grp_list[i]
        else:
            label = None
        
        vertlines = [] ## Positions of vertical lines
        if ('SofteningGas' in group.attrs):
            vertlines.append(group.attrs['SofteningGas'])

        if not stack:
            if ('R500' in group.attrs):
                vertlines.append(group.attrs['R500'])

            ## Plot density
            plt.subplot(3,1,1)
            plt.plot(bins[:,0], density, 'r-', drawstyle='steps-post')
            plt.xscale('log')
            plt.yscale('log')
            plt.ylabel(r"${\rho}_{GAS}$ ("+dens_unit+")", fontsize=16)
            plt.xlim(min(bins[:,0]), max(bins[:,2]))
            for vl in vertlines:
                plt.axvline(x=vl, color='0.75', linestyle='dashed', linewidth=0.5)
                
            ## Plot temperature
            plt.subplot(3,1,2)
            plt.plot(bins[:,0], temp, 'r-', drawstyle='steps-post')
            plt.xscale('log')
            plt.ylabel(r"kT ("+temp_unit+r")", fontsize=16)
            plt.xlim(min(bins[:,0]), max(bins[:,2]))
            for vl in vertlines:
                plt.axvline(x=vl, color='0.75', linestyle='dashed', linewidth=0.5)
        
            ## Plot entropy
            plt.subplot(3,1,3)
            plt.plot(bins[:,0], S, 'r-', drawstyle='steps-post')
            plt.xscale('log')
            plt.yscale('log')
            plt.ylabel(r"S = kT / n$_e^{2/3}$ ("+S_unit+r")", fontsize=16)
            plt.xlim(min(bins[:,0]), max(bins[:,2]))
            for vl in vertlines:
                plt.axvline(x=vl, color='0.75', linestyle='dashed', linewidth=0.5)
        
            plt.xlabel(r"Radial Distance (h$^{-1}$ kpc)", fontsize=18)
            
            if ('M500' in group.attrs):
                fig.suptitle(str(grp_names[i])+r" radial profiles $M_{500} =$ "+str(int(group.attrs['M500']))+r"$ \times 10^{10}$ $M_{\odot}$", fontsize=20)
            else: 
                fig.suptitle(str(grp_names[i])+" radial profiles", fontsize=20)

            plt.savefig(plotdir+"profile-"+str(grp_names[i])+".pdf")
            plt.clf()

        if stack:
            if ('M500' in group.attrs):
                labels.append(group.attrs['M500'] / 1e3)# / f.attrs['HubbleParam']

            if ('R500' in group.attrs):
                Rvir = group.attrs['R500']
            else:
                Rvir = 1000.0
                print "WARNING: ",str(grp_names[i]), " R500 attribute not found - setting R500 = 1 Mpc"

            ## SCALING ##
            rho_crit = 3.0/8.0 / pi / 6.67408e-11 * (1.0 * 100 / 3.086e+19)**2.0 * (3.086e+19)**3.0 / 1.989e+30 / 1e+10 ## Critical density in 10^10 Msun h^2 kpc^-3 (h = 1.0)
            #rho_crit = 2.77492421e-8 ## Critical density in 10^10 Msun h^2 kpc^-3 (same as above)
            
            bins_rel = np.asarray(bins) / Rvir
            vertlines /= Rvir
            bin_vir = (bins_rel[:,0] <= 1.0) & (bins_rel[:,2] >= 1.0)
            Tvir = np.asarray(temp)[bin_vir]
            if ('M500' in group.attrs and 'redshift' in f.attrs):
                M500 = group.attrs['M500']
                z = f.attrs['redshift']
                Ez = pow(0.3 * pow(1+z, 3.0) + 0.7, 0.5)
                Svir = 342.0 * pow(M500 * 1.0e10 / 1.0e14 / Ez, 2.0/3.0) / pow(h/0.73, 4.0/3.0) ## Sun et al 2009 (h0 = 0.73 fb=0.165)
            else:
                print "WARNING: No M500 in group attrs - using Svir = S(Rvir) for normalisation."
                Svirs = np.asarray(S)[bin_vir]
                Svir = Svirs[0]
            
            ## Mask to ignore zero bins
            yd = ma.masked_where((np.asarray(number) == 0), np.asarray(density) / float(rho_crit))
            yS = ma.masked_where((np.asarray(number) == 0), np.asarray(S) / float(Svir))
            yT = ma.masked_where((np.asarray(number) == 0), np.asarray(temp) / float(Tvir[0]))

            if plot_only=='all' or plot_only=='temperature':
                if (compare=="Rasmussen" and 'Tmean' in group.attrs):
                    Tmean = group.attrs['Tmean'] ## mean temperature
                    plt.sca(T_plt)
                    plt.ylabel(r"$T/<T>$", fontsize=16)
                    yT = ma.masked_where((np.asarray(number) == 0), np.asarray(temp) / Tmean)
                elif compare=="Sun" and plot_T2500 == True:
                    T2500 = group.attrs['T2500']
                    plt.sca(T_plt)
                    plt.ylabel(r"$T/T_{2500}$", fontsize=16)
                    yT = ma.masked_where((np.asarray(number) == 0), np.asarray(temp) / T2500)
                elif compare=="Sun" and 'T500' in group.attrs:
                    T500 = group.attrs['T500']
                    yT = ma.masked_where((np.asarray(number) == 0), np.asarray(temp) / T500)
                elif compare=="Sun":
                    yT = ma.masked_where((np.asarray(number) == 0), np.asarray(temp) / float(Tvir[0]))
                else:
                    print "WARNING: compare =", compare, "not recognised."
                    yT = ma.masked_where((np.asarray(number) == 0), np.asarray(temp) / float(Tvir[0]))
            

            ## PLOTTING ##
            if plot_only=='all':
                y = (yd, yT, yS)
                axes = (d_plt, T_plt, S_plt)
            if plot_only=='density':
                y = [yd]
                axes = [d_plt]
            if plot_only=='temperature':
                y = [yT]
                axes = [T_plt]
            if plot_only=='entropy':
                y = [yS]
                axes = [S_plt]
            y1 = [y1[ma.flatnotmasked_edges(y1)] for y1 in y] ## Plot points at either end of lines...
            ## ...since the first point will not be visible if it cannot join with the second point
            j = 0
            for ax in axes: 
                ax.plot(bins_rel[:,1], y[j], '-', color=cmap(colour_idx[i-Nstart]), label=label)
                ax.plot(bins_rel[ma.flatnotmasked_edges(y[j]), 1], y1[j], 'o', ms=3, mec='none', mfc=cmap(colour_idx[i-Nstart]))
                j += 1
                if i==Nstart:
                    plt.sca(ax)
                    plt.xlim(min(bins_rel[:,0]), max(bins_rel[:,2]))
                    #plt.xlim(0.003, max(bins_rel[:,2]))
                    if plot_vertlines:
                        for vl in vertlines: ## Plot vertical dashed lines for softening length
                            plt.axvline(x=vl, color=cmap(colour_idx[i-Nstart]), linestyle='dashed', linewidth=0.5)

            ## Calculate Means 
            if i==Nstart:
                if plot_only=='all': y_mean = np.zeros((len(temp), 5)) ## Array to hold means
                else: y_mean = np.zeros((len(temp), 3)) ## Array to hold means
            y_mean[:,0] += bins_rel[:,1] ## NEED TO FIX THIS - average bin centre radii
            y_mean[:,1] += np.logical_not(yd[:,0].mask) ## Count number of non-zero bins in sum
            if plot_only=='all':
                y_mean[:,2] += yd[:,0] ## sum of density in each bin
                y_mean[:,3] += yT[:,0] ## sum of temperature in each bin
                y_mean[:,4] += yS[:,0] ## sum of entropy in each bin
            if plot_only=='density': y_mean[:,2] += yd[:,0]
            if plot_only=='temperature': y_mean[:,2] += yT[:,0]
            if plot_only=='entropy': y_mean[:,2] += yS[:,0]

    if stack:
        if redshift is None:
            if ('redshift' in f.attrs):
                redshift = f.attrs['redshift']
        if redshift is not None:
            if not for_paper: fig.suptitle(title+"\nz = "+"{:.1f}".format(redshift), fontsize=18)
        else: 
            if not for_paper: fig.suptitle(title, fontsize=18)

        if (plotmean):
            nonzero = np.where(y_mean[:,1] > 0) ## Avoid dividing by zero
            y_mean[:, 0] = y_mean[:, 0] / len(y_mean[:, 0]) ## average bin radius
            j = 2
            for ax in axes:
                y_mean[nonzero, j] = y_mean[nonzero, j] / y_mean[nonzero, 1] ## arithmetic mean of density, temp, entropy
                average, = ax.plot(bins_rel[:,1], ma.masked_where(y_mean[:,1] < 1, y_mean[:, j]), linestyle='--', lw=1, marker='D', ms=3, mec='black', mfc='black', color='black', label="Average")
                j += 1
        
        overplot = True
        if (overplot and redshift < 0.05):
            ACCEPTnames = ["NGC_5846", "HCG_42", "HCG_0062",
                           "NGC_5044", "NGC_0507", "MKW_04", "ESO_5520200"]
            ACCEPTz = [0.0057, 0.0133, 0.0146, 0.009, 0.0164, 0.0198, 0.0314]
            ACCEPTr500 = [309, 324, 403, 430, 467, 690, 580]
            ACCEPT_temp = [0.64, 0.7, 1.1, 1.22, 1.4, 2.16, 2.34]
            if plot_only=='all' or plot_only=='density':
                ## Density Le Brun et al (<-- Sun et al 2009)
                d_obs1 = np.loadtxt(obs_dir+"Sun2009_density.txt")
                d_plt.errorbar(d_obs1[:,0], d_obs1[:,1], yerr=d_obs1[:,2:3], fmt='o', mfc='darkblue', color='darkblue', ecolor='darkblue', lw=1, ms=5, mec='none', label="Sun et al. (2009)")
                ## Vikhlinin+2006
                #for k in range(1,5):
                    #d_obs2 = np.loadtxt(obs_dir+"Vikh2006_density_profile_"+str(k)+".txt")
                    #if k==1: d_plt.plot(d_obs2[:,0], d_obs2[:,1], color='0.7', lw=1, label="Vikhlinin et al. (2006) ($T < 2.5$ keV)")
                    #else: d_plt.plot(d_obs2[:,0], d_obs2[:,1], color='0.7', lw=1)
                ## ACCEPT cluster profiles
                #for ACCEPTidx, ACCEPTname in enumerate(ACCEPTnames):
                    #d_obs3 = np.loadtxt(obs_dir+"ACCEPT_profiles/"+ACCEPTname+"_derived_profiles.dat")
                    #d_plt.plot(d_obs3[:,2]/ACCEPTr500[ACCEPTidx], d_obs3[:,6]/(rho_crit*0.7*0.7), lw=1, label=ACCEPTname.replace("_","\_")+" ($T_{X}= $ "+str(ACCEPT_temp[ACCEPTidx])+" keV)")

            if plot_only=='all' or plot_only=='entropy':
                ## Entropy Sun et al. 2009
                from matplotlib.patches import Polygon
                S_obs1 = np.loadtxt("/home/nh444/vault/ObsData/profiles/Sun_etal_entropy.txt")
                poly_S_obs1 = Polygon(S_obs1, color='0.9', ec='none', label="Sun et al. (2009) (Full range)")
                S_plt.axes.add_patch(poly_S_obs1)
                ## ACCEPT cluster profiles
                #for ACCEPTidx, ACCEPTname in enumerate(ACCEPTnames):
                    #S_obs2 = np.loadtxt(obs_dir+"ACCEPT_profiles/"+ACCEPTname+"_derived_profiles.dat")
                    #ACCEPTEz = pow(0.3 * pow(1.0+ACCEPTz[ACCEPTidx], 3.0) + 0.7, 0.5)
                    #SvirACC = 342.0 * pow(ACCEPTr500[ACCEPTidx]/690.0, 2.0) / pow(ACCEPTEz, 2.0/3.0) / pow(0.7/0.73, 4.0/3.0) ## Sun et al 2009 with M500 --> r500 scaling
                    #S_plt.plot(S_obs2[:,2]/ACCEPTr500[ACCEPTidx], S_obs2[:,5]/SvirACC, lw=1, label=ACCEPTname.replace("_","\_")+" ($T_{X}= $ "+str(ACCEPT_temp[ACCEPTidx])+" keV)")
                ## Entropy - Mean points
                #S_obs2 = np.loadtxt("/home/nh444/vault/ObsData/profiles/entropy_LeBrun.txt")
                #S_plt.errorbar(S_obs2[:,0], S_obs2[:,1], yerr=S_obs2[:,2:3], fmt='D', ms=4, mfc='black', ecolor='black')
                ## Entropy - resample 1.4 < M500 (1e13 Msun) < 4.0
                #S_obs3 = np.loadtxt("/data/vault/nh444/ObsData/profiles/Sun2009_entropy_5lowmass.txt") ## Resampled Sun 2009
                #for k in range(0, len(S_obs3[0,:])-1, 2):
                    #S_plt.plot(S_obs3[:,k], S_obs3[:,k+1], 'black')
                ## Entropy Johnson et al. 2009
                #S_obs4 = np.loadtxt("/data/vault/nh444/ObsData/profiles/Johnson_entropy_resample.txt") ## Resampled Johnson
                #johnson = S_plt.plot(S_obs4[:,0], S_obs4[:,1], 'D-', color='green', lw=0.5, ms=4, mec='none', label="Johnson et al. (2009) 1.1 $\leq$ M$_{500}$ ($10^{13}$ $M_{\odot}$) $\leq$ 3.4")
                #johnson = S_plt.plot(S_obs4[:,0], S_obs4[:,2:], 'D-', color='green', lw=0.5, ms=4, mec='none')

                ## Baseline relation (self-similar scaling, no non-gravitational processes) see Voit 2005
                x_base = np.asarray(S_plt.axes.get_xlim())
                x_base[0] = 0.01#0.07 ## approx limit of baseline approximation
                y_base = 1.40 * x_base**1.1
                S_plt.plot(x_base, y_base, 'black', linestyle='dotted')

            if plot_only=='all' or plot_only=='temperature':
                from matplotlib.patches import Polygon
                ## Temperature
                if (compare=="Rasmussen"):
                    ## Temperature Rasmussen et al. 2007
                    T_obs4 = np.loadtxt("/data/vault/nh444/ObsData/profiles/Rasmussen_temp-prof.txt")
                    Rasmussen = T_plt.errorbar(T_obs4[:,0], T_obs4[:,1], yerr=T_obs4[:,4:5], fmt='D',
                                               ms=3, mfc='0.6', mec='0.6', ecolor='0.6', zorder=1, label="Rasmussen et al. (2007)")
                elif compare=="Sun":
                    ## Temperature Sun et al. 2009
                    if plot_T2500 == True:
                        norm = 1.0
                    else:
                        norm = 0.54 ## normalisation T_500 / T_2500 according to Sun et al. best fit
                    T_obs1 = np.loadtxt("/home/nh444/vault/ObsData/profiles/temp_Sun_Etal_dist.txt")
                    T_obs1[:,1] /= norm
                    poly_T_obs1 = Polygon(T_obs1, color='0.9', ec='none', label="Sun et al. (2009) (Full range)")
                    T_plt.axes.add_patch(poly_T_obs1)
                    
                    #T_obs3 = np.loadtxt("/home/nh444/vault/ObsData/profiles/temp_Sun_Etal_1sig.txt")
                    #T_obs3[:,1] /= norm
                    #poly_T_obs3 = Polygon(T_obs3, color='0.8', ec='none', label="Sun et al. (2009) (1-$\sigma$ range)")
                    #T_plt.axes.add_patch(poly_T_obs3)
                    
                    #T_obs2 = np.loadtxt("/home/nh444/vault/ObsData/profiles/temp_Sun_Etal.txt")
                    #T_obs2[:,1:2] /= norm
                    #T_plt.errorbar(T_obs2[:,0], T_obs2[:,1], yerr=T_obs2[:,2], fmt='D', ms=3, mfc='black', ecolor='black')
                else:
                    print "ERROR: compare must be one of 'Rasmussen' or 'Sun'"
                    sys.exit(1)

            if plot_only=='all' or plot_only=='density':
                d_handles, d_labels = d_plt.axes.get_legend_handles_labels()
                d_plt.legend(d_handles, d_labels, loc=3, frameon=False, title="", borderaxespad=0.9, fontsize=14)
            if plot_only=='all' or plot_only=='temperature':
                T_handles, T_labels = T_plt.axes.get_legend_handles_labels()
                T_plt.legend(T_handles, T_labels, loc=4, frameon=False, title="", borderaxespad=0.9, fontsize=14)
            if plot_only=='all' or plot_only=='entropy':
                S_handles, S_labels = S_plt.axes.get_legend_handles_labels()
                S_plt.legend(S_handles, S_labels, loc=4, frameon=False, title="", borderaxespad=0.9, fontsize=14)

        ## Colour scale
        for j in range(0, len(labels)):
            labels[j] = '{:.2f}'.format(labels[j])
        ticks = np.linspace(0, 256, N-Nstart)
        c_plt.axes.set_xticks(ticks)
        c_plt.axes.set_xticklabels(labels[::-1], rotation='vertical')
        c_plt.axes.set_xlabel(r"M$_{500}$ ($10^{13}$ $h^{-1}$ $M_{\odot}$)", fontsize=16)
        plt.savefig(outname, bbox_inches='tight')

    print "Plot(s) saved to", outname
    #plt.close(fig)
    f.close()
    return outname


## --------------------------- DATA OUTPUT --------------------------- ##
def radprof (snapshot, target_list, bin_ratio=1.6, nbins=0, outdir="default", plot=False, metadata=None, stack=True, title="", units=None, masstab=True, mpc_units=False):

    ## snapshot - snapshot file path (String)
    ## target_list - txt file or array with [ID, xcentre, ycentre, zcentre, minrad, maxrad] for each target
    ## nbins - no. of logarithmic bins
    ## outdir - path to directory to contain output files
    ## plot - if True plots will be made in output directory
    ## metadata - list of dictionaries containing {Length, Mass, M500, R500, M200, R200, SofteningGas} for each object
    ## stack - If true all plots are put on the same figure
    ## title - title of stacked plot

    
    import h5py
    import numpy as np
    import readsnap as rs
    import sys, os, re
    import collections
    import snap

    os.nice(19)
    
    ## File checking
    if os.path.exists(snapshot):
        print snapshot, "found."
    else:
        print "Snapshot file", snapshot, "not found."
        #sys.exit()

    if (type(target_list) is str):#if name of file
        if os.path.exists(target_list):
            targets = np.loadtxt(target_list)
        else:
            print "Target file", target_list, "not found."
            sys.exit()
    else:#if array
        targets = np.transpose(np.asarray(target_list))
        
    if (outdir == "default"):
        ## Check write permissions for directory of input/output file
        if (os.access(os.path.dirname(snapshot), os.W_OK | os.X_OK)):
            outfile = h5py.File(os.path.splitext(snapshot)[0]+"_profiles.hdf5", "w")
        elif (os.access("./", os.W_OK | os.X_OK)):
            outfile = h5py.File("./"+os.path.splitext(os.path.basename(snapshot))[0]+"_profiles.hdf5", "w")
            print "WARNING: No write permission in snapshot directory. Outputting to current directory."
        else:
            outfile = h5py.File("/home/nh444/scripts/profile/output/"+os.path.splitext(os.path.basename(snapshot))[0]+"_profiles.hdf5", "w")
            print "WARNING: No write permission in directory. Outputting to /home/nh444/scripts/profile/output/"
    else:
        outdir = os.path.normpath(outdir)+"/"
        if not (os.path.exists(outdir)):
            os.makedirs(outdir)
        outfile = h5py.File(outdir+os.path.splitext(os.path.basename(snapshot))[0]+"_profiles.hdf5", "w")
    
    dirname = os.path.dirname(snapshot)
    if (dirname == ""):
        dirname = "."

    if units is None:
        UnitLength_in_cm, UnitMass_in_g, UnitVelocity_in_cm_per_s, SofteningGas = read_params(dirname, mpc_units)
        units = (UnitLength_in_cm, UnitMass_in_g, UnitVelocity_in_cm_per_s)

    lenfac = units[0] / 3.085678e21 # factor to convert to kpc units
    massfac = units[1] / 1.989e43 # factor to convert to 10^10 Msun units
    
    print "Reading header of", snapshot
    basedir = os.path.dirname(snapshot)
    if (basedir == ""):
        basedir = "."
    snapnum = int(str(os.path.splitext(snapshot)[0])[-3:])
    s = snap.snapshot(basedir, snapnum, header_only=True, read_params=False)
    header = s.header
    HubbleParam = header.hubble
    boxsize = header.boxsize
    redshift = header.redshift

    outfile.attrs['redshift'] = redshift
    outfile.attrs['boxsize'] = boxsize
    outfile.attrs['HubbleParam'] = HubbleParam
        
    ID = targets[:,0]
    xcentre = targets[:,1]
    ycentre = targets[:,2]
    zcentre = targets[:,3]
    minrad = targets[:,4]
    maxrad = targets[:,5]
    
    for i in range (0, len(ID)):
        if metadata is not None:
            mean_bins = [0.1*metadata[i]['R500'] / lenfac, 0.3*metadata[i]['R500'] / lenfac] ## inner and outer radii of bin for mean temperature
            R500 = metadata[i]['R500'] / lenfac
            R2500 = 0.48 * metadata[i]['R500'] / lenfac ##0.48 factor from NGC 1550 from Sun et al. 2009 (m500 = 3e15 Msun)
        else:
            mean_bins=None
            R500 = None
            R2500 = None

        d, Tmean, T500, T2500 = radprof_calc(snapshot, xcentre[i], ycentre[i], zcentre[i], minrad[i], maxrad[i], units, nbins=nbins, bin_ratio=bin_ratio, mean_bins=mean_bins, R500=R500, R2500=R2500, HubbleParam=HubbleParam, boxsize=boxsize, masstab=masstab, mpc_units=mpc_units)
        
        group = outfile.create_group("/ID-"+str(int(ID[i])).zfill(2))
        group.create_dataset('bins', data = np.c_[d.binleft, d.bincentre, d.binright])
        group.create_dataset('number', data = np.c_[d.binnums])
        group.create_dataset('mass_tot', data = np.c_[d.mbinsum])
        group.create_dataset('density', data = np.c_[d.mbindens])
        group.create_dataset('temperature', data = np.c_[d.Tbinmean])
        group.create_dataset('entropy', data = np.c_[d.Sbinmean])

        group.attrs['centre'] = "("+str(xcentre[i])+","+str(ycentre[i])+","+str(zcentre[i])+")"
        group.attrs['xcentre'] = xcentre[i]
        group.attrs['ycentre'] = ycentre[i]
        group.attrs['zcentre'] = zcentre[i]
        group.attrs['minrad'] = minrad[i] * lenfac
        group.attrs['maxrad'] = maxrad[i] * lenfac

        if metadata is not None:
            group.attrs['Length'] = metadata[i]['Length']
            group.attrs['Mass'] = metadata[i]['Mass']
            group.attrs['M500'] = metadata[i]['M500']
            group.attrs['R500'] = metadata[i]['R500']
            group.attrs['M200'] = metadata[i]['M200']
            group.attrs['R200'] = metadata[i]['R200']
            group.attrs['SofteningGas'] = metadata[i]['SofteningGas']
            group.attrs['Tmean'] = Tmean
            group.attrs['T500'] = T500
            group.attrs['T2500'] = T2500

        print "Profile "+str(i)+" (ID "+str(int(ID[i]))+") done."

    if (plot == True):
        print "Plotting..."
        radprof_plot(str(outfile.filename), outdir=outdir, stack=stack, redshift=redshift, title=title)
        
    print "Data saved as"+outfile.filename
    outfile.close()
    return

def radprof_halos (snapshot, fof_file="default", N=0, bin_ratio=1.6, nbins=0, outdir="default", masstab=True, plot=False, stack=True, title="", mpc_units=None, M500_min=1000):

    ### This function will calculate density, temperature and entropy profiles
    ### for a) the N most massive halos in a given snapshot or b) if N=0 those within a given range of M500
    ## snapshot - path of snapshot file (string)
    ## fof_file - name of corresponding fof file - if 'default' will search same directory as snapshot
    ## N - N most massive halos to consider
    ## nbins - number of bins across radial range
    ## outdir - path of directory to place output files
    ## plot - if True, will run func radprof_plot and output pdf plot(s)
    ## stack - If stack, profiles are calculated in terms of normalised R/R_500
    ##        - If plot and stack, one stacked plot is produced, otherwise individual plots for each halo
    ## title - title of plot(s)
    ## mpc_units - In case the parameters-usedvalues file cannot be accessed this will be used to set UnitLength_in_cm to Mpc (True) or kpc (False), otherwise user is prompted
    ## M500_min - minimum M_500_crit for halo sample (code units)
    
    import readsubf
    import os, re, sys
    import readsnap as rs
    import numpy as np

    ## Range of M500 used for sample of halos
    #M500_min = 1000
    M500_max = 100000
    
    if (fof_file == "default"):
        basedir = os.path.dirname(snapshot)
        if (basedir == ""):
            basedir = "."
        snapnum = int(str(os.path.splitext(snapshot)[0])[-3:])
    else:
        basedir = os.path.dirname(fof_file)
        snapnum = int(str(os.path.splitext(fof_file)[0])[-3:])

    halos = readsubf.subfind_catalog(basedir, snapnum, masstab=masstab)

    dirname = os.path.dirname(snapshot)
    if (dirname == ""):
        dirname = "."

    kpc_in_cm = 3.085678e21 # 1.0 kpc
    Msun1e10_in_g = 1.989e43 # 1.0e10 solar masses
    UnitLength_in_cm, UnitMass_in_g, UnitVelocity_in_cm_per_s, SofteningGas = read_params(dirname, mpc_units)
    units = (UnitLength_in_cm, UnitMass_in_g, UnitVelocity_in_cm_per_s)

    lenfac = UnitLength_in_cm / kpc_in_cm # factor to convert to kpc units
    massfac = UnitMass_in_g / Msun1e10_in_g # factor to convert to 10^10 Msun units
    ## Set min radius to grav softening length of gas
    minR = SofteningGas

    if 999 < lenfac < 1001:
        mpc_units=True
    elif 0.99 < lenfac < 1.01:
        mpc_units=False
    else:
        print "ERROR: lenfac = UnitLength_in_cm / kpc_in_cm = ", lenfac, "but code only supports kpc or Mpc units."
        sys.exit()
    
    ## If M_500 and R_500 not in halo file then calculate them
    if not (hasattr(halos, 'group_m_crit500') and hasattr(halos, 'group_r_crit500')):
        print "WARNING: No M_500 or R_500 in halo file. Calculating..."
        import snap
        snapdata = snap.snapshot(basedir, snapnum, masstab=masstab, mpc_units=mpc_units, read_params=False)

        M500c = np.zeros(snapdata.cat.ngroups)
        R500c = np.zeros(snapdata.cat.ngroups)
        ## Calculate M500 and R500 down to M500_min - assumes halo file is in mass order
        ## I should really correct this so that I only need to calculate it for the halos I'm using (as defined below)
        for groupnum in range(0, halos.ngroups):
            m500c, r500c = snapdata.get_so_mass_rad(groupnum, 500, ref="crit")
            if N==0 and m500c < M500_min / 2: ## Calculate down to M500_min / 2
                break
            if N > 0 and groupnum > N + 5: ## Calculate down to N + 5
                break
            M500c[groupnum] = m500c / massfac
            R500c[groupnum] = r500c / lenfac
    else:
        M500c = halos.group_m_crit500
        R500c = halos.group_r_crit500

    if hasattr(halos, 'group_m_crit200') and hasattr(halos, 'group_r_crit200'):
        M200c = halos.group_m_crit200
        R200c = halos.group_r_crit200
    else:
        M200c = np.zeros_like(M500c)
        R200c = np.zeros_like(R500c)
        
    if N == 0:
        print "Filtering halos by", M500_min, "< M500 <", M500_max, "(code units)"
        mask = (M500c > M500_min) & (M500c <= M500_max)
        ID = np.asarray([i for i,x in enumerate(mask) if x == 1]) ## Index numbers
        N = len(ID)
        xcentre = halos.group_pos[mask, 0]
        ycentre = halos.group_pos[mask, 1]
        zcentre = halos.group_pos[mask, 2]
        maxrad = 2 * R500c[mask] ## Maximum outer bin radius
        if not stack:
            minrad = np.full_like(maxrad, minR) ## Minimum inner bin radius
        if stack:
            minrad = 1e-3 * R500c[mask] ## keep R_min / R_500 = 1e-3 for all
        sorter = M500c[mask].argsort()
        ## Sort by M500
        ID = ID[sorter]
        xcentre = xcentre[sorter]
        ycentre = ycentre[sorter]
        zcentre = zcentre[sorter]
        maxrad = maxrad[sorter]
        minrad = minrad[sorter]
        metadata = []
        for i in ID:
            metadata.append({'Length':halos.group_len[i], 'Mass':halos.group_mass[i]*massfac, 'M500':M500c[i]*massfac, 'R500':R500c[i]*lenfac, 'M200':M200c[i]*massfac, 'R200':R200c[i]*lenfac, 'SofteningGas':SofteningGas*lenfac})
    else: ## CHANGE THIS TO USE M_500 NOT TOTAL MASS
        print "\nFiltering halos to", N, "most massive (mass of all halo particles)"
        ID = np.empty(N, dtype=np.uint32)
        xcentre = np.empty(N)
        ycentre = np.empty(N)
        zcentre = np.empty(N)
        minrad = np.empty(N)
        maxrad = np.empty(N)
        metadata = []
    
        for i in range(0, N):
            ID[i] = i
            xcentre[i] = halos.group_pos[i][0]
            ycentre[i] = halos.group_pos[i][1]
            zcentre[i] = halos.group_pos[i][2]

            if not stack:
                minrad[i] = minR ## Minimum inner bin radius
                maxrad[i] = 2 * R500c[i] ## Maximum outer bin radius
            if stack:
                minrad[i] = 1e-3 * R500c[i] ## keep R_min / R_500 = 1e-3 for all
                maxrad[i] = 2 * R500c[i]
            
            metadata.append({'Length':halos.group_len[i], 'Mass':halos.group_mass[i]*massfac, 'M500':M500c[i]*massfac, 'R500':R500c[i]*lenfac, 'M200':M200c[i]*massfac, 'R200':R200c[i]*lenfac, 'SofteningGas':SofteningGas*lenfac})

    targets = np.vstack((ID, xcentre, ycentre, zcentre, minrad, maxrad))

    radprof(snapshot, targets, bin_ratio=bin_ratio, nbins=nbins, outdir=outdir, plot=plot, metadata=metadata, stack=stack, title=title, units=units, masstab=masstab, mpc_units=mpc_units)
    
    return

### Read parameter file to get units
def read_params(dirname, mpc_units):
    import os, re

    UnitLength_in_cm = 3.085678e21 # 1.0 kpc
    UnitMass_in_g = 1.989e43 # 1.0e10 solar masses
    UnitVelocity_in_cm_per_s = 100000
    SofteningGas = 0.3 # 0.3 kpc
    
    prompt = False
    if os.path.exists(dirname+"/parameters-usedvalues"):
        if os.access(dirname+"/parameters-usedvalues", os.R_OK):
            params = open(dirname+'/parameters-usedvalues')
            for line in params:
                if re.match('SofteningGas', line):
                    SofteningGas = float(line.strip().split()[1])
                if re.match('UnitLength_in_cm', line):
                    UnitLength_in_cm = float(line.strip().split()[1])
                if re.match('UnitMass_in_g', line):
                    UnitMass_in_g = float(line.strip().split()[1])
                if re.match('UnitVelocity_in_cm_per_s', line):
                    UnitVelocity_in_cm_per_s = float(line.strip().split()[1])
            params.close()
        else:
            print "WARNING: No read permission on "+dirname+"/parameters-usedvalues. Using SofteningGas = "+str(SofteningGas)+" kpc and default mass and velocity units."
            prompt = True
    else:
        print "WARNING: Could not find "+dirname+"/parameters-usedvalues. Using SofteningGas = "+str(SofteningGas)+" kpc and default mass and velocity units."
        prompt = True

    if mpc_units == True:
        UnitLength_in_cm = 3.085678e24 # 1.0 Mpc
        SofteningGas /= 1000
        prompt = False
    if mpc_units == False:
        prompt = False
        
    while prompt:
        response = raw_input('Using Mpc units? [y/n]: ')
        if response == 'y':
            UnitLength_in_cm = 3.085678e24 # 1.0 Mpc
            SofteningGas /= 1000
            prompt = False
        if response == 'n':
            prompt = False
        else:
            print "Incorrect input format - Please enter 'y' or 'n'"

    return UnitLength_in_cm, UnitMass_in_g, UnitVelocity_in_cm_per_s, SofteningGas
