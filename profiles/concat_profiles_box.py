## This will combine profiles (hdf5 format) for certain groups from one or more hdf5 files
## with an existing hdf5 file if it exists, or into a new file if none exists
import h5py as h5
import os

indir = "/home/nh444/data/project1/profiles/L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff/"
outdir=indir

groups = [2]

overwrite = True ## overwrite outfile
infile_suffixes = ["_mass_weighted", "_mass_weighted_Tvir_thresh_2.5"]
outfile_suffix = "_grp2_Tvir_thresh_comparison"

snapnums = [25]
for snapnum in snapnums:
    outfile_name = "snap_"+str(snapnum).zfill(3)+"_profiles"+outfile_suffix+".hdf5"
    
    if os.path.exists(outdir+outfile_name) and not overwrite:
        outfile = h5.File(outdir+outfile_name, 'r+')
    else:
        outfile = h5.File(outdir+outfile_name, 'w')
    
    for infile_suffix in infile_suffixes:
        infile = h5.File(indir+"/snap_"+str(snapnum).zfill(3)+"_profiles"+infile_suffix+".hdf5", 'r')
        for group in groups:
            infile.copy("Group_"+str(group).zfill(2), outfile['/'], name="grp"+str(group)+infile_suffix)
            
            if not outfile.attrs.__contains__('HubbleParam'):
                outfile.attrs['HubbleParam'] = infile.attrs['HubbleParam']
                outfile.attrs['boxsize'] = infile.attrs['boxsize']
                outfile.attrs['redshift'] = infile.attrs['redshift']
        infile.close()
    
    outfile.close()
    print "Written to", outdir+outfile_name
