## This will combine profiles (hdf5 format) from zoom runs (i.e. copies one group)
## with an existing hdf5 file if it exists, or into a new file if none exists
import h5py as h5
import os

outdir = "/data/curie4/nh444/project1/profiles/"

dirnames = ["LDRIntRadioEffBHVel"]
zooms = ["c96_box_Arepo_new", "c96_box_Arepo_new_LowSoft",
           "c128_box_Arepo_new", "c128_box_Arepo_new_LowSoft",
           "c160_box_Arepo_new", "c160_box_Arepo_new_LowSoft",
           "c256_box_Arepo_new", "c256_box_Arepo_new_LowSoft",
           "c320_box_Arepo_new", "c320_box_Arepo_new_LowSoft",
           "c384_box_Arepo_new", "c384_box_Arepo_new_LowSoft",
           #"c448_box_Arepo_new", "c448_box_Arepo_new_LowSoft",
           ]
overwrite = True ## overwrite outfile
filetype = "radprof"
indir = "/data/curie4/nh444/project1/profiles/"
#infile_suffixes = ["_mass_weighted"] ## needs underscore prefix
#outfile_suffix = "_box_Arepo_LowSoft_mass_weighted"
infile_suffixes = ["_Rasia_Tvir3.0_mass_weighted"]
outfile_suffix = "_box_Arepo_LowSoft_Tvir3_mass_weighted"

dirnames = ["LDRIntRadioEffBHVel"]
zooms = ["c96_box_Arepo_new",
           "c128_box_Arepo_new",
           #"c160_box_Arepo_new",
           "c192_box_Arepo_new",
           "c256_box_Arepo_new",
           #"c320_box_Arepo_new",
           "c384_box_Arepo_new",
           #"c448_box_Arepo_new",
           ]
overwrite = False ## overwrite outfile, otherwise append profiles to it
filetype = "radprof"
indir = "/data/curie4/nh444/project1/profiles/"
infile_suffixes = ["_mass_weighted_default_Tvir4.0"]
outfile_suffix = "_box_Arepo_new_plus_zooms_default_mass_weighted_Tvir4.0"
outfile_suffix = "_LDRIntRadioEff_plus_zooms_default_mass_weighted_Tvir4.0"

#==============================================================================
# ## X-ray profiles
# dirnames = ["LDRIntRadioEffBHVel"]
# zooms = ["c96_box_Arepo_new_025",
#            "c128_box_Arepo_new_025",
#            "c160_box_Arepo_new_025",
#            "c256_box_Arepo_new_025",
#            "c320_box_Arepo_new_025",
#            "c384_box_Arepo_new_025",
#            "c448_box_Arepo_new_025",
#            ]
# overwrite = False ## overwrite outfile, otherwise append profiles to it
# filetype = "xray"
# indir = "/data/curie4/nh444/project1/L-T_relations/"
# infile_suffixes = ["_default_Tvir4_proj"]
# outfile_suffix = "_zooms_default_Tvir4_proj"
#==============================================================================

dirnames = ["MDRInt"]
zooms = [
           "c96_MDRInt", "c128_MDRInt", "c160_MDRInt", "c192_MDRInt",
           "c224_MDRInt", "c256_MDRInt", "c288_MDRInt", "c320_MDRInt",
           "c352_MDRInt", "c384_MDRInt", "c448_MDRInt",
           "c480_MDRInt",
           "c512_MDRInt",
#           "c112_MDRInt", "c144_MDRInt", "c176_MDRInt", "c208_MDRInt",
#           "c240_MDRInt", "c272_MDRInt", "c304_MDRInt",
#           "c336_MDRInt",
#           "c368_MDRInt", "c400_MDRInt", "c410_MDRInt", "c432_MDRInt",
#           "c464_MDRInt",
#           "c496_MDRInt_SUBF",
           ]

overwrite = False ## overwrite outfile, otherwise append profiles to it
filetype = "radprof"
indir = "/data/curie4/nh444/project1/profiles/"
infile_suffixes = ["_mass_weighted_default_Tvir4.0"]
#infile_suffixes = ["_mass_weighted_default"]
outfile_suffix = "_MDRInt_plus_zooms_default_mass_weighted_Tvir4.0"
#outfile_suffix = "_MDRInt_plus_zooms_default_mass_weighted"

snapnums = [20,21,22,23,24,25]
snapnums = [15,16,17,18,19]
groups = range(300)  # groups to copy over
for snapnum in snapnums:
    if filetype == "radprof":
        filebase = "snap_"+str(snapnum).zfill(3)+"_profiles"
    elif filetype == "xray":
        filebase = "profiles_xray_snap"+str(snapnum)
    else:
        assert False, "filetype "+str(filetype)+" not recognised."

    outfile_name = filebase+outfile_suffix+".hdf5"
    if os.path.exists(outdir+outfile_name) and not overwrite:
        outfile = h5.File(outdir+outfile_name, 'r+')
    else:
        outfile = h5.File(outdir+outfile_name, 'w')

    for dirname in dirnames:
        for zoom in zooms:
            for infile_suffix in infile_suffixes:
                if len(infile_suffixes) > 1:
                    name = zoom+infile_suffix
                else:
                    name = zoom
                infile = h5.File(indir + dirname + "/" + zoom + "/" +
                                 filebase + infile_suffix + ".hdf5", 'r')
                for group in groups:
                    grp_name = name + "_" + str(group).zfill(2)
                    # Copy hdf5 group to output file if it exists
                    if (("Group_" + str(group).zfill(2) in infile) and
                            (grp_name not in outfile)):
                        infile.copy("Group_" + str(group).zfill(2),
                                    outfile['/'],
                                    name=grp_name)

                if not outfile.attrs.__contains__('HubbleParam'):
                    outfile.attrs['HubbleParam'] = infile.attrs['HubbleParam']
                    outfile.attrs['boxsize'] = infile.attrs['boxsize']
                    outfile.attrs['redshift'] = infile.attrs['redshift']
                infile.close()

    outfile.close()
    print "Written to", outdir+outfile_name
