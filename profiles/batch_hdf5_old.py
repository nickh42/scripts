import radprof as rp
import os

#nbins = 20
bin_ratio=1.4
N = 1 #number of clusters
M500_min = 1000
topdir = "/data/emergence3/deboras/Elliptical_ZoomIns/"
#topdir = "/home/nh444/data/project1/zoom_ins/NDRIntRadioEffBHVelPrex/"
outdir = "/home/nh444/data/project1/profiles/"
#outdir = "/home/nh444/data/project1/profiles/NDRIntRadioEffBHVelPrex/"
'''
## names of directories containing the snapshot files
runs = ["L40_512_DutyInt1RadioWeak_Thermal_FixEta",
        "L40_512_DutyIntRadioWeak_Thermal",
        "L40_512_LowDutyRadioWeak_HalfThermal_FixEta",
        "L40_512_LowDutyRadioWeak_HalfThermal_FixEta_BHChangeVel",
        "L40_512_NoDutyRadioInt_HalfThermal_FixEta",
        "L40_512_NoDutyRadioIntLow_HalfThermal_FixEta",
        "L40_512_NoDutyRadioIntLow_HalfThermal_FixEta_BHChangeVel",
        "L40_512_NoDutyRadioWeak_HalfThermal_FixEta",
        "L40_512_NoDutyRadioWeak_HalfThermal_FixEta_BHChange",
        "L40_512_NoDutyRadioWeak_HalfThermal_FixEta_BHChangeVel",
        "L40_512_NoDutyRadioWeak_HalfThermal_FixEtaRadEff",
        "L40_512_Temp7DutyRadioWeak_HalfThermal_FixEta_BHChangeVel",
        "L40_512_TempDutyRadioWeak_HalfThermal_FixEta_BHChangeVel",
        "L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff",
        "L40_512_NoDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_Prex"]
'''
runs = ["L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff"]
#runs = ["c256"]
mpc_units=False
## snapshot numbers
#steps = [5, 8, 15, 25]
#steps = [6, 10, 20]
#steps = [5, 6, 8, 10, 15, 20, 25]
steps = [25]

for run in runs:
    for step in steps:
        if os.path.exists(outdir+run+"/snap_"+str(step).zfill(3)+"_profiles.hdf5"):
            os.system("cp "+outdir+run+"/snap_"+str(step).zfill(3)+"_profiles.hdf5 "+outdir+run+"/snap_"+str(step).zfill(3)+"_profiles_old.hdf5")
        print "Doing run", run, "snapshot", str(step)
        rp.radprof_halos(topdir+run+"/snap_"+str(step).zfill(3)+".hdf5", N=N, bin_ratio=bin_ratio, plot=False, stack=True, title=run, outdir=outdir+run, M500_min=M500_min, mpc_units=mpc_units)
