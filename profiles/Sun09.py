#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 30 15:08:40 2018

@author: nh444
"""
import numpy as np

obs_dir = "/data/vault/nh444/ObsData/profiles/" ## directory containing observational data

G = 6.67259e-8 ## gravitational constant in cgs
mp = 1.6726231e-24 ## proton mass in g

def get_median_profile(prop, xmin=0.01, xmax=1.0, scaled=True, mu=0.59, b=0.0, h_scale=0.679):
    assert prop in ["density", "temperature", "entropy", "pressure"], "property '"+str(prop)+"' not recognised."
    from glob import glob
    ## Glob the files
    if prop is not "temperature":
        d_files = glob(obs_dir+"Sun_2009_density_profiles/*_den")
    if prop is not "density":
        T_files = glob(obs_dir+"Sun_2009_temp_profiles/T_3D/*_T")
    ## Load catalogue of objects and their properties e.g. R500
    Suncat = np.genfromtxt(obs_dir+"Sun2009.csv", delimiter=",", dtype=None) ## dtype=None guesses data type (can be slow)
    ## Define array of r/r500 for median
    xbins = np.logspace(np.log10(max(0.01, xmin)), np.log10(min(xmax, 1.0)), num=(np.log10(xmax) - np.log10(xmin))/0.08) ## 0.08 dex spacing to be consistent with Sun09 density profile (0.02 dex spacing but plotting every 4 bins)
    all_values = np.zeros((len(xbins), len(T_files))) ## Stores all profiles
    for T_idx, T_file in enumerate(T_files):
        if prop is not "density":
            T_x, T_y, T_y_errlo, T_y_errhi = get_temp_profile(T_file, Suncat, scaled=scaled, mu=mu, b=b, h_scale=h_scale)
        #T_plt.errorbar(T_x, T_y, yerr=[T_y_errlo, T_y_errhi], c='grey', alpha=0.4, ls='none', zorder=1)
        #if not for_paper: T_plt.plot(T_x, T_y, c='0.8', alpha=0.4, lw=2, zorder=1)
        ## Interpolate at bin position in logspace, returning np.inf when outside range
        values = np.interp(np.log10(xbins), np.log10(T_x), np.log10(T_y), left=np.inf, right=np.inf)
        all_values[np.isfinite(values), T_idx] = 10.0**values[np.isfinite(values)] ## mask out the np.inf to avoid an error, leaving them as zero
    all_values = np.ma.masked_values(all_values, 0.0) ## masked array - mask out remaining zeros
    T_med = np.ma.median(all_values, axis=1)
    all_values_nan = np.ma.filled(all_values, np.nan) ## np.ma has no percentile so fill masked values with nans and use nanpercentile
    T_err1 = T_med - np.nanpercentile(all_values_nan, 16.0, axis=1)
    T_err2 = np.nanpercentile(all_values_nan, 84.0, axis=1) - T_med
    
    return xbins, T_med, T_err1, T_err2
#    if b > 0.0:
#        Sunlabel = "Sun+ 2009 [b = "+str(b)+"]"
#    else:
#        Sunlabel = "Sun+ 2009"
#    T_plt.errorbar(xbins, T_med, yerr=[T_err1, T_err2], zorder=3, fmt='o', mew=1.2, lw=1.2, ms=6, mec='none', mfc=suncol, color=suncol, ecolor=suncol, label=Sunlabel)

def get_temp_profile(T_file, Suncat, scaled=True, mu=0.59, b=0.0, h_scale=0.679):
    import os
    
    dat = np.loadtxt(T_file) ## log10(r/kpc)   T_3d (keV)   lower_boundary  upper_boundary
    ## Find r500 from catalogue of sun+2009 objects
    idx = np.where(Suncat['f2'] == os.path.basename(T_file).replace("_T",""))[0]
    assert len(idx)>0, "Could not find R500 for file "+T_file
    Sun_R500 = Suncat['f10'][idx] * (0.73/h_scale) ## R500 in kpc
    T_x = 10**dat[:,0] * (0.73/h_scale) / Sun_R500 * (1.0-b)**(1.0/3.0) ## r/r500
    T_y = dat[:,1] ## T_3D in keV
    T_y_errlo = dat[:,1] - dat[:,2]
    T_y_errhi = dat[:,3] - dat[:,1]
    if scaled: ## scale to T/T500
        Sun_T500 = G * mu * mp * Suncat['f19'][idx]*1e13*1.989e33 / (2.0 * Suncat['f10'][idx] * 3.085678e21) / 1.6021772e-9 / (1.0-b)**(2.0/3.0)
        T_y /= Sun_T500
        T_y_errlo /= Sun_T500
        T_y_errhi /= Sun_T500
        
    return T_x, T_y, T_y_errlo, T_y_errhi