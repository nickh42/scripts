import radprof2 as rp

bin_ratio = 1.3#1.4
topdir = "/data/curie4/nh444/project1/zoom_ins/"
outdir = "/home/nh444/data/project1/profiles/"

runs = ["LDRIntRadioEffBHVel/"]
subdirs = [#"c96_box_Arepo_new", #"c96_box_Arepo_new_LowSoft",
           #"c128_box_Arepo_new", #"c128_box_Arepo_new_LowSoft",
           #"c160_box_Arepo_new", #"c160_box_Arepo_new_LowSoft",
           #"c192_box_Arepo_new",
           #"c256_box_Arepo_new", #"c256_box_Arepo_new_LowSoft",
           #"c320_box_Arepo_new", #"c320_box_Arepo_new_LowSoft",
           #"c384_box_Arepo_new", #"c384_box_Arepo_new_LowSoft",
           #"c448_box_Arepo_new", #"c448_box_Arepo_new_LowSoft",
           "c512_box_Arepo_new",
           ]
runs = ["MDRInt/"]
subdirs = [
#           "c96_MDRInt", "c128_MDRInt", "c160_MDRInt", "c192_MDRInt",
#           "c224_MDRInt", "c256_MDRInt", "c288_MDRInt", "c320_MDRInt",
#           "c352_MDRInt", "c384_MDRInt", "c448_MDRInt",
           "c480_MDRInt",
#           "c512_MDRInt",
#           "c112_MDRInt", "c144_MDRInt", "c176_MDRInt", "c208_MDRInt",
#           "c240_MDRInt", "c272_MDRInt", "c304_MDRInt",
#           "c336_MDRInt",
#           "c368_MDRInt", "c400_MDRInt", "c410_MDRInt", "c432_MDRInt",
#           "c464_MDRInt",
#           "c496_MDRInt_SUBF",
           ]

mpc_units=True
# snapshot numbers
#steps = range(59, 100, 4)
steps = range(15, 17)
T_weighting = "mass"
rho_weighting = "mass"
temp_thresh = 4.0
suffix = "mass_weighted_default_Tvir4.0"
filter_type="default"
#suffix = "mass_weighted_Rasia2012_Tvir4.0"
#filter_type="Rasia2012"

# ## Gadget
# N = 1
# topdir = "/data/deboras/ewald/"
# outdir = "/home/nh444/data/project1/L-T_relations/gadget/"
# steps = [62]
# runs = ["m_14999_h_355_1278_z3"]
# subdirs = ["csf/", "csfbh/"]

for run in runs:
    for subdir in subdirs:
        for step in steps:
            print "Doing run", run, "snapshot", str(step), "subdir", subdir
            rp.radprof_halos(topdir+run+subdir+"/snap_"+str(step).zfill(3)+".hdf5",
                             N=0, M500_min=1e3*0.6774,
                             plot=False, stack=True, title=run,
                             outdir=outdir+run+"/"+subdir,
                             bin_ratio=bin_ratio, rmax=5.0,
                             T_weighting=T_weighting,
                             rho_weighting=rho_weighting,
                             suffix=suffix, filter_type=filter_type,
                             mpc_units=mpc_units, temp_thresh=temp_thresh)
