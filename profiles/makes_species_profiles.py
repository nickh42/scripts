#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 24 14:03:34 2017

@author: nh444
"""
def main():    
    basedir = "/home/nh444/data/project1/zoom_ins/LDRIntRadioEffBHVel/"
    simdirs = ["c96_box_Arepo",
               "c96_box_Arepo_RKLSF",
               #"c96_box_Arepo_2",
               #"c96_box_Arepo_new",
               #"c96_box_Arepo_new_ASMTH",
               #"c96_box_Arepo_new_PM1024",
               #"c96_box_Arepo_new_Ngb",
               "c96_FeedbackFix",
               "c96_FeedbackFix_OldGrav",
               #"c96_FeedbackFix_OldGrav_3",
               "c96_FeedbackFix_OldGrav_2",
               "c96_FeedbackFix_OldGrav_2_MHGG",
               ]
    outfilebase="species_mass_profiles_old_new_arepo_c96"
    labels = ["old arepo",
              "old arepo RK + LSF",
              #"old arepo no IND\_GRAV\_SOFT",
              #"old arepo modified",
              #"old arepo modified ASMTH 1.5 RCUT 6.0",
              #"old arepo modified PMGRID 1024",
              #"old arepo modified Ngb 256",
              "new arepo",
              "+ old gravity solver",
              #"+ IND\_GRAV\_SOFT",
              "+ RCUT + IND\_GRAV\_SOFT",
              "+ MH + GG",
              ]
    
#==============================================================================
#     simdirs = ["c128_box_Arepo",
#                "c128_box_Arepo_RKLSF",
#                "c128_new_FeedbackFix",
#                "c128_new_FeedbackFix_OldGrav",
#                "c128_FeedbackFix_OldGrav_2_openmpi_2",
#                "c128_FeedbackFix_OldGrav_2_MHGG",
#                ]
#     outfilebase="species_mass_profiles_old_new_arepo_c128"
#     labels = ["old arepo",
#               "old arepo RK + LSF",
#               "new arepo",
#               "+ old gravity solver",
#               "+ RCUT + IND\_GRAV\_SOFT",
#               "+ MH + GG",
#               ]
#==============================================================================
    
    mpc_units=True
    outdir = "/data/curie4/nh444/project1/profiles/LDRIntRadioEffBHVel/"
    snapnums = range(20,21)
    groupnum = 0
    
    #save_species_profiles(groupnum, snapnums, simdirs, basedir, outdir, mpc_units=mpc_units)
    plot_species_profiles(groupnum, 20, outdir, simdirs, plot="cummass", outfilebase=outdir+outfilebase, labels=labels)
    
def save_species_profiles(groupnum, snapnums, simdirs, basedir, outdir, mpc_units=False):
    import os
    import snap
    
    for simdir in simdirs:
        print "simdir =", simdir
        for snapnum in snapnums:
            if not os.path.exists(outdir+simdir):
                os.mkdir(outdir+simdir)
            sim = snap.snapshot(basedir+simdir, snapnum, mpc_units=mpc_units)
            sim.load_pos()
            sim.load_mass()
            sim.make_species_profile(group=groupnum, radius=sim.cat.group_r_crit200[groupnum], bintype="log", save_dat=outdir+simdir+"/grp"+str(groupnum)+"_species_profiles_snap"+str(snapnum)+".txt", save_plot="none", only_sel=False)
    
def plot_species_profiles(groupnum, snapnum, indir, simdirs, plot="density", outfilebase="species_mass_profiles", parttypes=[0,1,4], labels=None):
    """
    Load and plots profiles output by save_species_profiles() or snap.make_species_profile()
    Set variable plot to "density", "cummass" or "mass" to plot density, cumulative mass or mass 
    in each bin
    """
    import numpy as np
    import matplotlib.pyplot as plt
    
    assert max(parttypes) < 6
    if plot=="density":
        baseidx = 6 ## 4: mass 5: cum mass 6: density
    elif plot=="cummass":
        baseidx = 5
    elif plot=="mass":
        baseidx = 4
    else:
        assert False, "Variable plot must be one of 'density', 'cummass' or 'mass'"
    label = None
    fig, axarr = plt.subplots(len(parttypes), sharex=True, figsize=(8,12))

    axarr_twin = []
    for i, parttype in enumerate(parttypes):
        axarr[i].set_xscale('log')
        axarr_twin.append(axarr[i].twinx())
        if plot=="density":
            axarr[i].set_ylabel(r"$\rho$ / $\rho_{\mathrm{"+labels[0]+"}}$")
            axarr[i].set_ylim(0.0, 2.0)
        elif plot=="cummass":
            axarr[i].set_ylabel(r"M($<$R) / M($<$R)$_{\mathrm{"+labels[0]+"}}$")
            if parttype==0:
                axarr[i].set_ylim(0.0, 5.5)
        axarr[i].set_title("Type "+str(parttype))
    for ax in axarr_twin:
        ax.set_yscale('log')
        if plot=="density":
            ax.set_ylabel(r"$\rho$ [$h^{2}$ M$_{\odot}$ kpc$^{-3}$]")
        elif plot=="cummass":
            ax.set_ylabel(r"M($<$R) [$h^{-1}$ M$_{\odot}$]")
    axarr[-1].set_xlabel("R [$h^{-1}$ kpc]")
    plt.tight_layout()
    plt.subplots_adjust(wspace=0.1, hspace=0.1)
    
    dat1 = np.loadtxt(indir+simdirs[0]+"/grp"+str(groupnum)+"_species_profiles_snap"+str(snapnum)+".txt")
    axarr[0].set_xlim(1, max(dat1[:,3]))
    for i, simdir in enumerate(simdirs):
        dat = np.loadtxt(indir+simdir+"/grp"+str(groupnum)+"_species_profiles_snap"+str(snapnum)+".txt")
        if labels is not None:
            label = labels[i]
        #plt.step(dat[:,1], np.ma.masked_values(dat[:,idx], 0.0)*1e10, where='post', label=label)
        #plt.plot(dat[:,2], np.ma.masked_values(dat[:,idx], 0.0)*1e10, label=label)
        for j, parttype in enumerate(parttypes):
            axarr[j].plot(dat[:,2], np.ma.masked_values(dat[:, baseidx + parttype*3], 0.0)/np.ma.masked_values(dat1[:, baseidx + parttype*3], 0.0), lw=2, label=label)
            axarr_twin[j].plot(dat[:,2], np.ma.masked_values(dat[:, baseidx + parttype*3], 0.0)*1e10, lw=2, ls='dashed', alpha=0.5, label=label)
        
    axarr[1].legend(loc='best', fontsize=12)
    for ax in axarr:
        ymin, ymax = ax.get_ylim()
        ax.set_ylim(0.0, ymax)
    plt.savefig(outfilebase+"_snap"+str(snapnum)+"_"+plot+".pdf")
    print "Saved to", outfilebase+"_snap"+str(snapnum)+"_"+plot+".pdf"
   
if __name__ == "__main__":
    main()
    
    
    
    
    
    
    
    