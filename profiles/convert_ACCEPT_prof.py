#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 13 13:34:25 2016
Converts a selection of profiles from ACCEPT archive into median profiles
@author: nh444
"""
import os
import numpy as np

names = ["ABELL_0160",
         "ABELL_0262",
         "ABELL_0744",
         "ABELL_1991",
         "ABELL_2029",
         "ABELL_2462",
         "ABELL_2717",
         "ABELL_3581",
         "ESO_3060170",
         "ESO_5520200",
         "MKW_04",
         "RBS_0461",
         "RX_J1022.1+3830",
         ]

metallicity = [0.4,
               0.42,
               0.43,
               0.3,
               0.42,
               0.24,
               0.3,
               0.67,
               0.61,
               0.3,
               0.43,
               0.3,
               0.45,
               ]# metallicity in solar units

z = [0.0447,
     0.0163,
     0.0729,
     0.0587,
     0.0669,
     0.0733,
     0.0498,
     0.023,
     0.0358,
     0.0314,
     0.02,
     0.0296,
     0.0543,
     ]
     
R = [626,
     644,
     681,
     749,
     659,
     646,
     732,
     593,
     690,
     598,
     538,
     637,
     631] ## r500 in kpc

datadir = "/data/vault/nh444/ObsData/profiles/Sun_ACCEPT_profiles"
outdir = datadir

for i, name in enumerate(names):
    print "Converting", name

    ## Read in and process profile data
    infile = os.path.normpath(datadir)+"/"+name+"_profiles.dat"
    data = np.genfromtxt(infile)
    h = 0.7 #as in Cavagnolo+2009
    Rin = data[:,1] * 1000.0 ## kpc
    Rout = data[:,2] * 1000.0
    binvol = 4.0 / 3.0 * np.pi * (Rout**3 - Rin**3) * 3.085678e21**3 ## bin volumes in cm^3
    
    T = data[:,13] # X-ray temperature in keV
    Terr = data[:,14]
    ne = data[:,3] # electron number density cm^-3
    neerr = data[:,4]
    K = data[:,5]
    Kerr = data[:,7]
    assert len(T) == len(ne), "Temperature and density profiles have different number of radial bins"
    N = int(len(ne))
    
    ag_hydrogen = 0.70683 # Anders & Grevesse solar mass fractions 
    ag_helium = 0.27431
    ag_metals = 0.01886
    nelec = 1.0 + 0.5*ag_helium/ag_hydrogen + 0.5*metallicity[i]*ag_metals/ag_hydrogen # n_e / n_H i.e. number of free electrons per H nucleus
    f = ag_hydrogen/(ag_hydrogen+ag_helium+metallicity[i]*ag_metals) #hydrogen mass fraction f=0.7162864 for Z=0.3 solar
    #f = 0.76 # for consistency with xspec code
    mmean = 4.0 / (1.0 + 3.0*f + 4.0*f*nelec) #mean molecular weight per particle (ions + electrons) in units of mp
    rho = ne / nelec * 1.67262171e-24 / f # g cm^-3 hydrogen number density is approx mass density times f divided by mass of proton
    rhoerr = neerr / nelec * 1.67262171e-24 / f
    rho_crit = (3.0/8.0 / np.pi / 6.67408e-8 * (h * 100 / 3.086e+19)**2.0) * (0.3*(1+z[i])**3+0.7) ## Critical density at redshift z in g cm^-3
    
    mass = rho * binvol ## g
    masserr = rhoerr * binvol
    mass_tot = np.sum(mass)

    r = 10**((np.log10(Rin)+np.log10(Rout))/2) ## take radius as log-average of bin radii
    r[np.where(Rin == 0.0)] = Rout[np.where(Rin == 0.0)] / 2.0
    assert len(r) == N

    np.savetxt(os.path.normpath(datadir)+"/"+name+"_derived_profiles.dat",
               np.c_[Rin, Rout, r, r/R[i], T, Terr, ne, neerr, rho, rhoerr, rho/rho_crit, masserr, K, Kerr,],
                header="Rin(kpc) Rout(kpc) r(kpc) r/r500 T(keV) Terr ne(cm^-3) neerr rho(gcm^-3) rhoerr rho/rho_crit mass(g) masserr K(keV cm^2) Kerr")