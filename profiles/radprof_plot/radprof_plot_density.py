from profiles.radprof_plot import radprof_plot

indirs = ["/data/curie4/nh444/project1/profiles/"]

outdir = "/home/nh444/Documents/paper/"
outdir = "/data/curie4/nh444/project1/profiles/"
filename = "snap_025_profiles_box_Arepo_new_plus_zooms_Rasia_Tvir3_mass_weighted.hdf5"
filename = "snap_025_profiles_box_Arepo_new_plus_zooms_Rasia_mass_weighted.hdf5"
filename = "snap_025_profiles_box_Arepo_new_plus_zooms_default_mass_weighted.hdf5"
#filename = "snap_025_profiles_box_Arepo_new_plus_zooms_Rasia_mass_weighted_winds.hdf5"
#filename = "snap_025_profiles_box_Arepo_new_plus_zooms_Rasia_mass_weighted_nowinds.hdf5"
filename = "snap_025_profiles_box_Arepo_new_plus_zooms_default_mass_weighted_Tvir4.0.hdf5"
filename = "snap_025_profiles_MDRInt_plus_zooms_default_mass_weighted_Tvir4.0.hdf5"
radprof_plot([indir+filename for indir in indirs], outdir=outdir,
             plot_only="gasfrac", plot_colour_scale=True, for_paper=False,
             add_prof_labels=False, prof_labels=None,
             add_file_labels=True, file_labels=None,
             sort_by_M500=True,
             #compare="Sun",
             #M500_min=3.15, M500_max=25,## (1e13 Msun) Use negative for inf
             #rmin=0.007, rmax=1.0, ##-1 for infinity
             #ylims=[1e-5, 1],
             compare="REXCESS",
             M500_min=8.0, M500_max=-1,## (1e13 Msun) Use negative for inf
             rmin=0.007, rmax=2.0, ##-1 for infinity
             #ylims=[2e-5, 0.1],
             fout_suffix="", filtered=True,
             plot_mean=False, plot_median=False,
             scaled=False, scaledbyR=True)
    
