from profiles.radprof_plot import radprof_plot

indirs = ["/data/curie4/nh444/project1/profiles/"]
outdir = "/data/curie4/nh444/project1/profiles/"
filename = "snap_025_profiles_c320_noAGN.hdf5"

prof_labels = ["AGN feedback", "No AGN"]

radprof_plot([indir+filename for indir in indirs], outdir=outdir,

             plot_only="density", plot_colour_scale=False, for_paper=True,
             add_prof_labels=True, prof_labels=prof_labels,
             add_file_labels=False, file_labels=None,
             sort_by_M500=False,
             M500_min=0, M500_max=-1,## (1e13 Msun) Use negative for inf
             #nplot=1,
             rmin=0.01, rmax=-1, ##-1 for infinity
             plot_mean=False, plot_median=False,
             compare="none", plot_T2500=True)
    
