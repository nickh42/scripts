from profiles.radprof_plot import radprof_plot

indirs = ["/data/curie4/nh444/project1/profiles/"]

outdir = "/home/nh444/Documents/paper/"
outdir = "/data/curie4/nh444/project1/profiles/"
filename = "snap_025_profiles_box_Arepo_new_plus_zooms_Rasia_Tvir3_mass_weighted.hdf5"
filename = "snap_025_profiles_box_Arepo_new_plus_zooms_Rasia_mass_weighted.hdf5"
#filename = "snap_025_profiles_box_Arepo_new_plus_zooms_default_mass_weighted.hdf5"
filename = "snap_025_profiles_box_Arepo_new_plus_zooms_default_mass_weighted_Tvir4.0.hdf5"

indirs = ["/data/curie4/nh444/project1/profiles/L40_512_LDRIntRadioEff_noBH/",
          #"/data/curie4/nh444/project1/profiles/L40_512_LDRIntRadioEffBHVel/",
          ]
filename = "snap_025_profiles_mass_weighted_default_Tvir4.0.hdf5"

indirs = ["/data/curie4/nh444/project1/profiles/LDRIntRadioEffBHVel/c192_box_Arepo_new/"]
filename = "snap_025_profiles_mass_weighted_default_Tvir4.0.hdf5"

#indirs = ["/data/curie4/nh444/project1/profiles/LDRIntRadioEffBHVel/c320/",
#          "/data/curie4/nh444/project1/profiles/LDRIntRadioEffBHVel/c320_noAGN/",
#          "/data/curie4/nh444/project1/profiles/LDRIntRadioEffBHVel/c320_box_Arepo_new/"]
#filename = "snap_025_profiles_mass_weighted_default_Tvir4.0.hdf5"
#file_labels = ["c320", "c320 no BHs", "c320 (larger soft)"]

#indir = "/data/curie4/nh444/project1/profiles/LDRIntRadioEffBHVel/c320_box_Arepo_new/"
indir = "/data/curie4/nh444/project1/profiles/LDRIntRadioEffBHVel/c384_box_Arepo_new/"
indir = "/data/curie4/nh444/project1/profiles/LDRIntRadioEffBHVel/c448_box_Arepo_new/"
#indir = "/data/curie4/nh444/project1/profiles/LDRIntRadioEffBHVel/c160_box_Arepo_new/"
filenames = ["snap_025_profiles_mass_weighted_default_Tvir4.0.hdf5",
             "snap_024_profiles_mass_weighted_default_Tvir4.0.hdf5",
             "snap_023_profiles_mass_weighted_default_Tvir4.0.hdf5",
             "snap_022_profiles_mass_weighted_default_Tvir4.0.hdf5",
             "snap_021_profiles_mass_weighted_default_Tvir4.0.hdf5",
             "snap_020_profiles_mass_weighted_default_Tvir4.0.hdf5",]
             #"snap_015_profiles_mass_weighted_default_Tvir4.0.hdf5",
             #"snap_010_profiles_mass_weighted_default_Tvir4.0.hdf5"]
file_labels = ["$z=0$","$z=0.2$","$z=0.4$","$z=0.6$","$z=0.8$","$z=1$","$z=2$","$z=3$",]


#indir = "/data/curie4/nh444/project1/profiles/LDRIntRadioEffBHVel/c160_box_Arepo_new/"
#filenames = ["snap_025_profiles_mass_weighted_default_Tvir4.0.hdf5",
#             "snap_025_profiles_mass_weighted_Rasia2012_Tvir4.0.hdf5",
#             "snap_025_profiles_mass_weighted_Rasia_Tvir4.0.hdf5"]
#file_labels = ["default", "Rasia+2012", "Rasia+2012 rescaled"]
             
radprof_plot(#[indir+filename for indir in indirs],
             [indir+f for f in filenames],
             plot_only="temperature", plot_colour_scale=False, for_paper=True,
             add_prof_labels=False, prof_labels=None,
             add_file_labels=True, file_labels=file_labels,
             sort_by_M500=True,
             #M500_min=20, M500_max=-1,## (1e13 Msun) Use negative for inf
             nplot=1,
             #title=r"c160:  M$_{500}(z=0) = 3.8 \times 10^{13}$",
             title=r"c448:  M$_{500}(z=0) = 9.1 \times 10^{14}$",
             #title=r"c384:  M$_{500}(z=0) = 5.9 \times 10^{14}$",
             #fout_suffix="no_BHs",
             fout_suffix="c448_z",
             filtered=True,
             rmin=0.007, rmax=2, ##-1 for infinity
             ylims=[0.0,2.5],
             plot_mean=False, plot_median=False,
             outdir=outdir,
             colours = ["k","#c7673e","#78a44f","#6b87c8","#934fce","#e6ab02","#be558f","#47af8d"],
             compare="REXCESS")
    
