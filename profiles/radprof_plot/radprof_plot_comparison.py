from profiles.radprof_plot import radprof_plot

#indir = "/data/curie4/nh444/project1/profiles/"
#outdir = "/data/curie4/nh444/project1/profiles/"
#filenames = ["snap_025_profiles_box_Arepo_new_plus_zooms_default_mass_weighted_1.3_volcor.hdf5",
#             "snap_025_profiles_box_Arepo_new_plus_zooms_default_mass_weighted_1.3.hdf5",
#             ]
#file_labels = ["volume-corrected", "non-volume corrected"]

#indir = "/data/curie4/nh444/project1/profiles/"
#outdir = "/data/curie4/nh444/project1/profiles/L40_512_LDRIntRadioEffBHVel/"
#indir = outdir
#filenames = ["snap_025_profiles_mass_weighted_default_Tvir4.0.hdf5",
#             "snap_025_profiles_mass_weighted_Rasia2012_Tvir4.0.hdf5"]
#file_labels = ["Fiducial cut", "Rasia+2012 rescaled"]

#indir = "/home/nh444/data/project1/profiles/LDRIntRadioEffBHVel/"
#filenames = ["c96_box_Arepo_new/snap_025_profiles_mass_weighted_default_Tvir4.0.hdf5",
#             "c96_box_Arepo_new_noBH/snap_025_profiles_mass_weighted_default_Tvir4.0.hdf5"]
#file_labels = ["AGN", "no BH"]
#outdir = "/home/nh444/data/project1/profiles/LDRIntRadioEffBHVel/c96_box_Arepo_new_noBH/"

indir = "/data/curie4/nh444/project1/profiles/"
outdir = "/data/curie4/nh444/project1/profiles/"
filenames = ["L40_512_LDRIntRadioEff_noBH/snap_025_profiles_mass_weighted_default_Tvir4.0.hdf5",
             "L40_512_NoDutyRadioWeak/snap_025_profiles_mass_weighted_default_Tvir4.0.hdf5",
             "L40_512_NoDutyRadioInt/snap_025_profiles_mass_weighted_default_Tvir4.0.hdf5",
             "L40_512_DutyRadioWeak/snap_025_profiles_mass_weighted_default_Tvir4.0.hdf5",
             #"L40_512_LDRIntRadioEffBHVel/snap_025_profiles_mass_weighted_default_Tvir4.0.hdf5",
             "L40_512_MDRIntRadioEff/snap_025_profiles_mass_weighted_default_Tvir4.0.hdf5",
             ]
file_labels = ["no BHs", "weak radio", "stronger radio", "quasar duty cycle", "fiducial"]
fout_suffix = "models"

#==============================================================================
# indir = "/home/nh444/data/project1/profiles/L40_512_MDRIntRadioEff/"
# outdir = "/home/nh444/data/project1/profiles/L40_512_MDRIntRadioEff/"
# filenames = ["snap_025_profiles_mass_weighted_default_Tvir4.0.hdf5", "snap_025_profiles_mass_weighted_default.hdf5"]
# file_labels = ["T < 4 T$_{200}$", "no T threshold"]
# fout_suffix = "temp_thresh"
#==============================================================================

radprof_plot([indir+filename for filename in filenames], outdir=outdir,
             plot_only="density",
             #plot_only="temperature",
             #plot_only="entropy",
             #plot_only="gasfrac",
             #plot_only="pressure",
             plot_colour_scale=False, for_paper=False, plot_residual=False,
             add_prof_labels=False, prof_labels=None,
             add_file_labels=True, file_labels=file_labels,
             sort_by_M500=True,
             #M500_min=3, M500_max=4,## (1e13 Msun) Use negative for inf
             #M500_min=3.0, M500_max=-1,
             nplot=5,
             #title="Rasia+2012 cut",
             fout_suffix=fout_suffix, filtered=True,
             rmin=0.007, rmax=2.0, ##-1 for infinity
             #ylims=[3, 1e5],
             ylims = [1e-29, 2e-25],
             plot_mean=False, plot_median=False,
             compare="none")
    
