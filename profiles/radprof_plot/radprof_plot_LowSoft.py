from profiles.radprof_plot import radprof_plot

indirs = ["/data/curie4/nh444/project1/profiles/"]
outdir = "/data/curie4/nh444/project1/profiles/"
filename = "snap_025_profiles_c384_box_Arepo_LowSoft_Rasia_Tvir3.0_mass_weighted.hdf5"
filename = "snap_025_profiles_c320_box_Arepo_LowSoft_Rasia_Tvir3.0_mass_weighted.hdf5"
filename = "snap_025_profiles_c160_box_Arepo_LowSoft_Rasia_Tvir3.0_mass_weighted.hdf5"
filename = "snap_025_profiles_c128_box_Arepo_LowSoft_Rasia_Tvir3.0_mass_weighted.hdf5"
filename = "snap_025_profiles_c96_box_Arepo_LowSoft_Rasia_Tvir3.0_mass_weighted.hdf5"
filename = "snap_025_profiles_box_Arepo_LowSoft_Tvir3_mass_weighted.hdf5"
filename = "snap_025_profiles_box_Arepo_LowSoft_Tvir3_LowMass_mass_weighted.hdf5"
#filename = "snap_025_profiles_box_Arepo_LowSoft_Tvir3_HighMass_mass_weighted.hdf5"

prof_labels = ["High Soft", "Low Soft"]*7
#from palettable.colorbrewer.qualitative import Paired_12 as pallette
#colours = pallette.hex_colors
colours = ["#78a44f","#78a44f","#6b87c8","#6b87c8","#c7673e","#c7673e"]##low mass
#colours = ["#934fce","#934fce","#e6ab02","#e6ab02","#be558f","#be558f","#47af8d","#47af8d"]##high mass
#colours = ["#78a44f","#78a44f","#6b87c8","#6b87c8","#c7673e","#c7673e", "#934fce","#934fce","#e6ab02","#e6ab02","#be558f","#be558f","#47af8d","#47af8d"]
ls = ['solid', 'dashed']*7

radprof_plot([indir+filename for indir in indirs], outdir=outdir,
             plot_only="all", plot_colour_scale=True, for_paper=False,
             add_prof_labels=True, #prof_labels=prof_labels,
             colours=colours,
             ls=ls,
             leglabelsize = 10,
             add_file_labels=False, file_labels=None,
             #title="c96",
             sort_by_M500=False,
             M500_min=0, M500_max=-1,## (1e13 Msun) Use negative for inf
             #nplot=1,
             rmin=0.02, rmax=-1, ##-1 for infinity
             plot_mean=False, plot_median=False,
             compare="REXCESS", plot_T2500=True)
    
