from profiles.radprof_plot import radprof_plot

indirs = ["/home/nh444/data/project1/profiles/LDRIntRadioEffBHVel/c512_box_Arepo_new/"]
indirs = ["/home/nh444/data/project1/profiles/MDRInt/c384_MDRInt/",
          "/home/nh444/data/project1/profiles/MDRInt/c448_MDRInt/",
          "/home/nh444/data/project1/profiles/LDRIntRadioEffBHVel/c512_box_Arepo_new/",
          "/home/nh444/data/project1/profiles/MDRInt/c512_MDRInt/",
          ]
file_labels = ["c384\_MDRInt",
               "c448\_MDRInt",
               "c512\_LDRInt",
               "c512\_MDRInt",
               ]
filename = "snap_025_profiles_mass_weighted_default_Tvir4.0.hdf5"

outdir = "/home/nh444/data/project1/profiles/MDRInt/c512_MDRInt/"

radprof_plot([indir+filename for indir in indirs], outdir=outdir,
             plot_only="temperature", plot_colour_scale=False, for_paper=False,
             add_prof_labels=False, prof_labels=None,
             add_file_labels=True, file_labels=file_labels,
             sort_by_M500=True,
             z_label=True,
             #M500_min=6, M500_max=-1,
             nplot=1,
             fout_suffix="", filtered=True,
             rmin=0.003, rmax=2, ##-1 for infinity
             plot_mean=False, plot_median=False,
             compare="REXCESS")
    