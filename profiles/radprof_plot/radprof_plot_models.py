from profiles.radprof_plot import radprof_plot

indirs = ["/data/curie4/nh444/project1/profiles/L40_512_NoDutyRadioWeak_HalfThermal_FixEta/",
          "/data/curie4/nh444/project1/profiles/L40_512_NoDutyRadioInt_HalfThermal_FixEta/",
          "/data/curie4/nh444/project1/profiles/L40_512_LowDutyRadioWeak_HalfThermal_FixEta/",
          "/data/curie4/nh444/project1/profiles/L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff/",
          ]
file_labels = ["Weak radio", ## same order as indirs (its not sorted)
               "Stronger radio",
               "Quasar duty cycle",
               "Combined"]
filename = "snap_025_profiles_mass_weighted_Tvir_thresh_2.5.hdf5"
filename = "snap_025_profiles_mass_weighted_default_Tvir4.0.hdf5"

indirs = ["/data/curie4/nh444/project1/profiles/L40_512_DutyIntRadioWeak_Thermal/",
          "/data/curie4/nh444/project1/profiles/L40_512_DutyInt1RadioWeak_Thermal_FixEta/",
          "/data/curie4/nh444/project1/profiles/L40_512_LowDutyRadioWeak_HalfThermal_FixEta/",
          "/data/curie4/nh444/project1/profiles/L40_512_NoDutyRadioInt_HalfThermal_FixEta/",
          "/data/curie4/nh444/project1/profiles/L40_512_NoDutyRadioIntLow_HalfThermal_FixEta/",
          "/data/curie4/nh444/project1/profiles/L40_512_NoDutyRadioWeak_HalfThermal_FixEta/",
          #"/data/curie4/nh444/project1/profiles/L40_512_LDRIntRadioEff/",
          ]
file_labels = ["Qt0.05 R0.001 Th",
               "Qt0.02 R0.001 Th",
            "Qt0.01 R0.001 HalfTh",
            "Qt0 R0.01 HalfTh",
            "Qt0 R0.005 HalfTh",
            "Qt0 R0.001 HalfTh",
            "best model (large soft)"]
filename = "snap_025_profiles_mass_weighted_default_Tvir4.0.hdf5"

indirs = [#"/data/curie4/nh444/project1/profiles/L40_512_NoDutyRadioWeak/",
            #"/data/curie4/nh444/project1/profiles/L40_512_NoDutyRadioInt/",
            #"/data/curie4/nh444/project1/profiles/L40_512_DutyRadioWeak/",
            "/data/curie4/nh444/project1/profiles/L40_512_LDRIntRadioEffBHVel/",
            #"/data/curie4/nh444/project1/profiles/L40_512_LDRHighRadioEff/",
            #"/data/curie4/nh444/project1/profiles/L40_512_MDRIntRadioEff/",
            #"/data/curie4/nh444/project1/profiles/L40_512_LDRIntRadioEff_noBH/",
           ]
file_labels = [#"Weak radio",
              #"Stronger radio",
              #"Quasar duty cycle",
              "Fiducial",#"Combined",
              #"Longer radio duty",
              #"Longer quasar duty",
              #"No BHs",
              ]
filename = "snap_025_profiles_mass_weighted_default_Tvir4.0.hdf5"

indirs = ["/home/nh444/data/project1/profiles/LDRIntRadioEffBHVel/c128_box_Arepo_new/",
          "/home/nh444/data/project1/profiles/MDRInt/c128_MDRInt/"]
filename = "snap_025_profiles_mass_weighted_default_Tvir4.0.hdf5"
file_labels = ["delta T = 10 Myr", "delta T = 25 Myr"]


indirs = ["/data/curie4/nh444/project1/profiles/"]
filename = "snap_025_profiles_LDRIntRadioEff_plus_zooms_default_mass_weighted_Tvir4.0.hdf5"
file_labels = ["$\delta t$ = 10 Myr"]
filename = "snap_025_profiles_MDRInt_plus_zooms_default_mass_weighted_Tvir4.0.hdf5"
file_labels = ["$\delta t$ = 25 Myr"]

#outdir = "/data/curie4/nh444/project1/profiles/"
#outdir = indirs[0]
outdir = "/data/vault/nh444/VaultDocs/meetings/new_model/"

plot_only = ["density", "temperature"]#, "entropy", "pressure"]
plot_only = ["entropy", "pressure"]
for plot in plot_only:
    radprof_plot([indir+filename for indir in indirs], outdir=outdir,
                 #plot_only="density", scaled=False,
                 #plot_only="temperature", scaled=True, #ylims=[0, 3.5],
                 #plot_only="entropy", scaled=True,
                 plot_only=plot, scaled=True,
                 fout_suffix="",
                 for_paper=True, plot_colour_scale=True,
                 add_prof_labels=False, prof_labels=None,
                 add_file_labels=True, file_labels=file_labels,
                 sort_by_M500=True, plot_residual=False,
                 M500_min=2.0, M500_max=15,## (1e13 Msun) Use negative for inf
                 #nplot=1,
                 #b = 0.3,
                 #ylims=[0.0,3.0],
                 title=file_labels[0],
                 rmin=0.01, rmax=2, ##-1 for infinity
                 plot_mean=False, plot_median=False, scat_ind=[],
                 compare="Sun")
                 #compare="REXCESS")
    
#==============================================================================
# snapnums = [25, 24, 23, 22]
# z = [0.0, 0.2, 0.4, 0.6]
# for i, snapnum in enumerate(snapnums):
#     filename = "snap_"+str(snapnum).zfill(3)+"_profiles_mass_weighted_default_Tvir4.0.hdf5"
#     radprof_plot([indir+filename for indir in indirs], outdir=outdir,
#                  plot_only="temperature", scaled=True, ylims=[0, 2.5],
#                  fout_suffix="fiducial",
#                  #fout_suffix="quasar",
#                  #fout_suffix="radio",
#                  for_paper=False, plot_colour_scale=True,
#                  add_prof_labels=False, prof_labels=None,
#                  add_file_labels=True, file_labels=file_labels,
#                  sort_by_M500=True, plot_residual=False,
#                  M500_min=2.0, M500_max=-1,## (1e13 Msun) Use negative for inf
#                  b = 0.2,
#                  title=file_labels[0]+" z={:.1f}".format(z[i]),
#                  rmin=0.01, rmax=2, ##-1 for infinity
#                  plot_mean=False, plot_median=False, scat_ind=[],
#                  compare="Sun", filtered=True)    
# 
#==============================================================================
