from profiles.radprof_plot import radprof_plot

indirs = ["/data/curie4/nh444/project1/profiles/L40_512_NoDutyRadioWeak_HalfThermal_FixEta/",
          "/data/curie4/nh444/project1/profiles/L40_512_NoDutyRadioInt_HalfThermal_FixEta/",
          "/data/curie4/nh444/project1/profiles/L40_512_LowDutyRadioWeak_HalfThermal_FixEta/",
          "/data/curie4/nh444/project1/profiles/L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff/",
          ]
file_labels = ["Ref", ## same order as indirs (its not sorted)
               "Stronger radio",
               "Quasar duty cycle",
               "Best"]
filename = "snap_025_profiles_mass_weighted_Tvir_thresh_2.5.hdf5"

indirs = ["/data/curie4/nh444/project1/profiles/L40_512_DutyInt1RadioWeak_Thermal_FixEta/",
            "/data/curie4/nh444/project1/profiles/L40_512_DutyIntRadioWeak_Thermal/",
            "/data/curie4/nh444/project1/profiles/L40_512_LowDutyRadioWeak_HalfThermal_FixEta/",
            "/data/curie4/nh444/project1/profiles/L40_512_NoDutyRadioInt_HalfThermal_FixEta/",
            "/data/curie4/nh444/project1/profiles/L40_512_NoDutyRadioIntLow_HalfThermal_FixEta/",
            "/data/curie4/nh444/project1/profiles/L40_512_NoDutyRadioWeak_HalfThermal_FixEta/",]
file_labels = ["DutyInt1RadioWeak-Thermal",
            "DutyIntRadioWeak-Thermal",
            "LowDutyRadioWeak-HalfThermal",
            "NoDutyRadioInt-HalfThermal",
            "NoDutyRadioIntLow-HalfThermal",
            "NoDutyRadioWeak-HalfThermal",]
filename = "snap_025_profiles_mass_weighted_default_Tvir4.0.hdf5"

outdir = "/data/curie4/nh444/project1/profiles/"

radprof_plot([indir+filename for indir in indirs], outdir=outdir,
             #title="LDRIntRadioEffBHVel",
             #title="no pressure criterion",
             #title="LDRIntRadioEffBHVel  T $<$ 2.0 T200",
             #title ="c128 snap 20 (z=1.0)",
             #title=" ", redshift=1.0,
             plot_only="temperature", plot_colour_scale=True, for_paper=True,
             add_prof_labels=False, prof_labels=None,
             add_file_labels=True, file_labels=file_labels,
             sort_by_M500=True,
             M500_min=4.3, M500_max=-1,## (1e13 Msun) Use negative for inf
             #nplot=1,
             rmin=0.01, rmax=-1, ##-1 for infinity
             plot_mean=False, plot_median=True, scat_ind=[],
             compare="Sun")
    
