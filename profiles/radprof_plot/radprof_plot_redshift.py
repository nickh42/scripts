from profiles.radprof_plot import radprof_plot

indir = "/data/curie4/nh444/project1/profiles/MDRInt/c336_MDRInt/"
snapnums = range(83, 100, 8)
filenames = [indir + "snap_" + str(snapnum).zfill(3) +
             "_profiles_mass_weighted_default_Tvir4.0" +
             ".hdf5" for snapnum in snapnums]
file_labels = [str(snapnum) for snapnum in snapnums]
outdir = "/data/curie4/nh444/project1/profiles/MDRInt/c336_MDRInt/"

indir = "/data/curie4/nh444/project1/profiles/"
#indir = "/data/curie4/nh444/project1/profiles/L40_512_MDRIntRadioEff/"
snapnums = range(25, 26, 1)
filenames = [indir + "snap_" + str(snapnum).zfill(3) +
             "_profiles_MDRInt_plus_zooms_default_mass_weighted_Tvir4.0" +
             #"_profiles_mass_weighted_default_Tvir4.0" +
             ".hdf5" for snapnum in snapnums]
file_labels = [str(snapnum) for snapnum in snapnums]
outdir = "/data/curie4/nh444/project1/profiles/"

radprof_plot(filenames, outdir=outdir,
             plot_only="temperature", plot_colour_scale=True, for_paper=False,
             add_prof_labels=False, prof_labels=None,
             add_file_labels=True, file_labels=file_labels,
             sort_by_M500=True,
             M500_min=3, M500_max=100,  # 1e13 Msun
             #nplot=1,
             fout_suffix="", filtered=True,
             rmin=0.003, rmax=2,  # -1 for infinity
             plot_mean=False, plot_median=False,
             compare=None, scaled=True)
