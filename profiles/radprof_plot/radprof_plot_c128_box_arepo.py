from profiles.radprof_plot import radprof_plot

indirs = ["/data/curie4/nh444/project1/profiles/"]
outdir = "/data/curie4/nh444/project1/profiles/"
filename = "snap_025_profiles_c128_box_Arepo_mass_weighted_Tvir_thresh_2.5.hdf5"
radprof_plot([indir+filename for indir in indirs], outdir=outdir,
             #title="LDRIntRadioEffBHVel",
             #title="no pressure criterion",
             #title="LDRIntRadioEffBHVel  T $<$ 2.0 T200",
             #title ="c128 snap 20 (z=1.0)",
             #title=" ", redshift=1.0,
             plot_only="all", plot_colour_scale=True, for_paper=True,
             add_prof_labels=True, prof_labels=None,
             add_file_labels=True, file_labels=file_labels,
             sort_by_M500=True,
             M500_min=0, M500_max=-1,## (1e13 Msun) Use negative for inf
             #nplot=1,
             rmin=0.01, rmax=-1, ##-1 for infinity
             plot_mean=False, plot_median=False,
             compare="Sun", plot_T2500=True)