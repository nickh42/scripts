from profiles.radprof_plot import radprof_plot

indirs = ["/data/curie4/nh444/project1/profiles/L40_512_NoDutyRadioWeak_HalfThermal_FixEta/",
          "/data/curie4/nh444/project1/profiles/L40_512_NoDutyRadioInt_HalfThermal_FixEta/",
          "/data/curie4/nh444/project1/profiles/L40_512_LowDutyRadioWeak_HalfThermal_FixEta/",
          "/data/curie4/nh444/project1/profiles/L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff/",
          ]

outdir = "/data/curie4/nh444/project1/profiles/"
filename = "snap_025_profiles_mass_weighted_Tvir_thresh_2.5.hdf5"
filename = "snap_025_profiles_mass_weighted_Rasia_Tvir_thresh_3.0.hdf5"
file_labels = ["Ref", ## same order as indirs (its not sorted)
               "Stronger radio",
               "Quasar duty cycle",
               "Combined"]
radprof_plot([indir+filename for indir in indirs], outdir=outdir,
             plot_only="gasfrac", plot_colour_scale=False, for_paper=True,
             add_prof_labels=False, prof_labels=None,
             add_file_labels=True, file_labels=file_labels,
             sort_by_M500=True,
             M500_min=1.0, M500_max=-1,## (1e13 Msun) Use negative for inf
             #nplot=1,
             rmin=0.01, rmax=-1, ##-1 for infinity
             plot_mean=False, plot_median=True,
             compare="none", plot_T2500=True)
    
