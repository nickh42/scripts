from profiles.radprof_plot import radprof_plot

indirs = ["/data/curie4/nh444/project1/profiles/"]

outdir = "/data/curie4/nh444/project1/profiles/"
filename = "snap_025_profiles_box_Arepo_new_plus_zooms_default_mass_weighted_Tvir4.0.hdf5"
radprof_plot([indir+filename for indir in indirs], outdir=outdir,
             plot_only="all", plot_colour_scale=True, for_paper=True,
             add_prof_labels=False, prof_labels=None,
             add_file_labels=True, file_labels=None,
             sort_by_M500=True,
             M500_min=6, M500_max=-1,
             ## lower limit of TNG sample is 5.6e13 but this includes grp2 which has a big AGN bubble
             fout_suffix="TNG_comparison", filtered=True,
             rmin=0.003, rmax=2, ##-1 for infinity
             #ylims=[3, 1e5],
             plot_mean=False, plot_median=False,
             compare="TNG")

radprof_plot([indir+filename for indir in indirs], outdir=outdir,
             plot_only="gasfrac", plot_colour_scale=True, for_paper=True,
             add_prof_labels=False, prof_labels=None,
             add_file_labels=True, file_labels=None,
             sort_by_M500=True,
             M500_min=6, M500_max=-1,
             ## lower limit of TNG sample is 5.6e13 but this includes grp2 which has a big AGN bubble
             fout_suffix="TNG_comparison", filtered=True,
             rmin=0.003, rmax=2, ##-1 for infinity
             #ylims=[3, 1e5],
             plot_mean=False, plot_median=False,
             compare="TNG")

#==============================================================================
# radprof_plot([indir+filename for indir in indirs], outdir=outdir,
#              plot_only="density", plot_colour_scale=True, for_paper=True,
#              add_prof_labels=False, prof_labels=None,
#              add_file_labels=True, file_labels=None,
#              sort_by_M500=True,
#              M500_min=6, M500_max=-1,
#              ## lower limit of TNG sample is 5.6e13 but this includes grp2 which has a big AGN bubble
#              fout_suffix="", filtered=True,
#              rmin=0.003, rmax=2, ##-1 for infinity
#              #ylims=[3, 1e5],
#              plot_mean=False, plot_median=False,
#              compare="TNG")
#     
# radprof_plot([indir+filename for indir in indirs], outdir=outdir,
#              plot_only="temperature", plot_colour_scale=True, for_paper=True,
#              add_prof_labels=False, prof_labels=None,
#              add_file_labels=True, file_labels=None,
#              sort_by_M500=True,
#              M500_min=6, M500_max=-1,
#              ## lower limit of TNG sample is 5.6e13 but this includes grp2 which has a big AGN bubble
#              fout_suffix="", filtered=True,
#              rmin=0.003, rmax=2, ##-1 for infinity
#              #ylims=[3, 1e5],
#              plot_mean=False, plot_median=False,
#              compare="TNG")
# 
# radprof_plot([indir+filename for indir in indirs], outdir=outdir,
#              plot_only="entropy", plot_colour_scale=True, for_paper=True,
#              add_prof_labels=False, prof_labels=None,
#              add_file_labels=True, file_labels=None,
#              sort_by_M500=True,
#              M500_min=6, M500_max=-1,
#              ## lower limit of TNG sample is 5.6e13 but this includes grp2 which has a big AGN bubble
#              fout_suffix="", filtered=True,
#              rmin=0.003, rmax=2, ##-1 for infinity
#              #ylims=[3, 1e5],
#              plot_mean=False, plot_median=False,
#              compare="TNG")
#==============================================================================
