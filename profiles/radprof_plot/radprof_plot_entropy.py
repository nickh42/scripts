from profiles.radprof_plot import radprof_plot

indirs = ["/data/curie4/nh444/project1/profiles/"]

outdir = "/data/curie4/nh444/project1/profiles/"
filename = "snap_025_profiles_box_Arepo_new_plus_zooms_Rasia_Tvir3_mass_weighted.hdf5"
filename = "snap_025_profiles_box_Arepo_new_plus_zooms_Rasia_mass_weighted.hdf5"
#filename = "snap_025_profiles_box_Arepo_new_plus_zooms_default_mass_weighted.hdf5"
filename = "snap_025_profiles_box_Arepo_new_plus_zooms_default_mass_weighted_Tvir4.0.hdf5"

indirs=["/data/curie4/nh444/project1/profiles/LDRIntRadioEffBHVel/c512_box_Arepo_new/"]
filename = "snap_025_profiles_mass_weighted_default_Tvir4.0.hdf5"

radprof_plot([indir+filename for indir in indirs], outdir=outdir,
             plot_only="entropy", plot_colour_scale=True, for_paper=True,
             add_prof_labels=False, prof_labels=None,
             add_file_labels=True, file_labels=None,
             sort_by_M500=True,
             #M500_min=3.2, M500_max=4.5,## (1e13 Msun) Use negative for inf
             nplot=1,
             #title=r"$\rho < \rho_{SF}$ and $T>3\times10^4$K",#"all gas",#"Rasia+2012 cut",
             fout_suffix="", filtered=True,
             rmin=0.007, rmax=-1, ##-1 for infinity
             plot_mean=False, plot_median=False,
             compare="REXCESS")