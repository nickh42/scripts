from profiles.radprof_plot import radprof_plot

indirs = ["/data/curie4/nh444/project1/profiles/"]

outdir = "/data/curie4/nh444/project1/profiles/"
filename = "snap_025_profiles_MDRInt_plus_zooms_default_mass_weighted.hdf5"
radprof_plot([indir+filename for indir in indirs], outdir=outdir,
             plot_only="gasfrac", plot_colour_scale=True, for_paper=True,
             add_prof_labels=False, prof_labels=None,
             add_file_labels=True, file_labels=None,
             sort_by_M500=True,
             M500_min=0.1, M500_max=-1,
             rmin=0.01, rmax=-1,
             plot_mean=False, plot_median=False,
             compare="Sun")
