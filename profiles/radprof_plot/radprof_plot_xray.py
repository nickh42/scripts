from profiles.radprof_plot import radprof_plot

outdir = "/data/curie4/nh444/project1/profiles/"

plot_only = "temperature"

#indirs = ["/data/curie4/nh444/project1/profiles/L40_512_LDRIntRadioEffBHVel/"]
#filename = "snap_025_profiles_mass_weighted_default_Tvir4.0.hdf5"
indirs = ["/data/curie4/nh444/project1/profiles/"]
filename = "snap_025_profiles_box_Arepo_new_plus_zooms_default_mass_weighted_Tvir4.0.hdf5"
radprof_plot([indir+filename for indir in indirs], outdir=outdir,
             plot_only=plot_only, plot_colour_scale=True, for_paper=True,
             add_prof_labels=False, prof_labels=None,
             add_file_labels=True, file_labels=None,
             sort_by_M500=True,
             M500_min=3.5, M500_max=-1,
             title="3D mass-weighted",
             fout_suffix="", filtered=True,
             rmin=0.003, rmax=2, ##-1 for infinity
             #ylims=[3, 1e5],
             plot_mean=False, plot_median=False,
             compare="REXCESS", scaled=True)

#indirs = ["/data/curie4/nh444/project1/L-T_relations/L40_512_LDRIntRadioEff_025/"]
#filename = "profiles_xray_snap25_default_Tvir4.hdf5"
indirs = ["/data/curie4/nh444/project1/profiles/"]
filename = "profiles_xray_snap25_zooms_default_Tvir4.hdf5"
radprof_plot([indir+filename for indir in indirs], filetype="xray", outdir=outdir,
             plot_only=plot_only, plot_colour_scale=True, for_paper=True,
             add_prof_labels=False, prof_labels=None,
             add_file_labels=True, file_labels=None,
             sort_by_M500=True,
             M500_min=3.5, M500_max=-1,
             title="3D spectroscopic",
             fout_suffix="", filtered=True,
             rmin=0.003, rmax=2, ##-1 for infinity
             #ylims=[3, 1e5],
             plot_mean=False, plot_median=False,
             compare="REXCESS", scaled=True)
    
#indirs = ["/data/curie4/nh444/project1/L-T_relations/L40_512_LDRIntRadioEff_025/"]
#filename = "profiles_xray_snap25_default_Tvir4_2_proj.hdf5"
indirs = ["/data/curie4/nh444/project1/profiles/"]
filename = "profiles_xray_snap25_zooms_default_Tvir4_proj.hdf5"
radprof_plot([indir+filename for indir in indirs], filetype="xray", outdir=outdir,
             plot_only=plot_only, plot_colour_scale=True, for_paper=True,
             add_prof_labels=False, prof_labels=None,
             add_file_labels=True, file_labels=None,
             sort_by_M500=True,
             M500_min=3.5, M500_max=-1,
             title="projected spectroscopic (no deprojection)",
             fout_suffix="", filtered=True,
             rmin=0.003, rmax=2, ##-1 for infinity
             #ylims=[3, 1e5],
             plot_mean=False, plot_median=False,
             compare="REXCESS", scaled=True)