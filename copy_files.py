"""
Recursively make copies of all files in current directory and subdirectories
Usage:
copy_files.py <filename> <new filename>
"""

import sys, os

if len(sys.argv) != 3:
    print "Usage:"
    print "<filename> <new filename>"
name = sys.argv[1]
newname = sys.argv[2]

print "Copying"
print name
print "--->"
print newname
for root, dirs, files in os.walk("./"):
    if name in files:
        print root
        os.system("cp "+os.path.join(root, name)+" "+os.path.join(root, newname))
