#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Mar 31 17:58:47 2018
calculates the maximum extent of the high-res region of a zoom simulation
@author: nh444
"""
import snap
import numpy as np

basedir = "/data/curie4/nh444/project1/boxes/"

simnames = ["L40_512_MDRIntRadioEff",
           ]
mpc_units = False
groupnums = range(400, 600)

basedir = "/data/curie4/nh444/project1/zoom_ins/MDRInt/"
simnames = ["c96_MDRInt",
           "c128_MDRInt",
           "c192_MDRInt",
           "c256_MDRInt",
           "c320_MDRInt",
           "c384_MDRInt",
           "c448_MDRInt",
           "c512_MDRInt",
           ]
mpc_units = True
groupnums = [0]

snapnum = 25
print "simname grp Power03 comov maxphys [kpc/h] M200 [Msun]"
for isim, simname in enumerate(simnames):
    sim = snap.snapshot(basedir+simname, snapnum, mpc_units=mpc_units)
    h = sim.header.hubble
    z = sim.header.redshift
    if isim==0: print "z =", z
    soft_comov = np.float64(sim.pars["SofteningHalo"]) #* 2.8
    ## the actual softening length is 2.8 times the value set in the param file
    ## i.e. the force is Newtonian beyond this length
    soft_maxphys = np.float64(sim.pars["SofteningHaloMaxPhys"]) #* 2.8
    if mpc_units:
        soft_comov *= 1000.0
        soft_maxphys *= 1000.0
        
    for groupnum in groupnums:
        DMmass = sim.header.massarr[1]
        R200 = sim.cat.group_r_crit200[groupnum]
        M200 = sim.cat.group_m_crit200[groupnum]
        
        soft_Pow = 4.0 * R200 * np.sqrt(DMmass / M200) ## optimal DM softening length according to Power 2003
        
        print simname, groupnum, "{:.3f} ".format(soft_Pow), "{:.3f} ".format(soft_comov), "{:.3f}".format(soft_maxphys), "{:.1e}".format(M200 / h * 1e10)
