import argparse

parser = argparse.ArgumentParser(description="Compare two or more parameter files.",
                                 usage="pardiff [-diff] file1 file2 [file3 ...]",
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("fnames1", metavar="filenames", type=str, nargs=1,
                    help='Parameter file names, space-separated')
parser.add_argument("fnames2", metavar="filenames", type=str, nargs='+',
                    help=argparse.SUPPRESS)
parser.add_argument('-diff', action='store_false',
                    help='Show only parameters which differ')
args=parser.parse_args()
fnames = args.fnames1 + args.fnames2

from pylab import *

nfiles = fnames.__len__()

pnames = []
pvals = []
allnames = []

print "-------------------------------------"
print "files:"

for fname in fnames:
  print fname

  f = open(fname, "r")

  porig = (f.read()).splitlines()
  porig.sort()

  pname = []
  pval = []

  for i in arange(porig.__len__()):
    p = porig[i].strip()
    
    if p.__len__() == 0:
      continue
    
    if p[0]!="%":
      p = p.split()
      
      assert p.__len__() >= 2
      
      pname.append(p[0])
      allnames.append(p[0])
      pval.append(p[1])

  pnames.append(pname)
  pvals.append(pval)

print

maxlength = 0
for j in arange(nfiles):
  for i in arange(pnames[j].__len__()):
    if pnames[j][i].__len__() > maxlength:
      maxlength = pnames[j][i].__len__()

allnames.sort()
allnames = unique(array(allnames))

print "parameters:"

for curname in allnames:
  curvals = []

  for i in arange(nfiles):
    ind = where(array(pnames[i])==curname)[0]
    
    if ind.size==1:
      ind = ind[0]
      curvals.append((pvals[i][ind]).ljust(20))
    elif ind.size==0:
      curvals.append("---".ljust(20))
    else:
      assert False
      
  curline = curname.ljust(maxlength+3)+" "
  for i in arange(nfiles):
    curline += curvals[i]+" "
  
  if unique(array(curvals)).size == 1:
    if not args.diff:
      print curline
  else:
    print "\033[31m"+curline+"\033[0m"

print "-------------------------------------"
  
  
