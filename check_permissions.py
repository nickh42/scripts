## Check if current user has read permissions to given list of files

def check():
    import os
    print_per_file = False
    namebase = "/data/deboras/ewald"
    dirnames = ["m_585_h_486_2210_z3/csf", "m_585_h_486_2210_z3/csfbh",
		"m_717_h_497_3136_z3/csf", "m_717_h_497_3136_z3/csfbh",
		"m_878_h_466_1998_z3/csf", "m_878_h_466_1998_z3/csfbh",
		"m_1075_h_442_550_z3/csf", "m_1075_h_442_550_z3/csfbh",
		"m_1317_h_379_2174_z3/csf", "m_1317_h_379_2174_z3/csfbh",
		"m_1613_h_390_2864_z3/csf", "m_1613_h_390_2864_z3/csfbh",
		"m_1975_h_506_1096_z3/csf", "m_1975_h_506_1096_z3/csfbh",
		"m_2186_h_56_1789_z3/csf", "m_2186_h_56_1789_z3/csfbh",
		"m_2419_h_436_2408_z3/csf", "m_2419_h_436_2408_z3/csfbh",
		"m_2963_h_73_1513_z3/csf", "m_2963_h_73_1513_z3/csfbh",
		"m_3279_h_103_2600_z3/csf", "m_3279_h_103_2600_z3/csfbh",
		"m_3629_h_259_610_z3/csf", "m_3629_h_259_610_z3/csfbh",
		"m_4445_h_183_1161_z3/csf", "m_4445_h_183_1161_z3/csfbh",
		"m_5443_h_352_482_z3/csf", "m_5443_h_352_482_z3/csfbh",
		"m_6664_h_48_194_z3/csf", "m_6664_h_48_194_z3/csfbh",
		"m_10002_h_94_501_z3/csf", "m_10002_h_94_501_z3/csfbh",
		"m_14999_h_355_1278_z3/csf", "m_14999_h_355_1278_z3/csfbh",
		"m_22498_h_197_0_z2/csf", "m_22498_h_197_0_z2/csfbh",
		"m_33617_h_146_0_z2/csf", "m_33617_h_146_0_z2/csfbh",
		"m_62037_h_47_0_z2/csf", "m_62037_h_47_0_z2/csfbh",
		"m_107580_h_232_0_z2/csf", "m_107580_h_232_0_z2/csfbh"]
    filename = "parameters-usedvalues"

    allowed = []
    denied = []
    missing = []
    for i, dirname in enumerate(dirnames):
        name = os.path.join(dirname, filename)
        if os.path.exists(os.path.join(namebase, name)):
            if os.access(os.path.join(namebase, name), os.R_OK):
                allowed.append(name)
            else:
                denied.append(name)
        else:
            missing.append(name)

    if print_per_file:
        maxlength = len(max(allowed + denied, key=len))
        print maxlength
        for i, dirname in enumerate(dirnames):
            name = os.path.join(dirname, filename)
            if os.path.exists(os.path.join(namebase, name)):
                print name.ljust(maxlength+2), os.access(os.path.join(namebase, name), os.R_OK)
            else:
                print name.ljust(maxlength+2), "Missing"

    else:
        print "\nRead permission allowed:"
        #maxlength = len(max(allowed, key=len))
        for i in range(len(allowed)):
            print allowed[i]
        print "\nRead permission denied:"
        for i in range(len(denied)):
            print denied[i]#[:-len(filename)]
        if len(missing) > 0:
            print "\n"+"\033[31m"+"Missing files:"+"\033[0m"
            for i in range(len(missing)):
                print missing[i]
    
if __name__ == "__main__":
    import sys
    #check(str(sys.argv[1]))
    check()
