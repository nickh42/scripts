import snapHDF5 as ws
import numpy as np
import numpy.random as rd
from scipy.integrate import quad
import conversions as co
from scipy.interpolate import interp1d
import math
import tables
from matplotlib.colors import LogNorm
from matplotlib import pylab
from matplotlib.colors import BoundaryNorm
from pylab import *
import scipy
import os
from scipy.spatial import Voronoi, voronoi_plot_2d          

#Set units
UnitMass = 1.989e43
UnitLength = 3.086e21
UnitVel = 1.e5
UnitTime = UnitLength/UnitVel
UnitDensity = UnitMass/UnitLength**3.
UnitEnergy = UnitMass*UnitVel**2.

Grav = 6.6742e-8
CLIGHT = 3.e10
PROTONMASS = 1.6726e-24
BOLTZMANN = 1.38065e-16
THOMPSON = 6.65e-25
PI = np.pi
HYDROGEN_MASSFRAC = 0.76
GAMMA_MINUS1 = 5./3. - 1.

#set folder containing snapshots
snap_files = ["../../ClusterMdot/PerseusCluster/OutputCosma6/JetColdMdotLL/"]

#set grid resolution
Xpixels = 512
Ypixels = 512

#set axis orientation
Xi =0 
Yi = 2
Zi = 1

#set grid size (note slice is centered between Zmin and Zmax, these should not be equal, as this region encompasses volume searched to find local cells)
Xmin = -500.
Xmax = 500.
Ymin = -500.
Ymax = 500.
Zmin = -50.
Zmax = 50.

#set snapshot numbers
start_snap = 100
end_snap = 100
dsnap = 1

#===# WORKINGS #===#

snaps = np.arange(start_snap, end_snap+1,dsnap)

XC = (Xmax + Xmin) / 2.
YC = (Ymax + Ymin) / 2.
ZC = (Zmax + Zmin) /2.

LengthX = Xmax - Xmin
LengthY = Ymax - Ymin
LengthZ = Zmax - Zmin

XSmax = XC + LengthX * 0.5
YSmax = YC + LengthY * 0.5
XSmin = XC - LengthX * 0.5
YSmin = YC - LengthY * 0.5
ZSmax = ZC + LengthZ * 0.5
ZSmin = ZC - LengthZ * 0.5

dx = (Xmax-Xmin)/float(Xpixels)
dy = (Ymax-Ymin)/float(Ypixels)

fig_size = plt.rcParams["figure.figsize"]
fig_size[0] = 10
fig_size[1] = 10
plt.rcParams["figure.figsize"] = fig_size
fig = plt.figure()

for k in snap_files:
    for i in snaps:
	if i < 10:
		snap = k+"snap_00"+str(i)
		figfile= "fig_00"+str(i)+".png"
        if i > 9 and i < 100:
		snap = k+"snap_0"+str(i)
		figfile="fig_0"+str(i)+".png"
        if i > 99:
		snap = k+"snap_"+str(i)
		figfile="fig_"+str(i)+".png"
        if os.path.isfile(snap+".hdf5") == False:
            print snap
            break
        #read in necessary data from snap shots
        h = ws.snapshot_header(snap)
        print h.time
        print h.time*UnitTime / 60./60./24./365./1.e6
        box_centre = h.boxsize / 2.
        label=str(round(h.time,3))
        ngas = h.npart[0]
        nstars = h.npart[4]
        nbh = h.npart[5]
        grid_sum = np.zeros((Xpixels,Ypixels), dtype = np.float32, order='F')
        posg = ws.read_block(snap,"POS ",0).T * UnitLength / 3.086e21 - box_centre
        xg = posg[Xi,:]
        yg = posg[Yi,:]
        zg = posg[Zi,:]
        #find cells within search region
        wmax = np.where((xg <= XSmax) & (xg >= XSmin) & (yg <= YSmax) & (yg >= YSmin) & (zg <= ZSmax) & (zg >= ZSmin))[0]
        xg = xg[wmax]
        yg = yg[wmax]
        zg = zg[wmax]
        posg = [xg,yg,zg]
        gas_points = np.dstack((xg.ravel(),yg.ravel(),zg.ravel()))[0]
        #create tree for all relevant gas cells
        gas_tree = scipy.spatial.cKDTree(gas_points)
        x = np.linspace(Xmin, Xmax, Xpixels)
        y = np.linspace(Ymin, Ymax, Ypixels)
        X,Y = np.meshgrid(x,y)
        Z = np.zeros_like(X.ravel()) + ZC
        grid_points = np.dstack((X.ravel(),Y.ravel(),Z.ravel()))[0]
        #find nearest gas cells to slice grid points
        rp, loc = gas_tree.query(list(grid_points), k=1)
        #make unique list of cells
        loc = np.unique(loc)
        x = xg[loc]
        y = yg[loc]
        gp = np.dstack((x,y))[0]
        #create Voronoi mesh
        vor = scipy.spatial.Voronoi(gp)
        #plot mesh
        for r in vor.ridge_vertices:
            r0 = vor.vertices[r[0]]
            r1 = vor.vertices[r[1]]
            lx = r1[0] - r0[0]
            ly = r1[1] - r0[1]
            lr = sqrt(lx*lx + ly*ly)
            if lr < (Xmax-Xmin)/4.:
                plot([r0[0], r1[0]], [r0[1], r1[1]],'k',lw=0.5)
        plt.xlim([Xmin, Xmax])
        plt.ylim([Ymin, Ymax])
        pylab.xlabel("x (kpc)")
        pylab.ylabel("z (kpc)")
        plt.tight_layout()
        show()

stop
