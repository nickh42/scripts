import snap

simdir = "/data/emergence4/deboras/Elliptical_ZoomIns/L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff"
simdir = "/home/nh444/data/project1/zoom_ins/LDRIntRadioEffBHVel/c96_box_Arepo"
#simdir = "/home/nh444/data/project1/zoom_ins/LDRIntRadioEffBHVel/c96_FeedbackFix"
#simdir = "/home/nh444/data/project1/zoom_ins/LDRIntRadioEffBHVel/c96_FeedbackFix_OldGrav"
#simdir = "/home/nh444/data/project1/zoom_ins/LDRIntRadioEffBHVel/c96_FeedbackFix_OldGrav_2"
#simdir = "/home/nh444/data/project1/zoom_ins/LDRIntRadioEffBHVel/c96_Arepo_March15"
#simdir = "/home/nh444/data/project1/zoom_ins/LDRIntRadioEffBHVel/c256"
simdir = "/home/nh444/data/project1/zoom_ins/LDRIntRadioEffBHVel/c128_Arepo_May31"
#simdir = "/home/nh444/data/project1/zoom_ins/LDRIntRadioEffBHVel/c128_Arepo_March15"

group = 0
sim = snap.snapshot(simdir, 25, mpc_units=True, group_veldisp=False, masstab=True)

R = sim.cat.group_r_crit500[group]

#==============================================================================
# import numpy as np
# group_pos = sim.cat.group_pos[0]
# sim.load_highresgasmass()
# sim.load_gasmass()
# ind = np.where(sim.hrgm > 0.5*sim.gasmass)[0]
# sim.load_gaspos()
# R = np.max(sim.nearest_dist_3D(sim.gaspos[ind], group_pos))
# print "R =", R
#==============================================================================
R = 2500.0 # 5 Mpc

sim.get_gas_projection(sidelength=R*2.0, group=group, zthick=2.0*R, npix=1024,
                       use_vol=True, filebase="map_grp"+str(group)+"_", save_maps=["metallicity"],
                        phase_filter=True, name_suffix="HalfHighRes", nthreads=8)

