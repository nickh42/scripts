#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 22 12:14:01 2018

@author: nh444
"""


def plot_hsv_map(ax, dens, temp, colorbar=True,
                 cbarpos=[0.04, 0.03], cbarsize=0.2,
                 cmap=None, extent=None, interpolation=None,
                 densmin=None, densmax=None,
                 tempmin=None, tempmax=None,
                 return_img=False):
    """
    Plot a density/temperature map to the given axes where hue/saturation
    correspond to the temperature and brightness to the density
    Args:
        colorbar: plot (2D) colourbar
        cbarpos: position of bottom left of colourbar
        cbarsize: size of colourbar as fraction of figure size
        return_img: return result of ax.imshow
    """
    import numpy as np
    import matplotlib.pyplot as plt
    from matplotlib.colors import hsv_to_rgb
    from matplotlib.colors import rgb_to_hsv
    from mpl_toolkits.axes_grid.inset_locator import InsetPosition

    if densmin is None:
        densmin = np.min(dens)
    if densmax is None:
        densmax = np.max(dens)
    if tempmin is None:
        tempmin = np.min(temp)
    if tempmax is None:
        tempmax = np.max(temp)

    # With clip=True, Normalize returns 0 or 1 when a value is out
    # of bounds (which will be the first or last colour in the colourmap).
    densnorm = plt.Normalize(vmin=densmin, vmax=densmax, clip=True)
    tempnorm = plt.Normalize(vmin=tempmin, vmax=tempmax, clip=True)

    if cmap is None:
        from palettable.cmocean.sequential import Deep_20_r as pallette
        cmap = pallette.mpl_colormap

    # Set the out-of-bounds colours. Not needed if clip=True above.
#    cmap.set_under(cmap(0.0))
#    cmap.set_over(cmap(1.0))

    # Convert the rgb values corresponding to this colormap to hsv.
    hsv = rgb_to_hsv(cmap(tempnorm(temp))[:, :, :3])
    # Replace value (brightness) with normalised density.
    hsv[:, :, 2] = densnorm(dens)
    # Convert hsv back to rgb.
    rgb = hsv_to_rgb(hsv)

    img = ax.imshow(rgb, extent=extent, origin="lower",
                    aspect='auto',
                    interpolation=interpolation)

    if colorbar:
        # Create image for colourbar
        num = 100
        col_ind = np.linspace(0.0, 1.0, num=num)  # array from 0 to 1
        # matrix of 0 to 1
        ind = np.repeat(np.stack((col_ind, col_ind)), [1, num-1], axis=0)
        # convert matrix to rgb colourmap then convert to hsv
        hsv = rgb_to_hsv(cmap(ind)[:, :, :3])
        # set value (brightness) to array from 0 to 1
        hsv[:, :, 2] = np.rot90(ind)
        rgb = hsv_to_rgb(hsv)  # convert back to rgb
        cax = plt.axes([0, 0, 1, 1])  # create axes for colourbar
        # Set size and location of colourbar
        ip = InsetPosition(ax, [cbarpos[0], cbarpos[1], cbarsize, cbarsize])
        cax.set_axes_locator(ip)
        cax.imshow(rgb, extent=[tempmin, tempmax, densmin, densmax],
                   aspect='auto')

        if return_img:
            return cax, img
        else:
            return cax

    if return_img:
        return img
