import pylab as pyl
import os
os.chdir("/data/curie4/nh444/project1/maps/L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff")

add_axes = False
mapnames = ["density", "temperature"]
cmaps = ['viridis', 'magma']

#----------
for i, mapname in enumerate(mapnames):
    dat = pyl.load("box"+mapname+"_proj.npz")
    maparr = dat["maparr"]
    cenx,ceny,cenz,sidelength,zthick,a_z,a_x,a_z2 = dat["mappars"]
    #z, r_comov = dat["simpars"]
    
    mycmap = pyl.get_cmap(cmaps[i])
    
    if add_axes:
        fig = pyl.figure()
        ax = fig.add_subplot(111)
        #img = ax.imshow(pyl.log10(maparr.transpose()),cmap=mycmap,extent=[-0.5*sidelength/1000.0, 0.5*sidelength/1000.0, -0.5*sidelength/1000.0, +0.5*sidelength/1000.0], origin="lower")#, vmin=-2.0, vmax=-1.0)
        img = ax.imshow(pyl.log10(maparr.transpose()),cmap=mycmap,extent=[0, sidelength/1000.0, 0, sidelength/1000.0], origin="lower")
        outname = "box_"+mapname+".pdf"
        ax.set_xlabel("x [Mpc/h]",size="x-large")
        ax.set_ylabel("y [Mpc/h]",size="x-large")
        for label in ax.get_xticklabels() + ax.get_yticklabels():   # set size of axes tick numbers
            label.set_fontsize("large")
    else:
        fig = pyl.figure()
        xpix, ypix = pyl.shape(maparr)
        dpi = 80.0
        fig.set_size_inches(ypix/dpi, xpix/dpi)
        ax = pyl.Axes(fig, [0., 0., 1., 1.])
        ax.set_axis_off()
        fig.add_axes(ax)
        img = ax.imshow(pyl.log10(maparr.transpose()),cmap=mycmap, aspect='auto')

        outname = "box_"+mapname+".png"
        pyl.savefig(outname, dpi=dpi)
#==============================================================================
#     ax.xaxis.label.set_color('white')
#     ax.tick_params(axis='x', which='both', colors='white')
#     ax.yaxis.label.set_color('white')
#     ax.tick_params(axis='y', which='both', colors='white')
#     import matplotlib
#     for child in ax.get_children():
#         if isinstance(child, matplotlib.spines.Spine):
#             child.set_color('white')
#==============================================================================
    fig.savefig(outname, bbox_inches='tight', transparent=True)
    print "Saved map to", outname


