import snap

#simdir = "/data/emergence4/deboras/Elliptical_ZoomIns/L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff"
#simdir = "/data/emergence4/deboras/Elliptical_ZoomIns/L40_512_LowDutyRadioWeak_HalfThermal_FixEta"
#simdir = "/data/emergence4/deboras/Elliptical_ZoomIns/L40_512_NoDutyRadioWeak_HalfThermal_FixEta"
simdir = "/data/ERCblackholes1/nh444/boxes/L40_512_MDRIntRadioEff/"

outdir = "/data/curie4/nh444/project1/maps/L40_512_MDRIntRadioEff/"

group = 257
maps = ["density","temperature"]#, "xray"]
sim = snap.snapshot(simdir, 25, mpc_units = False, group_veldisp = False, masstab = True)

r500c = sim.cat.group_r_crit500[group]
zthick=2.0*r500c
zthick = 0

sim.get_gas_projection(sidelength=r500c*2.0, cenx=0.0, ceny=0.0, group=group,
                       zthick=zthick, npix=1024, use_vol=True, outdir=outdir,
                       zaxis=1, ## same as get_L-T.py
                       filebase="map_grp"+str(group)+"_", save_maps=maps,
                       name_suffix="", nthreads=8)
