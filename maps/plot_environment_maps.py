import matplotlib.pyplot as plt
import numpy as np
from matplotlib.colors import LogNorm
from matplotlib.backends.backend_pdf import PdfPages

simdir = "c448_MDRInt"
indir = "/data/curie4/nh444/project1/maps/MDRInt/"

outname = "environment_maps_c448_MDRInt_density.pdf"
mapname = "density"
#mapname = "temperature"
pdf = PdfPages(indir+simdir+"/"+outname)

group = 0
snapnums = range(1,26)

maxval = 0.0
minval = np.inf
print "Finding minimum values..."
for snapnum in snapnums:
    name_suffix="snap"+str(snapnum)
    dat = np.load(indir+simdir+"/"+"map_grp"+str(group)+"_"+mapname+"_"+name_suffix+"_proj.npz")
    thismax = np.max(dat["maparr"])
    if thismax > maxval:
        maxval = thismax
    thismin = np.min(dat["maparr"])
    if thismin < minval:
        minval = thismin
for snapnum in snapnums:
    print "Plotting snapnum", snapnum
    name_suffix="snap"+str(snapnum)
    dat = np.load(indir+simdir+"/"+"map_grp"+str(group)+"_"+mapname+"_"+name_suffix+"_proj.npz")
    maparr = dat["maparr"]
    #cenx,ceny,cenz,sidelength,zthick,a_z,a_x,a_z2 = dat["mappars"]
    #cenx,ceny,cenz,sidelength,zthick,a_z,a_x,a_z2,t,z,h = dat["mappars"]
    cenx,ceny,cenz,sidelength,zthick,a_z,a_x,a_z2,t,z,h,M500,R500 = dat["mappars"]
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    
    if mapname=="density":
        mycmap = plt.get_cmap('viridis')
    elif mapname=="temperature":
        mycmap = plt.get_cmap('magma')
    
    img = ax.imshow(maparr.transpose(),cmap=mycmap,
                    extent=[-0.5*sidelength/1000.0, 0.5*sidelength/1000.0, -0.5*sidelength/1000.0, +0.5*sidelength/1000.0],
                    origin="lower", norm=LogNorm(vmin=minval, vmax=maxval))#, vmin=0.0, vmax=0.4)
    ax.set_xlabel("x [Mpc/h]",size="x-large")
    ax.set_ylabel("y [Mpc/h]",size="x-large")
    ax.set_title("$z={:.1f}$".format(z))
    
    cbar = fig.colorbar(img)
    if mapname=="density":
        cbar.set_label("Density", size="x-large")
    elif mapname=="temperature":
        cbar.set_label("Temperature [keV]", size="x-large")
    
    #for curline in ax.spines.values(): # change width of axes
    #  curline.set_linewidth(0.5)
     
    mysize="large" 
     
    for label in ax.get_xticklabels() + ax.get_yticklabels():   # set size of axes tick numbers
      label.set_fontsize(mysize)
    
    for label in cbar.ax.get_xticklabels() + cbar.ax.get_yticklabels():   # set size of axes tick numbers
      label.set_fontsize(mysize)
    
    pdf.savefig(fig)
    plt.close()
print "Plot saved to", indir+simdir+"/"+outname
pdf.close()
