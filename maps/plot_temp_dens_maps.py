import matplotlib.pyplot as plt
import numpy as np

#pyl.matplotlib.rc('text',usetex=True)
#pyl.matplotlib.rc("font",family="serif",serif="Times")

indir = "/data/curie4/nh444/project1/maps/L40_512_MDRIntRadioEff/"
name_suffix=""
#name_suffix="_sph"
#name_suffix="_zaxis1"
outdir = indir

group = 257
circrad = [1.0] ## list of radii (r500) of circles drawn at centre, set empty list to disable

dat = np.load(indir+"map_grp"+str(group)+"_temperature"+name_suffix+"_proj.npz")
maparr = dat["maparr"]
cenx,ceny,cenz,sidelength,zthick,a_z,a_x,a_z2,t,z,h,M500,R500 = dat["mappars"]

fig = plt.figure()
ax = fig.add_subplot(111)

mycmap = plt.get_cmap('plasma')

img = ax.imshow(np.log10(maparr.transpose()),cmap=mycmap,extent=[-0.5*sidelength/1000.0, 0.5*sidelength/1000.0, -0.5*sidelength/1000.0, +0.5*sidelength/1000.0], origin="lower")#, vmin=0.0, vmax=3.0)
ax.set_xlabel("x [Mpc/h]",size="x-large")
ax.set_ylabel("y [Mpc/h]",size="x-large")
if len(circrad) > 0:
    for crad in circrad:
        circle = plt.Circle((0, 0), crad * R500 / 1000.0, fc='none', ec='white', ls='dotted')
        ax.add_artist(circle)

#ax.text(24,22,r"$M_{500c} = 2.5 \times 10^{14} \, h^{-1} M_\odot$", color="white", size="x-large", ha="right")
#ax.text(24,19.5,r"$z = 0.15$", color="white", size="x-large", ha="right")

#cbar = fig.colorbar(img,ticks=[1,2,3,4,5])
cbar = fig.colorbar(img)
cbar.set_label("log(temperature [keV])", size="x-large")

#for curline in ax.spines.values(): # change width of axes
#  curline.set_linewidth(0.5)
 
mysize="large" 
 
for label in ax.get_xticklabels() + ax.get_yticklabels():   # set size of axes tick numbers
  label.set_fontsize(mysize)

for label in cbar.ax.get_xticklabels() + cbar.ax.get_yticklabels():   # set size of axes tick numbers
  label.set_fontsize(mysize)

fig.show()
fig.savefig(outdir+"map_grp"+str(group)+"_temp_proj.pdf", bbox_inches='tight')
print "Temperature map saved to", outdir+"map_grp"+str(group)+"_temp_proj.pdf"

#----------

dat = np.load(indir+"map_grp"+str(group)+"_density"+name_suffix+"_proj.npz")
maparr = dat["maparr"]
cenx,ceny,cenz,sidelength,zthick,a_z,a_x,a_z2,t,z,h,M500,R500 = dat["mappars"]

fig = plt.figure()
ax = fig.add_subplot(111)

mycmap = plt.get_cmap('viridis')

img = ax.imshow(np.log10(maparr.transpose()),cmap=mycmap,extent=[-0.5*sidelength/1000.0, 0.5*sidelength/1000.0, -0.5*sidelength/1000.0, +0.5*sidelength/1000.0], origin="lower")#, vmin=-3.0, vmax=-1.0)
ax.set_xlabel("x [Mpc/h]",size="x-large")
ax.set_ylabel("y [Mpc/h]",size="x-large")

#ax.text(24,22,r"$M_{500c} = 2.5 \times 10^{14} \, h^{-1} M_\odot$", color="white", size="x-large", ha="right")
#ax.text(24,19.5,r"$z = 0.15$", color="white", size="x-large", ha="right")

#cbar = fig.colorbar(img,ticks=[1,2,3,4,5])
cbar = fig.colorbar(img)
cbar.set_label("Log(density)", size="x-large")

#for curline in ax.spines.values(): # change width of axes
#  curline.set_linewidth(0.5)
 
mysize="large" 
 
for label in ax.get_xticklabels() + ax.get_yticklabels():   # set size of axes tick numbers
  label.set_fontsize(mysize)

#for label in cbar.ax.get_xticklabels() + cbar.ax.get_yticklabels():   # set size of axes tick numbers
#  label.set_fontsize(mysize)

fig.show()
fig.savefig(outdir+"map_grp"+str(group)+"_density_proj.pdf", bbox_inches='tight')
print "Density map saved to", outdir+"map_grp"+str(group)+"_density_proj.pdf"
