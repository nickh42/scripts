import snap

basedir = "/home/nh444/data/project1/zoom_ins/LDRIntRadioEffBHVel/"
simdir = "c96_box_Arepo"
simdir = "c128_box_Arepo"
outdir = "/data/curie4/nh444/project1/maps/LDRIntRadioEffBHVel/"

basedir = "/data/ERCblackholes1/nh444/zooms/MDRInt/"
simdir = "c448_MDRInt"
outdir = "/data/curie4/nh444/project1/maps/MDRInt/"

mpc_units = True
fixed_position = True ## fix centre of map at group location in last snapshot
save_maps = ["density", "temperature"]

group = groupnum = 0
snapnums = range(1,26)
cenx = ceny = cenz = 0.0
if fixed_position:
    sim = snap.snapshot(basedir+simdir, snapnums[-1], mpc_units=mpc_units, group_veldisp=False, masstab=True)
    cenx = sim.cat.group_pos[groupnum,0]
    ceny = sim.cat.group_pos[groupnum,1]
    cenz = sim.cat.group_pos[groupnum,2]
    group = -1
for snapnum in snapnums:
    sim = snap.snapshot(basedir+simdir, snapnum, mpc_units=mpc_units, group_veldisp=False, masstab=True)
    
    #R = 10.0 * sim.cat.group_r_crit500[group]
    R = 10000.0
    
    sim.get_gas_projection(sidelength=R*2.0, group=group, zthick=2.0*R, npix=1024,cenx=cenx,ceny=ceny,cenz=cenz,
                           use_vol=True, filebase="map_grp"+str(groupnum)+"_", save_maps=save_maps,
                           phase_filter=False, highres_only=False, name_suffix="_snap"+str(snapnum), nthreads=8, outdir=outdir+simdir+"/")

