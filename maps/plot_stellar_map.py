#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 27 16:42:06 2018

@author: nh444
"""

import snap
import matplotlib.pyplot as plt
import numpy as np
from projectparticles.put_grid import put_grid_cic2D

outdir = "/home/nh444/data/project1/maps/MDRInt/"

basedir = "/home/nh444/data/project1/zoom_ins/MDRInt/"
simname = "c448_MDRInt"
mpc_units = True
group = 0
suffix="_c448_MDRInt"
sidelength = 20000.0 ## image sidelength in pkpc (no h)

basedir = "/data/curie4/nh444/project1/boxes/"
simname = "L40_512_MDRIntRadioEff"
mpc_units = False
group = 0
suffix="_box"
sidelength = 50000.0 ## image sidelength in pkpc (no h)

ngb = 16 ## factor that multiplies the cell volume and is cube-rooted for the smoothing length
npix = 2000
snapnum = 25


sim = snap.snapshot(basedir+simname, snapnum, mpc_units=mpc_units)
h = sim.header.hubble
z = sim.header.redshift
R500 = sim.cat.group_r_crit500[group]
sidelength *= h * (1+z) ## convert pkpc to ckpc/h
zthick = sidelength / 2.0 ## integration distance, 0 is whole box

print "sidelength =", sidelength, "[ckpc/h]"
print "npix =", npix
print "R500 =", R500, "[ckpc/h]"

## Calculate the stellar density projection
sim.load_starpos()
sim.load_starmass()
centre = sim.cat.group_pos[group]
rel_pos = sim.rel_pos(sim.starpos, centre)
ind = np.where(np.abs(rel_pos[:,2]) <= zthick)[0]
dens = put_grid_cic2D(npix, 0, 0, sidelength, rel_pos[ind,0], rel_pos[ind,1], sim.starmass[ind], 0)
dens.transpose() # such that x-coordinate correspond to x-axis and y-coordinate correspond to y-axis

fig, ax = plt.subplots(figsize=(7,7))
ax.set_xlabel("ckpc/h")
ax.set_ylabel("ckpc/h")

## First fill with a solid colour then plot stellar density on top with transparency
fill = np.full(dens.shape + (3,), 0, dtype=np.uint8) ##0 for black, 255 for white
ax.imshow(fill, aspect="equal", origin="lower", extent=(-sidelength/2.0, sidelength/2.0, -sidelength/2.0, sidelength/2.0))

## make a transparent colour map
col = 'white'
from matplotlib.colors import colorConverter
from matplotlib.colors import ListedColormap
N = 256 ## number of colours in colormap
## First get the rgba values for the colour
rgba = colorConverter.to_rgba(col)
## Initialise an array for the colormap using the first rgba value (r)
cmap = np.full((N, 4), rgba[0])
## Fill in g and b values
cmap[:,1] = rgba[1]
cmap[:,2] = rgba[2]
## Fill the alpha values with a linear sequence from 0 to 1
cmap[:,3] = np.linspace(0, 1, N)
## Create colormap from the array
cmap = ListedColormap(cmap)

## Plot the stellar density with the transparent colormap
ax.imshow(np.log10(dens), cmap=cmap, aspect="equal", origin="lower", extent=(-sidelength/2.0, sidelength/2.0, -sidelength/2.0, sidelength/2.0), interpolation='none')
