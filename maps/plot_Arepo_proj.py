#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 19 18:31:48 2017
Plots maps output by Arepo, which are binary files of a flattened array
@author: nh444
"""
import numpy as np
#import matplotlib as mpl
#mpl.use("Agg")
import matplotlib.pyplot as plt


indir = "./"
filename = "vproj_rho_125"

with open(indir+filename, "rb") as f:
    xpix = np.fromfile(f, dtype=np.int32, count=1)[0] ## first 32 bits are no. pixels in x
    ypix = np.fromfile(f, dtype=np.int32, count=1)[0] ## no. of pixels in y
    boxx = np.fromfile(f, dtype=np.float64, count=1)[0] ## dimensions
    boxy = np.fromfile(f, dtype=np.float64, count=1)[0] ## dimensions
    boxz = np.fromfile(f, dtype=np.float64, count=1)[0] ## dimensions
    img = np.fromfile(f, dtype=np.float64, count=xpix*ypix) ## the flattened image array
img = np.reshape(img, (xpix, ypix))
fig, ax = plt.subplots(figsize=(7,7))
#img = np.rot90(img) ## disabling and projecting along (0,0,-1) gives same result as makeimage
ax.imshow(np.log10(img), cmap=plt.get_cmap('viridis'))
fig.savefig("./projection.pdf")
