import pylab as pyl
from matplotlib.colors import LogNorm
ag_hydrogen = 0.70683 # Anders & Grevesse solar mass fractions
ag_helium = 0.27431
ag_metals = 0.01886

pyl.matplotlib.rc('text',usetex=True)
pyl.matplotlib.rc("font",family="serif",serif="Times")

group = 0
name_suffix=""
name_suffix="_HalfHighRes"
#name_suffix="_HighRes"

dat = pyl.load("map_grp"+str(group)+"_metallicity"+name_suffix+"_proj.npz")
maparr = dat["maparr"]
## Units of solar metallicity -->
maparr = maparr / (1-maparr) * (ag_hydrogen+ag_helium)/ag_metals
cenx,ceny,cenz,sidelength,zthick,a_z,a_x,a_z2 = dat["mappars"]

fig = pyl.figure()
ax = fig.add_subplot(111)

mycmap = pyl.get_cmap('viridis')

img = ax.imshow(maparr.transpose(),cmap=mycmap,
                extent=[-0.5*sidelength/1000.0, 0.5*sidelength/1000.0, -0.5*sidelength/1000.0, +0.5*sidelength/1000.0],
                origin="lower", norm=LogNorm(vmin=0.01, vmax=1.0))#, vmin=0.0, vmax=0.4)
ax.set_xlabel("x [Mpc/h]",size="x-large")
ax.set_ylabel("y [Mpc/h]",size="x-large")


cbar = fig.colorbar(img)
cbar.set_label("Z [Z$_{\odot}$]", size="x-large")

#for curline in ax.spines.values(): # change width of axes
#  curline.set_linewidth(0.5)
 
mysize="large" 
 
for label in ax.get_xticklabels() + ax.get_yticklabels():   # set size of axes tick numbers
  label.set_fontsize(mysize)

for label in cbar.ax.get_xticklabels() + cbar.ax.get_yticklabels():   # set size of axes tick numbers
  label.set_fontsize(mysize)

fig.show()
fig.savefig("map_grp"+str(group)+"_metals"+name_suffix+"_proj.pdf")
print "Plot saved to", "map_grp"+str(group)+"_metals"+name_suffix+"_proj.pdf"

