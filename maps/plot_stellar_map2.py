#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 14 12:00:12 2018

@author: nh444
"""
def main():
    
    indir = "/home/nh444/data/project1/maps/MDRInt/"
    outdir = "/data/vault/nh444/VaultDocs/Posters/DiRAC/"
    
    name_suffixes=["BCG"]
#    name_suffixes=["group"]
#    name_suffixes=["galaxy_infall"]
    
    toPlot = [#"mass",
              "gband",
              ]
#    plot_single_image(toPlot, name_suffixes,
#                      show_labels=True, image_only=False,
#                      indir=indir, outdir=outdir)
    
    plot_composite_image(name_suffixes, bands="irg",
                         indir=indir,
                         outdir=outdir)
    
def plot_composite_image(name_suffixes, bands="irg",
                         use_lupton=False, lupton_alpha=0.02, lupton_Q=8, scale_min=0.0, 
                         b_fac=0.7, g_fac=1.0, r_fac=1.3,
                         indir="/home/nh444/data/project1/maps/",
                         outdir="/home/nh444/data/project1/maps/"):
    """
    Plot a composite image where (r,g,b) colours each correspond to
    a different band following the method of Lupton+ 2004 and Torrey+ 2015
    Args:
        bands: string containing the bands corresponding to r, g and b
    """
    
    import numpy as np

    if len(bands) != 3 or not isinstance(bands, str):
        raise IOError("bands argument must be string of length 3")
    rmapname = bands[0]+"band"
    gmapname = bands[1]+"band"
    bmapname = bands[2]+"band"
    
    for name_suffix in name_suffixes:
        ## Load r, g and b maps
        cenx,ceny,cenz,sidelength,zthick,a_z,a_x,a_z2,t,z,h,M500,R500 = np.load(indir+"map_stellar_"+rmapname+"_"+name_suffix+"_proj.npz")["mappars"]
        rmap = np.load(indir+"map_stellar_"+rmapname+"_"+name_suffix+"_proj.npz")["maparr"]
        gmap = np.load(indir+"map_stellar_"+gmapname+"_"+name_suffix+"_proj.npz")["maparr"]
        bmap = np.load(indir+"map_stellar_"+bmapname+"_"+name_suffix+"_proj.npz")["maparr"]
        
        rmap *= r_fac
        gmap *= g_fac
        bmap *= b_fac
        
        ## Create zero array for final image
        npix = rmap.shape[0]
        assert rmap.shape[1] == npix, "Image not square"
        img = np.zeros((npix, npix, 3), dtype=float)
        
        if use_lupton:
            ## Scale each map according to eqn 2 in Lupton+2004
            I = (rmap + gmap + bmap) / 3
            f_of_I = asinh(I) ## F(x)=arcsinh(x/b)
            #f_of_I = np.arcsinh( lupton_alpha * lupton_Q * (I - scale_min))/lupton_Q
            I[I < 1e-6] = 1e100 ## this will effectively set the pixel to 0 instead of an extremely large number
            img[:,:,0] = rmap * f_of_I / I ## This is "R" in the notation of Lupton+04
            img[:,:,1] = gmap * f_of_I / I ## G
            img[:,:,2] = bmap * f_of_I / I ## B
            
            ## If R, G or B for a given pixel are greater than 1, scale all of them by the maximum value
            maxRGB = np.amax(img, axis=2) ## Maximum of the RGB values for each pixel
            changeind = (maxRGB > 1.0)
            img[changeind,0] = img[changeind,0]/maxRGB[changeind]
            img[changeind,1] = img[changeind,1]/maxRGB[changeind]
            img[changeind,2] = img[changeind,2]/maxRGB[changeind]
            
            ## If R, G or B for a given pixel are less than 0, set all of them to zero
            minRGB = np.amax(img, axis=2)
            changeind = (minRGB < 0.0)
            img[changeind,0] = 0
            img[changeind,1] = 0
            img[changeind,2] = 0
            
            ## If I is less than zero in a pixel, set to zero
            changeind = (I < 0.0)
            img[changeind,0] = 0
            img[changeind,1] = 0
            img[changeind,2] = 0
            img[img < 0] = 0
        else:       
            ## Standard mapping as in Lupton+04 eqn 1
            img[:,:,0] = asinh(rmap)
            img[:,:,1] = asinh(gmap)
            img[:,:,2] = asinh(bmap)
            img[img < 0] = 0
        
        unitfac = 1.0 / 1000.0 / h / (1+z) ## to convert ckpc/h to pMpc
        cur_sidelength = sidelength * unitfac
        outname = outdir+"map_stellar_composite_"+bands+"_"+name_suffix+".png"
        plot_image(img, cur_sidelength, outname)

def plot_single_image(toPlot, name_suffixes,
                      show_labels=True, image_only=False,
                      indir="/home/nh444/data/project1/maps/",
                      outdir="/home/nh444/data/project1/maps/"):
    """
    Plot a single image of e.g. stellar mass or a particular photometric band
    """
    import numpy as np
   
    for name_suffix in name_suffixes:
        for mapname in toPlot:              
            ## Load map
            cenx,ceny,cenz,sidelength,zthick,a_z,a_x,a_z2,t,z,h,M500,R500 = np.load(indir+"map_stellar_"+mapname+"_"+name_suffix+"_proj.npz")["mappars"]
            curmap = np.load(indir+"map_stellar_"+mapname+"_"+name_suffix+"_proj.npz")["maparr"]
            npix = curmap.shape[0]
            assert curmap.shape[1] == npix, "density map not square."
            ## Scale maps
            if mapname=="mass":
                from palettable.cmocean.sequential import Deep_20_r as pallette
                curmap *= 1e10 / h / (sidelength / h / (1+z) / npix)**2 ## convert 1e10 Msun/h per pixel to Msun/kpc^2
                curmap = np.log10(curmap.T)
                cbar_label = "Mass surface density"
            elif mapname=="gband":
                from palettable.colorbrewer.sequential import Greys_9 as pallette
                cbar_label = "g"
            else:
                raise IOError("mapname '"+str(mapname)+"' not recognised.")
        
               
            unitfac = 1.0 / 1000.0 / h / (1+z) ## to convert ckpc/h to pMpc
            cur_sidelength = sidelength * unitfac
            cmap = pallette.mpl_colormap
            if image_only:
                outname = outdir+"map_stellar_"+mapname+"_"+name_suffix+"_image.png"
            else:
                outname = outdir+"map_stellar_"+mapname+"_"+name_suffix+".png"
                
            plot_image(curmap, cur_sidelength, outname, cmap=cmap, cbar_label=cbar_label,
                       show_labels=show_labels, image_only=image_only)

            
def plot_image(amap, sidelength, outname, cmap=None, cbar_label=None,
               show_labels=True, image_only=False):
    """
    Given an image, plot it.
    Args:
        show_labels: show axis and tick labels on main plot
        image_only: no colorbar, spines, labels etc.
    """
    import numpy as np
    import matplotlib.pyplot as plt
    from mpl_toolkits.axes_grid.inset_locator import inset_axes
    
    col = 'white' ## colour of lines and edges
    lw = 1.8 ## line width of spines
    cbar_dist = 0.0 ## distance of colourbars from right-hand panels e.g. 0.02
    cbar_width = "5%"
    
    ## Get min. and max. values
    vmax = np.max(amap[np.isfinite(amap)])
    vmin = np.min(amap[np.isfinite(amap)])
    print "vmin, vmax =", vmin, vmax
    
    norm = plt.Normalize(vmin=vmin, vmax=vmax)
    if cmap is None:
        cmap = plt.get_cmap('viridis')
    
    L = 0.5*sidelength
    print "L =", L, "Mpc"

    fig, ax = plt.subplots(figsize=(12,12)) ### needs to be square for the border to have the same thickness on all edges
    img = ax.imshow(amap, norm=norm, cmap=cmap,
                    extent=[-L, L, -L, L], origin="lower")
    
        
    if show_labels:
        ax.set_xlabel("x [Mpc]")
        ax.set_ylabel("y [Mpc]")
    else:
        plt.setp(ax.get_xticklabels(), visible=False)
        plt.setp(ax.get_yticklabels(), visible=False)
        plt.setp(ax.get_xticklines(), visible=False)
        plt.setp(ax.get_yticklines(), visible=False)
    
    for i in ax.spines.itervalues():
        if image_only:
            i.set_visible(False)
        else:
            i.set_color(col)
            i.set_linewidth(lw)

    if not image_only and cbar_label is not None:
        cax = inset_axes(ax, width=cbar_width, height="100%",
                 loc=3,
                 bbox_to_anchor=(1.0+cbar_dist, 0.0, 1, 1),
                 bbox_transform=ax.transAxes,
                 borderpad=0,
                 )
        cbar = fig.colorbar(img, cax=cax, orientation='vertical')
        cbar.set_label(cbar_label)
        
        ### Distance scale ###
        figpos = np.array([0.5, 0.9]) ## position of middle of scale in fig coords
        pos = figpos * 2*L - L ## convert to axes units (Mpc relative to image centre)
        ## Plot line 5 pMpc in length
        d = 0.01
        ax.plot([pos[0]-d/2.0, pos[0]+d/2.0], [pos[1], pos[1]], lw=2, c='white')
        ax.text(pos[0], pos[1]+L/20.0, "{:.0f} kpc".format(d*1000.0), horizontalalignment='center',
                color='white', fontsize='medium')
    
    fig.patch.set_facecolor('k')
    ax.set_axis_bgcolor('k')
    
    fig.savefig(outname, bbox_inches='tight', dpi=200, transparent=True)
    print outname
    
def asinh(inputArray, scale_min=None, scale_max=None, non_linear=2.0):
    """
    From eqn 1 in Lupton+2004, this is the standard function for mapping
    a value onto the range [0,1]. Here the scaling function is arcsinh as
    recommended in Lupton+2004 but could be changed to e.g. natural log
    Args:
        scale_min: minimum value to display. Default is minimum of array.
        scale_max: maximum value to display. Default is maximum of array.
        non_linear: the softening parameter beta from Lupton+04
    """
    import numpy as np
    
    imageData=np.array(inputArray, copy=True)

    if scale_min == None:
        scale_min = imageData.min()
        #print "Using scale_min =", scale_min
    if scale_max == None:
        scale_max = imageData.max()
        #print "Using scale_max =", scale_max
    factor = np.arcsinh((scale_max - scale_min)/non_linear)
    indices0 = np.where(imageData < scale_min)
    indices1 = np.where((imageData >= scale_min) & (imageData <= scale_max))
    indices2 = np.where(imageData > scale_max)
    imageData[indices0] = 0.0
    imageData[indices2] = 1.0
    imageData[indices1] = np.arcsinh((imageData[indices1] - scale_min)/non_linear)/factor

    return imageData
            
if __name__=="__main__":
    main()
