import matplotlib.pyplot as plt
import numpy as np
from matplotlib.colors import LogNorm

#plt.rc('text',usetex=True)
#plt.rc("font",family="serif",serif="Times")
outdir = "/data/curie4/nh444/project1/SZ/"

indir = "/data/curie4/nh444/project1/SZ/LDRIntRadioEffBHVel/c128_box_Arepo_new/"
indir = "/data/curie4/nh444/project1/SZ/MDRInt/c256_MDRInt/"
indir = "/data/curie4/nh444/project1/SZ/MDRInt/c320_MDRInt/"
indir = "/data/curie4/nh444/project1/SZ/MDRInt/c384_MDRInt/"

group = 0
name_suffix=""
name_suffix="_default"
name_suffix = "_default_z02"

outname = "SZ_map_c256_MDRInt.pdf"
outname = "SZ_map_c320_MDRInt.pdf"
outname = "SZ_map_c384_MDRInt.pdf"

arcmin = True
textsize="medium" 

dat = np.load(indir+"map_grp"+str(group)+"_SZ"+name_suffix+"_proj.npz")
maparr = dat["maparr"]
vmax = np.max(maparr)
vmin = 1e-11#np.min(maparr[maparr>0.0])
print "sum, vmin, vmax =", np.sum(maparr), vmin, vmax

circrad = [1.0] ## list of radii (r500) of circles drawn at centre, set empty list to disable

cenx,ceny,cenz,sidelength,zthick,a_z,a_x,a_z2,t,z,h,M500,R500 = dat["mappars"]
M500 *= 1e10 / h
print "M500 =", M500, "at z =", z
Mexp = int(np.log10(M500))

fig, ax = plt.subplots(figsize=(7,7))

mycmap = plt.get_cmap('magma')
mycmap.set_bad(color='k')

if arcmin:
    import sim_python.cosmo as cosmo 
    cm = cosmo.cosmology(h, 0.3065) ## assume omega_m=0.3065 in flat cosmology
    kpc_arcsec = cm.arcsec_to_phys(1, z)*1000.0 / h ## kpc per arcsec
    unitfac = 1.0/h/(1+z) / kpc_arcsec / 60.0 ##convert ckpc/h to arcmin
else:
    unitfac = 1.0/1000.0/h/(1+z) ## convert ckpc/h to pMpc
L = 0.5*sidelength * unitfac

img = ax.imshow(maparr.transpose(),cmap=mycmap,
                extent=[-L, L, -L, L],
                origin="lower", norm=LogNorm(vmin=vmin, vmax=vmax))
if arcmin:
    ax.set_xlabel("x [arcmin]",size=textsize)
    ax.set_ylabel("y [arcmin]",size=textsize)
    ## Add circle for Planck beam 
    rad = 7.27/2.0 #143 GHz beam with FWHM 7.27 arcmin
    circle = plt.Circle((-L+1.5*rad, -L+1.5*rad), rad, fc='none', ec='white', ls='solid')
    ax.add_artist(circle)
    #ax.annotate("Planck 143 GHz beam (FWHM)", xy=(-L+3*rad, -L+0.75*rad), xycoords="data", color='white', weight='bold', fontsize='x-small')
    xmin, xmax = ax.get_xlim()
    ax.annotate(r"Planck 143 GHz FWHM", xy=((2.5*rad)/(xmax-xmin)+0.02, (0.5*rad)/(xmax-xmin)+0.02), xycoords="axes fraction", color='white', weight='bold', fontsize='x-small')
else:
    ax.set_xlabel("x [Mpc]",size=textsize)
    ax.set_ylabel("y [Mpc]",size=textsize)
    
ax.annotate(r"M500 = ${:.1f} \times 10^{{{:d}}} M_{{\odot}}$".format(M500/10**Mexp, Mexp)+"\n"+r"$z={:.1f}$".format(z),
            xy=(0.08, 0.83), xycoords="axes fraction", color='white', weight='bold', fontsize='small')

if len(circrad) > 0:
    for crad in circrad:
        circle = plt.Circle((0, 0), crad * R500 * unitfac, fc='none', ec='white', ls='dotted')
        ax.add_artist(circle)


#cbar = fig.colorbar(img)
cbar = plt.colorbar(img,fraction=0.046, pad=0.04)
#cbar.set_label("Y$_{\mathrm{SZ}}$ [Mpc$^2$]", size="x-large")
cbar.set_label("dimensional Compton y parameter", size="medium")

#for curline in ax.spines.values(): # change width of axes
#  curline.set_linewidth(0.5)

for label in ax.get_xticklabels() + ax.get_yticklabels():   # set size of axes tick numbers
  label.set_fontsize(textsize)

for label in cbar.ax.get_xticklabels() + cbar.ax.get_yticklabels():   # set size of axes tick numbers
  label.set_fontsize(textsize)

plt.tight_layout()
fig.savefig(outdir+outname, bbox_inches='tight')
print "Plot saved to", outdir+outname

