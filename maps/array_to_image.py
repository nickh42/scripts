"""
Loads a saved numpy array and saves as an image with a logarithmic colour scale
Each element of the numpy array describes one pixel.
"""

import matplotlib.pyplot as plt
import numpy as np
from matplotlib.colors import LogNorm

indir = "/home/nh444/data/project1/maps/MDRInt/"
outdir = indir

name_suffixes = ["c448_MDRInt_2r500"]
#name_suffixes = ["c448_MDRInt_az0.0_ax0.0",
#                 "c448_MDRInt_az0.5_ax0.0",
#                 "c448_MDRInt_az1.0_ax0.0",
#                 "c448_MDRInt_az1.5_ax0.0",
#                 "c448_MDRInt_az0.0_ax0.5",
#                 ]
maps = ["density", "temperature", "xray"]
log = [True, True, True] ## log colour scale for each map

for name_suffix in name_suffixes:
    for i, curmap in enumerate(maps):
        filename = "map_"+curmap+"_"+name_suffix+"_proj"
        dat = np.load(indir+filename+".npz")
        maparr = dat["maparr"]
        vmax = np.max(maparr)
        vmin = np.min(maparr[maparr>0.0])
        print "sum, vmin, vmax =", np.sum(maparr), vmin, vmax
        if log[i]:
            norm = LogNorm(vmin=vmin, vmax=vmax)
        else:
            norm = plt.Normalize(vmin=vmin, vmax=vmax)
        
        cmap = plt.get_cmap('magma')
        cmap.set_bad(color='k')
        
        img = cmap(norm(maparr))
        plt.imsave(outdir+filename+".png", img, format="png", origin="lower")
        print "Image saved to", outdir+filename+".png"

