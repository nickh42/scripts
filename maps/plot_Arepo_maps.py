#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 19 18:31:48 2017
Plots maps output by Arepo, which are binary files of a flattened array
@author: nh444
"""
import numpy as np
import matplotlib.pyplot as plt

indir = "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c96_test/"
filename = "density_proj_025"

indir = "/data/curie4/nh444/areGPU/test/"
filename = "density_proj_075"

with open(indir+filename, "rb") as f:
    xpix = np.fromfile(f, dtype=np.int32, count=1)[0] ## first 32 bits are no. pixels in x
    ypix = np.fromfile(f, dtype=np.int32, count=1)[0] ## no. of pixels in y
    img = np.fromfile(f, dtype=np.float32, count=xpix*ypix) ## the flattened image array
img = np.reshape(img, (xpix, ypix))
fig, ax = plt.subplots(figsize=(7,7))
img = np.rot90(img)
ax.imshow(np.log10(img), cmap=plt.get_cmap('viridis'))
