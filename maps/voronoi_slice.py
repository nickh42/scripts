"""
Functions for plotting slices of a Voronoi mesh from Arepo snapshots
"""
import numpy as np

def main():
    import numpy as np
    
    #set directories containing snapshots
    simdirs = ["/home/nh444/data/project1/zoom_ins/MDRInt/c448_MDRInt/"]
    mpc_units = True
    subhalo=8951
    
    Xpixels=1024
    Ypixels=1024
    
    #set grid size (note slice is centered between Zmin and Zmax, these should not be equal, as this region encompasses volume searched to find local cells)
#    Xmin = -80.
#    Xmax = 80.
#    Ymin = -80.
#    Ymax = 80.
    size = 100.0
    Xmin = Ymin = -size
    Xmax = Ymax = size
    Zmin = -50.
    Zmax = 50.
    
    #set snapshot numbers
    start_snap = 25
    end_snap = 25
    dsnap = 1
    
    snapnums = np.arange(start_snap, end_snap+1,dsnap)
    
    for simdir in simdirs:
        for snapnum in snapnums:
            plot_voronoi_slice(simdir, snapnum, subhalo=subhalo, toPlot="dens",
                               Xmin=Xmin, Xmax=Xmax,
                               Ymin=Ymin, Ymax=Ymax,
                               Zmin=Zmin, Zmax=Zmax,
                               #a_z=0, a_x=0, a_z2=0,
                               Xpixels=Xpixels, Ypixels=Ypixels,
                               colourise=True, colhsv=False, line_col='white',
                               image_only=True,
                               mpc_units=mpc_units)
#            for a_x in np.linspace(-0.1, 0.1, num=20):
#                plot_voronoi_slice(simdir, snapnum, subhalo=subhalo, toPlot="dens",
#                                   Xmin=Xmin, Xmax=Xmax,
#                                   Ymin=Ymin, Ymax=Ymax,
#                                   Zmin=Zmin, Zmax=Zmax,
#                                   a_z=0.25, a_x=a_x, a_z2=0,
#                                   Xpixels=Xpixels, Ypixels=Ypixels,
#                                   colourise=True, colhsv=False, line_col='white',
#                                   image_only=True,
#                                   mpc_units=mpc_units)
        
def plot_voronoi_slice(simdir, snapnum, ax=None, toPlot="dens",
                       centre=None, group=-1, subhalo=-1,
                       Xmin=-500.0, Xmax=500.0,
                       Ymin=-500.0, Ymax=500.0,
                       Zmin=-50.0, Zmax=50.0,
                       a_z=0, a_x=0, a_z2=0, align_to_spin=True,
                       Xpixels=512, Ypixels=512,
                       colourise=False, colhsv=False, line_col='k',
                       image_only=False,
                       outdir="/data/curie4/nh444/project1/maps/",
                       mpc_units=False, verbose=True):
    """
    Plot the voronoi mesh for a given slice.
    Based on code by Martin Bourne.
    Args:
        simdir: directory containing the snapshot
        snapnum: the number of the snapshot
        ax: an pyplot axes object. If None, a new figure is created.
        centre: the centre of the image in ckpc/h
        group: overrides centre to the position of the given FoF group
        subhalo: overrides centre to the position of the given subhalo
        Xmin, Xmax, Ymin, Ymax: limits of the image plane in ckpc/h
        Zmin, Zmax: limits of the slice along the projection axis (in ckpc/h)
        a_z, a_x and a_z2: angles of rotation (in fraction of pi radians) about the z, x and y axes respectively
        align_to_spin: if subhalo >= 0, align the projection axis to the subhalo spin vector
        Xpixels, Ypixels: number of pixels in x and y axis of image
        colourise: colour the Voronoi cells
        colhsv: hue/saturation corresponds to temperature and brightness to density
        line_col: colour of ridge lines
        image_only: plot just the image without axes
        mpc_units: is the snapshot in Mpc units?
    """
    import scipy.spatial
    import matplotlib.pyplot as plt
    import snap
    if image_only:
        ## remove default padding
        import matplotlib as mpl
        mpl.rcParams['savefig.pad_inches'] = 0
    
    #indices of the axes, where z is projection axis:
    Xi = 0 
    Yi = 1
    Zi = 2

    ## Centre of slice relative to 'centre' (usually zero):
    XC = (Xmax + Xmin) / 2.
    YC = (Ymax + Ymin) / 2.
    ZC = (Zmax + Zmin) /2.
    
    ## Lengths of slice:
    LengthX = Xmax - Xmin
    LengthY = Ymax - Ymin
    LengthZ = Zmax - Zmin
    
    ## Limits of slice:
    XSmax = XC + LengthX * 0.5
    YSmax = YC + LengthY * 0.5
    XSmin = XC - LengthX * 0.5
    YSmin = YC - LengthY * 0.5
    ZSmax = ZC + LengthZ * 0.5
    ZSmin = ZC - LengthZ * 0.5

    ## Create figure if an axes is not given
    if ax is None:
        have_ax = False
        if image_only:
            fig = plt.figure(figsize=(10,10))
            ax = plt.axes([0,0,1,1], frameon=False) ## turn off axes border with frameon=False
            ## disable axes
            ax.get_xaxis().set_visible(False)
            ax.get_yaxis().set_visible(False)
            ## remove padding between plotted data and frame
            plt.autoscale(tight=True)
        else:
            fig, ax = plt.subplots(figsize=(10,10))
    else:
        have_ax = True
        
    if verbose: print "Loading snapshot..."
    sim = snap.snapshot(simdir, snapnum, mpc_units=mpc_units)
    if centre is None: ## default is (0,0,0)
        centre = np.zeros(3)
    if group >= 0:
        ## Use FoF group centre
        centre = sim.cat.group_pos[group]
    elif subhalo >= 0:
        ## Use subhalo centre
        centre = sim.cat.sub_pos[subhalo]
        
    if verbose: print "Loading gas positions..."
    sim.load_gaspos()
    ## Positions of gas cells relative to the centre:
    posg = sim.rel_pos(sim.gaspos, centre)
    xg = posg[:,Xi]
    yg = posg[:,Yi]
    zg = posg[:,Zi]
    
    ## Rotation
    rotate = False
    ## Rotate camera axis
    if align_to_spin and subhalo >= 0:
        n = sim.cat.sub_spin[subhalo]
        ## Get rotation matrix that transforms z unit vector to n
        rotmat = R_2vect([0,0,1], n)
        rotate = True
    elif (a_z!=0 or a_x!=0 or a_z2!=0):
        rotmat = rot_matrix(a_z*np.pi, a_x*np.pi, a_z2*np.pi) # rotates coordinate frame / camera
        rotmat = rotmat.transpose()
        rotate = True
    if rotate:
        pos = np.column_stack((xg, yg, zg))
        pos = np.dot(pos, rotmat)
        xg = pos[:,0]
        yg = pos[:,1]
        zg = pos[:,2]
        if verbose:
            print "[1,0,0] becomes", np.dot(np.array([1,0,0]), rotmat.transpose())
            print "[0,1,0] becomes", np.dot(np.array([0,1,0]), rotmat.transpose())
            print "[0,0,1] becomes", np.dot(np.array([0,0,1]), rotmat.transpose())

    ## Find cells within the slice
    ind = np.where((xg <= XSmax) & (xg >= XSmin) & (yg <= YSmax) & (yg >= YSmin) & (zg <= ZSmax) & (zg >= ZSmin))[0]
    xg = xg[ind]
    yg = yg[ind]
    zg = zg[ind]
    ## Create new array with x, y, z in the order specified by Xi, Yi, Zi
    gas_points = np.dstack((xg.ravel(), yg.ravel(), zg.ravel()))[0]
    ## Create KDTree for all relevant gas cells
    if verbose: print "Creating kdTree..."
    gas_tree = scipy.spatial.cKDTree(gas_points)
    ## Create grid of pixels
    x = np.linspace(Xmin, Xmax, Xpixels)
    y = np.linspace(Ymin, Ymax, Ypixels)
    X,Y = np.meshgrid(x,y)
    ## Grid lies in the z-plane at z=ZC:
    Z = np.full_like(X.ravel(), ZC)
    grid_points = np.dstack((X.ravel(), Y.ravel(), Z.ravel()))[0]
    ## Find nearest gas cells to slice grid points
    rp, loc = gas_tree.query(list(grid_points), k=1) ## nearest k neighbour search
    ## Make a unique list of these cells
    loc = np.unique(loc)
    ## Save their positions
    x = xg[loc]
    y = yg[loc]
    gp = np.dstack((x,y))[0]
    ## Create Voronoi mesh from these positions
    vor = scipy.spatial.Voronoi(gp)
    ## Plot the Voronoi mesh
    if verbose: print "Plotting..."
    if colourise:
        import matplotlib.colors
        if colhsv:
            from matplotlib.colors import hsv_to_rgb
            from matplotlib.colors import rgb_to_hsv
            
            sim.load_gasrho()
            dens = np.log10(sim.gasrho[ind[loc]])
            sim.get_temp()
            temp = np.log10(sim.temp_kelvin[ind[loc]]) ## log K
            densnorm = plt.Normalize(vmin=np.min(dens), vmax=np.max(dens), clip=True)
            tempnorm = plt.Normalize(vmin=np.min(temp), vmax=np.max(temp), clip=True)
            
            from palettable.cmocean.sequential import Deep_20_r as pallette
            cmap = pallette.mpl_colormap
            ## convert the rgb values corresponding to this colormap to hsv
            hsv = rgb_to_hsv(cmap(tempnorm(temp))[:,:3])
            ## replace value (brightness) with normalised density
            hsv[:,2] = densnorm(dens)
            ## convert back to rgb
            col = hsv_to_rgb(hsv)
        else:
            if toPlot == "dens":
                sim.load_gasrho()
                value = np.log10(sim.gasrho[ind[loc]])
            elif toPlot == "temp":
                sim.get_temp()
                value = np.log10(sim.temp[ind[loc]]) ## log keV
            else:
                raise ValueError("toPlot unrecognised.")
            
            #cmap = plt.get_cmap('viridis')
            from palettable.cmocean.sequential import Deep_20_r as pallette
            cmap = pallette.mpl_colormap        
            norm = plt.Normalize(vmin=value.min(), vmax=value.max())
            col = cmap(norm(value))
        
        regions, vertices = voronoi_finite_polygons_2d(vor)
        
        # colorize
        for idx, region in enumerate(regions):
            polygon = vertices[region] ## list of x, y positions defining the region to fill
            polygon = zip(*polygon)
            cur_col = matplotlib.colors.rgb2hex(col[idx, :3]) ## need a hex string for the colour
            plt.fill(polygon[0], polygon[1], cur_col, ec=line_col)
    else:
        scipy.spatial.voronoi_plot_2d(vor, ax=ax,
                                      line_colors=line_col, line_width=0.5,
                                      show_points=False, show_vertices=False)
        
    if image_only:
        plt.setp(ax.get_xticklabels(), visible=False)
        plt.setp(ax.get_yticklabels(), visible=False)
        plt.setp(ax.get_xticklines(), visible=False)
        plt.setp(ax.get_yticklines(), visible=False)
        
    if not have_ax:
        ax.set_xlim([Xmin, Xmax])
        ax.set_ylim([Ymin, Ymax])
        if not image_only:
            ax.set_xlabel("x (kpc/h)")
            ax.set_ylabel("y (kpc/h)")
            plt.tight_layout()
        fig.savefig(outdir+"voronoi_slice.pdf", bbox_inches='tight')
        print "Saved to", outdir+"voronoi_slice.pdf"
        
def voronoi_finite_polygons_2d(vor, radius=None):
    """
    Reconstruct infinite voronoi regions in a 2D diagram to finite
    regions. Code from https://gist.github.com/pv/8036995
    Parameters
    ----------
    vor : Voronoi
        Input diagram
    radius : float, optional
        Distance to 'points at infinity'.
    Returns
    -------
    regions : list of tuples
        Indices of vertices in each revised Voronoi regions.
    vertices : list of tuples
        Coordinates for revised Voronoi vertices. Same as coordinates
        of input vertices, with 'points at infinity' appended to the
        end.
    """

    if vor.points.shape[1] != 2:
        raise ValueError("Requires 2D input")

    new_regions = []
    new_vertices = vor.vertices.tolist()

    center = vor.points.mean(axis=0)
    if radius is None:
        radius = vor.points.ptp().max()*2

    # Construct a map containing all ridges for a given point
    all_ridges = {}
    for (p1, p2), (v1, v2) in zip(vor.ridge_points, vor.ridge_vertices):
        all_ridges.setdefault(p1, []).append((p2, v1, v2))
        all_ridges.setdefault(p2, []).append((p1, v1, v2))

    # Reconstruct infinite regions
    for p1, region in enumerate(vor.point_region):
        vertices = vor.regions[region]

        if all(v >= 0 for v in vertices):
            # finite region
            new_regions.append(vertices)
            continue

        # reconstruct a non-finite region
        ridges = all_ridges[p1]
        new_region = [v for v in vertices if v >= 0]

        for p2, v1, v2 in ridges:
            if v2 < 0:
                v1, v2 = v2, v1
            if v1 >= 0:
                # finite ridge: already in the region
                continue

            # Compute the missing endpoint of an infinite ridge

            t = vor.points[p2] - vor.points[p1] # tangent
            t /= np.linalg.norm(t)
            n = np.array([-t[1], t[0]])  # normal

            midpoint = vor.points[[p1, p2]].mean(axis=0)
            direction = np.sign(np.dot(midpoint - center, n)) * n
            far_point = vor.vertices[v2] + direction * radius

            new_region.append(len(new_vertices))
            new_vertices.append(far_point.tolist())

        # sort region counterclockwise
        vs = np.asarray([new_vertices[v] for v in new_region])
        c = vs.mean(axis=0)
        angles = np.arctan2(vs[:,1] - c[1], vs[:,0] - c[0])
        new_region = np.array(new_region)[np.argsort(angles)]

        # finish
        new_regions.append(new_region.tolist())

    return new_regions, np.asarray(new_vertices)

def rot_matrix(a_z, a_x, a_z2): ## returns 3x3 rotation matrix -- rotates coordinate frame (not particles), arguments are in radians
    """
    Returns 3x3 rotation matrix
    Rotates by a_z*pi radians around z axis, then a_x*pi around x axis, then a_z2*pi around z axis again
    Rotations are anticlockwise and the angles are in fraction of pi radians
    Usage: e.g.
        rotmat = rot_matrix(a_z*np.pi, a_x*np.pi, a_z2*np.pi)
        pos = np.dot(pos,rotmat.transpose())
    """
    return np.array([[(np.cos(a_z2)*np.cos(a_z)-np.cos(a_x)*np.sin(a_z)*np.sin(a_z2)), (np.cos(a_z2)*np.sin(a_z)+np.cos(a_x)*np.cos(a_z)*np.sin(a_z2)), np.sin(a_z2)*np.sin(a_x)],
                  [(-np.sin(a_z2)*np.cos(a_z)-np.cos(a_x)*np.sin(a_z)*np.cos(a_z2)), (-np.sin(a_z2)*np.sin(a_z)+np.cos(a_x)*np.cos(a_z)*np.cos(a_z2)), np.sin(a_x)*np.cos(a_z2)],
                  [np.sin(a_x)*np.sin(a_z), -np.sin(a_x)*np.cos(a_z), np.cos(a_x)]])
    
def R_2vect(vector_orig, vector_fin):
    """Calculate the rotation matrix required to rotate from one vector to another.
    For the rotation of one vector to another, there are an infinite series of rotation matrices
    possible.  Due to axially symmetry, the rotation axis can be any vector lying in the symmetry
    plane between the two vectors.  Hence the axis-angle convention will be used to construct the
    matrix with the rotation axis defined as the cross product of the two vectors.  The rotation
    angle is the arccosine of the dot product of the two unit vectors.
    Given a unit vector parallel to the rotation axis, w = [x, y, z] and the rotation angle a,
    the rotation matrix R is::
              |  1 + (1-cos(a))*(x*x-1)   -z*sin(a)+(1-cos(a))*x*y   y*sin(a)+(1-cos(a))*x*z |
        R  =  |  z*sin(a)+(1-cos(a))*x*y   1 + (1-cos(a))*(y*y-1)   -x*sin(a)+(1-cos(a))*y*z |
              | -y*sin(a)+(1-cos(a))*x*z   x*sin(a)+(1-cos(a))*y*z   1 + (1-cos(a))*(z*z-1)  |
    @param vector_orig: The unrotated vector defined in the reference frame.
    @type vector_orig:  numpy array, len 3
    @param vector_fin:  The rotated vector defined in the reference frame.
    @type vector_fin:   numpy array, len 3
    
    Returns a 3x3 rotation matrix as a numpy array
    """
    from math import acos, cos, sin
    from numpy.linalg import norm

    ## Convert vectors to numpy arrays
    vector_orig = np.array(vector_orig)
    vector_fin = np.array(vector_fin)
    
    # Convert the vectors to unit vectors.
    vector_orig = vector_orig / norm(vector_orig)
    vector_fin = vector_fin / norm(vector_fin)

    # The rotation axis (normalised).
    axis = np.cross(vector_orig, vector_fin)
    axis_len = norm(axis)
    if axis_len != 0.0:
        axis = axis / axis_len

    # Alias the axis coordinates.
    x = axis[0]
    y = axis[1]
    z = axis[2]

    # The rotation angle.
    angle = acos(np.dot(vector_orig, vector_fin))

    # Trig functions (only need to do this maths once!).
    ca = cos(angle)
    sa = sin(angle)

    R = np.zeros((3,3))
    # Calculate the rotation matrix elements.
    R[0,0] = 1.0 + (1.0 - ca)*(x**2 - 1.0)
    R[0,1] = -z*sa + (1.0 - ca)*x*y
    R[0,2] = y*sa + (1.0 - ca)*x*z
    R[1,0] = z*sa+(1.0 - ca)*x*y
    R[1,1] = 1.0 + (1.0 - ca)*(y**2 - 1.0)
    R[1,2] = -x*sa+(1.0 - ca)*y*z
    R[2,0] = -y*sa+(1.0 - ca)*x*z
    R[2,1] = x*sa+(1.0 - ca)*y*z
    R[2,2] = 1.0 + (1.0 - ca)*(z**2 - 1.0)
    
    return R

if __name__=="__main__":
    main()