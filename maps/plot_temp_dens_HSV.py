"""
Loads a saved numpy array and saves as an image with a logarithmic colour scale
Each element of the numpy array describes one pixel.
"""

import matplotlib.pyplot as plt
import numpy as np
from matplotlib.colors import hsv_to_rgb
from matplotlib.colors import rgb_to_hsv
from mpl_toolkits.axes_grid.inset_locator import (inset_axes, InsetPosition, mark_inset)
from matplotlib.ticker import MultipleLocator

indir = "/home/nh444/data/project1/maps/MDRInt/"
outdir = indir

name_suffixes = ["c448_MDRInt"]
#name_suffixes = ["c448_MDRInt_az0.0_ax0.0",
#                 "c448_MDRInt_az0.5_ax0.0",
#                 "c448_MDRInt_az1.0_ax0.0",
#                 "c448_MDRInt_az1.5_ax0.0",
#                 "c448_MDRInt_az0.0_ax0.5",
#                 ]

logtemp = True ## logarithmic scale on temperature
circrad = [5.0] ## list of radii (r500) of circles drawn at centre, set empty list to disable
show_labels = False ## show axis and tick labels on main plot

for name_suffix in name_suffixes:
    fig, ax = plt.subplots(figsize=(15,10))
    
    ## Load density and temp maps
    cenx,ceny,cenz,sidelength,zthick,a_z,a_x,a_z2,t,z,h,M500,R500 = np.load(indir+"map_density_"+name_suffix+"_proj.npz")["mappars"]
    dens = np.load(indir+"map_density_"+name_suffix+"_proj.npz")["maparr"]
    npix = dens.shape[0]
    assert dens.shape[1] == npix, "density map not square."
    temp = np.load(indir+"map_temperature_"+name_suffix+"_proj.npz")["maparr"]
    assert temp.shape[0] == temp.shape[1] == npix, "temp map does not match density map dimensions "+str(npix)+"x"+str(npix)
    assert np.all(dens > 0.0) and np.all(temp > 0.0), "Cannot have zero values."
    ## Scale maps
    dens *= 1e10 / h / (sidelength / h / (1+z) / npix)**2 ## convert 1e10 Msun/h per pixel to Msun/kpc^2
    dens = np.log10(dens)
    if logtemp:
        temp = np.log10(temp)
    densmax = np.max(dens)
    densmin = np.min(dens)
    print "densmax, densmin =", densmax, densmin
    tempmax = np.max(temp)
    tempmin = np.min(temp)
    print "tempmax, tempmin", tempmax, tempmin
    
    densnorm = plt.Normalize(vmin=densmin, vmax=densmax)
    tempnorm = plt.Normalize(vmin=tempmin, vmax=tempmax)
       
    #from palettable.matplotlib import Inferno_20 as pallette
    #from palettable.cmocean.sequential import Thermal_20 as pallette
    from palettable.cmocean.sequential import Deep_20_r as pallette
    #from palettable.cmocean.sequential import Dense_20_r as pallette
    cmap = pallette.mpl_colormap
    #cmap = plt.get_cmap('gnuplot2') ## matplotlib colormap
    ## convert the rgb values corresponding to this colormap to hsv
    hsv = rgb_to_hsv(cmap(tempnorm(temp))[:,:,:3])
    ## replace value (brightness) with normalised density
    hsv[:,:,2] = densnorm(dens) ## value (brightness)
    rgb = hsv_to_rgb(hsv)
       
    unitfac = 1.0 / 1000.0 / h / (1+z) ## to convert ckpc/h to pMpc
    L = 0.5*sidelength * unitfac
    print "L =", L, "Mpc"
    img = ax.imshow(rgb,
                    extent=[-L, L, -L, L], origin="lower")
    
    for crad in circrad:
        circle = plt.Circle((0, 0), crad * R500 * unitfac, fc='none', ec='white', alpha=0.5, lw=2, ls='dashed')
        ax.add_artist(circle)
        
    if show_labels:
        ax.set_xlabel("x [Mpc]")
        ax.set_ylabel("y [Mpc]")
    else:
        plt.setp(ax.get_xticklabels(), visible=False)
        plt.setp(ax.get_yticklabels(), visible=False)
        plt.setp(ax.get_xticklines(), visible=False)
        plt.setp(ax.get_yticklines(), visible=False)
    
    ### Galaxy zoom-in
    col = '0.9' ## colour of lines and edges
    lw = 1.5
    ax2 = inset_axes(ax, width="30%", height=2.0, loc=2)
    ax2.imshow(rgb,
               extent=[-L, L, -L, L], origin="lower")
    ax2.set_xlim(2.0, 5.0)
    ax2.set_ylim(3.0, 6.0)
    size = 0.45
    ip1 = InsetPosition(ax, [1.01, 1.0-size, size, size]) ## position, size
    ax2.set_axes_locator(ip1)
    mark_inset(ax, ax2, loc1=2, loc2=3, fc="none", ec=col, lw=lw)
    plt.setp(ax2.get_xticklabels(), visible=False)
    plt.setp(ax2.get_yticklabels(), visible=False)
    plt.setp(ax2.get_xticklines(), visible=False)
    plt.setp(ax2.get_yticklines(), visible=False)
    fig.subplots_adjust(right=0.6) ## shift everything to the left so we can see it all in fig.show()
    for i in ax2.spines.itervalues():
        i.set_color(col)
        i.set_linewidth(lw)
    
    ## X-ray core zoom-in
    ax3 = inset_axes(ax, width="30%", height=2.0, loc=2)
    ax3.imshow(rgb,
               extent=[-L, L, -L, L], origin="lower")
    ax3.set_xlim(-2.0, 2.0)
    ax3.set_ylim(-2.0, 2.0)
    size = 0.45
    ip2 = InsetPosition(ax, [1.01, 0.0, size, size]) ## position, size
    ax3.set_axes_locator(ip2)
    mark_inset(ax, ax3, loc1=1, loc2=3, fc="none", ec=col, lw=lw)
    plt.setp(ax3.get_xticklabels(), visible=False)
    plt.setp(ax3.get_yticklabels(), visible=False)
    plt.setp(ax3.get_xticklines(), visible=False)
    plt.setp(ax3.get_yticklines(), visible=False)
    for i in ax3.spines.itervalues():
        i.set_color(col)
        i.set_linewidth(lw)

    ### Colourbar ###
    ## Create image for colourbar
    num = 100
    col_ind = np.linspace(0.0, 1.0, num=num)
    ind = np.repeat(np.stack((col_ind, col_ind)), [1,num-1], axis=0)
    hsv = rgb_to_hsv(cmap(ind)[:,:,:3])
    hsv[:,:,2] = np.rot90(ind) ## value (brightness)
    rgb = hsv_to_rgb(hsv)
    ## Set size and location of colorbar
    cax = plt.axes([0,0,1,1])
    #cpos = [0.77, 0.07] ## position bottom right
    cpos = [0.09, 0.08] ## position bottom left
    ip = InsetPosition(ax, [cpos[0],cpos[1],0.22,0.22]) ## set position and size
    cax.set_axes_locator(ip)
    cax.imshow(rgb, extent=[tempmin, tempmax, densmin, densmax])
    ## Set colorbar ticks, labels etc
    cax.set_ylabel(r"$\mathrm{log}_{10}$($\rho$ [$M_{\odot} \mathrm{kpc}^{-2}$])", fontsize='x-small')
    cax.set_xlabel("$\mathrm{log}_{10}$(T [keV])", fontsize='x-small')
    cax.xaxis.label.set_color('white')
    cax.yaxis.label.set_color('white')
    cax.xaxis.set_minor_locator(MultipleLocator(0.2)) ## minor tick spacing
    cax.yaxis.set_minor_locator(MultipleLocator(0.2))
    cax.tick_params(axis='x', which='both', colors='white', labelsize=11)
    cax.tick_params(axis='y', which='both', colors='white', labelsize=11)
    [i.set_color(col) for i in cax.spines.itervalues()] ## change colour
    
    fig.savefig(outdir+"map_hsv_"+name_suffix+".pdf", bbox_inches='tight')
    print "Saved to", outdir+"map_hsv_"+name_suffix+".pdf"
