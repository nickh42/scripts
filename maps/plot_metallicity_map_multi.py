import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from mpl_toolkits.axes_grid1 import ImageGrid
ag_hydrogen = 0.70683 # Anders & Grevesse solar mass fractions
ag_helium = 0.27431
ag_metals = 0.01886

dirnames = ["/home/nh444/data/project1/maps/LDRIntRadioEffBHVel/c96_box_Arepo",
            "/home/nh444/data/project1/maps/LDRIntRadioEffBHVel/c96_FeedbackFix",
            "/home/nh444/data/project1/maps/LDRIntRadioEffBHVel/c96_FeedbackFix_OldGrav",
            "/home/nh444/data/project1/maps/LDRIntRadioEffBHVel/c96_FeedbackFix_OldGrav_2",
            #"/home/nh444/data/project1/maps/LDRIntRadioEffBHVel/c96_Arepo_March15",
            #"/home/nh444/data/project1/maps/LDRIntRadioEffBHVel/c128_Arepo_May31",
            ]
outdir = "/data/curie4/nh444/project1/maps/LDRIntRadioEffBHVel/"
outname = "maps_c96_metallicity_HighRes"
dirlabels = ["c96 old arepo", "c96 Aug arepo", "c96 Aug arepo old grav", "c96 Aug old grav + RCUT \n+ IND_GRAV_SOFT"]
file_suffixes = ["_metallicity_HighRes_proj",
                 #"_metallicity_HalfHighRes_proj",
                 ]
filelabels = [""]*len(file_suffixes)
groups = [0]

dirnames = ["/home/nh444/data/project1/maps/LDRIntRadioEffBHVel/c128_Arepo_March15",
            "/home/nh444/data/project1/maps/LDRIntRadioEffBHVel/c128_Arepo_May31",
            ]
outdir = "/data/curie4/nh444/project1/maps/LDRIntRadioEffBHVel/"
outname = "maps_c128_metallicity_HighRes"
dirlabels = ["c128 March 15 arepo", "c128 May 31 arepo"]
file_suffixes = ["_metallicity_HighRes_proj",
                 #"_metallicity_HalfHighRes_proj",
                 ]
filelabels = [""]*len(file_suffixes)
groups = [0]

colbar_per_row = False
no_colbar = False
colbarlabel = "Z [Z$_{\odot}$]"
white_on_black = False
mylims = None
mylims = [0.01, 0.4] ## These will override minval, maxval
log=True

n = len(dirnames)*len(file_suffixes) ## total number of maps
if len(file_suffixes) == 1:
    nrow = max(1, int(len(dirnames))) ## number of rows
else:
    nrow = max(1, int(n/len(file_suffixes))) ## number of rows
if nrow>1: ncol = int(round(n/nrow)) ## number of columns
else: ncol = n

print "Plotting", n, "maps..."
print "ncol, nrow =", ncol, nrow
fig = plt.figure(figsize=(max(7,4*ncol),max(6,4*nrow))) ## allow ~4 inches per map

if colbar_per_row: ## same colorbar for each row
    cbar_mode="each"
    cbar_size="7%"
elif no_colbar:
    cbar_mode="None"
    cbar_size="5%"
else: ## same colorbar for everything
    cbar_mode="single"
    cbar_size="{:d}%".format(max(1,7/nrow))
grid = ImageGrid(fig, 111,
                 nrows_ncols=(nrow,ncol),
                 axes_pad=0,
                 #share_all=True,
                 label_mode="L", ## labels only on left and bottom
                 cbar_location="right",
                 cbar_mode=cbar_mode,
                 cbar_size=cbar_size,
                 cbar_pad=0,
                 )
icol = 0 # column index
irow = 0
minval = np.inf
maxval = -np.inf
if colbar_per_row:
    minvallist = [np.inf]*nrow
    maxvallist = [-np.inf]*nrow
imgs = []
j=0
for dirnum, dirname in enumerate(dirnames):
    for group in groups:
        for filenum, file_suffix in enumerate(file_suffixes):
            print "irow, icol, j =", irow, icol, j
            dat = np.load(dirname+"/"+"map_grp"+str(group)+file_suffix+".npz")
            maparr = dat["maparr"]
            ## Units of solar metallicity -->
            maparr = maparr / (1-maparr) * (ag_hydrogen+ag_helium)/ag_metals
            cenx,ceny,cenz,sidelength,zthick,a_z,a_x,a_z2 = dat["mappars"]
            
            cmap = plt.get_cmap('viridis')

            if np.min(maparr) < minval: minval = np.min(maparr)
            if np.max(maparr) > maxval: maxval = np.max(maparr)
            if colbar_per_row:
                if np.min(maparr) < minvallist[irow]: minvallist[irow] = np.min(maparr)
                if np.max(maparr) > maxvallist[irow]: maxvallist[irow] = np.max(maparr)
            if log:
                norm = LogNorm(vmin=max(np.min(maparr), 0.01), vmax=np.max(maparr))
            else:
                norm=None
            imgs.append(grid[j].imshow(maparr.transpose(), cmap=cmap,
                            extent=[-0.5*sidelength/1000.0, 0.5*sidelength/1000.0, -0.5*sidelength/1000.0, +0.5*sidelength/1000.0],
                            origin="lower", norm=norm))
            grid[j].set_xlabel("x [Mpc/h]")
            grid[j].set_ylabel("y [Mpc/h]")
            if len(groups) > 1:
                label = "Grp "+str(group)+" "+filelabels[filenum].replace("_","\_")
                #grid[j].annotate("M500 = {:.1f}".format(M500), xy=(0.08,0.1), xycoords="axes fraction", color='white', weight='bold')
            elif len(dirnames) > 1:
                label = dirlabels[dirnum].replace("_","\_")
            else:
                label = filelabels[filenum].replace("_","\_")
            grid[j].annotate(label, xy=(0.08,0.9), xycoords="axes fraction", color='white', weight='bold')

            if white_on_black:
                     grid[j].xaxis.label.set_color('white')
                     grid[j].tick_params(axis='x', which='both', colors='white')
                     grid[j].yaxis.label.set_color('white')
                     grid[j].tick_params(axis='y', which='both', colors='white')
                     import matplotlib
                     for child in grid[j].get_children():
                         if isinstance(child, matplotlib.spines.Spine):
                             child.set_color('white')
            if colbar_per_row:
                cbar = grid.cbar_axes[j].colorbar(imgs[j])
                cbar.solids.set_edgecolor("face") ## to fix white lines in colorbar in PDF
                #grid.cbar_axes[j].set_label("X-ray flux")
                axis = grid.cbar_axes[j].axis[grid.cbar_axes[j].orientation]
                axis.label.set_text(colbarlabel)
                if icol+1<ncol:## before end of row
                    grid.cbar_axes[j].get_yaxis().set_visible(False)
                    
            if icol+1 < ncol:
                icol += 1
            else:
                irow += 1
                icol = 0
            j+=1
    
if colbar_per_row:
    for i, img in enumerate(imgs):
        #print i/ncol, minvallist[i/ncol], maxvallist[i/ncol], round(minvallist[i/ncol]), round(maxvallist[i/ncol])
        if mylims is not None:
            if log:
                assert mylims[0] > 0, "log scale cannot have zero"
            vmin = mylims[0]
            vmax = mylims[1]
        else:
            if log:
                vmin = max(minvallist[i/ncol], 0.01)
            else:
                vmin = minvallist[i/ncol]
            vmax = maxvallist[i/ncol]
        img.set_clim(vmin=vmin, vmax=vmax)
else:
    print "minval, maxval =", minval, maxval
    if mylims is not None:
        if log:
            assert mylims[0] > 0, "log scale cannot have zero"
        vmin = mylims[0]
        vmax = mylims[1]
    else:
        if log:
            vmin = max(minval, 0.01)
        else:
            vmin = minval
        vmax = maxval
    for img in imgs:
        img.set_clim(vmin=vmin, vmax=vmax)
    cbar = grid[0].cax.colorbar(imgs[0])
    cbar.solids.set_edgecolor("face") ## to fix white lines in colorbar in PDF
    cax = grid.cbar_axes[0]
    axis = cax.axis[cax.orientation]
    axis.label.set_text(colbarlabel+" range:["+str(vmin)+"-"+str(vmax)+"]")
    if white_on_black:
        axis.label.set_color('white')
        cax.xaxis.label.set_color('white')
        cax.tick_params(which='both', colors='white')
        for child in cax.get_children():
            if isinstance(child, matplotlib.spines.Spine):
                child.set_color('white')

print "vmin, vmax =", vmin, vmax
print "vmin, vmax =", img.get_clim()
if white_on_black:
    plt.savefig(outdir+outname+".png", bbox_inches='tight', transparent=True)
    print "Plot saved to", outdir+outname+".png"
else:
    plt.savefig(outdir+outname+".pdf", bbox_inches='tight')
    print "Plot saved to", outdir+outname+".pdf"

