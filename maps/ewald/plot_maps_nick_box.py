from pylab import *

matplotlib.rc('text',usetex=True)
matplotlib.rc("font",family="serif",serif="Times")

#----------

dat = load("c384_nick_boxdensity_proj.npz")
maparr = dat["maparr"]
cenx,ceny,cenz,sidelength,zthick,a_z,a_x,a_z2 = dat["mappars"]
#z, r_comov = dat["simpars"]

fig = figure()
ax = fig.add_subplot(111)

mycmap = get_cmap('viridis')
#mycmap = get_cmap('gnuplot')
#mycmap = get_cmap('spectral')
#mycmap = get_cmap('gist_stern')
#mycmap = get_cmap('gist_earth')
#mycmap = get_cmap('gist_ncar')
#mycmap = get_cmap('coolwarm')

img = ax.imshow(log10(maparr.transpose()),cmap=mycmap,extent=[-0.5*sidelength/1000.0, 0.5*sidelength/1000.0, -0.5*sidelength/1000.0, +0.5*sidelength/1000.0], origin="lower")#, vmin=-2.0, vmax=-1.0)
ax.set_xlabel("x [Mpc/h]",size="x-large")
ax.set_ylabel("y [Mpc/h]",size="x-large")

#ax.text(24,22,r"$M_{500c} = 2.5 \times 10^{14} \, h^{-1} M_\odot$", color="white", size="x-large", ha="right")
#ax.text(24,19.5,r"$z = 0.15$", color="white", size="x-large", ha="right")

#cbar = fig.colorbar(img,ticks=[1,2,3,4,5])
#cbar = fig.colorbar(img)
#cbar.set_label("", size="x-large")

#for curline in ax.spines.values(): # change width of axes
#  curline.set_linewidth(0.5)
 
mysize="large" 
 
for label in ax.get_xticklabels() + ax.get_yticklabels():   # set size of axes tick numbers
  label.set_fontsize(mysize)

#for label in cbar.ax.get_xticklabels() + cbar.ax.get_yticklabels():   # set size of axes tick numbers
#  label.set_fontsize(mysize)

fig.show()
fig.savefig("c384_nick_dens.pdf")


