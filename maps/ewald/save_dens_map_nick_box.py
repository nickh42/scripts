import snap

simdir = "/data/emergence4/deboras/Elliptical_ZoomIns/L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff"

sim = snap.snapshot(simdir, 25, mpc_units = False, group_veldisp = False, masstab = True)

r200c = sim.cat.group_r_crit200[0]

sim.get_gas_projection(npix=1024, use_vol=True, filebase="c384_nick_box", save_maps=["density"], name_suffix="", nthreads=4)



