from pylab import *
import snap

simdir = "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c384"

sim = snap.snapshot(simdir, 25, mpc_units = True, group_veldisp = True, masstab = True)

r200c = sim.cat.group_r_crit200[0]

sim.get_gas_projection(sidelength=r200c*1.0, cenx=0.0, ceny=0.0, group=0, zthick=2.0*r200c, npix=512, use_vol=True, only_highres_gas=True, filebase="c384_nick_", save_maps=["density","temperature"], name_suffix="", nthreads=4)



