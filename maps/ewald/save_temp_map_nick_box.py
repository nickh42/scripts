from pylab import *
import snap

simdir = "/data/emergence4/deboras/Elliptical_ZoomIns/L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff"
#simdir = "/data/emergence4/deboras/Elliptical_ZoomIns/L40_512_LowDutyRadioWeak_HalfThermal_FixEta"
#simdir = "/data/emergence4/deboras/Elliptical_ZoomIns/L40_512_NoDutyRadioWeak_HalfThermal_FixEta"

group = 2
sim = snap.snapshot(simdir, 25, mpc_units = False, group_veldisp = False, masstab = True)

r200c = sim.cat.group_r_crit200[group]

sim.get_gas_projection(sidelength=r200c*2.0, cenx=0.0, ceny=0.0, group=group, zthick=2.0*r200c, npix=512, use_vol=True, only_highres_gas=False, filebase="c384_nick_box_", save_maps=["density","temperature"], name_suffix="", nthreads=4)



