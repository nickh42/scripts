import snap

basedir = "/home/nh444/data/project1/zoom_ins/MDRInt/"
outdir = "/data/curie4/nh444/project1/SZ/MDRInt/"
simnames = [#"c128_MDRInt",
            #"c192_MDRInt",
            #"c256_MDRInt",
            #"c320_MDRInt",
            "c384_MDRInt"]
highres_only = True
ngb = 2.5**3 ## factor that multiplies the cell volume and is cube-rooted for the smoothing length
## the gravitational potential of a cell is taken to be 2.5 times its cube-rooted volume

group = 0
for simname in simnames:
    sim = snap.snapshot(basedir+simname, 24, mpc_units=True, group_veldisp=False, masstab=True)
    
    R = sim.cat.group_r_crit500[group] * 5.0
    #zthick=2.0*R
    #zthick = 10.0*R
    zthick = 0 ## whole box
    
    sim.get_gas_projection(sidelength=R*2.0, group=group, zthick=zthick, npix=1024,
                           use_vol=True, filebase="map_grp"+str(group)+"_", save_maps=["SZ"],
                            phase_filter=True, highres_only=highres_only, ngb=ngb,
                            name_suffix="default_z02", nthreads=32, outdir=outdir+simname+"/")
