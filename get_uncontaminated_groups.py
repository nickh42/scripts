#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Jun  6 14:32:14 2019

@author: nh444
"""


def get_uncontaminated_groups(sim, grpind, RminLowRes=5.0):
    """For groups 'grpind', return groups uncontaminated within RminLowRes*R500

    For each group in 'grpind', test if it has any type 2 particles within
    RminLowRes * R500 of its centre. If it does, remove it from the list.
    Arguments:
        sim: snap.snapshot instance.
        grpind: list of FoF groups.
        RminLowRes: see above.
    """
    import numpy as np

    # Calculate distance of Type 2 particles
    sim.load_posarr(parttypes=[2])
    new_grpind = []
    for group in grpind:
        # Keep group 0 in the list regardless of contamination.
        if group == 0:
            new_grpind.append(group)
            continue
        # get distance of Type 2 particles relative to group
        r = sim.nearest_dist_3D(sim.posarr['type2'],
                                sim.cat.group_pos[group])
        # keep group if nearest Type 2 is further
        # than RminLowRes*R500 away
        if np.min(r) > RminLowRes * sim.cat.group_r_crit500[group]:
            new_grpind.append(group)
    return new_grpind
