"""
Python module for performing BCES fitting.
The vast majority of this code was written by R. Nemmen
"""
from __future__ import (absolute_import, division, print_function,
                        unicode_literals)
import numpy as np
import scipy
from scaling.fit_relation import estimate_scatter


# BCES fitting
# ===============

def bces(y1, y1err, y2, y2err, cerr, method="all"):
    """
Does the entire regression calculation for one of four slopes:
OLS(Y|X), OLS(X|Y), bisector, orthogonal.
Fitting form: Y=AX+B.

Usage:

>>> a,b,aerr,berr,covab,scat=bces(x,xerr,y,yerr,cov)

Output:

- a,b : best-fit parameters a,b of the linear regression 
- aerr,berr : the standard deviations in a,b
- covab : the covariance between a and b (e.g. for plotting confidence bands)
- scat : estimate of the intrinsic scatter assuming ZERO errors

Arguments:

- x,y : data
- xerr,yerr: measurement errors affecting x and y
- cerr : covariance between the measurement errors
- method: either "YX", "XY", "bisect", "orthog" or "all"
          for OLS(Y|X), OLS(X|Y), bisector, orthogonal and all four, respectively

v1 Mar 2012: ported from bces_regress.f. Added covariance output.
Rodrigo Nemmen, http://goo.gl/8S1Oo
    """
    
    # Arrays holding the code main results for each method:
    # Elements: 0-Y|X, 1-X|Y, 2-bisector, 3-orthogonal
    a,b,avar,bvar,covarxiz,scat=np.zeros(4),np.zeros(4),np.zeros(4),np.zeros(4),np.zeros(4),np.zeros(4)
    # Lists holding the xi and zeta arrays for each method above
    xi,zeta=[],[]
    
    if method=="YX":
        idx = 0
    elif method=="XY":
        idx = 1
    elif method=="bisect":
        idx = 2
    elif method=="orthog":
        idx = 3
    
    # Calculate sigma's for datapoints using length of conf. intervals
    sig11var = np.mean( y1err**2 )
    sig22var = np.mean( y2err**2 )
    sig12var = np.mean( cerr )
    
    # Covariance of Y1 (X) and Y2 (Y)
    covar_y1y2 = np.mean( (y1-y1.mean())*(y2-y2.mean()) )

    # Compute the regression slopes
    a[0] = (covar_y1y2 - sig12var)/(y1.var() - sig11var)    # Y|X
    a[1] = (y2.var() - sig22var)/(covar_y1y2 - sig12var)    # X|Y
    a[2] = ( a[0]*a[1] - 1.0 + np.sqrt((1.0 + a[0]**2)*(1.0 + a[1]**2)) ) / (a[0]+a[1])    # bisector
    if covar_y1y2<0:
        sign = -1.
    else:
        sign = 1.
    a[3] = 0.5*((a[1]-(1./a[0])) + sign*np.sqrt(4.+(a[1]-(1./a[0]))**2))    # orthogonal
    
    # Compute intercepts
    for i in range(4):
        b[i]=y2.mean()-a[i]*y1.mean()
    
    # Set up variables to calculate standard deviations of slope/intercept 
    xi.append(    ( (y1-y1.mean()) * (y2-a[0]*y1-b[0]) + a[0]*y1err**2 ) / (y1.var()-sig11var)    )    # Y|X
    xi.append(    ( (y2-y2.mean()) * (y2-a[1]*y1-b[1]) - y2err**2 ) / covar_y1y2    )    # X|Y
    xi.append(    xi[0] * (1.+a[1]**2)*a[2] / ((a[0]+a[1])*np.sqrt((1.+a[0]**2)*(1.+a[1]**2))) + xi[1] * (1.+a[0]**2)*a[2] / ((a[0]+a[1])*np.sqrt((1.+a[0]**2)*(1.+a[1]**2)))    )    # bisector
    xi.append(    xi[0] * a[3]/(a[0]**2*np.sqrt(4.+(a[1]-1./a[0])**2)) + xi[1]*a[3]/np.sqrt(4.+(a[1]-1./a[0])**2)    )    # orthogonal
    for i in range(4):
        zeta.append( y2 - a[i]*y1 - y1.mean()*xi[i]    )

    for i in range(4):
        # Calculate variance for all a and b
        avar[i]=xi[i].var()/xi[i].size
        bvar[i]=zeta[i].var()/zeta[i].size
        
        # Sample covariance obtained from xi and zeta (paragraph after equation 15 in AB96)
        covarxiz[i]=np.mean( (xi[i]-xi[i].mean()) * (zeta[i]-zeta[i].mean()) )
        
        ## estimate the intrinsic scatter (assuming zero errors)
        scat[i] = estimate_scatter(y1, y2, a[i], b[i])

    # Covariance between a and b (equation after eq. 15 in AB96)
    covar_ab=covarxiz/y1.size
    
    if method != "all":
        a = a[idx]
        b = b[idx]
        avar = avar[idx]
        bvar = bvar[idx]
        covar_ab = covar_ab[idx]
        scat = scat[idx]
    
    return a, b, np.sqrt(avar), np.sqrt(bvar), covar_ab, scat
        
def bootstrap(v):
    """
Constructs Monte Carlo simulated data set using the
Bootstrap algorithm.                                                                                   

Usage:

>>> bootstrap(x)

where x is either an array or a list of arrays. If it is a
list, the code returns the corresponding list of bootstrapped 
arrays assuming that the same position in these arrays map the 
same "physical" object.

Rodrigo Nemmen, http://goo.gl/8S1Oo
    """
    if type(v)==list:
        vboot=[]    # list of boostrapped arrays
        n=v[0].size
        iran=scipy.random.randint(0,n,n)    # Array of random indexes
        for x in v:
            vboot.append(x[iran])
    else:    # if v is an array, not a list of arrays
        n=v.size
        iran=scipy.random.randint(0,n,n)    # Array of random indexes
        vboot=v[iran]
    
    return vboot
    
def bcesboot(y1,y1err,y2,y2err,cerr,nsim=10000):
    """
Does the BCES with bootstrapping.    

Usage:

>>> a,b,aerr,berr,covab=bcesboot(x,xerr,y,yerr,cov,nsim)

:param x,y: data
:param xerr,yerr: measurement errors affecting x and y
:param cov: covariance between the measurement errors (all are arrays)
:param nsim: number of Monte Carlo simulations (bootstraps)

:returns: a,b -- best-fit parameters a,b of the linear regression 
:returns: aerr,berr -- the standard deviations in a,b
:returns: covab -- the covariance between a and b (e.g. for plotting confidence bands)

.. note:: this method is definitely not nearly as fast as bces_regress.f. Needs to be optimized. Maybe adapt the fortran routine using f2python?

v1 Mar 2012: ported from bces_regress.f. Added covariance output.
Rodrigo Nemmen, http://goo.gl/8S1Oo
    """
    import fish
    
    # Progress bar initialization
    peixe = fish.ProgressFish(total=nsim)
    print("Bootstrapping progress:")
    
    """
    My convention for storing the results of the bces code below as 
    matrixes for processing later are as follow:
    
          simulation\method  y|x x|y bisector orthogonal
              sim0           ...
    Am =      sim1           ...
              sim2           ...
              sim3           ...
    """
    for i in range(nsim):
        [y1sim,y1errsim,y2sim,y2errsim,cerrsim]=bootstrap([y1,y1err,y2,y2err,cerr])
        
        asim,bsim,errasim,errbsim,covabsim,scat=bces(y1sim,y1errsim,y2sim,y2errsim,cerrsim)    
        
        if i==0:
            # Initialize the matrixes
            am,bm=asim.copy(),bsim.copy()
        else: 
            am=np.vstack((am,asim))
            bm=np.vstack((bm,bsim))
                
        # Progress bar
        peixe.animate(amount=i)
    
    # Bootstrapping results
    a=np.array([ am[:,0].mean(),am[:,1].mean(),am[:,2].mean(),am[:,3].mean() ])
    b=np.array([ bm[:,0].mean(),bm[:,1].mean(),bm[:,2].mean(),bm[:,3].mean() ])

    # Error from unbiased sample variances
    erra,errb,covab=np.zeros(4),np.zeros(4),np.zeros(4)
    for i in range(4):
        erra[i]=np.sqrt( 1./(nsim-1) * ( np.sum(am[:,i]**2)-nsim*(am[:,i].mean())**2 ))
        errb[i]=np.sqrt( 1./(nsim-1) * ( np.sum(bm[:,i]**2)-nsim*(bm[:,i].mean())**2 ))
        covab[i]=1./(nsim-1) * ( np.sum(am[:,i]*bm[:,i])-nsim*am[:,i].mean()*bm[:,i].mean() )
    
    return a,b,erra,errb,covab
    
    
# Methods which make use of parallelization
# ===========================================


def ab(x):
    """
This method is the big bottleneck of the parallel BCES code. That's the 
reason why I put these calculations in a separate method, in order to 
distribute this among the cores. In the original BCES method, this is 
inside the main routine.
    
Argument:
[y1,y1err,y2,y2err,cerr,nsim]
where nsim is the number of bootstrapping trials sent to each core.

:returns: am,bm : the matrixes with slope and intercept where each line corresponds to a bootrap trial and each column maps a different BCES method (ort, y|x etc).

Be very careful and do not use lambda functions when calling this 
method and passing it to multiprocessing or ipython.parallel!
I spent >2 hours figuring out why the code was not working until I
realized the reason was the use of lambda functions.
    """
    y1,y1err,y2,y2err,cerr,nsim=x[0],x[1],x[2],x[3],x[4],x[5]
    
    for i in range(int(nsim)):
        #[y1sim,y1errsim,y2sim,y2errsim,cerrsim]=bootstrap([y1,y1err,y2,y2err,cerr])
        #asim,bsim,errasim,errbsim,covabsim,scatsim=bces(y1sim,y1errsim,y2sim,y2errsim,cerrsim)
        
        ### I've moved the resampling here since bootstrap() makes unnescessary copies of the arrays
        n=y1.size
        ind=scipy.random.randint(0,n,n) # Array of n random indexes from 0 to n
        ## Draw new indices if number of unique indices is fewer than n_unique
        #n_unique = min(3,n-1)
        #while len(np.unique(ind)) <= n_unique:
            #ind=scipy.random.randint(0,n,n)
        asim,bsim,errasim,errbsim,covabsim,scatsim=bces(y1[ind],y1err[ind],y2[ind],y2err[ind],cerr[ind],method='all')
        
        i#f asim[3] > 8:
            #print(asim[3], np.sort(np.unique(ind)))
    
        if i==0:
            # Initialize the matrixes
            am,bm,scatm=asim.copy(),bsim.copy(), scatsim.copy()
        else: 
            am=np.vstack((am,asim))
            bm=np.vstack((bm,bsim))
            scatm = np.vstack((scatm, scatsim))
        
    return am, bm, scatm


def bcesp(y1, y1err, y2, y2err, cerr, nsim=10000, take_mean=False,
          std_error=False, alphas=[0.32, 0.05],
          plot_dist=False, BCa=False):
    """
Parallel implementation of the BCES with bootstrapping.
Divide the bootstraps equally among the threads (cores) of
the machine. It will automatically detect the number of
cores available.

Usage:

>>> a,b,aerr,berr,covab=bcesp(x,xerr,y,yerr,cov,nsim)

Args:
    y1,y2: x,y data
    y1err,y2err: measurement errors affecting x and y
    cerr: covariance between the measurement errors (all are arrays)
    nsim: number of Monte Carlo simulations (bootstraps)
    take_mean: take the mean of the best-fitting parameters from the bootstrap
               resamples (NOT recommended), else take the best-fitting parameters from the full sample
    std_error: use the standard error as the uncertainty in the parameters (assumes normality),
               otherwise define a list of alphas
    alphas: (float or iterable) estimate the confidence interval of the parameters using percentiles.
           The uncertainties correspond to the confidence interval (alpha/2, 1-alpha/2)
           (e.g. alpha=0.05 gives the 95% confidence interval [2.5%,97.5%]),
    plot_dist: plot the distribution of the best-fit parameters from the bootstrap resamples
    BCa: use the bias-corrected accelerated method of Efron 1986 to estimate confidence intervals

:returns: a,b - best-fit parameters a,b of the linear regression 
:returns: aerr,berr - the standard deviations in a,b
:returns: covab - the covariance between a and b (e.g. for plotting confidence bands)

.. seealso:: Check out ~/work/projects/playground/parallel python/bcesp.py for the original, testing, code. I deleted some line from there to make the "production" version.

* v1 Mar 2012: serial version ported from bces_regress.f. Added covariance output.
* v2 May 3rd 2012: parallel version ported from nemmen.bcesboot.

.. codeauthor: Rodrigo Nemmen, http://goo.gl/8S1Oo
    """    
    import time    # for benchmarking
    import multiprocessing
       
    print("BCES,", nsim,"trials... ")
    tic=time.time()
    
    # Find out number of cores available
    ncores=multiprocessing.cpu_count()
    # We will divide the processing into how many parts?
    n=2*ncores
    
    """
    Must create lists that will be distributed among the many
    cores with structure 
    core1 <- [y1,y1err,y2,y2err,cerr,nsim/n]
    core2 <- [y1,y1err,y2,y2err,cerr,nsim/n]
    etc...
    """
    pargs=[]    # this is a list of lists!
    for i in range(n):
        pargs.append([y1,y1err,y2,y2err,cerr,nsim/n])
    
    # Initializes the parallel engine
    pool = multiprocessing.Pool(processes=ncores)    # multiprocessing package
            
    """
    Each core processes ab(input)
        return matrixes Am,Bm with the results of nsim/n
        presult[i][0] = Am with nsim/n lines
        presult[i][1] = Bm with nsim/n lines
    """
    presult=pool.map(ab, pargs)    # multiprocessing
    pool.close()    # close the parallel engine
    
    # vstack the matrixes processed from all cores
    for i, m in enumerate(presult):
        if i==0:
            # Initialize the matrixes
            am,bm,scatm=m[0].copy(),m[1].copy(),m[2].copy()
        else: 
            am=np.vstack((am,m[0]))
            bm=np.vstack((bm,m[1]))
            scatm=np.vstack((scatm,m[2]))
        
    ## Convert alphas to a list if not already an iterable
    if not hasattr(alphas, "__iter__"):
        alphas = [alphas]
        
    if std_error:
        erra,errb,covab,errscat=np.zeros((2,4)),np.zeros((2,4)),np.zeros(4),np.zeros((2,4))
    else:
        erra,errb,covab,errscat=np.zeros((2*len(alphas),4)),np.zeros((2*len(alphas),4)),np.zeros(4),np.zeros((2*len(alphas),4))
    if take_mean:
        # Computes the bootstrapping results on the stacked matrixes
        a=np.array([ am[:,0].mean(),am[:,1].mean(),am[:,2].mean(),am[:,3].mean() ])
        b=np.array([ bm[:,0].mean(),bm[:,1].mean(),bm[:,2].mean(),bm[:,3].mean() ])
        scat=np.array([ scatm[:,0].mean(),scatm[:,1].mean(),scatm[:,2].mean(),scatm[:,3].mean() ])
    
        # Error from unbiased sample variances
        for i in range(4):
            erra[:,i]=np.sqrt( 1./(nsim-1) * ( np.sum(am[:,i]**2)-nsim*(am[:,i].mean())**2 ))
            errb[:,i]=np.sqrt( 1./(nsim-1) * ( np.sum(bm[:,i]**2)-nsim*(bm[:,i].mean())**2 ))
            covab[i]=1./(nsim-1) * ( np.sum(am[:,i]*bm[:,i])-nsim*am[:,i].mean()*bm[:,i].mean() )
            errscat[:,i]=np.sqrt( 1./(nsim-1) * ( np.sum(scatm[:,i]**2)-nsim*(scatm[:,i].mean())**2 ))
    else:
        ## Get best-fit using ALL of the data
        a, b, err_a, err_b, covab, scat = bces(y1,y1err,y2,y2err,cerr,method='all')
        ## Estimate the variance on the best-fit parameters from the bootstrap results
        for i in range(4):
            if std_error: 
                ## standard error of the true population parameter is the standard
                ## deviation of the bootstrap distribution
                erra[:,i] = np.sqrt(1.0/nsim * np.sum((am[:,i] - np.mean(am[:,i]))**2))
                errb[:,i] = np.sqrt(1.0/nsim * np.sum((bm[:,i] - np.mean(bm[:,i]))**2))
                errscat[:,i] = np.sqrt(1.0/nsim * np.sum((scatm[:,i] - np.mean(scatm[:,i]))**2))
                covab[i] = np.sqrt(1.0/nsim * np.sum((am[:,i] - np.mean(am[:,i])) * (bm[:,i] - np.mean(bm[:,i]))))
                ## this is the method in Hogg 2010 which calculates the average
                ## squared deviation of the bootstrap resamples from the best-fit 
                ## rather than from the mean of the bootstrap resamples as above
                #erra[:,i] = np.sqrt(1.0/nsim * np.sum((am[:,i] - a[i])**2))
                #errb[:,i] = np.sqrt(1.0/nsim * np.sum((bm[:,i] - b[i])**2))
                #errscat[:,i] = np.sqrt(1.0/nsim * np.sum((scatm[:,i] - scat[i])**2))
                #covab[i] = np.sqrt(1.0/nsim * np.sum((am[:,i] - a[i]) * (bm[:,i] - b[i])))
            else: ## (1-alpha) confidence intervals
                ### Basic or 'empirical' bootstrap (see Davison and Hinkley 1997, equ. 5.6)
                for j, alpha in enumerate(alphas):
                    erra[2*j,i] = a[i] - (2.0*a[i] - np.percentile(am[:,i], (1.0-alpha/2.0)*100.0))
                    erra[2*j+1,i] = (2.0*a[i] - np.percentile(am[:,i], (alpha/2.0)*100.0)) - a[i]
                    errb[2*j,i] = b[i] - (2.0*b[i] - np.percentile(bm[:,i], (1.0-alpha/2.0)*100.0))
                    errb[2*j+1,i] = (2.0*b[i] - np.percentile(bm[:,i], (alpha/2.0)*100.0)) - b[i]
                    errscat[2*j,i] = scat[i] - (2.0*scat[i] - np.percentile(scatm[:,i], (1.0-alpha/2.0)*100.0))
                    errscat[2*j+1,i] = (2.0*scat[i] - np.percentile(scatm[:,i], (alpha/2.0)*100.0)) - scat[i]
                ### Percentile bootstrap e.g. Efron
#                for j, alpha in enumerate(alphas):
#                    erra[2*j,i] = a[i] - np.percentile(am[:,i], (alpha/2.0)*100.0)
#                    erra[2*j+1,i] = np.percentile(am[:,i], (1.0-alpha/2.0)*100.0) - a[i]
#                    errb[2*j,i] = b[i] - np.percentile(bm[:,i], (alpha/2.0)*100.0)
#                    errb[2*j+1,i] = np.percentile(bm[:,i], (1.0-alpha/2.0)*100.0) - b[i]
#                    errscat[2*j,i] = scat[i] - np.percentile(scatm[:,i], (alpha/2.0)*100.0)
#                    errscat[2*j+1,i] = np.percentile(scatm[:,i], (1.0-alpha/2.0)*100.0) - scat[i]
                ## Usual formula for the covariance:
                covab[i] = np.sqrt(1.0/nsim * np.sum((am[:,i] - a[i]) * (bm[:,i] - b[i])))
               
    if BCa:
        # Use the bias-corrected accelerated bootstrap method of Efron 1984
        import bootstrap
        import functools
        # Get the function to bootstrap, in this case I define a
        # partial function with the desired keywords
        func = functools.partial(bces, method="orthog")
        result = bootstrap.ci((y1, y1err, y2, y2err, cerr), func,
                              alpha=alphas[0], n_samples=nsim,
                              method='bca', multi=True)
    if plot_dist:
        # plot the bootstrap distributions for alphas[0]
        import matplotlib.pyplot as plt
        fig, (ax_a, ax_b, ax_scat) = plt.subplots(3)
        ax_a.set_xlabel(r"$\beta$")
        ax_b.set_xlabel(r"log$_{10}$C")
        ax_scat.set_xlabel(r"$\sigma$")
        ind = [3]
        for i in ind:
            ax_a.hist(am[:, i], bins='auto', histtype='step')
            ax_a.axvline(a[i])
            ax_a.axvline(a[i]-erra[0, i], ls='--')
            ax_a.axvline(a[i]+erra[1, i], ls='--')
            ax_b.hist(bm[:, i], bins='auto', histtype='step')
            ax_b.axvline(b[i])
            ax_b.axvline(b[i]-errb[0, i], ls='--')
            ax_b.axvline(b[i]+errb[1, i], ls='--')
            ax_scat.hist(scatm[:, i], bins='auto', histtype='step')
            ax_scat.axvline(scat[i])
            ax_scat.axvline(scat[i]-errscat[0, i], ls='--')
            ax_scat.axvline(scat[i]+errscat[1, i], ls='--')
            if BCa:
                ax_a.axvline(result[0][0], c='g', ls='--')
                ax_a.axvline(result[1][0], c='g', ls='--')
                ax_b.axvline(result[0][1], c='g', ls='--')
                ax_b.axvline(result[1][1], c='g', ls='--')
                ax_scat.axvline(result[0][5], c='g', ls='--')
                ax_scat.axvline(result[1][5], c='g', ls='--')
        
    print("Took %f s" % (time.time() - tic))

    return a, b, erra, errb, covab, scat, errscat