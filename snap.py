# -*- coding: utf-8 -*-
"""Utilties for loading and analysing Gadget and Arepo snapshots.

This module includes the 'snap' class, which represents a Gadget/Arepo
snapshot and any associated group catalogue. It contains methods for
loading snapshot data (e.g. particle positions, mass etc.), for
calculating derived quantities (e.g. gas temperature) and for various
analyses (e.g. radial profiles, maps, phase space diagrams).

The original implementation was written by Ewald Puchwein with
subsequent minor edits and commenting by Nick Henden.
"""
from __future__ import print_function

import pylab as pyl
import ctypes as ct
import os

import readsubf
import readsnap as rs
import sim_python.cosmo as cosmo
from sim_python.histogram_edges import histogram_edges

# Load constants in SI units.
from scipy.constants import parsec as pc
from scipy.constants import m_p as mproton
from scipy.constants import m_e as melectron
from scipy.constants import k as kboltzmann
from scipy.constants import G as Gnewton
from scipy.constants import c as clight
from scipy.constants import eV
from scipy.constants import e as electron_charge
from scipy.constants import epsilon_0
keV = 1000.0*eV
kpc = 1000.0*pc
Mpc = 1.0e6*pc
msun = 1.9884430e30
rhocritoverh2 = 3.0*(100.0*1000.0/Mpc)**2/(8.0*pyl.pi*Gnewton)
# Calculate Thomson cross-section.
sigma_thomson = (8.0 * pyl.pi / 3.0 *
                 (electron_charge**2 /
                  (4.0 * pyl.pi * epsilon_0 * melectron * clight**2))**2)
# Hydrogen mass fraction.
fhydrogen = 0.76  # 0.76 is assumed in Gadget and Arepo by default.
mmean_ionized = ((1.0 + (1.0 - fhydrogen) / fhydrogen) /
                 (2.0 + 3.0 * (1.0 - fhydrogen) / (4.0 * fhydrogen)))

# If Python 2, replace input() with raw_input() so that input() then
# works the same for both Python 2 and 3
try:
    input = raw_input
except NameError:
    pass


# -------------------------------------------------------------
# Utility functions
def rot_matrix(a_z, a_x, a_z2):
    """
    Return a 3x3 rotation matrix.
    The return matrix rotates by a_z*pi radians around z axis,
    then a_x*pi around x axis, then a_z2*pi around z axis again.
    All rotations are anticlockwise. Angles are in fraction of pi radians.
    Usage: e.g.
        rotmat = snap.rot_matrix(a_z*pyl.pi, a_x*pyl.pi, a_z2*pyl.pi)
        pos = pyl.dot(pos,rotmat.transpose())
    """
    return pyl.array(
        [[(pyl.cos(a_z2) * pyl.cos(a_z) -
           pyl.cos(a_x) * pyl.sin(a_z) * pyl.sin(a_z2)),
          (pyl.cos(a_z2) * pyl.sin(a_z) +
           pyl.cos(a_x) * pyl.cos(a_z) * pyl.sin(a_z2)),
          pyl.sin(a_z2) * pyl.sin(a_x)],
         [(-pyl.sin(a_z2) * pyl.cos(a_z) -
           pyl.cos(a_x) * pyl.sin(a_z) * pyl.cos(a_z2)),
          (-pyl.sin(a_z2) * pyl.sin(a_z) +
           pyl.cos(a_x) * pyl.cos(a_z) * pyl.cos(a_z2)),
          pyl.sin(a_x)*pyl.cos(a_z2)],
         [pyl.sin(a_x)*pyl.sin(a_z), -pyl.sin(a_x)*pyl.cos(a_z),
          pyl.cos(a_x)]])


def R_2vect(vector_orig, vector_fin):
    """Calculate rotation matrix to rotate one vector to another.

    For the rotation of one vector to another there are an infinite
    series of rotation matrices possible.  Due to axial symmetry, the
    rotation axis can be any vector lying in the symmetry plane between
    the two vectors.  Hence the axis-angle convention will be used to
    construct the matrix with the rotation axis defined as the cross
    product of the two vectors. The rotation angle is the arccosine
    of the dot product of the two unit vectors.
    Given a unit vector parallel to the rotation axis, w = [x, y, z]
    and the rotation angle a,  the rotation matrix R is:
     1 + (1-cos(a))*(x*x-1)  -z*sin(a)+(1-cos(a))*x*y  y*sin(a)+(1-cos(a))*x*z
     z*sin(a)+(1-cos(a))*x*y  1 + (1-cos(a))*(y*y-1)  -x*sin(a)+(1-cos(a))*y*z
    -y*sin(a)+(1-cos(a))*x*z  x*sin(a)+(1-cos(a))*y*z  1 + (1-cos(a))*(z*z-1)
    Arguments:
        vector_orig: The unrotated vector defined in the reference frame.
        vector_fin:  The rotated vector defined in the reference frame.

    Returns a 3x3 rotation matrix as a numpy array.
    """
    import numpy as np
    from math import acos, cos, sin
    from numpy.linalg import norm

    # Convert vectors to numpy arrays
    vector_orig = np.array(vector_orig)
    vector_fin = np.array(vector_fin)

    # Convert the vectors to unit vectors.
    vector_orig = vector_orig / norm(vector_orig)
    vector_fin = vector_fin / norm(vector_fin)

    # The rotation axis (normalised).
    axis = np.cross(vector_orig, vector_fin)
    axis_len = norm(axis)
    if axis_len != 0.0:
        axis = axis / axis_len

    # Alias the axis coordinates.
    x = axis[0]
    y = axis[1]
    z = axis[2]

    # The rotation angle.
    angle = acos(np.dot(vector_orig, vector_fin))

    # Trig functions
    ca = cos(angle)
    sa = sin(angle)

    R = np.zeros((3, 3))
    # Calculate the rotation matrix elements.
    R[0, 0] = 1.0 + (1.0 - ca)*(x**2 - 1.0)
    R[0, 1] = -z*sa + (1.0 - ca)*x*y
    R[0, 2] = y*sa + (1.0 - ca)*x*z
    R[1, 0] = z*sa+(1.0 - ca)*x*y
    R[1, 1] = 1.0 + (1.0 - ca)*(y**2 - 1.0)
    R[1, 2] = -x*sa+(1.0 - ca)*y*z
    R[2, 0] = -y*sa+(1.0 - ca)*x*z
    R[2, 1] = x*sa+(1.0 - ca)*y*z
    R[2, 2] = 1.0 + (1.0 - ca)*(z**2 - 1.0)

    return R


def box_wrap_diff(pos, boxsize):
    """Wrap positions to the range [-0.5*boxsize, 0.5*boxsize]."""
    ind = pyl.where(pos < -0.5*boxsize)
    pos[ind] += boxsize

    ind = pyl.where(pos > 0.5*boxsize)
    pos[ind] -= boxsize

    assert (pos >= -0.5*boxsize).all()
    assert (pos <= 0.5*boxsize).all()


# -------------------------------------------------------------
# The snapshot class and its methods.
class snapshot:
    def __init__(self, basedir, snapnum, mpc_units=False,
                 snap_basename="snap", header_only=False, read_params=True,
                 ic_file="none",
                 partload_hubble=0, partload_omega_m=0, partload_redshift=0,
                 group_veldisp=False, masstab=False,
                 long_ids=False, swap=False, extra_sub_props=False,
                 cosmic_ray_species=0, old_bfield=False):
        """Constructor for the snapshot class.

        Initialise the snapshot class by loading the snapshot header and
        (optionally) the associated parameter file and halo catalogue.
        Can also load an initial conditions file using the 'ic_file' argument.
        Examples:
            # Load snapshot 0 located in the current directory.
            sim = snap.snapshot("./", 0)
            print(sim.header.hubble)  # hubble parameter
            print(sim.cat.group_mass)  # total bound masses of FoF groups
            # Load masses of all particle types.
            sim.load_mass()
            # Calculate the average mass of type 1 particles.
            print(sim.mass[sim.species_start[1]:sim.species_end[1]].mean())
        Arguments:
            basedir (string): The directory path containing the snapshot file.
            snapnum (integer): The snapshot number.
            mpc_units (bool): True if length units are Mpc not kpc.
            snap_basename (string): The base of the snapshot file names.
            header_only (bool): Load only the snapshot header and not
                the halo catalogue.
            read_params (bool): Attempt to read the parameter file.
            ic_file (string): Path of an initial conditions file to load.
                Can also be any other snapshot-like file. 'basedir' is
                ignored so give the full path. 'snapnum' is also ignored.
            partload_hubble: Set the hubble parameter to this value.
                Overrides the value in the snapshot header.
            partload_omega_m: Set the matter parameter to this value.
                Overrides the value in the snapshot header.
            partload_redshift: Set the redshift to this value.
                Overrides the value in the snapshot header.
            group_veldisp (bool): True if the halo catalogue (binary format
                only) contains the group velocity dispersion.
            masstab (bool): True if the halo catalogue (binary format
                only) contains the subhalo masses by particle type.
            long_ids (bool): True if the IDs in the halo catalogue (binary
                format only) are 64-bit integers rather than 32-bit.
            swap (bool): If True, swap the bytes of the array elements in
                the halo catalogue (binary format only).
            extra_sub_props (bool): If True, load extra subhalo properties
                from the halo catalogue (see readsubf.py for details).
            cosmic_ray_species (integer): Number of cosmic ray species.
                Only required for binary snapshots.
            old_bfield (bool): Convert magnetic field units to Gauss-cgs
                if old MHD code was used.
        """
        if ic_file == "none":
            self.basedir = os.path.normpath(basedir)
            self.snapnum = snapnum
            self.snapname = (self.basedir + "/" + snap_basename + "_" +
                             str(self.snapnum).zfill(3))
        else:
            self.basedir = "none"
            self.snapnum = -1
            self.snapname = ic_file

        self.mpc_units = mpc_units
        self.group_veldisp = group_veldisp
        self.masstab = masstab
        self.long_ids = long_ids
        self.swap = swap
        self.info = "none"

        self.first_snapfile_name = self.snapname
        self.multiple_file = False

        if ((not os.path.exists(self.snapname)) and
                (not os.path.exists(self.snapname+".hdf5"))):
            if ic_file == "none":
                self.snapname = self.basedir + "/" + "snapdir_" + \
                    str(self.snapnum).zfill(3) + "/" + \
                    snap_basename + "_" + str(self.snapnum).zfill(3)
                self.first_snapfile_name = self.snapname + ".0"
                self.multiple_file = True
            else:
                self.snapname = ic_file
                self.first_snapfile_name = ic_file + ".0"

        self.header = rs.snapshot_header(
            self.first_snapfile_name, cosmic_ray_species=cosmic_ray_species)

        self.num_cr_species = cosmic_ray_species
        self.old_bfield = old_bfield

        self.cm = cosmo.cosmology(self.header.hubble, self.header.omega_m)

        self.mgadget = 10.0**10 * msun / self.header.hubble
        self.lgadget = kpc / self.header.hubble
        self.rhogadget = self.mgadget / self.lgadget**3

        # comoving critical density at snapshot redshift in Gadget units.
        self.rho_crit = (2.77536627e-8 *
                         pow(self.cm.hubble_z(self.header.redshift), 2) /
                         (pow(self.header.hubble, 2) *
                          (1+self.header.redshift)**3))
        # comoving mean matter density in Gadget units.
        self.rho_mean = 2.77536627e-8 * self.header.omega_m

        # physical critical density in kg / m^3.
        self.rho_crit_SI = self.cm.rho_crit_SI(self.header.redshift)

        # Hubble parameter at redshift of snapshot.
        self.hubble_z = self.cm.hubble_z(self.header.redshift)
        # multiply by distance in Gadget length units
        # to get physical velocity im km/s.
        self.hubble_z_gadget = self.hubble_z * 100.0 * \
            self.header.time / (1000.0 * self.header.hubble)

        if ic_file == "none" and read_params:
            try:
                lsdir = os.listdir(self.basedir)
                par_file_num = 0
                for curfile in lsdir:
                    if curfile.find("usedvalues") >= 0:
                        self.par_file_name = curfile
                        par_file_num += 1
                if par_file_num != 1:
                    raise IOError("no or multiple parameter files found.")

                par_file = open(self.basedir + "/" + self.par_file_name, "r")

                self.pars = dict()
                for line in par_file:
                    par_name, par_val = line.split()
                    self.pars[par_name] = par_val

                par_file.close()

                if (abs(pyl.float64(self.pars["Omega0"]) -
                        self.header.omega_m) > 1.0e-6):
                    raise ValueError("Omega0 in parameter file does "
                                     "not match Omega0 in snapshot.")
                if (abs(pyl.float64(self.pars["OmegaLambda"]) -
                        self.header.omega_l) > 1.0e-6):
                    raise ValueError("OmegaLambda in parameter file does "
                                     "not match OmegaLambda in snapshot.")
                if (abs(pyl.float64(self.pars["HubbleParam"]) -
                        self.header.hubble) > 1.0e-6):
                    raise ValueError("HubbleParam in parameter file does "
                                     "not match HubbleParam in snapshot.")
                if (abs((pyl.float64(self.pars["BoxSize"]) -
                         self.header.boxsize) / self.header.boxsize) > 1.0e-6):
                    raise ValueError("BoxSize in parameter file ({:.0f}) does "
                                     "not match BoxSize in snapshot ({:.0f})."
                                     "".format(pyl.float64(
                                               self.pars["BoxSize"]),
                                               self.header.boxsize))

                self.omega_b = pyl.float64(self.pars["OmegaBaryon"])
                self.rho_mean_baryons = (self.rho_mean /
                                         self.header.omega_m * self.omega_b)

                if (abs(pyl.float64(self.pars["UnitLength_in_cm"]) -
                        kpc * 100.0) < 1e20):
                    mpc_units_par = False
                elif (abs(pyl.float64(self.pars["UnitLength_in_cm"]) -
                          Mpc * 100.0) < 1e23):
                    mpc_units_par = True
                else:
                    raise ValueError("UnitLength_in_cm = " +
                                     str(self.pars["UnitLength_in_cm"]) +
                                     " not understood")
                if mpc_units_par != mpc_units:
                    print("\033[31m", "WARNING: mpc_units given as", mpc_units,
                          "but got mpc_units of", mpc_units_par,
                          "from parameters-usedvalues\033[0m")
                    print("\033[31m", "Using mpc_units", mpc_units_par,
                          "\033[0m")
                    mpc_units = mpc_units_par
                    self.mpc_units = mpc_units

            except Exception as e:
                print(e.message)
                print("parameter file could not be read correctly.")

        # values used in the simulation code
        self.unitmass_in_g = 10.0**10 * msun * 1000.0
        if mpc_units:
            self.unitlength_in_cm = Mpc * 100.0
        else:
            self.unitlength_in_cm = kpc * 100.0
        self.unitvel_in_cm_per_s = 1.0e5
        self.unittime_in_s = self.unitlength_in_cm / self.unitvel_in_cm_per_s

        self.box_sidelength = self.header.boxsize
        if mpc_units:
            self.box_sidelength *= 1000.0

        self.species_start = pyl.zeros(7, dtype=pyl.uint64)
        self.species_end = pyl.zeros(6, dtype=pyl.uint64)
        for i in range(7):
            if i == 0:
                self.species_start[i] = 0
            else:
                self.species_start[i] = (
                    self.header.nall.cumsum(dtype=pyl.int64))[i-1]

            if i <= 5:
                # index of last particle of species plus one
                self.species_end[i] = (
                        self.header.nall.cumsum(dtype=pyl.int64))[i]

        if partload_hubble != 0:
            self.header.hubble = partload_hubble
            self.header.omega_m = partload_omega_m
            self.header.redshift = partload_redshift
            self.header.time = 1.0/(partload_redshift+1.0)

        if not header_only:
            if ic_file == "none":
                found = True
                self.cat = readsubf.subfind_catalog(
                        basedir, snapnum, group_veldisp=group_veldisp,
                        masstab=masstab, long_ids=long_ids, swap=swap,
                        extra_sub_props=extra_sub_props)
                if self.cat.error == 1:
                    print("no SUBFIND catalogue found,"
                          "or it could not be read correctly.")
                    found = False
                if found:
                    if self.mpc_units:
                        self.cat.group_pos *= 1000.0
                        self.cat.group_cm *= 1000.0
                        self.cat.sub_pos *= 1000.0
                        self.cat.sub_cm *= 1000.0
                        self.cat.sub_halfmassrad *= 1000.0
                        self.cat.sub_halfmassradtype *= 1000.0
                        self.cat.sub_vmaxrad *= 1000.0
                        if extra_sub_props:
                            self.cat.sub_SPrad *= 1000.0
                        self.cat.group_r_mean200 *= 1000.0
                        self.cat.group_r_crit200 *= 1000.0
                        self.cat.group_r_tophat200 *= 1000.0
                        # old gadget runs for e.g don't have R200
                        if hasattr(self.cat, "group_r_crit500"):
                            # readsubf does not load r_mean500 etc
                            self.cat.group_r_crit500 *= 1000.0
                            ind = pyl.where(self.cat.group_m_crit500 > 0.0)[0]
                            self.cat.group_T_crit500 = pyl.zeros(
                                self.cat.ngroups)
                            # T500 as in Voit review 2005
                            self.cat.group_T_crit500[ind] = ((
                                Gnewton*self.mgadget*mmean_ionized*mproton /
                                (self.header.time*self.lgadget)) *
                                self.cat.group_m_crit500[ind] /
                                (2.0 * self.cat.group_r_crit500[ind]) / keV)

                    ind = pyl.where(self.cat.group_m_crit200 > 0.0)[0]
                    self.cat.group_T_crit200 = pyl.zeros(self.cat.ngroups)
                    # T500 as in Voit review 2005
                    self.cat.group_T_crit200[ind] = ((
                            Gnewton * self.mgadget * mmean_ionized * mproton /
                            (self.header.time*self.lgadget)) *
                            self.cat.group_m_crit200[ind] / (
                            2.0 * self.cat.group_r_crit200[ind]) / keV)
                    if read_params:
                        try:
                            # P200 in keV / cm^3
                            self.cat.group_P_crit200 = pyl.zeros(
                                                            self.cat.ngroups)
                            self.cat.group_P_crit200[ind] = (
                                    self.cat.group_T_crit200[ind] *
                                    (200.0 * self.rho_crit_SI * self.omega_b /
                                     (mmean_ionized*mproton)) / 1.0e6)
                        except AttributeError:
                            print("Could not compute P_crit200! Parameter "
                                  "file for reading omega_b missing?")

    # -------------------------------------------------------------
    # LOAD DATA BLOCKS

    # load positions and convert to ckpc/h
    def load_pos(self):
        self.pos = rs.read_block(
            self.snapname, "POS ", cosmic_ray_species=self.num_cr_species)
        if self.mpc_units:
            self.pos *= 1000.0

    # create a dictionary of position arrays,
    # one for each given particle type.
    def load_posarr(self, parttypes=range(6)):
        self.posarr = {}
        for parttype in parttypes:
            pos = rs.read_block(
                self.snapname, "POS ", parttype=parttype,
                cosmic_ray_species=self.num_cr_species)
            if self.mpc_units:
                pos *= 1000.0
            self.posarr['type'+str(parttype)] = pos

    # load gas positions
    def load_gaspos(self):
        self.gaspos = rs.read_block(
            self.snapname, "POS ", parttype=0,
            cosmic_ray_species=self.num_cr_species)
        if self.mpc_units:
            self.gaspos *= 1000.0

    # load star positions
    def load_starpos(self):
        self.starpos = rs.read_block(
            self.snapname, "POS ", parttype=4,
            cosmic_ray_species=self.num_cr_species)
        if self.mpc_units:
            self.starpos *= 1000.0

    # load velocities
    def load_vel(self, physical_velocities=True):
        self.vel = rs.read_block(
            self.snapname, "VEL ", physical_velocities=physical_velocities,
            cosmic_ray_species=self.num_cr_species)

    # load gas velocities
    def load_gasvel(self, physical_velocities=True):
        self.gasvel = rs.read_block(self.snapname, "VEL ", parttype=0,
                                    physical_velocities=physical_velocities,
                                    cosmic_ray_species=self.num_cr_species)

    # load star velocities
    def load_starvel(self, physical_velocities=True):
        self.starvel = rs.read_block(self.snapname, "VEL ", parttype=4,
                                     physical_velocities=physical_velocities,
                                     cosmic_ray_species=self.num_cr_species)

    # load masses
    def load_mass(self):
        self.mass = rs.read_block(self.snapname, "MASS",
                                  cosmic_ray_species=self.num_cr_species)

    # load gas masses
    def load_gasmass(self):
        self.gasmass = rs.read_block(self.snapname, "MASS", parttype=0,
                                     cosmic_ray_species=self.num_cr_species)

    # load stellar masses
    def load_starmass(self):
        self.starmass = rs.read_block(self.snapname, "MASS", parttype=4,
                                      cosmic_ray_species=self.num_cr_species)

    # load IDs
    def load_ids(self):
        self.ids = rs.read_block(self.snapname, "ID  ",
                                 cosmic_ray_species=self.num_cr_species)

    # load gas IDs
    def load_gasids(self):
        self.gasids = rs.read_block(self.snapname, "ID  ", parttype=0,
                                    cosmic_ray_species=self.num_cr_species)

    # load star IDs
    def load_starids(self):
        self.starids = rs.read_block(self.snapname, "ID  ", parttype=4,
                                     cosmic_ray_species=self.num_cr_species)

    # load densities
    def load_rho(self):
        self.rho = rs.read_block(self.snapname, "RHO ",
                                 cosmic_ray_species=self.num_cr_species)
        if self.mpc_units:
            self.rho /= 1.0e9

    # load gas densities
    def load_gasrho(self):
        self.gasrho = rs.read_block(self.snapname, "RHO ", parttype=0,
                                    cosmic_ray_species=self.num_cr_species)
        if self.mpc_units:
            self.gasrho /= 1.0e9

    # load internal energies
    def load_u(self):
        self.u = rs.read_block(self.snapname, "U   ",
                               cosmic_ray_species=self.num_cr_species)

    # load hsml
    def load_hsml(self):
        self.hsml = rs.read_block(self.snapname, "HSML",
                                  cosmic_ray_species=self.num_cr_species)
        if self.mpc_units:
            self.hsml *= 1000.0

    # load cell volumes
    def load_vol(self):
        self.vol = rs.read_block(self.snapname, "VOL ",
                                 cosmic_ray_species=self.num_cr_species)
        if self.mpc_units:
            self.vol *= 1.0e9

    # load ne
    def load_ne(self):
        self.ne = rs.read_block(self.snapname, "NE  ",
                                cosmic_ray_species=self.num_cr_species)

    # load nh
    def load_nh(self):
        self.nh = rs.read_block(self.snapname, "NH  ",
                                cosmic_ray_species=self.num_cr_species)

    # load nhe
    def load_nhe(self):
        self.nhe = rs.read_block(self.snapname, "HeI ",
                                 cosmic_ray_species=self.num_cr_species)

    # load nhep
    def load_nhep(self):
        self.nhep = rs.read_block(self.snapname, "HeII",
                                  cosmic_ray_species=self.num_cr_species)

    # load sfr
    def load_sfr(self):
        self.sfr = rs.read_block(self.snapname, "SFR ",
                                 cosmic_ray_species=self.num_cr_species)

    # load Zgas
    def load_Zgas(self):
        try:
            self.Zgas = rs.read_block(self.snapname, "Z   ", parttype=0,
                                      cosmic_ray_species=self.num_cr_species)
        except (ValueError, KeyError):
            self.Zgas = rs.read_block(self.snapname, "GZ  ", parttype=0,
                                      cosmic_ray_species=self.num_cr_species)

    # load Zstar
    def load_Zstar(self):
        try:
            self.Zstar = rs.read_block(self.snapname, "Z   ", parttype=4,
                                       cosmic_ray_species=self.num_cr_species)
        except (ValueError, KeyError):
            self.Zstar = rs.read_block(self.snapname, "GZ  ", parttype=4,
                                       cosmic_ray_species=self.num_cr_species)

    # load pot
    def load_pot(self):
        self.pot = rs.read_block(self.snapname, "POT ",
                                 cosmic_ray_species=self.num_cr_species)

    # load pot
    def load_gaspot(self):
        self.gaspot = rs.read_block(self.snapname, "POT ", parttype=0,
                                    cosmic_ray_species=self.num_cr_species)

    # load accelerations
    def load_accel(self):
        self.accel = rs.read_block(
            self.snapname, "ACCE", cosmic_ray_species=self.num_cr_species)

    # load accelerations
    def load_mg_accel(self):
        self.mg_accel = rs.read_block(
            self.snapname, "MGAC", cosmic_ray_species=self.num_cr_species)

    # load cooling rate
    def load_coolrate(self):
        self.coolrate = rs.read_block(
            self.snapname, "COOR", cosmic_ray_species=self.num_cr_species)

    # load wind decoupling time
    def load_decouple_time(self):
        self.decouple_time = rs.read_block(
            self.snapname, "DETI", cosmic_ray_species=self.num_cr_species)

    # load BH pos
    def load_bhpos(self):
        self.bhpos = rs.read_block(self.snapname, "POS ", parttype=5,
                                   cosmic_ray_species=self.num_cr_species)
        if self.mpc_units:
            self.bhpos *= 1000.0

    # load BH vel
    def load_bhvel(self, physical_velocities=True):
        self.bhvel = rs.read_block(self.snapname, "VEL ", parttype=5,
                                   physical_velocities=physical_velocities,
                                   cosmic_ray_species=self.num_cr_species)

    # load BH IDs
    def load_bhids(self):
        self.bhids = rs.read_block(self.snapname, "ID  ", parttype=5,
                                   cosmic_ray_species=self.num_cr_species)

    # load real BH mass
    def load_bhmass(self):
        self.bhmass = rs.read_block(
            self.snapname, "BHMA", cosmic_ray_species=self.num_cr_species)

    # load BH accretion rate
    def load_bhmassdot(self):
        self.bhmassdot = rs.read_block(
            self.snapname, "BHMD", cosmic_ray_species=self.num_cr_species)

    # Accreted  BH mass, seems to include (despite its name)
    # both quasar and radio mode.
    def load_bhmass_accreted(self):
        self.bhmass_accreted = rs.read_block(
            self.snapname, "BHMQ", cosmic_ray_species=self.num_cr_species)

    # load real BH mass at last bubble injection
    # (might be broken after BH merger)
    def load_bhmass_ini(self):
        self.bhmass_ini = rs.read_block(
            self.snapname, "BHMI", cosmic_ray_species=self.num_cr_species)

    # load real BH mass at last bubble injection and
    # accreted mass in radio mode since then.
    def load_bhmass_bubbles(self):
        self.bhmass_bubbles = rs.read_block(
            self.snapname, "BHMB", cosmic_ray_species=self.num_cr_species)

    # load BH hsml
    def load_bhhsml(self):
        self.bhhsml = rs.read_block(
            self.snapname, "BHSM", cosmic_ray_species=self.num_cr_species)
        if self.mpc_units:
            self.bhhsml *= 1000.0

    # load BH Pressure
    def load_bhpres(self):
        self.bhpres = rs.read_block(
            self.snapname, "BHPR", cosmic_ray_species=self.num_cr_species)
        if self.mpc_units:
            # Adjust length units since BH_Pressure is inversely proportional
            # to the volume around the BH.
            self.bhpres /= 1.0e9

    # load B-field
    def load_bfld(self):
        self.bfld = rs.read_block(
            self.snapname, "BFLD", cosmic_ray_species=self.num_cr_species)
        bfac = pyl.sqrt(self.unitmass_in_g /
                        self.unitlength_in_cm) / self.unittime_in_s
        if self.old_bfield:
            print(">>>     doing bfield conversion for old MHD code     <<<")
            # conversion from Heavyside-Lorentz to Gauss-cgs;
            # is now done in Arepo for code versions > revision 28177
            bfac *= pyl.sqrt(4.0*pyl.pi)
        self.bfld *= bfac  # result should be in Gauss

    # load HighResGasMass
    def load_highresgasmass(self):
        self.hrgm = rs.read_block(
            self.snapname, "HRGM", cosmic_ray_species=self.num_cr_species)

    # load stellar ages
    def load_stellar_ages(self):
        try:
            self.age = rs.read_block(
                self.snapname, "AGE ", cosmic_ray_species=self.num_cr_species)
        except (ValueError, KeyError):
            self.age = rs.read_block(
                self.snapname, "GAGE", cosmic_ray_species=self.num_cr_species)

    def load_star_initial_mass(self):
        self.star_inimass = rs.read_block(
                self.snapname, "GIM ", cosmic_ray_species=self.num_cr_species)

    # get the type of all particles.
    def get_parttype(self):
        self.parttype = pyl.zeros(
            self.header.nall.sum(), dtype=pyl.int8)  # initialize to zeros
        partskip = 0
        for i in range(6):
            self.parttype[partskip:partskip+self.header.nall[i]] = i
            partskip += self.header.nall[i]

    # get temperature in keV and (optionally) Kelvin.
    def get_temp(self, have_ne=True, del_u=True, del_ne=True,
                 get_also_in_Kelvin=False):
        try:
            self.u.size
        except AttributeError:
            self.load_u()

        f = fhydrogen
        if have_ne:
            try:
                self.ne.size
            except AttributeError:
                self.load_ne()
            mmean = 4.0 / (1.0 + 3.0*f + 4.0*f*self.ne)
        else:
            mmean = (1.0+(1.0-f)/f)/(2.0+3.0*(1.0-f)/(4.0*f))

        self.temp = 2.0/3.0*self.u*1.0e6*mmean*mproton/keV  # in keV

        self.temp_kelvin = self.temp * keV / kboltzmann

        if del_u:
            del self.u
        if del_ne and have_ne:
            del self.ne

        del mmean

    # get entropy in keV cm^2
    def get_entropy(self, have_ne=True, del_ne=True):
        try:
            self.temp.size
        except AttributeError:
            self.get_temp(have_ne=have_ne, del_u=True, del_ne=False)

        f = fhydrogen
        if have_ne:
            try:
                self.ne.size
            except AttributeError:
                self.load_ne()
            ne = self.ne
        else:
            ne = 1.0 + 2.0*(1.0-f)/(4.0*f)

        electron_density = self.rho*self.rhogadget*f / \
            mproton*ne*1.0e-6  # free electron density in cm^-3

        self.entropy = self.temp / electron_density**(2.0/3.0)  # in kev cm^2

        if del_ne:
            del ne
        del electron_density

    # get sound speed in km/s
    def get_csound(self, have_ne=True, del_u=True, del_temp=False,
                   del_ne=True):
        try:
            self.temp.size
        except AttributeError:
            self.get_temp(have_ne=have_ne, del_u=del_u, del_ne=False)

        f = fhydrogen
        if have_ne:
            try:
                self.ne.size
            except AttributeError:
                self.load_ne()
            mmean = 4.0 / (1.0 + 3.0*f + 4.0*f*self.ne)
        else:
            mmean = (1.0+(1.0-f)/f)/(2.0+3.0*(1.0-f)/(4.0*f))

        self.csound = pyl.sqrt(5.0/3.0 * self.temp*keV /
                               (mmean * mproton)) / 1000.0

        if del_temp:
            del self.temp
        if del_ne and have_ne:
            del self.ne

        del mmean

    # get thermal SZ Compton y times area in physical m^2
    def get_SZ_yA(self):
        try:
            self.temp
        except AttributeError:
            self.get_temp()

        try:
            self.gasmass
        except AttributeError:
            self.load_gasmass()

        try:
            self.ne
        except AttributeError:
            self.load_ne()

        # y x area in m^2
        self.yA = (pyl.float64(self.ne) * self.gasmass * self.mgadget *
                   fhydrogen / mproton * sigma_thomson * self.temp *
                   (keV / (melectron*clight*clight)))
        # NB: mgadget is in Msun not Msun/h

    # get H0, H+, He0, He+, He++, e abundances and temp using c code
    # NOT YET WORKING PROPERLY FOR MULTI-PHASE GAS
    def get_temp_and_abundances(self):
        import numpy as np
        try:
            self.u.size
        except AttributeError:
            self.load_u()

        try:
            self.rho.size
        except AttributeError:
            self.load_rho()

        try:
            self.ne.size
        except AttributeError:
            self.load_ne()

        libabund = np.ctypeslib.load_library(
            'libcool.so', os.environ["MYPYTHONPATH"]+'ctools/cooling/')
        libabund.get_temp_and_abundances_all.argtypes = [
                ct.c_double, ct.c_double, ct.c_int,
                np.ctypeslib.ndpointer(), np.ctypeslib.ndpointer(),
                np.ctypeslib.ndpointer(), np.ctypeslib.ndpointer(),
                np.ctypeslib.ndpointer(), np.ctypeslib.ndpointer(),
                np.ctypeslib.ndpointer(), np.ctypeslib.ndpointer(),
                np.ctypeslib.ndpointer(), np.ctypeslib.ndpointer()]

        self.temp_abund = pyl.empty(self.header.nall[0], dtype=pyl.float32)
        self.ne_abund = pyl.empty(self.header.nall[0], dtype=pyl.float32)
        self.nh_abund = pyl.empty(self.header.nall[0], dtype=pyl.float32)
        self.nhp = pyl.empty(self.header.nall[0], dtype=pyl.float32)
        self.nhe = pyl.empty(self.header.nall[0], dtype=pyl.float32)
        self.nhep = pyl.empty(self.header.nall[0], dtype=pyl.float32)
        self.nhepp = pyl.empty(self.header.nall[0], dtype=pyl.float32)

        assert self.u.flags['C_CONTIGUOUS']
        assert self.rho.flags['C_CONTIGUOUS']
        assert self.ne.flags['C_CONTIGUOUS']

        assert self.temp_abund.flags['C_CONTIGUOUS']
        assert self.ne_abund.flags['C_CONTIGUOUS']
        assert self.nh_abund.flags['C_CONTIGUOUS']
        assert self.nhp.flags['C_CONTIGUOUS']
        assert self.nhe.flags['C_CONTIGUOUS']
        assert self.nhep.flags['C_CONTIGUOUS']
        assert self.nhepp.flags['C_CONTIGUOUS']

        libabund.get_temp_and_abundances_all(
                self.header.redshift, self.header.hubble, self.header.nall[0],
                self.u, self.rho, self.ne, self.temp_abund, self.ne_abund,
                self.nh_abund, self.nhp, self.nhe, self.nhep, self.nhepp)

    # free memory used for particle data
    def free_particle_data(self):
        try:
            del self.pos
        except AttributeError:
            pass

        try:
            del self.vel
        except AttributeError:
            pass

        try:
            del self.mass
        except AttributeError:
            pass

        try:
            del self.ids
        except AttributeError:
            pass

        try:
            del self.rho
        except AttributeError:
            pass

        try:
            del self.u
        except AttributeError:
            pass

        try:
            del self.hsml
        except AttributeError:
            pass

        try:
            del self.vol
        except AttributeError:
            pass

        # some blocks still missing

    # -------------------------------------------------------------
    # USEFUL STUFF

    def rel_pos(self, x, centerx):
        """Get the position of 'x' relative to 'centerx'.

        Returns x - centerx, taking periodicity into account.
        """
        # Get length of x
        try:
            n = len(x)
        except TypeError:  # scalar
            n = 1

        if n >= 1:
            y = x - centerx

            assert y.min() > -self.box_sidelength, "y.min() = "+str(y.min())
            assert y.max() < self.box_sidelength, "y.max() = "+str(y.max())

            y[pyl.where(y < -0.5*self.box_sidelength)] += self.box_sidelength

            y[pyl.where(y > 0.5*self.box_sidelength)] -= self.box_sidelength

            return y
        else:
            # Return empty lists.
            return x

    def nearest_dist_1D(self, x1, x2):
        """Get nearest 1D distance of x1 relative to x2.

        Return the distance of the 1D position in x1 (list/array) that is
        nearest to the 1D position x2 (scalar).
        """
        dx = pyl.abs(x1 - x2)
        assert dx.max() < self.box_sidelength

        ind = pyl.where(dx > 0.5*self.box_sidelength)
        dx[ind] = self.box_sidelength - dx[ind]

        return dx

    def nearest_dist_3D(self, r1, r2):
        """Get nearest 3D distance of r1 relative to r2.

        Return the distance of the 3D position in r1 (2D list/array) that is
        nearest to the 3D position r2 (1D list/array).
        """
        r = self.rel_pos(r1, r2)
        return pyl.sqrt((r**2).sum(axis=r.ndim-1))

    # -------------------------------------------------------------
    # GET GLOBAL QUANTITIES

    # Global mass weighted temperature.
    def get_mean_temp_mw(self, inKelvin=False):
        mean_temp = pyl.average(
            self.temp, weights=self.mass[0:self.header.nall[0]])
        if inKelvin:
            mean_temp *= keV/kboltzmann
        return mean_temp

    # Global volume weighted temperature.
    def get_mean_temp_vw(self, inKelvin=False):
        mean_temp = pyl.average(
            self.temp, weights=self.mass[0:self.header.nall[0]]/self.rho)
        if inKelvin:
            mean_temp *= keV/kboltzmann
        return mean_temp

    # Mean baryon density.
    def get_mean_baryon_density_box(self, from_cosmo=False, set_params=False):
        # will not work for Arepo when refinement is used.
        if from_cosmo:
            ob2om = (self.mass[0:self.header.nall[0]].max() /
                     (self.mass[0:self.header.nall[0]].max() +
                      self.mass[self.header.nall[0]]))
            mean_rho = self.rho_mean * ob2om
        else:
            if self.header.nall[2] + self.header.nall[3] > 0:
                print("WARNING: calculating mean baryon density in box "
                      "in a zoom-in sim.")
            baryonmass = (
                self.mass[0:self.header.nall[0]].sum(dtype=pyl.float64) +
                self.mass[(self.header.nall.cumsum())[3]:].sum(
                                                            dtype=pyl.float64))
            mean_rho = baryonmass/self.header.boxsize**3
            if self.mpc_units:
                mean_rho /= 1.0e9

        if set_params:
            self.omega_b = mean_rho/self.rho_mean * self.header.omega_m
            print("setting omega_b =", self.omega_b)
            self.rho_mean_baryons = (self.rho_mean / self.header.omega_m *
                                     self.omega_b)

        return mean_rho

    def get_sph2real_volume(self):
        sphvol = (self.mass[0:self.header.nall[0]] /
                  self.rho).sum(dtype=pyl.float64)
        sph2real = sphvol/self.box_sidelength**3
        print("SPH / real volume =", sph2real)
        return sph2real

    def get_power_spectrum(self, ngrid, bins=50, parttype=-1, wrap=1,
                           zoom_center=0, zoom_box=0):
        from projectparticles.put_grid import put_grid_cic3D

        # make sure wrap is power of 2
        assert ((wrap != 0) and ((wrap & (wrap - 1)) == 0))
        # zoom_box=0 means use whole box
        # Otherwise use zoom_center and zoom_box for centre pos and boxsize
        if (zoom_box == 0):
            boxcenter = 0.5*self.box_sidelength / \
                wrap * pyl.ones(3, dtype=pyl.float64)
            boxlength = self.box_sidelength / wrap
        else:
            boxcenter = zoom_center
            boxlength = zoom_box

        print("boxlength for power spectrum =", boxlength)

        if wrap == 1:
            powpos = self.pos
        else:
            powpos = (self.pos/boxlength -
                      pyl.floor(self.pos/boxlength)) * boxlength

        if (zoom_box == 0):  # Use whole box
            if parttype == -1:
                # 'Put' (project) particle masses onto 3-dimensional grid
                # of size ngrid x ngrid x ngrid
                delta_field = put_grid_cic3D(
                    ngrid, boxcenter[0], boxcenter[1], boxcenter[2], boxlength,
                    powpos[:, 0], powpos[:, 1], powpos[:, 2], self.mass)
            else:
                # Only consider specific particle type
                ind = pyl.where(self.parttype == parttype)[0]
                delta_field = put_grid_cic3D(
                    ngrid, boxcenter[0], boxcenter[1], boxcenter[2], boxlength,
                    powpos[ind, 0], powpos[ind, 1], powpos[ind, 2],
                    self.mass[ind])
        else:  # Use part of box
            if parttype == -1:
                ind = pyl.where((self.pos[:, 0] > boxcenter[0]-0.5*boxlength) &
                                (self.pos[:, 0] < boxcenter[0]+0.5*boxlength) &
                                (self.pos[:, 1] > boxcenter[1]-0.5*boxlength) &
                                (self.pos[:, 1] < boxcenter[1]+0.5*boxlength) &
                                (self.pos[:, 2] > boxcenter[2]-0.5*boxlength) &
                                (self.pos[:, 2] < boxcenter[2]+0.5*boxlength)
                                )[0]
                delta_field = put_grid_cic3D(
                    ngrid, boxcenter[0], boxcenter[1], boxcenter[2], boxlength,
                    powpos[ind, 0], powpos[ind, 1], powpos[ind, 2],
                    self.mass[ind])
            else:
                ind = pyl.where((self.pos[:, 0] > boxcenter[0]-0.5*boxlength) &
                                (self.pos[:, 0] < boxcenter[0]+0.5*boxlength) &
                                (self.pos[:, 1] > boxcenter[1]-0.5*boxlength) &
                                (self.pos[:, 1] < boxcenter[1]+0.5*boxlength) &
                                (self.pos[:, 2] > boxcenter[2]-0.5*boxlength) &
                                (self.pos[:, 2] < boxcenter[2]+0.5*boxlength) &
                                (self.parttype == parttype))[0]
                print(powpos[ind, :].min(), powpos[ind, :].max())
                delta_field = put_grid_cic3D(
                    ngrid, boxcenter[0], boxcenter[1], boxcenter[2], boxlength,
                    powpos[ind, 0], powpos[ind, 1], powpos[ind, 2],
                    self.mass[ind])

        if (zoom_box == 0):
            mean_mass = delta_field.sum(dtype=pyl.float64)/ngrid**3
            delta_field /= mean_mass
            delta_field -= 1.0
        else:
            delta_field /= self.rho_mean*(boxlength/ngrid)**3
            delta_field -= 1.0
            vol_frac = delta_field.mean(dtype=pyl.float64) + 1.0
            print("vol_frac =", vol_frac)

        # Discrete fast Fourier transform of mass grid
        fft_delta = pyl.rfftn(delta_field)
        del delta_field

        # [0,1,2...,ngrid]*2*pyl.pi/boxlength
        k_a = 2*pyl.pi/boxlength*pyl.arange(ngrid)
        # [-ngrid, -ngrid+1..., 0]
        k_b = 2*pyl.pi/boxlength*(pyl.arange(ngrid)-ngrid)
        ind = pyl.where(abs(k_a) <= abs(k_b))[0]
        k_onedim = k_b
        k_onedim[ind] = k_a[ind]

        kx = k_onedim[:, None, None] * \
            pyl.ones((ngrid, ngrid, fft_delta.shape[2]))
        ky = k_onedim[None, :, None] * \
            pyl.ones((ngrid, ngrid, fft_delta.shape[2]))
        kz = k_onedim[None, None, 0:fft_delta.shape[2]] * \
            pyl.ones((ngrid, ngrid, fft_delta.shape[2]))
        kmag = pyl.sqrt(kx*kx + ky*ky + kz*kz)

        deconv = pyl.ones(
            (ngrid, ngrid, fft_delta.shape[2]), dtype=pyl.float64)
        deconv[1:, :, :] *= ((pyl.sin(kx[1:, :, :]*boxlength / (2.0*ngrid)) /
                             (kx[1:, :, :] * boxlength/(2.0*ngrid)))**2)
        deconv[:, 1:, :] *= ((pyl.sin(ky[:, 1:, :]*boxlength / (2.0*ngrid)) /
                              (ky[:, 1:, :] * boxlength / (2.0*ngrid)))**2)
        deconv[:, :, 1:] *= ((pyl.sin(kz[:, :, 1:]*boxlength / (2.0*ngrid)) /
                             (kz[:, :, 1:]*boxlength/(2.0*ngrid)))**2)
        fft_delta /= deconv   # CIC deconvolution

        kmin = 2*pyl.pi/boxlength
        kmax = abs(k_onedim).max()
        kvalues = pyl.logspace(pyl.log10(0.999*kmin),
                               pyl.log10(1.001*kmax), bins+1)

        self.powspec_nums = pyl.histogram(kmag, bins=kvalues)[0]
        weights = ((fft_delta.real**2 + fft_delta.imag**2)*boxlength**3 /
                   pyl.float64(ngrid)**6/(2.0*pyl.pi)**3)
        self.powspec = pyl.histogram(kmag, bins=kvalues,
                                     weights=weights)[0]  # in (h/kpc)**3
        ind = pyl.where(self.powspec_nums > 0)[0]
        self.powspec[ind] /= self.powspec_nums[ind]
        if wrap != 1:
            self.powspec *= wrap**3
        if (zoom_box != 0):
            self.powspec /= vol_frac
        self.powspec_kedges = kvalues
        self.powspec_kcenters = 0.5*(kvalues[:-1]+kvalues[1:])
        self.powspec_wrap = wrap

    # -------------------------------------------------------------
    # select spherical region
    def select_sphere(self, center, radius):
        radius_sq = radius*radius
        partrads_sq = ((center - self.pos)**2).sum(axis=1)
        self.sel_ind = pyl.where(partrads_sq < radius_sq)[0]

        self.sel_nall = pyl.zeros(6, dtype=pyl.int64)
        for i in range(6):
            if i == 0:
                startind = 0
            else:
                startind = (self.header.nall.cumsum()[i-1])

            stopind = (self.header.nall.cumsum()[i])

            self.sel_nall[i] = (pyl.where((self.sel_ind >= startind) &
                                (self.sel_ind < stopind))[0]).size

        assert self.sel_nall.sum() == self.sel_ind.size

    def select_sphere_group(self, groupnum, radius):
        print("selecting sphere around group", groupnum, "...")
        self.select_sphere(self.cat.group_pos[groupnum], radius)
        print("...done!")

    # tempmax < 0 means tempmax = infinity
    def select_gas_by_temp(self, tempmin, tempmax, in_kelvin=False):
        assert tempmin >= 0

        if in_kelvin:
            temp = self.temp_kelvin
        else:
            temp = self.temp

        ind = pyl.where(temp >= tempmin)[0]

        if tempmax >= 0:
            ind2 = pyl.where(temp[ind] <= tempmax)[0]
            ind = ind[ind2]

        self.sel_ind = ind
        self.sel_nall = pyl.zeros(6, dtype=pyl.int64)
        self.sel_nall[0] = ind.size

        if tempmax >= 0:
            print("selected", self.sel_nall[0], "gas particles by temperature",
                  tempmin, "to", tempmax)
        else:
            print("selected", self.sel_nall[0], "gas particles by temperature",
                  tempmin, "to infinity")

    # Zmax < 0 means Zmax = infinity
    def select_gas_by_metallicity(self, Zmin, Zmax):
        assert Zmin >= 0

        ind = pyl.where(self.Zgas >= Zmin)[0]

        if Zmax >= 0:
            ind2 = pyl.where(self.Zgas[ind] <= Zmax)[0]
            ind = ind[ind2]

        self.sel_ind = ind
        self.sel_nall = pyl.zeros(6, dtype=pyl.int64)
        self.sel_nall[0] = ind.size

        if Zmax >= 0:
            print("selected", self.sel_nall[0], "gas particles by metallicity",
                  Zmin, "to", Zmax)
        else:
            print("selected", self.sel_nall[0], "gas particles by metallicity",
                  Zmin, "to infinity")

    def deselect(self):
        del self.sel_ind
        del self.sel_nall

    # -------------------------------------------------------------

    # get spherical overdensity mass and radius
    def get_so_mass_rad(self, groupnum, meandens, ref="crit", subhalo=False,
                        use_sysexit=True, only_sel=False, verbose=False):
        # meandens is delta, the overdensity factor e.g. 500
        try:
            self.pos.size
        except AttributeError:
            if verbose:
                print("Loading positions...")
            self.load_pos()
        try:
            self.mass.size
        except AttributeError:
            if verbose:
                print("Loading masses...")
            self.load_mass()

        if ref == "crit":
            # Critical density in code units
            thresdens = (2.77536627e-8 *
                         self.cm.hubble_z(self.header.redshift)**2 /
                         (self.header.hubble**2 * (1+self.header.redshift)**3))
        elif ref == "crit_today":
            # Critical density today in code units
            thresdens = 2.77536627e-8 / (1+self.header.redshift)**3
        elif ref == "mean":
            thresdens = 2.77536627e-8 * self.header.omega_m  # code units
        else:
            pyl.sys.exit("reference density not known")

        if only_sel:
            sel_pos = self.pos[self.sel_ind]
            sel_mass = self.mass[self.sel_ind]
        else:
            sel_pos = self.pos
            sel_mass = self.mass

        if verbose:
            print("Calculating radii...")
        if not subhalo:
            partrads = self.nearest_dist_3D(
                sel_pos, self.cat.group_pos[groupnum])
        elif subhalo:
            partrads = self.nearest_dist_3D(
                sel_pos, self.cat.sub_pos[groupnum])
        if verbose:
            print("Sorting by radius...")
        radord = partrads.argsort()
        partrads = partrads[radord]
        if verbose:
            print("Calculating densities...")

        # maximum is used to prevent division by zero
        meandensity = ((sel_mass[radord].cumsum(dtype=pyl.float64) -
                       0.5*sel_mass[radord]) /
                       (4.0/3.0*pyl.pi*pyl.maximum(partrads, 1.0e-5)**3))

        if verbose:
            print("Finding critical point...")
        ind = pyl.where((meandensity[:-1] >= thresdens*meandens)
                        & (meandensity[1:] < thresdens*meandens))

        if ind[0].size == 0:
            if use_sysexit:
                pyl.sys.exit("no suitable radius found")
            else:
                return -1, -1
        elif ind[0].size != 1:
            print("WARNING:", ind[0].size,
                  "suitable radii found -> using smallest")

        ind = ind[0][0]
        if partrads[ind] == 0:
            pyl.sys.exit("SO radius not resolved")

        radius = ((partrads[ind]*(thresdens*meandens-meandensity[ind+1]) +
                  partrads[ind+1]*(meandensity[ind]-thresdens*meandens)) /
                  (meandensity[ind]-meandensity[ind+1]))  # code units
        mass = 4.0/3.0*pyl.pi*radius**3*thresdens*meandens  # code units

        if verbose:
            print("SO mass and radius for group", groupnum)
            print("M_"+ref+","+str(meandens)+" =", mass)
            print("r_"+ref+","+str(meandens)+" =", radius)

        return mass, radius

    # -------------------------------------------------------------
    # GET GROUP QUANTITIES

    # find number of galaxies in group.
    def group_Ngal(self, group, minmass=0, radius=-1):
        groupsubs = self.cat.group_firstsub[group] + \
            pyl.arange(self.cat.group_nsubs[group])
        groupsubs = groupsubs[pyl.where(
            self.cat.sub_masstab[groupsubs, 4] > minmass)]
        if radius > 0:
            subrads = pyl.sqrt(((self.cat.sub_pos[groupsubs] -
                                 self.cat.group_pos[group])**2).sum(axis=1))
            groupsubs = groupsubs[pyl.where(subrads < radius)]
        return groupsubs.__len__()

    def group_Ysz(self, group, radius):  # Ysz in 10^10 M_sun / h keV
        ind = self.get_particles_sphere(
            self.cat.group_pos[group], radius, parttype=0)
        return (self.mass[ind]*self.temp[ind]).sum(dtype=pyl.float64)

    # find radius to closest boundary particle (species 2 and 3)
    # WARNING: no box wrapping done
    def contamination_radius(self, group):
        if self.header.nall[2] + self.header.nall[3] > 0:
            contpartrads = pyl.sqrt(
                ((self.cat.group_pos[group] -
                  self.pos[self.species_start[2]:self.species_end[3]])**2
                 ).sum(axis=1))
            return contpartrads.min()
        else:
            return -1.0

    # -------------------------------------------------------------
    # SAVE STUFF TO TEXTFILES

    # save sim quantities
    def print_sim_properties(self, target_file="none"):
        if target_file == "none":
            f = pyl.sys.stdout
        else:
            f = open(target_file, 'w')

        print(self.snapname, "   ", "snapname", file=f)
        print(self.snapnum, "   ", "snapnum\n", file=f)
        print(self.header.time, "   ", "ascale", file=f)
        print(self.header.redshift, "   ", "redshift\n", file=f)
        print(self.header.boxsize, "   ", "boxsize\n", file=f)
        print(self.header.hubble, "   ", "hubble", file=f)
        print(self.header.omega_m, "   ", "omega_m", file=f)
        print(self.header.omega_l, "   ", "omega_l", file=f)
        try:
            omb = self.omega_b
            print(omb, "   ", "omega_b", file=f)
        except AttributeError:
            pass
        print("", file=f)
        print(self.header.cooling, "   ", "cooling", file=f)
        print(self.header.sfr, "   ", "sfr\n", file=f)
        for i in pyl.arange(6):
            print(self.header.nall[i], "   ", "nall["+str(i)+"]", file=f)
        print("", file=f)
        for i in pyl.arange(6):
            print(self.header.massarr[i], "   ", "massarr["+str(i)+"]", file=f)

        f.close()

    # save group quantities
    def print_group_properties(self, group=0, target_file="none",
                               only_sel=False):
        if target_file == "none":
            f = pyl.sys.stdout
        else:
            f = open(target_file, 'w')

        print(group, "   ", "group number", file=f)

        m200c, r200c = self.get_so_mass_rad(
            group, 200, ref="crit", only_sel=only_sel)
        m500c, r500c = self.get_so_mass_rad(
            group, 500, ref="crit", only_sel=only_sel)
        m200m, r200m = self.get_so_mass_rad(
            group, 200, ref="mean", only_sel=only_sel)
        m500m, r500m = self.get_so_mass_rad(
            group, 500, ref="mean", only_sel=only_sel)

        print(m200c, "   ", "m200c", file=f)
        print(m200m, "   ", "m200m", file=f)
        print(m500c, "   ", "m500c", file=f)
        print(m500m, "   ", "m500m", file=f)
        print(r200c, "   ", "r200c", file=f)
        print(r200m, "   ", "r200m", file=f)
        print(r500c, "   ", "r500c", file=f)
        print(r500m, "   ", "r500m", file=f)
        print(self.contamination_radius(
            group), "   ", "contamination radius", file=f)
        print(self.cat.group_pos[group][0], "   ", "x", file=f)
        print(self.cat.group_pos[group][1], "   ", "y", file=f)
        print(self.cat.group_pos[group][2], "   ", "z", file=f)
        # in keV
        print(self.cat.group_T_crit200[group], "   ", "T200c", file=f)
        # in keV / cm^3
        print(self.cat.group_P_crit200[group], "   ", "P200c", file=f)

        f.close()

    # -------------------------------------------------------------
    # MAKE ALL KINDS OF PROFILES

    # make species profiles
    def make_species_profile(self, group=0, cenpos="none", radius=2500.0,
                             bintype="lin", save_dat="none", save_plot="none",
                             only_sel=False):
        if cenpos == "none":
            cenpos = self.cat.group_pos[group]
        print("profile center =", cenpos)

        if only_sel:
            sel_pos = self.pos[self.sel_ind]
            sel_mass = self.mass[self.sel_ind]
            sel_nall = self.sel_nall
        else:
            sel_pos = self.pos
            sel_mass = self.mass
            sel_nall = self.header.nall

        if bintype == "lin":
            bin_edges = pyl.linspace(0, radius, 2501)
        elif bintype == "log":
            bin_edges = pyl.logspace(pyl.log10(0.1), pyl.log10(radius), 100)
            bin_edges = pyl.append(0.0, bin_edges)
        elif bintype == "gas":
            num_bins = 12800
            minperbin = 1000
            nmergemaxpow = 8
            rads = pyl.sqrt(((cenpos - sel_pos[0:sel_nall[0]])**2).sum(axis=1))
            bin_edges = histogram_edges(
                rads, (0, radius), num_bins, minperbin, nmergemaxpow)
        else:
            assert False

        self.volumes = 4.0*pyl.pi/3.0*(bin_edges[1:]**3-bin_edges[:-1]**3)
        self.bincenters = 0.5*(bin_edges[1:]+bin_edges[:-1])
        self.massprofiles = pyl.zeros((0, bin_edges.__len__()-1))
        self.cummassprofiles = pyl.zeros((0, bin_edges.__len__()-1))
        self.densities = pyl.zeros((0, bin_edges.__len__()-1))

        for i in range(6):
            if i == 0:
                startpart = 0
            else:
                startpart = (sel_nall.cumsum())[i-1]
            stoppartplusone = (sel_nall.cumsum())[i]

            rads = pyl.sqrt(
                ((cenpos - sel_pos[startpart:stoppartplusone])**2).sum(axis=1))

            rads = pyl.float64(rads)
            massprofile, buff = pyl.histogram(
                    rads, range=(0, radius), bins=bin_edges,
                    weights=pyl.float64(sel_mass[startpart:stoppartplusone]))
            self.massprofiles = pyl.append(
                self.massprofiles, pyl.array([massprofile]), axis=0)
            self.cummassprofiles = pyl.append(
                self.cummassprofiles, pyl.array([massprofile.cumsum()]),
                axis=0)
            self.densities = pyl.append(self.densities, pyl.array(
                [massprofile/self.volumes]), axis=0)

        if save_dat != "none":
            outdat = pyl.zeros((0, bin_edges.__len__()-1))
            outdat = pyl.append(outdat, pyl.array(
                [pyl.arange(bin_edges.__len__()-1)]), axis=0)
            outdat = pyl.append(outdat, pyl.array([bin_edges[:-1]]), axis=0)
            outdat = pyl.append(outdat, pyl.array([self.bincenters]), axis=0)
            outdat = pyl.append(outdat, pyl.array([bin_edges[1:]]), axis=0)
            for i in range(6):
                outdat = pyl.append(outdat, pyl.array(
                    [self.massprofiles[i]]), axis=0)
                outdat = pyl.append(outdat, pyl.array(
                    [self.cummassprofiles[i]]), axis=0)
                outdat = pyl.append(outdat, pyl.array(
                    [self.densities[i]]), axis=0)
            outdat = outdat.transpose()

            pyl.savetxt(save_dat, outdat)
            del outdat

    # make gas profiles
    def make_gas_profile(self, group=0, cenpos="none", radius=2500.0,
                         bintype="lin", save_dat="none", save_plot="none",
                         only_sel=False):
        if cenpos == "none":
            cenpos = self.cat.group_pos[group]
        print("profile center =", cenpos)

        if only_sel:
            sel_nall = self.sel_nall
            sel_pos = self.pos[self.sel_ind]
            sel_mass = self.mass[self.sel_ind]
            sel_rho = self.rho[self.sel_ind[0:sel_nall[0]]]
            sel_temp = self.temp[self.sel_ind[0:sel_nall[0]]]
            sel_entropy = self.entropy[self.sel_ind[0:sel_nall[0]]]
        else:
            sel_nall = self.header.nall
            sel_pos = self.pos
            sel_mass = self.mass
            sel_rho = self.rho
            sel_temp = self.temp
            sel_entropy = self.entropy

        rads = pyl.sqrt(((cenpos - sel_pos[0:sel_nall[0]])**2).sum(axis=1))

        if bintype == "lin":
            bin_edges = pyl.linspace(0, radius, 2501)
        elif bintype == "log":
            bin_edges = pyl.logspace(pyl.log10(0.1), pyl.log10(radius), 100)
            bin_edges = pyl.append(0.0, bin_edges)
        elif bintype == "gas":
            num_bins = 12800
            minperbin = 1000
            nmergemaxpow = 8
            bin_edges = histogram_edges(
                rads, (0, radius), num_bins, minperbin, nmergemaxpow)
        else:
            assert False

        volumes = 4.0*pyl.pi/3.0*(bin_edges[1:]**3-bin_edges[:-1]**3)

        mass, buff = pyl.histogram(
                rads, range=(0, radius), bins=bin_edges,
                weights=pyl.float64(sel_mass[0:sel_nall[0]]))
        sph_volume, buff = pyl.histogram(
                rads, range=(0, radius), bins=bin_edges,
                weights=pyl.float64(sel_mass[0:sel_nall[0]]/sel_rho))

        mw_temp, buff = pyl.histogram(
                rads, range=(0, radius), bins=bin_edges,
                weights=pyl.float64(sel_temp*sel_mass[0:sel_nall[0]]))
        mw_entropy, buff = pyl.histogram(
                rads, range=(0, radius), bins=bin_edges,
                weights=pyl.float64(sel_entropy*sel_mass[0:sel_nall[0]]))
        vw_entropy, buff = pyl.histogram(
            rads, range=(0, radius), bins=bin_edges,
            weights=pyl.float64(sel_entropy*sel_mass[0:sel_nall[0]]/sel_rho))

        cummass = mass.cumsum()
        density = mass/volumes

        ind = pyl.where(mass > 0)[0]
        mw_temp = mw_temp[ind]/mass[ind]
        mw_entropy = mw_entropy[ind]/mass[ind]
        sph_density = mass[ind]/sph_volume[ind]
        vw_entropy = vw_entropy[ind]/sph_volume[ind]
        binleft = bin_edges[ind]
        binright = bin_edges[ind+1]
        bincenters = 0.5*(binleft+binright)
        volumes = volumes[ind]
        cummass = cummass[ind]
        density = density[ind]
        mass = mass[ind]
        sph_volume = sph_volume[ind]

        if save_dat != "none":
            outdat = pyl.array(zip(ind, binleft, bincenters, binright,
                                   mass, cummass, density, mw_temp, mw_entropy,
                                   vw_entropy, volumes,
                                   sph_volume, sph_density))
            pyl.savetxt(save_dat, outdat)
            del outdat

    # make clumping factor and volume profiles
    def make_clumping_factor_volume_profile(self, mintemp=-1, group=0,
                                            cenpos="none", radius=2500.0,
                                            bintype="lin", save_dat="none",
                                            save_plot="none"):
        if cenpos == "none":
            cenpos = self.cat.group_pos[group]
        print("profile center =", cenpos)

        rads = pyl.sqrt(
            ((cenpos - self.pos[0:self.header.nall[0]])**2).sum(axis=1))

        if bintype == "lin":
            bin_edges = pyl.linspace(0, radius, 2501)
        elif bintype == "log":
            bin_edges = pyl.logspace(pyl.log10(0.1), pyl.log10(radius), 100)
            bin_edges = pyl.append(0.0, bin_edges)
        elif bintype == "gas":
            num_bins = 12800
            minperbin = 1000
            nmergemaxpow = 8
            bin_edges = histogram_edges(
                rads, (0, radius), num_bins, minperbin, nmergemaxpow)
        else:
            assert False

        volumes = 4.0*pyl.pi/3.0*(bin_edges[1:]**3-bin_edges[:-1]**3)

        ind = pyl.where(self.temp >= mintemp)[0]

        mass_bin, buff = pyl.histogram(rads[ind], range=(
            0, radius), bins=bin_edges, weights=pyl.float64(self.mass[ind]))
        density_bin = mass_bin/volumes

        volumes_sph, buff = pyl.histogram(
                rads[ind], range=(0, radius), bins=bin_edges,
                weights=pyl.float64(self.mass[ind]/self.rho[ind]))

        clumping_bin, buff = pyl.histogram(
                rads[ind], range=(0, radius), bins=bin_edges,
                weights=pyl.float64(self.mass[ind]*self.rho[ind]))
        clumping_sph = clumping_bin.copy()

        clumping_bin /= volumes  # gives volume weighted <rho^2>

        enclosed_volumes = volumes.cumsum()
        enclosed_volumes_sph = volumes_sph.cumsum()

        ind = pyl.where(mass_bin > 0)[0]
        binleft = bin_edges[ind]
        binright = bin_edges[ind+1]
        bincenters = 0.5*(binleft+binright)
        mass_bin = mass_bin[ind]
        density_bin = density_bin[ind]
        clumping_bin = clumping_bin[ind]/(density_bin*density_bin)
        clumping_sph = clumping_sph[ind]
        volumes = volumes[ind]
        volumes_sph = volumes_sph[ind]
        enclosed_volumes = enclosed_volumes[ind]
        enclosed_volumes_sph = enclosed_volumes_sph[ind]
        density_sph = mass_bin/volumes_sph
        clumping_sph = clumping_sph / mass_bin**2 * volumes_sph

        if save_dat != "none":
            outdat = pyl.array(zip(ind, binleft, bincenters, binright,
                                   mass_bin, volumes, volumes_sph,
                                   enclosed_volumes, enclosed_volumes_sph,
                                   density_bin, density_sph,
                                   clumping_bin, clumping_sph))
            pyl.savetxt(save_dat, outdat)
            del outdat

    # make pressure profile
    def make_pressure_profile(self, group=0, cenpos="none", radius=2500.0,
                              bintype="lin", save_dat="none",
                              save_plot="none"):
        if cenpos == "none":
            cenpos = self.cat.group_pos[group]
            cenvel = self.cat.sub_vel[self.cat.group_firstsub[group]]
        print("profile center =", cenpos)

        radvecs = self.pos[0:self.header.nall[0]] - cenpos
        vels = self.vel[0:self.header.nall[0]] - \
            cenvel + self.hubble_z_gadget*radvecs
        vels_squared = (vels**2).sum(axis=1)
        rads = pyl.sqrt((radvecs**2).sum(axis=1))

        vel_radial = (radvecs*vels).sum(axis=1)/rads

        if bintype == "lin":
            bin_edges = pyl.linspace(0, radius, 2501)
        elif bintype == "log":
            bin_edges = pyl.logspace(pyl.log10(0.1), pyl.log10(radius), 100)
            bin_edges = pyl.append(0.0, bin_edges)
        elif bintype == "gas":
            num_bins = 12800
            minperbin = 1000
            nmergemaxpow = 8
            bin_edges = histogram_edges(
                rads, (0, radius), num_bins, minperbin, nmergemaxpow)
        else:
            assert False

        volumes = 4.0*pyl.pi/3.0*(bin_edges[1:]**3-bin_edges[:-1]**3)

        mass, buff = pyl.histogram(
                rads, range=(0, radius), bins=bin_edges,
                weights=pyl.float64(self.mass[0:self.header.nall[0]]))

        Ekin, buff = pyl.histogram(
                rads, range=(0, radius), bins=bin_edges,
                weights=pyl.float64(self.mass[0:self.header.nall[0]] *
                                    vels_squared*0.5))

        Etherm, buff = pyl.histogram(
                rads, range=(0, radius), bins=bin_edges,
                weights=pyl.float64(self.mass[0:self.header.nall[0]]*self.u))

        vel_radial_profile, buff = pyl.histogram(
                rads, range=(0, radius), bins=bin_edges,
                weights=pyl.float64(self.mass[0:self.header.nall[0]] *
                                    vel_radial))

        ind = pyl.where(mass > 0)[0]
        binleft = bin_edges[ind]
        binright = bin_edges[ind+1]
        bincenters = 0.5*(binleft+binright)
        volumes = volumes[ind]
        mass = mass[ind]
        Ekin = Ekin[ind]
        Etherm = Etherm[ind]
        vel_radial_profile = vel_radial_profile[ind]/mass
        v_squared_half = Ekin/mass
        u_profile = Etherm/mass
        Pkin = 2.0/3.0 * Ekin * self.mgadget * 1.0e6 / \
            (volumes * (self.lgadget*self.header.time)**3) / \
            (keV / 0.01**3)  # in keV / cm^3
        Ptherm = 2.0/3.0 * Etherm * self.mgadget * 1.0e6 / \
            (volumes * (self.lgadget*self.header.time)**3) / \
            (keV / 0.01**3)  # in keV / cm^3

        have_bfld = False
        try:
            self.bfld
            have_bfld = True
        except AttributeError:
            try:
                self.load_bfld()
                have_bfld = True
            except AttributeError:
                print("no bfield found")

        if have_bfld:
            try:
                self.vol
            except AttributeError:
                self.load_vol()

            # magnetic pressure in Pascal
            # (0.1 factor is conversion from Barye to Pascal)
            PBparticle = (self.bfld**2).sum(axis=1) / (8.0 * pyl.pi) * 0.1
            PBvol, buff = pyl.histogram(
                    rads, range=(0, radius), bins=bin_edges,
                    weights=pyl.float64(PBparticle * self.vol))
            cellvol, buss = pyl.histogram(
                    rads, range=(0, radius), bins=bin_edges,
                    weights=pyl.float64(self.vol))

            PB = PBvol[ind]/cellvol[ind] / (keV / 0.01**3)  # in keV / cm^3
        else:
            PB = pyl.zeros_like(Ptherm)

        if save_dat != "none":
            outdat = pyl.array(zip(ind, binleft, bincenters, binright, mass,
                                   v_squared_half, u_profile,
                                   vel_radial_profile, Pkin, Ptherm, PB))
            pyl.savetxt(save_dat, outdat)
            del outdat

    # make B profile
    def make_bfld_profile(self, group=0, cenpos="none", radius=2500.0,
                          bintype="lin", save_dat="none", only_sel=False):
        if cenpos == "none":
            cenpos = self.cat.group_pos[group]
        print("profile center =", cenpos)

        try:
            self.vol
        except AttributeError:
            self.load_vol()
        sel_vol = self.vol

        try:
            self.bfld
        except AttributeError:
            self.load_bfld()
        sel_b2 = (self.bfld**2).sum(axis=1)

        if only_sel:
            sel_nall = self.sel_nall
            sel_pos = self.pos[self.sel_ind]
            sel_vol = self.vol[self.sel_ind]
            sel_b2 = (self.bfld[self.sel_ind]**2).sum(axis=1)
        else:
            sel_nall = self.header.nall
            sel_pos = self.pos
            sel_vol = self.vol
            sel_b2 = (self.bfld**2).sum(axis=1)

        rads = pyl.sqrt(((cenpos - sel_pos[0:sel_nall[0]])**2).sum(axis=1))

        if bintype == "lin":
            bin_edges = pyl.linspace(0, radius, 2501)
        elif bintype == "log":
            bin_edges = pyl.logspace(pyl.log10(0.1), pyl.log10(radius), 100)
            bin_edges = pyl.append(0.0, bin_edges)
        else:
            assert False

        self.bfld_profile_volume = 4.0*pyl.pi / \
            3.0*(bin_edges[1:]**3-bin_edges[:-1]**3)

        self.bfld_profile_cell_volume, buff = pyl.histogram(
                rads, range=(0, radius), bins=bin_edges,
                weights=pyl.float64(sel_vol[0:sel_nall[0]]))

        self.bfld_profile_vw_b2, buff = pyl.histogram(
                rads, range=(0, radius), bins=bin_edges,
                weights=pyl.float64(sel_b2[0:sel_nall[0]] *
                                    sel_vol[0:sel_nall[0]]))

        ind = pyl.where(self.bfld_profile_cell_volume > 0)[0]
        self.bfld_profile_ind = ind
        self.bfld_profile_vw_b2 = self.bfld_profile_vw_b2[ind] / \
            self.bfld_profile_cell_volume[ind]
        self.bfld_profile_binleft = bin_edges[ind]
        self.bfld_profile_binright = bin_edges[ind+1]
        self.bfld_profile_bincenters = 0.5 * \
            (self.bfld_profile_binleft+self.bfld_profile_binright)
        self.bfld_profile_volume = self.bfld_profile_volume[ind]
        self.bfld_profile_cell_volume = self.bfld_profile_cell_volume[ind]

        if save_dat != "none":
            outdat = pyl.array(zip(self.bfld_profile_ind,
                                   self.bfld_profile_binleft,
                                   self.bfld_profile_bincenters,
                                   self.bfld_profile_binright,
                                   self.bfld_profile_volume,
                                   self.bfld_profile_cell_volume,
                                   self.bfld_profile_vw_b2))
            pyl.savetxt(save_dat, outdat)
            del outdat

    def make_stellar_formation_time_profile(self, group=0, cenpos="none",
                                            radius=1000.0, bintype="lin",
                                            save_dat="none"):
        if cenpos == "none":
            cenpos = self.cat.group_pos[group]
        print("profile center =", cenpos)

        radvecs = self.pos[self.species_start[4]:self.species_end[4]] - cenpos
        rads = pyl.sqrt((radvecs**2).sum(axis=1))

        if bintype == "lin":
            bin_edges = pyl.linspace(0, radius, 2001)
        elif bintype == "log":
            bin_edges = pyl.logspace(pyl.log10(0.1), pyl.log10(radius), 100)
            bin_edges = pyl.append(0.0, bin_edges)
        else:
            assert False

        binnum = bin_edges.size - 1

        time_profile = pyl.zeros((binnum, 7))

        for i in pyl.arange(binnum):
            ind = pyl.where((rads > bin_edges[i]) & (rads < bin_edges[i+1]))[0]

            if ind.size > 0:
                # masses are not taken into account here
                time_profile[i, 0] = pyl.percentile(self.age[ind], 2.28)
                time_profile[i, 1] = pyl.percentile(self.age[ind], 15.87)
                time_profile[i, 2] = pyl.percentile(self.age[ind], 50.00)
                time_profile[i, 3] = pyl.percentile(self.age[ind], 84.13)
                time_profile[i, 4] = pyl.percentile(self.age[ind], 97.72)
                time_profile[i, 5] = pyl.mean(self.age[ind])
                time_profile[i, 6] = pyl.average(self.age[ind], weights=(
                    self.mass[self.species_start[4]:self.species_end[4]])[ind])

        ind = pyl.where(time_profile[:, 5] > 0.0)[0]
        binleft = bin_edges[ind]
        binright = bin_edges[ind+1]
        bincenters = 0.5*(binleft+binright)

        if save_dat != "none":
            outdat = pyl.array(zip(ind, binleft, bincenters, binright,
                                   time_profile[ind, 0], time_profile[ind, 1],
                                   time_profile[ind, 2], time_profile[ind, 3],
                                   time_profile[ind, 4], time_profile[ind, 5],
                                   time_profile[ind, 6]))
            pyl.savetxt(save_dat, outdat)

    # make profile of volume fractions, e.g., above and below
    # temperature threshold, e.g., T_thres ~ 2.5 T200c to find AGN bubbles
    def make_volume_frac_profiles(self, group=0, cenpos="none", radius=2500.0,
                                  bintype="lin", save_dat="none", T_thres=0):
        if cenpos == "none":
            cenpos = self.cat.group_pos[group]
        print("profile center =", cenpos)

        radvecs = self.pos[0:self.header.nall[0]] - cenpos
        rads = pyl.sqrt((radvecs**2).sum(axis=1))

        if bintype == "lin":
            bin_edges = pyl.linspace(0, radius, 2501)
        elif bintype == "log":
            bin_edges = pyl.logspace(pyl.log10(0.1), pyl.log10(radius), 100)
            bin_edges = pyl.append(0.0, bin_edges)
        else:
            assert False

        binleft = bin_edges[:-1]
        nbins = binleft.size
        binright = bin_edges[1:]
        bincenters = 0.5*(binleft+binright)
        volumes = 4.0*pyl.pi/3.0*(binright**3-binleft**3)

        if T_thres > 0:
            ind = pyl.where(self.temp > T_thres)[0]
            volumes_2, buff = pyl.histogram(
                    rads[ind], range=(0, radius), bins=bin_edges,
                    weights=pyl.float64(self.vol[ind]))  # hot stuff

            ind = pyl.where(self.temp <= T_thres)[0]
            volumes_1, buff = pyl.histogram(
                    rads[ind], range=(0, radius), bins=bin_edges,
                    weights=pyl.float64(self.vol[ind]))  # cold stuff

        if save_dat != "none":
            outdat = pyl.array(zip(pyl.arange(nbins),
                                   binleft, bincenters, binright,
                                   volumes, volumes_1, volumes_2))
            pyl.savetxt(save_dat, outdat)

    # -------------------------------------------------------------
    # SELECT SOME PARTICLES

    # get indices of particles in sphere
    def get_particles_sphere(self, pos, radius, parttype=-1, longarray=False,
                             verbose=True):

        partpos = rs.read_block(self.snapname, "POS ", parttype=parttype)
        if self.mpc_units:
            partpos *= 1000.0

        # If the sphere intersects the sim boundary, use nearest_dist_3D
        if (pyl.any((pos - radius < 0.0)) or
                pyl.any((pos + radius > self.box_sidelength))):
            partrads = self.nearest_dist_3D(partpos, pos)
        else:
            partrads = pyl.sqrt(((partpos - pos)**2).sum(axis=1))
        ind = pyl.where(partrads <= radius)

        if verbose:
            print(ind[0].size, "particles of species", parttype,
                  "found in sphere with center =", pos, "and radius", radius)

        # If longarray, a boolean array is returned with length
        # equal to the total no. of particles
        if longarray:
            ind_long = pyl.zeros(self.header.nall.sum(), dtype=bool)
            partskip = self.header.nall[:parttype].sum()
            ind_long[ind + partskip] = True
            return ind_long

        # If not longarray, an index array is returned
        return ind

    # get IDs of particles in a shpere
    def get_ids_sphere(self, pos, radius, parttype=-1):
        ind = self.get_particles_sphere(pos, radius, parttype)

        partids = rs.read_block(self.snapname, "ID  ", parttype=parttype)

        print(ind[0].size, "IDs obtained for particle species", parttype,
              "in sphere with center =", pos, "and radius", radius)

        return partids[ind]

    # find particle with specific IDs
    # elements of ids should be unique
    def find_particles_by_ids(self, ids, parttype=-1):
        partids = rs.read_block(self.snapname, "ID  ", parttype=parttype)

        ind = pyl.where(pyl.in1d(partids, ids, assume_unique=True))

        print(ind[0].size, "of", ids.size, "particles found by ID")

        return ind[0]

    # find host subhalos of particles
    def get_hostsubhalos(self):
        if self.cat.cat_type == "Gadget":
            try:
                self.cat.ids.size
                subids_loaded = False
            except AttributeError:
                self.cat.read_ids()
                subids_loaded = True

            # -1 if not part of subhalo but part of FoF group
            hostsubhalos = -1*pyl.ones(self.cat.nids, dtype=pyl.int32)
            for i in range(self.cat.nsubs):  # set to subhalo number
                hostsubhalos[self.cat.sub_offset[i]:self.cat.sub_offset[i] +
                             self.cat.sub_len[i]] = i

            subidsind = self.cat.ids.argsort()
            subids = self.cat.ids[subidsind]
            hostsubhalos = hostsubhalos[subidsind]
            del subidsind

            if subids_loaded:
                del self.cat.ids

            try:
                self.ids.size
                ids_loaded = False
            except AttributeError:
                self.load_ids()
                ids_loaded = True

            idsind = self.ids.argsort()
            partids = self.ids[idsind]
            origind = (pyl.arange(self.ids.size))[idsind]
            numids = self.ids.size
            del idsind

            if ids_loaded:
                del self.ids

            foundparts = pyl.in1d(partids, subids, assume_unique=True)
            foundparts = pyl.where(foundparts)[0]
            assert foundparts.size == subids.size
            assert (partids[foundparts] == subids).all()
            origind = origind[foundparts]
            del subids
            del partids

            # -2 if not part of any FoF group
            self.hostsubhalos = -2*pyl.ones(numids, dtype=pyl.int32)
            self.hostsubhalos[origind] = hostsubhalos
            del origind
            del hostsubhalos

            self.cat.sub_lentab = pyl.zeros(
                (self.cat.nsubs, 6), dtype=pyl.uint64)
            for i in pyl.arange(6):
                self.cat.sub_lentab[:, i] = (
                        pyl.bincount(self.hostsubhalos[
                            self.species_start[i]:self.species_end[i]] + 2,
                                     minlength=self.cat.nsubs+2))[2:]
            assert (self.cat.sub_lentab.sum(axis=1) == self.cat.sub_len).all()

        else:  # Arepo catalogue
            # -2 if not part of any FoF group
            self.hostsubhalos = (
                    -2 * pyl.ones(self.header.nall.sum(), dtype=pyl.int32))
            for i in pyl.arange(6):
                # -1 if not part of any subhalo
                self.hostsubhalos[self.species_start[i]:self.species_start[i] +
                                  self.cat.group_lentab[:, i].sum()] = -1

            for j in range(0, self.cat.nsubs):
                for i in pyl.arange(6):
                    self.hostsubhalos[
                        self.cat.sub_offsettab[j, i] +
                        self.species_start[i]:self.cat.sub_offsettab[j, i] +
                        self.species_start[i]+self.cat.sub_lentab[j, i]] = j

    # -------------------------------------------------------------
    # PLOT PARTICLES AND SHOW INFO

    # shows clicked subhalo
    def onclick(self, event):
        if self.info == "sub":
            print("----------------------------------------------------------")
            print("SHOWSUB")
            print("----------------------------------------------------------")
            print("clicked coordinates: x = ", event.xdata,
                  "y =", event.ydata, "\n")

            sub = ((self.cat.sub_pos[:, 0] - event.xdata)**2 +
                   (self.cat.sub_pos[:, 1] - event.ydata)**2).argmin()

            print("subhalo number =", sub)
            print("subhalo mass =", self.cat.sub_mass[sub])
            print("----------------------------------------------------------")

        elif self.info == "rcrit200":
            group = ((self.cat.group_pos[:, 0] - event.xdata)**2 +
                     (self.cat.group_pos[:, 1] - event.ydata)**2).argmin()

            r200 = self.cat.group_r_crit200[group]

            print("Plotting r_crit,200 =", r200, "of group", group)

            ang = pyl.arange(101)/100.0*2*pyl.pi
            self.axplotpart.plot(self.cat.group_pos[group, 0]+r200*pyl.cos(
                ang), self.cat.group_pos[group, 1]+r200*pyl.sin(ang), "b")

    # plots particle species
    def plotparts(self, parttype=-1, a_z=0, a_x=0, a_z2=0, ms=0.1,
                  clickable=False):
        plotpos = rs.read_block(self.snapname, "POS ", parttype=parttype)
        if self.mpc_units:
            plotpos *= 1000.0

        if (a_z != 0 or a_x != 0 or a_z2 != 0):
            # rotates coordinate frame / camera
            rotmat = rot_matrix(a_z*pyl.pi, a_x*pyl.pi, a_z2*pyl.pi)
            plotpos = pyl.dot(plotpos, rotmat.transpose())

        fig = pyl.figure()
        self.axplotpart = fig.add_subplot(111, aspect="equal")
        self.axplotpart.plot(
            plotpos[:, 0], plotpos[:, 1], "r.", ms=ms, c="blue", mec="blue")
        self.axplotpart.set_xlabel("comoving distance [kpc/h]")
        self.axplotpart.set_ylabel("comoving distance [kpc/h]")
        if clickable:
            fig.canvas.mpl_connect('button_press_event', self.onclick)
        plotpos = 0
        fig.show()

    # plot particles in slice through simulation
    def plotparts_slice(self, dz, parttype=-1, color_parttype=-1,
                        a_z=0, a_x=0, a_z2=0, ms=0.1, z="none"):
        if z == "none":
            z = 0.5*self.box_sidelength

        plotpos = rs.read_block(self.snapname, "POS ", parttype=parttype)
        if self.mpc_units:
            plotpos *= 1000.0

        if (a_z != 0 or a_x != 0 or a_z2 != 0):
            # rotates coordinate frame / camera
            rotmat = rot_matrix(a_z*pyl.pi, a_x*pyl.pi, a_z2*pyl.pi)
            plotpos = pyl.dot(plotpos, rotmat.transpose())

        fig = pyl.figure()

        ax = fig.add_subplot(111, aspect="equal")
        ax.set_xlabel("comoving distance [kpc/h]")
        ax.set_ylabel("comoving distance [kpc/h]")

        ind = pyl.where((plotpos[:, 2] >= z - 0.5*dz)
                        & (plotpos[:, 2] < z + 0.5*dz))[0]
        points, = ax.plot(plotpos[ind, 0], plotpos[ind, 1],
                          ls="none", marker=",", c="blue")

        if color_parttype in pyl.arange(6):
            col_all_ind = pyl.arange(self.header.nall[0:color_parttype].sum(),
                                     self.header.nall[0:color_parttype].sum() +
                                     self.header.nall[color_parttype])
            col_ind = pyl.where(
                (plotpos[col_all_ind, 2] >= z - 0.5*dz) &
                (plotpos[col_all_ind, 2] < z + 0.5*dz))[0]
            col_points, =  ax.plot(
                plotpos[col_all_ind[col_ind], 0],
                plotpos[col_all_ind[col_ind], 1],
                ls="none", marker=",", c="red", mec="red")

        fig.show()

        key = "1"
        while(key != "0"):

            if key == "+":
                z += dz
            elif key == "-":
                z -= dz

            ind = pyl.where((plotpos[:, 2] >= z - 0.5*dz)
                            & (plotpos[:, 2] < z + 0.5*dz))[0]
            print("plotting", ind.size, "particles")
            points.set_xdata(plotpos[ind, 0])
            points.set_ydata(plotpos[ind, 1])

            if color_parttype in pyl.arange(6):
                col_ind = pyl.where((plotpos[col_all_ind, 2] >= z - 0.5*dz) &
                                    (plotpos[col_all_ind, 2] < z + 0.5*dz))[0]
                col_points.set_xdata(plotpos[col_all_ind[col_ind], 0])
                col_points.set_ydata(plotpos[col_all_ind[col_ind], 1])

            fig.canvas.draw()

            print("press +/- to walk in z direction or 0 to exit.")
            key = input()

        plotpos = 0

    # plot particles in 3D
    def plotparts3D(self, parttype=-1, ms=0.1):
        plotpos = rs.read_block(self.snapname, "POS ", parttype=parttype)
        if self.mpc_units:
            plotpos *= 1000.0

        # from mpl_toolkits.mplot3d import Axes3D

        fig = pyl.figure()  # stuff for 3D plots
        ax = fig.gca(projection='3d')
        ax.plot(plotpos[:, 0], plotpos[:, 1], plotpos[:, 2], "r.", ms=ms)
        plotpos = 0
        fig.show()

    # plots particle species
    def density_map(self, parttype=-1, proj="xy", ngrid=256, center=None,
                    dxyimg="none", dzimg="none", show=True):

        if center is None:
            center = pyl.array([0.5*self.box_sidelength,
                                0.5*self.box_sidelength,
                                0.5*self.box_sidelength])
        if dxyimg == "none":
            dxyimg = 0.5*self.box_sidelength
        if dzimg == "none":
            dzimg = 0.5*self.box_sidelength

        try:
            self.mass
        except AttributeError:
            print("Loading masses...")
            self.load_mass()

        try:
            self.pos
        except AttributeError:
            print("Loading positions...")
            self.load_pos()

        if proj == "xy":
            plotpos_z = self.nearest_dist_1D(self.pos[:, 2], center[2])
            ind = pyl.where(plotpos_z < dzimg)[0]
            plotpos_x = self.pos[ind, 0]
            centerx = center[0]
            plotpos_y = self.pos[ind, 1]
            centery = center[1]

        elif proj == "yz":
            plotpos_z = self.nearest_dist_1D(self.pos[:, 0], center[0])
            ind = pyl.where(plotpos_z < dzimg)[0]
            plotpos_x = self.pos[ind, 1]
            centerx = center[1]
            plotpos_y = self.pos[ind, 2]
            centery = center[2]

        elif proj == "xz":
            plotpos_z = self.nearest_dist_1D(self.pos[:, 1], center[1])
            ind = pyl.where(plotpos_z < dzimg)[0]
            plotpos_x = self.pos[ind, 0]
            centerx = center[0]
            plotpos_y = self.pos[ind, 2]
            centery = center[2]
        else:
            assert False

        from projectparticles.put_grid import put_grid_cic2D

        if (centerx != 0.5*self.box_sidelength or
                centery != 0.5*self.box_sidelength):
            plotpos_x = self.rel_pos(plotpos_x, centerx)
            centerx = 0
            plotpos_y = self.rel_pos(plotpos_y, centery)
            centery = 0
            dens_map = put_grid_cic2D(
                ngrid, 0, 0, 2*dxyimg, plotpos_x, plotpos_y, self.mass[ind], 0)
        else:
            dens_map = put_grid_cic2D(ngrid, centerx, centery, 2*dxyimg,
                                      plotpos_x, plotpos_y, self.mass[ind], 1)

        # such that x-coordinate correspond to x-axis
        # and y-coordinate correspond to y-axis.
        dens_map = dens_map.transpose()

        if show:
            fig = pyl.figure()
            ax = fig.add_subplot(111, aspect="equal")
            ax.imshow(pyl.log(dens_map), aspect="equal", origin="lower",
                      extent=(centerx-dxyimg, centerx+dxyimg,
                              centery-dxyimg, centery+dxyimg))
            ax.set_xlabel("comoving distance [kpc/h]")
            ax.set_ylabel("comoving distance [kpc/h]")
            fig.show()
        else:
            return dens_map

    # -------------------------------------------------------------
    # MAKE MAPS

    # slice
    def get_gas_slice_voronoi(self, sidelength=0, group=-1, cenx=0, ceny=0,
                              zslice=0, npix=512, a_z=0, a_x=0, a_z2=0,
                              filebase="none", save_slices=[], name_suffix=""):
        from get_slice import get_slice

        if sidelength == 0:
            sidelength = self.box_sidelength
            cenx = 0.5*self.box_sidelength
            ceny = 0.5*self.box_sidelength
            zslice = 0.5*self.box_sidelength

        plotpos = rs.read_block(self.snapname, "POS ", parttype=0)
        if self.mpc_units:
            plotpos *= 1000.0

        if group >= 0:
            cenx = self.cat.group_pos[group, 0]
            ceny = self.cat.group_pos[group, 1]
            zslice = self.cat.group_pos[group, 2]

        cenvec = pyl.array([cenx, ceny, zslice])

        mapind = get_slice(plotpos[:, 0], plotpos[:, 1], plotpos[:, 2],
                           self.header.nall[0], self.box_sidelength,
                           zslice, cenx, ceny, sidelength,
                           a_z*pyl.pi, a_x*pyl.pi, a_z2*pyl.pi, npix, 0, 0)

        zthick = 0.0
        mappars = pyl.array([cenx, ceny, zslice, sidelength,
                             zthick, a_z, a_x, a_z2], dtype=pyl.float64)

        if filebase != "none":
            for curslice in save_slices:
                is_vector = False

                if curslice == "density":
                    try:
                        self.rho
                    except AttributeError:
                        self.load_rho()

                    curmap = self.rho[mapind]
                    curname = "density"

                elif curslice == "temperature":
                    try:
                        self.temp
                    except AttributeError:
                        self.get_temp()

                    curmap = self.temp[mapind]
                    curname = "temperature"

                elif curslice == "velocity":  # peculiar velocity
                    try:
                        self.vel
                    except AttributeError:
                        self.load_vel()

                    curmap = self.vel
                    if group >= 0:
                        curmap -= self.cat.sub_vel[
                                            self.cat.group_firstsub[group]]
                    curmap = pyl.sqrt((curmap**2).sum(axis=1))[mapind]

                    mapind1d = pyl.unique(mapind)
                    curpos = plotpos[mapind1d]
                    curvec = self.vel[mapind1d]
                    if group >= 0:
                        curvec -= self.cat.sub_vel[
                                        self.cat.group_firstsub[group]]
                    curname = "velocity"

                    is_vector = True

                elif curslice == "bfield":
                    try:
                        self.bfld
                    except AttributeError:
                        self.load_bfld()

                    curmap = pyl.sqrt((self.bfld**2).sum(axis=1))[mapind]

                    mapind1d = pyl.unique(mapind)
                    curpos = plotpos[mapind1d]
                    curvec = self.bfld[mapind1d]
                    curname = "bfield"

                    is_vector = True

                else:
                    assert False

                curname = filebase + curname

                if name_suffix != "":
                    curname = curname + "_" + name_suffix

                if is_vector:
                    curpos = curpos - cenvec
                    if (a_z != 0 or a_x != 0 or a_z2 != 0):
                        # rotates coordinate frame / camera
                        rotmat = rot_matrix(
                            a_z*pyl.pi, a_x*pyl.pi, a_z2*pyl.pi)
                        curpos = pyl.dot(curpos, rotmat.transpose())
                        curvec = pyl.dot(curvec, rotmat.transpose())

                    pyl.savez(curname+"_vec", pos=curpos,
                              vec=curvec, mappars=mappars)

                pyl.savez(curname+"_slice", maparr=curmap, mappars=mappars)

        return mapind

    # gas projection
    def get_gas_projection(self, sidelength=0, group=-1, subhalo=-1,
                           cenx=0, ceny=0, cenz=0, npix=512,
                           zthick=0, a_z=0, a_x=0, a_z2=0, zidx=0, zvec=None,
                           use_vol=False, ngb=64.0, save_maps=[], hdf5=False,
                           save_BHprops=False, outdir="",
                           filebase="map_", name_suffix="",
                           nthreads=4, highres_only=False, phase_filter=False,
                           chandra_band=False,
                           use_sim_metallicities=False, angular=False):
        """
        Saves maps of temperature, density, metallicity or Compton y
        Args:
            sidelength: length of map on each side in kpc/h
            group: the FOF group number to centre map on, if given
            subhalo: subhalo number to centre map on, if given
            cen: [ckpc/h] centre of map relative to group or subhalo if given
                 otherwise in box coordinates.
            npix: number of pixels on a side
            zthick: total depth of integration (0.5*zthick in front and behind)
            a_z, a_x, a_z2: angles of the rotation matrix in pi radians
            zidx: index of axis along which to project
                  (not compatible with a_x etc)
            use_vol: estimate smoothing length from cell volume
            save_maps: list of maps to save:
                "temperature", "density", "metallicity", "SZ"
            filebase: start of output filename e.g. "map_grp0_"
            name_suffix: suffix added to output filename
            phase_filter: filter out cold and multiphase gas
            highres_only: include only high-res gas if zoom sim
            angular: account for the change in angular extent of cells that are
                closer/further away from the observer than the centre is
        """
        from projectparticles.put_grid import put_grid
        if hdf5:
            import h5py

        have_dens = False

        if outdir is not "" and outdir[-1] != "/":
            outdir += "/"

        if sidelength == 0:  # use whole box
            sidelength = self.box_sidelength
            cenx = 0.5*self.box_sidelength
            ceny = 0.5*self.box_sidelength
            cenz = 0.5*self.box_sidelength

        if sidelength == self.box_sidelength:
            periodic = True
        else:
            periodic = False

        if zthick == 0:  # integrate along whole box
            print("Setting zthick to box length", self.box_sidelength,
                  "ckpc/h")
            zthick = self.box_sidelength

        # load a copy of gas positions
        plotpos = rs.read_block(self.snapname, "POS ", parttype=0)
        if self.mpc_units:  # convert Mpc/h to kpc/h
            plotpos *= 1000.0

        M500 = 0.0
        R500 = 0.0
        if group >= 0:
            cenx += self.cat.group_pos[group, 0]
            ceny += self.cat.group_pos[group, 1]
            cenz += self.cat.group_pos[group, 2]
            M500 = self.cat.group_m_crit500[group]
            R500 = self.cat.group_r_crit500[group]
        elif subhalo >= 0:
            cenx += self.cat.sub_pos[subhalo, 0]
            ceny += self.cat.sub_pos[subhalo, 1]
            cenz += self.cat.sub_pos[subhalo, 2]

        # set plotpos relative to centre
        plotpos[:, 0] -= cenx
        plotpos[:, 1] -= ceny
        plotpos[:, 2] -= cenz

        box_wrap_diff(plotpos, self.box_sidelength)

        t = self.header.time
        z = self.header.redshift
        h = self.header.hubble

        if (a_z != 0 or a_x != 0 or a_z2 != 0):
            if zidx != 0:
                raise NotImplementedError(
                    "Cannot set both non-zero zidx and rotation matrix.")
            if zvec is not None:
                raise NotImplementedError(
                    "Cannot set both rotation matrix and projection vector.")
            # rotates coordinate frame / camera
            rotmat = rot_matrix(a_z*pyl.pi, a_x*pyl.pi, a_z2*pyl.pi)
            plotpos = pyl.dot(plotpos, rotmat.transpose())
            # e.g. a_x=0.5 with zidx=0 is equivalent to zidx=1

        axes = [0, 1, 2]
        if zidx not in axes:
            raise ValueError("zidx must be one of [0,1,2]")
        axes.remove(zidx)  # indices of projection axes
        print("axes =", axes)

        if zvec is not None:
            normal = [0, 0, 0]
            normal[zidx] = 1
            # Get rotation matrix that transforms a unit vector to zvec
            rotmat = R_2vect(normal, zvec)
            # Rotate coordinate system
            plotpos = pyl.dot(plotpos, rotmat)

        ind = pyl.where((plotpos[:, zidx] > -0.5*zthick)
                        & (plotpos[:, zidx] < 0.5*zthick))[0]
        # exclude gas below 3e4 K and multiphase (star-forming) gas
        if phase_filter:
            if not hasattr(self, "gasrho"):
                self.load_gasrho()
            if not hasattr(self, "temp"):
                self.get_temp()
            import scaling.xray_python.xray as xray
            src = xray.xray_source(z, h, self.header.omega_m)
            curind = src.filter_phase_space(self.temp, self.gasrho,
                                            filter_type="default",
                                            M500=M500, R500=R500)
            ind = pyl.intersect1d(ind, curind, assume_unique=True)
        if highres_only:
            if not hasattr(self, "gasmass"):
                self.load_gasmass()
            self.load_highresgasmass()
            ind = pyl.intersect1d(ind, pyl.where(
                self.hrgm > 0.5*self.gasmass)[0], assume_unique=True)
        print("using", len(ind), "particles.")
        assert len(ind) > 0, "No particles in specified region."

        plotpos = plotpos[ind]

        if not use_vol:
            if not hasattr(self, "hsml"):
                self.load_hsml()

            hsml = self.hsml[ind]
        else:
            if hasattr(self, "vol"):
                hsml = (self.vol[ind]*ngb / (4.0*pyl.pi/3.0))**(1.0/3.0)
            else:
                try:
                    self.load_vol()
                    hsml = (self.vol[ind]*ngb / (4.0*pyl.pi/3.0))**(1.0/3.0)
                except KeyError:
                    if not hasattr(self, "gasmass"):
                        self.load_gasmass()
                    if not hasattr(self, "gasrho"):
                        self.load_gasrho()
                    hsml = (self.gasmass[ind] / self.gasrho[ind]
                            * ngb / (4.0*pyl.pi/3.0))**(1.0/3.0)

        if angular:
            # Cells are at different distances from observer so the angle
            # subtended by the cell's smoothing length will vary slightly.
            # We multiply hsml by the ratio of the (approx) comoving
            # distance at the cell position to the comoving distance at
            # the centre. This is equivalent to dividing the comoving
            # hsml by ckpc/" to get it in arcsec then multiplying by ckpc/"
            # at the cell's distance.
            if z > 0.001:
                hsml *= (self.cm.r_comoving(z) +
                         plotpos[:, zidx]/1000.0) / self.cm.r_comoving(z)
            else:
                raise ValueError("ERROR: redshift {:.1e} too small "
                                 "for angular=True".format(z))

        if hdf5:
            with h5py.File(outdir+filebase+name_suffix+"_proj.hdf5", "w") as f:
                f.attrs['h'] = h
                f.attrs['z'] = z
                f.attrs['omega_m'] = self.header.omega_m
                f.attrs['omega_l'] = self.header.omega_l
                f.attrs['ckpch_arcsec'] = self.cm.arcsec_to_comov(
                    ang=1, z=z) * 1000.0
                f.attrs['pkpc_arcsec'] = self.cm.arcsec_to_phys(
                    ang=1, z=z) * 1000.0 / h
                f.attrs['centre_ckpch'] = [cenx, ceny, cenz]
                f.attrs['sidelength_ckpch'] = sidelength
                f.attrs['sidelength_pkpc'] = sidelength / h / (1.0+z)
                f.attrs['sidelength_arcsec'] = sidelength / \
                    1000.0 / self.cm.arcsec_to_comov(ang=1, z=z)
                f.attrs['npix'] = npix
                f.attrs['pixel_size_pkpc'] = sidelength / h / (1.0+z) / npix
                f.attrs['pixel_size_arcsec'] = sidelength / 1000.0 / \
                    self.cm.arcsec_to_comov(ang=1, z=z) / npix
                f.attrs['zthick_ckpch'] = zthick
                f.attrs['a_z'] = a_z
                f.attrs['a_xz'] = a_x
                f.attrs['a_z2'] = a_z2
                if group >= 0:
                    f.attrs['group'] = group
                    f.attrs['M500_e10Msunh'] = M500
                    f.attrs['M500_Msun'] = M500 * 1e10 / h
                    f.attrs['R500_ckpch'] = R500
                    f.attrs['R500_pkpc'] = R500 / (1.0+z) / h
                if subhalo >= 0:
                    f.attrs['subhalo'] = subhalo
                f.attrs['use_vol'] = use_vol
                f.attrs['ngb'] = ngb
                f.attrs['phase_filter'] = phase_filter
                f.attrs['highres_only'] = highres_only
                f.attrs['use_sim_metallicities'] = use_sim_metallicities

                if save_BHprops:
                    if group >= 0:
                        sub = self.cat.group_firstsub[group]
                    elif subhalo >= 0:
                        sub = subhalo
                    else:
                        raise IOError("Must have group or subhalo >=0 "
                                      "to save BH properties.")
                    self.get_sub_bh_props(save_pos=True)
                    f.attrs['BHmass_Msun'] = self.sub_bhmass1[sub] * 1e10 / h
                    f.attrs['BHmdot_Msun_yr'] = self.sub_bhmassdot1[sub] * \
                        1e10 / h / (self.unittime_in_s / 3600.0 / 24 / 365.25)
                    f.attrs['BHpos_ckpch'] = self.sub_bhpos1[sub]
                    if self.sub_bhrad1[sub] > sidelength:
                        raise ValueError("Most massive BH in subhalo is "
                                         "outside FoV.")

        for curmap in save_maps:
            if curmap == "density":
                if not hasattr(self, "gasmass"):
                    self.load_gasmass()

                densmap = put_grid(npix, 0, 0, sidelength,
                                   plotpos[:, axes[0]], plotpos[:, axes[1]],
                                   hsml, self.gasmass[ind], periodic,
                                   num_threads=nthreads)
                have_dens = True

                curmap = densmap
                curname = "density"

            elif curmap == "temperature":
                if not hasattr(self, "temp"):
                    self.get_temp()

                if not hasattr(self, "gasmass"):
                    self.load_gasmass()

                if not have_dens:
                    densmap = put_grid(npix, 0, 0, sidelength,
                                       plotpos[:, axes[0]],
                                       plotpos[:, axes[1]], hsml,
                                       self.gasmass[ind], periodic,
                                       num_threads=nthreads)
                    have_dens = True

                curmap = put_grid(npix, 0, 0, sidelength, plotpos[:, axes[0]],
                                  plotpos[:, axes[1]], hsml,
                                  self.temp[ind]*self.gasmass[ind], periodic,
                                  num_threads=nthreads)

                indpix = pyl.where(densmap > 0)
                curmap[indpix] /= densmap[indpix]
                curname = "temperature"

            elif curmap == "metallicity":
                if not hasattr(self, "Zgas"):
                    self.load_Zgas()

                if not hasattr(self, "gasmass"):
                    self.load_gasmass()

                if not have_dens:
                    densmap = put_grid(npix, 0, 0, sidelength,
                                       plotpos[:, axes[0]],
                                       plotpos[:, axes[1]], hsml,
                                       self.gasmass[ind], periodic,
                                       num_threads=nthreads)
                    have_dens = True

                curmap = put_grid(npix, 0, 0, sidelength, plotpos[:, axes[0]],
                                  plotpos[:, axes[1]], hsml,
                                  self.Zgas[ind]*self.gasmass[ind], periodic,
                                  num_threads=nthreads)

                indpix = pyl.where(densmap > 0)
                curmap[indpix] /= densmap[indpix]
                curname = "metallicity"

            elif curmap == "SZ":
                if not hasattr(self, "yA"):
                    self.get_SZ_yA()  # units physical m^2, h of sim included

                # Dimensionless units:
                # Physical length of pixel at centre (redshift z) in pkpc
                L = sidelength / h / (1+z) / npix
                print("L =", L)
                print("self.yA[ind] / L**2 =", self.yA[ind] / L**2 / kpc**2)
                if angular:
                    # We want the physical length of the pixel at the angular
                    # diameter distance of the CELL, so similarly to the hsml
                    # above, we multiply L (pixel length at centre) by the
                    # ratio of the (approx) comoving distance at the cell
                    # position to the comoving distance at the centre.
                    # The factor 1/(1+z) to convert to physical has already
                    # been applied to L physical length (pkpc) of pixel at
                    # the angular diameter distance to cell.
                    L *= (self.cm.r_comoving(z) +
                          plotpos[:, zidx]/1000.0) / self.cm.r_comoving(z)
                    print("L =", L)
                    print("max L =", max(L))
                    print("min L =", min(L))
                    print("self.yA[ind] / L**2 =",
                          self.yA[ind] / L**2 / kpc**2)
                curmap = put_grid(npix, 0, 0, sidelength,
                                  plotpos[:, axes[0]], plotpos[:, axes[1]],
                                  hsml, self.yA[ind] / L**2 / kpc**2, periodic,
                                  num_threads=nthreads)

                curname = "SZ"
                curunits = "dimensionless Compton y"

            elif curmap == "xray":
                import scaling.xray_python.xray as xray
                if chandra_band:
                    src = xray.xray_source(
                            z, h, self.header.omega_m,
                            fluxdir="/data/vault/nh444/ObsData/"
                                    "flux_tables_apec_05-7keV/")
                else:
                    src = xray.xray_source(z, h, self.header.omega_m)

                ag_hydrogen = 0.70683  # Anders & Grevesse solar mass fractions
                ag_helium = 0.27431
                ag_metals = 0.01886

                if not hasattr(self, "temp"):
                    self.get_temp()

                if not hasattr(self, "gasmass"):
                    self.load_gasmass()

                if not hasattr(self, "gasrho"):
                    self.load_gasrho()

                if not hasattr(self, "ne"):
                    self.load_ne()

                if use_sim_metallicities:
                    if not hasattr(self, "Zgas"):
                        self.load_Zgas()
                    abund = self.Zgas[ind] / (1-self.Zgas[ind]) * \
                        (ag_hydrogen+ag_helium)/ag_metals
                else:  # assume 0.3 solar metallicity
                    abund = pyl.full_like(self.temp[ind], 0.3)

                xray_flux = src.get_flux_from_table(
                    self.temp[ind], abund, self.ne[ind], self.gasmass[ind],
                    self.gasrho[ind], modelname="apec", f=fhydrogen)

                # We want to filter out cold and multiphase gas
                # even if phase_filter is False.
                # Exclude gas below 3e4 K and multiphase (star-forming) gas.
                if not phase_filter:
                    ind2 = src.filter_phase_space(
                        self.temp[ind], self.gasrho[ind],
                        filter_type="default", M500=M500, R500=R500)
                    curmap = put_grid(npix, 0, 0, sidelength,
                                      plotpos[ind2, axes[0]],
                                      plotpos[ind2, axes[1]], hsml[ind2],
                                      xray_flux[ind2], periodic,
                                      num_threads=nthreads)
                else:
                    curmap = put_grid(npix, 0, 0, sidelength,
                                      plotpos[:, axes[0]], plotpos[:, axes[1]],
                                      hsml, xray_flux, periodic,
                                      num_threads=nthreads)

                curname = "xray"
                curunits = "erg/s/cm^2"

            elif curmap == "SFR":
                if not hasattr(self, "sfr"):
                    self.load_sfr()

                curmap = put_grid(npix, 0, 0, sidelength, plotpos[:, axes[0]],
                                  plotpos[:, axes[1]], hsml, self.sfr[ind],
                                  periodic, num_threads=nthreads)

                curname = "SFR"
                # Units have no h dependence since mass and time
                # have opposite h dependence.
                curunits = "Msun per yr"

            # elif curmap == "velocity": # peculiar velocity
                # try:
                # self.vel
                # except AttributeError:
                # self.load_vel()

                # curmap = self.vel[mapind]
                # if group >= 0:
                # curmap -= self.cat.sub_vel[self.cat.group_firstsub[group]]
                # curmap = pyl.sqrt((curmap**2).sum(axis=1))

                # mapind1d = pyl.unique(mapind)
                # curpos = plotpos[mapind1d]
                # curvec = self.vel[mapind1d]
                # if group >= 0:
                # curvec -= self.cat.sub_vel[self.cat.group_firstsub[group]]
                # curname = "velocity"

                # is_vector = True

            # elif curmap == "bfield":
                # try:
                # self.bfld
                # except AttributeError:
                # self.load_bfld()

                # curmap = pyl.sqrt((self.bfld[mapind]**2).sum(axis=1))

                # mapind1d = pyl.unique(mapind)
                # curpos = plotpos[mapind1d]
                # curvec = self.bfld[mapind1d]
                # curname = "bfield"

                # is_vector = True

            else:
                raise ValueError("map '"+str(curmap)+"' not recognised.")

            if hdf5:
                fname = outdir+filebase+name_suffix+"_proj.hdf5"
                with h5py.File(fname, "r+") as f:
                    dset = f.create_dataset(curname, data=curmap)
                    dset.attrs['units'] = curunits
            else:
                mappars = pyl.array([cenx, ceny, cenz, sidelength, zthick,
                                     a_z, a_x, a_z2, t, z, h, M500, R500],
                                    dtype=pyl.float64)
                pyl.savez(outdir+filebase+curname+name_suffix +
                          "_proj", maparr=curmap, mappars=mappars)
                print("Saved map to", outdir + filebase + curname +
                      name_suffix + "_proj.npz")

            print("Sum, min, max =", pyl.sum(curmap), curmap.min(),
                  curmap.max())
        if hdf5:
            print("Saved map to", outdir+filebase+name_suffix+"_proj.hdf5")

    def get_stellar_projection(self, sidelength=0, group=-1, subhalo=-1,
                               bound_only=True,
                               cenx=0, ceny=0, cenz=0,
                               zthick=0, npix=512, a_z=0, a_x=0, a_z2=0,
                               zidx=0, CiC=False, ngb=16,
                               save_maps=["mass"],
                               outdir="", filebase="map_stellar_",
                               name_suffix="", nthreads=4):
        """
        Saves maps of stellar mass, flux
        Args:
            sidelength: length of map on each side in kpc/h
            group: the FOF group number to centre map on, if given
            subhalo: subhalo number to centre map on, if given
            cen: centre used if group<0 and subhalo<0 (in ckpc/h)
            zthick: total depth of integration (0.5*zthick in front and behind)
            npix: number of pixels on a side
            a_z, a_x, a_z2: angles of the rotation matrix in pi radians
            zidx: axis along which to project (not compatible with a_x etc)
            CiC: use cloud-in-cell projection, otherwise smooth over kernel
            ngb: number of neighbours for smoothing kernel
            save_maps: list of maps to save:
                "temperature", "density", "metallicity", "SZ"
            filebase: start of output filename e.g. "map_grp0_"
            name_suffix: suffix added to output filename
        """
        from projectparticles.put_grid import put_grid
        from projectparticles.put_grid import put_grid_cic2D
        from scipy.spatial import cKDTree

        if outdir is not "" and outdir[-1] != "/":
            outdir += "/"

        if sidelength == 0:  # use whole box
            sidelength = self.box_sidelength
            cenx = 0.5*self.box_sidelength
            ceny = 0.5*self.box_sidelength
            cenz = 0.5*self.box_sidelength

        if sidelength == self.box_sidelength:
            periodic = True
        else:
            periodic = False

        if zthick == 0:  # integrate along whole box
            print("Setting zthick to box length", self.box_sidelength,
                  "ckpc/h")
            zthick = self.box_sidelength

        # load a copy of star positions
        self.load_starpos()
        # GFM_StellarFormationTime
        age = rs.read_block(self.snapname, "GAGE",
                            cosmic_ray_species=self.num_cr_species)
        # birth time <=0 is a wind particle (=0 if just recoupled)
        starind = pyl.where(age > 0.0)[0]
        plotpos = self.starpos[starind]

        M500 = 0.0
        R500 = 0.0
        if group >= 0:  # centre of map is centre of given group
            cenx = self.cat.group_pos[group, 0]
            ceny = self.cat.group_pos[group, 1]
            cenz = self.cat.group_pos[group, 2]
            M500 = self.cat.group_m_crit500[group]
            R500 = self.cat.group_r_crit500[group]
        elif subhalo >= 0:
            cenx = self.cat.sub_pos[subhalo, 0]
            ceny = self.cat.sub_pos[subhalo, 1]
            cenz = self.cat.sub_pos[subhalo, 2]

        # set plotpos relative to centre
        plotpos[:, 0] -= cenx
        plotpos[:, 1] -= ceny
        plotpos[:, 2] -= cenz

        box_wrap_diff(plotpos, self.box_sidelength)

        t = self.header.time
        z = self.header.redshift
        h = self.header.hubble

        if (a_z != 0 or a_x != 0 or a_z2 != 0):
            assert zidx == 0, "Cannot set both zidx and rotation matrix"
            # rotates coordinate frame / camera
            rotmat = rot_matrix(a_z*pyl.pi, a_x*pyl.pi, a_z2*pyl.pi)
            plotpos = pyl.dot(plotpos, rotmat.transpose())
            # e.g. a_x=0.5 with zidx=0 is equivalent to zidx=1

        axes = [0, 1, 2]
        axes.remove(zidx)  # indices of projection axes
        print("axes =", axes)

        ind = pyl.where((plotpos[:, zidx] > -0.5*zthick)
                        & (plotpos[:, zidx] < 0.5*zthick))[0]

        plotpos = plotpos[ind]
        # Cannot give negative positions to scipy cKDTree
        kdtree_star = cKDTree(self.starpos[starind[ind]], leafsize=5,
                              boxsize=self.box_sidelength, balanced_tree=True,
                              compact_nodes=True)
        d, i = kdtree_star.query(self.starpos[starind[ind]], k=ngb,
                                 distance_upper_bound=max(sidelength, zthick))
        hsml = pyl.asarray([dist[-1] for dist in d])

        for curname in save_maps:
            if curname == "mass":
                if not hasattr(self, "starmass"):
                    self.load_starmass()

                if CiC:
                    densmap = put_grid_cic2D(npix, 0, 0, sidelength,
                                             plotpos[:, axes[0]],
                                             plotpos[:, axes[1]],
                                             self.starmass[starind[ind]],
                                             periodic)
                else:
                    densmap = put_grid(npix, 0, 0, sidelength,
                                       plotpos[:, axes[0]],
                                       plotpos[:, axes[1]],
                                       hsml, self.starmass[starind[ind]],
                                       periodic, num_threads=nthreads)

                curmap = densmap
            elif curname[-4:] == "band":
                band = curname[:-4]
                # if band=="U":
                # idx = 0
                # elif band=="B":
                # idx = 1
                # elif band=="V":
                # idx = 2
                # elif band=="K":
                # idx = 3
                if band == "g":
                    idx = 4
                elif band == "r":
                    idx = 5
                elif band == "i":
                    idx = 6
                elif band == "z":
                    idx = 7
                else:
                    raise ValueError("band '"+str(band)+"' not recognised.")

                # Load GFM stellar photometrics
                phot = rs.read_block(self.snapname, "PHOT",
                                     cosmic_ray_species=self.num_cr_species)
                mag = phot[:, idx]
                # convert AB magnitudes (g,r,i,z) to flux in Janskys
                f = 10**(mag / -2.5) * 3631.0

                if CiC:
                    curmap = put_grid_cic2D(npix, 0, 0, sidelength,
                                            plotpos[:, axes[0]],
                                            plotpos[:, axes[1]],
                                            f[starind[ind]], periodic)
                else:
                    curmap = put_grid(npix, 0, 0, sidelength,
                                      plotpos[:, axes[0]], plotpos[:, axes[1]],
                                      hsml, f[starind[ind]], periodic,
                                      num_threads=nthreads)
            else:
                raise ValueError("map '"+str(curname)+"' not recognised.")

            print("Saving...")
            mappars = pyl.array([cenx, ceny, cenz, sidelength, zthick,
                                 a_z, a_x, a_z2, t, z, h, M500, R500],
                                dtype=pyl.float64)
            pyl.savez(outdir+filebase+curname+name_suffix +
                      "_proj", maparr=curmap, mappars=mappars)
            print("Saved map to", outdir + filebase + curname + name_suffix +
                  "_proj.npz")

    # -------------------------------------------------------------
    # SHOW PHASE DIAGRAM

    # Plot gas density vs temperature.
    def show_rho_T_plot(self, inKelvin=True, useax=False):
        try:
            self.rho.size
        except AttributeError:
            self.load_rho()

        try:
            self.temp.size
        except AttributeError:
            self.get_temp()

        if inKelvin:
            tempnorm = keV/kboltzmann
        else:
            tempnorm = 1.0

        if not useax:
            fig = pyl.figure()
            ax = fig.add_subplot(111)
        else:
            ax = useax

        try:
            ax.loglog(self.rho/self.rho_mean_baryons, self.temp *
                      tempnorm, linestyle="none", marker=",")
        except AttributeError:
            ax.loglog(self.rho, self.temp*tempnorm,
                      linestyle="none", marker=",")

        if not useax:
            fig.show()

    # Plot gas density vs internal energy.
    def show_rho_U_plot(self):
        try:
            self.rho.size
        except AttributeError:
            self.load_rho()

        try:
            self.u.size
        except AttributeError:
            self.load_u()

        fig = pyl.figure()
        ax = fig.add_subplot(111)
        ax.loglog(self.rho, self.u, linestyle="none", marker=".")
        fig.show()

    def show_gas_pdfs(self, plottype, inKelvin=True, useax=False, kwargs=0):
        try:
            self.mass.size
        except AttributeError:
            self.load_mass()

        try:
            self.rho.size
        except AttributeError:
            self.load_rho()

        if plottype == "rho":
            quantity = pyl.log10(self.rho/self.rho_mean_baryons)
            curvename = "density PDF"
            bin_edges = pyl.linspace(-2, 8, 200)

        elif plottype == "temp":
            try:
                self.temp.size
            except AttributeError:
                self.get_temp()

            if inKelvin:
                tempnorm = keV/kboltzmann
            else:
                tempnorm = 1.0

            quantity = pyl.log10(self.temp*tempnorm)
            curvename = "temperature PDF"
            bin_edges = pyl.linspace(0, 9, 200)

        else:
            assert False

        if not useax:
            fig = pyl.figure()
            ax = fig.add_subplot(111)
        else:
            ax = useax

        dist_m, bin_edges_m = pyl.histogram(
            quantity, bins=bin_edges, weights=self.mass[0:self.header.nall[0]])
        mgas = self.mass[self.species_start[0]:self.species_end[0]].sum(
                dtype=pyl.float64)
        mstar = self.mass[self.species_start[4]:self.species_end[4]].sum(
                dtype=pyl.float64)
        dist_m /= (mgas + mstar)

        dist_v, bin_edges_v = pyl.histogram(
                quantity, bins=bin_edges,
                weights=self.mass[0:self.header.nall[0]]/self.rho)
        dist_v /= (self.mass[0:self.header.nall[0]] /
                   self.rho).sum(dtype=pyl.float64)

        assert (bin_edges_m == bin_edges_v).all()
        bin_centers = 0.5*bin_edges_m[1:] + 0.5*bin_edges_m[0:-1]

        if useax:
            if kwargs == 0:
                kwargs = dict()

            if "label" in kwargs:
                kwargs["label"] + " " + curvename
            else:
                kwargs["label"] = curvename

            curname = kwargs["label"]
            kwargs["label"] = curname+" mass-w."
            ax.plot(bin_centers, dist_m, **kwargs)
            kwargs["label"] = curname+" vol.-w."
            ax.plot(bin_centers, dist_v, **kwargs)

        else:
            ax.plot(bin_centers, dist_m, c="red", label=curvename+" mass-w.")
            ax.plot(bin_centers, dist_v, c="blue", label=curvename+" vol.-w.")

            ax.legend()

            fig.show()

        return dist_m, dist_v, bin_centers

    def find_eos(self, Delta_eos):
        print("finding EOS at Delta =", Delta_eos)

        from sample_mode import sample_mode

        rho_eos = Delta_eos
        logrho_eos = pyl.log10(rho_eos)
        Delta = self.rho / self.rho_mean_baryons
        temp = self.temp_kelvin

        dlogrho_eos = pyl.log10(1.25)
        dlogrho = pyl.log10(1.05)

        log_rho_eos = pyl.log10(rho_eos)

        logrho1 = log_rho_eos - dlogrho_eos
        logrho2 = log_rho_eos + dlogrho_eos

        cur_gamma_minus_1_old = []
        cur_gamma_minus_1 = 1.0
        cur_gamma_minus_1_new = 0.0

        ind1 = pyl.where((pyl.log10(Delta) > logrho1-dlogrho)
                         & (pyl.log10(Delta) < logrho1+dlogrho))[0]
        logparttemp1 = pyl.log10(temp[ind1])
        logpartrho1 = pyl.log10(Delta[ind1])

        ind2 = pyl.where((pyl.log10(Delta) > logrho2-dlogrho)
                         & (pyl.log10(Delta) < logrho2+dlogrho))[0]
        logparttemp2 = pyl.log10(temp[ind2])
        logpartrho2 = pyl.log10(Delta[ind2])

        niter = 0
        while (abs(cur_gamma_minus_1_new-cur_gamma_minus_1) > 1.0e-6):
            cur_gamma_minus_1 = cur_gamma_minus_1_new

            reslogparttemp1 = logparttemp1 - \
                cur_gamma_minus_1*(logpartrho1 - logrho1)
            logtemp1 = sample_mode(reslogparttemp1)

            reslogparttemp2 = logparttemp2 - \
                cur_gamma_minus_1*(logpartrho2 - logrho2)
            logtemp2 = sample_mode(reslogparttemp2)

            cur_gamma_minus_1_new = (logtemp2 - logtemp1)/(logrho2 - logrho1)

            niter += 1

            if niter > 20:
                if cur_gamma_minus_1_new in cur_gamma_minus_1_old:
                    cur_gamma_minus_1_old = pyl.array(cur_gamma_minus_1_old)
                    ind = pyl.where(cur_gamma_minus_1_old ==
                                    cur_gamma_minus_1_new)[0]
                    assert ind.size == 1
                    cur_gamma_minus_1_new = cur_gamma_minus_1_old[ind:].mean()
                    print("period of length",
                          cur_gamma_minus_1_old[ind:].__len__(), "detected")
                    break

                cur_gamma_minus_1_old.append(cur_gamma_minus_1_new)

        assert niter < 1000

        ind = pyl.where((pyl.log10(Delta) > logrho_eos-dlogrho)
                        & (pyl.log10(Delta) < logrho_eos+dlogrho))[0]
        temp0 = (10.0**sample_mode(
                    pyl.log10(temp[ind]) -
                    cur_gamma_minus_1_new*(pyl.log10(Delta[ind]) -
                                           logrho_eos)))

        print("EOS: T =", temp0, "* (Delta/" +
              str(Delta_eos)+")^"+str(cur_gamma_minus_1_new))

        return temp0, cur_gamma_minus_1_new

    # -------------------------------------------------------------
    # GET PROPERTIES OF SUBHALOS

    # Total star formation rate.
    def get_sub_sfr(self):
        try:
            self.hostsuhbalos
        except AttributeError:
            self.get_hostsubhalos()

        try:
            self.sfr
        except AttributeError:
            self.load_sfr()

        self.sub_sfr = (pyl.bincount(
            self.hostsubhalos[0:self.header.nall[0]]+2,
            weights=self.sfr, minlength=self.cat.nsubs+2))[2:]

    # Average metallicity weighted by star formation rate.
    def get_sub_Zgas(self):
        try:
            self.sub_sfr
        except AttributeError:
            self.get_sub_sfr()

        try:
            self.Zgas
        except AttributeError:
            self.load_Zgas()

        self.sub_Zgas = (pyl.bincount(
                self.hostsubhalos[0:self.header.nall[0]] + 2,
                weights=self.sfr*self.Zgas, minlength=self.cat.nsubs+2))[2:]

        ind = pyl.where(self.sub_sfr > 0)
        # SFR weighted metallicity (see e.g. Dave et al. 2011, MNRAS 416, 1354)
        self.sub_Zgas[ind] /= self.sub_sfr[ind]

    # Total H1 mass
    def get_sub_HI_mass(self):
        f = 0.76

        try:
            self.hostsuhbalos
        except AttributeError:
            self.get_hostsubhalos()

        try:
            self.sfr
        except AttributeError:
            self.load_sfr()

        try:
            self.mass
        except AttributeError:
            self.load_mass()

        ind = pyl.where(self.sfr > 0.0)[0]
        self.sub_HI_mass = (pyl.bincount(
            self.hostsubhalos[ind]+2, weights=self.mass[ind]*f,
            minlength=self.cat.nsubs+2))[2:]  # include all SF hydrogen

        try:
            self.nh
        except AttributeError:
            self.load_nh()

        ind = pyl.where(self.sfr == 0.0)[0]
        # include neutral part of other hydrogen
        self.sub_HI_mass += (pyl.bincount(
            self.hostsubhalos[ind]+2, weights=self.mass[ind]*f*self.nh[ind],
            minlength=self.cat.nsubs+2))[2:]

    # get properties of 3 most massive BHs in each subhalo
    # and the total BH mass and mdot.
    def get_sub_bh_props(self, save_pos=False):
        try:
            self.bhmass
        except AttributeError:
            self.load_bhmass()

        try:
            self.bhpos
        except AttributeError:
            self.load_bhpos()

        try:
            self.bhvel
        except AttributeError:
            self.load_bhvel()

        try:
            self.bhmassdot
        except AttributeError:
            self.load_bhmassdot()

        try:
            self.bhids
        except AttributeError:
            self.load_bhids()

        try:
            self.hostsubhalos
        except AttributeError:
            self.get_hostsubhalos()

        self.sub_bhmass = (pyl.bincount(
            self.hostsubhalos[self.species_start[5]:self.species_end[5]] + 2,
            weights=self.bhmass, minlength=self.cat.nsubs+2))[2:]
        self.sub_bhmassdot = (pyl.bincount(
            self.hostsubhalos[self.species_start[5]:self.species_end[5]] + 2,
            weights=self.bhmassdot, minlength=self.cat.nsubs+2))[2:]

        self.sub_bhmass1 = pyl.zeros(self.cat.nsubs)
        self.sub_bhmass2 = pyl.zeros(self.cat.nsubs)
        self.sub_bhmass3 = pyl.zeros(self.cat.nsubs)

        self.sub_bhmassdot1 = pyl.zeros(self.cat.nsubs)
        self.sub_bhmassdot2 = pyl.zeros(self.cat.nsubs)
        self.sub_bhmassdot3 = pyl.zeros(self.cat.nsubs)

        self.sub_bhids1 = pyl.zeros(self.cat.nsubs)
        self.sub_bhids2 = pyl.zeros(self.cat.nsubs)
        self.sub_bhids3 = pyl.zeros(self.cat.nsubs)

        if save_pos:
            self.sub_bhpos1 = pyl.zeros((self.cat.nsubs, 3))
            self.sub_bhpos2 = pyl.zeros((self.cat.nsubs, 3))
            self.sub_bhpos3 = pyl.zeros((self.cat.nsubs, 3))

        self.sub_bhrad1 = pyl.zeros(self.cat.nsubs)
        self.sub_bhrad2 = pyl.zeros(self.cat.nsubs)
        self.sub_bhrad3 = pyl.zeros(self.cat.nsubs)

        self.sub_bhvel1 = pyl.zeros(self.cat.nsubs)
        self.sub_bhvel2 = pyl.zeros(self.cat.nsubs)
        self.sub_bhvel3 = pyl.zeros(self.cat.nsubs)

        for i in pyl.arange(self.cat.nsubs):
            ind = pyl.where(self.hostsubhalos[
                    self.species_start[5]:self.species_end[5]] == i)[0]
            curbhmasses = self.bhmass[ind]  # bh masses
            # Get indices that sort bhmass array in ascending order.
            ind2 = curbhmasses.argsort()

            if ind.size >= 1:  # most massive BH in subhalo
                # last index is most massive BH
                self.sub_bhmass1[i] = curbhmasses[ind2[-1]]
                self.sub_bhmassdot1[i] = self.bhmassdot[ind[ind2[-1]]]
                self.sub_bhids1[i] = self.bhids[ind[ind2[-1]]]
                if save_pos:
                    self.sub_bhpos1[i] = self.bhpos[ind[ind2[-1]]]
                self.sub_bhrad1[i] = self.nearest_dist_3D(
                    self.bhpos[ind[ind2[-1]]], self.cat.sub_pos[i])
                # difference in peculiar velocity, should add Hubble flow
                self.sub_bhvel1[i] = self.nearest_dist_3D(
                    self.bhvel[ind[ind2[-1]]], self.cat.sub_vel[i])

            if ind.size >= 2:  # second most massive
                self.sub_bhmass2[i] = curbhmasses[ind2[-2]]
                self.sub_bhmassdot2[i] = self.bhmassdot[ind[ind2[-2]]]
                self.sub_bhids2[i] = self.bhids[ind[ind2[-2]]]
                if save_pos:
                    self.sub_bhpos2[i] = self.bhpos[ind[ind2[-2]]]
                self.sub_bhrad2[i] = self.nearest_dist_3D(
                    self.bhpos[ind[ind2[-2]]], self.cat.sub_pos[i])
                self.sub_bhvel2[i] = self.nearest_dist_3D(
                    self.bhvel[ind[ind2[-2]]], self.cat.sub_vel[i])

            if ind.size >= 3:  # third most massive
                self.sub_bhmass3[i] = curbhmasses[ind2[-3]]
                self.sub_bhmassdot3[i] = self.bhmassdot[ind[ind2[-3]]]
                self.sub_bhids3[i] = self.bhids[ind[ind2[-3]]]
                if save_pos:
                    self.sub_bhpos3[i] = self.bhpos[ind[ind2[-3]]]
                self.sub_bhrad3[i] = self.nearest_dist_3D(
                    self.bhpos[ind[ind2[-3]]], self.cat.sub_pos[i])
                self.sub_bhvel3[i] = self.nearest_dist_3D(
                    self.bhvel[ind[ind2[-3]]], self.cat.sub_vel[i])

    # save subhalo list
    def print_subhalo_properties(self, group=-1, target_file="none"):
        if target_file == "none":
            f = pyl.sys.stdout
        else:
            f = open(target_file, 'w')

        if group >= 0:
            firstsub = self.cat.group_firstsub[group]
            nsubs = self.cat.group_nsubs[group]
            print("# subhalo population for group", group, file=f)
        else:
            firstsub = 0
            nsubs = self.cat.nsubs
            print("# subhalo population of snapshot", file=f)

        print("# nsubs =", nsubs, file=f)

        try:
            self.hostsubhalos
        except AttributeError:
            self.get_hostsubhalos()

        isub = 0
        for i in range(firstsub, firstsub+nsubs):
            grnr = self.cat.sub_grnr[i]
            parent = self.cat.sub_parent[i]

            nall = self.cat.sub_len[i]
            mall = self.cat.sub_mass[i]

            ngas = self.cat.sub_lentab[i, 0]
            mgas = self.cat.sub_masstab[i, 0]

            ndm = self.cat.sub_lentab[i, 1]
            mdm = self.cat.sub_masstab[i, 1]

            ncont = self.cat.sub_lentab[i, 2]+self.cat.sub_lentab[i, 3]
            mcont = self.cat.sub_masstab[i, 2]+self.cat.sub_masstab[i, 3]

            nstars = self.cat.sub_lentab[i, 4]
            mstars = self.cat.sub_masstab[i, 4]

            if self.header.sfr == 1:
                try:
                    self.sub_sfr
                except AttributeError:
                    self.get_sub_sfr()
                sfr = self.sub_sfr[i]

                try:
                    self.sub_Zgas
                except AttributeError:
                    self.get_sub_Zgas()
                Zgas = self.sub_Zgas[i]

                try:
                    self.sub_HI_mass
                except AttributeError:
                    self.get_sub_HI_mass()
                HI_mass = self.sub_HI_mass[i]

            nbhs = self.cat.sub_lentab[i, 5]
            mbhs = 0.0

            if self.header.nall[5] > 0:
                try:
                    self.sub_bhmass
                except AttributeError:
                    self.get_sub_bh_props()
                mbhs = self.sub_bhmass[i]
                mbh1 = self.sub_bhmass1[i]
                mbh2 = self.sub_bhmass2[i]
                mbh3 = self.sub_bhmass3[i]
                mdotbh1 = self.sub_bhmassdot1[i]
                mdotbh2 = self.sub_bhmassdot2[i]
                mdotbh3 = self.sub_bhmassdot3[i]
                rbh1 = self.sub_bhrad1[i]
                rbh2 = self.sub_bhrad2[i]
                rbh3 = self.sub_bhrad3[i]
                vbh1 = self.sub_bhvel1[i]
                vbh2 = self.sub_bhvel2[i]
                vbh3 = self.sub_bhvel3[i]

            print(i, isub, grnr, parent, nall, mall, ngas, mgas, ndm, mdm,
                  nstars, mstars, nbhs, mbhs, ncont, mcont, file=f)

            if self.header.sfr == 1:
                print(sfr, Zgas, HI_mass, file=f)
            else:
                print(0.0, 0.0,  0.0, file=f)

            if self.header.nall[5] > 0:
                print(mbh1, mdotbh1, rbh1, vbh1, mbh2, mdotbh2, rbh2, vbh2,
                      mbh3, mdotbh3, rbh3, vbh3, file=f)
            else:
                print(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                      0.0, 0.0, 0.0, 0.0, 0.0, file=f)

            print("", file=f)

            isub += 1

        f.close()

# -------------------------------------------------------------
#                      GUI stuff
# -------------------------------------------------------------

def opensnap():
    import Tkinter
    from tkFileDialog import askdirectory

    root = Tkinter.Tk()

    myselections = dict()

    mydir = Tkinter.Entry(root)

    def select_snapshot():
        mydir.delete(0, Tkinter.END)
        mydir.insert(0, askdirectory(initialdir="/"))

    Tkinter.Button(root, text='Choose simulation directory',
                   command=select_snapshot).pack(side=Tkinter.TOP,
                                                 padx=10, pady=10)

    mydir.pack(side=Tkinter.TOP, padx=10, pady=10)

    Tkinter.Label(root, text="Select simulation properties:").pack(
        side=Tkinter.TOP, padx=10, pady=10)

    myframe = Tkinter.LabelFrame(root, text="length unit")
    mpc_units = Tkinter.BooleanVar()
    rb = Tkinter.Radiobutton(myframe, text="Mpc / h",
                             variable=mpc_units, value=True)
    rb.pack(side=Tkinter.TOP, pady=2, padx=10)
    rb = Tkinter.Radiobutton(myframe, text="kpc / h",
                             variable=mpc_units, value=False)
    rb.pack(side=Tkinter.TOP, pady=2, padx=10)
    mpc_units.set(True)
    myframe.pack(side=Tkinter.TOP, padx=10, pady=10)

    myframe = Tkinter.LabelFrame(
        root, text="Subfind group velocity dispersion")
    group_veldisp = Tkinter.BooleanVar()
    rb = Tkinter.Radiobutton(myframe, text="available",
                             variable=group_veldisp, value=True)
    rb.pack(side=Tkinter.TOP, pady=2, padx=10)
    rb = Tkinter.Radiobutton(
        myframe, text="not available", variable=group_veldisp, value=False)
    rb.pack(side=Tkinter.TOP, pady=2, padx=10)
    group_veldisp.set(True)
    myframe.pack(side=Tkinter.TOP, padx=10, pady=10)

    myframe = Tkinter.LabelFrame(root, text="Subfind subhalo masstab")
    masstab = Tkinter.BooleanVar()
    rb = Tkinter.Radiobutton(myframe, text="available",
                             variable=masstab, value=True)
    rb.pack(side=Tkinter.TOP, pady=2, padx=10)
    rb = Tkinter.Radiobutton(
        myframe, text="not available", variable=masstab, value=False)
    rb.pack(side=Tkinter.TOP, pady=2, padx=10)
    myframe.pack(side=Tkinter.TOP, padx=10, pady=10)

    mysnapnum = Tkinter.Entry(root)
    mysnapnum.pack(side=Tkinter.TOP, padx=10, pady=10)

    def load_snapshot():
        myselections['dir'] = (mydir.get()).strip()
        if myselections['dir'][-1] != "/":
            myselections['dir'] += "/"
        myselections['snapnum'] = (mysnapnum.get()).strip()
        myselections['mpc_units'] = mpc_units.get()
        myselections['group_veldisp'] = group_veldisp.get()
        myselections['masstab'] = masstab.get()
        root.destroy()

    Tkinter.Button(root, text='Load snapshot', command=load_snapshot).pack(
        side=Tkinter.TOP, padx=10, pady=10)

    root.mainloop()

    print("simulation directory", myselections['dir'], "selected")
    print("mpc_units =", myselections['mpc_units'])
    print("group_veldisp =", myselections['group_veldisp'])
    print("masstab =", myselections['masstab'])

    return (snapshot(myselections['dir'], myselections['snapnum'],
                     mpc_units=myselections['mpc_units'],
                     group_veldisp=myselections['group_veldisp'],
                     masstab=myselections['masstab'], long_ids=False,
                     swap=False, cosmic_ray_species=0, ic_file="none"))
