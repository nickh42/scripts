from pylab import *

import powspec

testspec1 = powspec.powspec("Millennium")
testspec2 = powspec.powspec("Illustris")

fig = figure()
ax = fig.add_subplot(111)

testspec1.plot(useax=ax)
testspec2.plot(useax=ax)

fig.show()

