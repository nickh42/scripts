import matplotlib.pyplot as plt
import numpy as np

import powspec

# compute mass function and compare to result from HMFcalc
# Tinker 2008 is assumed for the fitting function

# Planck 2015 transfer function calculated with CAMBS
# with the settings in "planck+lowP+lensing+ext_2015.ini"
# compared to HMFcalc with power spectrum also generated
# with CAMBS (with their default parameters)
#spec = powspec.powspec("Planck15")
#omega_m = 0.307494  # value used in planck+lowP+lensing+ext_2015.ini
#hmfcalc_dat = np.loadtxt("hmfcalc_mVector_PLANCK15.txt")

# As above but inputting the same power spectrum
# as used by HMFcalc
spec = powspec.powspec("HMFcalc", "hmfcalc_kVector_PLANCK15.txt")
omega_m = 0.307493  # value given to HMFcalc
hmfcalc_dat = np.loadtxt("hmfcalc_mVector_PLANCK15.txt")

rho_mean = 2.77536627e11 * omega_m  # comoving density in h^2 M_sun / Mpc^3
Delta = 200.0
z = 0.0
masses, dn2dM = spec.get_massfunc(Delta, z, omega_m, Delta_type="crit",
                                  minmass=1.0e10, maxmass=10**14.95, nmass=100)

# load HMFcalc result for comparison
hmfcalc_dat_mass = hmfcalc_dat[:, 0]
hmfcalc_dat_dn2dM = hmfcalc_dat[:, 5]

# compare with plot in Tinker+ 2008; cosmology not exactly the same
fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(14, 6))

ax1.plot(np.log10(masses), np.log10(masses**2/rho_mean * dn2dM),
         c="blue", label="my result")

ax1.plot(np.log10(hmfcalc_dat_mass),
         np.log10(hmfcalc_dat_mass**2/rho_mean * hmfcalc_dat_dn2dM),
         c="red", label="HMF calc")

ax1.legend(loc="lower left")
ax1.set_xlabel(r"$\mathrm{log_{10} \, (M \, [M_{\odot}/h])}$")
ax1.set_ylabel(r"$\mathrm{log_{10}(M^2 \rho_{m}^{-1} \, dn/dM \, "
               "[h^4 \, Mpc^{-3} M_{\odot}^{-1}])}$")

# plot the ratio
assert np.all(np.abs(np.log10(hmfcalc_dat_mass) - np.log10(masses)) < 1e-3)
ax2.plot(np.log10(masses), (masses**2/rho_mean * dn2dM) /
         (hmfcalc_dat_mass**2/rho_mean * hmfcalc_dat_dn2dM) - 1.0)
ax2.set_ylabel("hmfcalc / my result - 1")
ax2.set_xlabel(r"$\mathrm{log_{10} \, (M \, [M_{\odot}/h])}$")

plt.tight_layout()
fig.show()
