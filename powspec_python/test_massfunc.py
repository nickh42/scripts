from pylab import *

import powspec

# compute mass function

spec = powspec.powspec("Planck15")
#spec.scale(8.0, 0.8159) # rescale amplitude of power spectrum

omega_m = 0.307493#0.281
rho_mean = 2.77536627e11 * omega_m # comoving density in h^2 M_sun / Mpc^3

masses, dn2dM = spec.get_massfunc(200.0, 0.0, omega_m, Delta_type = "crit", minmass=1.0e10, maxmass=1.0e16, nmass=200)

# load HMFcalc result for comparison

hmfcalc_dat = loadtxt("hmfcalc_mVector_PLANCK15.txt")
hmfcalc_dat_mass = hmfcalc_dat[:,0]
hmfcalc_dat_dn2dM = hmfcalc_dat[:,5]

fig = figure() # compare with plot in Tinker+ 2008; cosmology not exactly the same
ax = fig.add_subplot(111)

ax.plot(log10(masses),log10(masses**2/rho_mean * dn2dM), c="blue", label="my result")

ax.plot(log10(hmfcalc_dat_mass),log10(hmfcalc_dat_mass**2/rho_mean * hmfcalc_dat_dn2dM), c="red", label="HMF calc")

ax.set_ylim(-5,-1)
ax.legend(loc="lower left")
ax.set_xlabel("log10( M / (Msun/h) )")
ax.set_ylabel("log10( M^2 / rho_mean * dn/dM) [h^4/(Mpc^3*M_sun)]")
fig.show()

