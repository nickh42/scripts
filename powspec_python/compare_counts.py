#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 26 18:08:38 2018
Predict number of clusters above a given mass
threshold as a function of redshift.
@author: nh444
"""
from __future__ import print_function

import hmf
import numpy as np
import matplotlib.pyplot as plt

from scipy.constants import m_p, m_e, c, e
from scipy.constants import parsec as pc  # in m
thomson = 6.6524587158e-29  # thomson cross-section m^2
mu_e = 1.165  # mean molecular weight per free electron.
# get factor to convert Msun keV to Mpc^2
MsunkeV_to_Mpc2 = (1.9884430e30 * 1.0e3 * e /
                   (mu_e * m_p * m_e * c**2 / thomson) / (1.0e6 * pc)**2)


def main():
    # Use nz shells in redshift between z1 and z2.
    z1 = 0.0
    z2 = 2.0
    nz = 20
    # Survey area in degrees-squared.
    survey_area = 2500.0  # SPT-SZ and SPT-3G
    # Fraction of the sky spanned by survey_area
    sky_fraction = (survey_area / (4.0*np.pi*(180.0/np.pi)**2))

#    plot_counts(z1=z1, z2=z2, nz=nz, sky_fraction=sky_fraction,
#                # M_thresh=np.full(nz, 1e14 * 0.7),
#                M_thresh=np.column_stack((np.full(nz, 1e14 * 0.7),
#                                          np.full(nz, 3e14 * 0.7),
#                                          np.full(nz, 5e14 * 0.7))),
#                Delta=Delta, Delta_type=Delta_type,
#                verbose=True)

#    plot_counts(z1=z1, z2=z2, nz=nz, sky_fraction=sky_fraction,
#                M_func=get_Mthresh_weights, M_func_kwargs={'Y500': 0.88e-5},
#                Delta=500.0, Delta_type='crit',
#                labels=("FABLE", "MACSIS", "Truong+ 18", "Nagarajan+18"),
#                title="$\mathrm{Y_{lim}} \propto E(z)^{-2/3}$",
#                verbose=True)

# =============================================================================
#     plot_counts(z1=z1, z2=z2, nz=nz, sky_fraction=sky_fraction,
#                 M_func=get_Mthresh_weights, have_weights=True,
# #                M_func_kwargs={'Y500': 3.12e-5, 'Epow': 0.25},
# #                title="$\mathrm{Y_{lim}}(z) \propto E(z)^{0.25}$",
#                 M_func_kwargs={'Y500': 2.85e-5, 'Epow': 0.0},
#                 title="$\mathrm{Y_{lim}}(z) \sim$ const.",
#                 Delta=500.0, Delta_type='crit',
#                 labels=["FABLE", "MACSIS", "Planelles+17",
#                         "Nagarajan+18", "Andersson+11"],
#                 ylims=[0.1, 300],
#                 show_Ntot_z=[1.0, 1.5, 2.0], plot_Mthresh=True, plot_SPT=True,
#                 verbose=False)
# =============================================================================

    plot_counts(z1=z1, z2=z2, nz=nz, sky_fraction=sky_fraction,
                M_func=get_Mthresh_weights, have_weights=True,
                M_func_kwargs={'Y500': 1e-5, 'Epow': 0.25},  # 5000
                # M_func_kwargs={'Y500': 7.0e-6, 'Epow': 0.25},  # ~8000
                # M_func_kwargs={'Y500': 1.0e-6, 'Epow': 0.25},
                ylims=[10, 2e3], leg_kwargs={'borderaxespad': 2.2},
                # title=r"$\mathrm{Y_{lim}}(z) \propto E(z)^{0.25}$",
                Delta=500.0, Delta_type='crit',
                labels=["FABLE", "MACSIS", "Planelles+17", "Nagarajan+18",
                        # "Planck 14",
                        "Andersson+11"],
                plot_SPT3G=False, plot_CMBS4=False,
                show_Ntot_z=[1.0, 1.5, 2.0], plot_Mthresh=True,
                verbose=True)

#    plot_counts(z1=z1, z2=z2, nz=nz, sky_fraction=sky_fraction,
#                M_func=M_from_SNR, have_weights=False,
#                M_func_kwargs={'SNR': 4.5},
#                title=r"$\zeta = 4.5$",
#                Delta=500.0, Delta_type='crit',
#                labels=["de Haan+16"],
#                plot_SPT=True, plot_SPT3G=False, plot_CMBS4=False,
#                show_Ntot_z=[1.0, 1.5, 2.0], plot_Mthresh=False,
#                verbose=True)

#    plot_counts(z1=z1, z2=z2, nz=nz, sky_fraction=sky_fraction,
#                M_func=get_Mthresh_weights,
#                M_func_kwargs={'SNR': 3.4},
#                Delta=500.0, Delta_type='crit',
#                labels=("FABLE", "MACSIS", "Truong+ 18", "Nagarajan+18"),
#                verbose=True)

    # This second example mimics Nagarajan+14 whom use a fixed SZ threshold of
    # Y500 E(z)^-2/3 DA^2 = 0.88e-5 Mpc^2 which predicts 5000 clusters for
    # the 2500 deg^2 SPT-3G survey.
#    plot_counts(z1=z1, z2=z2, nz=nz, sky_fraction=sky_fraction,
#                M_func=Naga_bestfit, M_func_kwargs={},
#                Delta=500.0, Delta_type='crit',
#                verbose=True)


def get_Mthresh_weights(z, m=None, Y500=0.88e-5, Epow=-2.0/3.0, SNR=None,
                        z0_only=False, unbiased_MACSIS=False,
                        omega_m=0.3089, h_scale=0.6774):
    """Get mass threshold vs redshift for given Y500 for different Y500-M500.

    For the given detection threshold Y = Y500 E(z)^Epow DA^2 in Mpc^2,
    get the corresponding mass threshold (M500 in Msun/h) for each
    redshift in 'z' using several different Y500-M500 relations from
    the literature.
    If 'm' is not None, then also return a 3-D array of weights for each
    mass (Msun/h) in 'm' and each redshift in 'z' where each weight
    corresponds to the fraction of haloes that scatter above the given
    detection threshold based on the scatter about the relation.
    Arguments:
        z: A vector of redshifts.
        m: (optional) masses in Msun/h for which to calculate weights.
        Y500: (optional) detection threshold Y500 E(z)^-2/3 DA^2 in Mpc^2.
        Epow: power of E(z) to scale Y500 DA^2 by. Default -2/3 (self-similar).
        SNR: (optional) detection threshold as SPT signal-to-noise ratio.
             This is converted to a value of Y500 using the scaling relation
             in Andersson+11.
        z0_only: use the z=0 relation only, otherwise interpolate
                 the mass threshold at each redshift.
        unbiased_MACSIS: Also get the results for MACSIS after approximately
                         accounting for X-ray mass bias.
        h_scale: The value of h to scale each of the relations to.
                 The value of Y500 is assumed to have this value of h.
    Returns:
        if 'm' is None:
            (M_thresh)
        else:
            (M_thresh, weights)
        M_thresh: An array of masses in Msun/h with each row
                  corresponding to the redshift given in 'z'.
        weights: A 3-D array of weights for each redshift in 'z'
                 and each mass in 'm'.
    """
    from scaling.plotting.plot_relation import get_sim_relation
    from sim_python import cosmo
    from scipy.stats import norm

    axnames = ["mass", "Y500"]
    cm = cosmo.cosmology(h_scale, omega_m)

    if m is not None:
        get_weights = True
    else:
        get_weights = False

    # Get my best-fit parameters.
    dat = np.loadtxt("/data/curie4/nh444/project1/L-T_relations/" +
                     "fit_params_"+axnames[0]+"_"+axnames[1]+".txt")
    (zz, slope, slope_err_low, slope_err_high,
     logC, logC_err_low, logC_err_high, scat) = np.transpose(dat[:, :8])
    # Get pivot point used for fitting in plot_relation.
    from scaling.plotting.plot_relation import get_fit_params
    my_X0, _, _ = get_fit_params(axnames)
    if SNR is None:
        # Scale Y500 by appropriate power of E(z) to get it in units of
        # Y500 E(z)^-2/3 DA^2 in Mpc^2 as used in the scaling relations.
        Y = Y500 * cm.E_z(zz)**(-2.0/3.0 - Epow)
    else:
        # Get Y500 E(z)^0.25 DA^2 in Mpc^2 for the given SNR.
        Y = np.full_like(zz, Y_from_SNR(SNR))
        # Convert to Y500 E(z)^-2/3 DA^2
        Y *= cm.E_z(zz)**(-0.25 - 2.0/3.0)
    # Assume h=0.6774 was used in the fit, although this might not be the case!
    my_h = 0.6774
    # Rescale Y to h_scale. Y500 has no h-dependence itself but
    # it is scaled by DA^2, which has h^-2 dependence.
    my_Y = Y * (h_scale / my_h)**2
    # Get the masses (Msun/h) corresponding to these values for Y.
    M = my_X0 * my_h * 10**((np.log10(my_Y) - logC) / slope)
    print("M[0] =", '%.2E' % M[0])
    if z0_only:
        # Just use the lowest redshift fit.
        M1 = np.full_like(z, M[0])
    else:
        # Interpolate on the mass threshold. Mask values outside
        # the tabulated redshift range.
        M1 = np.interp(z, zz, M, right=np.inf)
        M1 = np.ma.masked_invalid(M1)
    if get_weights:
        if z0_only:
            cur_scat = np.full(z.shape, scat[0], dtype=float)
            cur_logC = np.full(z.shape, logC[0], dtype=float)
            cur_slope = np.full(z.shape, slope[0], dtype=float)
            cur_logY_z = np.full(z.shape, np.log10(my_Y[0]), dtype=float)
        else:
            # Interpolate best-fit parameters for given redshifts 'z'.
            cur_scat = np.interp(z, zz, scat, right=np.inf)
            cur_logC = np.interp(z, zz, logC, right=np.inf)
            cur_slope = np.interp(z, zz, slope, right=np.inf)
            # Interpolate Y threshold for given redshifts 'z'.
            cur_logY_z = np.interp(z, zz, np.log10(my_Y), right=np.inf)
        # Get log(Y500) corresponding to each mass.
        cur_logY_m = (cur_logC + cur_slope *
                      np.log10(m[:, np.newaxis] / (my_X0 * my_h)))
        # Assuming a log-normal distribution centred around the
        # threshold Y at each mass with standard deviation equal to the
        # scatter, get the proportion of Y-values that would lie above
        # the threshold Y (i.e. the survival function at cur_Y).
        # Each row will be a different redshift and each column
        # will correspond to a mass in 'm'.
        W1 = norm.sf(cur_logY_z[:, np.newaxis], loc=cur_logY_m.T,
                     scale=cur_scat[:, np.newaxis])

    def get_Mthresh_sims(name, bias=None):
        """Return M_thresh and weights for MACSIS or Truong+18 relations.

        Arguments:
            name: (string) "MACSIS" or "Truong"
            bias: (float) Level of X-ray hydrostatic mass bias assumed in the
                  best-fitting relation, where M_Xray / M_true = 1 - bias.
        """
        if bias is not None:
            from profiles.REXCESS_profiles import pres_prof_model
            from scipy.integrate import quad as scipyquad
        # Load best-fit Y500-M500 parameters for all available redshifts.
        simdat = get_sim_relation(name, axnames, h_scale=h_scale)
        if SNR is None:
            # Scale Y500 by appropriate power of E(z) to get it in units of
            # Y500 E(z)^-2/3 DA^2 in Mpc^2 as used in the scaling relations.
            Y = Y500 * cm.E_z(simdat['z'])**(-2.0/3.0 - Epow)
        else:
            # Get Y500 E(z)^0.25 DA^2 in Mpc^2 for the given SNR.
            Y = np.full_like(simdat['z'], Y_from_SNR(SNR))
            # Convert to Y500 E(z)^-2/3 DA^2
            Y *= cm.E_z(simdat['z'])**(-0.25 - 2.0/3.0)
        # Get M500 in Msun/h for the given Y500 for each available redshift.
        Msim = (simdat['X0'] * simdat['h_scale'] *
                10**((np.log10(Y) - simdat['norm']) / simdat['slope']))
        if bias is not None:
            # Assume that the best-fitting relation is based on biased masses.
            # Scale Y(r500) to the value measured if r500 was underestimated by
            # the factor (1 - bias)**1/3 using the universal pressure profile.
            for i in range(len(Y)):
                # Integrate universal pressure profile from r=0 to r=r500 for
                # the approx (unbiased) mass and redshift.
                integral_true = scipyquad(pres_prof_model, 0.0, 1.0,
                                          args=(None, Msim[i] / (1 - bias),
                                                simdat['z'][i]))[0]
                # If M500 is biased low, R500 will be biased low by the
                # factor (1-b)^(1/3). Thus Y will be biased low.
                integral_biased = scipyquad(
                        pres_prof_model, 0.0, 1.0 * (1.0-bias)**(1.0/3.0),
                        args=(None, Msim[i] / (1 - bias), simdat['z'][i]))[0]
                # The biased value of Y is the unbiased value multiplied
                # by this ratio.
                Y[i] *= integral_biased / integral_true
            # Recalculate the mass threshold from the biased Y value.
            Msim = (simdat['X0'] * simdat['h_scale'] *
                    10**((np.log10(Y) - simdat['norm']) / simdat['slope']))
            # Correct the mass for the bias.
            Msim /= (1.0 - bias)
        if z0_only:
            M = np.full_like(z, Msim[0])
        else:
            # Interpolate on the logarithm of the mass threshold.
            M = 10**np.interp(z, simdat['z'], np.log10(Msim), right=np.inf)
            # Mask values outside the tabulated redshift range.
            M = np.ma.masked_invalid(M)

        if get_weights:
            if z0_only:
                # Use the same values at all redshifts.
                scat = np.full(z.shape[0], simdat['scat'][0], dtype=float)
                logC = np.full(z.shape[0], simdat['norm'][0], dtype=float)
                slope = np.full(z.shape[0], simdat['slope'][0], dtype=float)
                logY_z = np.full(z.shape[0], np.log10(Y[0]), dtype=float)
                zind = np.array(range(z.shape[0]))
            else:
                # Interpolate best-fit parameters for given redshifts 'z'.
                # Get indices of redshifts that can be interpolated.
                zind = np.where(z <= np.max(simdat['z']))[0]
                scat = np.interp(z[zind], simdat['z'], simdat['scat'])
                logC = np.interp(z[zind], simdat['z'], simdat['norm'])
                slope = np.interp(z[zind], simdat['z'], simdat['slope'])
                # Interpolate Y threshold onto the redshift array 'z'.
                logY_z = np.interp(z[zind], simdat['z'], np.log10(Y),
                                   right=np.inf)
            # Get log(Y500) corresponding to each mass for each redshift.
            # This will be a 2D array of shape (n_mass_bins, n_z_bins).
            if bias is None:
                logY_m = (logC + slope *
                          np.log10(m[:, np.newaxis] / (simdat['X0'] *
                                                       simdat['h_scale'])))
            else:
                # Assuming that the best-fitting relation is based on biased
                # masses, get Y500 from the biased mass.
                logY_m = (logC + slope *
                          np.log10(m[:, np.newaxis] * (1.0 - bias) /
                                   (simdat['X0'] * simdat['h_scale'])))
                # These values of Y are measured within the biased r500 so
                # rescale them to (larger) unbiased Y values using the
                # universal pressure profile.
                print("Rescaling Y(r500_biased) to Y(r500).")
                for mbin in range(logY_m.shape[0]):
                    for zbin in range(logY_m.shape[1]):
                        # Integrate universal pressure profile from r=0 to
                        # r=r500 for the unbiased mass.
                        integral_true = scipyquad(
                                pres_prof_model, 0.0, 1.0,
                                args=(None, m[mbin], z[zbin]))[0]
                        # R500 is biased low by (1-b)^(1/3) so Y is biased low.
                        # Note that pres_prof_model takes x=r/r500 as arguments
                        # so the outer limit will be larger if r500 is smaller.
                        integral_biased = scipyquad(
                                pres_prof_model, 0.0, 1.0 / (1.0-bias)**(1.0/3.0),
                                args=(None, m[mbin], z[zbin]))[0]
                        logY_m[mbin, zbin] *= integral_true / integral_biased
            # Weights array has shape (N_z, N_m).
            # Use a weighting of unity when the best-fitting parameters
            # couldn't be interpolated onto a given redshift. Otherwise the
            # product of the weights and the hmf is zero and hmf.py complains.
            W = np.ones((z.shape[0], logY_m.shape[0]))
            W[zind, :] = norm.sf(logY_z[:, np.newaxis], loc=logY_m.T,
                                 scale=scat[:, np.newaxis])
            return M, W
        else:
            return M

    # MACSIS (based on X-ray masses)
    MW = get_Mthresh_sims("MACSIS")
    M_MACSIS = MW
    if get_weights:
        M_MACSIS = MW[0]
        W_MACSIS = MW[1]
    if unbiased_MACSIS:
        # MACSIS approximately corrected for X-ray mass bias of 20 per cent
        MW = get_Mthresh_sims("MACSIS", bias=0.2)
        M_MACSIS_BC = MW
        if get_weights:
            M_MACSIS_BC = MW[0]
            W_MACSIS_BC = MW[1]
    # Truong+18
    MW = get_Mthresh_sims("Truong")
    M_Truong = MW
    if get_weights:
        M_Truong = MW[0]
        W_Truong = MW[1]

    # Nagarajan+18
    if SNR is None:
        Y = Y500 * cm.E_z(z)**(-2.0/3.0 - Epow)
    else:
        Y = np.full_like(z, Y_from_SNR(SNR))
        Y *= cm.E_z(z)**(-0.25 - 2.0/3.0)
    M_Nag = Naga_bestfit(z, Y=Y, h_scale=h_scale)
    if get_weights:
        logY_z = np.log10(Y)
        scat = np.full_like(z, 0.19/np.log(10))
        # Get log(Y500) corresponding to each mass.
        logY_m = np.log10(Naga_bestfit(M=m, h_scale=h_scale))
        # This is the same for each redshift since we assume
        # self-similarity, so just repeat this vector.
        logY_m = np.repeat(logY_m[:, np.newaxis], z.size, axis=1)
        W_Nag = norm.sf(logY_z[:, np.newaxis], loc=logY_m.T,
                        scale=scat[:, np.newaxis])

#    # Planck+14 "baseline" relation
#    Y = Y500 * cm.E_z(z)**(-2.0/3.0 - Epow)
#    M5 = Planck14_bestfit(z, Y=Y, h_scale=h_scale)
#    if get_weights:
#        logY_z = np.log10(Y)
#        scat = np.full_like(z, 0.075)
#        # Get log(Y500) corresponding to each mass.
#        logY_m = np.log10(Planck14_bestfit(M=m, h_scale=h_scale))
#        # This is the same for each redshift since we assume
#        # self-similarity, so just repeat this vector.
#        logY_m = np.repeat(logY_m[:, np.newaxis], z.size, axis=1)
#        W5 = norm.sf(logY_z[:, np.newaxis], loc=logY_m.T,
#                     scale=scat[:, np.newaxis])

    # Andersson+11
    Y = Y500 * cm.E_z(z)**(-2.0/3.0 - Epow)
    M_And = Andersson_bestfit(z, Y=Y, h_scale=h_scale)
    if get_weights:
        logY_z = np.log10(Y)
        scat = np.full_like(z, 0.09)
        # Get log(Y500) corresponding to each mass.
        logY_m = np.log10(Andersson_bestfit(M=m, h_scale=h_scale))
        # This is the same for each redshift since we assume
        # self-similarity, so just repeat this vector.
        logY_m = np.repeat(logY_m[:, np.newaxis], z.size, axis=1)
        W_And = norm.sf(logY_z[:, np.newaxis], loc=logY_m.T,
                        scale=scat[:, np.newaxis])

    if unbiased_MACSIS:
        M_list = (M1, M_MACSIS, M_MACSIS_BC, M_Truong, M_Nag, M_And)
    else:
        M_list = (M1, M_MACSIS, M_Truong, M_Nag, M_And)
    M = np.ma.column_stack(M_list)
    if get_weights:
        # Stack the 2-D arrays of weights along a new axis.
        # The new axis will be the last dimension.
        if unbiased_MACSIS:
            W = np.stack((W1, W_MACSIS, W_MACSIS_BC, W_Truong, W_Nag, W_And),
                         axis=-1)
        else:
            W = np.stack((W1, W_MACSIS, W_Truong, W_Nag, W_And), axis=-1)
        return M, W
    else:
        return M


def Naga_bestfit(z=None, Y=None, M=None, h_scale=0.6774):
    """ Y500-M500 relation from Nagarajan+18.

    Args:
        z: Array of redshifts. The values are not actually used since
           we assume self-similarity (in this case the relation does not
           change with redshift since the normalisation of the
           relation is self-similarly scaled by the factor E(z)^-2/3.)
        Y: Y500 E(z)^-2/3 DA^2 in Mpc^2 for h=h_scale.
        M: M500 in Msun/h.
        h_scale: value of little h that is assumed for Y500.
    Returns:
        if Y is not None:
            M500 in Msun/h
        elif M is not None:
            Y500 E(z)^-2/3 DA^2 in Mpc^2 for h=h_scale.
    """

    # Best-fitting parameters
    A = 0.97
    B = 1.67

    if Y is not None:
        # return M500 (Msun/h) for Y500 E(z)^-2/3 DA^2 = Y (Mpc^2)
        if z is None:
            raise ValueError("z is required if Y is given.")
        if not hasattr(Y, "__len__"):
            # Y must be an iterable the same length as 'z'
            Y = np.full_like(z, Y)
        else:
            Y = np.array(Y)
            if Y.shape[0] != len(z):
                raise ValueError("Y must be a scalar or same length as z.")
        # Convert Y to their value of h=0.7. Y500 has no h-dependence itself
        # but it is scaled by DA^2, which has h^-2 dependence.
        Yh = Y * (h_scale / 0.7)**2
        m = 7.084e14 * 0.7 * (Yh / 7.93e-5 / A)**(1.0/B)
        return m
    elif M is not None:
        # return Y500 E(z)^-2/3 DA^2 (Mpc^2) for M500=M (Msun/h)
        y = 7.93e-5 * A * (M / (7.084e14 * 0.7))**B * (0.7 / h_scale)**2
        return y
    else:
        raise ValueError("Must supply either Y (and z) or M.")


def Haan_bestfit(z, Y=0.88e-5):
    """ YX-M500 relation from de Haan+16 for SPT-SZ 2500 deg^2 survey.

    Given YX E(z)^-0.251 in Mpc^2, return M500 in Msun/h.
    """
    if not hasattr(Y, "__len__"):
        Y = np.full_like(z, Y)
    else:
        Y = np.array(Y)
        assert Y.shape[0] == len(z), "Y must be a scalar or same length as z."
    A = 6.235
    B = 0.491
    # C = -0.251
    h = 0.738

    return (1e14 * (A * h**1.5 * (h/0.72)**(2.5*B - 1.5)) *
            (Y / (3e14 * MsunkeV_to_Mpc2))**B)


def Planck14_bestfit(z=None, Y=None, M=None, h_scale=0.6774):
    """Y500-M500 "baseline" relation from Planck 2013 XX

    Args:
        z: Array of redshifts. The values are not actually used since
           we assume self-similarity (in this case the relation does not
           change with redshift since the normalisation of the relation
           is already self-similarly scaled by the factor E(z)^-2/3.)
        Y: Y500 E(z)^-2/3 DA^2 in Mpc^2.
        M: M500 in Msun/h.
        h_scale: value of little h that is assumed for Y500.
    Returns:
        if Y is not None:
            M500 in Msun/h
        elif M is not None:
            Y500 E(z)^-2/3 DA^2 in Mpc^2.
    """
    # Best-fitting parameters
    A = -0.19  # normalisation = 10^A
    B = 1.79  # slope
    b = 0.0  # mass bias parameter

    if Y is not None:
        # return M500 (Msun/h) for Y500 E(z)^-2/3 DA^2 = Y (Mpc^2)
        if z is None:
            raise ValueError("z is required if Y is given.")
        if not hasattr(Y, "__len__"):
            # Y must be an iterable the same length as 'z'
            Y = np.full_like(z, Y)
        else:
            Y = np.array(Y)
            if Y.shape[0] != len(z):
                raise ValueError("Y must be a scalar or same length as z.")
        m = (1.0 / (1.0-b) * 6e14 * 0.7 *
             (Y * (h_scale / 0.7)**2 / 1e-4 / 10**A)**(1.0/B))
        return m
    elif M is not None:
        # return Y500 E(z)^-2/3 DA^2 (Mpc^2) for M500=M (Msun/h)
        y = 1e-4 * 10**A * ((1.0-b) * M / (6e14 * 0.7))**B * (0.7 / h_scale)**2
        return y
    else:
        raise ValueError("Must supply either Y (and z) or M.")


def Andersson_bestfit(z=None, Y=None, M=None, h_scale=0.6774):
    """ Y500-M500 relation from Andersson+11 for subset of SPT clusters.

    Args:
        z: Array of redshifts. The values are not actually used since
           we assume self-similarity (in this case the relation does not
           change with redshift since the normalisation of the relation
           is already self-similarly scaled by the factor E(z)^-2/3.)
        Y: Y500 E(z)^-2/3 DA^2 in Mpc^2 for h=h_scale.
        M: M500 in Msun/h.
        h_scale: value of little h that is assumed for Y500.
    Returns:
        if Y is not None:
            M500 in Msun/h
        elif M is not None:
            Y500 E(z)^-2/3 DA^2 in Mpc^2 for h=h_scale.
    """
    # Best-fitting parameters
    A = 14.06
    B = 1.67

    if Y is not None:
        # return M500 (Msun/h) for Y500 E(z)^-2/3 DA^2 = Y (Mpc^2)
        if z is None:
            raise ValueError("z is required if Y is given.")
        if not hasattr(Y, "__len__"):
            # Y must be an iterable the same length as 'z'
            Y = np.full_like(z, Y)
        else:
            Y = np.array(Y)
            if Y.shape[0] != len(z):
                raise ValueError("Y must be a scalar or same length as z.")
        # Convert Y to their value of h=0.7. Y500 has no h-dependence itself
        # but it is scaled by DA^2, which has h^-2 dependence.
        m = 3e14 * 0.702 * (Y * (h_scale / 0.702)**2 /
                            MsunkeV_to_Mpc2 / 10**A)**(1.0/B)
        return m
    elif M is not None:
        # return Y500 E(z)^-2/3 DA^2 (Mpc^2) for M500=M (Msun/h)
        y = (MsunkeV_to_Mpc2 * (0.702 / h_scale)**2 *
             10**A * (M / (3e14 * 0.702))**B)
        return y
    else:
        raise ValueError("Must supply either Y (and z) or M.")


def Y_from_SNR(SNR):
    """ Calculate Y500 for a given signal-to-noise ratio.

    Returns Y500 E(z)^0.25 DA^2 in Mpc^2 for a given SPT signal-to-noise
    ratio at redshift z using equation 17 from Andersson+11.
    """

    return (SNR / 5.56)**(1.0/0.64) * 2e14 * MsunkeV_to_Mpc2


def M_from_SNR(z, SNR=5.0):
    """ Calculate M500 (Msun/h) for a given SPT signal-to-noise ratio.

    Returns M500 for the given redshift(s) 'z' and signal-to-noise ratio
    (unbiased detection significance in the parlance of SPT) using
    equation 2 from de Haan et al. 2016.
    """
    # Best-fit parameters for SPT+H0+BBN
    A = 4.842
    B = 1.668
    C = 0.55
    omega_m = 0.289

    # Best-fit parameters for SPT+Planck+WP+BAO
    # A = 3.531
    # B = 1.661
    # C = 0.733
    # omega_m = 0.304

    M = 3e14 * (SNR / A / (Ez(z, omega_m) / Ez(0.6, omega_m))**C)**(1.0/B)
    return np.array(M)


def Ez(z, omega_m):
    """Return dimenionless hubble evolution parameter E(z).

    Assuming a flat cosmology (omega_l = 1-omega_m), return E(z)
    for the given redshift(s) 'z'.
    E(z) = sqrt(omega_m * (1+z)**3 + (1-omega_m))
    """

    return np.sqrt(omega_m * (1.0 + z)**3 + (1.0 - omega_m))


def plot_counts(z1=0.0, z2=2.0, nz=20, sky_fraction=None,
                M_thresh=None,
                M_func=None, have_weights=False, M_func_kwargs={},
                Delta=500.0, Delta_type='crit',
                plot_Mthresh=True, labels=None, title=None, col=None, ls=None,
                show_Ntot_z=None, ylims=None, leg_kwargs={},
                plot_SPT=False, plot_SPT3G=False, plot_CMBS4=False,
                obs_dir="/data/vault/nh444/ObsData/SZ/",
                outdir=None, outsuffix="",
                verbose=1):
    """ Plot counts as a function of redshift for different mass thresholds.

    Arguments:
        z1, z2: smallest and highest redshift.
        nz: number of redshift shells.
        sky_fraction: fraction of the sky. Multiplies total cluster counts.
        M_thresh: (optional) Lower mass threshold for each shell in Msun/h.
                  This should be a numpy array where each column is a
                  vector of length nz and each will be plotted separately.
                  If a column needs to be shorter than nz then M_thresh can
                  be a masked array with those values masked.
        M_func: (optional) Function that takes an array of redshifts as
                its first argument, as well as any keyword arguments
                given in M_func_kwargs, and returns a matrix consistent
                with M_thresh defined above.
                If 'have_weights' evaluates to True, M_func is passed
                a vector of masses (Msun/h) as its second argument and
                should return a tuple (M_thresh, weights) where 'weights'
                is a 3-D array of weights for each redshift and mass
                (first and second dimensions, respectively).
                The values must be floats between 0 and 1, which will
                multiply the derived number density per mass bin.
        have_weights: If True, it is assumed that 'M_func' takes a
                      vector of masses as its second argument and
                      returns a tuple (M_thresh, weights), otherwise
                      it is assumed that just (M_thresh) is returned.
        M_func_kwargs: dict of keyword arguments to pass to function M_func.
        Delta: overdensity parameter.
        Delta_type: 'crit' or 'mean' if overdensity is relative to critical
                    or mean density of the Universe, respectively.
        labels: label for each column in M_thresh.
        title: title for plot.
        plot_Mthresh: also plot the lower mass threshold vs redshift.
        show_Ntot_z: list of redshifts at which the total number of clusters
                     up to that redshift will be listed on the plot.
                     If None (default), show_Ntot_z = [z2].
        ylims: y-axis limits for cluster counts in the form [ymin, ymax]
               where ymin and ymax can be None for autoscaling.
        leg_kwargs: dictionary of keyword arguments passed to ax.legend().
        plot_SPT: Plot the SPT-SZ cluster counts in redshift bins.
        plot_SPT3G: Plot predicted SPT-3G cluster counts vs redshift.
        plot_CMBS4: Plot predicted counts for CMB-S4.
        obs_dir: path of directory containg observational data.
        outdir: output directory for saving figure. If None, nothing is saved.
        verbose: Frequency of print messages. 0 (Default) is no messages.
    """
    from cycler import cycler

    if labels is None:
        have_labels = False
        labels = []
    else:
        have_labels = True
        # Convert labels to a list so we can append to it if necessary.
        labels = list(labels)
    # Default for show_Ntot_z is [z2]
    if show_Ntot_z is None:
        show_Ntot_z = [z2]
    # ylims should be a list of 2 elements, which can be None
    if ylims is None:
        ylims = [None, None]
    if col is None:
        col = ['#24439b', '#ED7600', '#50BFD7', '0.3', '0.6']
    if ls is None:
        ls = ['-', '-', '-', '-', '--']

    # Define the redshift shells.
    zedges = np.linspace(z1, z2, nz+1)
    zcentres = (zedges[:-1] + zedges[1:]) / 2.0
    dz = (z2 - z1) / nz
    if verbose:
        print("delta_z =", dz)

    # Get the astropy.cosmology object
    my_cosmo = hmf.cosmo.Cosmology()
    # For some reason the default Planck15 cosmology uses omega_m=0.3075
    # rather than 0.3089 as given in Table 4 of Planck XIII 2015.
    # I have checked that the other params are the same as the table.
    my_cosmo.update(cosmo_params={'Om0': 0.3089})
#    from astropy.cosmology import FlatLambdaCDM
#    from astropy import units as u
#    new_model = FlatLambdaCDM(H0=64.5, Om0=0.353, Tcmb0=2.725,
#                              Ob0=0.022 / 0.645**2,  # doesn't affect counts.
#                              m_nu=0.39/3.0 * u.eV,  # small effect.
#                              )
#    my_cosmo = hmf.cosmo.Cosmology(cosmo_model=new_model)
    if verbose:
        print("h =", my_cosmo.cosmo.h)
        print("omega_m =", my_cosmo.cosmo.Om0)
        print("omega_l =", my_cosmo.cosmo.Ode0)
        print("omega_b =", my_cosmo.cosmo.Ob0)

    # Get the mass function from hmf
    mf = hmf.MassFunction(z=z1, Mmin=10, Mmax=19, dlog10m=0.01,
                          delta_h=Delta, delta_wrt=Delta_type,
                          hmf_model=hmf.fitting_functions.Tinker08,
                          # hmf_model=hmf.fitting_functions.Watson,
#                          transfer_model=hmf.transfer_models.FromFile,
#                          transfer_params={
#                              'fname': os.path.dirname(__file__) +
#                              "/PLANCK_transfer.dat"},
                          )

    if M_func is not None:
        if have_weights:
            M_thresh, weights = M_func(zcentres, mf.m, **M_func_kwargs)
        else:
            M_thresh = M_func(zcentres, **M_func_kwargs)
    elif M_thresh is None:
        raise ValueError("Must give either M_thresh or M_func.")
    elif have_weights:
        raise NotImplementedError("Cannot use have_weights=True with "
                                  "M_thresh. Try using M_func instead.")
    M_thresh = np.ma.array(M_thresh)
    if verbose:
        print("Got M_thresh of size", M_thresh.shape)
    if verbose > 1:
        print("M_thresh =", M_thresh)
    # Number of columns
    ncol = int(M_thresh.size/M_thresh.shape[0])
    if len(labels) < ncol:
        raise ValueError("Not enough labels. Have {:d} labels but mass "
                         "threshold array has {:d} columns.".format(
                                 len(labels), ncol))

    if np.any(np.log10(M_thresh) < mf.Mmin):
        raise ValueError("Minimum masses passed to hmf.MassFunction must all "
                         "be smaller than the specified lower mass threshold.")
    if M_thresh.shape[0] != nz:
        raise ValueError("M_thresh must have first dimension "
                         "of length nz = {:d}".format(int(nz)))

    # number of clusters per shell.
    num = np.ma.masked_where(np.ma.getmask(M_thresh),
                             np.ma.zeros(M_thresh.shape))
    # Comoving volume per shell.
    vol = np.ma.masked_where(np.ma.getmask(M_thresh),
                             np.ma.zeros(M_thresh.shape))

    # Loop over shells
    for i, zz in enumerate(zcentres):
        # Update mass function to redshift of shell centre.
        mf.update(z=zz)
        if have_weights:
            # Create a new array with dndm repeated in each column,
            # where dndm is the number density of haloes per mass bin
            # in h^4 Msun^-1 Mpc^-3.
            dndm = np.repeat(mf.dndm[:, np.newaxis], ncol, axis=1)
            # Multiply the number density in each mass bin by the weights.
            w = weights[i]
            if verbose > 2:
                for wi, ww in enumerate(w):
                    if np.any(ww > 0.0) and np.any(ww < 1.0):
                        print("{:.2f} ".format(np.log10(mf.m[wi])) +
                              " ".join(["{:.3f}".format(www) for www in ww]))
                print("M_thresh =", np.log10(M_thresh[i, 0]))
            dndm *= w
            # Get the cumulative mass function above mf.m in h^3 Mpc^-3.
            # Apply mf._gtm to columns individually since it assumes a vector.
            ngtm = np.apply_along_axis(mf._gtm, 0, dndm)
#            # Get the index of the mass bin that is closest to
#            # the minimum mass threshold in log space for each
#            # column in M_thresh.
#            ind = np.argmin(np.abs(np.log10(mf.m)[:, np.newaxis] -
#                            np.log10(M_thresh[i])), axis=0)
#            # Draw these indices from their respective columns in ngtm.
#            n = ngtm[ind, range(len(ind))]
            # Get the total number of haloes in all mass bins.
            # Since we have the weight we don't need the mass threshold.
            # This sum will only include bins with a non-zero weight so bins
            # much below the mass threshold should be excluded anyway.
            n = np.max(ngtm, axis=0)
        else:
            # Get the cumulative mass function above mf.m in h^3 Mpc^-3.
            ngtm = mf.ngtm
            # Get the index of the mass bin that is closest to
            # the minimum mass threshold in log space for each
            # column in M_thresh.
            ind = np.argmin(np.abs(np.log10(mf.m)[:, np.newaxis] -
                            np.log10(M_thresh[i])), axis=0)
            # The number density of haloes above each mass threshold is thus
            n = ngtm[ind]
        # If any entries in M_thresh[i] were masked, then ind=0 (and n=ngtm[0])
        # because numpy fills masked values with a constant.
        # Hence we should mask these values.
        n = np.ma.masked_where(np.ma.getmask(M_thresh[i]), n)
        # Calculate volume of shell in Mpc^3.
        vol[i] = (my_cosmo.cosmo.comoving_volume(zedges[i+1]).value -
                  my_cosmo.cosmo.comoving_volume(zedges[i]).value)
        # Convert volume to Mpc^3 h^-3.
        vol[i] *= (my_cosmo.cosmo.h)**3.0
        # The number of haloes in this shell is simply the volume of
        # the shell (vol[i] times the number density (n).
        num[i] = n * vol[i] * sky_fraction
    if verbose > 1:
        print("vol =", vol)
        print("num =", num)
    # Get total number of haloes up to each redshift specified in show_Ntot_z
    Ntot = np.zeros((len(show_Ntot_z), ncol), dtype=int)
    for j, z_Ntot in enumerate(show_Ntot_z):
        # Get indices of redshift bins at or below z_Ntot.
        ind = np.where(zedges[1:] <= z_Ntot)[0]
        # Sum the number in each redshift bin.
        Ntot[j] = np.ma.sum(num[ind], axis=0, dtype=int)
        # If any redshifts were masked, set Ntot to 0.
        if np.any(np.ma.getmask(num)):
            Ntot[j, np.any(np.ma.getmask(num)[ind], axis=0)] = 0

    fig, ax = plt.subplots(figsize=(7, 7))
    ax.set_yscale('log')
    ax.set_xlabel("Redshift")
    ax.set_ylabel(r"N(z) / $\delta z = "+"{:.1f}".format(dz)+"$")
    if title is not None:
        ax.set_title(title, pad=15, fontsize=20)
    if ylims[0] is not None:
        ax.set_ylim(bottom=ylims[0])
    if ylims[1] is not None:
        ax.set_ylim(top=ylims[1])

    # Set colour and linestyle cycle
    ax.set_prop_cycle(cycler('color', col) +
                      cycler('linestyle', ls))

    plot_kwargs = {'lw': 4}
    lines = ax.plot(zcentres, num, **plot_kwargs)

    if have_labels:
        if len(labels) < len(lines):
            raise ValueError("labels must have at least "
                             "{:d} entries.".format(len(lines)))
    nrows = len(lines)+1  # Number of legend entries (no. of lines plus header)

    if plot_SPT:
        SPT_col = '0.5'
        SPT = np.genfromtxt(obs_dir+"SPT/SPT-SZ_WL.csv",
                            delimiter=",")
        ind = np.where(SPT[:, 8] > 0.0)[0]  # z > 0
        z_SPT = SPT[ind, 8]
        # M_SPT = SPT[ind, 14] * 0.67  # Planck cosmology (h=0.67 omega_m=0.32)
        hist, bins = np.histogram(z_SPT, bins=zedges, density=False)
        print("hist =", hist)
        ax.errorbar(zcentres, hist, yerr=hist**0.5,
                    marker='o', ms=8, mec=SPT_col, c=SPT_col, ls='none',
                    label="SPT-SZ (N={:d})".format(len(z_SPT)))

        # Get total number of haloes up to each redshift in show_Ntot_z
        Ntot_SPT = np.zeros(len(show_Ntot_z), dtype=int)
        for j, z_Ntot in enumerate(show_Ntot_z):
            # Get indices of redshift bins at or below z_Ntot.
            ind = np.where(bins[1:] <= z_Ntot)[0]
            # Sum the number in each redshift bin.
            Ntot_SPT[j] = np.ma.sum(hist[ind], dtype=int)
        Ntot = np.c_[Ntot, Ntot_SPT]
        labels.append("SPT-SZ")
        nrows += 1
    if plot_SPT3G and abs(dz - 0.1) < 1e-3:
        # Plot predicted SPT-3G / AdvACT cluster counts taken from
        # plot in CMB-S4 paper by Abazajian et al. 2016
        SPT3G = np.genfromtxt(obs_dir +
                              "cluster_counts/SPT-3G_cluster_counts.csv",
                              delimiter=",")
        SPT3G_col = '0.5'
        ax.errorbar(SPT3G[:, 0], SPT3G[:, 1], lw=2, c=SPT3G_col,
                    label="SPT-3G / AdvACT")
        # Get total number of haloes up to each redshift in show_Ntot_z
        Ntot_SPT3G = np.zeros(len(show_Ntot_z), dtype=int)
        zbins = np.linspace(0.0, 2.0, num=21)  # 0.1 spacing
        N_SPT3G = np.interp(zbins, SPT3G[:, 0], SPT3G[:, 1],
                            left=0.0, right=0.0)
        for j, z_Ntot in enumerate(show_Ntot_z):
            ind = np.where(zbins <= z_Ntot)[0]
            Ntot_SPT3G[j] = np.ma.sum(N_SPT3G[ind], dtype=int)
        Ntot = np.c_[Ntot, Ntot_SPT3G]
        labels.append("SPT-3G")
        nrows += 1
    if plot_CMBS4 and abs(dz - 0.1) < 1e-3:
        # Plot predicted counts for CMB-S4 in 3 different
        # configurations: 1, 2 and 3 arcmin resolution.
        CMBcol = '0.5'
        CMBls = ['-', '--', 'dashdot']
        for j, res in enumerate([1, 2, 3]):
            CMBS4 = np.genfromtxt(obs_dir + "cluster_counts/" +
                                  "CMB-S4_"+"{:d}".format(res) +
                                  "arcmin_cluster_counts.csv",
                                  delimiter=",")
            ax.errorbar(CMBS4[:, 0], CMBS4[:, 1], lw=2, ls=CMBls[j], c=CMBcol,
                        label="CMB-S4 ({:d}')".format(res))
            # Get total number of haloes up to each redshift in show_Ntot_z
            Ntot_CMBS4 = np.zeros(len(show_Ntot_z), dtype=int)
            zbins = np.linspace(0.0, 2.0, num=21)  # 0.1 spacing
            N_CMB = np.interp(zbins, CMBS4[:, 0], CMBS4[:, 1],
                              left=0.0, right=0.0)
            for j, z_Ntot in enumerate(show_Ntot_z):
                ind = np.where(zbins <= z_Ntot)[0]
                Ntot_CMBS4[j] = np.ma.sum(N_CMB[ind], dtype=int)
            Ntot = np.c_[Ntot, Ntot_CMBS4]
            labels.append("CMB-S4 ({:d}')".format(res))
            nrows += 1

    if verbose:
        print("Ntot =", Ntot)

    # Custom legend with total number of clusters in a table.
    from matplotlib.lines import Line2D
    custom_lines = [Line2D([0], [0])] * nrows
    ncols = Ntot.shape[0]
    # Define the latex column type: 32pt wide, centred and aligned bottom
    coltype = r">{\centering}b{32pt}"
    # The 'b' type specifier requires the array package.
    if u'\\usepackage{array}' not in plt.rcParams['text.latex.preamble']:
        plt.rcParams['text.latex.preamble'] = (
                plt.rcParams['text.latex.preamble'] + [u'\\usepackage{array}'])
    cols = " ".join([coltype]*ncols)
    if have_labels:
        cols = "b{90pt} " + cols
    header = (r'''\noindent\begin{tabular}{@{}''' + cols + "}" +
              ("", "&")[have_labels] +
              "&".join([r"{z="+"{:.1f}".format(zz)+"}"
                        for zz in show_Ntot_z]) +
              r'''\end{tabular}''' +
              r'''\vspace{1pt}''' +
              r'''\hrule height 0.5pt depth 0pt width 226pt \relax''')
    Ntot_labels = [header]  # each element is one row (legend entry)
    for j in range(Ntot.shape[1]):  # loop over rows
        num_str = ["{:d}".format(N) if N > 0 else "--" for N in Ntot[:, j]]
        if have_labels:
            buf = r"{"+labels[j]+r"} & "
        else:
            buf = ""
        Ntot_labels.append(r'''\begin{tabular}''' + "{@{}" +
                           cols + "}" + buf +
                           r'''&'''.join(num_str) +
                           r'''\end{tabular}''')

    # define default ax.legend() kwargs.
    default_leg_kwargs = {'markerscale': 0, 'handlelength': 0,
                          'handletextpad': 0, 'borderaxespad': 1.2,
                          'fontsize': 'x-small', 'loc': 'lower left'}
    # fill in any missing keys in leg_kwargs with the defaults.
    for key in default_leg_kwargs:
        if key not in leg_kwargs:
            leg_kwargs[key] = default_leg_kwargs[key]
    leg2 = ax.legend(custom_lines, Ntot_labels,
                     **leg_kwargs)
    leg2.get_texts()[0].set_color('0.2')  # column header colour.
    for i, text in enumerate(leg2.get_texts()[1:]):
        if i < len(lines):
            text.set_color(lines[i].get_color())
        if plot_SPT and i == len(lines):
            text.set_color(SPT_col)
        if plot_SPT3G and i == len(lines)+int(plot_SPT):
            text.set_color(SPT3G_col)

    fig.show()

    if plot_Mthresh:
        plot_mass_vs_z(zcentres, M_thresh / 1e14 / my_cosmo.cosmo.h,
                       col=col, ls=ls,
                       labels=labels if have_labels else None,
                       omega_m=my_cosmo.cosmo.Om0,
                       outdir=outdir, outsuffix=outsuffix)

    if outdir is not None:
        fig.savefig(outdir+"counts-vs_z"+outsuffix+".pdf",
                    bbox_inches='tight')
        print("Output to", outdir+"counts-vs_z"+outsuffix+".pdf")


def plot_mass_vs_z(z, M, col=None, ls=None, title=None, labels=None,
                   omega_m=0.3089, outdir=None, outsuffix=""):
    from cycler import cycler

    fig, ax = plt.subplots(figsize=(7, 7))
    if col is not None and ls is not None:
        ax.set_prop_cycle(cycler('color', col) +
                          cycler('linestyle', ls))
    ax.set_ylabel(r"M$_{\rm 500, lim} \, [10^{14} M_{\odot}]$")
    ax.set_xlabel("Redshift")
    if title is not None:
        ax.set_title(title)
    l2 = ax.plot(z, M, lw=3)
    if labels is not None:
        leg = ax.legend(l2, labels, loc='best', fontsize='small',
                        borderaxespad=1.5, markerscale=0,
                        handlelength=0, handletextpad=0)
        for i, text in enumerate(leg.get_texts()):
            text.set_color(l2[i].get_color())

    # Add line showing slope of E(z)^-0.5
    # zlist = np.linspace(0.05, 2.0, num=50)
    # ax.plot(zlist, 2.3*Ez(zlist, omega_m)**-0.55,
            # c='0.5', alpha=0.5, lw=2, ls='dashdot')

    if outdir is not None:
        fig.savefig(outdir+"counts-mass_thresh_vs_z"+outsuffix+".pdf",
                    bbox_inches='tight')
        print("Output to", outdir+"counts-vs_z"+outsuffix+".pdf")


if __name__ == "__main__":
    main()
