import matplotlib.pyplot as plt
import numpy as np

import powspec
import hmf

# compute mass function and compare to result from HMFcalc
# Tinker 2008 is assumed for the fitting function

Delta = 200.0
Delta_type = 'mean'
z = 0.0

# Calculate HMF
mf = hmf.MassFunction(z=z, Mmin=10, Mmax=15, dlog10m=0.05,
                      delta_h=Delta, delta_wrt=Delta_type,
                      hmf_model=hmf.fitting_functions.Tinker08,
                      transfer_model=hmf.transfer_models.FromFile,
                      transfer_params={'fname': "PLANCK_transfer.dat"},
                      )


spec = powspec.powspec("Array", k=mf.k, power=mf.power / (2.0 * np.pi)**3)
omega_m = mf.cosmo_model.Om0

rho_mean = 2.77536627e11 * omega_m  # comoving density in h^2 M_sun / Mpc^3
masses, dn2dM = spec.get_massfunc(Delta, z, omega_m, Delta_type=Delta_type,
                                  minmass=1.0e10, maxmass=10**14.95, nmass=100)

# compare with plot in Tinker+ 2008; cosmology not exactly the same
fig, axes = plt.subplots(2, 2, figsize=(14, 10))
for ax in axes.ravel():
    ax.set_xlabel(r"$\mathrm{log_{10} \, (M \, [M_{\odot}/h])}$")

axes[0][0].plot(np.log10(masses), np.log10(masses**2/rho_mean * dn2dM),
                c="blue", label="my result")

axes[0][0].plot(np.log10(mf.m),
                np.log10(mf.m**2/rho_mean * mf.dndm),
                c="red", label="hmf.py")

axes[0][0].legend(loc="lower left",
                  title="$z = " + "{:.1f}".format(z) +
                  "\, \delta_{" + str(Delta_type) +
                  "} = " + "{:.0f}".format(Delta) + "$")
axes[0][0].set_ylabel(r"$\mathrm{log_{10}(M^2 \rho_{m}^{-1} \, dn/dM \, "
                      "[h^4 \, Mpc^{-3} M_{\odot}^{-1}])}$")

# plot the ratio
assert np.all(np.abs(np.log10(mf.m) - np.log10(masses)) < 1e-3)
axes[0][1].plot(np.log10(masses), (masses**2/rho_mean * dn2dM) /
                (mf.m**2/rho_mean * mf.dndm) - 1.0)
axes[0][1].set_ylabel("my result / hmf.py - 1")

print spec.Delta_mean, mf.delta_halo
# plot f(sigma) ratio
axes[1][0].plot(np.log10(masses), spec.fsigma / mf.fsigma - 1.0,
                label="$\mathrm{f(\sigma)}$")
myfsigma = np.zeros_like(mf.fsigma)
for i, sigma in enumerate(mf.sigma):
    myfsigma[i] = spec.f_of_sigma_tinker2008(sigma, Delta_mean=spec.Delta_mean,
                                             z=z)
axes[1][0].plot(np.log10(masses), myfsigma / mf.fsigma - 1.0, ls='--',
                label="$\mathrm{f(\sigma_{hmf})}$")
axes[1][0].set_ylabel("f(sigma) my result / hmf.py - 1")
axes[1][0].legend(loc='lower left')

plt.tight_layout()
fig.show()
