#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 26 18:08:38 2018
Predict number of clusters above a given mass
threshold as a function of redshift.
@author: nh444
"""

import hmf
import numpy as np
import matplotlib.pyplot as plt


def main():
    Delta = 500.0
    Delta_type = 'crit'
    # Use nz shells in redshift from z1 to z2
    z1 = 0.0
    z2 = 2.0
    nz = 20
    # Survey area in degrees-squared
    survey_area = 2500.0

    # This first example simply print the number of clusters
    # above the specified lower mass threshold.
    M_thresh = np.full(nz, 1e14 * 0.7)  # mass (Msun/h) threshold of each shell
    get_counts(z1=z1, z2=z2, nz=nz, survey_area=survey_area,
               M_thresh=M_thresh, Delta=Delta, Delta_type=Delta_type)

    # This second example mimics Nagarajan+14 whom use a fixed SZ threshold of
    # Y500 E(z)^-2/3 DA^2 = 0.88e-5 Mpc^2 which predicts 5000 clusters for
    # the 2500 deg^2 SPT-3G survey.
    get_counts(z1=z1, z2=z2, nz=nz, survey_area=survey_area,
               M_func=Naga_bestfit, M_func_kwargs={},
               Delta=Delta, Delta_type=Delta_type,
               verbose=True)


def Naga_bestfit(z, Y=0.88e-5):
    # given Y500 E(z)^-2/3 DA^2 in Mpc^2, return M500 in Msun/h
    A = 0.86
    B = 1.51
    M = 7.084e14 * 0.7 * (np.array(Y) / 7.93e-5 / A)**(1.0/B)
    return np.full_like(z, M)


def get_counts(z1=0.0, z2=2.0, nz=20, survey_area=None,
               M_thresh=None, M_func=None, M_func_kwargs={},
               Delta=500.0, Delta_type='crit',
               plot=False, verbose=False):
    """ Plot counts as a function of redshift for a given mass threshold.

    Arguments:
        z1, z2: smallest and highest redshift.
        nz: number of shells.
        survey_area: survey area in degrees-squared.
        M_thresh: (optional) Lower mass threshold for each shell in Msun/h
                  (vector of length nz).
        M_func: (optional) Function that takes an array of redshifts as
                its first (and only) positional argument, as well as any
                keyword arguments defined in M_func_kwargs, and returns
                an array containing the lower mass threshold at each
                redshift in Msun/h.
        M_func_kwargs: dict of keyword arguments to pass to function M_func.
    """

    # Define the redshift shells.
    zedges = np.linspace(z1, z2, nz+1)
    zcentres = (zedges[:-1] + zedges[1:]) / 2.0

    if M_thresh is None:
        if M_func is None:
            raise ValueError("Must give either M_thresh or M_func.")
        else:
            M_thresh = M_func(zcentres, **M_func_kwargs)
            if verbose:
                print "Got masses", M_thresh

    # Get the mass function from hmf
    mf = hmf.MassFunction(z=z1, Mmin=10, Mmax=18, dlog10m=0.01,
                          delta_h=Delta, delta_wrt=Delta_type,
                          hmf_model=hmf.fitting_functions.Tinker08,
                          transfer_model=hmf.transfer_models.FromFile,
                          transfer_params={'fname': "PLANCK_transfer.dat"},
                          )
    if np.any(np.log10(M_thresh) < mf.Mmin):
        raise ValueError("The minimum mass passed to hmf.MassFunction must "
                         "be smaller than the specified lower mass threshold.")
    if len(M_thresh) != nz:
        raise ValueError("M_thresh must be a vector "
                         "of length nz = {:d}".format(int(nz)))

    # Get the astropy.cosmology object
    my_cosmo = hmf.cosmo.Cosmology()

    num = np.zeros_like(zcentres)  # number of clusters per shell.
    vol = np.zeros_like(zcentres)  # Comoving volume per shell.

    # Loop over shells
    for i, zz in enumerate(zcentres):
        # Update mass function to redshift of shell centre.
        mf.update(z=zz)
        # Get the cumulative mass function above mf.m in h^3 Mpc^-3.
        ngtm = mf.ngtm
        # Get the index of the mass bin that is closest to
        # the minimum mass threshold in log space.
        idx = np.argmin(np.abs(np.log10(mf.m) - np.log10(M_thresh[i])))
        # The number density of haloes above the mass threshold is thus
        n = ngtm[idx]
        # Calculate volume of shell in Mpc^3.
        vol[i] = (my_cosmo.cosmo.comoving_volume(zedges[i+1]).value -
                  my_cosmo.cosmo.comoving_volume(zedges[i]).value)
        # Convert volume to Mpc^3 h^-3.
        vol[i] *= (my_cosmo.cosmo.h)**3.0
        # Number of haloes in this shell.
        if survey_area is not None:
            # Scale by the fraction of survey area / full sky
            num[i] = n * vol[i] * (survey_area / (4.0*np.pi*(180.0/np.pi)**2))
        else:
            num[i] = n * vol[i]
    if verbose:
        print "vol =", vol
        print "num =", num
    print int(np.sum(num)), "clusters."

    if plot:
        fig, ax = plt.subplots(figsize=(10, 10))
        ax.set_yscale('log')
        ax.set_xlabel("Redshift")
        ax.set_ylabel("N(z) / $\delta z = 0.1$")
        ax.plot(zcentres, num)
        ax.plot(zcentres, num/2.0)
        fig.show()

if __name__ == "__main__":
    main()
