import pylab as pyl
from scipy import integrate
import os


class powspec:

    def __init__(self, powtype, filename="none", k=None, power=None,
                 snapnum=-1, ngrid=256, bins=30, mpc_units=True, parttype=-1, verbose=False):
        """ Load specified power spectrum and instantiate variables. """

        self.powtype = powtype
        self.verbose = verbose

        if powtype == "Millennium":
            assert filename == "none"

            self.path = os.path.dirname(os.path.abspath(__file__)) + "/"

            filename = self.path + "Millennium_input_spectrum.txt"

            dat = pyl.loadtxt(filename)
            self.k = 10.0**dat[:, 0]
            self.powspec = 10.0**dat[:, 1] / (4.0*pyl.pi*(10.0**dat[:, 0])**3)

        elif powtype == "Illustris":
            assert filename == "none"

            self.path = os.path.dirname(os.path.abspath(__file__)) + "/"

            filename = self.path + "Illustris_input_spectrum.txt"

            dat = pyl.loadtxt(filename)
            self.k = 10.0**dat[:, 0]
            self.powspec = 10.0**dat[:, 1] / (4.0*pyl.pi*(10.0**dat[:, 0])**3)

        elif powtype == "Planck15":
            assert filename == "none"

            self.path = os.path.dirname(os.path.abspath(__file__)) + "/"

            # has been converted to N-GenIC format
            filename = (self.path +
                        "planck+lowP+lensing+ext_2015_matterpower_ngenic.dat")

            dat = pyl.loadtxt(filename)
            self.k = 10.0**dat[:, 0]
            self.powspec = 10.0**dat[:, 1] / (4.0*pyl.pi*(10.0**dat[:, 0])**3)

            # should we rescale to exact sigma_8?

        elif powtype == "N-GenIC_in":
            dat = pyl.loadtxt(filename)
            self.k = 10.0**dat[:, 0]
            self.powspec = 10.0**dat[:, 1] / (4.0*pyl.pi*(10.0**dat[:, 0])**3)

        elif powtype == "N-GenIC_out":
            dat = pyl.loadtxt(filename, skiprows=1)
            self.k = dat[:, 0]
            self.powspec = dat[:, 1] / (4.0*pyl.pi*dat[:, 0]**3)

        elif powtype == "Camb":
            dat = pyl.loadtxt(filename)
            self.k = dat[:, 0]
            self.powspec = dat[:, 1] / (2.0 * pyl.pi)**3

        elif powtype == "HMFcalc_old":
            dat = pyl.loadtxt(filename)
            self.k = pyl.e**dat[:, 1]
            self.powspec = pyl.e**dat[:, 0] / (2.0 * pyl.pi)**3

        elif powtype == "HMFcalc":
            dat = pyl.loadtxt(filename)
            self.k = dat[:, 0]
            self.powspec = dat[:, 1] / (2.0 * pyl.pi)**3

        elif powtype == "Array":
            self.k = k
            self.powspec = power

        elif powtype in ["sim", "ics"]:
            import snap

            if powtype == "sim":
                self.simdir = filename
                self.snapnum = snapnum

                sim = snap.snapshot(
                    self.simdir, self.snapnum, mpc_units=mpc_units,
                    group_veldisp=True, masstab=True)

            elif powtype == "ics":
                if self.verbose:
                    print "Loading snapshot..."
                sim = snap.snapshot("none", -1, mpc_units=mpc_units,
                                    group_veldisp=False, masstab=False,
                                    ic_file=filename)

            if self.verbose:
                print "Loading masses..."
            sim.load_mass()
            if self.verbose:
                print "Loading positions..."
            sim.load_pos()

            if parttype != -1:
                if self.verbose:
                    print "Getting particle types..."
                sim.get_parttype()  # get long list of every particle's type

            self.ngrid = ngrid
            self.bins = bins

            sim.get_power_spectrum(ngrid, bins=bins, parttype=parttype)

            self.k = sim.powspec_kcenters*1000.0
            self.powspec = sim.powspec/1.0e9

            self.sim_redshift = sim.header.redshift
            self.sim_hubble = sim.header.hubble
            self.sim_omega_m = sim.header.omega_m

        else:
            assert False

        self.filename = filename

        self.sigma8 = self.sigma_of_r(8.0)
        if self.verbose:
            print "sigma8 =", self.sigma8

    def plot(self, useax=0, logscale=True, label_axes=True, col=-1):
        """ Plot the matter power spectrum. """
        if useax == 0:
            fig = pyl.figure()
            ax = fig.add_subplot(111)
        else:
            ax = useax

        kwargs = dict()
        if col != -1:
            kwargs["c"] = col

        ax.plot(self.k, self.powspec, **kwargs)

        if logscale:
            ax.set_xscale("log")
            ax.set_yscale("log")

        if label_axes:
            ax.set_xlabel("k [h / Mpc]")
            ax.set_ylabel("matter power spectrum")

        if useax == 0:
            fig.show()

    def get_R_of_M(self, m, omega_m):
        """ Return the radius containing a mass m in comoving Mpc / h.

        Arguments:
            m: mass in Msun / h.
            omega_m: matter density parameter.
        Returns:
            r: radius in comoving Mpc / h.
        """

        # mean density of the Universe in h^2 M_sun / Mpc^3
        rho_mean = 2.77536627e11 * omega_m

        # radius containing a mass m in comoving Mpc / h
        r = (m / (4.0/3.0*pyl.pi*rho_mean))**(1.0/3.0)

        return r

    def sigma_of_r(self, r_tophat):
        """ Calculate sigma (mass variance) within a given radius r_tophat.

        sigma is the rms variance of mass within a sphere of radius R.
        It is calculated via the integral over k of the product of the
        linear power spectrum times k squared (P(k) * k^2) and the Fourier
        transform of the top-hat window function W(kR), where
            W(kR) = 3(sin(kR) - kR*cos(kR))/(kR)^3
        Arguments:
            r_tophat: radius within which to calculate sigma in cMpc/h
        Returns:
            res_sigma: rms variance of mass within the radius r_tophat.
        """

        kr = r_tophat * self.k
        # actually Fourier transfrom of top hat times (2*pi)**(3.0/2.0)
        w = 3.0 * (pyl.sin(kr) / kr**3 - pyl.cos(kr) / kr**2)
        sigma2_int = 4.0 * pyl.pi * self.k * self.k * w * w * self.powspec

        # integrate over wavenumber k
        res = integrate.simps(sigma2_int, x=self.k)
        res_sigma = pyl.sqrt(res)

        if self.verbose:
            print "sigma("+str(r_tophat)+") =", res_sigma

        return res_sigma

    def sigma_of_m(self, m, omega_m):
        """ Calculate sigma (mass variance) for a given mass in Msun/h. """

        # get radius containing a mass m in comoving Mpc / h
        r = self.get_R_of_M(m, omega_m)

        # get rms variance of mass within a sphere of this radius
        return self.sigma_of_r(r)

    def get_dln_sigma2dM(self, m, omega_m):
        """ Calculate the logarithmic slope of sigma (mass variance) and mass.

        Arguments:
            m: mass in Msun/h
            omega_m: matter density parameter
        """
        dm = m * 1.0e-4

        return (pyl.log(self.sigma_of_m(m+0.5*dm, omega_m)) -
                pyl.log(self.sigma_of_m(m-0.5*dm, omega_m))) / dm

    def scale(self, r_tophat, sigma):
        self.powspec *= (sigma / self.sigma_of_r(r_tophat))**2

        self.sigma8 = self.sigma_of_r(8.0)

    def tilt(self, delta_n):
        self.powspec *= (self.k / (2.0 * pyl.pi / 8.0))**delta_n

        self.scale(8.0, self.sigma8)

    # not yet tested
    def evolve(self, current_redshift, target_redshift, hubble, omega_m):
        import sys
        if "cosmo" not in sys.modules:
            import cosmo

        cm = cosmo.cosmology(hubble, omega_m)

        rescale_fac = (cm.growth_func_normhighz(target_redshift) /
                       cm.growth_func_normhighz(current_redshift))**2
        self.powspec *= rescale_fac

        self.evolved_redshift = target_redshift

        return rescale_fac

    def evolve_sim(self, target_redshift):
        assert self.powtype in ["sim", "ics"]

        self.evolve(self.sim_redshift, target_redshift,
                    self.sim_hubble, self.sim_omega_m)

    def write_ngenic_infile(self, filename):
        k_out = pyl.log10(self.k)
        pow_out = pyl.log10(self.powspec * 4.0*pyl.pi*self.k**3)

        pyl.savetxt(filename, pyl.float64(zip(k_out, pow_out)))

    def write_txt(self, filename):
        pyl.savetxt(filename, pyl.float64(zip(self.k, self.powspec)))

    # mass where sigma(mstar) = delta_c
    def compute_mstar(self, omega_m, delta_c=1.686):
        r = 8.0
        rold = 1.0
        sigma = 0.0

        i_iter = 0
        while abs(sigma - delta_c) > 1e-6:
            sigma = self.sigma_of_r(r)

            if self.verbose:
                print i_iter, r, sigma

            rold = r
            # assumes sigma(r) changes by ~1.5 per dex in r
            r *= 10.0**((sigma - delta_c)/1.5)

            i_iter += 1
        r = rold

        print "r(mstar) =", r, "comoving Mpc/h"

        rho_mean = 2.77536627e11 * omega_m  # in h^2 M_sun / Mpc^3
        mstar = 4.0/3.0*pyl.pi*r**3 * rho_mean  # in M_sun / h

        print "mstar =", '%e' % mstar, "M_sun / h"

        return mstar

    # Delta_mean needs to be >= 200.0
    def f_of_sigma_tinker2008(self, sigma, Delta_mean, z):
        try:
            self.tinker_table
        except AttributeError:
            mod_path = os.path.dirname(os.path.abspath(__file__)) + "/"
            self.tinker_table = pyl.loadtxt(mod_path +
                                            "tinker_2008_table2.txt")
            #self.tinker_table = pyl.loadtxt(mod_path + "Tinker_2008.csv",
                                            #delimiter=",")

        assert (Delta_mean >= self.tinker_table[0, 0] and
                Delta_mean <= self.tinker_table[-1, 0])

        # Interpolate (over delta_mean) the parameters of the fitting
        # function from Table 2 of Tinker+ 2008 and scale with
        # redshift according to Eqns. (5)-(8)
        A = pyl.interp(Delta_mean, self.tinker_table[:, 0],
                       self.tinker_table[:, 1]) * (1.0 + z)**(-0.14)
        a = pyl.interp(Delta_mean, self.tinker_table[:, 0],
                       self.tinker_table[:, 2]) * (1.0 + z)**(-0.06)

        alpha = 10.0**(-(0.75/pyl.log10(Delta_mean/75.0))**1.2)
        b = pyl.interp(Delta_mean, self.tinker_table[:, 0],
                       self.tinker_table[:, 3]) * (1.0 + z)**(-alpha)

        c = pyl.interp(Delta_mean, self.tinker_table[:, 0],
                       self.tinker_table[:, 4])

        # based on Eq. (3) of Tinker+ 2008
        f_of_sigma = A * ((sigma/b)**(-a) + 1.0) * pyl.exp(-c/sigma**2)

        return f_of_sigma

    def get_massfunc(self, Delta, z, omega_m, Delta_type="crit",
                     minmass=1.0e13, maxmass=1.0e16, nmass=200):
        """ Get the halo mass function (HMF).

        Calculate the comoving number density of haloes (n) per unit
        logarithm of the halo mass (ln M), i.e. dn / d(ln M),
        for a given range of halo masses.
        Arguments:
            Delta: overdensity parameter, e.g. 200
            z: redshift
            omega_m: matter density parameter
            Delta_type: "crit" or "mean"
            minmass, maxmass: range of masses in Msun/h
            nmass: number of masses to sample in this range.
        Returns:
            masses: array of masses determined by minmass, maxmass and nmass
            dn2dMs: HMF in units 1 / (Msun/h (comoving Mpc/h)^3)
        """
        # Get Delta in terms of the mean density of the Universe.
        if Delta_type == "crit":
            omega_l = 1.0 - omega_m
            rho_crit2rho_mean = (omega_m * (1.0 + z)**3 +
                                 omega_l) / (omega_m * (1.0 + z)**3)
            self.Delta_mean = Delta * rho_crit2rho_mean
        elif Delta_type == "mean":
            self.Delta_mean = Delta
        else:
            raise ValueError("Delta_type must be either 'crit' or 'mean'.")

        # get nmass masses logarithmically spaced in range [minmass, maxmass]
        masses = pyl.logspace(pyl.log10(minmass), pyl.log10(maxmass), nmass)
        dn2dMs = pyl.zeros_like(masses)

        # comoving mean density in h^2 M_sun / Mpc^3
        rho_mean = 2.77536627e11 * omega_m

        self.sigma = pyl.zeros(nmass)
        self.fsigma = pyl.zeros(nmass)
        self.dln_sigma2dM = pyl.zeros(nmass)
        for i in pyl.arange(nmass):
            # Calculate mass variance for current mass.
            self.sigma[i] = self.sigma_of_m(masses[i], omega_m)

            # Obtain value of Tinker 2008 fitting function at this mass.
            self.fsigma[i] = self.f_of_sigma_tinker2008(self.sigma[i],
                                                        self.Delta_mean, z)

            # Calculate logarithmic slope of sigma versus mass at this mass
            self.dln_sigma2dM[i] = self.get_dln_sigma2dM(masses[i], omega_m)

        # the number density of haloes per log mass is the product
        # of the fitting function, the slope of sigma versus mass,
        # the mean density and the reciprocal of the mass.
        # The units are 1 / (Msun/h (comoving Mpc/h)^3)
        dn2dMs = self.fsigma * (rho_mean / masses) * (-self.dln_sigma2dM)

        return masses, dn2dMs

    def concentration_maccio_bullock(self, mass, z, hubble, omega_m, K=4.2,
                                     F=0.01, delta_c=1.686):
        # see Maccio et al. 2008, Dutton & Maccio 2014
        # mass in Msun / h, powerspectrum should be scaled to z=0

        import sys
        if "cosmo" not in sys.modules:
            import cosmo

        mstar = F * mass

        D = delta_c / self.sigma_of_m(mstar, omega_m)

        cm = cosmo.cosmology(hubble, omega_m)

        zc = cm.z_of_growth_func(D)

        print "zc =", zc

        c = K * (cm.rho_crit(zc) / cm.rho_crit(z))**(1.0/3.0)

        return c
