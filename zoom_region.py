#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Mar 31 17:58:47 2018
Plot the contamination of a zoom as a function of radius. This is the ratio of
the mass in Type 2 particles to the total mass.
@author: nh444
"""
import snap
import readsnap as rs
import numpy as np
import matplotlib.pyplot as plt

basedir = "/data/curie4/nh444/project1/zoom_ins/"
simdir = "MDRInt/"
simnames = ["c96_MDRInt",
           #"c128_MDRInt",
           #"c192_MDRInt",
           #"c256_MDRInt",
           #"c320_MDRInt",
           #"c384_MDRInt",
           #"c448_MDRInt",
           #"c480_MDRInt",
           #"c512_MDRInt",
           ]
snapnums = [25]#,22,20,18,15]
groups = range(6)
mpc_units = True

#simnames = ["c400_MDRInt", "c410_MDRInt"]
#snapnum = 99
def main():
    #print_min_radius()
    plot_contamination_vs_radius()
    
def print_min_radius():
    print "name snapnum group M500 r/Mpc r/R500"
    for simname in simnames:
        for snapnum in snapnums:
                sim = snap.snapshot(basedir+simdir+simname, snapnum, mpc_units=mpc_units)
                h = sim.header.hubble
                z = sim.header.redshift
                
#==============================================================================
#                 ## Calculate distance of low-res gas cells
#                 sim.load_gasmass()
#                 sim.load_highresgasmass()
#                 ind = np.where(sim.hrgm < 0.5*sim.gasmass)[0] ## low-res cells            
#                 sim.load_gaspos()
#                 pos = sim.gaspos[ind]
#==============================================================================
        
                ## Calculate distance of Type 2 particles
                sim.load_posarr(parttypes=[2])
                pos = sim.posarr['type2']
                    
                for group in groups:
                    ## Distance relative to group
                    r = sim.nearest_dist_3D(pos, sim.cat.group_pos[group])
                    
                    ## Print the results
                    rmin = np.min(r) / h / (1.0+z) / 1000.0
                    R500 = sim.cat.group_r_crit500[group] / h / (1.0+z) / 1000.0
                    M500 = sim.cat.group_m_crit500[group] * 1e10 / h
                    print simname, snapnum, group, "{:.1e}".format(M500), "{:.1f}".format(rmin), "{:.1f}".format(rmin/R500)
    
def plot_contamination_vs_radius():
    fig = plt.figure(figsize=(7,7))
    plt.xscale('log')
    #plt.xlabel("r [ckpc/h]")
    plt.xlabel("$r / r_{500}$")
    plt.ylabel("Type 2 cumulative mass fraction")
    plt.axhline(0.01, c='0.5')
    bins = np.logspace(-1, 1, num=200)
    for simname in simnames:
        print "Starting sim", simname
        for snapnum in snapnums:
            print "snapshot", snapnum
            sim = snap.snapshot(basedir+simdir+simname, snapnum, mpc_units=mpc_units)
            #h = sim.header.hubble
            z = sim.header.redshift
            
            print "Loading positions..."
            sim.load_pos()
            print "Loading masses..."
            sim.load_mass()
            
            print "Calculating radial profiles..."
            for i, group in enumerate(groups):
                r = sim.nearest_dist_3D(sim.pos, sim.cat.group_pos[group])
                R = sim.cat.group_r_crit500[group]
                cur_bins = bins * R
                
                ## All
                massprof, bin_edges = np.histogram(r, weights=sim.mass, bins=cur_bins)
                cum_massprof = np.cumsum(massprof)
                
                ## Type 2
                massprof2, bin_edges = np.histogram(r[sim.species_start[2]:sim.species_end[2]], weights=sim.mass[sim.species_start[2]:sim.species_end[2]], bins=cur_bins)
                cum_massprof2 = np.cumsum(massprof2)
                
                #p = plt.step(cur_bins[1:], cum_massprof2 / cum_massprof, label=simname.replace("_","\_")+" R500={:.0f} ckpc/h".format(R500))
                #plt.axvline(R500, c=p[0].get_color(), ls='dashed')
                plt.step(bins[1:], cum_massprof2 / cum_massprof, label=simname.replace("_","\_")+" z={:.1f}".format(z)+" grp"+str(group))
                print "Done group", group
        
    plt.legend(frameon=True, loc='best', fontsize='small')
    fig.savefig("/data/curie4/nh444/project1/contamination_zooms.pdf", bbox_inches='tight')
    
if __name__=="__main__":
    main()