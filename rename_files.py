"""
Recursively rename all files in current directory and subdirectory
Usage:
rename_files.py <filename> <new filename>
"""

import sys, os

if len(sys.argv) != 3:
    print "Usage:"
    print "<filename> <new filename>"
name = sys.argv[1]
newname = sys.argv[2]

print "Converting"
print name
print "--->"
print newname
for root, dirs, files in os.walk("./"):
    if name in files:
        print root
        os.system("mv "+os.path.join(root, name)+" "+os.path.join(root, newname))
