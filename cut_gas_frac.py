#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 12 17:50:33 2016
Plot phase space plot for a given group
@author: nh444
"""
import snap
import os
import matplotlib.pyplot as plt
import numpy as np
from scipy.constants import eV
from scipy.stats import binned_statistic
keV = 1000.0*eV
UnitLength_in_cm = 3.085678e21 ## 1.0 kpc
UnitMass_in_g = 1.9884430e43 ## 1.0e10 solar masses

snapnum = 25

simdir = "/data/emergence4/deboras/Elliptical_ZoomIns/"
simname = "L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff"
label = "Best"
outdir = "/data/curie4/nh444/project1/L-T_relations/L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_"+str(snapnum).zfill(3)+"/"
mpc_units = False

#==============================================================================
# simdir = "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/"
# simname = "c96_box_Arepo_new"
# label = "c96 old arepo"
# simname = "c128_box_Arepo_new"
# label = "c128 old arepo"
# simname = "c160_box_Arepo_new"
# label = "c160 old arepo"
# #simname = "c256_box_Arepo_new"
# #label = "c256 old arepo"
# #simname = "c384_box_Arepo_new"
# #label = "c384 old arepo"
# #simname = "c448"
# #label = "c448"
# outdir = "/data/curie4/nh444/project1/L-T_relations/LDRIntRadioEffBHVel/"+simname+"_"+str(snapnum).zfill(3)+"/"
# mpc_units = True
#==============================================================================

outname = "cut_gas_fractions.pdf"
if not os.path.exists(outdir):
    os.mkdir(outdir)

sim = snap.snapshot(simdir+simname+"/", snapnum, mpc_units=mpc_units)
h = sim.header.hubble
z = sim.header.redshift
rhonorm = UnitMass_in_g / h / (UnitLength_in_cm / h / (1+z))**3 ## physical g cm^-3
sim.load_gasrho()
sim.get_temp()
sim.load_gaspos()
sim.load_gasmass()

totGasMass = []
totMass = []
DefCutMass = []
rasiaMass = []
rasia2Mass = []
rasia3Mass = []

#groupnums = [20,34,45,58,82,96,137,156,235]#,339]
groupnums = [0]
groupnums = [4,8,12,16,20]
groupnums = np.where(sim.cat.group_m_crit500 > 1e2*h)[0]
Ngroups = len(groupnums)

for i, groupnum in enumerate(groupnums):
    print "Group", groupnum, "({:d}/{:d})".format(i+1, Ngroups)
    
    M500 = sim.cat.group_m_crit500[groupnum]
    R500 = sim.cat.group_r_crit500[groupnum] / (1+z)
    T200 = sim.cat.group_T_crit200[groupnum]
    
    radius = sim.cat.group_r_crit500[groupnum]
    partrads_sq = ((sim.cat.group_pos[groupnum] - sim.gaspos)**2).sum(axis=1)
    ind = np.where(partrads_sq < radius*radius)[0]
    #ind = np.intersect1d(ind, np.where(np.logical_and((sim.temp >= 3e4*8.617342e-8) | (sim.gasrho <= 500*0.044*2.77536627e-8), sim.gasrho < 0.00085483/(1+sim.header.redshift)**3))[0])
    rho = sim.gasrho[ind] * rhonorm
    temp = sim.temp[ind]
    mass = sim.gasmass[ind] * 1e10 / h
    totMass.append(M500*1e10/h)
    totGasMass.append(np.sum(mass))
    
    Tcut = 3e4*8.617342e-8
    Rhocut = 0.00085483/(1+z)**3 * rhonorm

    gamma = 0.25 ## default is 0.25 from Rasia 2012
    N_def = 3e6 * (1e-27)**(0.25 - gamma) ## scale normalisation with gamma to match normalisation of Rasia 2012 at 1e-27 g/cm^3
    rasia = lambda rho: N_def * rho**gamma ## Rasia 2012 cut-off relation - gives temp in keV
    rasia2 = lambda rho: N_def * (M500 / 56000) * (910 / R500) * rho**gamma ## Rasia 2012 cut-off relation with scaling - gives temp in keV
    c = 4.0
    N = min(N_def, N_def * c * (M500 / 56000) * (910 / (R500 / (1+z))))
    rasia3 = lambda rho: N * rho**gamma
    
    cutInd = np.where((temp < Tcut) | (rho > Rhocut))[0]
    DefCutMass.append(np.sum(mass[cutInd]))
    cutInd = np.where(temp < rasia(rho))[0]
    rasiaMass.append(np.sum(mass[cutInd]))
    cutInd = np.where(temp < rasia2(rho))[0]
    rasia2Mass.append(np.sum(mass[cutInd]))
    cutInd = np.where(temp < rasia3(rho))[0]
    rasia3Mass.append(np.sum(mass[cutInd]))

totMass = np.asarray(totMass)
totGasMass = np.asarray(totGasMass)
DefCutMass = np.asarray(DefCutMass)
rasiaMass = np.asarray(rasiaMass)
rasia2Mass = np.asarray(rasia2Mass)
rasia3Mass = np.asarray(rasia3Mass)

fig, ax = plt.subplots(1,1, figsize=(10,10))
ax.set_xscale('log')
Mmin = np.min(totMass)
Mmax = np.max(totMass)
bins = np.logspace(np.log10(Mmin), np.log10(Mmax), num=30)

labels = ["default", "no scaling", "scaled", "scaled c=4"]
for i, mass in enumerate((DefCutMass, rasiaMass, rasia2Mass, rasia3Mass)):
    #ax.hist(totMass, bins=bins, weight=mass/totGasMass)
    #hist, bin_edges = np.histogram(totMass, bins=bins, weights=mass/totGasMass)
    hist, bin_edges, binnum = binned_statistic(totMass, mass/totGasMass, statistic='mean', bins=bins)
    plt.step(bin_edges[1:], hist, lw=2, label=labels[i])

if label is None:
    label = simname.replace("_", "\_")
ax.legend(loc='best')
ax.set_xlabel("M$_{500}$ [$M_{\odot}$]")
ax.set_ylabel("Excluded gas mass fraction")

plt.savefig(outdir+outname)
print "Saved to", outdir+outname
