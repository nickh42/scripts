import argparse

parser = argparse.ArgumentParser(description="Compare two or more Config files.",
                                 usage="configdiff [-diff] file1 file2 [file3 ...]",
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("fnames1", metavar="filenames", type=str, nargs=1,
                    help='Config file names, space-separated')
parser.add_argument("fnames2", metavar="filenames", type=str, nargs='+',
                    help=argparse.SUPPRESS)
parser.add_argument('-diff', action='store_false',
                    help='Show only config options which differ')
args=parser.parse_args()
fnames = args.fnames1 + args.fnames2

from pylab import *

nfiles = fnames.__len__()

cnames = []
cvals = []
allnames = []

print "-------------------------------------"
print "files:"

for fname in fnames:
  print fname

  f = open(fname, "r")

  corig = (f.read()).splitlines()
  corig.sort()

  cname = []
  cval = []

  for i in arange(corig.__len__()):
    c = corig[i].strip()
    
    if c.__len__() == 0:
      continue
    
    if c[0]!="#":
      c = c.split()
      c = c[0].split("=")
      
      assert c.__len__() >= 1
      
      cname.append(c[0])
      allnames.append(c[0])

      if c.__len__() >= 2:
        cval.append(c[1])
      else:
        cval.append("on") 

  cnames.append(cname)
  cvals.append(cval)

print

maxlength = 0
for j in arange(nfiles):
  for i in arange(cnames[j].__len__()):
    if cnames[j][i].__len__() > maxlength:
      maxlength = cnames[j][i].__len__()

allnames.sort()
allnames = unique(array(allnames))

print "Config options:"

for curname in allnames:
  curvals = []

  for i in arange(nfiles):
    ind = where(array(cnames[i])==curname)[0]
    
    if ind.size==1:
      ind = ind[0]
      curvals.append((cvals[i][ind]).ljust(20))
    elif ind.size==0:
      curvals.append("---".ljust(20))
    else:
      print cnames[i]
      print ind
      print array(cnames[i])[ind]
      assert False
      
  curline = curname.ljust(maxlength+3)+" "
  for i in arange(nfiles):
    curline += curvals[i]+" "
  
  if unique(array(curvals)).size == 1:
    if not args.diff:
      print curline
  else:
    print "\033[31m"+curline+"\033[0m"

print "-------------------------------------"
  
  
