; compute the stellar mass function for cosmological volumes 
set_plot, 'X'
name = "L40_512_NoDutyRadioWeak_HalfThermal_FixEta"
dir ="/data/emergence3/deboras/Elliptical_ZoomIns/"+name+"/"
outdir = "/data/curie4/nh444/project1/profiles/"+name+"/SMF/"
plotfile = outdir + name + "_smf.eps"
step = [4, 5, 6, 8, 10, 15, 20, 25]
;step = [12, 16, 20, 30, 40, 50, 67, 117]
nstep = n_elements(step)

h = 0.679 
massunit = 1e10/h 
lbox = 40.0/h 
vol = lbox^3 

for j=0, nstep-1 do begin 
   fof = dir +  'fof_subhalo_tab_'+ string(step[j], format = '(i3.3)')+'.hdf5'
   outfile = outdir + 'SMF_'+string(step[j], format = '(i3.3)')+'.dat'
   file_id =  H5F_OPEN(fof)
   header_id =  H5G_OPEN(file_id,'/Header')
   ngrp = H5A_READ(H5A_OPEN_NAME(Header_id, 'Nsubgroups_Total'))
   redshift =  H5A_READ(H5A_OPEN_NAME(Header_id, 'Redshift'))
   print, "redshift =", redshift
   H5G_CLOSE, header_id
   
;retall
   if (ngrp ne 0) then begin 

      GroupFirstSub = H5D_READ(H5D_OPEN(file_id, 'Group/GroupFirstSub'))

      subhalo_id = H5G_OPEN(file_id,'/Subhalo')
      subhalo_mass_type = H5D_READ(H5D_OPEN(file_id, 'Subhalo/SubhaloMassType'));total mass of particles of each type in subhalo
      subhalo_mass = H5D_READ(H5D_OPEN(file_id, 'Subhalo/SubhaloMass'));total mass of subhalo (all particle types)
      subhalo_mass_InRad = H5D_READ(H5D_OPEN(file_id, 'Subhalo/SubhaloMassInRad'))
      subhalo_mass_InRad_type = H5D_READ(H5D_OPEN(file_id, 'Subhalo/SubhaloMassInRadType'))
      
      H5G_CLOSE, subhalo_id 
   endif 
   H5F_CLOSE, file_id 

   ind = where(GroupFirstSub gt -1) ;only consider groups with a subhalo
   
   mstar = subhalo_mass_type(4, GroupFirstSub[ind])*massunit  ;total mass in stars (type 4)
   mstar_InRad = subhalo_mass_InRad_type(4, GroupFirstSub[ind])*massunit 
   
; binning 
   
   logm_min = 7.0
   logm_max = 13.0
   nbins = 61
   dlogm = (logm_max - logm_min)/(nbins-1)
   logm_bins = findgen(nbins)*dlogm + logm_min 
   logm_binsp =  findgen(nbins-1)*dlogm + logm_min + 0.5*dlogm
   
   smf_total = fltarr(nbins-1) 
   smf_inrad = fltarr(nbins-1) 
   
   ; this counts star particles in each mass bin and divides this by the total box volume and the width of the bin (dlogm)
   for i=0, nbins-2 do begin 
      index1 = where(alog10(mstar) ge logm_bins[i] and alog10(mstar) lt logm_bins[i+1]) 
      if(index1[0] ne -1) then begin 
         smf_total[i] = n_elements(index1)*1.0/dlogm/vol
      endif 
      index2 = where(alog10(mstar_inrad) ge logm_bins[i]and alog10(mstar_inrad) lt logm_bins[i+1]) 
      if(index2[0] ne -1) then begin 
         smf_inrad[i] = n_elements(index2)*1.0/dlogm/vol
      endif 
      
   endfor 

   close, 1
   openw, 1, outfile
   printf, 1, redshift 
   printf, 1, n_elements(logm_binsp)
   printf, 1, logm_binsp
   printf, 1, smf_total
   printf, 1, smf_inrad 
   close, 1 


   plottops = 0
;!p.multi = [0, 2, 2]
   if (plottops eq 0) then begin 
      window,0, retain = 2
      tk = 1
      device, decomposed = 0
      bw = 255
   endif else begin
      entry_device = !d.name
      set_plot,'PS'
      device, bits_per_pixel = 8, color = 1, filename = plotfile;, xsize = xs, ysize =ys, /inches, /LANDSCAPE
      tk = 3
      bw = 0
   endelse
   
   
   plot, logm_bins, alog10(smf_total), xtitle = 'log!l10!n(M!l*!n [M'+sunsymbol()+' ])', ytitle = 'log!l10!n (SMF[Mpc!u-3!n dex!u-1!n])', charsize = 1.5, xthick = tk, ythick = tk, thick = tk, charthick = tk, xrange = [7, 12]
   oplot, logm_bins, alog10(smf_inrad), linestyle = 2 
   ;oplot, [9, 10, 11], [-1.8, -2.2, -2.7], psym = 4
   
   labels = ['total', 'within r*']
   ;legend, labels, linestyle = [0, 2], thick = tk  
   
   if (plottops ne 0) then begin
      device, /close_file
      set_plot, entry_device
   endif
endfor 

end 


