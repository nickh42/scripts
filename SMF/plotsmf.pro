name = "L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff"
dir = "/data/curie4/nh444/project1/profiles/"+name+"/SMF/"
datadir = "/home/nh444/scripts/SMF/"
item = [name]
;step = [12, 16, 20, 30, 40, 50, 67, 117]
step = [4, 5, 6, 8, 10, 15, 20, 25]
nstep = n_elements(step)
z = [7, 6, 5, 4, 3, 2, 1, 0] 

;; EVEN redshifts
iselect = [1, 3, 5, 7] ;array indices for z[] and step[]
labels = ['z=6', 'z=4', 'z=2', 'z=0']
plotfile = dir + "SMF_zeven_"+name+".pdf"

;; ODD redshifts
;iselect = [0, 2, 4, 6] 
;labels = ['z=7', 'z=5', 'z=3', 'z=1']
;plotfile = dir + "SMF_zodd_"+name+".pdf"

ns = n_elements(iselect) 

nbins = 60 
logm_bins = fltarr(nbins) 
smf_total = fltarr(nbins) 
smf_inrad = fltarr(nbins) 


loadct, 39
  plottops = 1
;!p.multi = [0, 2, 2]
   if (plottops eq 0) then begin 
      window,0, retain = 2
      tk = 1
      device, decomposed = 0
      bw = 255
   endif else begin
      entry_device = !d.name
      set_plot,'PS'
      device, bits_per_pixel = 8, color = 1, /portrait,filename = plotfile, xsize = xs, ysize =ys, /inches
      tk = 3
      bw = 0
   endelse
colors = [bw, 60, 100, 250]


for j = 0, ns-1 do begin 
   file = dir + 'SMF_'+string(step[iselect(j)], format = '(i3.3)')+'.dat'
   print, "file = ", file
   datafile = datadir + 'SMF_Genel_data_z'+string(z[iselect(j)], format = '(i1.1)')+'.txt'
   print, "data = ", datafile
   close, 1
   nn = 0L
   openr, 1, file
   readf, 1, redshift 
   readf, 1, nn 
   logm_bins = fltarr(nn)
   smf_total = fltarr(nn)
   smf_inrad = fltarr(nn) 
   readf, 1, logm_bins
   readf, 1, smf_total
   readf, 1, smf_inrad 
   close, 1 

   openr, 1, datafile 
   readf, 1, ndata
   logm_data = fltarr(ndata) 
   logsmf_data = fltarr(ndata) 
   sig_high = fltarr(ndata)
   sig_low = fltarr(ndata) 

      for k=0, ndata-1 do begin
         if (z[iselect(j)] eq 0) then begin 
            readf, 1, x1, x2 
            logm_data[k] = x1
            logsmf_data[k] = x2 

         endif else begin 
            readf, 1, x1, x2, x3, x4 
            logm_data[k] = x1
            logsmf_data[k] = x2 
            sig_high[k] = x3
            sig_low[k] = x4
         endelse 
      endfor 
      close, 1
      ;; Plot data produced by smf_new.pro
      if(j eq 0) then begin 
         plot, logm_bins, alog10(smf_total), xtitle = 'log!l10!n(M!l*!n [M'+sunsymbol()+' ])', ytitle = 'log!l10!n (SMF[Mpc!u-3!n dex!u-1!n])', charsize = 1.5, xthick = tk, ythick = tk, thick = tk, charthick = tk, xrange = [7, 12], /nodata, yrange = [-5.0, -1.0]
      endif 
   oplot, logm_bins, alog10(smf_total), color = colors[j], thick = tk
   oplot, logm_bins, alog10(smf_inrad), color = colors[j], linestyle = 2, thick = tk

   ;; Plot observational data
   oplot, logm_data, logsmf_data, psym = 4, color = colors[j],symsize = 0.5
   if(z[iselect(j)] ne 0) then begin 
      oploterror, logm_data, logsmf_data, sig_high, /hibar, psym = 2, errcolor = colors[j],color = colors[j],symsize = 0.5
      oploterror, logm_data, logsmf_data, sig_low, /lobar, psym = 2, errcolor = colors[j], color= colors[j],symsize = 0.5
   endif 
endfor 


AL_Legend, labels, linestyle = 0, color = colors[0:ns-1], /top, /right, thick = tk, charsize=0.8, box = 0
AL_Legend, item, /bottom, /left, box = 0, charthick = tk, charsize = 0.8

if (plottops ne 0) then begin
      device, /close_file
      set_plot, entry_device
   endif
end 
