#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 25 12:12:07 2018

@author: nh444
"""
import numpy as np
import snap
from sim_python import cosmo

cm = cosmo.cosmology(0.6774, 0.3089)
# definitions in Arepo:
# The simulated timespan is mapped onto the integer interval [0,TIMEBASE]
# where TIMEBASE must be a power of 2
TIMEBINS = 29  # or 60 if ENLARGE_DYNAMIC_RANGE_IN_TIME (defined in allvars.h)
TIMEBASE = int(1 << TIMEBINS)  # 2^TIMEBINS

# from parameter file:
TimeBegin = 0.0078125  # in units of scale factor a
TimeMax = 1.0
MaxSizeTimeStep = 0.005  # in units of ln(a)

Timebase_interval = (np.log(TimeMax) - np.log(TimeBegin)) / TIMEBASE

nsnap = 200
# Get list of output times
times = np.loadtxt("/data/ERCblackholes1/nh444/zooms/MDRInt/c496_MDRInt/"
                   "Arepo/OutputList_688")[:nsnap, 0]

# Get actual times of snapshots
snaptimes = []
for snapnum in range(nsnap):
    sim = snap.snapshot("/data/ERCblackholes1/nh444/zooms/MDRInt/c496_MDRInt/",
                        snapnum, mpc_units=True, header_only=True)
    snaptimes.append(sim.header.time)


for i, time in enumerate(times):
    ti = int((np.log(time) - np.log(TimeBegin)) / Timebase_interval)

    # first determine maximum output interval based on MaxSizeTimestep
    timax = int(MaxSizeTimeStep / Timebase_interval)

    # make timax a power of 2 subdivision of TIMEBASE
    ti_min = TIMEBASE
    while(ti_min > timax):
        ti_min >>= 1  # divide by 2
    timax = ti_min

    # now round ti to the nearest multiple of timax
    multiplier = ti / float(timax)

    ti = int((multiplier + 0.5)) * timax
    nexttime = TimeBegin * np.exp(ti * Timebase_interval)

    # print "{:.5f}".format(np.abs(time - nexttime))
    # print "{:.5f} {:.5f}".format(time, nexttime)
    # print "{:.5f} {:.5f} {:.5f}".format(time, nexttime, snaptimes[i])
    # print "{:.5f} {:.5f} {:.5f}".format(time, snaptimes[i], snaptimes[i]-time)
    z = 1.0/time-1.0
    zsnap = 1.0/snaptimes[i]-1.0
    print "{:d} {:.5f} {:.5f} {:.5f}".format(i, z, zsnap, z - zsnap)
#    print "{:d} {:.5f} {:.5f} {:.5f}".format(
#                i, cm.lookback(z), cm.lookback(zsnap),
#                cm.lookback(z) - cm.lookback(zsnap))
