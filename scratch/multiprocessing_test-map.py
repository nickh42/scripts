#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 19 18:50:15 2018

@author: nh444
"""
from __future__ import print_function

import multiprocessing
from itertools import product
from functools import partial


def getFlux(args, fac=1):
    """This is the main processing function. It will contain whatever
    code should be run on multiple processors.

    """
    abund, temp = args

    return abund * temp * fac

if __name__ == '__main__':
    # Define the parameters to test
    abund = range(2, 6)
    temp = range(0, 10)

    params = list(product(abund, temp))
    for param in params:
        print(param)

    pool = multiprocessing.Pool()
    func = partial(getFlux, fac=2)
    results = pool.map(func, params)
    print(results)
