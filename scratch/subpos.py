#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 15 15:09:34 2018

@author: nh444
"""
import snap
import matplotlib.pyplot as plt
import h5py
import os
import numpy as np

basedir = "/home/nh444/data/project1/zoom_ins/MDRInt/"
simdir = "c128_MDRInt/"
mpc_units = True
snapnums = range(1, 20)[::-1]

col = ["#c7673e","#78a44f","#6b87c8","#934fce","#e6ab02","#be558f","#47af8d"]

subnum = -1
group = 0

fig, ax = plt.subplots()
x = []
y = []
x2 = []
y2 = []
maxR200 = 0.0
new_subnum = subnum
new_group = group
for i, snapnum in enumerate(snapnums):
    sim = snap.snapshot(basedir+simdir, snapnum, mpc_units=mpc_units)
    if i==0:
        new_mainhalo = sim.cat.group_firstsub[group]
    
    if subnum >= 0:
        x.append(sim.cat.sub_pos[subnum,0])
        y.append(sim.cat.sub_pos[subnum,1])
        x2.append(sim.cat.sub_pos[new_subnum,0])
        y2.append(sim.cat.sub_pos[new_subnum,1])
    elif group >= 0:
        x.append(sim.cat.group_pos[group,0])
        y.append(sim.cat.group_pos[group,1])
        
        new_group = sim.cat.sub_parent[new_mainhalo]
        x2.append(sim.cat.group_pos[new_group,0])
        y2.append(sim.cat.group_pos[new_group,1])
        
    #R200 = sim.cat.group_r_crit200[sim.cat.sub_parent[subnum]]
    #print "R200 =", R200
    #if R200 > maxR200:
        #maxR200 = R200
    ax.annotate(str(snapnum), xy=(x[-1], y[-1]), xytext=(6,0), textcoords='offset points', color=col[0], fontsize=10)
    ax.annotate(str(snapnum), xy=(x2[-1], y2[-1]), xytext=(6,0), textcoords='offset points', color=col[1], fontsize=10)
    
    filename = basedir+simdir+"merger_cat_"+str(snapnum).zfill(3)+".hdf5"
    if not os.path.exists(filename):
        raise IOError("File does not exist:"+filename)
    with h5py.File(filename, "r") as f:
        subnums = f['SubNum'].value[:,0]
        prog = f['MainProgenitor'].value[:,0]
        if subnum >= 0:
            new_subnum = prog[np.where(subnums == new_subnum)[0]]
        elif group >=0:
            new_mainhalo = prog[np.where(subnums == new_mainhalo)[0]]
    
ax.plot(x, y, ms=5, marker='o', mec='none', c=col[0], label=simdir.replace("_","\_"))
ax.plot(x2, y2, ms=8, marker='o', mfc='none', c=col[1], mec=col[1])
#xmin, xmax = ax.get_xlim()
#ymin, ymax = ax.get_ylim()
#ax.set_xlim((xmin+xmax)/2.0-maxR200, (xmin+xmax)/2.0+maxR200)
#ax.set_ylim((ymin+ymax)/2.0-maxR200, (ymin+ymax)/2.0+maxR200)
ax.legend()