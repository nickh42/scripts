#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 19 17:18:00 2018

@author: nh444
"""
from __future__ import print_function


def main():
    size = 36
    block_size = 4

    #serial_test(size, block_size)
    #multiprocessing_test(size, block_size)

def serial_test(size, block_size, verbose=False):
    import numpy as np
    import itertools

    X = np.random.random((size, size))  # A random 100x100 matrix
    tmp = np.zeros((size, size))  # Placeholder

    def fill_per_window(args):
        window_x, window_y = args

        if verbose:
            print(window_x, window_y)
        for i in range(window_x, window_x + block_size):
            for j in range(window_y, window_y + block_size):
                if verbose:
                    print("i, j =", i, j)
                tmp[i, j] = X[i, j]

    window_idxs = list(itertools.product(range(0, size, block_size),
                                         range(0, size, block_size)))

    for idx in window_idxs:
        fill_per_window(idx)

    print(np.array_equal(X, tmp))


#def multiprocessing_test(size, block_size):
size = 36
block_size = 4
import numpy as np
import itertools
from multiprocessing import Pool
from multiprocessing import sharedctypes

X = np.random.random((size, size))
result = np.ctypeslib.as_ctypes(np.zeros((size, size)))
shared_array = sharedctypes.RawArray(result._type_, result)

def fill_per_window(args):
    window_x, window_y = args
    tmp = np.ctypeslib.as_array(shared_array)

    for idx_x in range(window_x, window_x + block_size):
        for idx_y in range(window_y, window_y + block_size):
            tmp[idx_x, idx_y] = X[idx_x, idx_y]

window_idxs = list(itertools.product(range(0, size, block_size),
                                     range(0, size, block_size)))

pool = Pool()

tmp = np.ctypeslib.as_array(shared_array)
#from functools import partial
#func = partial(fill_per_window, block_size=block_size, tmp=tmp, X=X)
#pool.map(func, window_idxs)
pool.map(fill_per_window, window_idxs)
pool.close()
pool.join()
result = np.ctypeslib.as_array(shared_array)

print(np.array_equal(X, result))


#==============================================================================
# def fill_per_window(xy, block_size=4, tmp=None, X=None):
#     if tmp is None or X is None:
#         raise ValueError("Need tmp and X.")
#     window_x, window_y = xy
#     for idx_x in range(window_x, window_x + block_size):
#         for idx_y in range(window_y, window_y + block_size):
#             # print(idx_x, idx_y)
#             tmp[idx_x, idx_y] = X[idx_x, idx_y]
# 
#==============================================================================

if __name__ == "__main__":
    main()
