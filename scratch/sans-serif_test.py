#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Oct  8 11:49:08 2018

@author: nh444
"""

import matplotlib.pyplot as plt

#plt.rc('font',**{'family':'sans-serif','sans-serif':['Courier']})
plt.rc('font', family='sans-serif')
plt.rc('text', usetex=True)
#plt.rc('text.latex', preamble=r"\usepackage{helvet}, \usepackage{sansmath}, \sansmath")
##plt.rc('text.latex', preamble=r"\usepackage{helvet},"
##                              "\usepackage{sansmath},"
##                              "\sansmath")
fig, ax = plt.subplots()
ax.plot(range(5), range(5))
ax.set_xlabel("x label")
#ax.set_ylabel("$y_{label}$")
fig.show()
