"""
Test script to use simple C extension with NumPy.

LHW 19/04/18
"""

import numpy as np
from example_C_ext import example

# Arguments to pass to example function
arr1 = np.arange(30.0)
arr2 = np.arange(30)
val1 = 5
val2 = -np.pi
nthreads = 3

# Print argument info
print("Setup:")
print("arr1 = {}".format(arr1))
print("arr2 = {}".format(arr2))
print("arr1.dtype = {}, arr2.dtype = {}".format(arr1.dtype, arr2.dtype))
print("val1 = {}, val2 = {}".format(val1, val2))
print("type(val1) = {}, type(val2) = {}\n".format(type(val1), type(val2)))

# Call example function with arguments initialized above
print("Running C extension function:")
out_arr = example(arr1, arr2, val1, val2, nthreads)

# Print results
print("\nResults:")
print("out_arr = {}".format(out_arr))
print("out_arr.dtype = {}".format(out_arr.dtype))
print("out_arr.size = {}".format(out_arr.size))
