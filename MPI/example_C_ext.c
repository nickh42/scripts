/******************************************************************************
 * Python C extension module, using NumPy C API, to demonstrate some basic
 * usage.
 * 
 * Function example() takes arguments:
 *     arr1, 1D NumPy array, dtype=np.float64
 *     arr2, 1D NumPy array, dtype=np.int64
 *     val1, Python integer
 *     val2, Python float
 *     nthreads, Python integer
 * It returns a new array, made as an arithmetic combination of the arguments.
 * I've added some basic signal handling to allow keyboard interrupts when
 * the extension is running. 
 *
 * Parallelized using OpenMP. The GIL prevents separate threads from making
 * calls to the Python C API, so we first need to copy all of the Pythonic 
 * objects into C variables, then we can do any multithreaded work on those.
 * After the parallel block is finished we can copy back anything we need to
 * into the Python objects.
 *
 * Note: designed for Python 3. The API is slightly different for Python 2.
 *
 * LHW 19/04/18    
 * Updated 26/04/18
 *****************************************************************************/


#include <math.h>
#include <signal.h>
#include <unistd.h>
#include <omp.h>

#include "Python.h"
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include "numpy/arrayobject.h"

/* Prototypes */
static PyObject* example(PyObject *self, PyObject *args);
void sighandler(int sig);


/*---------------------------------------------------------------------------*/
/* MODULE FUNCTIONS ---------------------------------------------------------*/


/* Signal handler to allow interrupts when extension in use */
void sighandler(int sig)
{
    fprintf(stderr,"Signal = %s (SIGID = %d) received. \n",strsignal(sig), sig);
    exit(sig);
}


/* Python callable function */
static PyObject* example(PyObject *self, PyObject *args)
{
    /* Function arguments (Python objects) */
    PyArrayObject *arr1=NULL, *arr2=NULL;
    PyObject *val1=NULL, *val2=NULL, *val3=NULL;
    
    /* Output array to return in Python */
    PyArrayObject *out_arr=NULL;

    /* C variables */
    int ndims, i;
    npy_float64 *x, *z, *zi;
    npy_int64 *y;
    long a, nthreads;
    double b;

    /* Signal handler to allow interrupts */
    signal(SIGINT,sighandler);

    /* Parse arguments */
    if (!PyArg_ParseTuple(args, "O!O!O!O!O!", &PyArray_Type, &arr1, 
                                              &PyArray_Type, &arr2, 
                                              &PyLong_Type,  &val1,
                                              &PyFloat_Type, &val2,
                                              &PyLong_Type,  &val3)) 
    {
        return NULL;
    }

    /* Array dimensions */
    ndims = PyArray_DIM(arr1,0);
    if (PyArray_DIM(arr2,0) != ndims)
    { 
        /* For this function we want the arrays to have the same length */
        PyErr_SetString(PyExc_ValueError, "Input array dimensions don't match");
        return NULL; 
    }
    printf("ndims = %d\n", ndims);

    /* Copy scalar Python variables into C variables */
    a = PyLong_AsLong(val1);
    b = PyFloat_AsDouble(val2);
    nthreads = PyLong_AsLong(val3);
    printf("val1 = %ld, val2 = %lf, nthreads = %ld\n", a, b, nthreads);

    /* Allocate memory for C arrays, the copies of the Python arrays */
    x = (npy_float64 *)malloc(sizeof(npy_float64)*ndims);
    if (x==NULL) { free(x); printf("malloc error!\n"); exit(0); }
    y = (npy_int64 *)malloc(sizeof(npy_int64)*ndims);
    if (y==NULL) { free(y); printf("malloc error!\n"); exit(0); }
    z = (npy_float64 *)malloc(sizeof(npy_float64)*ndims);
    if (z==NULL) { free(z); printf("malloc error!\n"); exit(0); }

    /* Copy Python arrays into C arrays */
    for(i=0; i<ndims; i++)
    {
        x[i] = *(npy_float64 *)PyArray_GETPTR1(arr1, i);
        y[i] = *(npy_int64 *)PyArray_GETPTR1(arr2, i);
    }

    /* OpenMP set threads */
    omp_set_num_threads(nthreads);
    printf("Using max. of %d threads for calculation\n", omp_get_max_threads());

    /* Example calculation, note no Python API calls in parallel block */
    #pragma omp parallel for
    for (i=0; i<ndims; i++)
    {
        /* Arbitrary arithmetic combination (implicit type casting) */
        z[i] = a*x[i] + b*y[i];
    }
    
    /* Create output array (note: function handles reference counting) */
    out_arr = PyArray_FromDims(1, &ndims, NPY_FLOAT64);

    for (i=0; i<ndims; i++)
    {
        /* Final result should point to output Python array */
        zi = (npy_float64 *)PyArray_GETPTR1(out_arr, i);
        *zi = z[i];
    }

    return PyArray_Return(out_arr);
}


/*---------------------------------------------------------------------------*/
/* MODULE INITIALIZATION ----------------------------------------------------*/

/* Note (again) module designed for use with Python 3! */

static PyMethodDef example_methods[] = {
    {"example", example, METH_VARARGS, ""},
    {NULL, NULL, 0, NULL}         /* Sentinel */
};

static struct PyModuleDef moduledef = {
    PyModuleDef_HEAD_INIT,
    "example_C_ext",
    NULL,
    -1,
    example_methods,
    NULL,
    NULL,
    NULL,
    NULL,
};

PyMODINIT_FUNC *PyInit_example_C_ext(void)
{
   PyObject *m;

   m = PyModule_Create(&moduledef);
   if (m == NULL) { return NULL; }

   import_array();

   return m;

}
