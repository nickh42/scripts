"""
    setup.py file for example_c_ext.c

    Calling
    $python setup.py build_ext --inplace
    will build the extension library in the current directory.

    LHW 19/04/18
"""

from distutils.core import setup, Extension
import numpy
import os

os.environ["CC"] = "gcc"

ext_module = [ Extension('example_C_ext', 
                         sources=['example_C_ext.c'],
                         extra_compile_args=['-fopenmp'],
                         extra_link_args=['-lgomp'],
                         include_dirs = [numpy.get_include()]) ]

setup(name = 'Example C extension',
      version='1.0',
      description='Demonstrates basics of NumPy C API',
      ext_modules = ext_module) 
