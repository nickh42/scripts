def main():
    import os

    # General settings
    obs_dir = "/data/vault/nh444/ObsData/L-T/"
    N=2000
    delta = 500.0
    ref = "crit"
    use_fof = True ## Use halo file for M_delta and R_delta
    h_scale = 0.679
    filenames = []
    
    basedir = "/data/emergence3/deboras/Elliptical_ZoomIns/"
    simdirs = ["L40_512_NoDutyRadioWeak_HalfThermal_FixEta/",
            "L40_512_NoDutyRadioWeak_HalfThermal_FixEtaRadEff/",
            "L40_512_NoDutyRadioWeak_HalfThermal_FixEta_BHChangeVel/",
            "L40_512_NoDutyRadioWeak_HalfThermal_FixEta_BHChange/",
            "L40_512_LowDutyRadioWeak_HalfThermal_FixEta/",
            "L40_512_LowDutyRadioWeak_HalfThermal_FixEta_BHChangeVel/",
            "L40_512_TempDutyRadioWeak_HalfThermal_FixEta_BHChangeVel/",
            "L40_512_NoDutyRadioIntLow_HalfThermal_FixEta/",
            "L40_512_NoDutyRadioIntLow_HalfThermal_FixEta_BHChangeVel/",
            "L40_512_NoDutyRadioInt_HalfThermal_FixEta/",
            "L40_512_NoDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_Prex/",
            "L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff/"]
    labels = [r"NDRW",
          r"NDRWRadEff",
          r"NDRWBHVel",
          r"NDRWBHAcc",
          r"LDRW",
          r"LDRWBHVel",
          r"TDRWBHVel",
          r"NDRIntLow",
          r"NDRIntLowBHVel",
          r"NDRInt",
          r"NDRIntRadioEffBHVelPre",
          r"LDRIntRadioEffBHVel"]

    #simdirs = ["L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff/"]
    #outdir = "/data/curie4/nh444/project1/profiles/"
    simdir = "L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff/"
    #basedir = "/data/curie4/nh444/project1/zoom_ins/"
    #simdir = "LDRIntRadioEffBHVel/c256/"
    outdir = "/data/curie4/nh444/project1/L-T_relations/"
    #snapnums = [25, 24, 23, 22, 21, 20]
    snapnums = [20, 25]
    #snapnums = [25]
    #snapnums = [4, 5, 6, 8, 10, 15, 20, 25]
    masstab = False
    mpc_units = False

    ## Millenium runs
    #basedir = "/data/deboras/ewald/"
    #simdirs = ["m_10002_h_94_501_z3/csfbh/"]
    #snapnums = [63]
    #masstab = True
    #mpc_units = True

    outfile = "gas_fractions.pdf"
    
    #fracs = massfracs(basedir, simdir, outdir, snapnums, obs_dir, N=N, delta=delta, ref=ref, h_scale=h_scale, use_fof=use_fof, masstab=masstab, mpc_units=mpc_units)

    ''' Stellar mass fractions'''
    #fracs.get_gasmass_fracs()
    #plotfile = fracs.plot_mgasfrac_v_mass_v_redshift(for_paper=True) #requires get_gasmass_fracs

    ''' Combine plots into one pdf '''
    #filenames.append(plotfile)

    #os.system("gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile="+outfile+" "+" ".join(filenames))
    #print "Combined PDF saved to", outfile
    plot_massfrac_v_mass(outdir, simdir, snapnums, obs_dir, outfilename=outdir+"mass_fractions.pdf", z=0, h_scale=h_scale, delta=delta, for_paper=True, labels=labels)

## This function will combine the data output by fracs.get_gasmass_fracs() for multiple simdirs into one plot
def plot_massfrac_v_mass(basedir, simdir, snapnums, obs_dir, outfilename="mass_fractions.pdf", z=0, h_scale=0.679, delta=500, fout_base="default", for_paper=False, labels=None):
    import numpy as np
    import matplotlib.pyplot as plt
    import matplotlib.ticker as ticker
    import os
    import scipy.stats as stats
    
    Omega0 = 0.2726
    OmegaBaryon = 0.0456

    if fout_base == "default":
        fout_base = "mass_fracs_M"+str(int(delta))+"_"
    basedir = os.path.normpath(basedir)+"/"

    fig = plt.figure(figsize=(10,8))#width,height
    plt.ylabel(r"M/M$_{"+str(int(delta))+"\mathrm{c}}$", fontsize=18)
    plt.xlabel(r"M$_{"+str(int(delta))+"}$ [M$_{\odot}$]", fontsize=18)
    plt.xlim(1e11,1e15)
    plt.ylim(0.0, 0.2)
    plt.xscale('log')
    #plt.yscale('log')
    plt.setp(plt.gca().get_xticklabels(), fontsize=16)
    plt.setp(plt.gca().get_yticklabels(), fontsize=16)

    #Vikh = np.genfromtxt(obs_dir+"gas_frac_M-T_viklinin.txt")
    #Sun = np.genfromtxt(obs_dir+"sun_group_x-ray_properties.txt", usecols=np.arange(0,24))
    #Gastal = np.genfromtxt(obs_dir+"gastaldello_M500.txt")
    Gio = np.genfromtxt(obs_dir+"Giodini09_baryon_fracs.txt")

    if z <= 0.23:
        #plt.errorbar(Sun[:,13]*1e13*0.73/h_scale, Sun[:,16], xerr=[Sun[:,3],Sun[:,2]], yerr=[Sun[:,18],Sun[:,17]], c='#90AC28', mec='none', marker='D', linestyle='none', ms=3, label="Sun et al. 2008")# 0.012 <z< 0.12
        #plt.errorbar(Vikh[:,11]*1e14*0.72/h_scale, Vikh[:,15], xerr=Vikh[:,4], yerr=Vikh[:,16], c='#ff471a', mec='none', marker='D', linestyle='none', ms=3, label="Vikhlinin et al. 2006")#z < 0.23
        #plt.errorbar(Gastal[:,0]*0.7/h_scale, Gastal[:,6], xerr=[Gastal[:,1],Gastal[:,2]], yerr=[Gastal[:,7],Gastal[:,8]], c='#cc99ff', mec='none', marker='D', linestyle='none', ms=3, label="Gastaldello et al. 2007")# z< 0.08
        plt.errorbar(Gio[:,0]*0.72/h_scale, Gio[:,5], xerr=[Gio[:,1], Gio[:,2]], yerr=Gio[:,6], mec='green', c='green', mfc='none', marker='o', linestyle='none', ms=8)
        plt.errorbar(Gio[:,0]*0.72/h_scale, Gio[:,3], xerr=[Gio[:,1], Gio[:,2]], yerr=Gio[:,4], mec='red', c='red', mfc='none', marker='o', linestyle='none', ms=8)
        plt.errorbar(Gio[:,0]*0.72/h_scale, Gio[:,7], xerr=[Gio[:,1], Gio[:,2]], yerr=Gio[:,8], mec='black', c='black', mfc='none', marker='o', linestyle='none', ms=8, label="Giodini et al. 2009")

    plt.axhline(OmegaBaryon / Omega0, linestyle='dashed', c='black', label="Cosmic baryon fraction")
    ## For median profiles
    xmin, xmax = plt.gca().get_xlim()
    xmax = 3.1e13
    num = int((np.log10(xmax) - np.log10(xmin/10.0)) / 0.2)
    bins = np.logspace(np.log10(xmin/10.0), np.log10(xmax), num=num)
    #x = 10**((np.log10(bins[:-1]) + np.log10(bins[1:]))/2)
    x = bins[1:]

    simdir = os.path.normpath(simdir)+"/"
    for i, snapnum in enumerate(snapnums):
        print i
        data = np.genfromtxt(basedir+simdir+fout_base+str(snapnum).zfill(3)+".txt")
        M_delta = data[:,1]
        M_delta_gas = data[:,3]
        M_delta_stars = data[:,4]
        mask = np.where(M_delta*1e10 > xmax)
        if i==0: ls = 'solid'
        else: ls = 'dashed'
    
        plt.plot(M_delta[mask]*1e10, M_delta_gas[mask]/M_delta[mask], marker='o', mew=1.4, mfc='none', mec='green', linestyle='none')
        med, bin_edges, n = stats.binned_statistic(M_delta*1e10, M_delta_gas/M_delta, statistic='mean', bins=bins)
        plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c='green', label="Gas", lw=1.5, ls=ls)
        
        plt.plot(M_delta[mask]*1e10, M_delta_stars[mask]/M_delta[mask], marker='*', mew=1.4, mfc='none', mec='red', linestyle='none')
        med, bin_edges, n = stats.binned_statistic(M_delta*1e10, M_delta_stars/M_delta, statistic='mean', bins=bins)
        plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c='red', label="Stars", lw=1.5, ls=ls)
        
        plt.plot(M_delta[mask]*1e10, (M_delta_gas[mask]+M_delta_stars[mask])/M_delta[mask], marker='s', mew=1.4, mfc='none', mec='black', linestyle='none')
        med, bin_edges, n = stats.binned_statistic(M_delta*1e10, (M_delta_gas+M_delta_stars)/M_delta, statistic='mean', bins=bins)
        plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c='black', label="Baryons", lw=1.5, ls=ls)
        
    plt.legend(loc='upper right', frameon=False, borderaxespad=0.5, numpoints=1, fontsize=15, ncol=2)
    ## Set axis labels to decimal format not scientific when using log scale
    #plt.gca().yaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))
    
    if not for_paper:
        plt.savefig(outfilename)
    else:
        plt.savefig(outfilename, bbox_inches='tight')
    print "Plot saved to", outfilename
    return outfilename

class massfracs:
    def __init__(self, basedir, simdir, outdir, snapnums, obs_dir, N=20, delta=500.0, ref="crit", h_scale = 0.679, use_fof=True, snap_basename="snap", mpc_units=False, masstab=False):

        import os
        import cosmo
        
        self.basedir = os.path.normpath(basedir)+"/"
        self.simdir = os.path.normpath(simdir)+"/"
        self.outdir = os.path.normpath(outdir)+"/"
        if not os.path.exists(self.outdir+self.simdir):
            os.makedirs(self.outdir+self.simdir)
        self.snapnums = snapnums
        self.obs_dir = obs_dir
        self.N = N
        self.delta = delta
        self.ref = ref
        self.h_scale = h_scale
        self.use_fof = use_fof
        self.snap_basename = snap_basename
        self.mpc_units = mpc_units
        self.masstab = masstab

        import readsnap as rs
        self.z = []
        for snapnum in snapnums:
            header = rs.snapshot_header(self.basedir + self.simdir + "snap_" + str(snapnum).zfill(3))
            self.z.append(header.redshift)
        self.h = header.hubble
        self.Omega0 = header.omega_m
        self.OmegaBaryon = 0.0483 # This needs to be read from the parameter file
        self.cm = cosmo.cosmology(header.hubble, header.omega_m)

        # for gas mass fracs
        self.fout_base = "mass_fracs_M"+str(int(delta))+"_"

        self.colours = ["#d05352",
                        "#65b97f",
                        "#7979cf",
                        "#be53be",
                        "#9fd550",
                        "#c0933d",
                        "#44c7e2",
                        "#d9d458",
                        "#ce568b",
                        "#5c8532"]

    def get_gasmass_fracs(self):
        import numpy as np
        import snap

        delta = self.delta
        N = self.N
        
        for j, snapnum in enumerate(self.snapnums):
            ID = np.arange(N)
            M_delta = np.zeros(N)
            R_delta = np.zeros(N)
            M_delta_gas = np.zeros(N)
            M_delta_stars = np.zeros(N)

            h = self.cm.hubble_z(self.z[j])

            snapshot = snap.snapshot(self.basedir+self.simdir, snapnum, mpc_units=self.mpc_units, masstab=self.masstab, read_params=False)
            pos = getattr(snapshot.cat, 'group_pos')[:N]
            snapshot.load_mass()
            if self.use_fof and hasattr(snapshot.cat, 'group_m_crit'+str(int(delta))):
                M_delta = getattr(snapshot.cat, 'group_m_crit'+str(int(delta)))[:N]
                R_delta = getattr(snapshot.cat, 'group_r_crit'+str(int(delta)))[:N]
                #print "M_"+ref+","+str(delta)+"[0] =", M_delta[0]

            for i in range(0, N):
                if M_delta[i] == 0.0:
                    M_delta[i], R_delta[i] = snapshot.get_so_mass_rad(i, delta, ref=self.ref, only_sel=False)
                ind_gas = snapshot.get_particles_sphere(pos[i], R_delta[i], parttype=0)
                M_delta_gas[i] = np.sum(snapshot.mass[ind_gas])
                ind_stars = snapshot.get_particles_sphere(pos[i], R_delta[i], parttype=4)
                M_delta_stars[i] = np.sum(snapshot.mass[ind_stars])
                print "M_"+self.ref+","+str(int(delta))+"["+str(i)+"] =", M_delta[i]
                print "R_"+self.ref+","+str(int(delta))+"["+str(i)+"] =", R_delta[i]
                print "M_gas_"+self.ref+"_"+str(int(delta))+" =", M_delta_gas[i]
                print "M_stars_"+self.ref+"_"+str(int(delta))+" =", M_delta_stars[i]

            ## Sort
            sorter = np.argsort(M_delta)[::-1]
            ID = ID[sorter]
            M_delta = M_delta[sorter] / h
            R_delta = R_delta[sorter] / h
            M_delta_gas = M_delta_gas[sorter] / h
            M_delta_stars = M_delta_stars[sorter] / h
            pos = pos[sorter]
            np.savetxt(self.outdir+self.simdir+self.fout_base+str(snapnum).zfill(3)+".txt", np.c_[ID, M_delta, R_delta, M_delta_gas, M_delta_stars, pos[:,0], pos[:,1], pos[:,2]], fmt=['%d','%.5e','%.5e','%.5e','%.5e','%f','%f','%f'], header="ID, M_"+str(int(delta))+"(1e10Msun), R_"+str(int(delta))+"(kpc), M_"+str(int(delta))+"_gas(1e10Msun), M_"+str(int(delta))+"_stars(1e10Msun), x, y, z")

    def plot_mgasfrac_v_mass_v_redshift(self, for_paper=False):
        import numpy as np
        import matplotlib.pyplot as plt
        import matplotlib.ticker as ticker
        from matplotlib.backends.backend_pdf import PdfPages

        delta = self.delta
        N = self.N
        obs_dir = self.obs_dir
        fout_base = self.fout_base
        z = self.z
        outfilename = self.outdir+self.simdir+"gas_mass_fractions.pdf"
        
        Vikh = np.genfromtxt(obs_dir+"gas_frac_M-T_viklinin.txt")
        Sun = np.genfromtxt(obs_dir+"sun_group_x-ray_properties.txt", usecols=np.arange(0,24))
        Gastal = np.genfromtxt(obs_dir+"gastaldello_M500.txt")

        with PdfPages(outfilename) as pdf:
            for j, snapnum in enumerate(self.snapnums):
                fig = plt.figure(figsize=(12,13))

                data = np.genfromtxt(self.outdir+self.simdir+fout_base+str(snapnum).zfill(3)+".txt")
                M_delta = data[:,1]
                R_delta = data[:,2]
                M_delta_gas = data[:,3]

                plt.plot(M_delta*1e10, M_delta_gas/M_delta, 'o', label=str(N)+" most massive halos ("+self.simdir[8:-1]+")", mec='none', mfc='darkgreen')
                if not for_paper: plt.title(self.simdir[8:-1]+"\n z = "+str(z[j]))
                plt.xlim(3e12,1e15)
                #plt.ylim(0.004, 0.7)
                plt.xscale('log')
                plt.yscale('log')
                plt.ylabel(r"Gas mass fraction within r$_{"+str(int(delta))+"}$", fontsize=16)
                plt.xlabel(r"M$_{"+str(int(delta))+"}$ [$h^{-1}$ M$_{\odot}$]", fontsize=16)

                ## Set axis labels to decimal format not scientific when using log scale
                plt.gca().yaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))

                plt.axhline(0.1654, linestyle='dashed', c='black', label="WMAP 5yr + SN + BAO baryon fraction")
                plt.axhline(self.OmegaBaryon / self.Omega0, linestyle='dashed', c='blue', label=r"$\Omega_b / \Omega_m$")

                if z[j] <= 0.23:
                    plt.errorbar(Sun[:,13]*1e13*0.73, Sun[:,16], xerr=[Sun[:,3],Sun[:,2]], yerr=[Sun[:,18],Sun[:,17]], c='#90AC28', mec='none', marker='D', linestyle='none', ms=3, label="Sun et al. 2008")# 0.012 <z< 0.12
                    plt.errorbar(Vikh[:,11]*1e14*0.72, Vikh[:,15], xerr=Vikh[:,4], yerr=Vikh[:,16], c='#ff471a', mec='none', marker='D', linestyle='none', ms=3, label="Vikhlinin et al. 2006")#z < 0.23
                    plt.errorbar(Gastal[:,0]*0.7, Gastal[:,6], xerr=[Gastal[:,1],Gastal[:,2]], yerr=[Gastal[:,7],Gastal[:,8]], c='#cc99ff', mec='none', marker='D', linestyle='none', ms=3, label="Gastaldello et al. 2007")# z< 0.08

                plt.legend(loc='upper left', frameon=False, borderaxespad=1, numpoints=1, fontsize=14)
                if not for_paper:
                    pdf.savefig()
                else:
                    pdf.savefig(bbox_inches='tight')
                plt.close()
            print "Plot saved to", outfilename
        return outfilename

if __name__ == "__main__":
    main()
