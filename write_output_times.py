#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu May  3 15:20:04 2018

@author: nh444
"""
import numpy as np
outdir = "/data/ERCblackholes1/nh444/zooms/MDRInt/c512_MDRInt/Arepo/"
z = np.concatenate((np.linspace(0.0, 3.0, num=60, endpoint=False),
                    np.linspace(3.0, 5.0, num=20, endpoint=False),
                    np.linspace(5.0, 8.0, num=15, endpoint=False),
                    np.linspace(8.0, 10.0, num=5)))

ones = np.ones_like(z) ## flags

np.savetxt(outdir+"ExpansionList_100", np.column_stack((1.0 / (1.0 + z[::-1]), ones)), fmt=' %.14f %d ')

