#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon May 14 14:22:06 2018
Plot black hole accretion rate density (solar mass per year per unit volume)
as a function of redshift
@author: nh444
"""

import snap
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

h_scale = 0.679

basedir = "/data/curie4/nh444/project1/boxes/"
subdirs = ["L40_512_MDRIntRadioEff"]
mpc_units = False

snapnums = range(1, 26)

fig, ax = plt.subplots(figsize=(7,7))
ax.set_xlabel("$z+1$")
ax.set_ylabel("BHARD [M$_{\odot}$ yr$^{-1}$ Mpc$^{-3}$]")
ax.set_xscale('log')
ax.set_yscale('log')
ax.set_xlim(10, 1)
ax.xaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))

Ill = np.genfromtxt("/data/vault/nh444/ObsData/blackholes/BHARD_Illustris.csv", delimiter=",")
ax.plot(Ill[:,0], Ill[:,1], c='0.5', lw=2, label="Illustris $1820^3$")
Ill = np.genfromtxt("/data/vault/nh444/ObsData/blackholes/BHARD_Illustris_910.csv", delimiter=",")
ax.plot(Ill[:,0], Ill[:,1], c='0.5', ls='--', lw=2, label="Illustris $910^3$")

if mpc_units:
    UnitTime_in_s = 3.085678e+24 / 1e5
else:
    UnitTime_in_s = 3.085678e+21 / 1e5

for subdir in subdirs:
    z = []
    BHARD_tot = []
    for snapnum in snapnums:
        sim = snap.snapshot(basedir+subdir, snapnum, mpc_units=mpc_units)
        z.append(sim.header.redshift)
        vol = (sim.box_sidelength / 1000.0 / h_scale)**3.0 ## (cMpc)^3
        sim.load_bhmassdot()
        BHARD_tot.append(np.sum(sim.bhmassdot)*1e10 / h_scale / (UnitTime_in_s / 3600 / 24 / 365.25) / vol)
        
    ax.plot(np.array(z)+1.0, BHARD_tot, lw=3)
    
ax.legend(loc='best')
fig.savefig("/home/nh444/data/project1/blackholes/BHARD_MDRInt.pdf", bbox_inches='tight')