#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  7 13:21:46 2016
Plots BH mass as a function of redshift for a specific black hole particle
@author: nh444
"""

import snap
import readsnap as rs
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import glob, os
import scipy.stats as stats

title=None
h_scale = 0.679
radeff = [0.1]*20 ## BH radiative efficiency, used to calculate eddington rate
pres_crit = [True]*20 ## pressure criterion on/off (True/False)
minsnapnums = [0]*20 ## lowest snapshot number to track back to

outdir = "/home/nh444/data/project1/blackholes/"

basedirs = [#"/home/nh444/data/project1/zoom_ins/NDRIntRadioEffBHVelPrex/c256/",
            "/home/nh444/data/project1/zoom_ins/LDRIntRadioEffBHVelPrexAlpha1HighSeed/c256/",
            "/home/nh444/data/project1/zoom_ins/LDRIntRadioEffBHVelPrexAlpha1LowSeed/c256/",
            "/home/nh444/data/project1/zoom_ins/LDRIntRadioEffBHVelPrexAlphaVariable/c256/",
            "/home/nh444/data/project1/zoom_ins/LDRIntRadioEffBHVelPrexLowSeed/c256/",
            "/home/nh444/data/project1/zoom_ins/LDRIntRadioEffBHVelPrex/c256/",
            "/home/nh444/data/project1/zoom_ins/LDRIntRadioEffBHVel/c256/",
            ]
labels = [#"NDRIntRadioEffBHVelPrex",
          r"$\alpha = 1$ M$_{\mathrm{seed}}=8 \times 10^5$",
          r"$\alpha = 1$",
          r"$\alpha \propto n_H^2$",
          r"$\alpha = 100$ M$_{\mathrm{seed}}=10^4$",
          r"$\alpha = 100$",
          "fiducial",
             ]
suffix = "c256_Prex"
basedirs = ["/home/nh444/data/project1/zoom_ins/LDRIntRadioEffBHVel/c128_box_Arepo/",
            "",#"/home/nh444/data/project1/zoom_ins/LDRIntRadioEffBHVel/c128_new/",
            "",#"/home/nh444/data/project1/zoom_ins/LDRIntRadioEffBHVel/c128_new_ThermFeedbackFix/",
            "/home/nh444/data/project1/zoom_ins/LDRIntRadioEffBHVel/c128_new_FeedbackFix/",
            "/home/nh444/data/project1/zoom_ins/LDRIntRadioEffBHVel/c128_new_FeedbackFix_OldGrav/",
            ]
suffix = "c128_old_new_arepo"

basedirs = [#"/home/nh444/data/project1/zoom_ins/LDRIntRadioEffBHVel/c96_box_Arepo/",
            #"/home/nh444/data/project1/zoom_ins/LDRIntRadioEffBHVel/c96_box_Arepo_2/",
            "/home/nh444/data/project1/zoom_ins/LDRIntRadioEffBHVel/c96_box_Arepo_new/",
            "/home/nh444/data/project1/zoom_ins/LDRIntRadioEffBHVel/c96_box_Arepo_new_BHVel/",
            "/home/nh444/data/project1/zoom_ins/LDRIntRadioEffBHVel/c96_box_Arepo_new_DRAINGAS_2/",
            #"/home/nh444/data/project1/zoom_ins/LDRIntRadioEffBHVel/c96_box_Arepo_new_PM1024/",
            #"/home/nh444/data/project1/zoom_ins/LDRIntRadioEffBHVel/c96_box_Arepo_new_Ngb/",
            #"/home/nh444/data/project1/zoom_ins/LDRIntRadioEffBHVel/c96/",
            #"/home/nh444/data/project1/zoom_ins/LDRIntRadioEffBHVel/c96_ThermFeedbackFix/",
            #"/home/nh444/data/project1/zoom_ins/LDRIntRadioEffBHVel/c96_Arepo_March15/",
            #"/home/nh444/data/project1/zoom_ins/LDRIntRadioEffBHVel/c96_FeedbackFix/",
            #"/home/nh444/data/project1/zoom_ins/LDRIntRadioEffBHVel/c96_FeedbackFix_OldGrav/",
            #"/home/nh444/data/project1/zoom_ins/LDRIntRadioEffBHVel/c96_FeedbackFix_OldGrav_2/",
            ]
suffix = "c96_old_arepo_BHVel"
labels = [#"old arepo",
          #"old arepo no IND\_GRAV\_SOFT",
          "old arepo modified",
          "old arepo modified BHVel",
          "old arepo modified DRAINGAS 2",
          #"old arepo modified PMGRID 1024",
          "old arepo modified Ngb 256",
          "new arepo",
          #"new feedback fix",
          #"March Arepo",
          "+ fixed feedback",
          "+ old gravity solver",
          "+ RCUT + IND\_GRAV\_SOFT",
          ]

basedirs = ["/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c96_box_Arepo_new/",
           "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c96_box_Arepo_new_BHVel/",
           "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c96_box_Arepo_new_Ngb/",
           #"/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c96_box_Arepo_new_DRAINGAS_2/",
           #"/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c96_box_Arepo_new_PM1024/",
           "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c96_box_Arepo_new_LowSoft/",
           "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c96_box_Arepo_new_LowSoft_BHVel/",
           "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c96_box_Arepo_new_LowSoft_BHVel_Ngb/",
           #"/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c96_box_Arepo_new_LowSoft_BHVel_Ngb_PM1024_DRAINGAS2/",
           ]
suffix = "c96_old_arepo_BHVel_NumNgb"
labels = ["c96 Large Soft", "c96 Large Soft with BHVel", "c96 Large Soft 256 Ngb", #"c96 DRAINGAS 2",  "c96 PMGRID 1024",
          "c96 Small Soft", "c96 Small Soft BHVel", "c96 Small Soft BHVel Ngb 256", "c96 Small Soft BHVel 256 Ngb DRAINGAS=2 PM=1024"]


basedirs = ["/home/nh444/data/project1/zoom_ins/LDRIntRadioEffBHVel/c96_box_Arepo_new_LowSoft/",
            "/home/nh444/data/project1/zoom_ins/LDRIntRadioEffBHVel/c96_box_Arepo_new/",
            ]
suffix = "c96_small_large_softening"
title = "c96"
labels = ["small softening", "large softening",
          ]

basedirs = ["/data/emergence4/deboras/Elliptical_ZoomIns/L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff/",
          "/data/curie4/nh444/project1/boxes/L40_512_LDRIntRadioEff/",
          "/data/curie4/nh444/project1/boxes/L40_512_LDRIntRadioEff_SoftSmall/",
           ]
suffix = "box_softening"
title = None
labels = ["small softening", "large softening"]
labels = ["$\epsilon$(z=0)=0.3375 $z_{\epsilon}=3$", "$\epsilon$(z=0)=2.4 $z_{\epsilon}=5$", "$\epsilon$(z=0)=2.4 $z_{\epsilon}=1$"]

basedirs = ["/home/nh444/data/project1/zoom_ins/LDRIntRadioEffBHVel/c320_box_Arepo_new/"]
suffix = "c320"
title = "c320"
labels = ["c320"]

basedirs = ["/home/nh444/data/project1/zoom_ins/MDRInt/c448_MDRInt/"]
maxsnapnums = [24] ## lowest redshift snapshot number
#minsnapnums = [23]
radeff = [0.1] ## BH radiative efficiency
labels = ["c448 MDRInt"]
suffix = "c448_MDRInt"
title = "c448\_MDRInt"

#==============================================================================
# basedirs = ["/home/nh444/data/project1/zoom_ins/LDRIntRadioEffBHVel/c448_box_Arepo_new/",
#             "/home/nh444/data/project1/zoom_ins/LDRIntRadioEffBHVel/c448_box_Arepo_new_LowSoft/",
#             "/home/nh444/data/project1/zoom_ins/MDRInt/c448_MDRInt/"]
# maxsnapnums = [5, 5, 5]
# suffix = "c448_LowSoft_z6"
# title = "c448"
# labels = ["$\epsilon$(z=0)=2.4 $z_{\epsilon}=5$ ($\Delta t = 10$ Myr)", "$\epsilon$(z=0)=0.3375 $z_{\epsilon}=3$ ($\Delta t = 10$ Myr)", "$\epsilon$(z=0)=2.4 $z_{\epsilon}=5$ ($\Delta t = 25$ Myr)"]
# 
#==============================================================================
basedirs = ["/home/nh444/data/project1/zoom_ins/MDRInt/c448_MDRInt/",
            "/home/nh444/data/project1/zoom_ins/MDRInt/c448_MDRInt_noprescrit/",
            "/home/nh444/data/project1/zoom_ins/MDRInt/c448_MDRInt_noprescrit_SmallHaloSeed/",
            #"/home/nh444/project1/zoom_ins/Illustris_model/c448_Ill/",
            ]
maxsnapnums = [5, 
               9,
               9,
               #9,
            ] ## lowest redshift snapshot number
radeff = [0.1, 
          0.1,
          0.1,
          #0.2,
          ] ## BH radiative efficiency
suffix = "c448_MDRInt_Illustris"
suffix = "c448_MDRInt_noprescrit_SmallHaloSeed"
title = "c448\_MDRInt"
labels = ["Fiducial", 
          "no pres. crit.",
          "no pres. crit. + small seed halo mass",
          #"Illustris model",
          ]

#==============================================================================
# basedirs = [#"/home/nh444/data/project1/zoom_ins/MDRInt/Millennium_MDRInt_old/",
#             "/home/nh444/data/project1/zoom_ins/MDRInt/Millennium_MDRInt/",
#             "/home/nh444/data/project1/zoom_ins/MDRInt/Millennium_MDRInt_noprescrit/",
#             "/home/nh444/data/project1/zoom_ins/MDRInt/Millennium_MDRInt_noprescrit_LowSeedHaloMass/",
#             ]
# maxsnapnums = [5,
#                5,
#                5] ## lowest redshift snapshot number
# radeff = [0.1,
#           0.1,
#           0.1] ## BH radiative efficiency
# pres_crit = [#True,
#             True,
#             False,
#             False] ## pressure criterion on/off (True/False)
# suffix = "Millennium_MDRInt"
# title = "Millennium halo"
# labels = [#"Millennium old",
#           "fiducial",
#           "no pres. crit.",
#           "no pres. crit. Low Seed Halo Mass",
#           ]
#==============================================================================
basedirs = ["/home/nh444/data/project1/zoom_ins/MDRInt/c384_MDRInt/"]
maxsnapnums = [18] ## lowest redshift snapshot number
radeff = [0.1] ## BH radiative efficiency
suffix = "c384_MDRInt"
title = "c384\_MDRInt"
labels = ["Fiducial"]


verbosity=0
mpc_units = True
groupnum = 0
subhalo = -1 ## use <0 for most massive BH in group, otherwise its the most massive BH in this subhalo of the group
get_merger_times = True
plot_only_mergers = False ## If plotting mass, plot masses only at times of merger (before and after masses)
get_all_times = True ## Get all properties given in the details files
plot_redshift = True
plot_redshift_log = False
plot_lookback_time = False
xlims = None#[8.1, 5.9] ## list: [xmin, xmax] or None
plot_median = False ## plot median in bins for properties from details files which have very high frequency
edd_start_z = -1#10.35#10.0 ## starting redshift of theoretical eddington accreting BH - make negative to disable
edd_start_m = 1.84e5#2.2e5 ## starting mass (Msun/h) of theoretical eddington accreting BH
plot = ["mass", "feddsnap"]#, "mass"]#, "cs"]#, "mdot"] ## What to plot: max. 2 of:
##"mass", "mdot", "rho", "cs" (soundspeed), "hsml", "pres" (pressure criterion),
##"mdotsnap" (mdot from snapshots), "vel"/"dist" (velocity/distance relative to host subhalo),
##"gasvel" (BH velocity relative to surrounding gas), "fedd" (eddington fraction),
##"feddsnap" (eddington fraction at each snapshot)
outname = "BH_"+"_".join(plot)+"_vs_time_"+suffix+".pdf"

def main():
    assert len(maxsnapnums)==len(basedirs), "Wrong number of maxsnapnums given"
    assert len(labels) >= len(basedirs), "Not enough labels"
    if plot_only_mergers:
        assert get_merger_times, "Need get_merger_times==True if plot_only_mergers==True"
    fig, ax1 = plt.subplots(figsize=(17,7)) #8,7  17,7
    axes = [ax1]
    if len(plot)>1:
        ax2 = ax1.twinx()
        axes = [ax1, ax2]
        
    if title is not None:
        ax1.set_title(title)
    colours = ["#c7673e", "#78a44f", "#6b87c8","#934fce","#E6AB02","#be558f","#47af8d"]
    marker='o'
    lw = 3
    ms=8

    if plot_lookback_time or "mass" in plot:
        from astropy.cosmology import Planck15 ## Planck 2015 cosmology
        
    unitvel_in_cm_per_s = 1e5
    if mpc_units:
        UnitTime_in_s = 3.085678e+24 / unitvel_in_cm_per_s
    else:
        UnitTime_in_s = 3.085678e+21 / unitvel_in_cm_per_s
    Ref_BH_Pressure = 1.23845e+13 / 1.0e9 ## from log file and converted to kpc units from Mpc
    Ref_BH_Mass = 0.199056
    have_pres = False

    get_snap_props = []
    if "hsml" in plot:
        get_snap_props.append("hsml")
    if "vel" in plot:
        get_snap_props.append("vel")
    if "dist" in plot:
        get_snap_props.append("dist")
    if "gasvel" in plot:
        get_snap_props.append("gasvel")
    if "pressnap" in plot:
        get_snap_props.append("pres")
    if "mdotsnap" in plot or "feddsnap" in plot:
        get_snap_props.append("mdotsnap")
    if "feddsnap" in plot:
        get_snap_props.append("mass")
    for i, basedir in enumerate(basedirs):
        if basedir=="": continue ## skip
        snap_times, snap_masses, snap_props, mask, merger_times_all, merger_masses_all, details = get_BH_props_vs_time(basedir, maxsnapnums[i], minsnapnum=minsnapnums[i], get_merger_times=get_merger_times, get_snap_props=get_snap_props, verbosity=verbosity)
        if plot_redshift:
            snap_times2 = (1.0 / snap_times) - 1.0
            merger_times_all = (1.0 / merger_times_all) - 1.0
            det_times = (1.0 / details['time']) - 1.0
        elif plot_redshift_log:## 1 + z
            snap_times2 = (1.0 / snap_times)
            merger_times_all = (1.0 / merger_times_all)
            det_times = (1.0 / details['time'])
        elif plot_lookback_time:
            snap_times2 = Planck15.lookback_time((1.0 / snap_times) - 1.0).value
            merger_times_all = Planck15.lookback_time((1.0 / merger_times_all) - 1.0).value
            det_times = Planck15.lookback_time((1.0 / details['time']) - 1.0).value
        else:
            snap_times2 = snap_times
            det_times = details['time']
        if verbosity: print "snap_times2 =", snap_times2
        if plot_only_mergers:
            times = merger_times_all
            BHmasses = merger_masses_all
        else:
            times = snap_times2[mask]
            BHmasses = snap_masses[mask]
            if verbosity: print "mask =", mask
            if get_merger_times:
                times = np.concatenate((times, merger_times_all))
                BHmasses = np.concatenate((BHmasses, merger_masses_all))
            if get_all_times:
                times = np.concatenate((times, det_times))
                BHmasses = np.concatenate((BHmasses, details['BH_mass']))
        sorter = np.argsort(times)
        times = times[sorter]
        if verbosity: print "times =", times
        BHmasses = BHmasses[sorter]
        if verbosity: print "BHmasses =", BHmasses*1e10/h_scale
        for plotnum, curplot in enumerate(plot):
            ax = axes[plotnum]
            if plotnum > 0:##second y axis
                ls='dashed'
                label=None
            else:##first y axis
                ls='solid'
                label = labels[i]
            if curplot=="mass":
                ax.set_ylabel("BH sub-grid mass (M$_{\odot}$)")
                ax.set_yscale('log')
                if plot_only_mergers:
                    ax.plot(times, BHmasses*1e10/h_scale, ls=ls, label=label, lw=lw, marker=marker, mec='none', c=colours[i])
                else:
                    ax.plot(times, BHmasses*1e10/h_scale, ls=ls, label=label, lw=lw, c=colours[i])
                    
                ### Plot theoretical eddington accreting BH
                ## Get Eddington constant of proportionality in Gyr^-1
                k = (4 * np.pi * 6.6738e-8 * 1.67262178e-24 / (1.0 * 2.99792458e10 * 6.65245873e-25)) * 3600 * 24 * 365.25 * 1e9 #/ h_scale
                if edd_start_z >= 0:
                    if plot_redshift:
                        zlist = np.linspace(edd_start_z, np.min(snap_times2), num=200)
                        t = Planck15.age(zlist).value - Planck15.age(edd_start_z).value ## Gyr
                        ax.plot(zlist, edd_start_m*np.exp(k / 0.2 * t), c='0.5', lw=3, label="$\epsilon_r = 0.2$" if i==0 else "")
                        ax.plot(zlist, edd_start_m*np.exp(k / 0.1 * t), c='0.5', lw=3, ls='dashed', label="$\epsilon_r = 0.1$" if i==0 else "")
                        
                        #edd_start_z2 = 9.3
                        #edd_start_m2 = 6.8e5
                        #zlist = np.linspace(edd_start_z2, np.min(snap_times2), num=200)
                        #t = Planck15.age(zlist).value - Planck15.age(edd_start_z2).value ## Gyr
                        #ax.plot(zlist, edd_start_m2*np.exp(k / 0.1 * t), c='0.5', lw=3, ls='dashed', label="$\epsilon_r = 0.1$" if i==0 else "")

                #plt.plot(1.0/(1.0+times), BHmasses*1e10, label=labels[i], lw=lw, c=colours[i])
                #plt.plot(times, BHmasses*1e10, linestyle='none', marker='o', mec='none', ms=4)
                #if get_all_times: plt.plot(det_times, det_masses*1e10, linestyle='none', marker='+', ms=9, label="blackhole_details")
                #plt.plot(merger_times_all, merger_masses_all*1e10, linestyle='none', marker='o', mfc='none', mec='red', ms=7, mew=1, label="blackhole_mergers")
                #plt.plot(snap_times2, np.asarray(snap_masses)*1e10, ls='none', marker='o', mec='none', ms=8, label="snapshots", mfc=colours[i])
            value = None
            ## First get the pressure criterion if pressure is listed in the BH details files
            ## so that we can correct the accretion rate
            if (curplot=="pres" or pres_crit[i]) and 'pres' in details.dtype.names:
                have_pres = True
                press_thresh = Ref_BH_Pressure * (details['BH_mass'] / Ref_BH_Mass)
                tot_physical_press = details['pres'] / details['time']**3.0
                if mpc_units:
                    tot_physical_press /= 1.0e9
                prescrit = (tot_physical_press / press_thresh)**2
                prescrit_fac = np.copy(prescrit)
                prescrit_fac[prescrit_fac > 1.0] = 1.0
            elif curplot=="pres":
                raise IOError(basedir+" does not have pressure in details file.")
            elif pres_crit[i] and have_pres: ## previous directory had pressure but this one doesn't
                raise IOError(basedir+" does not have pressure in details file but previous directory does.")
            if curplot=="rho":
                ax.set_yscale('log')
                ax.set_ylabel("BH density (h$^{2}$ M$_{\odot}$ kpc$^{-3}$)")
                value = details['rho']*1e10
            elif curplot=="cs":
                ax.set_yscale('log')
                ax.set_ylabel("sound speed (km s$^{-1}$)")
                value = details['soundspeed'] ## physical km/s
            elif curplot=="mdot":
                if pres_crit[i] and 'pres' in details.dtype.names: ## correct accretion rate for pressure criterion
                    ax.set_ylabel("$\dot{\mathrm{M}}_{\mathrm{BH}}$ (h$^{-1}$ M$_{\odot}$ yr$^{-1}$)")
                    value = details['mdot']*1e10 * prescrit_fac / (UnitTime_in_s / 3600 / 24 / 365.25)
                elif not pres_crit[i]:
                    ax.set_ylabel("$\dot{\mathrm{M}}_{\mathrm{BH}}$ (h$^{-1}$ M$_{\odot}$ yr$^{-1}$)")
                    value = details['mdot']*1e10 / (UnitTime_in_s / 3600 / 24 / 365.25)
                else: ## pres_crit=True but no pressure in details file
                    ax.set_ylabel("$\dot{\mathrm{M}}_{\mathrm{BH}}$ (h$^{-1}$ M$_{\odot}$ yr$^{-1}$) (before pressure criterion)")
                    value = details['mdot']*1e10 / (UnitTime_in_s / 3600 / 24 / 365.25)
                ax.set_yscale('log')
                if plot[0] is "mdotsnap": ## if mdot from snapshots is plotted on left axis, give it the same axis limits
                    ax1.set_ylim(ax.get_ylim())
            elif curplot=="fedd":
                ax.set_yscale('log')
                ax.axhline(1.0, c='0.5', ls='dotted')
                medd = (4 * np.pi * 6.6738e-8 * 1.67262178e-24 / (radeff[i] * 2.99792458e10 * 6.65245873e-25)) * details['BH_mass'] * UnitTime_in_s / h_scale
                if pres_crit[i] and 'pres' in details.dtype.names: ## correct accretion rate for pressure criterion
                    ax.set_ylabel("$f_{edd}$")
                    value = details['mdot'] * prescrit_fac / medd
                elif not pres_crit[i]:
                    ax.set_ylabel("$f_{edd}$")
                    value = details['mdot'] / medd
                else: ## pres_crit=True but no pressure in details file
                    ax.set_ylabel("$f_{edd}$ (before pressure criterion)")
                    value = details['mdot'] / medd
            elif curplot=="pres":
                ax.set_yscale('log')
                ax.axhline(y=1.0, ls='dashed', c='0.7')
                ax.set_ylabel(r"$(\mathrm{P}_{ext}/\mathrm{P}_{ref})^2$")
                value = prescrit
                
            if plot_median and value is not None:## plot median of value
                if plot_redshift: binwidth = 0.02 #0.2
                elif plot_redshift_log: binwidth = 0.05 ## dex
                elif plot_lookback_time:
                    binwidth = 0.2
                    #det_times = np.array(det_times) ## coz astropy.cosmo does weird things
                else: binwidth = 0.015
                if plot_redshift_log:
                    bins = np.logspace(np.log10(np.min(det_times)), np.log10(np.max(det_times)), num=((np.log10(np.max(det_times)) - np.log10(np.min(det_times)))/binwidth))
                else:
                    bins = np.linspace(np.min(det_times), np.max(det_times), num=(np.max(det_times) - np.min(det_times))/binwidth)
                med, bin_edges, n = stats.binned_statistic(det_times, value, statistic='mean', bins=bins)
                ax.plot((bin_edges[:-1]+bin_edges[1:])/2.0, med, ls=ls, dash_capstyle='round', lw=lw, c=colours[i], label=label)
            elif value is not None:
                ax.plot(det_times, value, ls=ls, dash_capstyle='round', lw=lw, c=colours[i], label=label)
                
            if curplot=="hsml":
                ax.set_ylabel("BH\_Hsml (h$^{-1}$ kpc)")
                ax.plot(snap_times2[mask], snap_props['hsml'][mask], ls=ls, dash_capstyle='round', lw=lw, c=colours[i], marker=marker, ms=ms, mec='none', label=label)
            elif curplot=="vel":
                ax.set_ylabel("radial $v_{\mathrm{BH, sub}}$ (km s$^{-1}$)")
                vel = snap_props['vel'][mask] ## physical km/s
                ax.plot(snap_times2[mask], vel, ls=ls, dash_capstyle='round', lw=lw, c=colours[i], marker=marker, ms=ms, mec='none', label=label)
                if plot[0] is "cs": ## if sound speed plotted on left axis
                    ax.set_ylim(ax1.get_ylim())
            elif curplot=="dist":
                ax.set_ylabel("d$_{\mathrm{BH, sub}}$ (h$^{-1}$ physical kpc)")
                dist = snap_props['dist'][mask] ## kpc/h
                ax.plot(snap_times2[mask], dist, ls=ls, dash_capstyle='round', lw=lw, c=colours[i], marker=marker, ms=ms, mec='none', label=label)
            elif curplot=="gasvel":
                ax.set_ylabel("$v_{BH}$ (km s$^{-1}$)")
                gasvel = snap_props['gasvel'][mask] ## physical km/s
                ax.plot(snap_times2[mask], gasvel, ls=ls, dash_capstyle='round', lw=lw, c=colours[i], marker=marker, ms=ms, mec='none', label=label)
                if plot[0] is "cs": ## if sound speed plotted on left axis
                    ax.set_ylim(ax1.get_ylim())
            elif curplot=="pressnap":               
                ax.set_ylabel("BH\_Pressure")
                ax.set_yscale('log')
                #ax.plot(snap_times2, snap_props, ls=ls, dash_capstyle='round', lw=lw, c=colours[i], label=label)
                press_thresh = Ref_BH_Pressure * (snap_masses[mask] / Ref_BH_Mass)
                tot_physical_press = snap_props['pres'][mask] / snap_times[mask]**3.0
                prescrit = (tot_physical_press / press_thresh)**2
                ax.plot(snap_times2[mask], prescrit, ls=ls, dash_capstyle='round', lw=lw, c=colours[i], marker=marker, ms=ms, mec='none', label=label)
                ax.axhline(y=1.0, ls='dashed', c='0.7')
                ax.set_ylabel(r"$(\mathrm{P}_{ext}/\mathrm{P}_{ref})^2$")
            elif curplot=="mdotsnap":
                ax.set_ylabel("$\dot{\mathrm{M}}_{\mathrm{BH}}$ (h$^{-1}$ M$_{\odot}$ yr$^{-1}$) (snapshot)")
                ax.set_yscale('log')
                #print snap_props['mdot'][mask]*1e10 / (UnitTime_in_s / 3600 / 24 / 365.25)
                ax.plot(snap_times2[mask], snap_props['mdot'][mask]*1e10 / (UnitTime_in_s / 3600 / 24 / 365.25), ls=ls, dash_capstyle='round', lw=lw, c=colours[i], marker=marker, ms=ms, mec='none', label=label)
                if plot[0] is "pres":
                    ax.plot(snap_times2[mask], snap_props['mdot'][mask] / prescrit *1e10 / (UnitTime_in_s / 3600 / 24 / 365.25), ls=ls, dash_capstyle='round', lw=lw, c=colours[i], alpha=0.5, label=label)
                ## meddington as calculated in code - it multiplies by UnitTime_in_s to get the rate in code units of time
                medd = (4 * np.pi * 6.6738e-8 * 2.99792458e10 * 1.67262178e-24 / (radeff[i] * 2.99792458e10 * 2.99792458e10 * 6.65245873e-25)) * BHmasses * UnitTime_in_s / h_scale
                #ax.plot(times, 0.01*medd*1e10 / (UnitTime_in_s / 3600 / 24 / 365.25), lw=lw, c=colours[i], alpha=0.5)
                ## Get into units of Msun/h per year
                ax.plot(times, medd*1e10 / (UnitTime_in_s / 3600 / 24 / 365.25), lw=lw, c=colours[i], alpha=0.5, label="$\dot{M}_{\mathrm{Edd}}$")
                if plot[0] is "mdot": ## if mdot from details file is plotted on left axis, use the same axis limits
                    ax.set_ylim(ax1.get_ylim())
            elif curplot=="feddsnap":
                ax.set_ylabel("$f_{edd}$")
                #ax.set_ylim(0.0, 1.0)
                ax.set_yscale('log')
                medd = (4 * np.pi * 6.6738e-8 * 2.99792458e10 * 1.67262178e-24 / (radeff[i] * 2.99792458e10 * 2.99792458e10 * 6.65245873e-25)) * snap_props['mass'][mask] * UnitTime_in_s / h_scale
                ax.plot(snap_times2[mask], snap_props['mdot'][mask] / medd, ls=ls, dash_capstyle='round', lw=lw, c=colours[i], marker=marker, ms=ms, mec='none', label=label)

    if xlims is not None:
        xmax, xmin = xlims[0], xlims[1]
    else:
        xmin, xmax = ax1.get_xlim()
    if plot_redshift:
        ax1.set_xlabel("redshift")
        ax1.set_xlim(xmax, xmin) ## flip x axis
    elif plot_redshift_log:
        ax1.set_xlabel("1 + z")
        ax1.set_xlim(10, xmin) ## flip x axis
        ax1.set_xscale('log')
        import matplotlib.ticker as ticker
        ax1.xaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))
    elif plot_lookback_time:
        ax1.set_xlabel("Lookback Time [Gyr]")
        ax1.set_xlim(xmax, xmin) ## flip x axis
    else:
        ax1.set_xlabel("time")
    ax1.legend(frameon=False, loc='best')
    if len(axes) > 1:
        ax2.legend(frameon=False, loc='lower right')
    plt.savefig(outdir+outname, bbox_inches='tight')
    print "Saved to", outdir+outname
    
def get_BH_props_vs_time(basedir, maxsnapnum, minsnapnum=0, get_merger_times=True, get_snap_props=[], verbosity=0):
    """
    Trace back BH properties as a function of time.
    basedir is the directory path containing the simulation data
    maxsnapnum is the number of the lowest redshift snapshot
    minsnapnum is the highest redshift snapshot
    """
    
    assert minsnapnum<maxsnapnum, "minimum snapshot number must be less than maximum snapshot number"
    
    ## Load sim data of lowest redshift snapshot
    sim = snap.snapshot(basedir, maxsnapnum, mpc_units=mpc_units)
    final_time = sim.header.time
    if subhalo >= 0:
        ## get most massive BH in subhalo
        sim.get_hostsubhalos()
        sim.get_sub_bh_props()
        final_BHmass = sim.sub_bhmass1[sim.cat.group_firstsub[groupnum]+subhalo]
        final_BHid = sim.sub_bhids1[sim.cat.group_firstsub[groupnum]+subhalo]
        assert final_BHid > 0, "no BH bound to subhalo"
    else:
        ## get most massive BH within r500 of group centre
        sim.load_bhpos()
        sim.load_bhmass()
        sim.load_bhids()
        bhind = np.where(sim.nearest_dist_3D(sim.bhpos, sim.cat.group_pos[groupnum]) <= sim.cat.group_r_crit500[groupnum])[0]
        assert len(bhind) > 0
        bhidx = bhind[np.argmax(sim.bhmass[bhind])]
        final_BHid = sim.bhids[bhidx]
        final_BHmass = sim.bhmass[bhidx]
        del sim.bhpos
        del sim.bhmass

    ### First get the properties of the BH in the first snapshot ###
    
    snap_times = [sim.header.time]
    snap_masses = [final_BHmass]
    snap_props = []
    propnum = 0
    if len(get_snap_props) > 0: ### Get properties from first snapshot
        ## initialise array for snapshot properties
        snap_props = np.zeros(maxsnapnum+1-minsnapnum, dtype=[('hsml','f8'), ('vel','f8'), ('dist','f8'), ('gasvel','f8'), ('pres','f8'), ('mdot','f8'), ('mass','f8')])##change to np.empty 
        if verbosity: print "snap_props =", snap_props
    
        idx = np.where(sim.bhids==final_BHid)[0]
        assert len(idx)==1
        if "hsml" in get_snap_props:
            sim.load_bhhsml()
            snap_props['hsml'][0] = sim.bhhsml[idx]
            if verbosity: print "snap_props =", snap_props
            del sim.bhhsml
        if "vel" in get_snap_props:
            sim.load_bhvel(physical_velocities=True) ## load BH vel as physical since subhalo velocity is physical
            vBH = sim.bhvel[idx]
            #if idx > np.max(sim.cat.sub_offsettab[:,5]):
                #print "idx =", idx
                #print "sim.cat.sub_offsettab[:,5] =", sim.cat.sub_offsettab[:,5]
                #assert False, "BH is not a member of a subhalo. Cannot calculate relative velocity."
            #subnum = np.where(idx >= sim.cat.sub_offsettab[:,5])[0][0] ## For a given parttype i, sub_offsettab[j,i] is the number of particles of that type in subhalo j for all subhalos
            if not hasattr(sim, "hostsubhalos"):
                sim.get_hostsubhalos()
            subnum = sim.hostsubhalos[sim.species_start[5]+idx]
            #snap_props['vel'][0] = sim.nearest_dist_3D(vBH, sim.cat.sub_vel[subnum]) ## BH velocity relative to host subhalo velocity
            sim.load_bhpos()
            dx = sim.rel_pos(sim.bhpos[idx], sim.cat.sub_pos[subnum])[0]
            if dx[0] == dx[1] == dx[2] == 0.0:
                dx[0] = dx[1] = dx[2] = 1.0
            dv = sim.rel_pos(vBH, sim.cat.sub_vel[subnum])[0]
            snap_props['vel'][0] = np.dot(dv, dx / np.sqrt(np.dot(dx, dx)))
        if "dist" in get_snap_props:
            sim.load_bhpos()
            #if idx > np.max(sim.cat.sub_offsettab[:,5]):
                #print "idx =", idx
                #print "sim.cat.sub_offsettab[:,5] =", sim.cat.sub_offsettab[:,5]
                #assert False, "BH is not a member of a subhalo. Cannot calculate distance."
            #subnum = np.where(idx >= sim.cat.sub_offsettab[:,5])[0][0] ## For a given parttype i, sub_offsettab[j,i] is the number of particles of that type in subhalo j for all subhalos
            if not hasattr(sim, "hostsubhalos"):
                sim.get_hostsubhalos()
            subnum = sim.hostsubhalos[sim.species_start[5]+idx]
            snap_props['dist'][0] = sim.nearest_dist_3D(sim.bhpos[idx], sim.cat.sub_pos[subnum]) * sim.header.time ## BH velocity relative to host subhalo velocity
        if "gasvel" in get_snap_props:
            sim.load_bhvel(physical_velocities=True)
            vBH = sim.bhvel[idx]
            sim.load_bhpos()
            sim.load_gaspos()
            r = sim.nearest_dist_3D(sim.gaspos, sim.bhpos[idx])
            del sim.gaspos
            sim.load_bhhsml()
            h = sim.bhhsml[idx]
            ngbind = np.where(r <= h)[0] ## get gas cells within hsml
            if verbosity: print "Got", len(ngbind), "BH neighbours."
            u = r[ngbind] / h
            KERNEL_COEFF_1 = 2.546479089470
            KERNEL_COEFF_2 = 15.278874536822
            KERNEL_COEFF_5 = 5.092958178941
            wk = (1.0 - u)**3 * KERNEL_COEFF_5 / h**3
            wk[u < 0.5] = (KERNEL_COEFF_1 + KERNEL_COEFF_2*u[u < 0.5]**2 * (u[u < 0.5]-1.0)) / h**3
            sim.load_gasmass()
            gasmass = sim.gasmass[ngbind]
            del sim.gasmass
            sim.load_gasvel(physical_velocities=True)
            gasvel = np.empty((1,3))
            #gasvel = np.sum(gasmass * wk * sim.gasvel[ngbind, :], axis=1)# / np.sum(gasmass * wk) ## velocity of surround gas, "BH_SurroundingGasVel"
            for i in range(3):
                gasvel[:,i] = np.sum(gasmass * wk * sim.gasvel[ngbind, i]) / np.sum(gasmass * wk) ## velocity of surround gas, "BH_SurroundingGasVel"
            BHgasvel = np.sqrt(((vBH - gasvel)**2).sum(axis=1))
            snap_props['gasvel'][0] = BHgasvel ## BH velocity relative to surrounding gas velocity
        if "pres" in get_snap_props:
            sim.load_bhpres()
            snap_props['pres'][0] = sim.bhpres[idx]
            del sim.bhpres
        if "mdotsnap" in get_snap_props:
            sim.load_bhmassdot()
            snap_props['mdot'][0] = sim.bhmassdot[idx]
            del sim.bhmassdot
        if "mass" in get_snap_props:
            sim.load_bhmass()
            snap_props['mass'][0] = sim.bhmass[idx]
            del sim.bhmass
        if verbosity: print "snap_props =", snap_props
        if verbosity: print "snap_props[0] =", snap_props[0]


    ### Now get the BH properties at previous times ###

    ### blackhole_mergers and blackhole_details have higher frequency than the snapshots
    ### so we'll use these to get mass, mdot, density and sound speed

    ## Load blackhole_mergers files
    ## format is ThisTask, All.Time, id1, particlemass1, id2, BH_mass2
    ## (the first mass is dynamical and the second is subgrid)
    ## Usually id1 > id2 and is the ID given to the resulting BH particle
    ## But in some cases I have modified the output so that the BH takes the ID
    ## of the most massive BH and also output its subgrid mass as an extra column
    assert os.path.exists(basedir+"blackhole_mergers/"), "No blackhole_mergers files"
    filenames = glob.glob(basedir+"blackhole_mergers/*.txt")
    alist = []
    for f in filenames:
        #print "Opening", f[-8:-4]
        if os.path.getsize(f) > 0:
            data = pd.read_csv(f, sep=" ", header=None, engine='python')
            if data.shape[1] == 6:
                data.columns = ['Task', 'time', 'id1', 'pmass1', 'id2', 'submass2']
            elif data.shape[1] == 7: ## has extra column containing the subgrid mass of the first BH
                data.columns = ['Task', 'time', 'id1', 'pmass1', 'id2', 'submass2', 'submass1']
            else:
                raise IOError(f+" has a weird shape: "+str(data.shape))
            alist.append(data)
    ## Convert pandas dataframes to numpy record array
    mergers = pd.concat(alist).to_records(index=False)
    mergers = mergers[np.lexsort((mergers['submass2'], mergers['time']))[::-1]] ## Sort by time of merger, decreasing, and then by m2, decreasing
        
    ## Load blackhole_details files
    alist = []
    ## Get all filenames
    filenames = glob.glob(basedir+"blackhole_details/*.txt")
    for f in filenames:
        data = pd.read_csv(f, sep=" |=", header=None, engine='python')
        if data.shape[1] == 7:
            have_pres = False
            data.columns = ['astring', 'ID', 'time', 'BH_mass', 'mdot', 'rho', 'soundspeed']
        elif data.shape[1] == 8:
            have_pres = True
            data.columns = ['astring', 'ID', 'time', 'BH_mass', 'mdot', 'rho', 'soundspeed', 'pres']
        else:
            raise IOError(f+" has a weird shape: "+str(data.shape))
        data.drop('astring', axis=1, inplace=True)
        alist.append(data)
    ## Convert pandas dataframes to numpy record array
    BHdetails = pd.concat(alist).to_records(index=False)
    BHdetails = BHdetails[BHdetails['time'].argsort()[::-1]] ## Sort by time, decreasing

    ## Initialise variables
    BHid = final_BHid
    prev_time = final_time
    merger_times_all = np.empty((0))
    merger_masses_all = np.empty((0))
    det_times = np.empty((0))
    det_redshifts = np.empty((0))
    det_masses = np.empty((0))
    det_mdot = np.empty((0))
    det_rho = np.empty((0))
    det_cs = np.empty((0))
    if have_pres:
        det_pres = np.empty((0))
    mask = np.ones(maxsnapnum+1-minsnapnum, dtype=bool)
    j=1
    for i in range(minsnapnum, maxsnapnum)[::-1]:
        ## Loop over snapshots from maxsnapnum to minsnapnum
        snapname = basedir+"snap_"+str(i).zfill(3)
        head = rs.snapshot_header(snapname)
        ## Load BH ID numbers
        bhids = rs.read_block(snapname, "ID  ", parttype=5)
        if verbosity: print "\n", i, "t =", head.time, "z =", 1.0/head.time - 1.0
        if verbosity: print "len(bhids) =", len(bhids)
        
        ## Follow the ID of the most massive BH in each of the final BH's past mergers
        ## to end up with the ID of the BH progenitor in the previous snapshot
        if verbosity: print "BHid =", int(BHid)
        changeID_time = prev_time
        prev_changeID_time = prev_time
        changeID = True
        while changeID:
            print "BHid =", BHid
            old_BHid = BHid
            changeID, BHid, changeID_time, merge_times, merge_masses = get_BH_id(mergers, BHdetails, BHid,
                                                                                 head.time, prev_changeID_time,
                                                                                 get_merger_times=get_merger_times,
                                                                                 verbosity=verbosity)
            if changeID:
                prev_changeID_time = changeID_time
            if get_all_times:
                if changeID:
                    if verbosity: print "new BHid =", int(BHid)
                    if verbosity: print "adding between", prev_changeID_time, "and", changeID_time
                    ## Get all entries in BHdetails which occur between this and the previous merger (which occurs at a later time)
                    det_ind_all = np.where((BHdetails['ID'] == old_BHid) & (BHdetails['time'] < prev_changeID_time) & (BHdetails['time'] > changeID_time+1e-6))[0]
                    ## Note: the + 1e-6 is necessary due to rounding in the details files to 6 d.p.
                else:
                    if verbosity: print "BHid unchanged =", int(BHid)
                    if verbosity: print "adding between", prev_changeID_time, "and", head.time
                    ## Get all entries in BHdetails which occur between this snapshot and prev_changeID_time
                    det_ind_all = np.where((BHdetails['ID'] == BHid) & (BHdetails['time'] < prev_changeID_time) & (BHdetails['time'] > head.time))[0]
                det_times = np.concatenate((det_times, BHdetails['time'][det_ind_all]))
                det_redshifts = np.concatenate((det_redshifts, (1.0/BHdetails['time'][det_ind_all])-1.0))
                det_masses = np.concatenate((det_masses, BHdetails['BH_mass'][det_ind_all]))
                det_mdot = np.concatenate((det_mdot, BHdetails['mdot'][det_ind_all]))
                det_rho = np.concatenate((det_rho, BHdetails['rho'][det_ind_all]))
                det_cs = np.concatenate((det_cs, BHdetails['soundspeed'][det_ind_all]))
                if have_pres:
                    det_pres = np.concatenate((det_pres, BHdetails['pres'][det_ind_all]))
            if get_merger_times:
                assert len(merge_times) == len(merge_masses)
                merger_times_all = np.concatenate((merger_times_all, merge_times))
                merger_masses_all = np.concatenate((merger_masses_all, merge_masses))
            
        ### Now save the snapshot properties of the BH with this ID
        ## Get the index of the BH with the given ID
        idx = np.where(bhids == BHid)[0]
        if verbosity: print "idx =", idx
        propnum += 1
        if len(idx) > 1:
            assert False, "Multiple BH particles found with ID "+str(BHid)
        snap_times = np.append(snap_times, head.time)
        if len(idx) <= 0: ## no BH progenitor at this snapshot
            mask[j] = False ## set mask to False everywhere (except the first snap)
            snap_masses = np.append(snap_masses, 0.0) ## add zero to snap_masses so it remains the same length as mask
        else:
            ## Load BH masses for this snapshot and save to snap_masses:
            bhmass = rs.read_block(snapname, "BHMA")
            snap_masses = np.append(snap_masses, bhmass[idx])
            if verbosity: print "BH mass =", bhmass[idx]
            if np.argmax(bhmass) != idx and verbosity:
                print "\033[31m"+"WARNING: not the most massive BH in snapshot "+str(i)+"\033[0m"
                print "We have idx =", idx, "ID =", BHid, "mass =", bhmass[idx]
                print "But the most massive has idx =", np.argmax(bhmass), "ID =", bhids[np.argmax(bhmass)], "mass =", np.max(bhmass)
            if len(get_snap_props) > 0:
                if mpc_units: lenfac = 1000.0
                else: lenfac = 1.0
            if "hsml" in get_snap_props:
                snap_props['hsml'][propnum] = rs.read_block(snapname, "BHSM")[idx] * lenfac
            if "vel" in get_snap_props:
                vBH = rs.read_block(snapname, "VEL ", parttype=5, physical_velocities=True)[idx]
                cursim = snap.snapshot(basedir, i, mpc_units=mpc_units)## need halo cat (could just use readsubf)
                #if idx > np.max(cursim.cat.sub_offsettab[:,5]):
                    #print "idx =", idx
                    #print "sub_offsettab[:,5] =", cursim.cat.sub_offsettab[:,5]
                    #assert False, "BH is not a member of a subhalo. Cannot calculate relative velocity."
                #subnum = np.where(idx >= cursim.cat.sub_offsettab[:,5])[0][0] ## For a given parttype i, sub_offsettab[j,i] is the number of particles of that type in subhalo j for all subhalos
                if not hasattr(cursim, "hostsubhalos"):
                    cursim.get_hostsubhalos()
                subnum = cursim.hostsubhalos[cursim.species_start[5]+idx]
                #snap_props['vel'][propnum] = sim.nearest_dist_3D(vBH, cursim.cat.sub_vel[subnum]) ## BH velocity relative to host subhalo velocity
                bhpos = rs.read_block(snapname, "POS ", parttype=5)[idx]
                if mpc_units:
                    bhpos *= 1000.0
                dx = cursim.rel_pos(bhpos, cursim.cat.sub_pos[subnum])[0]
                if dx[0] == dx[1] == dx[2] == 0.0:
                    dx[0] = dx[1] = dx[2] = 1.0
                dv = cursim.rel_pos(vBH, cursim.cat.sub_vel[subnum])[0]
                snap_props['vel'][propnum] = np.dot(dv, dx / np.sqrt(np.dot(dx, dx)))
            if "dist" in get_snap_props:
                bhpos = rs.read_block(snapname, "POS ", parttype=5)[idx]
                if mpc_units:
                    bhpos *= 1000.0
                cursim = snap.snapshot(basedir, i, mpc_units=mpc_units)## need halo cat (could just use readsubf)
                #if idx > np.max(cursim.cat.sub_offsettab[:,5]):
                    #print "idx =", idx
                    #print "sub_offsettab[:,5] =", cursim.cat.sub_offsettab[:,5]
                    #assert False, "BH is not a member of a subhalo. Cannot calculate distance."
                #subnum = np.where(idx >= cursim.cat.sub_offsettab[:,5])[0][0] ## For a given parttype i, sub_offsettab[j,i] is the number of particles of that type in subhalo j for all subhalos
                if not hasattr(cursim, "hostsubhalos"):
                    cursim.get_hostsubhalos()
                subnum = cursim.hostsubhalos[cursim.species_start[5]+idx]
                snap_props['dist'][propnum] = sim.nearest_dist_3D(bhpos, cursim.cat.sub_pos[subnum]) * cursim.header.time ## BH distance relative to host subhalo
            if "gasvel" in get_snap_props:
                vBH = rs.read_block(snapname, "VEL ", parttype=5)[idx]
                bhpos = rs.read_block(snapname, "POS ", parttype=5)[idx] * lenfac
                gaspos = rs.read_block(snapname, "POS ", parttype=0) * lenfac
                r = sim.nearest_dist_3D(gaspos, bhpos)
                h = rs.read_block(snapname, "BHSM")[idx] * lenfac
                ngbind = np.where(r <= h)[0] ## get gas cells within hsml
                u = r[ngbind] / h
                KERNEL_COEFF_1 = 2.546479089470
                KERNEL_COEFF_2 = 15.278874536822
                KERNEL_COEFF_5 = 5.092958178941
                NORM_COEFF = 4.188790204786 ## 4/3 pi
                ReferenceGasPartMass = 0.00103512 ## for c96, from output log: "The mean cell mass, which is used as a reference, is 0.00103512"
                wk = (1.0 - u)**3 * KERNEL_COEFF_5 / h**3
                wk[u < 0.5] = (KERNEL_COEFF_1 + KERNEL_COEFF_2*u[u < 0.5]**2 * (u[u < 0.5]-1.0)) / h**3
                gasmass = rs.read_block(snapname, "MASS", parttype=0)[ngbind]
                weighted_numngb = np.sum(NORM_COEFF * gasmass * wk * h**3)
                numngb = weighted_numngb / ReferenceGasPartMass
                if verbosity: print "Got", numngb, "BH neighbours for h", h
                
                gasvel = rs.read_block(snapname, "VEL ", parttype=0)[ngbind]
                totgasvel = np.empty((1,3))
                for i in range(3):
                    totgasvel[:,i] = np.sum(gasmass * wk * gasvel[:, i]) / np.sum(gasmass * wk) ## velocity of surround gas, "BH_SurroundingGasVel"
                BHgasvel = np.sqrt(((vBH - totgasvel)**2).sum(axis=1))
                snap_props['gasvel'][propnum] = BHgasvel
            if "pres" in get_snap_props:
                snap_props['pres'][propnum] = rs.read_block(snapname, "BHPR")[idx] / lenfac**3  # lenfac**3 since BPP(i).BH_Pressure = GAMMA_MINUS1 * BPP(i).BH_U / BPP(i).BH_VolSum
            if "mdotsnap" in get_snap_props:
                snap_props['mdot'][propnum] = rs.read_block(snapname, "BHMD")[idx]
            if "mass" in get_snap_props:
                snap_props['mass'][propnum] = rs.read_block(snapname, "BHMA")[idx]

        prev_time = head.time
        j += 1
    
    ## Create structured array containing details data
    if have_pres:
        details = np.empty(len(det_masses), dtype=np.dtype([('time', det_times.dtype),('redshift', det_redshifts.dtype),('BH_mass', det_masses.dtype),('mdot', det_mdot.dtype),('rho', det_rho.dtype),('soundspeed', det_cs.dtype),('pres', det_pres.dtype)]))
        details['pres'] = det_pres
    else:
        details = np.empty(len(det_masses), dtype=np.dtype([('time', det_times.dtype),('redshift', det_redshifts.dtype),('BH_mass', det_masses.dtype),('mdot', det_mdot.dtype),('rho', det_rho.dtype),('soundspeed', det_cs.dtype)]))
    details['time'] = det_times
    details['redshift'] = det_redshifts
    details['BH_mass'] = det_masses
    details['mdot'] = det_mdot
    details['rho'] = det_rho
    details['soundspeed'] = det_cs
    return snap_times, snap_masses, snap_props, mask, merger_times_all, merger_masses_all, details

def get_BH_id(mergers, BHdetails, BHid, t1, t2, get_merger_times=True, verbosity=0):
    """ Follow the ID of the most massive BH in each of the given BH's past mergers 
        Note: mergers contains ('Task', 'time', 'id1', 'pmass1', 'id2', 'submass2' and possibly 'submass1')
        Note: BHdetails contains ('ID', 'time', 'BH_mass', 'mdot', 'rho', 'soundspeed')
    """
    ## get indices where BH was involved in a merger between now and the next (later) time
    ## Note we only need to consider mergers where id1=BHid, there should not be
    ## any mergers with id2=BHid at these times since this would have destroyed BHid
    inds = np.where((mergers['id1'] == BHid) & (mergers['time'] >= t1) & (mergers['time'] < t2))[0]
    if len(np.where((mergers['id2'] == BHid) & (mergers['time'] >= t1) & (mergers['time'] < t2))[0]) > 0:
        if verbosity: print "(ThisTask, All.Time, id1, particlemass1, id2, BH_mass2) =", mergers[np.where((mergers['id2'] == BHid) & (mergers['time'] > t1) & (mergers['time'] < t2))[0], :]
        assert False, "Something went wrong. Found merger that destroys this BH with id "+str(BHid)
        
    ## Do we have the sub-grid mass of the first BH?
    if "submass1" in mergers.dtype.names:
        have_submass = True
    else:
        have_submass = False
        
    ## Step backwards in time merger by merger and change current ID if it doesn't belong to the most massive BH in the merger
    merge_times = np.empty((0))
    merge_masses = np.empty((0))
    for i, ind in enumerate(inds):
        ## In mergers, the first mass is the particle mass, not subgrid
        ## So we need to find the subgrid mass in the details files
        merger_time = mergers['time'][ind]
        if verbosity: print "\nmerger time =", merger_time
        if verbosity>1: print "id1 =", mergers['id1'][ind], "id2 =", mergers['id2'][ind]
        if have_submass:
            BH_mass1 = mergers['submass1'][ind]
            time1 = mergers['time'][ind]
            BH_mass2 = mergers['submass2'][ind]
            time2 = mergers['time'][ind]
            if verbosity>1: print "BH_mass1 =", BH_mass1, "BH_mass2 =", BH_mass2
        else:
            if mergers['pmass1'][ind] < mergers['submass2'][ind]:
                ## If the first mass (particle) is less than the second mass (subgrid)
                ## then there's a chance the second BH is more massive and we'll
                ## need to switch to a different BHid
                ## we set avoid_merger_time=True so that we don't get an entry from 
                ## the details files at the merger time as it could possibly give the mass
                ## AFTER the merger not before it
                if verbosity>1: print "Setting avoid_merger_time"
                if verbosity>1: print mergers['pmass1'][ind], mergers['submass2'][ind]
                avoid_merger_time = True
            else:
                ## If the first mass (particle) is greater than the second mass (subgrid)
                ## then the first BH must be more massive than the second since particle
                ## mass is always less than or equal to subgrid mass. Therefore we won't
                ## need to switch to a different BHid
                ## We set avoid_merger_time=False but check below whether the resultant
                ## subgrid mass (BH_mass1) is actually the merged mass
                ## Note: I can set avoid_merger_time=True always but this has no effect on the results
                avoid_merger_time = False
            BH_mass1, time1 = get_BH_mass_at_merger(BHdetails, BHid, merger_time, avoid_merger_time=avoid_merger_time, verbosity=verbosity)
            
            ## Also get subgrid mass of second BH in merger - this cannot be
            ## the mass after the merger since this BH (id) is destroyed, 
            ## therefore we can set avoid_merger_time=False
            BH_mass2, time2 = get_BH_mass_at_merger(BHdetails, mergers['id2'][ind], merger_time, avoid_merger_time=False, verbosity=verbosity) # mergers['id2'][ind] is ID of second BH in merger
            if verbosity>1: print "delta_t1 =", merger_time - time1, "BH_mass1 =", BH_mass1*1e10, "BHid =", BHid
            if verbosity>1: print "delta_t2 =", merger_time - time2, "BH_mass2 =", BH_mass2*1e10, "BHid =", mergers['id2'][ind]
            if BH_mass1 > BH_mass2:
                ## Double check that previous mass entry is also less than BH_mass2 in case BH_mass1 is the merged mass
                BH_mass3, time3 = get_BH_mass_at_merger(BHdetails, BHid, merger_time, avoid_merger_time=True, verbosity=verbosity)
                if verbosity>1: print "delta_t3 =", merger_time - time3, "BH_mass3 =", BH_mass3*1e10, "BHid =", BHid
                if BH_mass1 >= BH_mass3+BH_mass2 and np.absolute(merger_time-time1)<1e-6:
                    ## if BH_mass1 is higher than BH_mass3+BH_mass2 (which suggests BH_mass1 is the merged mass not the mass just before the merger)
                    ## set the BH_mass to this secondary value
                    if verbosity: print "\n\n\033[31m"+"Warning: BH_mass1 ("+str(BH_mass1)+") > BH_mass3 ("+str(BH_mass3)+") + BH_mass2 ("+str(BH_mass2)+") at\033[0m"
                    if verbosity: print "\033[31m"+"t1="+str(time1), "t2="+str(time2), "t3="+str(time3)+"\033[0m"
                    #if verbosity: print "\033[31m"+"avoid_merger_time is set to", avoid_merger_time, "\033[0m"
                    BH_mass1 = BH_mass3
                    time1 = time3
        
        if get_merger_times: ## append merger time and mass to output list
            ## Check if another merger occurs with this ID at an identical time
            ## and if it does, only add the merger time if the ID changes
            dup = np.where(mergers['time'][inds] == merger_time)[0]
            if len(dup) > 1:
                if verbosity: print "dup =", dup, len(dup)
                if BH_mass1 < BH_mass2:
                    merge_times = np.append(merge_times, merger_time) ## Use merger time for mass after merger
                    merge_masses = np.append(merge_masses, BH_mass1+BH_mass2)
                    if plot_only_mergers:
                        merge_times = np.append(merge_times, time1) ## But use details time for mass before merger
                        merge_masses = np.append(merge_masses, BH_mass1)
            else:
                if verbosity: print "adding merger at t=", merger_time
                merge_times = np.append(merge_times, merger_time)
                if verbosity>1: print "merge_times =", merge_times
                merge_masses = np.append(merge_masses, BH_mass1+BH_mass2)
                if verbosity>1: print "merge_masses =", merge_masses
                if plot_only_mergers:
                    if BH_mass1 > BH_mass2:
                        merge_times = np.append(merge_times, time1)
                        merge_masses = np.append(merge_masses, BH_mass1)
                    elif BH_mass2 > BH_mass1:
                        merge_times = np.append(merge_times, time2)
                        merge_masses = np.append(merge_masses, BH_mass2)

        ## when merger companion is more massive, break the while loop in
        ## get_BH_props_vs_time and start looking at the new BH ID
        if BH_mass1 < BH_mass2:
            BHid_new = mergers['id2'][ind]
            if verbosity: print "ID changed. BH_mass1 =", BH_mass1, "BH_mass2 =", BH_mass2
            return True, BHid_new, merger_time, merge_times, merge_masses

    return False, BHid, t1, merge_times, merge_masses
        
def get_BH_mass_at_merger(BHdetails, BHid, merger_time, avoid_merger_time=False, lookup=0, verbosity=0):
    """
    This functions reads the subgrid mass of a BH (with id BHid) from BHdetails at
    the given merger time (or as soon before the merger time as possible if avoid_merger_time==True)
    We have to be careful because the times in BHdetails are given to 6 d.p. and so
    an entry found for the given merger time (to 6 d.p.) could list the mass immediately after
    the merger rather than immediately before it. If avoid_merger_time is set to True,
    then we ignore any entry at the merger time and look for the next soonest
    ## lookup is the index of the lookup time i.e. 0 is the first entry in BHdetails occurring before merger_time
    """
    err = 1e-7 # see comments below
    ## get the entries occuring at or before the merger time
    det_ind = np.where((BHdetails['ID'] == BHid) & (BHdetails['time'] <= merger_time+err))[0]
    ## It has to include entries at the merger time otherwise the entry at the next earliest time might be very low mass if a merger occurred in between
    ## However, sometimes these entries give the mass AFTER the merger (we want the mass just before)
    ## so we don't use entries at the merger time if avoid_merger_time==True
    ## The err=1e-7 is necessary because sometimes np.where will not count two numbers as being equal even though they are the same to the 6 d.p. given in the details/merger files
    ## alternatively I could use np.around(BHdetails['time'], decimals=6)
    if len(det_ind) < 1:
        print "ERROR: BHid =", BHid, "t_merger =", merger_time
        assert False, "Could not find BHid before merger time in details files"
    t_dist = merger_time - BHdetails['time'][det_ind] ## how long before the merger this entry occurred
    if avoid_merger_time:## find next nearest time before merger
        t_dist2 = np.sort(t_dist)
        det_idx = det_ind[np.argmin(t_dist)]
        for i in range(len(t_dist2)):
            if verbosity>1: print "t_dist2[i] =", t_dist2[i]
            if t_dist2[i] < err*10.0:
                det_idx = det_ind[np.argsort(t_dist)[i+1]]
            else:
                break
    elif lookup>0:
        det_idx = det_ind[np.argsort(t_dist)[lookup]]
    elif lookup==0:
        det_idx = det_ind[np.argmin(t_dist)] ## Get BH properties at nearest possible time before merger
    else:
        assert False, "Invalid lookup value = "+str(lookup)
    BH_mass = BHdetails['BH_mass'][det_idx]
    det_time = BHdetails['time'][det_idx]
    return BH_mass, det_time
    
if __name__ == "__main__":
    main()
    