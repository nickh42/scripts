#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed May 30 11:48:47 2018

@author: nh444
"""
from __future__ import print_function

import numpy as np
import matplotlib.pyplot as plt
import snap


def main():
    h_scale = 0.679
    outdir = "/data/curie4/nh444/project1/blackholes/"
    
    basedir = "/data/curie4/nh444/project1/"
    simdirs = ["boxes/L40_512_MDRIntRadioEff/"]
    zooms = [
            "c96_MDRInt", "c128_MDRInt", "c160_MDRInt", "c192_MDRInt",
            "c224_MDRInt", "c256_MDRInt", "c288_MDRInt", "c320_MDRInt",
            "c352_MDRInt", "c384_MDRInt", "c448_MDRInt", "c480_MDRInt",
            "c512_MDRInt",
            "c112_MDRInt", "c144_MDRInt", "c176_MDRInt", "c208_MDRInt",
            "c240_MDRInt", "c272_MDRInt", "c304_MDRInt", "c336_MDRInt",
            "c368_MDRInt", "c400_MDRInt", "c410_MDRInt", "c432_MDRInt",
            "c464_MDRInt", "c496_MDRInt_SUBF",
            ]
    simdirs += ["zoom_ins/MDRInt/" + zoom for zoom in zooms]
    snapnums = [25, 20, 15, 10, 8]
    snapnums = [25, 24, 23, 21]
    snapnums = [25, 20]
    snapnum_lists = [[25]*14 + [99]*14,
                     [24]*14 + [95]*14,
                     [23]*14 + [91]*14,
                     [22]*14 + [87]*14,
                     [21]*14 + [83]*14,
                     [20]*14 + [79]*14,
                     [19]*14 + [75]*14,
                     [18]*14 + [71]*14,
                     [17]*14 + [67]*14,
                     [16]*14 + [63]*14,
                     [15]*14 + [59]*14,
                     ]
#    snapnum_lists = [[20]*14 + [79]*14]
    radeff = 0.1  # BH radiative efficiency, used to calculate eddington rate.
    chi = 0.01  # eddington fraction below which BHs are in the radio-mode.

    M500lim = [3e13, 1e16]
    Epow = -0.5  # the lower mass limit is scaled by E(z) to this power.
    centrals_only = True  # only most massive BH in each central halo
    normalise = True  # plot probability density, otherwise number per bin
    verbose = False

    fig, ax = plt.subplots(figsize=(10, 7))
    # ax.set_yscale('log')
    ax.set_xlabel(r"$\mathrm{log_{10}(f_{edd})}$")
    if normalise:
        ax.set_ylabel(r"$\phi(\mathrm{f_{edd}})$")
    else:
        ax.set_ylabel(r"N($\mathrm{f_{edd}}$)")

    ax.axvline(np.log10(chi), ls='--', c='0.5', lw=2)

    bins = np.linspace(-8, 0, num=20)

    for snapnums in snapnum_lists:
        # Eddington ratios for all BHs at this snapshot number
        fedd = []
        redshift = None
        for simidx, simdir in enumerate(simdirs):
            if verbose:
                print("Loading snapshot")
            sim = snap.snapshot(basedir+simdir, snapnums[simidx],
                                mpc_units=True)
            if verbose:
                print(simdir, "z = {:.1f}".format(sim.header.redshift))
            if redshift is None:
                redshift = sim.header.redshift
            elif abs(sim.header.redshift - redshift) > 0.01:
                raise ValueError("Redshift doesn't match.")

            if centrals_only:
                M500 = sim.cat.group_m_crit500 * 1e10 / h_scale
                Ez = sim.cm.E_z(redshift)
                grpind = np.where((M500 > M500lim[0] * Ez**Epow) &
                                  (M500 < M500lim[1]))[0]
                if verbose:
                    print("Found {:d} haloes in mass range.".format(len(grpind)))
                sim.get_sub_bh_props()
                # Get mass of most massive BH in each central halo.
                bhmass = sim.sub_bhmass1[sim.cat.group_firstsub[grpind]]
                # Ignore haloes without a blackhole (zero mass)
                ind = np.where(bhmass > 0.0)[0]
                bhmass = bhmass[ind]
                bhmdot = sim.sub_bhmassdot1[sim.cat.group_firstsub[grpind[ind]]]

#                # Add second most massive BH
#                bhmass2 = sim.sub_bhmass2[sim.cat.group_firstsub[grpind]]
#                ind = np.where(bhmass2 > 0.0)[0]
#                bhmass = np.concatenate((bhmass, bhmass2[ind]))
#                bhmdot2 = sim.sub_bhmassdot2[sim.cat.group_firstsub[grpind[ind]]]
#                bhmdot = np.concatenate((bhmdot, bhmdot2))
#                # Add third most massive BH
#                bhmass3 = sim.sub_bhmass3[sim.cat.group_firstsub[grpind]]
#                ind = np.where(bhmass3 > 0.0)[0]
#                bhmass = np.concatenate((bhmass, bhmass3[ind]))
#                bhmdot3 = sim.sub_bhmassdot3[sim.cat.group_firstsub[grpind[ind]]]
#                bhmdot = np.concatenate((bhmdot, bhmdot3))
            else:
                sim.load_bhmass()
                bhmass = sim.bhmass
                sim.load_bhmassdot()
                bhmdot = sim.bhmassdot

            # Get Eddington accretion rate in code units
            medd = ((4 * np.pi * 6.6738e-8 * 1.67262178e-24 /
                    (radeff * 2.99792458e10 * 6.65245873e-25)) *
                    bhmass * sim.unittime_in_s / sim.header.hubble)
            fedd = np.concatenate((fedd, bhmdot / medd))

        # Plot the eddington ratio distribution for the current snapshot.
        ind = np.where((fedd > 0.0) & np.isfinite(fedd))[0]
        # print("max of fedd =", np.max(fedd[ind]))
        hist, bin_edges = np.histogram(np.log10(fedd[ind]), bins=bins,
                                       density=normalise)
        ax.step(bin_edges[1:], hist, where='pre', lw=2,
                label="z = {:.1f} (N = {:d})".format(redshift, len(ind)))

        # Count what fraction are in the radio-mode vs the quasar-mode
        print("{:.2f} in radio-mode at z = {:.1f}".format(
                float(sum(fedd[ind] < chi)) / len(ind), redshift))

    ax.legend(loc='upper left', frameon=True)

    #fig.savefig(outdir+"BH_fedd.pdf", bbox_inches='tight')    


if __name__ == "__main__":
    main()
