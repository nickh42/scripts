import snap
import numpy as np
from numpy import ma
import matplotlib.pyplot as plt

## Names of directories containing snapshots
names = ["L40_512_NoDutyRadioIntLow_HalfThermal_FixEta_BHChangeVel",
        "L40_512_NoDutyRadioWeak_HalfThermal_FixEta_BHChangeVel"]
snapnum = 25
N=20
outfile = "/data/vault/nh444/project1/profiles/BubbleEnergy-NoDuty_HalfThermal_z0_"+str(N)+".pdf"
nbins = 40
mpc_units=False
PDF = False
BubbleEnergy = True
BlackHoleRadioTriggeringFactor = [1.005, 1.001]
GrowthFactor = [0.005, 0.001]
BlackHoleRadiativeEfficiency = 0.1
RadioFeedbackFactor = 0.2
c2 = 8.9875518e16 # c squared
Msun = 1.989e30

plt.rc('text', usetex=True)
plt.rc('font', family='sans-serif')

colour_idx = np.linspace(0, 0.85, len(names))
fig = plt.figure(figsize=(10,10))
fig.patch.set_facecolor('white')

bhmass1 = np.empty([N, len(names)])
j = 0

for name in names:
    snapdir = "/data/emergence4/deboras/Elliptical_ZoomIns/"+name+"/"
    outdir = "/home/nh444/vault/project1/profiles/"+name+"/"

    sim = snap.snapshot(snapdir, snapnum, mpc_units=mpc_units)
    
    sim.get_hostsubhalos() ## Creates array containing host halo index of every particle (-1 if not part of any subhalo)
    sim.get_sub_bh_props()  ## get properties of 3 most massive BHs in subhalo (e.g. sub_bhmass1, sub_bhmass2, sub_bhmassdot1 etc.) and overall BH mass, mdot
    
    for i in range(0, N):
        bhmass1[i][j] = sim.sub_bhmass1[sim.cat.group_firstsub[i]] ## Mass of most massive BH in ith most massive halo
        if i < 10:
            print "Halo",i,"bhmass1 = ", bhmass1[i, j]
    j += 1
        
bhmass1 *= 1.0e10 ## Convert to solar masses
tol = 9.0e03  ## Mask BHs of mass < 10^4 Msun
bhmass1 = ma.masked_where(bhmass1 < tol, bhmass1)
if PDF:
    y = np.log10(bhmass1)
    bins = np.linspace(np.amin(y), np.amax(y), nbins)
    weights = np.ones_like(y)
    y_label = "Probability Density [$h^{-1}$ $M_{\odot}^{-1}$ $dex^{-1}$]"
    normed = True
elif BubbleEnergy:
    y = np.log10(bhmass1 * GrowthFactor * BlackHoleRadiativeEfficiency * RadioFeedbackFactor * c2 * Msun / 1e-7)
    bins = np.linspace(np.amin(y), np.amax(y), nbins)
    weights = np.ones_like(y)
    y_label = "Probability Density [ergs$^{-1}$ dex$^{-1}$]"
    normed = True
else:
    bins = np.logspace(np.log10(np.amin(bhmass1)), np.log10(np.amax(bhmass1)), nbins)
    y = bhmass1
    normed = False
    weights = np.ones_like(y)
    y_label = "Number"
            ## ---------------- PLOTTING ---------------- ##
j = 0
n = np.zeros((nbins-1, len(names)))
for name in names:
    title = name.replace('_','\_')
    if j==0:
        n[:, j], Bins, patches = plt.hist(y[:,j], bins=bins, ec=plt.cm.brg(colour_idx[j]), fc='none', lw=1, histtype='step', label=title+" $N_{halo}$ = "+str(N)+" $N_{BH}$ = "+str(bhmass1[:,j].count()), weights=weights[:,j], normed=normed)
    if j==1:
        n[:, j], Bins, patches = plt.hist(y[:,j], bins=bins, ec=plt.cm.brg(colour_idx[j]), fc='none', lw=2, histtype='step', label=title+" $N_{halo}$ = "+str(N)+" $N_{BH}$ = "+str(bhmass1[:,j].count()), weights=weights[:,j], normed=normed, linestyle='dotted')
    if j== 2:
        n[:, j], Bins, patches = plt.hist(y[:,j], bins=bins, ec=plt.cm.brg(colour_idx[j]), fc='none', lw=2, histtype='step', label=title+" $N_{halo}$ = "+str(N)+" $N_{BH}$ = "+str(bhmass1[:,j].count()), weights=weights[:,j], normed=normed, linestyle='dashed')
    j += 1

ax = plt.gca()
handles, labels = ax.get_legend_handles_labels()
plt.gca().set_position((0.1, 0.1, 0.85, 0.75))
ax.legend(handles, labels, bbox_to_anchor=(0.0, 1.01, 1.0, 0.1), loc=3, numpoints=1, frameon=False)


if PDF:
    plt.xlabel(r"$log_{10}$ (M$_{BH}$ [$h$ $M_{\odot}$])", fontsize=16)
    plt.xlim(np.amin(Bins)-0.2, np.amax(Bins)+0.2)
elif BubbleEnergy:
    plt.xlabel(r"$log_{10}$ (E$_{Bub}$ [ergs])", fontsize=16)
    plt.xlim(np.amin(Bins)-0.2, np.amax(Bins)+0.2)
    plt.ylim(0.0, np.max(n[:,:]))
    #plt.axvline(x=56)
else:
    plt.xscale('log')
    plt.xlabel(r"GroupBHMass ($h$ $M_{\odot}$)", fontsize=16)
    plt.xlim(np.amin(Bins)*0.8, np.amax(Bins)*2.0)

plt.ylabel(y_label, fontsize=16)

fig.show()
plt.savefig(outfile)

