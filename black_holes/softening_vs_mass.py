#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 30 14:50:33 2017

@author: nh444
"""
import numpy as np
import matplotlib.pyplot as plt

#SofteningType1 = 1.35 #kpc
SofteningComovingType1 = 0.016875 # Mpc
SofteningComovingType2 = 0.0675
SofteningComovingType3 = 0.6

AvgType1Mass = 5.544e7

m = np.logspace(7, 13, num=1000)

soft_old = np.full_like(m, SofteningComovingType1)
soft_old[m > AvgType1Mass] = SofteningComovingType1 * (m[m > AvgType1Mass] / AvgType1Mass)**(1.0/3.0)

soft_new = np.full_like(m, SofteningComovingType1)
eps = SofteningComovingType1 * (m / AvgType1Mass)**(1.0/3.0) ## desired softening
eps[eps < SofteningComovingType1] = SofteningComovingType1
ind2 = np.where((SofteningComovingType2 - eps) < (eps - SofteningComovingType1))[0]
soft_new[ind2] = SofteningComovingType2
ind3 = np.where((SofteningComovingType3 - eps) < (eps - SofteningComovingType2))[0]
soft_new[ind3] = SofteningComovingType3

fig = plt.figure(figsize=(8,3.5))

plt.plot(m, 2.8*soft_old*1000.0, lw=2, label="Old arepo")
plt.plot(m, 2.8*soft_new*1000.0, lw=2, label="New arepo")
plt.legend(frameon=False, loc='upper left')
plt.xscale('log')
plt.yscale('log')
plt.ylabel("Softening Length (kpc)")
plt.xlabel("Mass [M$_{\odot}$/h]")
plt.savefig("/home/nh444/Documents/arepo_code_diff/softening_vs_mass.pdf", bbox_inches='tight')

