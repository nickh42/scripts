import snap
import readsubf
import numpy as np
from numpy import ma
import matplotlib.pyplot as plt

## Names of directories containing snapshots
names = ["L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff",
         "L40_512_NoDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_Prex"]
outfile = "/home/nh444/data/project1/blackholes/BH-halo-mass.pdf"
snapnum = 25
N = 200 ##Total number to plot
n = 20 ## n most massive to highlight
mpc_units=False
filelist = [] ## list of output files

plt.rc('text', usetex=True)
plt.rc('font', family='sans-serif')

colour_idx = np.linspace(0, 0.85, len(names))
j=0
fig = plt.figure(figsize=(10,10))
fig.patch.set_facecolor('white')


for name in names:
    snapdir = "/data/emergence4/deboras/Elliptical_ZoomIns/"+name+"/"
    outdir = "/home/nh444/vault/project1/profiles/"+name+"/"
    title = name.replace('_','\_')

    sim = snap.snapshot(snapdir, snapnum, mpc_units=mpc_units)
    
    sim.get_hostsubhalos() ## Creates array containing host halo index of every particle (-1 if not part of any subhalo)
    sim.get_sub_bh_props()  ## get properties of 3 most massive BHs in subhalo (e.g. sub_bhmass1, sub_bhmass2, sub_bhmassdot1 etc.) and overall BH mass, mdot
    halos = readsubf.subfind_catalog(snapdir, snapnum)
    
    bhmass1 = np.empty(N)
    group_mass = np.empty(N)
    
    for i in range(0, N):
        bhmass1[i] = sim.sub_bhmass1[sim.cat.group_firstsub[i]] ## Mass of most massive BH in ith most massive halo
        group_mass[i] = halos.group_mass[i]
        if i < 10:
            print "Halo",i,"bhmass1 = ", bhmass1[i], "halo mass = ", group_mass[i]

    group_mass *= 1e10
    bhmass1 *= 1e10
    tol = 9e03  ## Mask BHs of mass < 10^4 Msun
    bhmass1 = ma.masked_where(bhmass1 < tol, bhmass1)

    ## ---------------- PLOTTING ---------------- ##
    bhmass1_plt, = plt.plot(group_mass, bhmass1, 'o', label=title[8:], ms=5, mew=1, mfc='none', mec=plt.cm.brg(colour_idx[j]))
    #plt.plot(group_mass[:n-1], bhmass1[:n-1], 'o',  ms=7-j, mec='none', mfc=plt.cm.brg(colour_idx[j]))
    j += 1
    print "Run "+name+" done."

ax = plt.gca()
handles, labels = ax.get_legend_handles_labels()
ax.legend(handles, labels, loc='lower right', numpoints=1, frameon=False)

plt.xscale('log')
plt.xlabel(r"GroupMass [$h^{-1}$ $M_{\odot}$]", fontsize=16)
plt.xlim(np.amin(group_mass), np.amax(group_mass)*2.0)
plt.yscale('log')
plt.ylabel(r"M$_{BH}$ [$h^{-1}$ $M_{\odot}$]", fontsize=16)

plt.savefig(outfile)

