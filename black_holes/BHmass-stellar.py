#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 16 16:36:50 2017
Plots BH mass versus stellar mass for subhaloes
@author: nh444
"""
import snap
import matplotlib.pyplot as plt
from matplotlib.ticker import NullFormatter
import numpy as np
h_scale = 0.679
outdir = "/data/curie4/nh444/project1/blackholes/"
title=None
suffix = ""
composite = True ## plot simdirs all at once rather than individually

basedir = "/data/emergence4/deboras/Elliptical_ZoomIns/"
mpc_units = False
subdirs = ["L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff",
           #"L40_512_LowDutyRadioWeak_HalfThermal_FixEta_BHChangeVel",
           #"L40_512_LowDutyRadioWeak_HalfThermal_FixEta",
           "L40_512_NoDutyRadioIntLow_HalfThermal_FixEta_BHChangeVel",
           "L40_512_NoDutyRadioIntLow_HalfThermal_FixEta"]
labels = [subdir.replace("_","\_") for subdir in subdirs]
labels = ["Best",
          #"QM duty cycle with BHVel", "QM duty cycle no BHVel",
          "BHVel", "no BHVel"]
          
basedir = "/data/curie4/nh444/project1/boxes/"
mpc_units = False
subdirs = ["L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff",
           "L40_512_LDRIntRadioEffBHVel",
           "L40_512_LDRIntRadioEff_SoftSmall",
           ]
labels = ["$\epsilon$(z=0)=0.3375 $z_{\epsilon}=3$ Ngb=256",
          "$\epsilon$(z=0)=2.4 $z_{\epsilon}=5$ Ngb=32",
          "$\epsilon$(z=0)=2.4 $z_{\epsilon}=1$ Ngb=32",
          ]
#outname = "BHmass_stellar_mass_LowHigh_softening_z0.pdf"
title = ""
suffix = "_LowHigh_softening_z6"
composite = False ## plot simdirs all at once rather than individually
snapnums = [5, 5, 5]

#==============================================================================
# basedir = "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/"
# mpc_units = True
# subdirs = ["c96_box_Arepo_new_LowSoft",
#            "c128_box_Arepo_new_LowSoft",
#            "c160_box_Arepo_new_LowSoft",
#            "c256_box_Arepo_new_LowSoft",
#            "c320_box_Arepo_new_LowSoft",
#            "c384_box_Arepo_new_LowSoft",
# #           "c96_box_Arepo_new",
# #           "c128_box_Arepo_new",
# #           "c160_box_Arepo_new",
# #           "c256_box_Arepo_new",
# #           "c320_box_Arepo_new",
# #           "c384_box_Arepo_new",
#            ]
# labels = ["c96 small soft", 
#           "c128 small soft", 
#           "c160 small softt", 
#           "c256 small soft", 
#           "c320 small soft", 
#           "c384 small soft", 
# #          "c96 large soft",
# #          "c128 large soft",
# #          "c160 large soft",
# #          "c256 large soft",
# #          "c320 large soft",
# #          "c384 large soft",
#           ]
# suffix = "_smallsoft"
# title = "$\epsilon_{com} = 1.6$ kpc/h $\epsilon_{maxphys} = 0.4$ kpc/h"
# #suffix = "_largesoft"
# #title = "$\epsilon_{com} = 16.9$ kpc/h $\epsilon_{maxphys} = 2.8$ kpc/h"
# composite = True
#==============================================================================
# =============================================================================
# basedir = "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/"
# mpc_units = True
# subdirs = ["c384_box_Arepo_new",
#            "c384_box_Arepo_new_LowSoft",
#            ]
# snapnums = [25, 25]
# labels = ["$\epsilon_{com} = 16.9$ kpc/h $\epsilon_{maxphys} = 2.8$ kpc/h",
#           "$\epsilon_{com} = 1.6$ kpc/h $\epsilon_{maxphys} = 0.4$ kpc/h",
#           ]
# suffix = "_c384_soft"
# title = "c384"
# composite = False
# =============================================================================
#==============================================================================
# subdirs = ["c96_box_Arepo_new",
#            "c96_box_Arepo_new_BHVel",
#            "c96_box_Arepo_new_Ngb",
#            #"c96_box_Arepo_new_DRAINGAS_2",
#            #"c96_box_Arepo_new_PM1024",
#            "c96_box_Arepo_new_LowSoft",
#            "c96_box_Arepo_new_LowSoft_BHVel",
#            "c96_box_Arepo_new_LowSoft_BHVel_Ngb",
#            #"c96_box_Arepo_new_LowSoft_BHVel_Ngb_PM1024_DRAINGAS2",
#            ]
# labels = ["c96 High Soft", "c96 High Soft BHVel", "c96 256 Ngb", #"c9 DRAINGAS 2",  "c96 PMGRID 1024",
#           "c96 Low Soft", "c96 Low Soft BHVel", "c96 Low Soft BHVel Ngb 256", "c96 Low Soft BHVel 256 Ngb DRAINGAS=2 PM=1024"]
# suffix = "_c96"
#==============================================================================

#==============================================================================
# basedir = "/data/curie4/nh444/project1/"
# mpc_units = True
# subdirs = ["boxes/L40_512_MDRIntRadioEff",
#            #"zoom_ins/MDRInt/c128_MDRInt",
#            #"zoom_ins/MDRInt/c192_MDRInt",
#            #"zoom_ins/MDRInt/c256_MDRInt",
#            "zoom_ins/MDRInt/c320_MDRInt",
#            "zoom_ins/MDRInt/c384_MDRInt",
#            "zoom_ins/MDRInt/c448_MDRInt",
#            "zoom_ins/MDRInt/c512_MDRInt",
#            ]
# labels = ["Box", 
#           #"c128", "c192", "c256",
#           "c320", "c384", "c448", "c512"]
# title = ""
# suffix = "_MDRInt"
#==============================================================================

# =============================================================================
# basedir = "/data/curie4/nh444/project1/zoom_ins/"
# mpc_units = True
# subdirs = ["MDRInt/c448_MDRInt",
#            "MDRInt/c448_MDRInt_noprescrit",
#            "Illustris_model/c448_Ill",
#            ]
# labels = ["Fiducial", "without pres. crit.", "Illustris model"]
# composite=False
# outname = "BHmass_stellar_mass_c448_MDRInt_noprescrit.pdf"
# title = ""
# =============================================================================
basedir = "/data/curie4/nh444/project1/zoom_ins/MDRInt/"
mpc_units = True
#subdirs = ["c384_MDRInt"]
#snapnums = [18]
#labels = ["c384"]
#composite = False
#suffix = "c384_MDRInt_z1.4"
#title = ""
#centrals_only = False
subdirs = ["c96_MDRInt", "c128_MDRInt", "c160_MDRInt",
           "c192_MDRInt", "c224_MDRInt", "c256_MDRInt",
           "c288_MDRInt", "c320_MDRInt",
           "c352_MDRInt", "c384_MDRInt",
           "c448_MDRInt", "c480_MDRInt", "c512_MDRInt",
#           "c112_MDRInt", "c144_MDRInt", "c176_MDRInt", "c208_MDRInt",
#           "c240_MDRInt", "c272_MDRInt", "c304_MDRInt", "c336_MDRInt",
#           "c368_MDRInt", "c400_MDRInt", "c410_MDRInt", "c432_MDRInt",
#           "c464_MDRInt", "c496_MDRInt_SUBF",
           ]
snapnums = [18]*len(subdirs)
labels = [subdir.replace("_", "\_") for subdir in subdirs]
composite = False
suffix = "MDRInt_z1.4"
title = ""
centrals_only = True

#snapnums = [5, 9, 9]
highlight_main_halo = True
add_histograms = True ## show histograms of x and y values

def main():
    halfmass = True ## plot stellar half-mass rather than total mass
    TwoDimHist = False ## plot as two-dimensional histogram rather than points
    toPlot = "mass" ## "mass" or "veldisp_1D" or "veldisp_eff"
    outname = "BHmass-veldisp_1D_"+suffix+".pdf"
    outname = "BHmass-stellar_mass_"+suffix+".pdf"
    #outname = "stellar_mass-veldisp_1D_z0"+suffix+".pdf"
    plot_BHmass_stellar(toPlot=toPlot, outname=outname, halfmass=halfmass, composite=composite, TwoDimHist=TwoDimHist, centrals_only=centrals_only)
    #show_particles_per_halo()
    
def plot_BHmass_stellar(toPlot="mass", outname="BHmass-stellar.pdf", halfmass=False, composite=False, TwoDimHist=False, centrals_only=False):
    """
    Plots black hole mass versus stellar mass or velocity dispersion
    toPlot -- "mass" or "veldisp_1D" or "veldisp_eff" to plot BH mass against stellar mass or 1-D or effective velocity dispersion, respectively
    halfmass -- If True, plot stellar half mass, otherwise total bound mass
    composite -- If true, plot results from all simdirs at once rather than individually with labels
    TwoDimHist -- If True, plot 2D histogram, otherwise plot individual points
    """    
    fig = plt.figure(figsize=(10,10))

    if add_histograms:
        # axes dimensions
        left, width = 0.1, 0.65
        bottom, height = 0.1, 0.65
        left_h = left + width + 0.02
        bottom_h = bottom + height + 0.02
        rect_scatter = [left, bottom, width, height]
        rect_histx = [left, bottom_h, width, 0.2]
        rect_histy = [left_h, bottom, 0.2, height]
        
        axScat = plt.axes(rect_scatter)
        axHistx = plt.axes(rect_histx)
        axHisty = plt.axes(rect_histy)
    else:
        axScat = fig.add_subplot(111)
    ## For points if scatter plot:
    ms = 8
    alpha = 0.8
    
    axScat.set_ylabel("M$_{\mathrm{BH}}$ [M$_{\odot}$]")
    if toPlot=="mass":
        if halfmass:
            axScat.set_xlabel("Subhalo stellar half-mass [M$_{\odot}$]")
        else:
            axScat.set_xlabel("Subhalo total stellar mass [M$_{\odot}$]")
    elif toPlot=="veldisp_1D":
        axScat.set_xlabel("$\sigma_{\mathrm{1D}}$ [km/s]")
    elif toPlot=="veldisp_eff":
        axScat.set_xlabel("$\sigma_{\mathrm{eff}}$ [km/s]")
    elif toPlot=="veldisp_1D-mass":
        axScat.set_ylabel("$\sigma_{\mathrm{1D}}$ [km/s]")
        if halfmass:
            axScat.set_xlabel("Subhalo stellar half-mass [M$_{\odot}$]")
        else:
            axScat.set_xlabel("Subhalo total stellar mass [M$_{\odot}$]")
    else:
        assert False, "toPlot '"+toPlot+"' not recognised."
    axScat.set_yscale('log')
    axScat.set_xscale('log')
    if add_histograms:
        axHistx.set_xscale('log')
        axHisty.set_yscale('log')
    
    ## Define bins for histograms
    if toPlot=="mass" or toPlot=="veldisp_1D-mass":
        xmin, xmax = 1e7, 3e12 ## stellar mass Msun
    elif toPlot=="veldisp_1D" or toPlot=="veldisp_eff":
        xmin, xmax = 10.0, 1000.0
        
    if toPlot=="veldisp_1D-mass":
        ymin, ymax = 10.0, 1000.0
    else:
        ymin, ymax = 1e5, 1e12 ## BH mass Msun
    xbins = np.logspace(np.log10(xmin), np.log10(xmax), num=60) 
    ybins = np.logspace(np.log10(ymin), np.log10(ymax), num=60)
    axScat.set_xlim(xmin, xmax)
    axScat.set_ylim(ymin, ymax)
    
    nxmax = 0
    nymax = 0
    
    if composite:
        sub_propx_all = []
        sub_propy_all = []
    for dirnum, subdir in enumerate(subdirs):
        sim = snap.snapshot(basedir+subdir, snapnums[dirnum], mpc_units=mpc_units, extra_sub_props=True)       
        sim.get_hostsubhalos()
        sim.get_sub_bh_props()
        
        ## Restrict to subhaloes with at least one bound BH and at least one star particle within the half-mass radius
        subind = np.where((sim.sub_bhmass1 > 0.0) & (sim.cat.sub_massinhalfradtab[:,4] > 0.0))[0] ## bhmass1 will be 0 if subhalo has no BHs
        if centrals_only:
            subind = np.intersect1d(subind, sim.cat.group_firstsub) ## only subnums that are main haloes
        if toPlot=="mass":
            if halfmass:
                #sub_propx = sim.cat.sub_massinhalfradtab[subind,4]*1e10/h_scale
                sub_propx = sim.cat.sub_massinhalfrad[subind]*1e10/h_scale
            else:
                sub_propx = sim.cat.sub_masstab[subind,4]*1e10/h_scale
        elif toPlot=="veldisp_1D" or toPlot=="veldisp_eff" or toPlot=="veldisp_1D-mass":
            #sub_propx = sim.cat.sub_veldisp[ind] ## physical 1-D velocity dispersion calculated for all bound stellar particles
            ## Calculate physical 1-D velocity dispersion within half-mass radius
            print "Calculating velocity dispersion for", subdir
            sim.load_starpos()
            sim.load_starvel(physical_velocities=True)
            sim.load_starmass()
            sim.load_stellar_ages()## stellar formation time (code units)
            sub_propx = np.zeros(len(subind))
            sub_propy = np.zeros(len(subind))
            for i, subidx in enumerate(subind):
                ## Indices of star particles bound to current subhalo
                starind1 = np.where(sim.hostsubhalos[sim.species_start[4]:sim.species_end[4]] == subidx)[0]
                ### Exclude wind particles, which count as gas
                starind1 = starind1[np.where(sim.age[starind1] > 0)[0]] ## winds have formation time <=0 (=0 if only just recoupled)
                ## Calculate distance from minimum of potential of subhalo since the half-mass radius is calculated relative to the potential minimum
                r = sim.nearest_dist_3D(sim.starpos[starind1], sim.cat.sub_pos[subidx])
                ## get indices of bound star particles within stellar half-mass radius
                starind = starind1[np.where(r <= sim.cat.sub_halfmassradtype[subidx,4])[0]]
                mass = np.sum(sim.starmass[starind]) ## total mass of stars
                v = np.sum(sim.starvel[starind] * sim.starmass[starind, None], axis=0) / mass ## mean mass-weighted velocity of stars
                dv = (sim.starvel[starind] - v) ## physical velocity of stars wrt to mean mass-weighted
                ## now add in the difference in Hubble velocity between the stars and the centre of mass
                s = sim.cat.sub_cm[subidx] ## position of centre of mass of subhalo
                dx = sim.header.time * sim.rel_pos(sim.starpos[starind], s) ## distance of stars from CoM
                dv += sim.cm.hubble_z(sim.header.redshift) * dx ## add difference in Hubble velocity
                
                if toPlot=="veldisp_1D" or toPlot=="veldisp_1D-mass":
                    ## calculate the mass-weighted average of the 3D velocity dispersion
                    ## as a proxy for the line-of-sight (1-D) velocity dispersion
                    ## The following line is how vel disp is calculated in subfind_properties.c:
                    veldisp = np.sqrt(np.sum(sim.starmass[starind] * np.sum(dv**2.0, axis=1)) / (3.0 * mass))
                    ## although the following seems more intuitive and gives the same result (probably because the stellar masses don't vary much)
                    #veldisp = np.sum(np.sqrt(np.sum(sim.starmass[starind, None] * dv**2) / mass)) / np.sqrt(3)
                    if toPlot=="veldisp_1D-mass":
                        sub_propy[i] = veldisp
                    else:
                        sub_propx[i] = veldisp
                elif toPlot=="veldisp_eff":
                    ## Calculate the "effective" velocity dispersion, which is the 
                    ## quadrature sum of the 1D velocity dispersion and the rotational velocity
                    dxnorm = np.sum(dx**2.0, axis=1)
                    vrot = np.cross(np.cross(dx, dv) / dxnorm[:,None], dx) ## tangential velocity vector as cross-product of angular velocity vector and position vector (dx)
                    ## get mass-weighted average of the 1-D vel. disp. and
                    ## the rotational velocity added in quadrature
                    ## Note that, like for the vel. disp., the line-of-sight (1D) rotational velocity
                    ## is approximated as sqrt((v_x^2 + v_y^2 + v_z^2) / 3.0)
                    sub_propx[i] = np.sqrt(np.sum(sim.starmass[starind] * (np.sum(dv**2.0, axis=1) + np.sum(vrot**2.0, axis=1))) / (3.0*mass))
                
                if np.isnan(sub_propx[i]):
                    print "halfmass =", sim.cat.sub_massinhalfradtab[subidx,4]
                    print np.sum(sim.starmass[starind1]), sim.cat.sub_masstab[subidx,4]
                    print starind1
                    print starind
                    print "r =", np.sort(r)
                    massprof = np.cumsum(sim.starmass[starind1[np.argsort(r)]]) / np.sum(sim.starmass[starind1])
                    indices = np.argsort(r)
                    print r[indices[np.where(massprof < 0.5)[0][-1]]]
                    print r[indices[np.where(massprof > 0.5)[0][0]]]
                    print sim.cat.sub_halfmassradtype[subidx,4]
                    assert False
#==============================================================================
#             sim.load_pos()
#             sim.load_vel()
#             sim.load_mass()
#             sub_propx = np.zeros(len(subind))
#             for i, subidx in enumerate(subind):
#                 ind = np.where(sim.hostsubhalos == subidx)[0]
#                 ## need to restrict to aperture
#                 mass = np.sum(sim.mass[ind]) ## total mass of stars in subhalo
#                 v = np.sum(sim.vel[ind] * sim.mass[ind, None], axis=0) / mass ## mean mass-weighted velocity of subhalo stars
#                 dv = (sim.vel[ind] - v) / sim.header.time  ## physical velocity of stars wrt to mean mass-weighted
#                 ## now add in the difference in Hubble velocity between the stars and the centre of mass
#                 s = sim.cat.sub_cm[subidx] ## position of centre of mass of subhalo
#                 dx = sim.header.time * sim.rel_pos(sim.pos[ind], s) ## distance of stars from CoM
#                 dv += sim.cm.hubble_z(sim.header.redshift) * dx ## add difference in Hubble velocity
#                 ## calculate mass-weighted mean dv^2 and square-root to get 3D vel. dispersion
#                 sub_propx[i] = np.sqrt(np.sum(sim.mass[ind] * np.sum(dv**2.0, axis=1)) / mass)
#                 sub_propx[i] /= np.sqrt(3.0) ## convert to 1D velocity dispersion
#==============================================================================
            print "ratio =", sub_propx / sim.cat.sub_veldisp[subind]
        if toPlot=="veldisp_1D-mass":
            if halfmass:
                sub_propx = sim.cat.sub_massinhalfradtab[subind,4]*1e10/h_scale
            else:
                sub_propx = sim.cat.sub_masstab[subind,4]*1e10/h_scale
        else:
            sub_propy = sim.sub_bhmass1[subind]*1e10/h_scale
        
        if composite:
            ## Add to composite list and plot below
            #print np.shape(sub_propx)
            #print np.shape(sub_propx_all)
            sub_propx_all = np.concatenate((sub_propx_all, sub_propx))
            sub_propy_all = np.concatenate((sub_propy_all, sub_propy))
        else:
            axScat.plot(sub_propx, sub_propy, mec='none', alpha=alpha, marker='o', ms=ms, ls='none', label=labels[dirnum])
            # ^this is just the most massive BH - can use hostsubhalos to find all the others or sim.sub_bhmass2/3 for the next two most massive
            if highlight_main_halo:
                ind = [sim.cat.group_firstsub[0]]#range(sim.cat.group_firstsub[0], sim.cat.group_firstsub[0]+sim.cat.group_nsubs[0])
                axScat.plot(sub_propx[ind], sub_propy[ind], mec='none',
                            alpha=alpha, marker='o', ms=5, ls='none',
                            label=labels[dirnum]+" main halo")
            if add_histograms:
                nx, bin_edges, count = axHistx.hist(sub_propx, bins=xbins, histtype='step')
                ny, bin_edges, count = axHisty.hist(sub_propy, bins=ybins, orientation='horizontal', histtype='step')
                if np.max(nx) > nxmax: nxmax = np.max(nx)
                if np.max(ny) > nymax: nymax = np.max(ny)
            
    if composite:
        if TwoDimHist:
            ## Plot 2D histogram of all points
            hist, xedges, yedges = np.histogram2d(sub_propx_all, sub_propy_all, bins=[xbins, ybins])
            from matplotlib.colors import LogNorm
            cmap = plt.get_cmap('Blues')
            img = axScat.pcolor(xedges, yedges, hist.transpose(),
                          norm=LogNorm(vmin=np.min(hist[hist>0]), vmax=np.max(hist)),
                          cmap=cmap)
            
            ## Plot colourbar
            from mpl_toolkits.axes_grid1.inset_locator import inset_axes
            cbaxes = inset_axes(axScat,
                        width="4%", #of parent_bbox width
                        height="45%",
                        loc=3,
                        bbox_to_anchor=(0.05, 0.1, 1, 1),
                        bbox_transform=axScat.transAxes,
                        borderpad=0)
            plt.colorbar(img, cax = cbaxes) 
        else:
            axScat.plot(sub_propx_all, sub_propy_all, mec='none', alpha=alpha, marker='o', ms=ms, ls='none')
            
        if add_histograms:
            nx, bin_edges, count = axHistx.hist(sub_propx_all, bins=xbins, histtype='step')#, color='darkblue')
            ny, bin_edges, count = axHisty.hist(sub_propy_all, bins=ybins, orientation='horizontal', histtype='step')#, color='darkblue')
            nxmax = np.max(nx)
            nymax = np.max(ny)

    ## Observations
    c='0.3'
    alpha = 0.8
    if toPlot=="mass":
        if halfmass:
            Kormendy_fit = lambda M: 0.49*1e9*(M / 1e11)**1.17 ## M_BH vs M_bulge in Msun
            axScat.plot([xmin, xmax], Kormendy_fit(np.array([xmin, xmax])), c=c, ls='dashed', lw=2, label="Kormendy \& Ho 2013")
            
            E = np.genfromtxt("/data/vault/nh444/ObsData/blackholes/"+"Kormendy_Ho_2013_ellipticals.csv", delimiter=",")
            ind = range(0, 32) ## ellipticals with reliable BH masses
            axScat.errorbar(10**E[ind,9]*0.705/h_scale, E[ind,11]*0.705/h_scale, xerr=[(10**E[ind,9] - 10**(E[ind,9]-E[ind,10]))*0.705/h_scale, (10**(E[ind,9]+E[ind,10]) - 10**E[ind,9])*0.705/h_scale], yerr=[(E[ind,11]-E[ind,12])*0.705/h_scale, (E[ind,13]-E[ind,11])*0.705/h_scale], marker='o', c=c, mec=c, ls='none', alpha=alpha, label="ellipticals")
            #ind = range(32, 37) ## mergers
            #axScat.errorbar(10**E[ind,9]*0.705/h_scale, E[ind,11]*0.705/h_scale, xerr=[(10**E[ind,9] - 10**(E[ind,9]-E[ind,10]))*0.705/h_scale, (10**(E[ind,9]+E[ind,10]) - 10**E[ind,9])*0.705/h_scale], yerr=[(E[ind,11]-E[ind,12])*0.705/h_scale, (E[ind,13]-E[ind,11])*0.705/h_scale], marker='o', c=c, mfc='none', mec=c, ls='none')
            
            E = np.genfromtxt("/data/vault/nh444/ObsData/blackholes/"+"Kormendy_Ho_2013_bulges.csv", delimiter=",")
            ind = range(0, 19) ## classical bulges with reliable BH masses
            axScat.errorbar(10**E[ind,15]*0.705/h_scale, E[ind,17]*0.705/h_scale, xerr=[(10**E[ind,15] - 10**(E[ind,15]-E[ind,16]))*0.705/h_scale, (10**(E[ind,15]+E[ind,16]) - 10**E[ind,15])*0.705/h_scale], yerr=[(E[ind,17]-E[ind,18])*0.705/h_scale, (E[ind,19]-E[ind,17])*0.705/h_scale], marker='d', c=c, mec=c, ls='none', alpha=alpha, label="classical bulges")
            ind = range(19, 41) ## pseudo-bulges with reliable BH masses
            axScat.errorbar(10**E[ind,15]*0.705/h_scale, E[ind,17]*0.705/h_scale, xerr=[(10**E[ind,15] - 10**(E[ind,15]-E[ind,16]))*0.705/h_scale, (10**(E[ind,15]+E[ind,16]) - 10**E[ind,15])*0.705/h_scale], yerr=[(E[ind,17]-E[ind,18])*0.705/h_scale, (E[ind,19]-E[ind,17])*0.705/h_scale], marker='s', mfc='none', c=c, mec=c, ls='none', alpha=alpha, label="pseudo bulges")
            
            ## WARNING: Savorgnan+16 don't define their assumed cosmology, nor do the papers which they get their BH masses and M/L ratio from
            Sav = np.genfromtxt("/data/vault/nh444/ObsData/blackholes/"+"Savorgnan_2016.csv", delimiter=",")
            axScat.errorbar(Sav[:,13]*1e10, Sav[:,4]*1e8, xerr=[np.abs(Sav[:,15])*1e10, Sav[:,14]*1e10], yerr=[np.abs(Sav[:,6]*1e8), Sav[:,5]*1e8], marker='s', c=c, mec='none', ls='none', alpha=alpha, label="Savorgnan+16")
            
            ## WARNING: They don't define their assumed cosmology
            ## WARNING: Some of these masses are corrected in Graham & Scott 2015
            #Scott = np.genfromtxt("/data/vault/nh444/ObsData/blackholes/"+"Scott_2013.csv", delimiter=",")
            #p = axScat.errorbar(Scott[:,8]*1e10, Scott[:,3]*1e8, xerr=[np.abs(Scott[:,10])*1e10, Scott[:,9]*1e10], yerr=[np.abs(Scott[:,5]*1e8), Scott[:,4]*1e8], marker='o', mec='none', ls='none', label="Scott+13")
            #sersic_fit = lambda M: 10**(2.22*np.log10(M / 2e10) + 7.89) ## M_BH as a function of M_sph (M) for sersic galaxies
            #axScat.errorbar([xmin, 6e10], sersic_fit(np.array([xmin, 6e10])), c=p[0].get_color(), ls='-', lw=2, label="Scott+13 Sersics")
            
            #GS = np.genfromtxt("/data/vault/nh444/ObsData/blackholes/"+"Graham_Scott_2015.csv", delimiter=",")
            #axScat.errorbar(GS[:,7]*1e9*0.7/h_scale, GS[:,1]*1e5*0.7/h_scale, xerr=[np.abs(GS[:,9])*1e9*0.7/h_scale, GS[:,8]*1e9*0.7/h_scale], yerr=GS[:,2]*1e5*0.7/h_scale, marker='o', mec='none', ls='none', label="Graham \& Scott 2015")
        else:
            Haring_fit = lambda M: 10**(8.2 + 1.12*np.log10(M / 1e11)) ## M_BH vs M_bulge in Msun
            axScat.plot([xmin, xmax], Haring_fit(np.array([xmin, xmax])), c=c, ls='dashed', label="Haring \& Rix 2004")
            
            ### Could also plot data from Terrazas+ 2017
            
        if sim.header.redshift > 0.9 and sim.header.redshift < 1.1 and not halfmass:
            patch = np.loadtxt("/data/vault/nh444/ObsData/blackholes/Sijacki2007_MBH_Mstar.txt")[:20]
            pts = np.loadtxt("/data/vault/nh444/ObsData/blackholes/Sijacki2007_MBH_Mstar.txt")[20:]
            from matplotlib.patches import Polygon
            axScat.axes.add_patch(Polygon(patch, color='none', ec='k', lw=2, zorder=5))
            axScat.plot(pts[:,0], pts[:,1], ls='none', marker='D', mec='k', mfc='none', ms=5, label="Sijacki+2007 ($z=1$)")
    elif toPlot=="veldisp_1D" or toPlot=="veldisp_eff":
        E = np.genfromtxt("/data/vault/nh444/ObsData/blackholes/"+"Kormendy_Ho_2013_ellipticals.csv", delimiter=",")
        ind = range(0, 32) ## ellipticals with reliable BH masses
        axScat.errorbar(E[ind,14], E[ind,11]*0.705/h_scale, xerr=E[ind,15], yerr=[(E[ind,11]-E[ind,12])*0.705/h_scale, (E[ind,13]-E[ind,11])*0.705/h_scale], marker='o', c=c, mec=c, ls='none', label="ellipticals")
        
        E = np.genfromtxt("/data/vault/nh444/ObsData/blackholes/"+"Kormendy_Ho_2013_bulges.csv", delimiter=",")
        ind = range(0, 19) ## classical bulges with reliable BH masses
        axScat.errorbar(E[ind,20], E[ind,17]*0.705/h_scale, xerr=E[ind,21], yerr=[(E[ind,17]-E[ind,18])*0.705/h_scale, (E[ind,19]-E[ind,17])*0.705/h_scale], marker='d', c=c, mec=c, ls='none', label="classical bulges")
        ind = range(19, 41) ## pseudo-bulges with reliable BH masses
        axScat.errorbar(E[ind,20], E[ind,17]*0.705/h_scale, xerr=E[ind,21], yerr=[(E[ind,17]-E[ind,18])*0.705/h_scale, (E[ind,19]-E[ind,17])*0.705/h_scale], marker='s', mfc='none', c=c, mec=c, ls='none', label="pseudo bulges")

        
    if add_histograms:
        axHistx.set_xlim(axScat.get_xlim())
        axHistx.set_ylim(0, int(nxmax*1.1)+1)
        axHisty.set_ylim(axScat.get_ylim())
        axHisty.set_xlim(0, int(nymax*1.1)+1)
        for label in axHisty.get_xticklabels():
            label.set_rotation(90)
    
        nullfmt = NullFormatter() # for no labels
        axHistx.xaxis.set_major_formatter(nullfmt)
        axHisty.yaxis.set_major_formatter(nullfmt)
    
        if title is not None:
            axHistx.set_title(title+" z={:.1f}".format(sim.header.redshift))
        
    axScat.legend(loc='upper left', frameon=False, fontsize='small')
    fig.savefig(outdir+outname, bbox_inches='tight')
    print "Output to", outdir+outname
        
def show_particles_per_halo():
    from scipy.stats import binned_statistic
    
    fig, (ax1, ax2) = plt.subplots(2, sharex=True, figsize=(10,10))
    fig.subplots_adjust(hspace=0.0)
    ax2.set_xlabel("Subhalo total stellar mass [M$_{\odot}$]")
    ax1.set_ylabel("Number of star particles")
    ax2.set_ylabel("Ratio")
    ax1.set_xscale('log')
    ax1.set_yscale('log')
    
    xmin, xmax = 1e7, 3e12 ## stellar mass Msun
    xbins = np.logspace(np.log10(xmin), np.log10(xmax), num=60) 
    
    h = 0.679
    sim = snap.snapshot("/data/emergence4/deboras/Elliptical_ZoomIns/"+"L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff", 25, mpc_units=False)
    hist_sim, bin_edges, n = binned_statistic(sim.cat.sub_masstab[:,4]*1e10/sim.header.hubble, sim.cat.sub_lentab[:,4], statistic='median', bins=xbins)
    
    p = ax1.plot(sim.cat.sub_masstab[:,4]*1e10/h, sim.cat.sub_lentab[:,4], marker='.', ls='none', label="Box")
    #ax1.step(xbins[:-1], hist_sim, where='post', c=p[0].get_color())

    for i, subdir in enumerate(subdirs):
        zoom = snap.snapshot(basedir+subdir, 25, mpc_units=True)
        hist_zoom, bin_edges, n = binned_statistic(zoom.cat.sub_masstab[:,4]*1e10/zoom.header.hubble, zoom.cat.sub_lentab[:,4], statistic='median', bins=xbins)
        p = ax1.plot(zoom.cat.sub_masstab[:,4]*1e10/h, zoom.cat.sub_lentab[:,4], marker='.', ls='none', label=labels[i])
        #ax1.step(xbins[:-1], hist_zoom, where='post', c=p[0].get_color())
        ax2.step(xbins[:-1], hist_zoom / hist_sim, where='post', c=p[0].get_color())
    
    ax1.legend(loc='best')
    
if __name__ == "__main__":
    main()