#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 15 19:34:20 2018

@author: nh444
"""

import sqlite3
## Create a connection object which represents the database
connection = sqlite3.connect("/data/ERCblackholes/cdegraf/BHDB/AREPO/c128.sqlite")
## Create a cursor object
c = connection.cursor()
## Use the execute method of the cursor to perform SQL commands
## Get the ID of the most massive BH in details at time=1
c.execute('SELECT bhid FROM blackholes WHERE mass = (SELECT max(mass) FROM blackholes WHERE time_q = 1.0)')
BHid = c.fetchone() ## fetchone() gets the next row of a query result
## Get the merger tree ID for this BH
c.execute('SELECT tid FROM bh2tid WHERE bhid = ?', BHid)
tree_id = c.fetchone()
## Get the merger entries corresponding to the tree ID and print
c.execute('SELECT time_q, bh1, bh2, mass1, mass2 FROM mergeeventsext WHERE tid=?', tree_id)
#print c.fetchall()
for row in c.fetchall():
    print row[1], row[2]
## or you can iterate over the rows
#for row in c.execute('SELECT time_q, bh1, bh2, mass1, mass2 FROM mergeeventsext WHERE tid=?', tree_id):
#    ## print whichever mass is largest (except one is sub-grid and the other dynamical mass)
#    print max(row[3], row[4])
### Can now apply the same logic as in BHprops_vs_redshift

## Close the connection to the database
connection.close()