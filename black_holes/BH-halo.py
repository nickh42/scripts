#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed May 30 11:48:47 2018

@author: nh444
"""

import matplotlib.pyplot as plt
import snap

h_scale = 0.679
outdir = "/data/curie4/nh444/project1/blackholes/"

basedir = "/data/curie4/nh444/project1/"
simdirs = ["boxes/L40_512_MDRIntRadioEff/",
#          "zoom_ins/MDRInt/c256_MDRInt/",
#          "zoom_ins/MDRInt/c384_MDRInt/",
#          "zoom_ins/MDRInt/c448_MDRInt/",
#          "zoom_ins/MDRInt/c512_MDRInt/",
#          "zoom_ins/MDRInt/c585_MDRInt/",
          ]
labels = ["Box",
#          "c256",
#          "c384",
#          "c448",
#          "c512",
#          "c585",
          ]
snapnum = 25
mpc_units=True

fig, ax = plt.subplots(figsize=(7,7))
ax.set_xscale('log')
ax.set_yscale('log')
#ax.set_ylim(1e5, 1e9)
ax.set_xlim(5e9, 5e12)
ax.set_ylabel("M$_{\mathrm{BH}}$ [M$_{\odot}$]")
ax.set_xlabel("M$_{500}$ [M$_{\odot}$]")

c='navy'
c=None
for simidx, simdir in enumerate(simdirs):
    sim = snap.snapshot(basedir+simdir, snapnum, mpc_units=mpc_units)
    
    sim.get_sub_bh_props()
    bhmass = sim.sub_bhmass1[sim.cat.group_firstsub] * 1e10 / h_scale
    M500 = sim.cat.group_m_crit500 * 1e10 / h_scale

    ax.plot(M500, bhmass, c=c, alpha=0.5, ms=5, marker='o', ls='none', mec='none')#, label=labels[simidx])
    
power = 2.0
func = lambda M500: 5e5 * (M500 / 3e10)**power
xmin, xmax = 3e10, 3e11
ax.plot([xmin, xmax], [func(xmin), func(xmax)], ls='-', c='0.2', lw=3, label="M$_{\mathrm{BH}}$ $\propto$ M$_{500}^{"+str(power)+"}$")
    
ax.set_title("z = {:.1f}".format(sim.header.redshift))
ax.legend(loc='upper left', frameon=True)

fig.savefig(outdir+"BHmass-halo_mass.pdf", bbox_inches='tight')

