#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Oct  3 14:17:19 2017

@author: nh444
"""
import numpy as np
import matplotlib.pyplot as plt
from scipy.constants import c ## 299 792 458 m/s

fig, ax = plt.subplots()
ax.set_xlabel("M$_{BH}$ [M$_{\odot}$]")
ax.set_ylabel("Total bubble energy [erg]")
ax.set_xscale('log')
ax.set_yscale('log')

### Allen+ 2006 ###
Adat = np.genfromtxt("/data/vault/nh444/ObsData/blackholes/Allen_2006.csv", delimiter=",")
M_BH_1 = Adat[:,25] * 1e8 ## mass used in Bondi accretion rate estimate in Msun - this is the same as the mass from velocity dispersion except for M87 which has a directly measured BH mass of 1e9 Msun
#M_BH_2 = Adat[:,26] * 1e8 ## mass from velocity dispersion in Msun
E1 = Adat[:,31] * 1e54 ## energy of first bubble in erg
E1_err = Adat[:,32] * 1e54
E2 = Adat[:,44] * 1e54 ## energy of second bubble in erg
E2_err = Adat[:,45] * 1e54
ax.errorbar(M_BH_1, E1+E2, yerr=np.sqrt(E1_err**2 + E2_err**2), marker='o', mec='none', ls='none', label="Allen+ 2006")

### Rafferty+ 2006 ###
Rdat = np.genfromtxt("/data/vault/nh444/ObsData/blackholes/Rafferty_2006_masterTable.csv", delimiter=",")
E = Rdat[:,2] * 4.0 * 1e58 ## E = 4PV (erg)
E_errlow = np.abs(Rdat[:,4]) * 4.0 * 1e58 ## - error
E_errhi = Rdat[:,3] * 4.0 * 1e58 ## + error
M_BH = np.zeros_like(E)
M_L = Rdat[:,14] * 1e9 ## BH mass from luminosity in Msun
M_sigma = Rdat[:,11] * 1e9 ## BH mass from velocity dispersion in Msun
M_dir = Rdat[:,8] * 1e9 ## BH mass measured using gas kinematics in Msun
## directly measured mass is most preferred, followed by velocity dispersion then luminosity derived
M_BH[np.isfinite(M_L)] = M_L[np.isfinite(M_L)]
M_BH[np.isfinite(M_sigma)] = M_sigma[np.isfinite(M_sigma)]
M_BH[np.isfinite(M_dir)] = M_dir[np.isfinite(M_dir)]
ax.errorbar(M_BH, E, yerr=[E_errlow, E_errhi], marker='o', mec='none', ls='none', label="Rafferty+ 2006")

## draw arrows between systems which appear in both Rafferty+2006 and Allen+2006
arrInd = [[14, 1], [15,3], [16,6], [25,8]] ## pair in format [Raff index, Allen index] for each system
for ind in arrInd:
    ax.arrow(M_BH[ind[0]], E[ind[0]], M_BH_1[ind[1]]-M_BH[ind[0]], E1[ind[1]]+E2[ind[1]]-E[ind[0]],
             color='0.7')
    ax.errorbar(M_BH[ind[0]], E[ind[0]], marker='o', mec='k', mew=0.6, mfc='none', ls='none')
    ax.errorbar(M_BH_1[ind[1]], E1[ind[1]]+E2[ind[1]], marker='o', mec='k', mew=0.6, mfc='none', ls='none')


xlims = ax.get_xlim()
xbins = np.logspace(np.log10(xlims[0]), np.log10(xlims[1]), num=10)##yes i know this isnt necessary
E = xbins * 1.989e30 * c**2 / 1e-7 ## erg
ax.plot(xbins, 0.4*0.1*0.001*E, c='k', ls='dashed', label="Weak radio")# ($\delta_{BH}=1.001$ $\epsilon_m=0.4$ $\epsilon_r=0.1$)")
ax.plot(xbins, 0.8*0.1*0.01*E, c='k', ls='solid', label="Stronger radio")# ($\delta_{BH}=1.01$ $\epsilon_m=0.8$ $\epsilon_r=0.1$)")
ax.plot(xbins, 0.35*0.2*0.15*E, c='k', ls='dashdot', label="Illustris")# ($\delta_{BH}=1.15$ $\epsilon_m=0.35$ $\epsilon_r=0.2$)")
ax.legend(fontsize='x-small', loc='upper left')

fig.savefig("/data/curie4/nh444/project1/blackholes/bubble_energy_vs_mass.pdf")
print "saved to /data/curie4/nh444/project1/blackholes/bubble_energy_vs_mass.pdf"