#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 14 11:29:02 2017

@author: nh444
"""

import snap
import matplotlib.pyplot as plt
import numpy as np

basedir = "/home/nh444/data/project1/zoom_ins/LDRIntRadioEffBHVel/"
subdirs = ["c128_box_Arepo",
           "c128_new",
           "c128_new_ThermFeedbackFix",
           ]
mpc_units = True
labels = ["old arepo",
          "new arepo",
          "new arepo feedback fix",
          ]
Ngb = None
suffix = ""

basedir = "/home/nh444/data/project1/zoom_ins/LDRIntRadioEffBHVel/"
subdirs = ["c96_box_Arepo_new",
           "c96_box_Arepo_new_Ngb",
           ]
mpc_units = True
labels = ["old arepo modified",
          "old arepo modified Ngb 256",
          ]
Ngb = [32, 256]
suffix = "_old_arepo_Ngb"
#==============================================================================
# basedir = ""
# subdirs = ["/data/emergence4/deboras/Elliptical_ZoomIns/L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff/",
#            "/data/curie4/nh444/project1/L40_512_LDRIntRadioEffBHVel/"]
# mpc_units = False
#==============================================================================
snapnum = 9
outdir = "/data/curie4/nh444/project1/blackholes/"
outname = "BH_temp_vs_dist_snap"+str(snapnum)+suffix+".pdf"


group = 0
subhalo = 0 # 0 is most massive subhalo in group

fig, axarr = plt.subplots(len(subdirs), figsize=(10,10))#sharex=True
#from palettable.colorbrewer.qualitative import Dark2_8 as pallette
#col=pallette.hex_colors
col = ["#c7673e", "#78a44f", "#6b87c8","#934fce","#be558f","#47af8d"]

for i, subdir in enumerate(subdirs):
    sim = snap.snapshot(basedir+subdir, snapnum, mpc_units=mpc_units)
    sim.get_hostsubhalos() ## Creates array containing host halo index of every particle (-1 if not part of any subhalo)
    
    sub = sim.cat.group_firstsub[group]+subhalo
    ind = np.where(sim.hostsubhalos[sim.species_start[5]:sim.species_end[5]] == sub)[0] ## indices of BHs in subhalo
    if ind.size>=1:
        sim.load_bhpos()
        sim.load_bhmass()
        sim.load_gaspos()
        sim.get_temp()

        #r = sim.nearest_dist_3D(sim.bhpos[ind], sim.cat.sub_pos[sub]) ## distance from subhalo position
        #idx = ind[r.argsort()[0]] ## index of BH nearest sub_pos
        idx = ind[np.argmax(sim.bhmass[ind])] ## most massive BH in subhalo
        print subdir, sim.bhmass[idx], sim.bhpos[idx]
        bhpos = sim.bhpos[idx]

        r = sim.nearest_dist_3D(sim.gaspos, bhpos)
        ind = np.where(r < 30.0)
        axarr[i].plot(r[ind], sim.temp[ind], marker='.', ls='none', label=subdir.replace("_","\_"), mec='none', mfc=col[i])
        if Ngb is not None:
            ind = np.argsort(r)[:Ngb[i]]
            axarr[i].plot(r[ind], sim.temp[ind], marker='.', ls='none', mec='none', mfc='k')
        #axarr[i].legend(frameon=False)
        axarr[i].set_ylabel("Temperature (keV)")
        axarr[i].set_yscale('log')
        axarr[i].set_ylim(7e-4, 10)
        axarr[i].set_title(subdir.replace("_","\_"))

fig.suptitle("snap "+str(snapnum)+" z = {:.2f}".format(sim.header.redshift)+" t = {:.2f}".format(sim.header.time))
plt.xlabel("Distance from BH (kpc)")
plt.savefig(outdir+outname)
print "Plot saved to", outdir+outname