#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 14 11:29:02 2017

@author: nh444
"""

import snap
import matplotlib.pyplot as plt
import numpy as np

outdir = "/data/curie4/nh444/project1/blackholes/"

#==============================================================================
# basedir = "/data/emergence3/deboras/Elliptical_ZoomIns/"
# subdirs = [#"L40_512_NoDutyRadioWeak_HalfThermal_FixEta",
#            #"L40_512_NoDutyRadioInt_HalfThermal_FixEta",
#            #"L40_512_LowDutyRadioWeak_HalfThermal_FixEta",
#            "L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff",
#            ]
# outname = "BH_vel_pos_vs_time_box_most_bound_particle.pdf"
# labels = [#"Ref",
#           #"Stronger radio",
#           #"Quasar duty cycle",
#           "Best"]   
# mpc_units = False
#==============================================================================

basedir = "/home/nh444/data/project1/zoom_ins/LDRIntRadioEffBHVel/"
#==============================================================================
# subdirs = ["c128_box_Arepo",
#            "c128_new",
#            "c128_new_noBHVel",
#            ]
# labels = ["old arepo",
#           "new arepo",
#           "new arepo non-BHVel",
#           ]
# outname = "BH_vel_pos_vs_time_c128_box_Arepo_BH.pdf"
#==============================================================================
subdirs = [#"c96_box_Arepo",
           #"c96_box_Arepo_2",
           "c96_box_Arepo_new",
           "c96_box_Arepo_new_BHVel",
           #"c96_box_Arepo_new_PM1024",
           #"c96_box_Arepo_new_Ngb",
           #"c96_FeedbackFix",
           #"c96_FeedbackFix_OldGrav",
           #"c96_FeedbackFix_OldGrav_3",
           #"c96_FeedbackFix_OldGrav_2",
           ]
labels = [#"old arepo",
          #"old arepo no IND_GRAV_SOFT",
          "old arepo modified",
          "old arepo modified BHVel",
          #"old arepo modified PMGRID 1024",
          "old arepo modified Ngb 256",
          "new arepo",
          "+ old gravity solver",
          "+ IND_GRAV_SOFT",
          "+ RCUT + IND_GRAV_SOFT",
          ]
outname = "BH_vel_pos_vs_time_c96_old_Arepo_BHVel.pdf"
mpc_units = True

basedir = "/home/nh444/data/project1/zoom_ins/MDRInt/"
subdirs = ["c448_MDRInt"]
labels = ["c448 MDRInt"]
maxsnapnums = [25]
outname = "BH_vel_pos_vs_time_c448_MDRInt.pdf"

#==============================================================================
# basedir = "/home/nh444/data/project1/zoom_ins/"
# subdirs = [#"MDRInt/c448_MDRInt", "MDRInt/c448_MDRInt_noprescrit",
#            "Illustris_model/c448_Ill"]
# labels = [#"Fiducial", "without pres. crit.",
#           "Illustris model"]
# maxsnapnums = [#5, 9,
#                9]
# outname = "BH_vel_pos_vs_time_c448_Illustris.pdf"
#==============================================================================

redshift = True ## plot vs redshift rather than the scale factor

group = 0
subhalo = 0 ## subhalo no. RELATIVE TO GROUP - 0 is most massive subhalo in group
bound_particle = False ## plot most bound particle instead of black hole
BH_rel_bound_particle = True ## plot BH relative to most bound particle rather than centre of subhalo
subhalo_only = False ## Only consider BH bound to specified subhalo, otherwise look within R500
most_massive = True ## use most massive BH rather than the closest to the subhalo centre
plot_CM = False ## Plot distance relative to subhalo CM position
logscale = False ## log scale for distance
plot_med = False

if BH_rel_bound_particle:
    import readsnap as rs
if BH_rel_bound_particle or not plot_CM:
    f, axarr = plt.subplots(2, sharex=True, figsize=(10,10))
else:
    f, axarr = plt.subplots(3, sharex=True, figsize=(10,10))
    axarr[2].set_ylabel("Distance CM [kpc/h]")
if redshift:
    axarr[-1].set_xlabel("1+z")
else:
    axarr[-1].set_xlabel("Time")
unitvel_in_cm_per_s = 1.0e5 ## the usual code units
VelFac = unitvel_in_cm_per_s / 3.085677581305729e+21 * (3.154e7 * 1e6) ## kpc per Myr
axarr[0].set_ylabel("Relative Velocity [kpc/h per Myr]")
axarr[1].set_ylabel("Distance [kpc/h]")
if logscale:
    axarr[1].set_yscale('log')
#axarr[1].set_ylim(0,12)
col = ["#c7673e","#78a44f","#6b87c8","#934fce","#e6ab02","#be558f","#47af8d"]

for i, subdir in enumerate(subdirs):
    t = []
    relpossub = []
    relCMsub = []
    relvelsub = []
    relposgrp = []
    relCMgrp = []
    relvelgrp = []
    bhmass = 0.0
    for snapnum in range(0, maxsnapnums[i]+1):
        sim = snap.snapshot(basedir+subdir, snapnum, mpc_units=mpc_units)
        if not hasattr(sim, "cat") or len(sim.cat.group_firstsub)==0: ##skip snapshots without halo catalogue
            continue
        
        sub = sim.cat.group_firstsub[group]+subhalo

        if bound_particle:
            ## Use sub_id_mostbound, the ID of the most bound particle in the suhalo
            sim.load_ids()
            idx = np.where(sim.ids == sim.cat.sub_id_mostbound[sub])[0]
            assert idx.size==1, "Found more than one particle with ID="+str(sim.cat.sub_id_mostbound[sub])
            sim.load_vel()
            sim.load_pos()
            partpos = sim.pos[idx]
            partvel = sim.vel[idx]
            relpossub.append(sim.nearest_dist_3D(partpos, sim.cat.sub_pos[sub]))
            relCMsub.append(sim.nearest_dist_3D(partpos, sim.cat.sub_cm[sub]))
            relvelsub.append(sim.nearest_dist_3D(partvel, sim.cat.sub_vel[sub]))
            relposgrp.append(sim.nearest_dist_3D(partpos, sim.cat.group_pos[group]))
            relCMgrp.append(sim.nearest_dist_3D(partpos, sim.cat.group_cm[group]))
            relvelgrp.append(sim.nearest_dist_3D(partvel, sim.cat.group_vel[group]))
            t.append(sim.header.time)
            print t[-1], relposgrp[-1], partpos, sim.cat.group_pos[group], sim.cat.group_m_crit500[group]
        else:
            sim.get_hostsubhalos() ## Creates array containing host halo index of every particle (-1 if not part of any subhalo)
            if subhalo_only:
                ## get indices of BHs in given subhalo
                ind = np.where(sim.hostsubhalos[sim.species_start[5]:sim.species_end[5]] == sub)[0] ## indices of BHs in subhalo
                if ind.size<1:
                    continue
                sim.load_bhpos()
            else:
                ## get indices of BHs within R500 of group centre
                sim.load_bhpos()
                r = sim.nearest_dist_3D(sim.bhpos, sim.cat.group_pos[group])
                ind = np.where(r <= sim.cat.group_r_crit200[group])[0]
                if ind.size<1:
                    continue

            sim.load_bhmass()
            sim.load_bhvel()
            sim.load_bhids()
            if most_massive:
                idx = ind[sim.bhmass[ind].argsort()[-1]] ## index of most massive BH in subhalo
            else:
                r = sim.nearest_dist_3D(sim.bhpos[ind], sim.cat.sub_pos[sub]) ## distance from subhalo position
                idx = ind[r.argsort()[0]] ## index of BH nearest sub_pos
            
            print "\n", ind.size, "BHs at t =", sim.header.time, "bhid =", sim.bhids[idx], "m =", sim.bhmass[idx]
            print "d2sub =", sim.nearest_dist_3D(sim.bhpos[idx], sim.cat.sub_pos[sub])
            print "bhpos   =", sim.bhpos[idx]
            print "sub pos =", sim.cat.sub_pos[sub], sim.cat.sub_mass[sub]
            print "sim.bhmass[ind] =", sim.bhmass[ind]
            print "d2sub[ind] =", sim.nearest_dist_3D(sim.bhpos[ind], sim.cat.sub_pos[sub])
            #assert sim.bhmass[idx] >= bhmass, "BH less massive than in previous snapshot "+str(sim.bhmass[idx])+" vs "+str(bhmass)+" - different BH or subhalo"
            ## BH must be more massive than in previous snapshot if it's the same BH
            bhmass = sim.bhmass[idx]
            bhid = sim.bhids[idx]
            bhpos = sim.bhpos[idx]
            bhvel = sim.bhvel[idx]
            if not subhalo_only:## find its host subhalo
                newsub = sim.hostsubhalos[sim.species_start[5] + idx]
                if newsub != sub:
                    print "\033[31m"+"host subhalo "+str(newsub)+" is different to specified subhalo "+str(sub)+" at t="+str(sim.header.time)+"\033[0m"
                    print "\033[31m"+"sub "+str(sub)+" pos = ", sim.cat.sub_pos[sub], "mass =", sim.cat.sub_mass[sub], "\033[0m"
                    print "\033[31m"+"sub "+str(newsub)+" pos = ", sim.cat.sub_pos[newsub], "mass =", sim.cat.sub_mass[newsub], "\033[0m"
                    print "\033[31m"+"dist = ", (((sim.cat.sub_pos[newsub]-sim.cat.sub_pos[sub])**2).sum())**0.5, "\033[0m"
                    print "\033[31m"+"relative vel = ", (((sim.cat.sub_vel[newsub]-sim.cat.sub_vel[sub])**2).sum())**0.5, "\033[0m"
                    print "sim.bhmass[ind] =", sim.bhmass[ind]
                    #sub = newsub
                    print "host d2sub =", sim.nearest_dist_3D(bhpos, sim.cat.sub_pos[newsub])
            

            if BH_rel_bound_particle:
                ## Get most bound particle using sub_id_mostbound, the ID of the most bound particle in the suhalo
                sim.load_ids()
                idx = np.where(sim.ids == sim.cat.sub_id_mostbound[sub])[0][0]
                parttype = np.where(idx < sim.species_end)[0][0]
                print "parttype =", parttype
                print "idx =", idx
                print "sim.species_start[parttype] =", sim.species_start[parttype]
                print "sim.species_end[parttype] =", sim.species_end[parttype]
                #sim.load_vel()
                #sim.load_pos()
                #partpos = sim.pos[idx]
                #partvel = sim.vel[idx]
                vel = rs.read_block(sim.snapname, "VEL ", parttype=parttype, cosmic_ray_species=sim.num_cr_species)
                pos = rs.read_block(sim.snapname, "POS ", parttype=parttype, cosmic_ray_species=sim.num_cr_species)
                print "len(pos) =", len(pos)
                if sim.mpc_units:
                    pos *= 1000.0
                partpos = pos[idx - sim.species_start[parttype]]
                partvel = vel[idx - sim.species_start[parttype]]
                relpossub.append(sim.nearest_dist_3D(bhpos, partpos))
                relvelsub.append(sim.nearest_dist_3D(bhvel, partvel))
                t.append(sim.header.time)
                print t[-1], bhpos, partpos
            else:
                relpossub.append(sim.nearest_dist_3D(bhpos, sim.cat.sub_pos[sub]))
                relCMsub.append(sim.nearest_dist_3D(bhpos, sim.cat.sub_cm[sub]))
                relvelsub.append(sim.nearest_dist_3D(bhvel, sim.cat.sub_vel[sub]))
                relposgrp.append(sim.nearest_dist_3D(bhpos, sim.cat.group_pos[group]))
                relCMgrp.append(sim.nearest_dist_3D(bhpos, sim.cat.group_cm[group]))
                relvelgrp.append(sim.nearest_dist_3D(bhvel, sim.cat.group_vel[group]))
                t.append(sim.header.time)
          
    if redshift: t = 1.0 / np.asarray(t)
    relpossub = np.asarray(relpossub)
    relvelsub = np.asarray(relvelsub)
    print "relvelsub =", relvelsub
    print "\nrelpossub =", relpossub
    if logscale:
        relpossub[relpossub<0.0101] = 0.0101
    if BH_rel_bound_particle:
        axarr[0].plot(t, relvelsub * VelFac, lw=2, label=labels[i].replace("_", "\_"))
        axarr[1].plot(t, relpossub, lw=2, label=labels[i].replace("_", "\_"))
    else:
        axarr[0].plot(t, relvelsub * VelFac, lw=2, label=labels[i].replace("_", "\_")+" (subhalo)")
        axarr[1].plot(t, relpossub, lw=2, label=labels[i].replace("_", "\_")+" (subhalo)")
        #axarr[0].plot(t, relvelgrp, lw=2, label=labels[i].replace("_", "\_")+" (group)", ls='dashed')
        #axarr[1].plot(t, relposgrp, lw=2, label=labels[i].replace("_", "\_")+" (group)", ls='dashed')
        if plot_CM:
            axarr[2].plot(t, relCMsub, lw=2, label=labels[i].replace("_", "\_")+" (subhalo CM)")
            #axarr[2].plot(t, relCMgrp, lw=2, label=labels[i].replace("_", "\_")+" (group CM)", ls='dashed')
        if plot_med:
            assert redshift
            med = np.median(relvelsub * VelFac)
            axarr[0].annotate('', xytext=(10, med), xy=(8, med),
            arrowprops=dict(color=col[i], width=1, headwidth=8))
            med = np.median(relpossub)
            axarr[1].annotate('', xytext=(10, med), xy=(8, med),
            arrowprops=dict(color=col[i], width=1, headwidth=8))
    
if redshift:
    #xmin, xmax = axarr[-1].get_xlim()
    axarr[-1].set_xlim(10, 1)
    axarr[-1].set_xscale('log')
    import matplotlib.ticker as ticker
    axarr[-1].xaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))
    dat = np.loadtxt("/data/curie4/nh444/Sijing/dist.txt")
    axarr[1].plot(dat[:,0], dat[:,1], lw=1.5, alpha=0.5, c='0.5', label="Sijing")
    axarr[1].axvline(x=4, ls='dashed', c='0.5')
for ax in axarr:
    ax.legend(frameon=False, fontsize=12, ncol=2)
if bound_particle:
    f.suptitle("Most bound particle subhalo "+str(sub))
elif BH_rel_bound_particle:
    if most_massive: f.suptitle("Most massive blackhole rel. to most bound particle")
    else: f.suptitle("Blackhole nearest subhalo centre rel. to most bound particle")
else:
    if most_massive: f.suptitle("Most massive blackhole relative to subhalo "+str(sub))
    else: f.suptitle("Blackhole nearest centre of subhalo "+str(sub))
plt.savefig(outdir+outname)
print "Plot saved to", outdir+outname