#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 16 16:36:50 2017
Compares masses of central black holes
@author: nh444
"""
import snap
import matplotlib.pyplot as plt
import numpy as np

basedir = "/data/emergence4/deboras/Elliptical_ZoomIns/"
mpc_units = False
subdirs = ["L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff",
           #"L40_512_LowDutyRadioWeak_HalfThermal_FixEta_BHChangeVel",
           #"L40_512_LowDutyRadioWeak_HalfThermal_FixEta",
           "L40_512_NoDutyRadioIntLow_HalfThermal_FixEta_BHChangeVel",
           "L40_512_NoDutyRadioIntLow_HalfThermal_FixEta"]
labels = [subdir.replace("_","\_") for subdir in subdirs]
labels = ["Best",
          #"QM duty cycle with BHVel", "QM duty cycle no BHVel",
          "BHVel", "no BHVel"]

basedir = "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/"
mpc_units = True
subdirs = ["c96_box_Arepo_new", "c96_box_Arepo_new_LowSoft",
           "c128_box_Arepo_new", "c128_box_Arepo_new_LowSoft",
           "c160_box_Arepo_new", "c160_box_Arepo_new_LowSoft",
           "c256_box_Arepo_new", "c256_box_Arepo_new_LowSoft",
           "c320_box_Arepo_new", "c320_box_Arepo_new_LowSoft",
           "c384_box_Arepo_new", "c384_box_Arepo_new_LowSoft",
           #"c448_box_Arepo_new", "c448_box_Arepo_new_LowSoft",
           ]
labels = ["c96 High Soft", "c96 Low Soft",
          "c128 High Soft", "c128 Low Soft",
          "c160 High Soft", "c160 Low Soft",
          "c256 High Soft", "c256 Low Soft",
          "c320 High Soft", "c320 Low Soft",
          "c384 High Soft", "c384 Low Soft",
          #"c448 High Soft", "c448 Low Soft",
          ]
n = 2 # comparison group size, e.g. if n=2, compares every pair of subdirs
outdir = "/data/curie4/nh444/project1/blackholes/"

h = 0.7

snapnum = 25

def main():
    #compare_mass_distribution(outname="BHmass_dist_comparison_LowSoft.pdf")
    #compare_central_BHmass(outname="BHmass_comparison_LowSoft.pdf")
    compare_subhalo_mass_distribution()
    
def compare_central_BHmass(outname="BHmass_comparison_LowSoft.pdf"):
    ### Compares the masses of the central black holes of the most massive group in each sim
    fig, ax = plt.subplots(figsize=(7,7))
    x = range(1, (len(subdirs) / n) + 1) ## for zoom names on x axis
    ax.set_xlim(0, (len(subdirs) / n) + 1)
    newlabels = [label.split()[0] for label in labels[::n]]
    ax.set_yscale('log')
    #ax.set_xscale('log')
    ax.set_ylabel('Central M$_{\mathrm{BH}}$ [M$_{\odot}$]')
    #ax.set_xlabel("Stellar Mass [M$_{\odot}$]")
    
    for dirnum, subdir in enumerate(subdirs):
        sim = snap.snapshot(basedir+subdir, snapnum, mpc_units=mpc_units)
        centre = sim.cat.sub_pos[sim.cat.group_firstsub[0]]
        sim.load_bhpos()
        sim.load_bhmass()
        ind = np.where(sim.bhmass > 0.0)[0]
        r = sim.nearest_dist_3D(sim.bhpos[ind], centre)
        bhmass = sim.bhmass[ind[np.argmin(r)]] * 1e10 / h
                            
        if dirnum % n == 0:
            p = ax.plot(x[dirnum / n], bhmass, marker='o', mec='none', label=labels[dirnum], ls='none')
            #p = ax.plot(sim.cat.sub_masstab[sim.cat.group_firstsub[0], 4], bhmass, marker='o', mec='none', label=labels[dirnum], ls='none')
        else:
            ax.plot(x[dirnum / n], bhmass, marker='D', mfc='none', mec=p[0].get_color(), label=labels[dirnum], ls='none')
            #ax.plot(sim.cat.sub_masstab[sim.cat.group_firstsub[0], 4], bhmass, marker='D', mfc='none', mec=p[0].get_color(), label=labels[dirnum], ls='none')
            
    ax.legend(loc='upper left', ncol=2, fontsize=12)
    plt.xticks(x, newlabels)
    fig.savefig(outdir+outname)
    print "Plot saved to", outdir+outname
    
def compare_mass_distribution(outname="BHmass_comparison.pdf"):
    ### Plots the mass distribution of central BHs
    nplots = len(subdirs) / n
    ncols=2
    fig, axes = plt.subplots(nrows=nplots/ncols, ncols=ncols, figsize=(10,16))
    for ax in axes.ravel():
        ax.set_xscale('log')
        ax.set_yscale('log')
        ax.set_xlabel('M$_{\mathrm{BH}}$ [M$_{\odot}$]')
    
    for dirnum, subdir in enumerate(subdirs):
        sim = snap.snapshot(basedir+subdir, snapnum, mpc_units=mpc_units)
        sim.get_hostsubhalos()
        sim.get_sub_bh_props()
        
        ax = axes[dirnum/n/ncols, (dirnum/n)%ncols] # which axes
        ind = np.where(sim.sub_bhmass1 > 0.0)
        if dirnum % n == 0:## first to be plotted on this axes
            bins = np.logspace(np.log10(np.min(sim.sub_bhmass1[ind]) * 1e10 / h), np.log10(np.max(sim.sub_bhmass1[ind]) * 1e10 / h), num=20)
            ymin = np.inf
            ymax = 0
        hist, bin_edges, N = ax.hist(sim.sub_bhmass1[ind] * 1e10 / h, bins=bins, histtype='step', lw=1.5, label=labels[dirnum])
        if np.min(hist) < ymin:## could just use zero...
            ymin = np.min(hist)
        if np.max(hist) > ymax:
            ymax = np.max(hist)
        ax.set_ylim(ymin, int(ymax*1.1)+1)
        
    for ax in axes.ravel():
        ax.legend(loc='best', fontsize=12)
    plt.tight_layout()
    fig.savefig(outdir+outname)
    print "Plot saved to", outdir+outname
    
def compare_subhalo_mass_distribution(outname="subhalo_mass_comparison.pdf"):
    ### Plots the mass distribution of central BHs
    nplots = len(subdirs) / n
    ncols=2
    fig, axes = plt.subplots(nrows=nplots/ncols, ncols=ncols, figsize=(10,16))
    for ax in axes.ravel():
        ax.set_xscale('log')
        ax.set_yscale('log')
        ax.set_xlabel('M$_{\mathrm{sub}}$ [M$_{\odot}$]')
    
    for dirnum, subdir in enumerate(subdirs):
        sim = snap.snapshot(basedir+subdir, snapnum, mpc_units=mpc_units)
        
        ax = axes[dirnum/n/ncols, (dirnum/n)%ncols] # which axes
        ind = np.where(sim.cat.sub_mass > 0.0)[0]
        if dirnum % n == 0:## first to be plotted on this axes
            bins = np.logspace(np.log10(np.min(sim.cat.sub_mass[ind]) * 1e10 / h), np.log10(np.max(sim.cat.sub_mass[ind]) * 1e10 / h), num=20)
            ymin = np.inf
            ymax = 0
        hist, bin_edges, N = ax.hist(sim.cat.sub_mass[ind] * 1e10 / h, bins=bins, histtype='step', lw=1.5, label=labels[dirnum])
        if np.min(hist) < ymin:## could just use zero...
            ymin = np.min(hist)
        if np.max(hist) > ymax:
            ymax = np.max(hist)
        ax.set_ylim(ymin, int(ymax*1.1)+1)
        
    for ax in axes.ravel():
        ax.legend(loc='best', fontsize=12)
    plt.tight_layout()
    
if __name__ == "__main__":
    main()