#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 15 10:46:36 2018
Build a rough merger tree. A catalogue is created for each snapshot that contains,
for each subhalo, the ID of the descendant subhalo (strict hierarchical formation
is assumed so each subhalo can have only one descendant) in the next (later) snapshot,
and the ID of the main progenitor subhalo in the previous (earlier) snapshot, if any.

The algorithm is that described in Appendix A2.1 of Jiang+2014 but does not currently
take into account "missing" subhaloes as discussed in A2.2
@author: nh444
"""
import snap
import readsnap as rs
import numpy as np
import h5py

basedir = "/home/nh444/data/project1/zoom_ins/MDRInt/"
simdirs = ["c448_MDRInt"]
mpc_units = True
snapnums = range(20, 26) ## must be high to low redshift
#snapnums = range(0,100)

## We will track the following number of subhaloes from the first snapshot
## but will increase this number in later snapshots when necessary
nsubs = 10

## 'most bound' method parameters as in Jiang+14:
Nlinkmax = 100
Nlinkmin = 10
f_trace = 0.1

## level of verbosity
verbose = 2

## index of particle type
parttype = 1 ## currently DM particle tracking only

for simdir in simdirs:
    if simdir[-1] != "/":
        simdir += "/"
    next_main_prog = None
    for snapidx, snapnum in enumerate(snapnums[:-1]):
        print "\n\nStarting snapshot", snapnum
        ## Load snapshot and check that we have a halo catalogue
        cursnap = snap.snapshot(basedir+simdir, snapnum, mpc_units=mpc_units)
        if not hasattr(cursnap, "cat"):
            print "No halo catalogue, skipping..."
            continue
        if len(cursnap.cat.group_len) == 0:
            print "No groups in halo catalogue, skipping..."
            continue
        
        ## Load the next snapshot unless this is the last snapshot
        last_snapshot = False
        try:
            nextsnap = snap.snapshot(basedir+simdir, snapnums[snapidx+1], mpc_units=mpc_units)
        except IOError:
            ## This is the last snapshot
            last_snapshot = True
            
        if not last_snapshot:
            if nextsnap.header.redshift > cursnap.header.redshift:
                raise ValueError("Snapshot "+str(snapnums[snapidx+1])+" has a higher redshift than snapshot "+str(snapnum))
        
        print "Loading hostsubhalos..."
        ## Get the host subhalo for all particles of the given type in both snapshots
        cursnap.get_hostsubhalos()
        curhostsubhalos = cursnap.hostsubhalos[cursnap.species_start[parttype]:cursnap.species_end[parttype]]
        if not last_snapshot:
            nextsnap.get_hostsubhalos()
            nexthostsubhalos = nextsnap.hostsubhalos[nextsnap.species_start[parttype]:nextsnap.species_end[parttype]]
        
        print "Loading IDs..."
        ## load particle IDs so we can trace particles between the snapshots
        curids = rs.read_block(cursnap.snapname, "ID  ", parttype=parttype, cosmic_ray_species=cursnap.num_cr_species)
        if not last_snapshot:
            nextids = rs.read_block(nextsnap.snapname, "ID  ", parttype=parttype, cosmic_ray_species=nextsnap.num_cr_species)
        
        print "Loading potential..."
        ## Load the gravitational potential of the particles so we can identify the most bound particles
        curpot = rs.read_block(cursnap.snapname, "POT ", parttype=parttype, cosmic_ray_species=cursnap.num_cr_species)
        if not last_snapshot:
            nextpot = rs.read_block(nextsnap.snapname, "POT ", parttype=parttype, cosmic_ray_species=nextsnap.num_cr_species)
        
        ## Define the subhaloes to consider in the current snapshot
        subnums = range(nsubs)
#==============================================================================
#         if next_main_prog is not None:
#             ## If a main progenitor is missing, extend the range of subhaloes
#             if np.max(next_main_prog) >= nsubs:
#                 print "Increasing number of subhaloes from", nsubs, "to", np.max(next_main_prog)+1
#                 nsubs = np.max(next_main_prog) + 1
#                 subnums = range(nsubs)
#         if verbose: print "Using", nsubs, "subhaloes."
#==============================================================================
        
        if not last_snapshot:
            descendants = np.full_like(subnums, -1, dtype=int) ## index of descendant subhalo in later snapshot
        cur_main_prog = np.full_like(subnums, -1, dtype=int) ## index of main progenitor subhalo in previous (earlier) snapshot
        if next_main_prog is not None:
            ## store main progenitor information identified in previous loop
            #main_prog_sub = np.where(next_main_prog > -1)[0]
            main_prog_sub = np.where((next_main_prog > -1) & (next_main_prog < nsubs))[0]
            cur_main_prog[next_main_prog[main_prog_sub]] = main_prog_sub
            if verbose > 1: print "cur_main_prog =", cur_main_prog

        if not last_snapshot:
            next_main_prog = np.full_like(subnums, -1, dtype=int) ## subhaloes in the next snapshot that will have main progenitors
        
            for subidx, subnum in enumerate(subnums):
                if verbose>1: print "\nsubnum =", subnum
                if cursnap.cat.sub_lentab[subnum, parttype] < Nlinkmin:
                    if verbose>1: print "Not enough particles. Skipping subhalo", subnum
                    descendants[subidx] = -2
                    continue
                ## Get indices of particles in subhalo in current snap
                curpind = np.where(curhostsubhalos == subnum)[0]
                ## Only consider the Nlink most bound
                Nlink = int(min(Nlinkmax, max(f_trace*len(curpind), Nlinkmin)))
                if verbose>2: print "Nlink =", Nlink
                ## get indices which sort the subhalo particles by potential
                curpotind = np.argsort(curpot[curpind])
                ## Take the IDs of Nlink particles with the smallest potential (most bound)
                match_ids = curids[curpind[curpotind[:Nlink]]]
                del curpotind
                del curpind
                
                if verbose>1: print "Matching IDs to find candidates..."
                ## Get indices of particles in next snap with matching IDs
                nextpind = np.nonzero(np.in1d(nextids, match_ids, assume_unique=True))[0]
                ## Get subhalos in next snap which contain at least one of the
                ## particles in the current subhalo
                sub_candidates = np.unique(nexthostsubhalos[nextpind])
                ## Values <0 indicate particles not belonging to a subhalo so ignore them
                sub_candidates = sub_candidates[sub_candidates > -1]
                if verbose>1: print "sub_candidates =", sub_candidates
                del nextpind
                
                ### Now to check if any of these subhalo candidates in the next
                ### snapshot received a larger fraction of their Nlink particles
                ### from the current progenitor halo than from any other halo.
                ### If yes, then the current subhalo will be the main progenitor
                ### of the chosen descendant
                if len(sub_candidates) > 0:
                    
                    prog_cand_ind = [] ## indices of the main progenitor candidates
                    main_progenitor = False
                    sub_cand_npart = np.zeros(len(sub_candidates), dtype=int)
                    
                    if verbose>1: print "Searching candidates..."
                    for cand_idx, sub_cand in enumerate(sub_candidates):
                        if verbose>1: print "sub_cand =", sub_cand
                        
                        ## First find the constituent particles
                        pind = np.where(nexthostsubhalos == sub_cand)[0]
                        
                        ## Find the number of particles that were bound to subnum
                        sub_cand_npart[cand_idx] = sum([1 for x in nextids[pind] if x in match_ids])
                        
                        ## Now get the IDs of the Nlink most bound
                        Nlink = int(min(Nlinkmax, max(f_trace*len(pind), Nlinkmin)))
                        if verbose>2: print "Nlink =", Nlink
                        nextpotind = np.argsort(nextpot[pind])
                        cand_ids = nextids[pind[nextpotind[:Nlink]]]
                        
                        ## Now find host subhalos for these particles in the current (earlier) snapshot
                        if verbose>1: print "Matching IDs for candidate..."
                        ## Get particles in current snapshot with matching IDs
                        ind = np.nonzero(np.in1d(curids, cand_ids, assume_unique=True))[0]
                        
                        ## determine if this candidate received more of these particles from the 
                        ## current progenitor halo than any other subhalo in the earlier snapshot
                        if verbose>1: print "Finding other progenitors for candidate..."
                        prog_subs, counts = np.unique(curhostsubhalos[ind], return_counts=True)
                        if prog_subs[np.argmax(counts)] == subnum:
                            prog_cand_ind.append(cand_idx)
                            
                    if len(prog_cand_ind) > 0:
                        if verbose>1: print "Got possible main progenitors", sub_candidates[prog_cand_ind]
                        ## Only consider candidates who received a larger fraction of particles
                        ## from subnum than any other subhalo
                        sub_candidates = sub_candidates[prog_cand_ind]
                        sub_cand_npart = sub_cand_npart[prog_cand_ind]
                        main_progenitor = True
                        
                    ## Now take the descendant as the candidate that received the largest
                    ## fraction of Nlink most bound particles
                    descendants[subidx] = sub_candidates[np.argmax(sub_cand_npart)] ## note that if multiple candidates have the same number, the first occurence is returned by np.argmax
                    if main_progenitor:
                        ## Set the element in 'next_main_prog' corresponding to the
                        ## current subhalo to the descendant number
                        next_main_prog[subidx] = descendants[subidx]
                        
                else: ## no subhalo candidates
                    descendants[subidx] = -3
                    
                if verbose>1: print "descendant =", descendants[subidx]
                if verbose>1 and descendants[subidx] != subnum:
                    print "subhalo", subnum, "becomes", descendants[subidx]
                
        ## Save as a hdf5 file for this snapshot
        with h5py.File(basedir+simdir+"merger_cat_"+str(snapnum).zfill(3)+".hdf5", "w") as f:
            f.create_dataset("SubNum", data=np.c_[subnums])
            if not last_snapshot:
                f.create_dataset("Descendant", data=np.c_[descendants])
            f.create_dataset("MainProgenitor", data=np.c_[cur_main_prog])
               

                
                
            
            