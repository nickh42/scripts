import numpy as np
import pylab as plt
import readsnap as rs
import sys
import os
import scipy.interpolate
import matplotlib.tri as tri
from matplotlib import ticker
from matplotlib.colors import LogNorm
import math


filename = sys.argv[1]
type = 0 #particle type to consider

if filename=="":
    print "No filename given in first argument"
    sys.exit()

if os.path.exists(filename):
    print "Found", filename
else:
    print "File", filename, "not found."
    sys.exit()


pos = rs.read_block(filename, "POS", parttype=type, verbose=True)
density = rs.read_block(filename, "RHO", parttype=type, verbose=True)
#pos = np.asarray(pos)

z1 = 103
z2 = 104
x1 = 96.9
x2 = 97.0
y1 = 100.65
y2 = 100.75
filter = (pos[:,0] > x1) & (pos[:,0] < x2) & (pos[:,1] > y1) & (pos[:,1] < y2) & (pos[:,2] > z1) & (pos[:,2] < z2)
print "For", z1, "<z<", z2, "N =", filter.sum()

'''
for z1 in range(0,3000,20):
    z2 = z1 + 20
    mask = (pos[:,2] > z1) & (pos[:,2] < z2)
    print "For", z1, "<z<", z2, "N =", mask.sum()
'''

pos = pos[filter,:]
density = density[filter]

x = pos[:,0]
y = pos[:,1]
z = density[:]

#create grid of interpolation points
N = 1000
dx = (x2 - x1) / N
xi = np.linspace(x1, x2, N)#creates regular grid points in x
yi = np.linspace(y1, y2, N)
xi, yi = np.meshgrid(xi, yi)

#Interpolate onto grid

#Radial basis function interpolation
#rbf = scipy.interpolate.Rbf(x, y, z, function='linear')
#zi = rbf(xi, yi)

#'griddata' interpolation
zi = scipy.interpolate.griddata((x, y), z, (xi, yi), method='linear')

#nearest-neighbour interpolation
#zi = scipy.interpolate.NearestNDInterpolator((x, y), z)

plt.imshow(zi, vmin=z.min(), vmax=z.max(), norm=LogNorm(), origin='lower', extent=[x.min(), x.max(), y.min(), y.max()])
#plt.scatter(x, y, c=z)
plt.colorbar(label='Density')
plt.title("Gas density in %.1f < z < %.1f dx = 0.01" %(z1, z2))
plt.xlabel('X')
plt.ylabel('Y')
plt.show()
'''

#Delaunay triangulation
triang = tri.Triangulation(x, y) 
plt.tricontourf(triang, z, locator=ticker.LogLocator())
#plt.scatter(x, y, c=z)
plt.colorbar()
plt.show()
'''
print "All done."
exit()
