#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 12 17:50:33 2016
Plot phase space plot for gas in specified group(s)
@author: nh444
"""
import snap
import os
import matplotlib.pyplot as plt
from scaling.xray_python import xray
from matplotlib.colors import LogNorm
import numpy as np
from scipy.constants import k as kboltzmann
from scipy.constants import eV
keV = 1000.0*eV
UnitLength_in_cm = 3.085678e21 ## 1.0 kpc
UnitMass_in_g = 1.9884430e43 ## 1.0e10 solar masses
ag_hydrogen = 0.70683 # Anders & Grevesse solar mass fractions
ag_helium = 0.27431
ag_metals = 0.01886

outdir = "/data/curie4/nh444/project1/L-T_relations/"
h_scale = 0.679

simdir = "/data/curie4/nh444/project1/boxes/"
simname = "L40_512_MDRIntRadioEff"
simname = "L40_512_LDRIntRadioEff_noBH"
label = None
mpc_units = False
highres_only = False

simdir = "/data/curie4/nh444/project1/zoom_ins/MDRInt/"
simname = "c464_MDRInt"
label = r"c464\_MDRInt"
outdir = "/data/curie4/nh444/project1/L-T_relations/MDRInt/"
mpc_units = True
highres_only = True

snapnum = 79
groupnums = [0]

if not os.path.exists(outdir):
    os.mkdir(outdir)

projected = False  # use gas within projected aperture (whole box)
plot_metallicity = False  # Colour by mass-weighted metallicity rather than mass
plot_flux = True  # Colour by X-ray flux rather than mass
fluxdir = "/data/vault/nh444/ObsData/flux_tables/flux_tables_apec_02-12keV_redshifted/"
log = False  # log scales on both axes
inKelvin = False
Tvir_thresh = 4.0

if inKelvin:
    tempnorm = keV/kboltzmann
else:
    tempnorm = 1.0

# =============================================================================
# print "Loading sim..."
# sim = snap.snapshot(simdir+simname+"/", snapnum, mpc_units=mpc_units)
# z = sim.header.redshift
# # Factor to convert density in code units to physical g cm^-3
# rhonorm = UnitMass_in_g / h_scale / (UnitLength_in_cm / h_scale / (1+z))**3
# print "Loading data..."
# sim.load_gasrho()
# sim.get_temp(del_ne=False)
# sim.load_gaspos()
# sim.load_gasmass()
# if highres_only:
#     sim.load_highresgasmass()
# if plot_metallicity or plot_flux:
#     sim.load_Zgas()
# print "Loaded."
# 
# =============================================================================
for groupnum in groupnums:
    outname = "phase_space_grp"+str(groupnum)+".pdf"

    M500 = sim.cat.group_m_crit500[groupnum]
    R500 = sim.cat.group_r_crit500[groupnum]
    T200 = sim.cat.group_T_crit200[groupnum]
    
    Rmin = 0.0
    Rmax = R500
    if projected:
        partrads_sq = ((sim.cat.group_pos[groupnum,:2] - sim.gaspos[:,:2])**2).sum(axis=1)
    else:
        partrads_sq = ((sim.cat.group_pos[groupnum] - sim.gaspos)**2).sum(axis=1)
    ind = np.where((partrads_sq < Rmax**2) & (partrads_sq > Rmin**2))[0]
    if highres_only:
        ind = ind[np.where(sim.hrgm[ind] > 0.5*sim.gasmass[ind])[0]]
    if len(ind) == 0:
        print "Skipping group", groupnum
        continue
    #ind = np.intersect1d(ind, np.where(np.logical_and((sim.temp >= 3e4*8.617342e-8) | (sim.gasrho <= 500*0.044*2.77536627e-8), sim.gasrho < 0.00085483/(1+sim.header.redshift)**3))[0])
    rho = sim.gasrho[ind] * rhonorm
    temp = sim.temp[ind]*tempnorm
    mass = sim.gasmass[ind] * 1e10 / h_scale
    totMass = np.sum(mass)
    
    Tcut = 3e4*8.617342e-8 * tempnorm ## default is 3e4 K
    Rhocut = 0.00085483/(1+z)**3 * rhonorm ## 2.67e-25 g/cm^3
    #Rhocut2 = 500*0.044*2.77536627e-8 * rhonorm ## 1.9e-28 g/cm^3
    ## this used to be included in filter_type=="default"
    ## gas was included if rho<rho_SF and EITHER T>3e4 K OR rho<Rhocut2

    gamma = 0.25 ## default is 0.25 from Rasia 2012
    rasia = lambda rho: tempnorm * 3e6 * (1e-27)**(0.25 - gamma) * (rho/(h_scale/0.72)**2)**gamma ## Rasia 2012 cut-off relation - gives temp in keV
    rasia2 = lambda rho: tempnorm * 3e6 * (1e-27)**(0.25 - gamma) * (M500 / 56091.5) * (910.6 / (R500 / (1+z))) * (rho/(h_scale/0.72)**2)**gamma ## Rasia 2012 cut-off relation with scaling - gives temp in keV
    N_def = 1e7 * (1e-27)**(0.25 - gamma) ## scale normalisation with gamma to match at 1e-27
    print "M500, R500 =", M500, R500
    N = N_def * (M500 / 59666.9) * (1008.8 / (R500 / (1+z))) ## scale relative to M500/R500 of c448
    rasia3 = lambda rho: tempnorm * N * (rho/(h_scale/0.6774)**2)**gamma ## normalisation is chosen for h=0.6774 so convert rho to same h
    
    cutInd = np.where((temp < Tcut) | (rho > Rhocut))[0]
    DefCutMass = np.sum(mass[cutInd])
    ColdCutMass = np.sum(mass[np.where(temp < Tcut)[0]])
    SFCutMass = np.sum(mass[np.where(rho > Rhocut)[0]])
    ColdSFCutMass = np.sum(mass[np.where((temp < Tcut) & (rho > Rhocut))[0]])
    cutInd = np.where(temp < rasia(rho))[0]
    rasiaMass = np.sum(mass[cutInd])
    cutInd = np.where(temp < rasia2(rho))[0]
    rasia2Mass = np.sum(mass[cutInd])
    cutInd = np.where(temp < rasia3(rho))[0]
    rasia3Mass = np.sum(mass[cutInd])

    fig, ax = plt.subplots(1,1, figsize=(9,8))
    cmap = plt.get_cmap('Blues')
    lw=2
    
    if log:
        x = np.log10(rho)
        y = np.log10(temp)
        xmin = int(np.min(x))-1
        xmax = int(np.max(x))
        ymin = int(np.min(y))-1
        ymax = int(np.max(y))+1
        xbins = np.linspace(xmin, xmax, num=100)
        ybins = np.linspace(ymin, ymax, num=100)
    else:
        x = rho
        y = temp
        xmin = np.min(x)
        xmax = np.max(x)
        ymin = np.min(y)
        ymax = np.max(y)
        xbins = np.logspace(int(np.log10(xmin))-1, int(np.log10(xmax))+1, num=100)
        ybins = np.logspace(int(np.log10(ymin))-1, int(np.log10(ymax))+1, num=100)
        ax.set_xscale('log')
        ax.set_yscale('log')
    
    if plot_metallicity:
        Z = sim.Zgas[ind] / (1-sim.Zgas[ind]) * (ag_hydrogen+ag_helium)/ag_metals
        hist, xedges, yedges = np.histogram2d(x, y, bins=[xbins, ybins], weights=Z*mass) ## Total Z*mass in each bin
        mass_hist, xedges, yedges = np.histogram2d(x, y, bins=[xbins, ybins], weights=mass)
        hist[hist>0] = hist[hist>0] / mass_hist[hist>0] ## mass weighted Z
        if log:
            img = ax.imshow(hist.transpose(), extent=[xmin, xmax, ymin, ymax], origin="lower",
                            #norm=LogNorm(vmin=np.min(hist[hist>0]), vmax=np.max(hist)),
                            cmap=cmap, interpolation='none')
        else:
            img = ax.pcolor(xedges, yedges, hist.transpose(),
                            #norm=LogNorm(vmin=np.min(hist[hist>0]), vmax=np.max(hist)),
                            cmap=cmap)
            plt.axis([10**(int(np.log10(xmin))-1), 10**(int(np.log10(xmax))+1), 10**(int(np.log10(ymin))-1), 10**(int(np.log10(ymax))+1)])
    elif plot_flux:
        src = xray.xray_source(z, sim.header.hubble, sim.header.omega_m, fluxdir=fluxdir)
        abund = sim.Zgas[ind] / (1-sim.Zgas[ind]) * (ag_hydrogen+ag_helium)/ag_metals
        flux = src.get_flux_from_table(sim.temp[ind], abund, sim.ne[ind],
                                       sim.gasmass[ind], sim.gasrho[ind],
                                       modelname="apec", ignore_outside_range=False)
        hist, xedges, yedges = np.histogram2d(x, y, bins=[xbins, ybins], weights=flux)
#        num, _, _ = np.histogram2d(x, y, bins=[xbins, ybins])
#        hist[hist>0] = hist[hist>0] / num[hist > 0]
        if log:
            img = ax.imshow(hist.transpose(), extent=[xmin, xmax, ymin, ymax], origin="lower",
                            norm=LogNorm(vmin=np.min(hist[hist > 1e-30]), vmax=np.max(hist)),
                            cmap=cmap, interpolation='none')
        else:
            img = ax.pcolor(xedges, yedges, hist.transpose(),
                            norm=LogNorm(vmin=np.min(hist[hist > 1e-20]), vmax=np.max(hist)),
                            cmap=cmap)
            plt.axis([10**(int(np.log10(xmin))-1), 10**(int(np.log10(xmax))+1), 10**(int(np.log10(ymin))-1), 10**(int(np.log10(ymax))+1)])       
    else:
        hist, xedges, yedges = np.histogram2d(x, y, bins=[xbins, ybins], weights=mass)
        if log:
            img = ax.imshow(hist.transpose(), extent=[xmin, xmax, ymin, ymax], origin="lower",
                            norm=LogNorm(vmin=np.min(hist[hist>0]), vmax=np.max(hist)),
                            cmap=cmap, interpolation='none')
        else:
            img = ax.pcolor(xedges, yedges, hist.transpose(),
                            norm=LogNorm(vmin=np.min(hist[hist>0]), vmax=np.max(hist)),
                            cmap=cmap)
            plt.axis([10**(int(np.log10(xmin))-1), 10**(int(np.log10(xmax))+1), 10**(int(np.log10(ymin))-1), 10**(int(np.log10(ymax))+1)])       
   
    cbar = fig.colorbar(img)
    if plot_metallicity:
        cbar.set_label("Z [Z$_{\odot}$]")
    elif plot_flux:
        cbar.set_label("X-ray flux")
    else:
        cbar.set_label("Mass [M$_{\odot}$]")
    
    if log:
        ax.axhline(y=np.log10(Tcut), lw=lw, ls='dashed', c='0.5', label="default f={:.3f}".format(DefCutMass/totMass))
        ax.axvline(x=np.log10(Rhocut), lw=lw, ls='dashed', c='0.5')
        ax.axhline(y=np.log10(Tvir_thresh*T200*tempnorm), lw=lw, ls='dotted', c='k', label=r"$"+r"{:d}".format(int(Tvir_thresh))+r" \times T_{200}$")
    else:
        ax.axhline(y=Tcut, lw=lw, ls='dashed', c='0.5', label="default"+"f={:.4f}\n$f_c$={:.4f} $f_{{SF}}$={:.4f}\n$f_{{c,SF}}$={:.4f}".format(DefCutMass/totMass, ColdCutMass/totMass, SFCutMass/totMass, ColdSFCutMass/totMass))
        ax.axvline(x=Rhocut, lw=lw, ls='dashed', c='0.5')
        #ax.axvline(x=Rhocut2, lw=lw, ls='dashed', c='r')
        ax.axhline(y=Tvir_thresh*T200*tempnorm, lw=lw, ls='dotted', c='k', label=r"$"+r"{:d}".format(int(Tvir_thresh))+r" \times T_{200}$")
    xmin, xmax = ax.get_xlim()
    ax.plot([xmin, xmax], [rasia(xmin), rasia(xmax)], lw=lw, ls='solid', label="no scaling f={:.3f}".format(rasiaMass/totMass))
    ax.plot([xmin, xmax], [rasia2(xmin), rasia2(xmax)], lw=lw, ls='solid', label="scaled f={:.3f}".format(rasia2Mass/totMass))
    ax.plot([xmin, xmax], [rasia3(xmin), rasia3(xmax)], lw=lw, ls='solid', label="N = {:.1e}".format(N_def)+" f={:.3f}".format(rasia3Mass/totMass))
    #ax.plot([xmin, xmax], [rasia3(xmin), rasia3(xmax)], lw=lw, ls='solid', label="Rasia+12 rescaled")#+"\nf={:.3f}".format(rasia3Mass/totMass))
    #plt.loglog(rho[cutInd], temp[cutInd], lw=lw, ls='none', marker=".")
    
    ax.legend(frameon=False)
    if log:
        ax.set_xlabel("log(Gas Density [g cm$^{-3}$])")
        if inKelvin:
            ax.set_ylabel("log(Temperature [K])")
            ax.set_ylim(3, 9)
        else:
            ax.set_ylabel("log(Temperature [keV])")
    else:
        ax.set_xlabel("Gas Density [g cm$^{-3}$]")
        if inKelvin:
            ax.set_ylabel("Temperature [K]")
            ax.set_ylim(1e3, 1e9)
        else:
            ax.set_ylabel("Temperature [keV]")
    if label is None:
        label = simname.replace("_", "\_")
    ax.set_title(label+"\nGrp "+str(groupnum)+" M$_{500}=$ "+"{:.1f}".format(sim.cat.group_m_crit500[groupnum]/1e3/sim.header.hubble)+r"$\times 10^{13} M_{\odot}$")
    
#    print "Saving..."
#    plt.savefig(outdir+simname+"/"+outname, bbox_inches='tight')
#    print "Saved to", outdir+simname+"/"+outname
