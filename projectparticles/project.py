## This starter script uses put_grid.py to project the mass (or temperature) of particles of a given type (or gas) onto the x-y plane and create a FITS image of the result

import sys, os
import numpy as np
import readsnap as rs
import snap
import pyfits as pf
import put_grid

### --------------- PARAMETERS --------------- ###
value_name = "TEMP" #name of value to project e.g. MASS or TEMP
basedir = "/data/emergence4/deboras/Elliptical_ZoomIns/L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff/"
snapnum = 25
outdir = "/home/nh444/data/project1/L-T_relations/L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_025/"
sim = snap.snapshot(basedir, snapnum, mpc_units=False)
group = 2
centre = sim.cat.group_pos[group]
fieldlength = 250 #sim.cat.group_r_crit500[group] # size of image in kpc
dist_proj = fieldlength#sim.box_sidelength ## TOTAL distance along (z) axis to project (symmetric about centre)
temp_thresh = -1 ## Excludes gas above temp_thresh * T200. Use <0 to not apply
main_halo_only = False
use_smoothing_length = False
if use_smoothing_length: quick=True
parttype=0 #particle type
grid_size = 512 #number of pixels along each dimension of image
proj_axis = "z"
nthreads = 8
outputfile = outdir+"grp"+str(group)+"_"+value_name+"_map_size_"+str(int(fieldlength))+"_"+str(grid_size)+"x"+str(grid_size)+".fit" #_T-lt-2.0Tvir

gamma = 5.0 / 3.0 # adiabatic index: 5/3 for monatomic gas
Xh = 0.76 #hydrogen mass fraction
HubbleParam = sim.header.hubble

##physical constants in cgs units
GRAVITY = 6.672e-8
BOLTZMANN = 1.3806e-16
PROTONMASS = 1.6726e-24
ELECTRONMASS = 9.1094e-28
ELECTRONCHARGE = 4.8032e-10 #in cgs ESU/Gaussian units (statC/Fr = 10^-1 * c Coulombs)
ELECTRONVOLT = 1.60218e-12 #in ergs (electron charge times 1V = 10^8 * c^-1 statV)

filename = sim.snapname

## Read in positions and smoothing lengths
pos = rs.read_block(filename, "POS ", parttype=parttype)

## Apply filter to include only particles within dist_proj
if proj_axis=="z": pind = 2
elif proj_axis=="y": pind = 1
elif proj_axis=="x": pind = 0
ind = [0,1,2]
ind.remove(pind)
z1 = centre[pind] - dist_proj/2
z2 = centre[pind] + dist_proj/2
mask = (pos[:,pind] > z1) & (pos[:,pind] < z2)
    
if use_smoothing_length:
    x = pos[:,ind[0]]
    y = pos[:,ind[1]]
    h = rs.read_block(filename, "HSML", parttype=parttype)
else:
    x = sim.rel_pos(pos[:,ind[0]], centre[ind[0]])
    y = sim.rel_pos(pos[:,ind[1]], centre[ind[1]])

if (sim.header.massarr[parttype] == 0.0):
    m = rs.read_block(filename, "MASS", parttype=parttype)
else:
    m = np.empty(sim.header.npart[parttype]); mass.fill(sim.header.massarr[parttype])

if (value_name == "MASS"):
    value = m
    ## Get virial temperature and exclude bubbles
    if temp_thresh > 0:
        sim.get_temp()
        Tvir = sim.cat.group_T_crit200[group]
        print "Tvir =", Tvir
        mask = (mask) & (sim.temp < temp_thresh*Tvir)
    if main_halo_only:
        sim.get_hostsubhalos()
        sim.get_parttype()
        mask = (mask) & (sim.hostsubhalos[sim.parttype==parttype] == sim.cat.group_firstsub[group])
elif (value_name == "TEMP"):
    assert (parttype == 0)
    sim.get_temp()
    value = sim.temp
    ## Get virial temperature and exclude bubbles
    if temp_thresh > 0:
        Tvir = sim.cat.group_T_crit200[group]
        print "Tvir =", Tvir
        mask = (mask) & (sim.temp < temp_thresh*Tvir)
    if main_halo_only:
        sim.get_hostsubhalos()
        assert parttype==0, "main halo option doesn't currently work with parttype != 0"
        mask = (mask) & (sim.hostsubhalos[:sim.header.nall[parttype]] == sim.cat.group_firstsub[group])
else:
    print "Oops. Value name", value_name, "not known"
    sys.exit()

## put_grid handles the projection, returning an array for the image
print "Running putgrid..."
if (value_name == "MASS"):
    if use_smoothing_length:
        pic = put_grid.put_grid(grid_size, xcentre, ycentre, fieldlength, x[mask] , y[mask], h[mask], value[mask], periodic=0, num_threads=nthreads)
    else:
        pic = put_grid.put_grid_cic2D(grid_size, 0, 0, fieldlength, x[mask], y[mask], value[mask], 0)
elif (value_name == "TEMP"): ## Mass-weighted temperature
    if use_smoothing_length:
        massmap = put_grid.put_grid(grid_size, xcentre, ycentre, fieldlength, x[mask] , y[mask], h[mask], m[mask], periodic=0, num_threads=nthreads)
        value_massmap = put_grid.put_grid(grid_size, xcentre, ycentre, fieldlength, x[mask] , y[mask], h[mask], m[mask] * value[mask], periodic=0, num_threads=nthreads)
        pic = np.divide(value_massmap, massmap)
    else:
        #massmap = put_grid.put_grid_cic2D(grid_size, 0, 0, fieldlength, x, y, m, 0)
        #value_massmap = put_grid.put_grid_cic2D(grid_size, 0, 0, fieldlength, x, y, m * value, 0)
        #pic = np.divide(np.where(massmap>0, value_massmap, 0), np.where(massmap>0, massmap, 0))
        pic = put_grid.put_grid_cic2D(grid_size, 0, 0, fieldlength, x[mask], y[mask], value[mask], 0)
else:
    print "Error: Value name", value_name, "not recognized."
    sys.exit()

if os.path.exists(outputfile):
    os.remove(outputfile)
    
## Create FITS file (with minimal header)
hdu = pf.PrimaryHDU(pic.transpose())
hdu.writeto(outputfile)

print "Done. Written to", outputfile
