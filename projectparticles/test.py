import pyfits as pf
from put_grid import put_grid

from pylab import *

x = array([0.4,0,0.5])
y = array([0.1,0.7,0.5])
h = array([0.05,0.03,0.02])
value = array([1,1,1])
grid_size = 512
centerx = 0.5
centery = 0.5
fieldlength = 1.0

#pic = put_grid(grid_size, centerx, centery, fieldlength, x , y, h, value)
pic = put_grid(grid_size, centerx, centery, fieldlength, x , y, h, value, periodic=1, num_threads=4)

hdu = pf.PrimaryHDU(pic.transpose())
hdu.writeto("pic_test.fit")
