import numpy as np
import ctypes as ct
import time
import os

def put_grid (grid_size, centerx, centery, fieldlength, x , y, h, value, periodic, num_threads=1):   
  start = time.time()
  
  libpath = os.path.dirname(os.path.abspath(__file__)) + "/"
  
  lp = np.ctypeslib.load_library('libpython_project.so', libpath)
  
  #define data types for arguments passed to 1minussquaredsquared
  if num_threads == 1:
    lp.put_1minusrsquaredsquared.argtypes = [np.ctypeslib.ndpointer(), ct.c_int, ct.c_int, np.ctypeslib.ndpointer(), np.ctypeslib.ndpointer(), np.ctypeslib.ndpointer(), np.ctypeslib.ndpointer(), ct.c_int]
    lp.put_1minusrsquaredsquared.restype = ct.c_int
  else:
    lp.put_grid_threads.argtypes = [ct.c_int, np.ctypeslib.ndpointer(), ct.c_int, ct.c_int, np.ctypeslib.ndpointer(), np.ctypeslib.ndpointer(), np.ctypeslib.ndpointer(), np.ctypeslib.ndpointer(), ct.c_int]
    lp.put_grid_threads.restype = ct.c_int

  ## Initialize image array with zeroes
  array = np.zeros((grid_size,grid_size), dtype=np.float32, order='C')
  
  num_part = x.__len__()
  assert y.__len__() == num_part
  assert h.__len__() == num_part
  assert value.__len__() == num_part

  ## Shift x and y particles coords so that x,y=(0,0) is at bottom left of image then divide by the size of a pixel to give the corresponding pixel number for that particle (i.e. the x coord in pixel units)
  ## Finally subtract 0.5 pixels such that the pixel grid is defined by the centre of each pixel
  ## e.g. a particle at x,y=(0,0) in code units is at xpix,ypix=(-0.5,-0.5) relative to the centre of the bottom-left pixel
  xpix = np.float32( ( x - (centerx - 0.5*fieldlength) ) / ( np.float32(fieldlength) / grid_size ) - 0.5 ) # x=0 corresponds to beginning of first pixel, x = fieldlength to end of last pixel 
  ypix = np.float32( ( y - (centery - 0.5*fieldlength) ) / ( np.float32(fieldlength) / grid_size ) - 0.5 )
  ## smoothing length h in units of pixels
  hpix = np.float32( h / ( np.float32(fieldlength) / grid_size ) )
  valuefloat = np.float32( value ) 

  assert xpix.flags['C_CONTIGUOUS']
  assert ypix.flags['C_CONTIGUOUS']
  assert hpix.flags['C_CONTIGUOUS']
  assert valuefloat.flags['C_CONTIGUOUS']

  assert periodic in [0,1]

  ##call C function to fill the array "pixels"
  if num_threads == 1:
    count = lp.put_1minusrsquaredsquared(array, grid_size, num_part, valuefloat, xpix, ypix, hpix, periodic)
  else:
    count = lp.put_grid_threads(num_threads, array, grid_size, num_part, valuefloat, xpix, ypix, hpix, periodic)

  fin = time.time()
  
  if num_threads == 1:
    print count,"particles put on grid in",fin-start,"seconds"
  else:
    print count,"particles put on grid in",fin-start,"seconds using",num_threads,"threads"

  return array
  
def put_grid_cic2D (grid_size, centerx, centery, fieldlength, x , y, value, periodic):
  ## Instead of using the smoothing kernel in the projection
  ## this function uses a basic projection of each point onto the corners
  ## of the grid square containing the point e.g. val*(1.0-dx)*(1.0-dy)
  start = time.time()

  assert periodic in [0,1]

  libpath = os.path.dirname(os.path.abspath(__file__)) + "/"

  lp = np.ctypeslib.load_library('libpython_project.so', libpath)

  lp.put_grid_cic2D.argtypes = [np.ctypeslib.ndpointer(), ct.c_int, ct.c_int, np.ctypeslib.ndpointer(), np.ctypeslib.ndpointer(), np.ctypeslib.ndpointer(), ct.c_int]
  lp.put_grid_cic2D.restype = ct.c_int
  
  array = np.zeros((grid_size,grid_size), dtype=np.float32, order='C')
  
  num_part = x.__len__()
  assert y.__len__() == num_part
  assert value.__len__() == num_part

  xpix = np.float32( ( x - (centerx - 0.5*fieldlength) ) / ( np.float32(fieldlength) / grid_size ) - 0.5 ) # x=0 corresponds to beginning of first pixel, x = fieldlenght to end of last pixel 
  ypix = np.float32( ( y - (centery - 0.5*fieldlength) ) / ( np.float32(fieldlength) / grid_size ) - 0.5 )
  valuefloat = np.float32( value )

  assert xpix.flags['C_CONTIGUOUS']
  assert ypix.flags['C_CONTIGUOUS']
  assert valuefloat.flags['C_CONTIGUOUS']
  
  count = lp.put_grid_cic2D(array, grid_size, num_part, valuefloat, xpix, ypix, periodic)

  fin = time.time()
  
  print count, "particles put on 2D grid in", fin-start, "seconds"
  
  return array
  
def put_grid_cic3D (grid_size, centerx, centery, centerz, fieldlength, x , y, z, value):
  start = time.time()

  libpath = os.path.dirname(os.path.abspath(__file__)) + "/"
  
  lp = np.ctypeslib.load_library('libpython_project.so', libpath)

  lp.put_grid_cic3D.argtypes = [np.ctypeslib.ndpointer(), ct.c_int, ct.c_int, np.ctypeslib.ndpointer(), np.ctypeslib.ndpointer(), np.ctypeslib.ndpointer(), np.ctypeslib.ndpointer()]
  lp.put_grid_cic3D.restype = ct.c_int
  
  array = np.zeros((grid_size,grid_size,grid_size), dtype=np.float32, order='C')
  
  num_part = x.__len__()
  assert y.__len__() == num_part
  assert z.__len__() == num_part
  assert value.__len__() == num_part

  xpix = np.float32( ( x - (centerx - 0.5*fieldlength) ) / ( np.float32(fieldlength) / grid_size ) - 0.5 ) # x=0 corresponds to beginning of first pixel, x = fieldlenght to end of last pixel 
  ypix = np.float32( ( y - (centery - 0.5*fieldlength) ) / ( np.float32(fieldlength) / grid_size ) - 0.5 )
  zpix = np.float32( ( z - (centerz - 0.5*fieldlength) ) / ( np.float32(fieldlength) / grid_size ) - 0.5 )
  valuefloat = np.float32( value )

  assert xpix.flags['C_CONTIGUOUS']
  assert ypix.flags['C_CONTIGUOUS']
  assert zpix.flags['C_CONTIGUOUS']
  assert valuefloat.flags['C_CONTIGUOUS']
  
  count = lp.put_grid_cic3D(array, grid_size, num_part, valuefloat, xpix, ypix, zpix)

  fin = time.time()
  
  print count, "particles put on 3D grid in", fin-start, "seconds"
  
  return array
