#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <pthread.h>
#include <assert.h>

#define min(a, b) (((a) < (b)) ? (a) : (b))
#define max(a, b) (((a) > (b)) ? (a) : (b)) 

int put_1minusrsquaredsquared(float *array, int grid_size, int num_part, float *valuearr, float *yposarr, float *zposarr, float *h_kernelarr, int periodic)
{
  // This function distributes values in valuearr onto a grid of size grid_size
  // for particles with positions given by yposarr and zposarr in grid units (pixels)
  // The grid should be defined by the centres of the pixels such that a particle
  // at the very bottom-left of the grid has pixel coords (-0.5,-0.5) - see e.g. put_grid.py
  // The distribution occurs over a kernel (1-u^2)^2 for u<1 where u = r/h,
  // where r is the distance of the particle from the pixel centre and h is the 
  // smoothing length given by h_kernelarr
  int count = 0;
  
  int i;
  for (i=0; i<num_part; ++i)// loop over all particles
  {
    //ypos and zpos are the two coordinates of each particle in units of pixels
    float value = valuearr[i];
    float ypos = yposarr[i];
    float zpos = zposarr[i];
    float h_kernel = h_kernelarr[i]; 
    
    if (h_kernel < 1) h_kernel = 1;  

    //Only consider particles whose hsml is within the image bounds
    if (ypos <= grid_size-1+h_kernel && ypos>=-h_kernel &&
        zpos <= grid_size-1+h_kernel && zpos>=-h_kernel)
    {
      // We only contribute to pixels whose centres are inside the circle
      // with radius h_kernel centred at (ypos, zpos)
      // iymin, izmin, iymax, izmax are the minimum and maximum y and z coords
      // that these pixels (centres) can have. We then loop over [iymin, iymax]
      // and [izmin, izmax] and calculate the contribution to each of these pixels
      // whose distance (to pixel centre) is less than h_kernel
        int iymin = (int) ceil(ypos - h_kernel); //ceil rounds up to nearest integer
        int izmin = (int) ceil(zpos - h_kernel); //can be negative
        int iymax = (int) floor(ypos + h_kernel);
        int izmax = (int) floor(zpos + h_kernel);

        double weight = 0, ysq, usq, hsq = h_kernel*h_kernel;
        int iy, iz, iiy, iiz;
        // First calculate the sum of (1-u^2)^2 for all the pixels
        // so that we can normalise later
        for (iy = iymin; iy <= iymax; ++iy)
        {
          ysq = (ypos-iy)*(ypos-iy);//squared distance
          for (iz = izmin; iz <= izmax; ++iz)
          {
            usq = (ysq + (zpos-iz)*(zpos-iz))/hsq;//sum of squared distance over h squared - a measure of the relative distance of the pixel
            if (usq<1.0) weight += (1.0-usq)*(1.0-usq); //pixels that are close enough to the particle that their distance-squared is less than h-squared get a contribution
          }
        }
        double norm = value/weight;
        // norm is the particle value divided by this total weight so that the
        // weighted sum of norm weighted by (1-u^2)^2 is normalised

        if (periodic==0)//if not periodic
	{
          //if not periodic, disregard pixels outside the image e.g. negative coords
          iymin = max(iymin,0);
          izmin = max(izmin,0);
          iymax = min(iymax,grid_size-1);
          izmax = min(izmax,grid_size-1);
        }

        for (iy = iymin; iy <= iymax; ++iy)
        {
          ysq = (ypos-iy)*(ypos-iy);

          iiy = iy;
          // if periodic=1 then iiy can be negative and needs reflecting to the opposite side of the grid
          while(iiy < 0) iiy += grid_size;
          while(iiy > grid_size-1) iiy -= grid_size;//similarly iiy can be beyond the grid

          for (iz = izmin; iz <= izmax; ++iz)
          {
            usq = (ysq + (zpos-iz)*(zpos-iz))/hsq;
            
            iiz = iz;
	    while(iiz < 0) iiz += grid_size;
	    while(iiz > grid_size-1) iiz -= grid_size;
            
            if (usq<1.0) array[iiz+iiy*grid_size] += (1.0-usq)*(1.0-usq)*norm;
            //the importance of each particle to a given pixel increases with decreasing distance
          }
        }

        ++count;
    }
  }
 
  return count;
}

/* not yet working */
int put_1minusrsquaredsquared_multigrid(float *array, int start_grid_size, int grid_size, int min_cells, int num_part, float *valuearr, float *xposarr, float *yposarr, float *h_kernelarr)
/* coordinates start at lower left corner of pixel 0,0 instead of its center, the upper right corner of the field corresponds to x=grid_size,y=grid_size */
{
  int count = 0;
  int check_count = 0;
  
  int grids_ok = 0;
  if (grid_size%start_grid_size == 0)
    if (((grid_size/start_grid_size) & (grid_size/start_grid_size-1)) == 0) grids_ok = 1; // checks if ratio is power of 2

  if (grids_ok == 0) return -1;
  
  float old_posfac = 0;
  int cur_grid_size;
  float *cur_array;
  for (cur_grid_size=start_grid_size; cur_grid_size <= grid_size; cur_grid_size *= 2)
  {
    float *old_arr = cur_array;
    
    cur_array = (float*) malloc(cur_grid_size*cur_grid_size*sizeof(float));
    float posfac = 1.0/grid_size*cur_grid_size;  
    
    int i;
    for (i=0; i<num_part; ++i)
    {
      float h_kernel = h_kernelarr[i]; 
      if (h_kernel*posfac < min_cells || h_kernel*old_posfac >= min_cells) continue; /* do only particles with hsml resolved well enough that have not been done before */
      
      h_kernel *= posfac; 
      float value = valuearr[i];
      float xpos = xposarr[i]*posfac;
      float ypos = yposarr[i]*posfac;
      
      if (h_kernel < 1) h_kernel = 1;  
      
      if (xpos-h_kernel <= cur_grid_size-0.5 && xpos+h_kernel >= 0.5 &&
          ypos-h_kernel <= cur_grid_size-0.5 && ypos+h_kernel >= 0.5)
      {
        int ixmin = (int) ceil(xpos - h_kernel + 0.5);
        int iymin = (int) ceil(ypos - h_kernel + 0.5);
        int ixmax = (int) floor(xpos + h_kernel - 0.5);
        int iymax = (int) floor(ypos + h_kernel - 0.5);

        double weight = 0, xsq, usq, hsq = h_kernel*h_kernel;
        int ix, iy;
        for (ix = ixmin; ix <= ixmax; ++ix)
        {
          xsq = (xpos-ix-0.5)*(xpos-ix-0.5);
          for (iy = iymin; iy <= iymax; ++iy)
          {
            usq = (xsq + (ypos-iy-0.5)*(ypos-iy-0.5))/hsq;
            if (usq<1.0) weight += (1.0-usq)*(1.0-usq);
          }
        }
        double norm = value/weight;

        ixmin = max(ixmin,0);
        iymin = max(iymin,0);
        ixmax = min(ixmax,grid_size-1);
        iymax = min(iymax,grid_size-1);

        for (ix = ixmin; ix <= ixmax; ++ix)
        {
          xsq = (xpos-ix-0.5)*(xpos-ix-0.5);
          for (iy = iymin; iy <= iymax; ++iy)
          {
            usq = (xsq + (ypos-iy-0.5)*(ypos-iy-0.5))/hsq;
            if (usq<1.0) array[iy+ix*grid_size] += (1.0-usq)*(1.0-usq)*norm;
          }
        }

        ++count;
      }
      
      ++check_count;
    }
    
    old_posfac = posfac;
  }
}

pthread_mutex_t mutex_curnum;
pthread_mutex_t mutex_results;

struct TaskArg
{
  int grid_size, num_part;
  float *allarr, *valuearr, *yposarr, *zposarr, *h_kernelarr;
  int *curnum, tid, junksize, *count;
  int periodic;
};

void *ProjectTaskCode (void *argument)
{
  struct TaskArg Args = *((struct TaskArg *) argument);
  
  int i;
  float *array = (float*) malloc(Args.grid_size*Args.grid_size*sizeof(float));
  for (i=0; i<Args.grid_size*Args.grid_size; ++i) array[i] = 0.0;
    
  int curstart;
  int curend;
  int count = 0;
  
  pthread_mutex_lock(&mutex_curnum);
    curstart = *(Args.curnum);
    curend = min(*(Args.curnum) + Args.junksize - 1, Args.num_part - 1);
    *(Args.curnum) = curend + 1;
    printf("Thread %i doing numbers %i to %i\n", Args.tid, curstart, curend);
  pthread_mutex_unlock(&mutex_curnum);
 
  while (curstart < Args.num_part)
  {
    if (curend>=curstart)
      count += put_1minusrsquaredsquared(array, Args.grid_size, curend-curstart+1, Args.valuearr+curstart, Args.yposarr+curstart, Args.zposarr+curstart, Args.h_kernelarr+curstart, Args.periodic);
   
    pthread_mutex_lock(&mutex_curnum);
      curstart = *(Args.curnum);
      curend = min(*(Args.curnum) + Args.junksize - 1, Args.num_part - 1);
      *(Args.curnum) = curend + 1;
      printf("Thread %i doing numbers %i to %i of %i\n", Args.tid, curstart, curend, Args.num_part);
    pthread_mutex_unlock(&mutex_curnum);
  }
 
  pthread_mutex_lock(&mutex_results);
    printf("Adding result of thread %i %i\n", Args.tid, count);
    for (i=0; i<Args.grid_size*Args.grid_size; ++i) Args.allarr[i] += array[i];
    Args.count[0] += count;
  pthread_mutex_unlock(&mutex_results);
    
  free(array);
  
  pthread_exit(0);
}

int put_grid_threads(int num_threads, float *array, int grid_size, int num_part, float *valuearr, float *yposarr, float *zposarr, float *h_kernelarr, int periodic)
{
  int count = 0;
  time_t start, end;
  time(&start);
  
  pthread_t threads[num_threads];
  struct TaskArg Args[num_threads];

  pthread_mutex_init(&mutex_curnum, NULL);
  pthread_mutex_init(&mutex_results, NULL);
 
  pthread_attr_t attr;
  pthread_attr_init(&attr);
  pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

  int curnum = 0;
  int i;
  for(i=0; i<num_threads; i++) 
  {
    Args[i].grid_size = grid_size;
    Args[i].num_part = num_part;
    Args[i].allarr = array; 
    Args[i].valuearr = valuearr;
    Args[i].yposarr = yposarr;
    Args[i].zposarr = zposarr;
    Args[i].h_kernelarr = h_kernelarr;
    Args[i].curnum = &curnum;
    Args[i].tid = i;
    Args[i].junksize = 500;
    Args[i].count = &count;
    Args[i].periodic = periodic;    

    pthread_create(&threads[i], &attr, ProjectTaskCode, (void *) &(Args[i]));
  }
  
  pthread_attr_destroy(&attr);

  void *status;
  for(i=0; i<num_threads; i++) pthread_join(threads[i], &status);
  
  pthread_mutex_destroy(&mutex_curnum);
  pthread_mutex_destroy(&mutex_results);
  
  time(&end);
  printf("parallel projection on grid took %f seconds using %i threads\n", difftime(end, start), num_threads);
 
  return count;
}

int put_grid_cic2D(float *array, int grid_size, int num_part, float *valuearr, float *xposarr, float *yposarr, int periodic)
{
  int count = 0;
  
  int i, xl, xr, yl, yr;
  float dx, dy, val;
  for (i=0; i<num_part; ++i)
  {
    xl = floor(xposarr[i]);
    xr = ceil(xposarr[i]);
    if (xr==xl) ++xr;
    
    yl = floor(yposarr[i]);
    yr = ceil(yposarr[i]);
    if (yr==yl) ++yr;
    
    dx = xposarr[i] - xl;
    dy = yposarr[i] - yl;
    
    if (periodic)
    {
      if (xl==-1) xl = grid_size-1; /* assumes periodic boundaries */
      if (yl==-1) yl = grid_size-1;
      if (xr==grid_size) xr = 0;
      if (yr==grid_size) yr = 0;
    
      assert(0<=xl && xl<grid_size); /* assumes all particels are in pixel */
      assert(0<=xr && xr<grid_size);
      assert(0<=yl && yl<grid_size);
      assert(0<=yr && yr<grid_size);
    }
    
    assert(0<=dx && dx<1.0);
    assert(0<=dy && dy<1.0);
    
    val = valuearr[i];
    if (0<=yl && yl<grid_size && 0<=xl && xl<grid_size) array[yl + grid_size*xl] += val*(1.0-dx)*(1.0-dy);
    if (0<=yl && yl<grid_size && 0<=xr && xr<grid_size) array[yl + grid_size*xr] += val*dx*(1.0-dy);
    if (0<=yr && yr<grid_size && 0<=xl && xl<grid_size) array[yr + grid_size*xl] += val*(1.0-dx)*dy;
    if (0<=yr && yr<grid_size && 0<=xr && xr<grid_size) array[yr + grid_size*xr] += val*dx*dy;
  }
  
  count = num_part;
  
  return count;
}

int put_grid_cic3D(float *array, int grid_size, int num_part, float *valuearr, float *xposarr, float *yposarr, float *zposarr)
{
  int count = 0;
  
  int i, xl, xr, yl, yr, zl, zr;
  float dx, dy, dz, val;
  for (i=0; i<num_part; ++i)
  {
    xl = floor(xposarr[i]);
    xr = ceil(xposarr[i]);
    if (xr==xl) ++xr;
    
    yl = floor(yposarr[i]);
    yr = ceil(yposarr[i]);
    if (yr==yl) ++yr;
    
    zl = floor(zposarr[i]);
    zr = ceil(zposarr[i]);
    if (zr==zl) ++zr;
    
    dx = xposarr[i] - xl;
    dy = yposarr[i] - yl;
    dz = zposarr[i] - zl;
    
    if (xl==-1) xl = grid_size-1; /* assumes periodic boundaries */
    if (yl==-1) yl = grid_size-1;
    if (zl==-1) zl = grid_size-1;
    if (xr==grid_size) xr = 0;
    if (yr==grid_size) yr = 0;
    if (zr==grid_size) zr = 0;
    
    assert(0<=xl && xl<grid_size); /* assumes all particels are in pixel */
    assert(0<=xr && xr<grid_size);
    assert(0<=yl && yl<grid_size);
    assert(0<=yr && yr<grid_size);
    assert(0<=zl && zl<grid_size);
    assert(0<=zr && zr<grid_size);
    
    assert(0<=dx && dx<1.0);
    assert(0<=dy && dy<1.0);
    assert(0<=dz && dz<1.0);
    
    val = valuearr[i];
    array[zl + grid_size*yl + grid_size*grid_size*xl] += val*(1.0-dx)*(1.0-dy)*(1.0-dz);
    array[zl + grid_size*yl + grid_size*grid_size*xr] += val*dx*(1.0-dy)*(1.0-dz);
    array[zl + grid_size*yr + grid_size*grid_size*xl] += val*(1.0-dx)*dy*(1.0-dz);
    array[zl + grid_size*yr + grid_size*grid_size*xr] += val*dx*dy*(1.0-dz);
    array[zr + grid_size*yl + grid_size*grid_size*xl] += val*(1.0-dx)*(1.0-dy)*dz;
    array[zr + grid_size*yl + grid_size*grid_size*xr] += val*dx*(1.0-dy)*dz;
    array[zr + grid_size*yr + grid_size*grid_size*xl] += val*(1.0-dx)*dy*dz;
    array[zr + grid_size*yr + grid_size*grid_size*xr] += val*dx*dy*dz;
  }
  
  count = num_part;
  
  return count;
}
