
def main():
    basedir = "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/"
    simnames = ["c96_box_Arepo", "c96_box_Arepo_RKLSF",
                "c96_FeedbackFix", "c96_FeedbackFix_OldGrav", "c96_FeedbackFix_OldGrav_2", "c96_FeedbackFix_OldGrav_2_MHGG",
                ]
    labels = ["old arepo", "old arepo RK + LSF",
               "new arepo", "+ old gravity solver", "+ RCUT + IND\_GRAV\_SOFT", "+ MH + GG",
               ]
               
    simnames = [#"c96_box_Arepo",
             #"c96_box_Arepo_RKLSF",
             "c96_box_Arepo_new",
             #"c96_box_Arepo_new_BHVel",
             #"c96_box_Arepo_new_ASMTH",
             "c96_box_Arepo_new_PM1024",
             #"c96_box_Arepo_new_Ngb",
            ]
    labels = [#"old arepo",
                  #"old arepo RK + LSF",
                  "old arepo modified",
                  #"old arepo modified with BHVel",
                  #"old arepo modified ASMTH 1.5 RCUT 6.0",
                  "old arepo modified PMGRID 1024",
                  "old arepo modified Ngb 256",
                  "new arepo",
                  "+ old gravity solver",
                  #"+ IND\_GRAV\_SOFT",
                  "+ RCUT + IND\_GRAV\_SOFT",
                  "+ MH + GG",
                  ]
               
    plot_global_timestep_distribution(basedir, simnames, labels)
    plot_timestep_vs_redshift(basedir, simnames, labels)
    #compare_timebins_at_redshift(basedir, simnames, labels, 2.5)
           
def compare_timebins_at_redshift(basedir, simnames, labels, redshift):
    """
    This will compare the number of cells and particles in each timebin at
    a given redshift.
    The redshift used is the one closest to the specified redshift whilst
    still being lower than it
    """
    import matplotlib.pyplot as plt
    
    fig, (ax1, ax2) = plt.subplots(ncols=2, figsize=(16,10), sharey=True)
    for ax in (ax1, ax2):
        ax.set_yscale('log')
        ax.set_xlabel('dt')

    col = ["#c7673e", "#78a44f", "#6b87c8","#934fce","#E6AB02","#be558f","#47af8d"]
    for i, simname in enumerate(simnames):
        lines = []
        readlines = False
        with open(basedir+simname+"/"+"timebins.txt", 'r') as f:
            for linenum, line in enumerate(f):
                if line[:4]=="Sync":
                    arr = line.split()
                    cur_z = float(arr[5].replace(",",""))
                    if cur_z < redshift:
                        if readlines:
                            break
                        readlines = True
                        z = cur_z
                if readlines:
                    lines.append(line)
                    
        print "z =", z
        Npart = []
        Ncells = []
        dt = []
        for line in lines:
            line = line.replace('X', '')
            line = line.strip()
            if line[:3] == 'bin':
                arr = line.split()
                Npart.append(int(arr[1]))
                Ncells.append(int(arr[2]))
                dt.append(float(arr[3]))
        ax1.step(dt, Npart, c=col[i], label=labels[i])
        ax2.step(dt, Ncells, c=col[i])
    ax1.legend(loc='upper left')
    ax1.set_title("Particles")
    ax2.set_title("Gas cells")
    ax1.set_ylabel("Number")
    fig.suptitle("z = {:.2f}".format(z))
    plt.subplots_adjust(wspace=0)
    print dt

def plot_global_timestep_distribution(basedir, simnames, labels):
    import matplotlib.pyplot as plt
    import numpy as np

    filename = "timebins.txt"
    col = ["#c7673e", "#78a44f", "#6b87c8","#934fce","#E6AB02","#be558f","#47af8d"]
    TimeStepArray = []
    N = []
    minval = np.inf
    maxval = 0.0
    
    for i, simname in enumerate(simnames):
        TimeStep = []
        with open(basedir+simname+"/"+filename, 'r') as f:
            for line in f:
                if line[:4]=="Sync":
                    arr = line.split()
                    TimeStep.append(float(arr[7].replace(",","")))
            TimeStep = np.array(TimeStep)
            if np.min(TimeStep[TimeStep>0]) < minval:
                minval = np.min(TimeStep[TimeStep>0])
            if np.max(TimeStep) > maxval:
                maxval = np.max(TimeStep)
            TimeStepArray.append(TimeStep)
            N.append(len(TimeStep))
    
    fig = plt.figure(figsize=(14,10))
    bins = np.logspace(np.log10(minval), np.log10(maxval), num=30)
    for i, simname in enumerate(simnames):
        #plt.hist(TimeStepArray[:][i], bins=bins, label=labels[i], fc='none', ec=col[i], lw=2)
        hist, bin_edges = np.histogram(TimeStepArray[:][i], bins=bins)
        plt.step(bin_edges[1:], hist, lw=2, c=col[i], label=labels[i])
    plt.legend(loc='best')
    plt.xscale('log')
    plt.xlabel("Time Step ($a$)")
    plt.ylabel("number")
    fig.savefig("/data/curie4/nh444/project1/timestep_dist.pdf")
    
def plot_timestep_vs_redshift(basedir, simnames, labels):
    import matplotlib.pyplot as plt
    import numpy as np
    from astropy.cosmology import Planck15 ## Planck 2015 cosmology
    from scipy.stats import binned_statistic

    col = ["#c7673e", "#78a44f", "#6b87c8","#934fce","#E6AB02","#be558f","#47af8d"]
    bins = np.logspace(0, 1, num=25)
    plt.figure(figsize=(14,10))
    
    for i, simname in enumerate(simnames):
        z = [] # redshift of timestep
        with open(basedir+simname+"/"+"timebins.txt", 'r') as f:
            for line in f:
                if line[:4]=="Sync":
                    arr = line.split()
                    z.append(float(arr[5].replace(",","")))
                    
        z = np.asarray(z)
        tl = Planck15.lookback_time(z)   ## lookback time in Gyr for each redshift
        tdiff = (tl[:-1] - tl[1:])*1000.0 ## Myr
        
        plt.plot(z[1:]+1.0, tdiff, c=col[i], alpha=0.5, label=labels[i])
        hist, bin_edges, n = binned_statistic(z[1:]+1.0, tdiff, statistic='median', bins=bins)
        x = np.sqrt(bin_edges[1:]*bin_edges[:-1])
        plt.plot(x, hist, c='k', lw=2, label=labels[i]+" median")
    
    
    
    plt.legend(loc='best')
    plt.xscale('log')
    plt.yscale('log')
    plt.xlim(10, 1)
    plt.xlabel("1 + z")
    plt.ylabel("timestep [Myr]")

if __name__ == "__main__":
    main()