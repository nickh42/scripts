
from nfw import NFW
import numpy as np
import astropy.cosmology

data = np.genfromtxt("/home/nh444/data/project1/L-T_relations/obs/gastaldello_masses.txt")
outfile = "/home/nh444/data/project1/L-T_relations/obs/gastaldello_M500.txt"

cosmo = astropy.cosmology.FlatLambdaCDM(70, 0.3, Tcmb0=0)
astropy.cosmology.default_cosmology.set(cosmo)

N = len(data[:,0])
M500 = np.zeros((N,3))
M500_gas = np.zeros((N,3))
M500_stars = np.zeros((N,3))
f_gas  = np.zeros((N,3))
f_stars  = np.zeros((N,3))
Delta = np.zeros(N)
M500_delta = np.zeros((N,3))
f_stars_delta = np.zeros((N,3))
for i in range(0, N):
    z = data[i,1]
    delta = data[i,2]
    Delta[i] = delta
    c_delta = data[i,6]
    M_delta = data[i,12]*1e13
    r_delta = data[i,9]

    nfw = NFW(M_delta, c_delta, z, overdensity=delta, overdensity_type="critical")

    m500 = nfw.mass_Delta(500)
    #print M_delta, ">>>", m500.value
    M500[i,0] = m500.value
    M500[i,1] = abs(data[i,13]/data[i,12])*m500.value
    M500[i,2] = abs(data[i,14]/data[i,12])*m500.value

    r200 = nfw.radius_Delta(200)
    r500 = nfw.radius_Delta(500)
    #print "r_"+str(int(delta))+" / r_200 =", (r_delta/r200.value/1000.0)
    print "r_"+str(int(delta))+" / r_500 =", (r_delta/r500.value/1000.0)

    M_gas = data[i,15]*1e12
    nfw_gas = NFW(M_gas, c_delta, z, overdensity=delta, overdensity_type="critical")
    m500_gas = nfw_gas.mass_Delta(500)
    #print M_gas, ">>>", m500_gas.value
    M500_gas[i,0] = m500_gas.value
    M500_gas[i,1] = abs(data[i,16]/data[i,15])*m500_gas.value
    M500_gas[i,2] = abs(data[i,17]/data[i,15])*m500_gas.value

    f_gas[i,0] = M500_gas[i,0] / M500[i,0]
    f_gas[i,1] = f_gas[i,0]*(((M500[i,1]/M500[i,0])**2 + (M500_gas[i,1]/M500_gas[i,0])**2)**0.5)
    f_gas[i,2] = f_gas[i,0]*(((M500[i,2]/M500[i,0])**2 + (M500_gas[i,2]/M500_gas[i,0])**2)**0.5)

    M_stars = data[i,24]*1e10
    if M_stars>0:
        
        nfw_stars= NFW(M_stars, c_delta, z, overdensity=delta, overdensity_type="critical")
        m500_stars = nfw_stars.mass_Delta(500)
        #print M_stars, ">>>", m500_stars.value
        M500_stars[i,0] = m500_stars.value
        M500_stars[i,1] = abs(data[i,25]/data[i,24])*m500_stars.value
        M500_stars[i,2] = abs(data[i,26]/data[i,24])*m500_stars.value

        f_stars[i,0] = M500_stars[i,0] / M500[i,0]
        f_stars[i,1] = f_stars[i,0]*(((M500[i,1]/M500[i,0])**2 + (M500_stars[i,1]/M500_stars[i,0])**2)**0.5)
        f_stars[i,2] = f_stars[i,0]*(((M500[i,2]/M500[i,0])**2 + (M500_stars[i,2]/M500_stars[i,0])**2)**0.5)

        ## The following calculates the stellar fraction at the orginal overdensity
        ## delta but of course this gives the same result as above
        #f_stars_delta[i,0] = M_stars/M_delta
        #f_stars_delta[i,1] = f_stars_delta[i,0]*(((data[i,13]/data[i,12])**2 + (data[i,25]/data[i,24])**2)**0.5)
        #f_stars_delta[i,2] = f_stars_delta[i,0]*(((data[i,14]/data[i,12])**2 + (data[i,26]/data[i,24])**2)**0.5)


header = "m500 e1 e2 m500_gas e1 e2 f_gas_500 e1 e2 m500_stars e1 e2 f_stars_500 e1 e2"
np.savetxt(outfile, np.c_[M500, M500_gas, f_gas, M500_stars, f_stars], header=header, fmt="%.2e")
