
from NFW.nfw import NFW
import numpy as np
import astropy.cosmology

dat = np.genfromtxt("/data/vault/nh444/ObsData/L-M/COSMOS_L-M_Leauthaud2010.csv", delimiter=",")
z = dat[:,8]
c_delta = dat[:,14]
M_delta = dat[:,5] * 1e13
M_delta_e1 = dat[:,7] * 1e13
M_delta_e2 = dat[:,6] * 1e13
delta = 200

cosmo = astropy.cosmology.FlatLambdaCDM(70, 0.3, Tcmb0=0)
astropy.cosmology.default_cosmology.set(cosmo)

N = len(dat[:,8])
M500 = np.zeros((N,3))
for i in range(0, N):

    nfw = NFW(M_delta[i], c_delta[i], z[i], overdensity=delta, overdensity_type="critical")

    m200 = nfw.mass_Delta(200)
    m500 = nfw.mass_Delta(500)
    print "M_{:d} =".format(delta), M_delta[i]
    print "M_200 =", m200.value
    print "M_500 =", m500.value, "\n"
    M500[i,0] = m500.value
    M500[i,1] = M_delta_e1[i]/M_delta[i]*m500.value
    M500[i,2] = M_delta_e2[i]/M_delta[i]*m500.value

    r200 = nfw.radius_Delta(200)
    r500 = nfw.radius_Delta(500)
    
for i in range(0, N):
    print ",".join(M500[i,:].astype('S20'))


print M500

#header = "m500 e1 e2 m500_gas e1 e2 f_gas_500 e1 e2 m500_stars e1 e2 f_stars_500 e1 e2"
#np.savetxt(outfile, np.c_[M500, M500_gas, f_gas, M500_stars, f_stars], header=header, fmt="%.2e")
