
from NFW.nfw import NFW
#import astropy.cosmology
#
#cosmo = astropy.cosmology.FlatLambdaCDM(70, 0.3, Tcmb0=0)
#astropy.cosmology.default_cosmology.set(cosmo)

c = 5.0  # concentration
z = 0.0  # redshift
nfw = NFW(1e15, c, z, overdensity=200, overdensity_type="critical")

m200 = nfw.mass_Delta(200)
m500 = nfw.mass_Delta(500)
print "M_200 =", m200.value
print "M_500 =", m500.value
print "M200 / M500 =", m200.value / m500.value
print "M500 / M200 =", m500.value / m200.value
