import h5py
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.colors import LogNorm

outdir = "/data/ERCblackholes1/nh444/SZ_AGN/maps/"
indir = "/data/ERCblackholes1/nh444/SZ_AGN/maps/"

group = 0
name_suffix = "box_z02_L500_ps1"
name_suffix = "c384_z02_L200_ps1"
name_suffix = "c448_AGN_z6_L200_ps1"
name_suffix = "c448_AGN_new_z6_L200_ps1"
name_suffix="c448_AGN_new_SmallSeedHaloMass_z6_L200_ps1"
name_suffix="Millennium_AGN_z6_L200_ps1"
name_suffix="Millennium_AGN_new_z6_L200_ps1"
name_suffix="Millennium_AGN_new_SmallSeedHaloMass_z6_L200_ps1"
#name_suffix = "c448_noBH_z6_L200_ps1"
#name_suffix = "c585_AGN_z6_L200_ps1"
#name_suffix = "c585_noBH_z6_L200_ps1"
maps = ["SZ", #"SFR", #"xray"
        ]
title = None ## default None
arcsec = True
textsize="medium" 


with h5py.File(indir+"map_"+name_suffix+"_proj.hdf5", 'r') as f:
    h = f.attrs['h']
    z = f.attrs['z']
    sidelength = f.attrs['sidelength_ckpch']
    kpc_arcsec = f.attrs['pkpc_arcsec'] ## physical kpc per arcsec
    M500 = f.attrs['M500_e10Msunh'] * 1e10 / h
    R500 = f.attrs['R500_ckpch']
    
    for curmap in maps:
        maparr = f[curmap].value ## important to take values so that indexing works properly
        
        vmax = 9e-5#np.max(maparr)
        vmin = 3e-10# np.min(maparr[maparr>0.0])
        print "sum, vmin, vmax =", np.sum(maparr), vmin, vmax
        
        circrad = [1.0] ## list of radii (r500) of circles drawn at centre, set empty list to disable
        
        print "M500 = {:.2e}".format(M500), "at z = {:.3f}".format(z)
        Mexp = int(np.log10(M500))
        
        fig, ax = plt.subplots()
        
        mycmap = plt.get_cmap('magma')
        mycmap.set_bad(color='k')
        
        if arcsec:
            unitfac = 1.0/h/(1+z) / kpc_arcsec ##convert ckpc/h to arcsec
        else:
            unitfac = 1.0/1000.0/h/(1+z) ## convert ckpc/h to pMpc
        L = 0.5*sidelength * unitfac
        
        img = ax.imshow(np.transpose(maparr),cmap=mycmap,
                        extent=[-L, L, -L, L],
                        origin="lower", norm=LogNorm(vmin=vmin, vmax=vmax))
        if arcsec:
            ax.set_xlabel("x [arcsec]",size=textsize)
            ax.set_ylabel("y [arcsec]",size=textsize)
            ## Add circle for Planck beam  FWHM
            rad = 7.27*60.0/2.0 #143 GHz beam with FWHM 7.27 arcmin
            if rad < 0.25*L:
                circle = plt.Circle((-L+1.5*rad, -L+1.5*rad), rad, fc='none', ec='white', ls='solid')
                ax.add_artist(circle)
                xmin, xmax = ax.get_xlim()
                ax.annotate(r"Planck 143 GHz FWHM", xy=((2.5*rad)/(xmax-xmin)+0.02, (0.5*rad)/(xmax-xmin)+0.02), xycoords="axes fraction", color='white', weight='bold', fontsize='x-small')
        else:
            ax.set_xlabel("x [Mpc]",size=textsize)
            ax.set_ylabel("y [Mpc]",size=textsize)
            
        ax.annotate(r"M500 = ${:.1f} \times 10^{{{:d}}} M_{{\odot}}$".format(M500/10**Mexp, Mexp)+"\n"+r"$z={:.1f}$".format(z),
                    xy=(0.08, 0.83), xycoords="axes fraction", color='white', weight='bold', fontsize='small')
        
        if len(circrad) > 0:
            for crad in circrad:
                circle = plt.Circle((0, 0), crad * R500 * unitfac, fc='none', ec='white', ls='dotted')
                ax.add_artist(circle)
        
        
        cbar = plt.colorbar(img,fraction=0.046, pad=0.04)
        if curmap is "SZ":
            cbar.set_label("dimensionless Compton y parameter", size=textsize)
        elif curmap is "SFR":
            cbar.set_label("Integrated SFR [$M_{\odot} \mathrm{yr}^{-1}$]", size=textsize)
        elif curmap is "xray":
            cbar.set_label("X-ray flux [erg s$^{-1}$ cm$^{-2}$]", size=textsize)
        
        #for curline in ax.spines.values(): # change width of axes
        #  curline.set_linewidth(0.5)
        
        for label in ax.get_xticklabels() + ax.get_yticklabels():   # set size of axes tick numbers
          label.set_fontsize(textsize)
        
        for label in cbar.ax.get_xticklabels() + cbar.ax.get_yticklabels():   # set size of axes tick numbers
          label.set_fontsize(textsize)
        
        if title is not None:
            ax.set_title(title, fontsize=textsize)
        plt.tight_layout()
        outname = "map_"+curmap+"_"+name_suffix+"_proj.pdf"
        fig.savefig(outdir+outname, bbox_inches='tight')
        print "Plot saved to", outdir+outname

