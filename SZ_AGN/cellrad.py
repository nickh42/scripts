#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 12 17:21:01 2018
Plot gas cell radius (average, max, min) as a function of radial distance
@author: nh444
"""

def main():
    basedir = "/home/nh444/project1/zoom_ins/"
    dirnames = [#"LDRIntRadioEffBHVel/c512_box_Arepo_new",
                "MDRInt/c585_MDRInt",
                "MDRInt/c512_MDRInt",
                "MDRInt/c448_MDRInt",
                "MDRInt/c384_MDRInt",
                "MDRInt/c320_MDRInt",
                "MDRInt/c256_MDRInt",
                ]
    mpc_units = True
    group = 0
    
    #==============================================================================
    # basedir = "/data/curie4/nh444/project1/boxes/"
    # dirnames = ["L40_512_MDRIntRadioEff"]
    # mpc_units = False
    # group = 0
    #==============================================================================
    
    snapnum = 24#5#18#24
    snapnums = [5]#, 18, 24]
    
    show_masses(basedir, dirnames, snapnums, group=group, mpc_units=mpc_units)
    #cellrad(basedir, dirnames, snapnum, group=group, mpc_units=mpc_units)
    
def cellrad(basedir, dirnames, snapnum, group=0, mpc_units=True):
    import snap
    import numpy as np
    import matplotlib.pyplot as plt

    fig, ax = plt.subplots()
    arcsec = True
    pkpc = False
    
    hist = False
    if hist:
        fig2, ax2 = plt.subplots()
        ax2.set_xlabel("cell radius [arcsec]")
    
    for dirname in dirnames:
        sim = snap.snapshot(basedir+dirname, snapnum, mpc_units=mpc_units)
        h = sim.header.hubble
        z = sim.header.redshift
        ckpc_arcsec = sim.cm.arcsec_to_comov(ang=1, z=z)*1000.0 ## ckpc/h per arcsec
        print "ckpc_arcsec =", ckpc_arcsec
        Rhocut = 0.000880623 / (1+z)**3 ## comoving SF density thresh in code units
        
        centre = sim.cat.group_pos[group]
        M500 = sim.cat.group_m_crit500[group]
        print dirname, "M500 = {:.2e} Msun".format(M500 * 1e10 / h)
        print "max M500 = {:.2e} Msun".format(np.max(sim.cat.group_m_crit500) * 1e10 / h)
        R500 = sim.cat.group_r_crit500[group]
        
        print "Loading sim data..."
        sim.load_gaspos()
        sim.load_gasmass()
        sim.load_gasrho()
        
        r = sim.nearest_dist_3D(sim.gaspos, centre)
        vol = sim.gasmass / sim.gasrho ## (ckpc/h)**3
        cellrad = (vol / (4.0*np.pi/3.0))**(1.0/3.0) ## ckpc/h
        
        if hist:
            ind = np.where(r < R500)[0]
            ind = ind[np.where(sim.gasrho[ind] < Rhocut)[0]] ## only non-SF gas
            #print np.max(cellrad[ind]), "ckpc/h", np.max(cellrad[ind])/ckpc_arcsec, "arcsec"
            ax2.hist(cellrad[ind]/ckpc_arcsec, bins=100)
            ax2.axvline(np.mean(cellrad[ind]/ckpc_arcsec), c='k')
            ax2.axvline(np.median(cellrad[ind]/ckpc_arcsec), c='k', ls='dashed')
            ax2.axvline(np.percentile(cellrad[ind]/ckpc_arcsec, 5.0), c='k', ls='dashed')
            ax2.set_title("$z = {:.1f}$".format(z))
        if arcsec:
            x = np.linspace(1, 400, num=100) ## arcsec
            radii = x * ckpc_arcsec ## ckpc/h
        elif pkpc:
            x = np.linspace(10, 2000, num=100) ## pkpc
            radii = x * h * (1+z) ## ckpc/h
        else:
            x = np.linspace(0.1, 2.1, num=101) ## R500
            radii = x * R500 ## ckpc/h
        meanrad = []
        medrad = []
        maxrad = []
        minrad = []
        for rad in radii:
            ind = np.where(r < rad)[0]
            ind = ind[np.where(sim.gasrho[ind] < Rhocut)[0]] ## only non-SF gas
            #print rad, np.max(cellrad[ind])/ckpc_arcsec
            if pkpc:
                meanrad.append(np.mean(cellrad[ind])/h/(1+z))
                medrad.append(np.median(cellrad[ind])/h/(1+z))
                maxrad.append(np.max(cellrad[ind])/h/(1+z))
                minrad.append(np.min(cellrad[ind])/h/(1+z))
            else:
                meanrad.append(np.mean(cellrad[ind])/ckpc_arcsec)
                medrad.append(np.median(cellrad[ind])/ckpc_arcsec)
                maxrad.append(np.max(cellrad[ind])/ckpc_arcsec)
                minrad.append(np.min(cellrad[ind])/ckpc_arcsec)
            
        p = ax.plot(x, meanrad, label=dirname.replace("_","\_"))
        ax.plot(x, medrad, c=p[0].get_color(), alpha=0.5)
        ax.plot(x, minrad, c=p[0].get_color(), ls='dashed')
        ax.plot(x, maxrad, c=p[0].get_color(), ls='dashed')
        if arcsec:
            ax.axvline(R500 / ckpc_arcsec, c=p[0].get_color(), ls='dashdot')
    
    if arcsec: ax.set_xlabel("R [arcsec]")
    elif pkpc: ax.set_xlabel("R [pkpc]")
    else: ax.set_xlabel("R$_{500}$")
    if pkpc: ax.set_ylabel("Avg. cell radius [pkpc]")
    else:
        ax.set_ylabel("Avg. cell radius [arcsec]")
        ax.axhline(5, c='0.5')
    ax.legend(loc='best')
    ax.set_title("$z = {:.1f}$".format(z))
    
def show_masses(basedir, dirnames, snapnums, group=0, mpc_units=True):
    import snap
    
    for snapnum in snapnums:
        for dirname in dirnames:
            try:
                sim = snap.snapshot(basedir+dirname, snapnum, mpc_units=mpc_units)
                sim.get_hostsubhalos()
                sim.get_sub_bh_props()
                print dirname, "z={:.1f}".format(sim.header.redshift), "M200m={:.2e}".format(sim.cat.group_m_mean200[group]*1e10/sim.header.hubble), "M_BH={:.2e}/{:.2e}".format(sim.sub_bhmass1[sim.cat.group_firstsub[0]]*1e10/sim.header.hubble, max(sim.bhmass)*1e10/sim.header.hubble)
            except IOError:
                pass


if __name__ == "__main__":
    main()