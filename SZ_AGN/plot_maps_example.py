import matplotlib.pyplot as plt
import numpy as np
from matplotlib.colors import LogNorm
import h5py

indir = "/data/ERCblackholes1/nh444/SZ_AGN/maps/"
outdir = "/data/ERCblackholes1/nh444/SZ_AGN/maps/"

suffix = "c384_z02_L200_ps1"
maps = ["SZ", "SFR", "xray"]

arcsec = True ## Plot in arcsecs, otherwise Mpc

with h5py.File(indir+"map_"+suffix+"_proj.hdf5", 'r') as f:
    h = f.attrs['h']
    z = f.attrs['z']
    sidelength = f.attrs['sidelength_ckpch']
    kpc_arcsec = f.attrs['pkpc_arcsec'] ## physical kpc per arcsec
    M500 = f.attrs['M500_e10Msunh'] * 1e10 / h
    
    for curmap in maps:
        maparr = f[curmap].value
        
        fig, ax = plt.subplots()
        vmax = np.max(maparr)
        vmin = np.min(maparr[maparr>0.0])
        cmap = plt.get_cmap('magma')
        cmap.set_bad(color='k')
        
        if arcsec:
            unitfac = 1.0/h/(1+z) / kpc_arcsec ## convert ckpc/h to arcsec
        else:
            unitfac = 1.0/1000.0/h/(1+z) ## convert ckpc/h to pMpc
        L = 0.5*sidelength * unitfac
        
        img = ax.imshow(np.transpose(maparr),cmap=cmap,
                        extent=[-L, L, -L, L],
                        origin="lower", norm=LogNorm(vmin=vmin, vmax=vmax))
        if arcsec:
            ax.set_xlabel("x [arcsec]")
            ax.set_ylabel("y [arcsec]")
        else:
            ax.set_xlabel("x [Mpc]")
            ax.set_ylabel("y [Mpc]")
                          
        cbar = plt.colorbar(img,fraction=0.046, pad=0.04)
        if curmap is "SZ":
            cbar.set_label("dimensionless Compton y parameter")
        elif curmap is "SFR":
            cbar.set_label("Integrated SFR [$M_{\odot} \mathrm{yr}^{-1}$]")
        elif curmap is "xray":
            cbar.set_label("X-ray flux [erg s$^{-1}$ cm$^{-2}$]")
        
        outname = "map_"+curmap+"_"+suffix+"_proj.pdf" ## name of output pdf
        fig.savefig(outdir+outname, bbox_inches='tight')
        print "Plot saved to", outdir+outname

