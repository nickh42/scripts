"""
Loads a saved numpy array and saves as an image with a logarithmic colour scale
Each element of the numpy array describes one pixel.
"""

import matplotlib.pyplot as plt
import numpy as np
from matplotlib.colors import LogNorm

indir = "/data/ERCblackholes1/nh444/SZ_AGN/maps/tests/"
outdir = indir

name_suffix = "box_z02_L2000_ps1_10r500"
name_suffix = "c384_z02_L200_ps1_lowres"

filename = "map_SZ_"+name_suffix+"_proj"
dat = np.load(indir+filename+".npz")
maparr = dat["maparr"]
vmax = np.max(maparr)
vmin = np.min(maparr[maparr>0.0])
print "sum, vmin, vmax =", np.sum(maparr), vmin, vmax
norm = LogNorm(vmin=vmin, vmax=vmax)#plt.Normalize(vmin=vmin, vmax=vmax)

cmap = plt.get_cmap('magma')
cmap.set_bad(color='k')

img = cmap(norm(maparr))
plt.imsave(outdir+filename+".png", img, format="png")
print "Image saved to", outdir+filename+".png"

