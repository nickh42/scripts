import snap

outdir = "/data/ERCblackholes1/nh444/SZ_AGN/maps/"

basedir = "/home/nh444/data/project1/zoom_ins/MDRInt/"
simnames = [#"c128_MDRInt",
            #"c192_MDRInt",
            #"c256_MDRInt",
            #"c320_MDRInt",
            #"c384_MDRInt",
            #"c448_MDRInt",
            #"c448_MDRInt_noprescrit",
            #"c448_MDRInt_noprescrit_SmallHaloSeed",
            #"Millennium_MDRInt",
            #"Millennium_MDRInt_noprescrit",
            "Millennium_MDRInt_noprescrit_LowSeedHaloMass",
            #"c585_MDRInt",
            ]
#basedir = "/data/ERCblackholes1/nh444/zooms/noBH/"
#simnames = ["c448_noBH",
#            #"c585_noBH",
#            ]
mpc_units = True
highres_only = False
group = 0

#==============================================================================
# basedir = "/data/curie4/nh444/project1/boxes/"
# simnames = ["L40_512_MDRIntRadioEff"]
# mpc_units = False
# highres_only = False
# group = 0
#==============================================================================

h_scale = 0.679
ngb = 32#2.5**3.0 ## factor that multiplies the cell volume and is cube-rooted for the smoothing length
## the gravitational potential of a cell is taken to be 2.5 times its cube-rooted volume
sidelength = 200.0 ## image sidelength in arcsec
pixelsize = 1.0 ## arcsec
snapnum = 5 ## 24 (z=0.2), 18 (z=1.4), 5 or 9 (z=6)
name_suffix="box_z02_L200_ps1"
#name_suffix="c256_z02_L200_ps1"
#name_suffix="c384_z02_L200_ps1"
#name_suffix="c384_z14_L200_ps1"
#name_suffix="c448_AGN_z6_L200_ps1"
#name_suffix="c448_AGN_new_z6_L200_ps1"
#name_suffix="c448_AGN_new_SmallSeedHaloMass_z6_L200_ps1"
name_suffix="Millennium_AGN_z6_L200_ps1"
name_suffix="Millennium_AGN_new_z6_L200_ps1"
name_suffix="Millennium_AGN_new_SmallSeedHaloMass_z6_L200_ps1"
#name_suffix="c585_AGN_z6_L200_ps1"
save_BHprops = True
#name_suffix="c448_noBH_z6_L200_ps1"
#name_suffix="c585_noBH_z6_L200_ps1"
#save_BHprops = False
maps = ["SZ", "SFR"]#, "xray"]

for simname in simnames:
    sim = snap.snapshot(basedir+simname, snapnum, mpc_units=mpc_units)
    z = sim.header.redshift
    R500 = sim.cat.group_r_crit500[group]
    zthick = 40000.0 #320000.0 ## integration distance in ckpc/h, 0 is whole box
    
    ckpc_arcsec = sim.cm.arcsec_to_comov(ang=1, z=z)*1000.0 ## ckpc/h per arcsec
    print "ckpc_arcsec =", ckpc_arcsec
    sidelengthkpc = sidelength * ckpc_arcsec
    npix = int(sidelength / pixelsize)
    
    print "sidelength =", sidelength, "[arcsec]"
    print "pixelsize =", pixelsize, "[arcsec]"
    print "npix =", npix
    print "R500 =", R500, "[ckpc/h]"
    print "sidelengthkpc =", sidelengthkpc, "[ckpc/h] =", sidelengthkpc/R500, "[R500]"
    
    sim.get_gas_projection(sidelength=sidelengthkpc, group=group, zthick=zthick, npix=npix,
                           use_vol=True, filebase="map_", save_maps=maps,
                            phase_filter=False, highres_only=highres_only, ngb=ngb,
                            name_suffix=name_suffix, nthreads=8,
                            hdf5=True, save_BHprops=save_BHprops, angular=True, outdir=outdir)
