#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 28 20:52:14 2017

@author: nh444
"""
import matplotlib.pyplot as plt
import numpy as np

## L [Mpc/h], N_DM^1/3, epsilon(z=0) [kpc/h], z(MaxPhys)
Ill = [106.5*0.704, 1820, 0.71*0.704, 1.0]
TNG100 = [72.1, 1820, 0.74*0.677, 1.0]
TNG300 = [204.86, 2500, 1.48*0.677, 1.0]
small = [40.0, 512, 0.3375, 3.0]
fid = [40.0, 512, 2.39344, 5.0]
med = [40.0, 512, 2.39344, 1.0]
smallish = [40.0, 512, 0.95737662, 4.0]
sims = (Ill, TNG100, TNG300, small, fid, med, smallish)
labels = ["Illustris-1", "TNG100", "TNG300", "$\epsilon$(z=0)=0.3375 $z_{\epsilon}=3$", "$\epsilon$(z=0)=2.4 $z_{\epsilon}=5$", "$\epsilon$(z=0)=2.4 $z_{\epsilon}=1$", "$\epsilon$(z=0)=0.96 $z_{\epsilon}=4$"]
#sims = (small, fid, med)
#labels = ["$\epsilon$(z=0)=0.3375 $z_{\epsilon}=3$", "$\epsilon$(z=0)=2.4 $z_{\epsilon}=5$", "$\epsilon$(z=0)=2.4 $z_{\epsilon}=1$"]
sims = (fid, smallish, Ill)
labels = ["$\epsilon$(z=0)=2.4 $z_{\epsilon}=5$", "$\epsilon$(z=0)=0.96 $z_{\epsilon}=4$", "Illustris-1"]
meandist = [1e3*sim[0]/sim[1] for sim in sims]
print meandist
scaled = True ## scale to meandist

col = ["#c7673e","#78a44f",'0.5',"#6b87c8","#934fce","#e6ab02","#be558f","#47af8d"]
fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(14,7))

z = np.linspace(12, 0, num=1200)
for i, sim in enumerate(sims):
    if scaled:
        fac = 1.0 / meandist[i] * 100.0
    else:
        fac = 1.0
    ax1.plot(z[z>=sim[3]], sim[2] * (1+sim[3]) /(1.0+z[z>=sim[3]]) * fac, lw=2, c=col[i], label=labels[i])
    ax1.plot(z[z<=sim[3]], np.full_like(z[z<=sim[3]], sim[2] * fac), lw=2, c=col[i])
    ax2.plot(z[z>=sim[3]], np.full_like(z[z>=sim[3]], sim[2] * (1+sim[3]) * fac), lw=2, c=col[i], label=labels[i])
    ax2.plot(z[z<=sim[3]], sim[2] * (1.0+z[z<=sim[3]]) * fac, lw=2, c=col[i])
if scaled:
    ax2.axhline(y=2.0, c='0.5', ls='dashed', label="2\%")

if scaled:
    ax1.set_ylabel("$\epsilon_{phys}$ as a fraction of $L/N_{DM}^{1/3}$ [\%]")
    ax2.set_ylabel("$\epsilon_{com}$ as a fraction of $L/N_{DM}^{1/3}$ [\%]")
else:
    ax1.set_ylabel("$\epsilon_{phys}$ [kpc/h]")
    ax2.set_ylabel("$\epsilon_{com}$ [kpc/h]")
ax1.set_xlabel("z")
ax2.set_xlabel("z")
ax1.legend(loc='upper right', borderaxespad=0.5)
ax2.legend(loc='best', borderaxespad=2)
plt.tight_layout()
fig.savefig("/data/curie4/nh444/project1/softening_length_vs_redshift.pdf")