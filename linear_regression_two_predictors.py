#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 28 19:56:41 2019
This script can be used to fit a linear regression model with two predictor
variables (x,y) and one outcome variable (z) using emcee. The model is:
    Z_n = alpha + (beta_x * x_n_ + (beta_y * y_n) + epsilon
where epsilon is a Gaussian noise term.
@author: nh444
"""
from __future__ import print_function

import numpy as np


def lnlike(p, x, y, z):
    """Log-likelihood for linear regression with two predictors and scatter."""

    # The parameters are stored in a vector 'p'. Unpack them.
    alpha, beta_x, beta_y, eps = p
    # The model that gives z from x and y is
    model = alpha + beta_x * x + beta_y * y
    # The likelihood is the sum of Gaussian distributions for each point.
    # I'm ignoring the log(2*pi) term
    return -0.5 * np.sum((z - model)**2 / eps**2 + np.log(eps**2))


def lnprior_uniform(p, bounds=None):
    """Uniform log-prior on normalisation, slopes and scatter.

    Arguments:
        p: Vector of parameters: (alpha, beta_x, beta_y, eps).
        bounds: Sequence of (min, max) pairs for each parameter.
                None is used to specify no bound.
    """
    # Unpack parameters from vector.
    alpha, beta_x, beta_y, eps = p
    if bounds is None:
        # If no bounds given, use infinite priors except for eps.
        alpha_bounds = (-np.inf, np.inf)
        beta_x_bounds = (-np.inf, np.inf)
        beta_y_bounds = (-np.inf, np.inf)
        eps_bounds = (0.0, np.inf)
    else:
        alpha_bounds, beta_x_bounds, beta_y_bounds, eps_bounds = bounds
    # Uniform priors.
    if (alpha_bounds[0] < alpha < alpha_bounds[1] and
            beta_x_bounds[0] < beta_x < beta_x_bounds[1] and
            beta_y_bounds[0] < beta_y < beta_y_bounds[1] and
            eps_bounds[0] < eps < eps_bounds[1]):
        # Return an (arbitrary) uniform value.
        return 0.0
    else:
        # Return a number for the log-prior as close to zero as possible.
        return -np.inf


def lnprob_uniform(p, x, y, z, bounds=None):
    """Full log-probability function. i.e. prior * likelihood"""
    lp = lnprior_uniform(p, bounds=bounds)
    if not np.isfinite(lp):
        # If prior is zero, return zero.
        return -np.inf
    return lp + lnlike(p, x, y, z)


def linreg_two_predictors(x, y, z, initial_guess, bounds=None, plot=True):
    """ Perform linear regression with two predictors (x, y) using emcee.

    This function fits a model of the form z = alpha + beta_x * x + beta_y * y
    including some Gaussian scatter with standard deviation 'eps'.
    Arguments:
        x, y: Predictors (independent variables).
        z: Dependent variable.
        initial_guess: (sequence) Initial guess for the parameters in the
                       order (alpha, beta_x, beta_y, eps)
        bounds: Sequence of (min, max) pairs for each parameter.
                None is used to specify no bound.
    """
    import scipy.optimize as op
    import matplotlib.pyplot as plt
    import emcee
    import corner

    # The maximum likelihood result can be obtained by minimising the negative
    # log-likelihood using e.g. scipy.optimize.minimize
    nll = lambda *args: -lnlike(*args)  # Returns negative log-likelihood
    result = op.minimize(nll, initial_guess, args=(x, y, z), method="TNC", bounds=bounds)
    print("Scipy minimize:")
    print("success:", result['success'])
    print("message:", result['message'])
    print("niterations:", result['nit'])
    print("result:", result['x'], "\n")
    alpha_ml, beta_x_ml, beta_y_ml, eps_ml = result['x']
# =============================================================================
#     # The local minimum found my scipy minimize may not be the global minimum.
#     # The basin-hopping algorithm estimates the location of the global minimum
#     # by running scipy minimize from many random starting points and returning
#     # the lowest minimum found.
#     minimizer_kwargs = dict(method="TNC", args=(x, y, z), bounds=bounds)
#     result = op.basinhopping(nll, initial_guess, niter=500,
#                              minimizer_kwargs=minimizer_kwargs)
#     print("Scipy minimize w/ basin-hopping:")
#     print("message:", result['message'])
#     print("niterations:", result['nit'])
#     print("result:", result['x'], "\n")
#     alpha_ml, beta_x_ml, beta_y_ml, eps_ml = result['x']
# =============================================================================

    if plot:
        # Plot the datapoints.
        fig, (ax_x, ax_y) = plt.subplots(2, figsize=(7, 7))
        ax_x.set_xlabel('X')
        ax_y.set_xlabel('Y')
        ax_x.set_ylabel(r'Z - alpha - beta\_y*y')
        ax_y.set_ylabel(r'Z - alpha - beta\_x*x')
        ax_x.scatter(x, z - alpha_ml - beta_y_ml * y,
                     marker='o')
        ax_y.scatter(y, z - alpha_ml - beta_x_ml * x,
                     marker='o')
        # Plot the maximum-likelihood models.
        xlim = np.array([x.min(), x.max()])
        ax_x.plot(xlim, xlim * beta_x_ml, lw=3, label="Maximum likelihood")
        ylim = np.array([y.min(), y.max()])
        ax_y.plot(ylim, ylim * beta_y_ml, lw=3)

    # To estimate the uncertainties on the maximum-likelihood parameters we
    # will use MCMC via emcee
    # First define the number of parameters (4) and number of MCMC walkers.
    ndim = 4
    nwalkers = 100
    # Initialise each walker in a random position within a small Gaussian
    # "ball" centred on the maximum likelihood result.
    # np.random.randn(ndim) gives an array of length ndim of random numbers
    # drawn from the standard normal distribution (mean 0 variance 1).
    pos0 = [result['x'] + 1e-4*np.random.randn(ndim) for i in range(nwalkers)]
    # Set up the emcee sampler
    lnprob = lambda *args: lnprob_uniform(*args, bounds=bounds)
    sampler = emcee.EnsembleSampler(nwalkers, ndim, lnprob,
                                    args=(x, y, z))
    # Run the MCMC for 500 steps
    pos, prob, state = sampler.run_mcmc(pos0, 500)
    # The sampler now has a chains attribute with shape (Nwalker, 500, Ndim)
    if plot:
        # Plot each walker as a function of the number of steps in the chain.
        fig, axes = plt.subplots(4, figsize=(7, 12))
        ax_alpha, ax_beta_x, ax_beta_y, ax_eps = axes
        axlabels = ("alpha", r"beta\_x", r"beta\_y", "eps")
        for i, ax in enumerate((ax_alpha, ax_beta_x, ax_beta_y, ax_eps)):
            ax.set_xlabel("step number")
            ax.set_ylabel(axlabels[i])
            ax.plot(sampler.chain[:, :, i].T, '-', c='0.5', alpha=0.2)

    # Remove the burn-in by excluding the first ~100 steps and concatenate all
    # the chains together to create an array of shape (450*nwalkers, ndim)
    samples = sampler.chain[:, 100:, :].reshape((-1, ndim))

    if plot:
        # Make a corner plot.
        corner.corner(samples, labels=axlabels)

        # Plot the models corresponding to the median parameters.
        alpha_med, beta_x_med, beta_y_med, eps_med = np.median(samples, axis=0)
        ax_x.scatter(x, z - alpha_med - beta_y_med * y,
                     marker='o')
        ax_y.scatter(y, z - alpha_med - beta_x_med * x,
                     marker='o')
        ax_x.plot(xlim, xlim * beta_x_med, lw=3, label="Median")
        ax_y.plot(ylim, ylim * beta_y_med, lw=3)

        ax_x.legend()

    return samples
