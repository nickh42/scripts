#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 30 14:50:33 2017

@author: nh444
"""
import numpy as np
import matplotlib.pyplot as plt

SofteningComovingType0 = 2.8 * 0.016875 * 1000.0 # kpc
MinimumComovingHydroSoftening = 2.8 * 0.0028125 * 1000.0 #kpc
AdaptiveHydroSofteningSpacing = 1.2
NSOFTTYPES_HYDRO = 64

soft_table = MinimumComovingHydroSoftening * np.power(np.full(NSOFTTYPES_HYDRO, AdaptiveHydroSofteningSpacing), range(0, NSOFTTYPES_HYDRO))
print "soft_table =", soft_table
print "SofteningComovingType0 =", SofteningComovingType0

GasSoftFactor = 2.5

vol = np.logspace(-3, 8, num=1000) # kpc^3
r = (3.0 * vol / 4.0 / np.pi)**(1.0/3.0) # kpc
print np.min(r)
print np.max(r)

soft_old = GasSoftFactor * r
soft_old[soft_old < SofteningComovingType0] = SofteningComovingType0

soft_new = GasSoftFactor * r
soft_new[soft_new <= MinimumComovingHydroSoftening] = MinimumComovingHydroSoftening
k = 0.5 + np.log10(soft_new / MinimumComovingHydroSoftening) / np.log10(AdaptiveHydroSofteningSpacing)
k = k.astype(int)
k[k >= NSOFTTYPES_HYDRO] = NSOFTTYPES_HYDRO -1
soft_new = soft_table[k]

hist = np.loadtxt("/home/nh444/Documents/arepo_code_diff/radius_hist_box.txt")

fig, ax1 = plt.subplots(figsize=(8,5))
ax2 = ax1.twinx()

ax1.plot(r, soft_old, lw=2, label="Old arepo")
ax1.plot(r, soft_new, lw=2, label="New arepo")
ax2.step(hist[:,0], hist[:,1], label="Number of cells", c='0.5', lw=2)
ax2.set_ylabel("Number of cells in 40 Mpc/h box")
ax1.legend(frameon=False, loc='upper left')
ax2.legend(frameon=False, loc='lower right')
ax1.set_xscale('log')
ax1.set_yscale('log')
ax1.set_ylabel("Softening Length (kpc/h)")
ax1.set_xlabel("Cell radius [kpc/h]")
fig.savefig("/home/nh444/Documents/arepo_code_diff/softening_vs_cell_radius.pdf", bbox_inches='tight')

