#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 16 17:36:50 2017
Rough cluster galaxy stellar mass functions - Compares the subhalo stellar mass distribution of different FoF groups
@author: nh444
"""

# General settings
h_scale = 0.7
masstab = False
basedir = "/data/emergence3/deboras/Elliptical_ZoomIns/"
simdirs = [#"L40_512_NoDutyRadioWeak_HalfThermal_FixEta/",
           #"L40_512_NoDutyRadioInt_HalfThermal_FixEta/",
           #"L40_512_LowDutyRadioWeak_HalfThermal_FixEta/",
           "L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff/",
           ]
labels = ["Ref",
          "Stronger radio",
          "Quasar duty cycle",
          "Best"]     
outdir = "/data/curie4/nh444/project1/stars/" ## where stellar masses are saved
outfilename = "stellar_mass_distribution_Low_Softening_c128.pdf"
snapnums = [25]

zoombasedir = "/home/nh444/data/project1/zoom_ins/"
zoomdirs = [#"","","",
            "LDRIntRadioEffBHVel/"] ## corresponding to each simdir
zooms = ["c96", "c128_fixed", "c160", "c192", "c256_fixed", "c320", "c384_fixed", "c448"]
zooms = ["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new",
         #"c256_box_Arepo_new", "c384_box_Arepo_new", "c448_box_Arepo_new",
         ]
zooms = ["c96_box_Arepo_new", "c96_box_Arepo_new_LowSoft"]#, "c128_box_Arepo_new", "c128_box_Arepo_new_LowSoft"]
zooms = ["c128_box_Arepo_new", "c128_box_Arepo_new_LowSoft"]

def main():
    mstar_dist_comparison(basedir, simdirs, zoombasedir, zoomdirs, zooms, 25, M500min=1.3e13, M500max=1.3e13, outfilename=outfilename, outdir=outdir, labels=labels)

def mstar_dist_comparison(basedir, simdirs, zoombasedir, zoomdirs, zooms, snapnum, M500min=0.0, M500max=None, bound_only=True, outdir = "/data/curie4/nh444/project1/stars/", outfilename="stellar_mass_dist.pdf", labels=None, h_scale=0.7):
    """
    Plots the number of subhalos as a function of their mass for groups in the given mass range (for simdirs) or group 0 (for zooms)
    If bound_only is True, only uses subhaloes bound to the group, otherwise it uses all subhaloes within r500
    """
    import snap
    import numpy as np
    import matplotlib.pyplot as plt
    
    if M500max is None:
        M500max = np.inf
        
    plt.figure(figsize=(16,10))
    plt.ylabel("Count")
    plt.xlabel("M$_{\mathrm{star}}$ ($M_{\odot}$)")
    plt.xscale('log')
    
    bins = np.logspace(6, 12, num=30)
    ls = ['solid', 'dashed']
    #col = ["#c7673e","#78a44f","#6b87c8","#934fce","#e6ab02","#be558f","#47af8d"]

    for simnum, simdir in enumerate(simdirs):
        sim = snap.snapshot(basedir+simdir, snapnum, mpc_units=False)
        groups = np.where((sim.cat.group_m_crit500*1e10/h_scale > M500min) & (sim.cat.group_m_crit500*1e10/h_scale < M500max))[0]
        print "Using groups", groups
        for group in groups:
            if bound_only:
                subnums = np.arange(sim.cat.group_firstsub[group], sim.cat.group_firstsub[group] + sim.cat.group_nsubs[group])
            else:
                R = sim.cat.group_r_crit500[group]
                subnums = np.where(sim.nearest_dist_3D(sim.cat.sub_pos, sim.cat.group_pos[group]) <= R)[0]
            mstar = sim.cat.sub_masstab[subnums,4]
            mstar = mstar[mstar>0.0] * 1e10 / h_scale
            hist, bin_edges = np.histogram(mstar, bins=bins)
            plt.step(bin_edges[:-1], hist, c='0.5', label=r"{:.1e}".format(sim.cat.group_m_crit500[group]*1e10/h_scale))
        if zoomdirs[simnum] is not "":
            for i, zoom in enumerate(zooms):
                sim = snap.snapshot(zoombasedir+zoomdirs[simnum]+zoom, snapnum, mpc_units=True)
                if bound_only:
                    subnums = np.arange(sim.cat.group_firstsub[0], sim.cat.group_firstsub[0] + sim.cat.group_nsubs[0])
                else:
                    R = sim.cat.group_r_crit500[0]
                    subnums = np.where(sim.nearest_dist_3D(sim.cat.sub_pos, sim.cat.group_pos[0]) <= R)[0]
                mstar = sim.cat.sub_masstab[subnums,4]
                mstar = mstar[mstar>0.0] * 1e10 / h_scale
                hist, bin_edges = np.histogram(mstar, bins=bins)
                p = plt.step(bin_edges[:-1], hist, where='post', linestyle=ls[i%len(ls)], lw=2, label=zoom.replace("_","\_")+r" (M$_{500}$ "+r"{:.2e})".format(sim.cat.group_m_crit500[0]*1e10/h_scale))
                plt.plot((bin_edges[:-1]*bin_edges[1:])**0.5, hist, linestyle='none', marker='o', c=p[0].get_color(), mfc=p[0].get_color(), mec='none')
                #plt.plot((bin_edges[:-1]*bin_edges[1:])**0.5, hist, linestyle=ls[i%len(ls)], lw=2, label=zoom.replace("_","\_")+r" {:.1e}".format(sim.cat.group_m_crit500[0]*1e10/h_scale))
    plt.legend(ncol=1)
    ymin, ymax = plt.gca().get_ylim()
    plt.ylim(ymin,ymax+1)
    plt.savefig(outdir+outfilename)
    print "Plot saved to", outdir+outfilename
    
if __name__ == "__main__":
    main()