#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 16 17:36:50 2017
Rough cluster galaxy stellar mass functions - Compares the subhalo stellar mass distribution of different FoF groups
@author: nh444
"""

# General settings
h_scale = 0.679
masstab = False

def main():
    basedir = "/data/emergence3/deboras/Elliptical_ZoomIns/"
    simdirs = [#"L40_512_NoDutyRadioWeak_HalfThermal_FixEta/",
               #"L40_512_NoDutyRadioInt_HalfThermal_FixEta/",
               #"L40_512_LowDutyRadioWeak_HalfThermal_FixEta/",
               "L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff/",
               ]
    labels = [#"Ref",
              #"Stronger radio",
              #"Quasar duty cycle",
              "Best"]     
    outdir = "/data/curie4/nh444/project1/stars/" ## where stellar masses are saved
    outfilename = "Mstar_vs_MHalo_comparison.pdf"
    
#    basedir = "/data/curie4/nh444/project1/boxes/"
#    simdirs = ["L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff/",
#               "L40_512_LDRIntRadioEff/",
#               "L40_512_LDRIntRadioEff_SoftSmall/",
#               ]
#    labels = [#"small softenings",
#              "larger softenings"]
#    labels = ["$\epsilon$(z=0)=0.3375 $z_{\epsilon}=3$", "$\epsilon$(z=0)=2.4 $z_{\epsilon}=5$", "$\epsilon$(z=0)=2.4 $z_{\epsilon}=1$"]
#    outdir = "/data/curie4/nh444/project1/stars/" ## where stellar masses are saved
#    outfilename = "Mstar_vs_MHalo_comparison_30kpc.pdf"
#    outfilename = "Mstar_vs_MHalo_comparison_diff_rad_TotHaloMass.pdf"
#    outfilename = "Mstar_vs_MHalo_comparison_softening.pdf"

    basedir = "/data/curie4/nh444/project1/boxes/"
    simdirs = [#"L40_512_LDRIntRadioEff_noBH",
                #"L40_512_NoDutyRadioWeak",
                #"L40_512_NoDutyRadioInt",
                #"L40_512_DutyRadioWeak",
                "L40_512_MDRIntRadioEff",
               ]
    labels = [#"No BHs",
              #"Weak radio",
              #"Stronger radio",
              #"Quasar duty cycle",
              "Fiducial",
              ]
    outfilename = "Mstar_vs_MHalo_comparison.pdf"
    #outfilename = "Mstar_vs_MHalo_comparison_models_totalmass.pdf"
    
    zoombasedir = "/home/nh444/data/project1/zoom_ins/"
#    zoomdirs = [#"","","",#"",
#                "LDRIntRadioEffBHVel/"] ## corresponding to each simdir
#    zooms = ["c96_box_Arepo_new", "c96_box_Arepo_new_LowSoft", "c128_box_Arepo_new", "c128_box_Arepo_new_LowSoft", "c160_box_Arepo_new", "c160_box_Arepo_new_LowSoft"]#, "c320_box_Arepo_new", "c320_box_Arepo_new_LowSoft", "c384_box_Arepo_new", "c384_box_Arepo_new_LowSoft"]
#    zoomlabels = ["c96 High soft", "c96 Low soft", "c128 High soft", "c128 Low soft", "c160 High soft", "c160 Low soft", "c320 High soft", "c320 Low soft", "c384 High soft", "c384 Low soft"]
    zoomdirs = [#"","","","",
                "MDRInt/"] ## corresponding to each simdir
    zooms = ["c128_MDRInt", "c192_MDRInt", "c256_MDRInt",
             "c320_MDRInt", "c384_MDRInt",
             "c448_MDRInt", "c512_MDRInt"]
    zoomlabels = [zoom.replace("_","\_") for zoom in zooms]
    snapnums = [25, 20, 15, 10]#8,6,5,4
    
    mstar_mHalo_comparison(basedir, simdirs, 25,
                           compare_apertures=True, aperture=True, DM_only=False,
                           total_mass=False, fractional=True, for_paper=False, err_env=False, scat_ind=[],
                           delta=200, Mhmin=0.0, Mhmax=None, outfilename=outfilename,
                           outdir=outdir, labels=labels, zoomlabels=zoomlabels)
    #mstar_mHalo_redshift(basedir, simdirs[0], snapnums, Mhmin=0.0, Mhmax=None, plot_obs=True,
    #                     outdir=outdir, outfilename="Mstar_vs_MHalo_z.pdf", h_scale=0.7)
    
#==============================================================================
#     simdir = "L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff/"
#     zoomdir = "LDRIntRadioEffBHVel/"
#     #zooms = ["c96_box_Arepo_new", "c128_box_Arepo_new"]
#     #zooms2 = ["c96_box_Arepo_new_LowSoft", "c128_box_Arepo_new_LowSoft"]
#     zooms = ["c320_box_Arepo_new"]
#     zooms2 = ["c320_box_Arepo_new_LowSoft"]
#     titles = ["High softenings", "Low softenings"]
#     snapnums = [25]#, 20, 15, 10]#8,6,5,4
#     outfilename="Mstar_vs_MHalo_zooms_z0_c320.pdf"
#     mstar_mHalo_zooms_comparison(basedir, simdir, zoombasedir, zoomdir, zooms, snapnums,
#                          zooms2=zooms2, DM_only=True, highres_only=True, total_mass=False, Mhmin=1e5, Mhmax=None, plot_obs=True,
#                          outdir=outdir, outfilename=outfilename,
#                          zoomlabels=zoomlabels, titles=titles, h_scale=0.7)
#==============================================================================

def mstar_mHalo_comparison(basedir, simdirs, snapnum, zoombasedir=None, zoomdirs=None, zooms=None,
                         DM_only=True, total_mass=False, fractional=True,
                         compare_apertures=False, aperture=False, err_env=True, scat_ind=[],
                         delta=200, Mhmin=0.0, Mhmax=None,
                         outdir = "/data/curie4/nh444/project1/stars/", outfilename="Mstar_vs_MHalo_comparison.pdf",
                         labels=None, zoomlabels=None, h_scale=0.7, for_paper=False, verbose=True):
    """
    Plots stellar mass fraction vs halo mass for central galaxies, similar to Genel+ 2014 fig. 6 and Vogelsberger+ 2014 fig 12 
    If DM_only, we calculate the DM halo mass Mh,DM, otherwise we use the total mass Mh
    If total_mass, calculate total stellar mass (central + satellites within Rh), otherwise just central mass
    """
    import snap
    import numpy as np
    import matplotlib.pyplot as plt
    from scipy.stats import binned_statistic
    from matplotlib.patches import Polygon
    
    if Mhmax is None:
        Mhmax = np.inf
    if zoombasedir is None or zoomdirs is None or zooms is None:
        zoomdirs = [""] * len(simdirs) ## don't plot any zooms
        
    if for_paper:
        plt.figure(figsize=(7,7))
    else:
        plt.figure(figsize=(10,10))
    if DM_only:
        plt.xlabel("M$_{\mathrm{DM},"+str(int(delta))+"}$ ($M_{\odot}$)")
    else:
        plt.xlabel("M$_{\mathrm{tot},"+str(int(delta))+"}$ ($M_{\odot}$)")
    if total_mass:
        if fractional: plt.ylabel("(M$_{\star,\mathrm{tot}}$ / M$_{"+str(int(delta))+"}$)/$f_b$")
        else: plt.ylabel("M$_{\star,\mathrm{tot}}$")
    else:
        if fractional: plt.ylabel("(M$_{\star,\mathrm{cen}}$ / M$_{"+str(int(delta))+"}$)/$f_b$")
        else: plt.ylabel("M$_{\star,\mathrm{cen}}$")
    plt.xscale('log')
    plt.xlim(6e9, 1.5e14)
    #plt.ylim(0, 0.35)
    if not fractional:
        plt.yscale('log')
    
    if for_paper:
        from palettable.cartocolors.sequential import Teal_4 as pallette
        col=pallette.hex_colors[-3:]+['#E7298A']
    else:
        col = ["#c7673e","#78a44f","#6b87c8","#934fce","#e6ab02","#be558f","#47af8d"]
    bins = np.logspace(9, 15, num=30)
    xsim = (bins[1:]*bins[:-1])**0.5
    x = np.logspace(9, 15, num=300)

    if fractional and int(delta)==200:
        ## Abundance Matching results
        #if DM_only:
        ## Moster+ 2013 use DM only masses
        s = snap.snapshot(basedir+simdirs[0], snapnum, mpc_units=False, header_only=True)
        z = s.header.redshift
        mfrac = Moster2013_SHM(x, z, h_scale) ## scaled by f_b
        plt.plot(x, mfrac, c='limegreen', alpha=0.5, lw=3, dashes=(3,6), label="Moster+ 2013 [z={:.1f}]".format(z), dash_capstyle='round')
    #==============================================================================
    #     mfrac, mfrac_low, mfrac_high, zused = Moster2017_SHM(x, z, h_scale) ## scaled by f_b
    #     plt.plot(x, mfrac, c='red', alpha=0.5, lw=3, dashes=(3,6), label="EMERGE [$z={:.1f}$] (Moster+ 2017)".format(zused), dash_capstyle='round')
    #     if err_env:
    #         patch = np.ma.column_stack((np.concatenate((x,x[::-1])), np.ma.concatenate((mfrac_high,mfrac_low[::-1]))))
    #         plt.gca().axes.add_patch(Polygon(patch[patch[:,1]>0], alpha=0.1, color='red', ec='none'))
    #==============================================================================
    
        if z < 0.1:
            ## Note: I think Kravtsov plot total DM+baryonic masses, not just DM, but I'm gonna plot them anyway since it's only a 5% difference
            Krav = np.genfromtxt("/data/vault/nh444/ObsData/mass_fracs/Kravtsov2014_SHM.csv", delimiter=",") 
            if total_mass:
                plt.plot(10**Krav[:,3] * 0.7 / h_scale, Krav[:,4], c='darkblue', alpha=0.5, lw=3, dashes=(3,6), label="Kravtsov+ 2014", dash_capstyle='round')## scaled by f_b
            else:
                plt.plot(10**Krav[:,0] * 0.7 / h_scale, Krav[:,1], c='darkblue', alpha=0.5, lw=3, dashes=(3,6), label="Kravtsov+ 2014", dash_capstyle='round')## scaled by f_b
            #Ill = np.genfromtxt("/data/vault/nh444/ObsData/SHM/Illustris.csv", delimiter=",")
            #plt.plot(10**Ill[:,0] * 0.6774 / h_scale, Ill[:,1], c='0.5', ls='solid', lw=3, label="Illustris")
            TNG = np.genfromtxt("/data/vault/nh444/ObsData/SHM/IllustrisTNG.csv", delimiter=",")
            plt.plot(10**TNG[:,0] * 0.6774 / h_scale, TNG[:,1], c='orange', alpha=0.5, dashes=(3,6), lw=3, label="IllustrisTNG300 [$<30$ kpc]", dash_capstyle='round')
            EMERGE = np.genfromtxt("/data/vault/nh444/ObsData/SHM/EMERGE_2017.csv", delimiter=",")
            plt.plot(10**EMERGE[:,0] * 0.6774 / h_scale, EMERGE[:,1], c='k', alpha=0.5, dashes=(3,6), lw=3, label="EMERGE (Moster+ 2017)", dash_capstyle='round')

    for simnum, simdir in enumerate(simdirs):
        if verbose:
            print "Loading snapshot", snapnum
        sim = snap.snapshot(basedir+simdir, snapnum, mpc_units=False, extra_sub_props=True)
        
        fb = sim.omega_b / sim.header.omega_m
        
        Mh, mstar_total, mstar_inrad, mstar_inhalfrad, mstar_inmaxrad, mstar_inSPrad, mstar_inAper = get_SHM(sim, delta, DM_only=DM_only, total_mass=total_mass, Mhmin=Mhmin, Mhmax=Mhmax, verbose=verbose)
        if fractional:
            fac = 1.0 / Mh / fb
        else:
            fac = 1.0
        if compare_apertures:
            masses = (mstar_total, mstar_inAper, mstar_inrad, mstar_inSPrad) # mstar_inhalfrad, mstar_inmaxrad
            aperls = ['solid', 'solid', 'solid', 'solid']#, 'dashed', 'dotted', 'dashdot'] ## linestyles for when compare_apertures is True
            aperlabels = [" Total bound", " spherical 30 pkpc", " $2 r_{\star, 0.5}$", " Photometric radius"] #" (half-mass radius)", " (V$_{\mathrm{max}}$ radius)"
            apercol = ["#c7673e","#78a44f","#6b87c8","#934fce","#e6ab02","#be558f","#47af8d"]
            for midx, mstar in enumerate(masses):
                y = mstar * fac
                med, bin_edges, n = binned_statistic(Mh, y, statistic='median', bins=bins)
                plt.plot(xsim, med, c=apercol[midx], lw=3, ls=aperls[midx], label=aperlabels[midx])
        else:
            if aperture:
                y = mstar_inAper * fac
            else:
                y = mstar_total * fac
            if simnum in scat_ind:
                plt.plot(Mh, y, c=col[simnum], marker='o', ms=4, alpha=0.5, mec='none', ls='none')
            
            med, bin_edges, n = binned_statistic(Mh, y, statistic='median', bins=bins)
            plt.plot(xsim, med, c=col[simnum], lw=3, ls='solid', label=labels[simnum])
            if err_env:
                errlow, bin_edges, n = binned_statistic(Mh, y, statistic=lambda Y: np.percentile(Y, 16.0), bins=bins)
                errhigh, bin_edges, n = binned_statistic(Mh, y, statistic=lambda Y: np.percentile(Y, 84.0), bins=bins)
                patch = np.ma.column_stack((np.concatenate((xsim,xsim[::-1])), np.ma.concatenate((errlow,errhigh[::-1]))))
                plt.gca().axes.add_patch(Polygon(patch[patch[:,1]>0], alpha=0.1, color=col[simnum], ec='none'))
        
        if zoomdirs[simnum] is not "":
            for i, zoom in enumerate(zooms):
                sim = snap.snapshot(zoombasedir+zoomdirs[simnum]+zoom, snapnum, mpc_units=True, extra_sub_props=True)
                fb = sim.omega_b / sim.header.omega_m
                Mh, mstar_total, mstar_inrad, mstar_inhalfrad, mstar_inmaxrad, mstar_inSPrad, mstar_inAper = get_SHM(sim, delta, groups=[0], DM_only=DM_only, total_mass=total_mass, Mhmin=Mhmin, Mhmax=Mhmax, verbose=verbose)
                if fractional:
                    fac = 1.0 / Mh / fb
                else:
                    fac = 1.0
                if aperture:
                    y = mstar_inAper * fac
                else:
                    y = mstar_total * fac
                if zoomlabels is None:
                    label = zoom.replace("_","\_")
                else:
                    label = zoomlabels[i]
                plt.plot(Mh, y, marker='o', mec='none', ls='none', label=label)
                    
    plt.legend(ncol=1, loc='upper left', frameon=False, fontsize=14)
    plt.savefig(outdir+outfilename, bbox_inches='tight')
    print "Plot saved to", outdir+outfilename
    
def mstar_mHalo_redshift(basedir, simdir, snapnums,
                         DM_only=True, delta=200, Mhmin=0.0, Mhmax=None, plot_obs=True,
                         outdir = "/data/curie4/nh444/project1/stars/", outfilename="Mstar_vs_MHalo_z.pdf", h_scale=0.7, verbose=True):
    """
    Plots stellar mass fraction vs halo mass for central galaxies at different snapshots, similar to Genel+ 2014 fig. 6 and Vogelsberger+ 2014 fig 12 
    If plot_obs is True, results from Moster+ 2013 abundance matching are also plotted
    If DM_only, we calculate the DM halo mass Mh,DM, otherwise we use the total mass Mh
    """
    import snap
    import numpy as np
    import matplotlib.pyplot as plt
    from scipy.stats import binned_statistic
    #from matplotlib.patches import Polygon
    
    if Mhmax is None:
        Mhmax = np.inf
        
    fig, ax = plt.subplots(1, figsize=(10,10))
    ax.set_xlabel("M$_{"+str(int(delta))+"}$ ($M_{\odot}$)")
    ax.set_ylabel("(M$_{\mathrm{star}}$ / M$_{"+str(int(delta))+"}$)/$f_b$")
    ax.set_xscale('log')
    ax.set_xlim(6e9, 1.5e14)
    #ax.set_ylim(0, 0.2)
    
    col = ["#c7673e","#78a44f","#6b87c8","#934fce","#e6ab02","#be558f","#47af8d"] ## dif redshifts
    bins = np.logspace(9, 15, num=30)

    for j, snapnum in enumerate(snapnums):
        sim = snap.snapshot(basedir+simdir, snapnum, mpc_units=False, extra_sub_props=True)
        z = sim.header.redshift
        
        fb = sim.omega_b / sim.header.omega_m
        
        Mh, mstar, mstar_inrad, mstar_inhalfrad, mstar_inmaxrad, mstar_inSPrad, mstar_inAper = get_SHM(sim, delta, DM_only=DM_only, Mhmin=Mhmin, Mhmax=Mhmax, verbose=verbose)
        y = mstar/Mh / fb
        #p = plt.plot(Mh, y, c=col[simnum], marker='o', ms=3, alpha=0.5, mec='none', ls='none')
        
        med, bin_edges, n = binned_statistic(Mh, y, statistic='mean', bins=bins)
        x = (bin_edges[1:]*bin_edges[:-1])**0.5
        ax.plot(x, med, c=col[j], lw=3, label="z = {:.1f}".format(z))
        #errlow, bin_edges, n = binned_statistic(Mh, y, statistic=lambda Y: np.percentile(Y, 16.0), bins=bins)
        #errhigh, bin_edges, n = binned_statistic(Mh, y, statistic=lambda Y: np.percentile(Y, 84.0), bins=bins)
        #patch = np.ma.column_stack((np.concatenate((x,x[::-1])), np.ma.concatenate((errlow,errhigh[::-1]))))
        #plt.gca().axes.add_patch(Polygon(patch[patch[:,1]>0], alpha=0.1, color=col[simnum], ec='none'))
        
        if plot_obs and int(delta)==200:
            mfrac = Moster2013_SHM(x, sim.header.redshift) ## scaled by f_b
            ax.plot(x, mfrac, c=col[j], ls='dashed', lw=3, label="Moster+ 2013 (z = {:.1f})".format(z))

    ax.legend(loc='upper left', ncol=1, frameon=True, fontsize=14)
    plt.tight_layout()
    plt.savefig(outdir+outfilename, bbox_inches='tight')
    print "Plot saved to", outdir+outfilename
   
def mstar_mHalo_zooms_comparison(basedir, simdir, zoombasedir, zoomdir, zooms, snapnums,
                                 zooms2=None, highres_only=True, delta=200, Mhmin=0.0, Mhmax=None,
                                 DM_only=True, total_mass=False, plot_obs=True,
                                 outdir = "/data/curie4/nh444/project1/stars/", outfilename="Mstar_vs_MHalo_zooms.pdf",
                                 zoomlabels=None, titles=None, h_scale=0.7, verbose=True):
    """
    Plots stellar mass fraction vs halo mass for central galaxies (main halo of FoF groups) in simdir and overplots the same for zooms
    (see e.g. Genel+ 2014 fig. 6 and Vogelsberger+ 2014 fig 12) 
    Note: Mhmin and Mhmax are (total) mass limits on the FoF groups, not the DM masses of the central subhalo
    If highres_only is True, for zooms only subhaloes in the high-res region are used (this is usually most of the subhaloes)
    If 'zooms2' is a list of zooms like 'zooms', these are plotted on a second subplot for comparing to 'zooms'
    If DM_only, masses (Mh) will be for DM only, otherwise the total Mh (i.e. from Fof halo file)
    """
    import snap
    import numpy as np
    import matplotlib.pyplot as plt
    from scipy.stats import binned_statistic
    #from matplotlib.patches import Polygon
    
    if Mhmax is None:
        Mhmax = np.inf
        
    if zooms2 is not None:
        fig, axarr = plt.subplots(1, 2, figsize=(20,10), sharex=True, sharey=True)
    else:
        fig, axarr = plt.subplots(1, figsize=(10,10))
        axarr = [axarr]
    if DM_only:
        xlabel= "M$_{\mathrm{DM},"+str(int(delta))+"}$ ($M_{\odot}$)"
    else:
        xlabel = "M$_{\mathrm{tot},"+str(int(delta))+"}$ ($M_{\odot}$)"
    if total_mass:
        ylabel = "(M$_{\star,\mathrm{tot}}$ / M$_{"+str(int(delta))+"}$)/$f_b$"
    else:
        ylabel = "(M$_{\star,\mathrm{cen}}$ / M$_{"+str(int(delta))+"}$)/$f_b$"
    for ax in axarr:
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)
        ax.set_xscale('log')
        ax.set_xlim(6e9, 1.5e14)
        ax.set_ylim(0, 0.4)
    
    
    col = ["#c7673e","#78a44f","#6b87c8","#934fce","#e6ab02","#be558f","#47af8d"] ## dif redshifts
    markers = ['o', 'D', 's', '^'] ## dif zooms
    bins = np.logspace(9, 15, num=30)
    x = (bins[1:]*bins[:-1])**0.5

    for j, snapnum in enumerate(snapnums):
        if verbose: print "Starting snapshot", snapnum
        sim = snap.snapshot(basedir+simdir, snapnum, mpc_units=False, extra_sub_props=True)
        z = sim.header.redshift
        
        fb = sim.omega_b / sim.header.omega_m
        
        Mh, mstar, mstar_inrad, mstar_inhalfrad, mstar_inmaxrad, mstar_inSPrad, mstar_inAper = get_SHM(sim, delta, DM_only=DM_only, total_mass=total_mass, Mhmin=Mhmin, Mhmax=Mhmax, verbose=verbose)
        y = mstar/Mh / fb
        #p = plt.plot(Mh, y, c=col[simnum], marker='o', ms=3, alpha=0.5, mec='none', ls='none')
        
        med, bin_edges, n = binned_statistic(Mh, y, statistic='mean', bins=bins)
        for ax in axarr:
            ax.plot(x, med, c=col[j], lw=3, label="z = {:.1f}".format(z))
        #errlow, bin_edges, n = binned_statistic(Mh, y, statistic=lambda Y: np.percentile(Y, 16.0), bins=bins)
        #errhigh, bin_edges, n = binned_statistic(Mh, y, statistic=lambda Y: np.percentile(Y, 84.0), bins=bins)
        #patch = np.ma.column_stack((np.concatenate((x,x[::-1])), np.ma.concatenate((errlow,errhigh[::-1]))))
        #plt.gca().axes.add_patch(Polygon(patch[patch[:,1]>0], alpha=0.1, color=col[simnum], ec='none'))
        
        if plot_obs and int(delta)==200:
            if not total_mass:
                mfrac = Moster2013_SHM(x, sim.header.redshift) ## scaled by f_b, uses DM only masses
                for ax in axarr:
                    ax.plot(x, mfrac, c='limegreen', lw=3, ls='dashed', label="Moster+ 2013 (z = {:.1f})".format(z))
            ## I think Kravtsov plot total DM+baryonic masses, not just DM
            Krav = np.genfromtxt("/data/vault/nh444/ObsData/mass_fracs/Kravtsov2014_SHM.csv", delimiter=",") 
            if total_mass:
                for ax in axarr:
                    ax.plot(10**Krav[:,3], Krav[:,4], c='darkblue', lw=2, label="Kravtsov+ 2014")## scaled by f_b
            else:
                for ax in axarr:
                    ax.plot(10**Krav[:,0], Krav[:,1], c='darkblue', lw=2, label="Kravtsov+ 2014 with scatter")## scaled by f_b
                    ax.plot(10**Krav[:,6], Krav[:,7], c='darkblue', lw=2, ls='dashed', label="Kravtsov+ 2014 w/o scatter")## scaled by f_b
        
        nzooms1 = 0
        if zooms2 is not None:
            nzooms1 = len(zooms)
            zooms = np.concatenate((zooms, zooms2))
        for i, zoom in enumerate(zooms):
            if verbose: print "Starting zoom", zoom
            sim = snap.snapshot(zoombasedir+zoomdir+zoom, snapnum, mpc_units=True)
            fb = sim.omega_b / sim.header.omega_m
            if highres_only:
                """update this so it can calculate the total mass like in get_SHM(), not just central galaxy mass"""
                assert not total_mass, "total_mass not currently compatible with highres_only"
                ## Find groups in high resolution region
                if verbose: print "Loading hostsubhalos..."
                sim.get_hostsubhalos()
                if verbose: print "Loading gas masses..."
                sim.load_gasmass()
                sim.load_highresgasmass()
                ind = np.where(sim.hrgm > 0.5 * sim.gasmass)[0] ## indices of high res gas
                subnums = np.unique(sim.hostsubhalos[ind]) ## indices of subhaloes containing at least some high res gas
                subnums = subnums[subnums >= 0] ## remove -1 and -2 (not in subhalo but in Fof group and not in any group, respectively)
                hrgm = sim.gasmass[ind] ## gas masses of high res gas
                if verbose: print "Calculating subhalo gas masses..."
                subhrgm = np.asarray([np.sum(hrgm[np.where(sim.hostsubhalos[ind] == subnum)[0]]) for subnum in subnums])
                subgasmass = np.asarray([np.sum(sim.gasmass[np.where(sim.hostsubhalos[:sim.species_end[0]] == subnum)[0]]) for subnum in subnums])
                #sim.load_mass()
                #submstar = np.asarray([np.sum(sim.mass[sim.species_start[4]+np.where(sim.hostsubhalos[sim.species_start[4]:sim.species_end[4]] == subnum)[0]]) for subnum in subnums])
                #print subhrgm / sim.cat.sub_masstab[subnums, 0]
                #print subgasmass / sim.cat.sub_masstab[subnums, 0] - 1.0
                if verbose:
                    print "Max. and min. fractional difference between calculated gas mass and SUBFIND gas mass:"
                    print np.max(subgasmass / sim.cat.sub_masstab[subnums, 0] - 1.0), np.min(subgasmass / sim.cat.sub_masstab[subnums, 0] - 1.0)
                    #print np.max(subhrgm / sim.cat.sub_masstab[subnums, 0] - 1.0), np.min(subhrgm / sim.cat.sub_masstab[subnums, 0] - 1.0)
                    print "Max. and min. high-res gas mass fraction:"
                    print np.max(subhrgm / subgasmass), np.min(subhrgm / subgasmass)
                #print submstar / sim.cat.sub_masstab[subnums, 4] - 1.0
                subnums = subnums[np.where(subhrgm / subgasmass > 0.99)[0]] ## subhaloes where >99% of the gas mass is high-res
                print "Found", len(subnums), "subhaloes in high-res region."
                subgrpnums = sim.cat.sub_grnr[subnums] ## group numbers for each subhalo
                if DM_only:
                    Mh, Rh = get_DM_mass(subnums, sim, delta, verbose=verbose)
                else:
                    Mh = getattr(sim.cat, 'group_m_crit'+str(int(delta)))[subgrpnums] * 1e10 / h_scale
                    #Rh = getattr(sim.cat, 'group_r_crit'+str(int(delta)))[subgrpnums] / h_scale
                mstar = sim.cat.sub_masstab[subnums, 4] * 1e10 / h_scale
                CentInd = np.where(subnums == sim.cat.group_firstsub[subgrpnums])[0]
            else:
                Mh, mstar, CentInd = get_SHM(sim, delta, centrals_only=False, DM_only=DM_only, total_mass=total_mass, Mhmin=0.0, Mhmax=None, verbose=verbose)
#==============================================================================
#                 if DM_only:
#                     Mh, Rh = get_DM_mass(range(sim.cat.nsubs), sim, delta, verbose=verbose)
#                 else:
#                     Mh = getattr(sim.cat, 'group_m_crit'+str(int(delta)))[sim.cat.sub_grnr] * 1e10 / h_scale
#                     Rh = getattr(sim.cat, 'group_r_crit'+str(int(delta)))[sim.cat.sub_grnr] / h_scale
#                 mstar = sim.cat.sub_masstab[:, 4] * 1e10 / h_scale
#                 CentInd = sim.cat.group_firstsub ## indices denoting which subhaloes are centrals (main halos)
#==============================================================================
            
            ind = np.where(mstar > 0.0)[0]
            y = mstar/Mh / fb
            if zoomlabels is None:
                label = zoom.replace("_","\_")
            else:
                label = zoomlabels[i]
            if nzooms1 > 0 and i >= nzooms1:
                ax = axarr[1]
            else:
                ax = axarr[0]
            ax.plot(Mh[ind], y[ind], marker=markers[i], mfc='none', mec=col[j], ls='none', label=label)
            CentInd = np.intersect1d(ind, CentInd)
            ax.plot(Mh[CentInd], y[CentInd], marker=markers[i], mfc=col[j], mec=col[j], mew=0.5, ls='none', label=label+" (centrals)")

    for ax in axarr:
        ax.legend(loc='upper left', ncol=1, frameon=True, fontsize=14)
    if titles is not None:
        axarr[0].set_title(titles[0])
        axarr[1].set_title(titles[1])
    plt.tight_layout()
    plt.savefig(outdir+outfilename, bbox_inches='tight')
    print "Plot saved to", outdir+outfilename
    
def get_SHM(sim, delta, total_mass=False, DM_only=True, centrals_only=True, groups=None, Mhmin=0.0, Mhmax=None, verbose=True):
    """
    Calculate stellar mass fraction as a function of Mh
    sim - snap.snapshot class object
    delta = 200 or 500 for critical overdensity
    groups - list of FoF groups to use, otherwise uses groups with Mh between Mhmin and Mhmax
    total_mass - calculate total stellar mass (central + satellites within Rh), otherwise just central mass
    DM_only - Mh is DM only, otherwise total Mh (DM + baryons)
    """
    import numpy as np
    if Mhmax is None:
        Mhmax = np.inf
        
    if centrals_only:
        if groups is None:
            groups = np.where((getattr(sim.cat, 'group_m_crit'+str(int(delta)))*1e10/h_scale > Mhmin) & (getattr(sim.cat, 'group_m_crit'+str(int(delta)))*1e10/h_scale < Mhmax))[0]
        subnums = sim.cat.group_firstsub[groups] ## central galaxies
        assert len(subnums) == len(groups)
    else:
        if (groups is not None) or (Mhmin > 0.0):
            assert False, "centrals_only=False currently doesn't work for specific groups"
        groups = np.arange(sim.cat.ngroups)
        subnums = np.arange(sim.cat.nsubs) ## all subhalos
        CentInd = sim.cat.group_firstsub ## indices of centrals
    
    ind = np.where(sim.cat.sub_masstab[subnums, 4] > 0.0)[0] ## ignore any subhaloes with no stars
    if centrals_only:
        grpind = ind
    else:
        grpind = sim.cat.sub_grnr[subnums[ind]]
        CentInd = np.where(np.in1d(ind, CentInd))
    if DM_only:
        print "Calculating DM mass..."
        Mh, Rh = get_DM_mass(subnums[ind], sim, delta, verbose=verbose)
    else:
        Mh = getattr(sim.cat, 'group_m_crit'+str(int(delta)))[groups[grpind]] * 1e10 / h_scale ## assumes Mh and Rh at position of central galaxy is equal to Rh at the centre of the group
        Rh = getattr(sim.cat, 'group_r_crit'+str(int(delta)))[groups[grpind]]
        if not hasattr(Rh, "__len__"): # if a scalar, convert to a list to avoid indexing problems below
            Rh = [Rh]
    if total_mass: ## masses of central galaxies and satellites within Rh
        ## Note: This counts satellites within Rh from the position of the central galaxy, which may not necessarily be the same as the centre of its parent group
        mstar = []
        mstar_inrad = []
        mstar_inhalfrad = []
        mstar_inmaxrad = []
        mstar_inSPrad = [] ## mass within surface brightness limit radius (default 20.7 mag/arcsec^2 in K-band)
        mstar_inAper = []
        subind = np.where(sim.cat.sub_masstab[:, 4] > 0.0)[0] ## don't bother with subhalos with no bound star particles
        mstar_inAper_all = get_stellar_masses_in_aperture(sim, subind, R=30.0) ## get stellar masses within aperture
        for i, subnum in enumerate(subnums[ind]):
            if verbose and (i+1)%500==0: print "Subnum", subnum, "({:d}/{:d})".format(i+1, len(ind))
            rsub = sim.nearest_dist_3D(sim.cat.sub_pos[subind], sim.cat.sub_pos[subnum]) / h_scale ## distance of all other subhalos
            rind = np.where(rsub <= Rh[i])[0]
            rsubind = subind[rind]
            if not subnum in rsubind:
                print subnum, rsubind
                print "rsub =", rsub
                print "Rh[i] =", Rh[i]
                print "np.where(rsub < Rh[i])[0] =", np.where(rsub <= Rh[i])[0]
                print np.where(subind == subnum)
                print "sim.cat.sub_masstab[subnum, 4] =", sim.cat.sub_masstab[subnum, 4]
                assert False
            mstar.append(np.sum(sim.cat.sub_masstab[rsubind,4]) * 1e10 / h_scale)
            mstar_inrad.append(np.sum(sim.cat.sub_massinradtab[rsubind,4]) * 1e10 / h_scale)
            mstar_inhalfrad.append(np.sum(sim.cat.sub_massinhalfradtab[rsubind,4]) * 1e10 / h_scale)
            mstar_inmaxrad.append(np.sum(sim.cat.sub_massinmaxradtab[rsubind,4]) * 1e10 / h_scale)
            mstar_inSPrad.append(np.sum(sim.cat.sub_massinSPrad[rsubind]) * 1e10 / h_scale)
            mstar_inAper.append(np.sum(mstar_inAper_all[rind]) * 1e10 / h_scale)
            #if verbose: print mstar[i] / (sim.cat.sub_masstab[subnum, 4] * 1e10 / h_scale)
            #raw_input('') ## wait for input to continue
        mstar = np.array(mstar)
        mstar_inrad = np.array(mstar_inrad)
        mstar_inhalfrad = np.array(mstar_inhalfrad)
        mstar_inmaxrad = np.array(mstar_inmaxrad)
        mstar_inSPrad = np.array(mstar_inSPrad)
        mstar_inAper = np.array(mstar_inAper)
    else:## just take the mass of the central galaxy, not sats
        mstar = sim.cat.sub_masstab[subnums[ind], 4] * 1e10 / h_scale
        mstar_inrad = sim.cat.sub_massinradtab[subnums[ind], 4] * 1e10 / h_scale
        mstar_inhalfrad = sim.cat.sub_massinhalfradtab[subnums[ind], 4] * 1e10 / h_scale
        mstar_inmaxrad = sim.cat.sub_massinmaxradtab[subnums[ind], 4] * 1e10 / h_scale
        mstar_inSPrad = sim.cat.sub_massinSPrad[subnums[ind]] * 1e10 / h_scale ## mass within surface brightness limit radius (default 20.7 mag/arcsec^2 in K-band)
        mstar_inAper = get_stellar_masses_in_aperture(sim, subnums[ind], R=30.0) * 1e10 / h_scale ## get stellar masses within aperture
    if centrals_only:
        return Mh, mstar, mstar_inrad, mstar_inhalfrad, mstar_inmaxrad, mstar_inSPrad, mstar_inAper
    else:
        return Mh, mstar, CentInd
    
def get_stellar_masses_in_aperture(sim, subnums, R=30.0, verbose=True):
    ## Calculate stellar masses within aperture of radius R (proper kpc) for subhalos with indices 'subnums'
    ## Note this only counts star particles which are bound to the subhalo, as in Schaye+2014 (EAGLE)
    import readsnap as rs
    import numpy as np
    
    if verbose: print "Loading masses and positions..."
    R = R * sim.header.hubble * (1 + sim.header.redshift) ## proper distance aperture converted to comoving distance
    pos = rs.read_block(sim.snapname, "POS ", parttype=4, cosmic_ray_species=sim.num_cr_species)
    mass = rs.read_block(sim.snapname, "MASS", parttype=4, cosmic_ray_species=sim.num_cr_species)
    age = rs.read_block(sim.snapname, "GAGE", cosmic_ray_species=sim.num_cr_species) ## GFM_StellarFormationTime 
    mstar_30kpc = []
    if verbose: print "Calculating subhalo stellar masses within 30 kpc"
    for subnum in subnums:
        r = sim.nearest_dist_3D(pos[sim.cat.sub_offsettab[subnum,4]:sim.cat.sub_offsettab[subnum,4]+sim.cat.sub_lentab[subnum,4]], sim.cat.sub_pos[subnum])
        ageind = np.where(age[sim.cat.sub_offsettab[subnum,4]:sim.cat.sub_offsettab[subnum,4]+sim.cat.sub_lentab[subnum,4]] > 0.0)[0] ## GFM wind particles are star particles with formation time is <0, subfind counts these as gas when calculating subhalo mass table
        starind = sim.cat.sub_offsettab[subnum,4] + np.intersect1d(np.where(r <= R), ageind)
        mstar_30kpc.append(np.sum(mass[starind]))
    return np.array(mstar_30kpc) ## code units (1e10 Msun / h)
        
def get_DM_mass(subnums, sim, delta, calc_Rh=True, verbose=True):
    """
    I should rewrite this function to calculate the TOTAL mass, which is what I should be using
    This function should only be necessary for satellites since Mh is already known at the position
    of groups and therefore their central galaxies
    """
    ## Always have calc_Rh=True so that Rh is calculated for every subhalo. Otherwise, the DM mass returned is the mass within the parent group's Rh as based on total mass not DM
    import numpy as np
    import readsnap as rs
    import scipy.spatial
    if verbose:
        from timeit import default_timer as timer
        import sys
        
    if verbose: print "Loading DM positions..."
    DMpos = rs.read_block(sim.snapname, "POS ", parttype=1, cosmic_ray_species=sim.num_cr_species)
    if sim.mpc_units:
        DMpos *= 1000.0
    if verbose: print "Constructing k-d tree (may take a while)..."
    starttime = timer()
    kdtree = scipy.spatial.cKDTree(DMpos, leafsize=20, boxsize=sim.box_sidelength, balanced_tree=False) ## set leafsize to approx the smallest number of particles in a subhalo of interest
    if verbose: print "Done. k-d tree took", timer() - starttime, "seconds."
    if verbose: print "Loading DM masses..."
    DMmass = rs.read_block(sim.snapname, "MASS", parttype=1, cosmic_ray_species=sim.num_cr_species)
    Mh = []
    Rh = []
    for k, subnum in enumerate(subnums):
        if verbose:
            #sys.stdout.write("\rDoing subnum "+str(subnum)+" ({:d}/{:d})".format(k+1, len(subnums)))
            print "Doing subnum "+str(subnum)+" ({:d}/{:d})".format(k+1, len(subnums))
        if calc_Rh:
            ## Just use DM particles within a radius 2.0 times the parent group total Rh, which should always be larger than the subhalo's true Rh, even satellities
            sel_ind = kdtree.query_ball_point(sim.cat.sub_pos[subnum], 2.0 * getattr(sim.cat, 'group_r_crit'+str(int(delta)))[sim.cat.sub_grnr[subnum]])
            m, r,flag = get_so_mass_rad(DMpos[sel_ind], DMmass[sel_ind], sim.cat.sub_pos[subnum], delta, sim, ref="crit", verbose=False)
            if flag is "not found": ## if failed, try again using more DM particles
                if verbose: print "Re-doing subnum "+str(subnum)+" ({:d}/{:d})".format(k+1, len(subnums))
                sel_ind = kdtree.query_ball_point(sim.cat.sub_pos[subnum], 5.0 * getattr(sim.cat, 'group_r_crit'+str(int(delta)))[sim.cat.sub_grnr[subnum]])
                m, r = get_so_mass_rad(DMpos[sel_ind], DMmass[sel_ind], sim.cat.sub_pos[subnum], delta, sim, ref="crit", verbose=False)
            Mh.append(m * 1e10 / h_scale)
            Rh.append(r / h_scale)
            #rad_ind = kdtree.query_ball_point(sim.cat.sub_pos[subnum], Rh)
            #print Mh, np.sum(DMmass[rad_ind])
        else: ## use Rh of parent group instead of calculating it
            rad_ind = kdtree.query_ball_point(sim.cat.sub_pos[subnum], getattr(sim.cat, 'group_r_crit'+str(int(delta)))[sim.cat.sub_grnr[subnum]])
            Mh.append(np.sum(DMmass[rad_ind]) * 1e10 / h_scale)
            Rh.append(getattr(sim.cat, 'group_r_crit'+str(int(delta)))[sim.cat.sub_grnr[subnum]])
        if verbose:
            sys.stdout.flush()
    if verbose:
        sys.stdout.write("\n")
    Mh = np.asarray(Mh)
    Rh = np.asarray(Rh)
    return Mh, Rh
    
def get_so_mass_rad(pos, mass, centre, meandens, sim, ref="crit", verbose=False):
    ## calculate spherical overdensity mass and radius using positions 'pos' and masses 'mass'
    ## meandens is delta, the overdensity factor e.g. 500
    ## sim is an instance of the sim.snapshot class
    import numpy as np

    if ref=="crit":
        thresdens =  2.77536627e-8 * pow(sim.cm.hubble_z(sim.header.redshift),2)/(pow(sim.header.hubble,2) * (1+sim.header.redshift)**3)
    elif ref=="crit_today":
        thresdens =  2.77536627e-8 / (1+sim.header.redshift)**3 ## comoving value at z = redshift of the physical critical density today
    elif ref=="mean":
        thresdens =  2.77536627e-8 * sim.header.omega_m
    else:
        assert False, "reference density not known"

    if verbose: print "Calculating radii..."
    partrads = sim.nearest_dist_3D(pos, centre)
    radord = partrads.argsort()
    partrads = partrads[radord]
    meandensity = (mass[radord].cumsum(dtype=np.float64) - 0.5*mass[radord])/(4.0/3.0*np.pi*np.maximum(partrads,1.0e-5)**3) # maximum is used to prevent division by zero

    ind = np.where((meandensity[:-1]>=thresdens*meandens) & (meandensity[1:]<thresdens*meandens))

    if ind[0].size==0:
        if verbose: print "no suitable radius found"
        flag = "not found"
        return 0.0, 0.0, flag
    elif ind[0].size!=1:
        if verbose: print "WARNING:",ind[0].size,"suitable radii found -> using smallest"

    ind = ind[0][0]
    if partrads[ind]==0:
        if verbose: print "SO radius not resolved"
        flag = "unresolved"
        return 0.0, 0.0, flag

    radius = (partrads[ind]*(thresdens*meandens-meandensity[ind+1]) + partrads[ind+1]*(meandensity[ind]-thresdens*meandens))/(meandensity[ind]-meandensity[ind+1])
    mass = 4.0/3.0*np.pi*radius**3*thresdens*meandens

    if verbose:
        print "M_"+ref+","+str(meandens)+" =",mass
        print "r_"+ref+","+str(meandens)+" =",radius

    flag = "found"
    return mass, radius, flag
    
def Moster2013_SHM(M200, z, h):
    """ Redshift-dependent SHM relation from Moster et al. 2013 """
    M200 *= h / 0.704 ## convert to h scaling of Moster+2013
    omega_m = 0.272
    omega_b = 0.046
    M10 = 11.59
    M11 = 1.195
    N10 = 0.0351
    N11 = -0.0247
    beta10 = 1.376
    beta11 = -0.826
    gamma10 = 0.608
    gamma11 = 0.329
    M1 = 10**(M10 + M11 * (z / (z+1)))
    N = N10 + N11 * (z / (z+1))
    beta = beta10 + beta11 * (z / (z+1))
    gamma = gamma10 + gamma11 * (z / (z+1))
    mfrac = 2 * N / ((M200 / M1)**-beta + (M200 / M1)**gamma) * omega_m / omega_b ## scaled by f_b
    return mfrac
    
def Moster2017_SHM(M200, z, h, centrals=True):
    """ Redshift-dependent SHM relation from Moster et al. 2017 (EMERGE) """
    import numpy as np
    M200 *= h / 0.6781 ## convert to h scaling of Moster+2013
    #omega_m = 0.308
    #omega_b = 0.0484
    dat = np.genfromtxt("/data/vault/nh444/ObsData/SHM/EMERGE_Moster2017.csv", delimiter=",")
    ### cols are z,M_1,epsilon_N,beta,gamma,M_sigma,sigma_0,alpha
    ### rows are z=0.1,0.5,1,2,4,8 for centrals, quenched centrals, SF centrals, and all galaxies
    assert z <= 8.0, "Cannot plot EMERGE SHM relation for redshift z>8"
    zdat = [0.1, 0.5, 1.0, 2.0, 4.0, 8.0]
    zrow = np.min(np.where(zdat > z-0.1)[0])
    if centrals:
        irow = zrow
    else:
        irow = len(zdat) * 3 + zrow
    zused = dat[irow, 0]
    assert zused in zdat, "Unexpected redshift value in table"
    M1, N, beta, gamma, M_sigma, sigma0, alpha = dat[irow, 1:]
    M1 = 10**M1
    M_sigma = 10**M_sigma
    mfrac = 2 * N / ((M200 / M1)**-beta + (M200 / M1)**gamma) ## scaled by f_b
    scat = sigma0 + np.log10((M200 / M_sigma)**(-alpha) + 1)
    mfrac_low = 10**(np.log10(mfrac) - scat)
    mfrac_high = 10**(np.log10(mfrac) + scat)
    return mfrac, mfrac_low, mfrac_high, zused

if __name__ == "__main__":
    main()