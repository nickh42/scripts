
; cosmology stuff

PRO setcosmo, omegamatter, hubble ;call by e.g.: setcosmo, 0.3, 0.7
  common cosmoparams, omegam, omegal, h
  omegam = omegamatter
  omegal = 1.0D - omegam
  h = hubble
END

PRO printcosmo
  common cosmoparams, omegam, omegal, h
  print, "omegam = ", omegam
  print, "omegal = ", omegal
  print, "h = ", h
END

FUNCTION difftime, scalefactor ; dt/da in (Mpc/h)/(km/s)
  common cosmoparams, omegam, omegal, h
  H0 = 100.0D*h
  RETURN, 1.0D/(H0*sqrt(omegam/(scalefactor*scalefactor*scalefactor)+omegal)*scalefactor)*h
END

FUNCTION time, scalefactor1, scalefactor2 ; time in (Mpc/h)/(km/s), i.e. in internal Gadget units if Mpc/h are used for lenght
  RETURN, qsimp('difftime',scalefactor1,scalefactor2,/DOUBLE)
END

;--------------------------------------------------

; interpolates orbits using x = a + b*(t-t1) + c*(t-t1)^2 + d*(t-t1)^3
; and x1=x(t1), v1=v(t1), x2=x(t2), v2=v(t2)
; see orbit_interpolation.nb

FUNCTION interpolate_orbit, x1, v1, t1, x2, v2, t2, t
  cur_t = t - t1
  DT = t2 - t1
  RETURN, x1 + v1*cur_t - cur_t^2*(2*DT*v1 + DT*v2 + 3*x1 - 3*x2)/DT^2 - cur_t^3*(-DT*v1 - DT*v2 - 2*x1 + 2*x2)/DT^3
END

;--------------------------------------------------

Num       =  40        ; get icl and gal star ids from this snapshot 
numsnaps  =  64         ; number of snapshots

; Simulation directory:

;Base= "/gpfs/mpa/ewp/sims/zoom_3_sims/m_3629_h_259_610_z3_csfbh/"
;Base= "/gpfs/mpa/ewp/sims/zoom_3_sims_new/m_5443_h_352_482_z3_csfbh/"
;Base= "/gpfs/mpa/ewp/sims/zoom_2_sims/m_14999_h_355_1278_z2_csf/"
;Base= "/gpfs/mpa/ewp/sims/zoom_2_sims/m_1975_h_506_1096_z2_csf/"
;Base= "/gpfs/mpa/ewp/sims/zoom_2_sims/m_4445_h_183_1161_z2_csf/"
;Base= "/gpfs/mpa/ewp/sims/test_runs/m_14999_h_355_1278_z2_csf_accurate/"
;Base= "/gpfs/mpa/ewp/sims/test_runs/m_14999_h_355_1278_z2_csf_slow_sf/"
;Base= "/scratch/temp_simdat/m_1975_h_506_1096_z2_csf/"
;Base= "/gpfs/mpa/ewp/sims/test_runs/m_10002_h_94_501_csf/"
;Base= "/gpfs/mpa/ewp/sims/test_runs/m_10002_h_94_501_csf_winds/"
;Base= "/gpfs/mpa/ewp/sims/zoom_3_sims/m_10002_h_94_501_z3_csfbh/"
;Base= "/gpfs/mpa/ewp/sims/zoom_2_sims/m_22498_h_197_0_z2_csfbh/"
;Base= "/gpfs/mpa/ewp/sims/zoom_2_sims/m_33617_h_146_0_z2_csf/"
Base= "/data/deboras/ewald/m_10002_h_94_501_z3/csfbh/"

; include readnew.pro
!PATH = !PATH +':'+EXPAND_PATH('+/home/nh444/scripts/mass_fracs/')

; Group cat directory:

firstgroupsdir = "groups_"
;firstgroupsdir = "postproc_"       ; required if cd_cutoff_method = 2

lsubdir = "postproc_"

;check_before = 0 ;find new stars after formation 
check_before = 1 ;"new stars" are the parent gas particles that will form a star
;;find gas particles before forming a new star

group = 0L              ; FoF group to analyze, currently only 0 supported 

include_fuzzy = 1       ; set to 1 to include stars from the FoF group that are not bound to any subhalo

cd_cutoff_method = 0          ; 0 for cutoff_rad, 1 for scaled cutoff_rad, 2 for binding energy
cutoff_rad = 0.03D            ; used if cd_cutoff_method = 0 (Mpc)
cutoff_rad_norm = 0.0091*3.0  ; cutoff_rad at M_200,crit = 10^15 M_sun/h, used if cd_cutoff_method = 1

;comments:
  ;to save images: end with typing 0 and e.g. for i=30,63 do write_png,"snap_"+strcompress(string(i),/remove_all)+".png",reform(cache[i,*,*,*])

;--------------------------------------------------

FLAG_Group_VelDisp = 0  ; Set this to one if the SO-properties computed by SUBFIND for
                        ; the FOF halos contain velocity dispersions

;--------------------------------------------------

if num ge 1000 then begin
   exts='0000'
   exts=exts+strcompress(string(Num),/remove_all)
   exts=strmid(exts,strlen(exts)-4,4)
endif else begin
   exts='000'
   exts=exts+strcompress(string(Num),/remove_all)
   exts=strmid(exts,strlen(exts)-3,3)
endelse

skip = 0L
skip_sub = 0L
fnr = 0L

repeat begin
    ;; read halo file
    f = Base + "/" + firstgroupsdir + exts +"/subhalo_tab_"+exts +"."+strcompress(string(fnr),/remove_all)

    Ngroups = 0L
    TotNgroups = 0L
    Nids = 0L
    TotNids = 0LL
    NTask = 0L
    Nsubgroups = 0L
    TotNsubgroups = 0L

    openr,1,f
    readu,1, Ngroups, TotNgroups, Nids, TotNids, NTask, Nsubgroups, TotNsubgroups

    if fnr eq 0 then begin
        GroupLen = lonarr(TotNgroups)
        GroupOffset = lonarr(TotNgroups)
        GroupMass = fltarr(TotNgroups)
        GroupPos = fltarr(3, TotNgroups)
        Group_M_Mean200 = fltarr(TotNgroups)
        Group_R_Mean200 = fltarr(TotNgroups)
        Group_M_Crit200 = fltarr(TotNgroups)
        Group_R_Crit200 = fltarr(TotNgroups)
        Group_M_TopHat200 = fltarr(TotNgroups)
        Group_R_TopHat200 = fltarr(TotNgroups)
        Group_VelDisp_Mean200 = fltarr(TotNgroups)
        Group_VelDisp_Crit200 = fltarr(TotNgroups)
        Group_VelDisp_TopHat200 = fltarr(TotNgroups)
        GroupContaminationCount = lonarr(TotNgroups)
        GroupContaminationMass = fltarr(TotNgroups)
        GroupNsubs = lonarr(TotNgroups)
        GroupFirstSub = lonarr(TotNgroups)

        SubhaloLen = lonarr(TotNsubgroups)
        SubhaloOffset = lonarr(TotNsubgroups)
        SubhaloParent = lonarr(TotNsubgroups)
        SubhaloMass = fltarr(TotNsubgroups)
        SubhaloPos = fltarr(3, TotNsubgroups)
        SubhaloVel = fltarr(3, TotNsubgroups)
        SubhaloCM = fltarr(3, TotNsubgroups)
        SubhaloSpin = fltarr(3, TotNsubgroups)
        SubhaloVelDisp = fltarr(TotNsubgroups)
        SubhaloVmax = fltarr(TotNsubgroups)
        SubhaloVmaxRad = fltarr(TotNsubgroups)
        SubhaloHalfmassRad = fltarr(TotNsubgroups)
        SubhaloIDMostbound = lonarr(TotNsubgroups)
        SubhaloGrNr = lonarr(TotNsubgroups)
    endif

    if Ngroups gt 0 then begin
        
        locLen = lonarr(Ngroups)
        locOffset = lonarr(Ngroups)
        locMass = fltarr(Ngroups)
        locPos = fltarr(3, Ngroups)
        loc_M_Mean200 = fltarr(Ngroups)
        loc_R_Mean200 = fltarr(Ngroups)
        loc_M_Crit200 = fltarr(Ngroups)
        loc_R_Crit200 = fltarr(Ngroups)
        loc_M_TopHat200 = fltarr(Ngroups)
        loc_R_TopHat200 = fltarr(Ngroups)
        loc_VelDisp_Mean200 = fltarr(Ngroups)
        loc_VelDisp_Crit200 = fltarr(Ngroups)
        loc_VelDisp_TopHat200 = fltarr(Ngroups)
        locContaminationCount = lonarr(Ngroups)
        locContaminationMass = fltarr(Ngroups)
        locNsubs = lonarr(Ngroups)
        locFirstSub = lonarr(Ngroups)

        readu,1, loclen 
        readu,1, locOffset
        readu,1, locMass
        readu,1, locPos
        readu,1, loc_M_Mean200
        readu,1, loc_R_Mean200
        readu,1, loc_M_Crit200
        readu,1, loc_R_Crit200
        readu,1, loc_M_TopHat200
        readu,1, loc_R_TopHat200
        if FLAG_Group_VelDisp ne 0 then begin
          readu,1, loc_VelDisp_Mean200
          readu,1, loc_VelDisp_Crit200
          readu,1, loc_VelDisp_TopHat200
        endif
        readu,1, locContaminationCount
        readu,1, locContaminationMass
        readu,1, locNsubs
        readu,1, locFirstSub

        GroupLen(skip:skip+Ngroups-1) = locLen(*)
        GroupOffset(skip:skip+Ngroups-1) = locOffset(*)
        GroupMass(skip:skip+Ngroups-1) = locMass(*)
        GroupPos(*, skip:skip+Ngroups-1) = locPos(*,*)
        Group_M_Mean200(skip:skip+Ngroups-1) = loc_M_Mean200(*)
        Group_R_Mean200(skip:skip+Ngroups-1) = loc_R_Mean200(*)
        Group_M_Crit200(skip:skip+Ngroups-1) = loc_M_Crit200(*)
        Group_R_Crit200(skip:skip+Ngroups-1) = loc_R_Crit200(*)
        Group_M_TopHat200(skip:skip+Ngroups-1) = loc_M_TopHat200(*)
        Group_R_TopHat200(skip:skip+Ngroups-1) = loc_R_TopHat200(*)
        Group_VelDisp_Mean200(skip:skip+Ngroups-1) = loc_VelDisp_Mean200(*)
        Group_VelDisp_Crit200(skip:skip+Ngroups-1) = loc_VelDisp_Crit200(*)
        Group_VelDisp_TopHat200(skip:skip+Ngroups-1) = loc_VelDisp_TopHat200(*)
        GroupContaminationCount(skip:skip+Ngroups-1) = locContaminationCount(*) 
        GroupContaminationMass(skip:skip+Ngroups-1) = locContaminationMass(*)
        GroupNsubs(skip:skip+Ngroups-1) = locNsubs(*)
        GroupFirstsub(skip:skip+Ngroups-1) = locFirstSub(*)

        skip+= Ngroups
    endif
 

    if Nsubgroups gt 0 then begin
        
        locLen = lonarr(Nsubgroups)
        locOffset = lonarr(Nsubgroups)
        locParent = lonarr(Nsubgroups)
        locMass = fltarr(Nsubgroups)
        locPos = fltarr(3, Nsubgroups)
        locVel = fltarr(3, Nsubgroups)
        locCM = fltarr(3, Nsubgroups)
        locSpin= fltarr(3, Nsubgroups)
        locVelDisp = fltarr(Nsubgroups)
        locVmax = fltarr(Nsubgroups)
        locVmaxRad = fltarr(Nsubgroups)
        locHalfMassRad = fltarr(Nsubgroups)
        locIDMostBound = lonarr(Nsubgroups)
        locGrNr = lonarr(Nsubgroups)

        readu,1, loclen 
        readu,1, locOffset
        readu,1, locParent
        readu,1, locMass
        readu,1, locPos
        readu,1, locVel
        readu,1, locCM
        readu,1, locSpin
        readu,1, locVelDisp
        readu,1, locVmax
        readu,1, locVmaxRad
        readu,1, locHalfMassRad
        readu,1, locIDMostBound
        readu,1, locGrNr

        SubhaloLen(skip_sub:skip_sub+Nsubgroups-1) = locLen(*)
        SubhaloOffset(skip_sub:skip_sub+Nsubgroups-1) = locOffset(*)
        SubhaloParent(skip_sub:skip_sub+Nsubgroups-1) = locParent(*)
        SubhaloMass(skip_sub:skip_sub+Nsubgroups-1) = locMass(*)
        SubhaloPos(*, skip_sub:skip_sub+Nsubgroups-1) = locPos(*,*)
        SubhaloVel(*, skip_sub:skip_sub+Nsubgroups-1) = locVel(*,*)
        SubhaloCM(*, skip_sub:skip_sub+Nsubgroups-1) = locCM(*,*)
        SubhaloSpin(*, skip_sub:skip_sub+Nsubgroups-1) = locSpin(*,*)
        SubhaloVeldisp(skip_sub:skip_sub+Nsubgroups-1) = locVeldisp(*)
        SubhaloVmax(skip_sub:skip_sub+Nsubgroups-1) = locVmax(*)
        SubhaloVmaxRad(skip_sub:skip_sub+Nsubgroups-1) = locVmaxRad(*)
        SubhaloHalfmassRad(skip_sub:skip_sub+Nsubgroups-1) = locHalfmassRad(*)
        SubhaloIDMostBound(skip_sub:skip_sub+Nsubgroups-1) = locIDMostBound(*)
        SubhaloGrNr(skip_sub:skip_sub+Nsubgroups-1) = locGrNr(*)

        skip_sub+= Nsubgroups
    endif

    close, 1

    fnr++

endrep until fnr eq NTask

print
print, "TotNgroups   =", TotNgroups
print, "TotNsubgroups=", TotNsubgroups
print
print, "Largest group of length ", GroupLen(0)," has", GroupNsubs(0)," substructures"

sfrflag = 0
bhflag = 0

;--------------------------------------------------

skip = 0L
fnr = 0L

repeat begin
    ;; read halo file header
    f = Base + "/" + firstgroupsdir + exts +"/subhalo_ids_"+exts +"."+strcompress(string(fnr),/remove_all)

    idNgroups = 0L
    idTotNgroups = 0L
    idNids = 0L
    idTotNids = 0LL
    idNTask = 0L
    idOffset = 0L

    openr,1,f
    readu,1, idNgroups, idTotNgroups, idNids, idTotNids, idNTask, idOffset

    if fnr eq 0 then begin
        IDs = lonarr(idTotNids)
    endif

    if idNids gt 0 then begin
        
        locIDs = lonarr(idNids)
        readu,1, locIDs

        IDs(skip:skip+idNids-1) = locIDs(*)
        skip+= idNids

    endif

    close, 1

    fnr++

endrep until fnr eq idNTask

print
print, "Total IDs in grps=", idTotNids ;;Total number of particles that are within any halo
print

IDs = ulong(IDs)

;--------------------------------------------------
;; compute cutoff_rad if not already specified with cd_cutoff_method = 0
if (cd_cutoff_method LE 1) then print, "cutoff_rad = ", cutoff_rad, " Mpc/h"
if (cd_cutoff_method EQ 1) then begin
  print, "scaling cutoff_rad with cluster mass  M_crit,200 = ", Group_M_Crit200[group]/1.0e5, " [10^15 M_sun / h]"
  cutoff_rad = cutoff_rad_norm * (Group_M_Crit200[group]/1.0e5)^0.29205
endif

;; binding energy cutoff method
if (cd_cutoff_method EQ 2) then begin
  ;; requires firstgroupsdir = postproc_
  f = Base + "/" + firstgroupsdir + exts +"/cDidx_1.dat"
  openr,1,f
    
  curgroup = 0L
  thisgroup = 0L
  nstars = 0L
  vdsearch = 0L
  buff = 0L
  Rgrav = 0D
  numcD = 0L
  while (curgroup LE group) do begin
    readu,1,thisgroup
    if (curgroup NE thisgroup) then print, "Error finding group in cD file"
    readu,1,nstars
    readu,1,vdssearch
    for i=1,14 do readu,1,buff
    readu,1,Rgrav
    readu,1,numcD
    tempcdids = lon64arr(numcD)
    readu,1,tempcdids
    ++curgroup
  endwhile
  cd_ids = ulonarr(numcD)
  cd_ids[*] = tempcdids
  tempcdids = 0L
  close,1
  
  ind = sort(cd_ids)
  cd_ids = cd_ids[ind]
  cd_ids_size = n_elements(cd_ids)
  
  print, "read cD data for group", thisgroup
  print, "stars in first subhalo = ", nstars
  if (vdsearch EQ 1) then print, "fit was done for ICL"
  if (vdsearch EQ 2) then print, "fit was done for BCG"
  if (vdsearch EQ 3) then begin
    print, "NO FIT TO VELOCITY DISTRIBUTIONS WAS POSSIBLE!!!"
    stop
  endif
print, "Rgrav = ", Rgrav
print, "all cD particles = ", numcD

endif

;--------------------------------------------------

numallsubids = 0LL
;; total number of particles in all subhalos of group
numallsubids = total(subhalolen[groupfirstsub[group]:groupfirstsub[group]+groupnsubs[group]-1],/integer)
print, "numallsubids = ", numallsubids
readnew,Base+"/snap_"+exts,head,"HEAD",parttype=4
setcosmo, head.Omega0, head.HubbleParam
printcosmo
head = 0L

;; Read in star particle positions and age and order by their IDs
readnew,Base+"/snap_"+exts,part_pos,"POS ",parttype=4
readnew,Base+"/snap_"+exts,part_ids,"ID  ",parttype=4
readnew,Base+"/snap_"+exts,part_age,"AGE ",parttype=4
ind = sort(part_ids)
part_ids = part_ids[ind]
part_pos = part_pos[*,ind]
part_age = part_age[ind]
part_ids_size = (size(part_ids))[1]
print, "part_ids_size = ", part_ids_size

;; Get star particle mass - assume all same mass
readnew,Base+"/snap_"+exts,part_mass,"MASS",parttype=4
star_part_mass = part_mass[0]
part_mass = 0L

;; position of most massive subhalo in group (normally the same pos as the group)
centerpos = SubhaloPos[*,groupfirstsub[group]]

;--------------------------------------------------
;; IDs of particles in "satellites" i.e. in subhalos of group other than the first
gal_ids = IDs[subhalooffset[groupfirstsub[group]+1]:subhalooffset[groupfirstsub[group]+groupnsubs[group]-1]+SubhaloLen[groupfirstsub[group]+groupnsubs[group]-1]-1]
gal_ids_size = (size(gal_ids))[1]
gal_exclude_id = lonarr(gal_ids_size)
gal_exclude_id[*] = 1 ;;exclude all initially
ind = sort(gal_ids)
gal_ids = gal_ids[ind]
gal_exclude_id = gal_exclude_id[ind]

gal_age = dblarr(gal_ids_size)

galind = 0L
partind = 0L
;; loop over all particles in satellites (gal_ids) and over all star particles (part_ids)
;; and include those whose IDs match i.e. get star particles in satellites
while (galind LT gal_ids_size AND partind LT part_ids_size) do begin
  if (gal_ids[galind] EQ part_ids[partind]) then begin
    gal_exclude_id[galind] = 0
    gal_age[galind] = part_age[partind]
    ++galind
    ++partind
  endif else if (gal_ids[galind] LT part_ids[partind]) then ++galind else ++partind
endwhile
gal_ids = gal_ids[where(gal_exclude_id EQ 0)] ;; now just star particles in sats
gal_age = gal_age[where(gal_exclude_id EQ 0)] ;; ages of star parts in sats
gal_ids_size = (size(gal_ids))[1]
gal_exclude_id = 0L

;; Initialise icl_ids with IDs of all particles in MAIN subhalo of group
icl_ids = IDs[subhalooffset[groupfirstsub[group]]:subhalooffset[groupfirstsub[group]]+SubhaloLen[groupfirstsub[group]]-1]
;; If include_fuzzy, add particles in group that aren't in any subhalo
if (include_fuzzy EQ 1) then icl_ids = [icl_ids, IDs[subhalooffset[groupfirstsub[group]+groupnsubs[group]-1]+SubhaloLen[groupfirstsub[group]+groupnsubs[group]-1]:grouplen[group]-1]]
ind = sort(icl_ids)
icl_ids = icl_ids[ind]
icl_ids_size = (size(icl_ids))[1]
icl_exclude_id = lonarr(icl_ids_size)
icl_exclude_id[*] = 1

icl_age = dblarr(icl_ids_size)
icl_rad = dblarr(icl_ids_size)

iclind = 0L
partind = 0L
;; Filter icl to star particles (in MAIN subhalo of group i.e. ICL + BCG)
while (iclind LT icl_ids_size AND partind LT part_ids_size) do begin
  if (icl_ids[iclind] EQ part_ids[partind]) then begin
    icl_exclude_id[iclind] = 0
    icl_age[iclind] = part_age[partind]
    icl_rad[iclind] = sqrt((part_pos[0,partind]-centerpos[0])^2+(part_pos[1,partind]-centerpos[1])^2+(part_pos[2,partind]-centerpos[2])^2) 
    ++iclind
    ++partind
  endif else if (icl_ids[iclind] LT part_ids[partind]) then ++iclind else ++partind
endwhile
icl_ids = icl_ids[where(icl_exclude_id EQ 0)]
icl_age = icl_age[where(icl_exclude_id EQ 0)]
icl_rad = icl_rad[where(icl_exclude_id EQ 0)] 
icl_ids_size = (size(icl_ids))[1]
icl_exclude_id = 0L
print, "stars in BCG+ICL = ", icl_ids_size

;; Now exclude BCG particles from BCG+ICL particles to get ICL and BCG IDs
;--------------------------------------------------
;; If cutoff method 0 or 1:
if (cd_cutoff_method LE 1) then begin
  
  is_bcg = lonarr(icl_ids_size)
  is_bcg[where(icl_rad LT cutoff_rad)] = 1
   
  bcg_ids = icl_ids[where(is_bcg EQ 1)]
  bcg_age = icl_age[where(is_bcg EQ 1)]
  bcg_ids_size = (size(bcg_ids))[1]
  
  icl_ids = icl_ids[where(is_bcg EQ 0)]
  icl_age = icl_age[where(is_bcg EQ 0)] 
  icl_ids_size = (size(icl_ids))[1]
  
  is_bcg = 0L

;; If cutoff method 2, use cd_ids calculated previously:
endif else begin
  iclind = 0L
  cdind = 0L
  icl_exclude_id = lonarr(icl_ids_size)
  icl_exclude_id[*] = 0
  while (iclind LT icl_ids_size AND cdind LT cd_ids_size) do begin
    if (icl_ids[iclind] EQ cd_ids[cdind]) then begin
      icl_exclude_id[iclind] = 1
      ++iclind
      ++cdind
    endif else if (icl_ids[iclind] LT cd_ids[cdind]) then ++iclind else ++cdind
  endwhile
  
  bcg_ids = icl_ids[where(icl_exclude_id EQ 1)]
  bcg_age = icl_age[where(icl_exclude_id EQ 1)]
  bcg_ids_size = (size(bcg_ids))[1]
  
  icl_ids = icl_ids[where(icl_exclude_id EQ 0)]
  icl_age = icl_age[where(icl_exclude_id EQ 0)]
  icl_ids_size = (size(icl_ids))[1]
  
  print, total(icl_exclude_id,/integer), " cD stars removed from ICL"
  icl_exclude_id = 0L

endelse

icl_rad = 0L

;--------------------------------------------------

all_gal_ids = gal_ids
all_gal_ids_size = gal_ids_size
all_gal_age = gal_age
all_bcg_ids = bcg_ids
all_bcg_ids_size = bcg_ids_size
all_bcg_age = bcg_age
all_icl_ids = icl_ids
all_icl_ids_size = icl_ids_size
all_icl_age = icl_age

print, "gal stars = ", all_gal_ids_size
print, "gal star fraction = ", double(all_gal_ids_size)/double(all_gal_ids_size+all_bcg_ids_size+all_icl_ids_size)
print, "bcg stars = ", all_bcg_ids_size
print, "bcg star fraction = ", double(all_bcg_ids_size)/double(all_gal_ids_size+all_bcg_ids_size+all_icl_ids_size)
print, "icl stars = ", all_icl_ids_size
print, "icl star fraction = ", double(all_icl_ids_size)/double(all_gal_ids_size+all_bcg_ids_size+all_icl_ids_size)

idsind = 0L
partind = 0L
stars_in_group = 0L
;; get IDs of all particles in group
sorted_IDs = IDs[0:grouplen[group]-1]
sorted_IDs = sorted_IDs[sort(sorted_IDs)]
ids_size = n_elements(sorted_IDs)
;; Loop over particles in group (sorted_IDs) and all star particles (part_ids)
;; to get total number of star particles in group
while (idsind LT ids_size AND partind LT part_ids_size) do begin
  if (sorted_IDs[idsind] EQ part_ids[partind]) then begin
    stars_in_group += 1
    ++idsind
    ++partind
  endif else if (sorted_IDs[idsind] LT part_ids[partind]) then ++idsind else ++partind
endwhile
sorted_IDs = 0L
print, "all stars in group = ", stars_in_group

all_part_ids = part_ids
all_part_age = part_age
all_part_ids_size = part_ids_size

part_age = 0L
part_ids = 0L
part_pos = 0L

;; Look at when and where star particles were formed
;--------------------------------------------------

numbin = 1000

snapshot = Num
show_new = 0

have_in_cache = intarr(numsnaps)
cache = bytarr(numsnaps,3,numbin+1,numbin+1)
snaptimes = dblarr(numsnaps)

skip_input = 0
dont_use_cache = 0
stopcache = -1
command = 0L

window, 0, retain=2,xsize=numbin+1,ysize=numbin+1

num_subhalos = lonarr(numsnaps)
have_descendants = intarr(numsnaps)
subhalo_descendants = ptrarr(numsnaps)
subhalo_stellar_mass = ptrarr(numsnaps)
subhalo_pos = ptrarr(numsnaps)
subhalo_vel = ptrarr(numsnaps)

halo_stellar_mass = 0
halo_dist = 0
halo_pos = 0
halo_index = -2

;; Loop over ALL snapshots except the last one
while (snapshot GT 0 AND snapshot LE numsnaps-1) do begin

this_is_cached = 1
if (have_in_cache[snapshot] NE 1 OR dont_use_cache EQ 1) then begin

if snapshot ge 1000 then begin
   exts='0000'
   exts=exts+strcompress(string(snapshot),/remove_all)
   exts=strmid(exts,strlen(exts)-4,4)
endif else begin
   exts='000'
   exts=exts+strcompress(string(snapshot),/remove_all)
   exts=strmid(exts,strlen(exts)-3,3)
endelse

;; Get star particle positions and ages sorted by their IDs
readnew,Base+"/snap_"+exts,head,"HEAD",parttype=4
readnew,Base+"/snap_"+exts,part_pos,"POS ",parttype=4
readnew,Base+"/snap_"+exts,part_ids,"ID  ",parttype=4
readnew,Base+"/snap_"+exts,part_age,"AGE ",parttype=4
ind = sort(part_ids)
part_ids = part_ids[ind]
part_pos = part_pos[*,ind]
part_age = part_age[ind]
part_ids_size = (size(part_ids))[1]

snaptimes[snapshot] = head.time

;; If there are BH particles, read in their positions to bh_pos
if (head.npart[5] GT 0) then readnew, Base+"/snap_"+exts,bh_pos,"POS ",parttype=5

;; find star particles that will be found in SAT, BCG, or ICL component later on in the specified snapshot
galind = 0L
partind = 0L
include_id = lonarr(part_ids_size)
cur_gal_ids = ulonarr(part_ids_size)
;; use gal_ids from before = IDs of star particles in satellites at late time in specified snapshot (num)
;; get star particles in current snapshot (cur_gal_ids) that will end up in satellites in snapshot num 
while (galind LT gal_ids_size AND partind LT part_ids_size) do begin
  if (gal_ids[galind] EQ part_ids[partind]) then begin
    include_id[partind] = 1
    cur_gal_ids[partind] = gal_ids[galind]
    ++galind
    ++partind
  endif else if (gal_ids[galind] LT part_ids[partind]) then ++galind else ++partind
endwhile
ind = where(include_id EQ 1)
gal_pos = part_pos[*,ind]
cur_gal_age = part_age[ind] ;;ages of star particles in current snapshot that will end up in a satellite in later snapshot num
cur_gal_ids = cur_gal_ids[ind]
cur_gal_ids_size = n_elements(cur_gal_ids)

;; Similarly for BCG and ICL
;; Get star particles in current snapshot that will end up in BCG and ICL in later snapshot (num)
if (bcg_ids_size GE 1) then begin
bcgind = 0L
partind = 0L
include_id = lonarr(part_ids_size)
cur_bcg_ids = ulonarr(part_ids_size)
while (bcgind LT bcg_ids_size AND partind LT part_ids_size) do begin
  if (bcg_ids[bcgind] EQ part_ids[partind]) then begin
    include_id[partind] = 1
    cur_bcg_ids[partind] = bcg_ids[bcgind]
    ++bcgind
    ++partind
  endif else if (bcg_ids[bcgind] LT part_ids[partind]) then ++bcgind else ++partind
endwhile
ind = where(include_id EQ 1)
if (ind[0] GE 0) then begin
  bcg_pos = part_pos[*,ind]
  cur_bcg_age = part_age[ind]
  cur_bcg_ids = cur_bcg_ids[ind]
  cur_bcg_ids_size = n_elements(cur_bcg_ids)
endif else begin
  bcg_pos = 0L
  cur_bcg_age = -1
  cur_bcg_ids = 0L
  cur_bcg_ids_size = 0
endelse
endif

iclind = 0L
partind = 0L
include_id = lonarr(part_ids_size)
cur_icl_ids = ulonarr(part_ids_size)
while (iclind LT icl_ids_size AND partind LT part_ids_size) do begin
  if (icl_ids[iclind] EQ part_ids[partind]) then begin
    include_id[partind] = 1
    cur_icl_ids[partind] = icl_ids[iclind]
    ++iclind
    ++partind
  endif else if (icl_ids[iclind] LT part_ids[partind]) then ++iclind else ++partind
endwhile
ind = where(include_id EQ 1)
icl_pos = part_pos[*,ind]
cur_icl_age = part_age[ind]
cur_icl_ids = cur_icl_ids[ind]
cur_icl_ids_size = n_elements(cur_icl_ids)

; find gas particles that get later converted into SAT, BCG, or ICL stars
; note: if only the second star produced from the gas particle is part of one of the components the gas particle is not found
if (check_before EQ 1 AND snapshot LT Num) then begin ;earlier snapshot
  ;; read in positions of all gas particles
  readnew,Base+"/snap_"+exts,gas_pos,"POS ",parttype=0
  readnew,Base+"/snap_"+exts,gas_ids,"ID  ",parttype=0
  ind = sort(gas_ids)
  gas_ids = gas_ids[ind]
  gas_pos = gas_pos[*,ind]
  gas_ids_size = (size(gas_ids))[1]

  galind = 0L
  gasind = 0L
  include_id = lonarr(gas_ids_size)
  gas_age = fltarr(gas_ids_size)
  gas_age[*] = -1
  ;; Apparently star particles are given the same ID as the gas particle
  ;; from which they formed so can find parent gas particles by ID:
  while (galind LT gal_ids_size AND gasind LT gas_ids_size) do begin
    if (gal_ids[galind] EQ gas_ids[gasind]) then begin
      include_id[gasind] = 1
      gas_age[gasind] = gal_age[galind]
      ++galind
      ++gasind
    endif else if (gal_ids[galind] LT gas_ids[gasind]) then ++galind else ++gasind
  endwhile
  ind = where(include_id EQ 1)
  if (ind[0] GE 0) then begin
    gal_gas_pos = gas_pos[*,where(include_id EQ 1)]
    cur_gal_gas_age = gas_age[where(include_id EQ 1)]
  endif else begin
    gal_gas_pos = 0L
    cur_gal_gas_age = -1
  endelse
  gas_age = 0L

  ;; as above but for BCG stars
  bcgind = 0L
  gasind = 0L
  include_id = lonarr(gas_ids_size)
  gas_age = fltarr(gas_ids_size)
  gas_age[*] = -1
  while (bcgind LT bcg_ids_size AND gasind LT gas_ids_size) do begin
    if (bcg_ids[bcgind] EQ gas_ids[gasind]) then begin
      include_id[gasind] = 1
      gas_age[gasind] = bcg_age[bcgind]
      ++bcgind
      ++gasind
    endif else if (bcg_ids[bcgind] LT gas_ids[gasind]) then ++bcgind else ++gasind
  endwhile
  ind = where(include_id EQ 1)
  if (ind[0] GE 0) then begin
    bcg_gas_pos = gas_pos[*,where(include_id EQ 1)]
    cur_bcg_gas_age = gas_age[where(include_id EQ 1)]
  endif else begin
    bcg_gas_pos = 0L
    cur_bcg_gas_age = -1
  endelse
  gas_age = 0L

  ;; as above but for ICL stars
  iclind = 0L
  gasind = 0L
  include_id = lonarr(gas_ids_size)
  gas_age = fltarr(gas_ids_size)
  gas_age[*] = -1
  while (iclind LT icl_ids_size AND gasind LT gas_ids_size) do begin
    if (icl_ids[iclind] EQ gas_ids[gasind]) then begin
      include_id[gasind] = 1
      gas_age[gasind] = icl_age[iclind]
      ++iclind
      ++gasind
    endif else if (icl_ids[iclind] LT gas_ids[gasind]) then ++iclind else ++gasind
  endwhile
  ind = where(include_id EQ 1)
  if (ind[0] GE 0) then begin
    icl_gas_pos = gas_pos[*,where(include_id EQ 1)]
    cur_icl_gas_age = gas_age[where(include_id EQ 1)]
  endif else begin
    icl_gas_pos = 0L
    cur_icl_gas_age = -1
  endelse
  gas_age = 0L

  gas_ids = 0L
  gas_pos = 0L
endif

;plot, part_pos[0,*], part_pos[1,*], xrange=[centerpos[0]-1.5,centerpos[0]+1.5], yrange=[centerpos[1]-1.5,centerpos[1]+1.5], psym=3, xstyle=1, ystyle=1
;oplot, icl_pos[0,*], icl_pos[1,*], psym=3, color='0000FF'x
;oplot, gal_pos[0,*], gal_pos[1,*], psym=3, color='FF0000'x

;; Start plotting onto field of length fieldsize centred on group position
fieldsize = 4.0D ;(Mpc)
xmin = centerpos[0]-0.5*fieldsize
xmax = centerpos[0]+0.5*fieldsize
ymin = centerpos[1]-0.5*fieldsize
ymax = centerpos[1]+0.5*fieldsize
xbin = fieldsize/numbin
ybin = fieldsize/numbin

;; Bin particle positions onto grid of size numbin x numbin
part_img = hist_2d(part_pos[0,*],part_pos[1,*],bin1=xbin,bin2=ybin,min1=xmin,min2=ymin,max1=xmax,max2=ymax)
gal_img = hist_2d(gal_pos[0,*],gal_pos[1,*],bin1=xbin,bin2=ybin,min1=xmin,min2=ymin,max1=xmax,max2=ymax)
if (bcg_ids_size GE 1 AND cur_bcg_age[0] GE 0) then bcg_img = hist_2d(bcg_pos[0,*],bcg_pos[1,*],bin1=xbin,bin2=ybin,min1=xmin,min2=ymin,max1=xmax,max2=ymax)$
else bcg_img = lonarr(numbin+1,numbin+1) 
icl_img = hist_2d(icl_pos[0,*],icl_pos[1,*],bin1=xbin,bin2=ybin,min1=xmin,min2=ymin,max1=xmax,max2=ymax)

if (check_before EQ 0) then begin ;get new stars
  ind = where(cur_gal_age GT head.time*0.95) ;; stars formed recently (age must actually be formation time (0 to 1))
  if (ind[0] GE 0) then gal_new_img = hist_2d(gal_pos[0,ind],gal_pos[1,ind],bin1=xbin,bin2=ybin,min1=xmin,min2=ymin,max1=xmax,max2=ymax) else gal_new_img = lonarr(numbin+1,numbin+1)

  if (bcg_ids_size GE 1) then begin
    ind = where(cur_bcg_age GT head.time*0.95)
    if (ind[0] GE 0) then bcg_new_img = hist_2d(bcg_pos[0,ind],bcg_pos[1,ind],bin1=xbin,bin2=ybin,min1=xmin,min2=ymin,max1=xmax,max2=ymax) else bcg_new_img = lonarr(numbin+1,numbin+1)
  endif else bcg_new_img = lonarr(numbin+1,numbin+1) 

  ind = where(cur_icl_age GT head.time*0.95) 
  if (ind[0] GE 0) then icl_new_img = hist_2d(icl_pos[0,ind],icl_pos[1,ind],bin1=xbin,bin2=ybin,min1=xmin,min2=ymin,max1=xmax,max2=ymax) else icl_new_img =  lonarr(numbin+1,numbin+1)
endif else begin
if (snapshot LT Num) then begin ;get pre-formation gas particles
  if (cur_gal_gas_age[0] GE 0)then begin
    ind = where(cur_gal_gas_age LT head.time*1.05) ;; gas_age is age of star it produces later
    if (ind[0] GE 0) then gal_new_img = hist_2d(gal_gas_pos[0,ind],gal_gas_pos[1,ind],bin1=xbin,bin2=ybin,min1=xmin,min2=ymin,max1=xmax,max2=ymax) else gal_new_img = lonarr(numbin+1,numbin+1)
  endif else gal_new_img = lonarr(numbin+1,numbin+1) 

  if (bcg_ids_size GE 1 AND cur_bcg_gas_age[0] GE 0) then begin
    ind = where(cur_bcg_gas_age LT head.time*1.05)
    if (ind[0] GE 0) then bcg_new_img = hist_2d(bcg_gas_pos[0,ind],bcg_gas_pos[1,ind],bin1=xbin,bin2=ybin,min1=xmin,min2=ymin,max1=xmax,max2=ymax) else bcg_new_img = lonarr(numbin+1,numbin+1)
  endif else bcg_new_img = lonarr(numbin+1,numbin+1) 

  ind = where(cur_icl_gas_age LT head.time*1.05)
  ;ind = where(cur_icl_gas_age LT 0.6223 AND cur_icl_gas_age GE 0.5927)
  if (ind[0] GE 0) then icl_new_img = hist_2d(icl_gas_pos[0,ind],icl_gas_pos[1,ind],bin1=xbin,bin2=ybin,min1=xmin,min2=ymin,max1=xmax,max2=ymax) else icl_new_img =  lonarr(numbin+1,numbin+1)
endif
endelse

;; if black holes
if (head.npart[5] GT 0) then begin
  bh_img = hist_2d(bh_pos[0,*],bh_pos[1,*],bin1=xbin,bin2=ybin,min1=xmin,min2=ymin,max1=xmax,max2=ymax)
  bh_img_cross = lonarr(numbin+1,numbin+1) ;;initialise to zero
  for i=0,numbin do begin
  for j=0,numbin do begin
    if (bh_img[i,j] GT 0) then begin
      ;; for every 2D bin with a BH fill bh_img_cross with a 5x5 cross of 1's centred on the BH bin
      bh_img_cross[i,max([j-2,0]):min([j+2,numbin])] = 1
      bh_img_cross[max([i-2,0]):min([i+2,numbin]),j] = 1
    endif
  endfor
  endfor
endif
bh_img = 0L

;; Similarly for halos but with crosses of 15x15 bins
if (halo_index[0] GT -2) then if (halo_index[snapshot] GE 0) then begin
  halo_img = hist_2d([halo_pos[0,snapshot],-10],[halo_pos[1,snapshot],-10],bin1=xbin,bin2=ybin,min1=xmin,min2=ymin,max1=xmax,max2=ymax)
  halo_img_cross = lonarr(numbin+1,numbin+1)
  for i=0,numbin do begin
  for j=0,numbin do begin
    if (halo_img[i,j] GT 0) then begin
      halo_img_cross[i,max([j-7,0]):min([j+7,numbin])] = 1
      halo_img_cross[max([i-7,0]):min([i+7,numbin]),j] = 1
    endif
  endfor
  endfor
endif
halo_img = 0L

red_img = lonarr(numbin+1,numbin+1)
green_img = lonarr(numbin+1,numbin+1)
blue_img = lonarr(numbin+1,numbin+1) 

red_img[where(part_img GT 0)]=255
green_img[where(part_img GT 0)]=255
blue_img[where(part_img GT 0)]=255

red_img[where(icl_img GT 0)]=255
green_img[where(icl_img GT 0)]=0
blue_img[where(icl_img GT 0)]=0

red_img[where(gal_img GT 0)]=0
green_img[where(gal_img GT 0)]=0
blue_img[where(gal_img GT 0)]=255

if (bcg_ids_size GE 1) then begin
ind = where(bcg_img GT 0)
if (ind[0] GE 0) then begin
  red_img[where(bcg_img GT 0)]=15
  green_img[where(bcg_img GT 0)]=107
  blue_img[where(bcg_img GT 0)]=23
endif
endif

if (show_new EQ 1 AND (check_before EQ 0 OR snapshot LT Num)) then begin
  red_img[where(icl_new_img GT 0)]=251 ;yellow
  green_img[where(icl_new_img GT 0)]=247
  blue_img[where(icl_new_img GT 0)]=0

  red_img[where(gal_new_img GT 0)]=0 ;cyan
  green_img[where(gal_new_img GT 0)]=255
  blue_img[where(gal_new_img GT 0)]=255
endif

;if(head.npart[5] GT 0) then begin
;  red_img[where(bh_img_cross GT 0)]=255
;  green_img[where(bh_img_cross GT 0)]=0
;  blue_img[where(bh_img_cross GT 0)]=238
;endif

if (halo_index[0] GT -2) then if (halo_index[snapshot] GE 0) then begin
  red_img[where(halo_img_cross GT 0)]=0
  green_img[where(halo_img_cross GT 0)]=255
  blue_img[where(halo_img_cross GT 0)]=0
endif

; red_img[*,numbin]=255
; red_img[numbin,*]=255
; green_img[*,numbin]=255
; green_img[numbin,*]=255
; blue_img[*,numbin]=255
; blue_img[numbin,*]=255

comp_img = bytarr(3,numbin+1,numbin+1)
comp_img[0,*,*] = red_img
comp_img[1,*,*] = green_img
comp_img[2,*,*] = blue_img

cache[snapshot,*,*,*] = comp_img
have_in_cache[snapshot] = 1

this_is_cached = 0
dont_use_cache = 0

endif else comp_img = reform(cache[snapshot,*,*,*])

tv,comp_img,true=1
print,"current snapshot = ",snapshot, ", time = ",snaptimes[snapshot]

repeat begin

;; Command to select a single subhalo
if (command EQ -6) then begin
  gal_dist = sqrt((gal_pos[0,*]-SubhaloPos[0,mousesub])^2+(gal_pos[1,*]-SubhaloPos[1,mousesub])^2+(gal_pos[2,*]-SubhaloPos[2,mousesub])^2)*head.time
  if (cur_bcg_age[0] GE 0) then bcg_dist = sqrt((bcg_pos[0,*]-SubhaloPos[0,mousesub])^2+(bcg_pos[1,*]-SubhaloPos[1,mousesub])^2+(bcg_pos[2,*]-SubhaloPos[2,mousesub])^2)*head.time
  icl_dist = sqrt((icl_pos[0,*]-SubhaloPos[0,mousesub])^2+(icl_pos[1,*]-SubhaloPos[1,mousesub])^2+(icl_pos[2,*]-SubhaloPos[2,mousesub])^2)*head.time

  gal_dist_dist = histogram(gal_dist/SubhaloHalfmassRad[mousesub],binsize=0.1,min=0.0,max=5.0,LOCATIONS=dist_dists)
  if (cur_bcg_age[0] GE 0) then bcg_dist_dist = histogram(bcg_dist/SubhaloHalfmassRad[mousesub],binsize=0.1,min=0.0,max=5.0) 
  icl_dist_dist = histogram(icl_dist/SubhaloHalfmassRad[mousesub],binsize=0.1,min=0.0,max=5.0)
  dist_dists = dist_dists+0.5*0.1
  gal_dist = 0L
  bcg_dist = 0L
  icl_dist = 0L

  window, 1, retain = 2;, xsize=640, ysize=1024
  !p.multi=[0,1,3]
  plot, dist_dists, gal_dist_dist, /nodata, background='FFFFFF'x, color=0
  oplot, dist_dists, gal_dist_dist, psym=10, color='FF0000'x
  if (cur_bcg_age[0] GE 0) then oplot, dist_dists, bcg_dist_dist, psym=10, color='0F6B17'x
  oplot, dist_dists, icl_dist_dist, psym=10, color='0000FF'x
  ind = where(halo_index GE 1)
  if (ind[0] GE 0) then begin
    plot, snaptimes[ind], halo_stellar_mass[ind], xrange=[0.0,1.0], background='FFFFFF'x, color=0

    halo_pos_fine = dblarr(3,(numsnaps-1)*32+1)
    main_pos_fine = dblarr(3,(numsnaps-1)*32+1)
    snaptimes_fine = dblarr((numsnaps-1)*32+1)
    halo_dist_fine = dblarr((numsnaps-1)*32+1)
    for i=0,numsnaps-2 do begin
      if (halo_index[i] GE 1 AND halo_index[i+1] GE 1) then begin
        time1 = 0.0D
        time2 = time(snaptimes[i],snaptimes[i+1])
        for j=0,31 do begin
          cur_time = time(snaptimes[i],snaptimes[i]+double(j)/double(32)*(snaptimes[i+1]-snaptimes[i]))
          snaptimes_fine[i*32+j] = snaptimes[i]+double(j)/double(32)*(snaptimes[i+1]-snaptimes[i])
          halo_pos_fine[0,i*32+j] = interpolate_orbit(halo_pos[0,i],halo_vel[0,i],time1,halo_pos[0,i+1],halo_vel[0,i+1],time2,cur_time)
          halo_pos_fine[1,i*32+j] = interpolate_orbit(halo_pos[1,i],halo_vel[1,i],time1,halo_pos[1,i+1],halo_vel[1,i+1],time2,cur_time)
          halo_pos_fine[2,i*32+j] = interpolate_orbit(halo_pos[2,i],halo_vel[2,i],time1,halo_pos[2,i+1],halo_vel[2,i+1],time2,cur_time)
          main_pos_fine[0,i*32+j] = interpolate_orbit(main_pos[0,i],main_vel[0,i],time1,main_pos[0,i+1],main_vel[0,i+1],time2,cur_time)
          main_pos_fine[1,i*32+j] = interpolate_orbit(main_pos[1,i],main_vel[1,i],time1,main_pos[1,i+1],main_vel[1,i+1],time2,cur_time)
          main_pos_fine[2,i*32+j] = interpolate_orbit(main_pos[2,i],main_vel[2,i],time1,main_pos[2,i+1],main_vel[2,i+1],time2,cur_time)
          halo_dist_fine[i*32+j] = sqrt(total((halo_pos_fine[*,i*32+j]-main_pos_fine[*,i*32+j])^2,/double))
        endfor
      endif
    endfor
    if (halo_index[numsnaps-1] GE 1) then begin
      snaptimes_fine[(numsnaps-1)*32] = snaptimes[numsnaps-1]
      halo_pos_fine[*,(numsnaps-1)*32] = halo_pos[*,numsnaps-1]
      main_pos_fine[*,(numsnaps-1)*32] = main_pos[*,numsnaps-1]
      halo_dist_fine[(numsnaps-1)*32] = sqrt(total((halo_pos_fine[*,(numsnaps-1)*32]-main_pos_fine[*,(numsnaps-1)*32])^2,/double)) 
    endif 

    plot, snaptimes[ind], halo_dist[ind], xrange=[0.0,1.0], background='FFFFFF'x, color=0
    ind = where(snaptimes_fine GT 0.0D)
    if (ind[0] GE 0) then oplot, snaptimes_fine[ind], halo_dist_fine[ind], color=255  
  endif
  wset, 0
  !p.multi=[0,1,1]

  binding_egys_file = Base + "/" + lsubdir + exts + "/binding_egys.dat"   
  if (GroupFirstsub[SubhaloGrNr[mousesub]] EQ mousesub AND FILE_TEST(binding_egys_file) EQ 1) then begin
    print, "loading binding energies from ", binding_egys_file
    mousegr = SubhaloGrNr[mousesub]
    
    openr, 41, binding_egys_file
    curgr = 0L
    egys_in_gr = 0L
    num_gr_in_file = 0L
    readu,41,num_gr_in_file 
    REPEAT BEGIN
      readu,41,curgr,egys_in_gr
      curgr_ids = 0L
      curgr_binding_egys = 0.0D 
      if (egys_in_gr GE 1) then begin
        curgr_ids = ulonarr(egys_in_gr)
        curgr_binding_egys = fltarr(egys_in_gr)
        thisid = ulong(0)
        this_binding_egy = float(0) 
        for i=0L,egys_in_gr-1 do begin
          readu,41,thisid,this_binding_egy
          curgr_ids[i] = thisid
          curgr_binding_egys[i] = this_binding_egy
        endfor
      endif      
    ENDREP UNTIL (curgr EQ mousegr)
    close,41
    
    if (egys_in_gr GE 1) then begin
      galind = 0L
      egyind = 0L
      gal_binding_egys = fltarr(cur_gal_ids_size)
      gal_binding_egys[*] = 1.0e10
      gal_egy_found = 0L
      while (galind LT cur_gal_ids_size AND egyind LT egys_in_gr) do begin
        if (cur_gal_ids[galind] EQ curgr_ids[egyind]) then begin
          gal_binding_egys[galind] = curgr_binding_egys[egyind]
          ++galind
          ++egyind
          ++gal_egy_found
        endif else if (cur_gal_ids[galind] LT curgr_ids[egyind]) then ++galind else ++egyind
      endwhile
      print, "found binding energy for ", 100.0D*double(gal_egy_found)/double(cur_gal_ids_size), " % of galaxy stars" 

      bcgind = 0L
      egyind = 0L
      bcg_binding_egys = fltarr(cur_bcg_ids_size)
      bcg_binding_egys[*] = 1.0e10
      bcg_egy_found = 0L
      while (bcgind LT cur_bcg_ids_size AND egyind LT egys_in_gr) do begin
        if (cur_bcg_ids[bcgind] EQ curgr_ids[egyind]) then begin
          bcg_binding_egys[bcgind] = curgr_binding_egys[egyind]
          ++bcgind
          ++egyind
          ++bcg_egy_found
        endif else if (cur_bcg_ids[bcgind] LT curgr_ids[egyind]) then ++bcgind else ++egyind
      endwhile
      print, "found binding energy for ", 100.0D*double(bcg_egy_found)/double(cur_bcg_ids_size), " % of BCG stars"

      iclind = 0L
      egyind = 0L
      icl_binding_egys = fltarr(cur_icl_ids_size)
      icl_binding_egys[*] = 1.0e10
      icl_egy_found = 0L
      while (iclind LT cur_icl_ids_size AND egyind LT egys_in_gr) do begin
        if (cur_icl_ids[iclind] EQ curgr_ids[egyind]) then begin
          icl_binding_egys[iclind] = curgr_binding_egys[egyind]
          ++iclind
          ++egyind
          ++icl_egy_found
        endif else if (cur_icl_ids[iclind] LT curgr_ids[egyind]) then ++iclind else ++egyind
      endwhile
      print, "found binding energy for ", 100.0D*double(icl_egy_found)/double(cur_icl_ids_size), " % of ICL stars"

      curgr_ids = 0L
      curgr_binding_egys = 0.0D

      if ((gal_egy_found GE 1 OR bcg_egy_found GE 1) AND icl_egy_found GE 1) then begin
        if (gal_egy_found GE 1) then gal_binding_egys = gal_binding_egys[where(gal_binding_egys LT 1.0e9)]
        if (bcg_egy_found GE 1) then bcg_binding_egys = bcg_binding_egys[where(bcg_binding_egys LT 1.0e9)] 
        icl_binding_egys = icl_binding_egys[where(icl_binding_egys LT 1.0e9)]
        egymin = min([min(gal_binding_egys),min(bcg_binding_egys),min(icl_binding_egys)])

        icl_bind_dist = histogram(icl_binding_egys/egymin,binsize=0.1,min=0.0,max=1.2,LOCATIONS=dist_binds)
        if (gal_egy_found GE 1) then gal_bind_dist = histogram(gal_binding_egys/egymin,binsize=0.1,min=0.0,max=1.2)$
        else gal_bind_dist = lonarr(n_elements(icl_bind_dist))
        if (bcg_egy_found GE 1) then bcg_bind_dist = histogram(bcg_binding_egys/egymin,binsize=0.1,min=0.0,max=1.2)$
        else bcg_bind_dist = lonarr(n_elements(icl_bind_dist))
        dist_binds = dist_binds+0.5*0.1
        gal_binding_egys = 0L
        bcg_binding_egys = 0L
        icl_binding_egys = 0L
        if (gal_bind_dist[10] LE 2) then begin
          gal_bind_dist[9] += gal_bind_dist[10]
          gal_bind_dist[10] = 0
        endif
        if (bcg_bind_dist[10] LE 2) then begin
          bcg_bind_dist[9] += bcg_bind_dist[10]
          bcg_bind_dist[10] = 0
        endif
        if (icl_bind_dist[10] LE 2) then begin
          icl_bind_dist[9] += icl_bind_dist[10]
          icl_bind_dist[10] = 0
        endif

        window, 2, retain = 2
        plot, dist_binds, double(icl_bind_dist)/double(icl_bind_dist+gal_bind_dist+bcg_bind_dist), psym=10, yrange=[0.0,1.2] ;/nodata, background='FFFFFF'x, color=0
        ;oplot, dist_binds, gal_bind_dist, psym=10, color='FF0000'x
        ;oplot, dist_binds, icl_bind_dist, psym=10, color='0000FF'x 
        wset, 0

        gal_bind_dist = 0L
        bcg_bind_dist = 0L
        icl_bind_dist = 0L
      endif
    endif
  endif

endif ;; end command -6 (Select subhalo)

old_command = command

if (skip_input EQ 0) then begin
command = 0L
print, "snapnum = ? (0 to exit, -1 to show gals, -2 to show icl, -3 to show new gals, -4 to show new icl, -5 add/remove new stars, -6 select subhalo, -7 deselect subhalo, -8 cache images, -9 use +/- to navigate, -10 get galaxy merger tree, -11 save infalling gal props, -12 show all DM, -13 change center, -14 change sidelength)"
read, command
endif else skip_input = 0

if (command GE 0) then snapshot = command
;--------------------------------------------
;; Command -1, -2, -3, -4 (show old/new gals/icl)
if ((command GE -4 AND command LE -1) OR command EQ -12) then begin
  if (this_is_cached EQ 0) then begin
    if (command EQ -1) then tvscl,alog10(gal_img),/nan
    if (command EQ -2) then tvscl,alog10(icl_img),/nan
    if (command EQ -3) then tvscl,alog10(gal_new_img),/nan
    if (command EQ -4) then tvscl,alog10(icl_new_img),/nan
    if (command EQ -12) then begin
      readnew,Base+"/snap_"+exts,dm_pos,"POS ",parttype=0
      dm_img = hist_2d(dm_pos[0,*],dm_pos[1,*],bin1=xbin,bin2=ybin,min1=xmin,min2=ymin,max1=xmax,max2=ymax)
      tvscl,alog10(dm_img),/nan
      dm_pos = 0L
      dm_img = 0L
    endif
    snapshot = command
  endif else begin
    dont_use_cache = 1
    skip_input = 1
  endelse
endif
;--------------------------------------------
if (command EQ -5) then begin
  have_in_cache = intarr(numsnaps)
  cache = bytarr(numsnaps,3,numbin+1,numbin+1) 
  
  if (show_new EQ 0) then show_new=1 else show_new=0
endif
;--------------------------------------------
if (command EQ -6) then begin
  have_in_cache = intarr(numsnaps)
  cache = bytarr(numsnaps,3,numbin+1,numbin+1)

  cursor,mousex,mousey,/down, /DEVICE
  mousex = mousex*xbin + xmin
  mousey = mousey*ybin + ymin
  print, "looking for suhbalo at x = ", mousex, ", y = ", mousey

  skip = 0L
  skip_sub = 0L
  fnr = 0L

  repeat begin

    f = Base + "/groups_" + exts +"/subhalo_tab_"+exts +"."+strcompress(string(fnr),/remove_all)

    Ngroups = 0L
    TotNgroups = 0L
    Nids = 0L
    TotNids = 0LL
    NTask = 0L
    Nsubgroups = 0L
    TotNsubgroups = 0L

    openr,1,f
    readu,1, Ngroups, TotNgroups, Nids, TotNids, NTask, Nsubgroups, TotNsubgroups

    if fnr eq 0 then begin
        GroupLen = lonarr(TotNgroups)
        GroupOffset = lonarr(TotNgroups)
        GroupMass = fltarr(TotNgroups)
        GroupPos = fltarr(3, TotNgroups)
        Group_M_Mean200 = fltarr(TotNgroups)
        Group_R_Mean200 = fltarr(TotNgroups)
        Group_M_Crit200 = fltarr(TotNgroups)
        Group_R_Crit200 = fltarr(TotNgroups)
        Group_M_TopHat200 = fltarr(TotNgroups)
        Group_R_TopHat200 = fltarr(TotNgroups)
        Group_VelDisp_Mean200 = fltarr(TotNgroups)
        Group_VelDisp_Crit200 = fltarr(TotNgroups)
        Group_VelDisp_TopHat200 = fltarr(TotNgroups)
        GroupContaminationCount = lonarr(TotNgroups)
        GroupContaminationMass = fltarr(TotNgroups)
        GroupNsubs = lonarr(TotNgroups)
        GroupFirstSub = lonarr(TotNgroups)

        SubhaloLen = lonarr(TotNsubgroups)
        SubhaloOffset = lonarr(TotNsubgroups)
        SubhaloParent = lonarr(TotNsubgroups)
        SubhaloMass = fltarr(TotNsubgroups)
        SubhaloPos = fltarr(3, TotNsubgroups)
        SubhaloVel = fltarr(3, TotNsubgroups)
        SubhaloCM = fltarr(3, TotNsubgroups)
        SubhaloSpin = fltarr(3, TotNsubgroups)
        SubhaloVelDisp = fltarr(TotNsubgroups)
        SubhaloVmax = fltarr(TotNsubgroups)
        SubhaloVmaxRad = fltarr(TotNsubgroups)
        SubhaloHalfmassRad = fltarr(TotNsubgroups)
        SubhaloIDMostbound = lonarr(TotNsubgroups)
        SubhaloGrNr = lonarr(TotNsubgroups)
    endif

    if Ngroups gt 0 then begin
        
        locLen = lonarr(Ngroups)
        locOffset = lonarr(Ngroups)
        locMass = fltarr(Ngroups)
        locPos = fltarr(3, Ngroups)
        loc_M_Mean200 = fltarr(Ngroups)
        loc_R_Mean200 = fltarr(Ngroups)
        loc_M_Crit200 = fltarr(Ngroups)
        loc_R_Crit200 = fltarr(Ngroups)
        loc_M_TopHat200 = fltarr(Ngroups)
        loc_R_TopHat200 = fltarr(Ngroups)
        loc_VelDisp_Mean200 = fltarr(Ngroups)
        loc_VelDisp_Crit200 = fltarr(Ngroups)
        loc_VelDisp_TopHat200 = fltarr(Ngroups)
        locContaminationCount = lonarr(Ngroups)
        locContaminationMass = fltarr(Ngroups)
        locNsubs = lonarr(Ngroups)
        locFirstSub = lonarr(Ngroups)

        readu,1, loclen 
        readu,1, locOffset
        readu,1, locMass
        readu,1, locPos
        readu,1, loc_M_Mean200
        readu,1, loc_R_Mean200
        readu,1, loc_M_Crit200
        readu,1, loc_R_Crit200
        readu,1, loc_M_TopHat200
        readu,1, loc_R_TopHat200
        if FLAG_Group_VelDisp ne 0 then begin
          readu,1, loc_VelDisp_Mean200
          readu,1, loc_VelDisp_Crit200
          readu,1, loc_VelDisp_TopHat200
        endif
        readu,1, locContaminationCount
        readu,1, locContaminationMass
        readu,1, locNsubs
        readu,1, locFirstSub

        GroupLen(skip:skip+Ngroups-1) = locLen(*)
        GroupOffset(skip:skip+Ngroups-1) = locOffset(*)
        GroupMass(skip:skip+Ngroups-1) = locMass(*)
        GroupPos(*, skip:skip+Ngroups-1) = locPos(*,*)
        Group_M_Mean200(skip:skip+Ngroups-1) = loc_M_Mean200(*)
        Group_R_Mean200(skip:skip+Ngroups-1) = loc_R_Mean200(*)
        Group_M_Crit200(skip:skip+Ngroups-1) = loc_M_Crit200(*)
        Group_R_Crit200(skip:skip+Ngroups-1) = loc_R_Crit200(*)
        Group_M_TopHat200(skip:skip+Ngroups-1) = loc_M_TopHat200(*)
        Group_R_TopHat200(skip:skip+Ngroups-1) = loc_R_TopHat200(*)
        Group_VelDisp_Mean200(skip:skip+Ngroups-1) = loc_VelDisp_Mean200(*)
        Group_VelDisp_Crit200(skip:skip+Ngroups-1) = loc_VelDisp_Crit200(*)
        Group_VelDisp_TopHat200(skip:skip+Ngroups-1) = loc_VelDisp_TopHat200(*)
        GroupContaminationCount(skip:skip+Ngroups-1) = locContaminationCount(*) 
        GroupContaminationMass(skip:skip+Ngroups-1) = locContaminationMass(*)
        GroupNsubs(skip:skip+Ngroups-1) = locNsubs(*)
        GroupFirstsub(skip:skip+Ngroups-1) = locFirstSub(*)

        skip+= Ngroups
    endif


    if Nsubgroups gt 0 then begin
        
        locLen = lonarr(Nsubgroups)
        locOffset = lonarr(Nsubgroups)
        locParent = lonarr(Nsubgroups)
        locMass = fltarr(Nsubgroups)
        locPos = fltarr(3, Nsubgroups)
        locVel = fltarr(3, Nsubgroups)
        locCM = fltarr(3, Nsubgroups)
        locSpin= fltarr(3, Nsubgroups)
        locVelDisp = fltarr(Nsubgroups)
        locVmax = fltarr(Nsubgroups)
        locVmaxRad = fltarr(Nsubgroups)
        locHalfMassRad = fltarr(Nsubgroups)
        locIDMostBound = lonarr(Nsubgroups)
        locGrNr = lonarr(Nsubgroups)

        readu,1, loclen 
        readu,1, locOffset
        readu,1, locParent
        readu,1, locMass
        readu,1, locPos
        readu,1, locVel
        readu,1, locCM
        readu,1, locSpin
        readu,1, locVelDisp
        readu,1, locVmax
        readu,1, locVmaxRad
        readu,1, locHalfMassRad
        readu,1, locIDMostBound
        readu,1, locGrNr

        SubhaloLen(skip_sub:skip_sub+Nsubgroups-1) = locLen(*)
        SubhaloOffset(skip_sub:skip_sub+Nsubgroups-1) = locOffset(*)
        SubhaloParent(skip_sub:skip_sub+Nsubgroups-1) = locParent(*)
        SubhaloMass(skip_sub:skip_sub+Nsubgroups-1) = locMass(*)
        SubhaloPos(*, skip_sub:skip_sub+Nsubgroups-1) = locPos(*,*)
        SubhaloVel(*, skip_sub:skip_sub+Nsubgroups-1) = locVel(*,*)
        SubhaloCM(*, skip_sub:skip_sub+Nsubgroups-1) = locCM(*,*)
        SubhaloSpin(*, skip_sub:skip_sub+Nsubgroups-1) = locSpin(*,*)
        SubhaloVeldisp(skip_sub:skip_sub+Nsubgroups-1) = locVeldisp(*)
        SubhaloVmax(skip_sub:skip_sub+Nsubgroups-1) = locVmax(*)
        SubhaloVmaxRad(skip_sub:skip_sub+Nsubgroups-1) = locVmaxRad(*)
        SubhaloHalfmassRad(skip_sub:skip_sub+Nsubgroups-1) = locHalfmassRad(*)
        SubhaloIDMostBound(skip_sub:skip_sub+Nsubgroups-1) = locIDMostBound(*)
        SubhaloGrNr(skip_sub:skip_sub+Nsubgroups-1) = locGrNr(*)

        skip_sub+= Nsubgroups
    endif

    close, 1

    fnr++

  endrep until fnr eq NTask

  print
  print, "TotNgroups   =", TotNgroups
  print, "TotNsubgroups=", TotNsubgroups
  print
  print, "Largest group of length ", GroupLen(0)," has", GroupNsubs(0)," substructures"

  sfrflag = 0
  bhflag = 0

  ;--------------------------------------------------

  skip = 0L
  fnr = 0L

  repeat begin

    f = Base + "/groups_" + exts +"/subhalo_ids_"+exts +"."+strcompress(string(fnr),/remove_all)

    idNgroups = 0L
    idTotNgroups = 0L
    idNids = 0L
    idTotNids = 0LL
    idNTask = 0L
    idOffset = 0L

    openr,1,f
    readu,1, idNgroups, idTotNgroups, idNids, idTotNids, idNTask, idOffset

    if fnr eq 0 then begin
        IDs = lonarr(idTotNids)
    endif

    if idNids gt 0 then begin
        
        locIDs = lonarr(idNids)
        readu,1, locIDs

        IDs(skip:skip+idNids-1) = locIDs(*)
        skip+= idNids

    endif

    close, 1

    fnr++

  endrep until fnr eq idNTask

  print
  print, "Total IDs in grps=", idTotNids
  print

  IDs = ulong(IDs)

  ;--------------------------------------------------
 
  subdist = (SubhaloPos[0,*]-mousex)^2+(SubhaloPos[1,*]-mousey)^2
  print, "subhalo found at projected distancs of ", min(subdist,mousesub)
  print, "FoF group = ", SubhaloGrNr[mousesub], ", subhalo = ", mousesub, ", subhalo of group = ", mousesub - GroupFirstsub[SubhaloGrNr[mousesub]]

  mouseIDs = IDs[SubhaloOffset[mousesub]:SubhaloOffset[mousesub]+SubhaloLen[mousesub]-1]
  mouseIDs = mouseIDs[sort(mouseIDs)]
  mouse_ids_size = n_elements(mouseIDs)

  ;if snapshot ge 1000 then begin
  ;  exts='0000'
  ;  exts=exts+strcompress(string(snapshot),/remove_all)
  ;  exts=strmid(exts,strlen(exts)-4,4)
  ;endif else begin
  ;  exts='000'
  ;  exts=exts+strcompress(string(snapshot),/remove_all)
  ;  exts=strmid(exts,strlen(exts)-3,3)
  ;endelse

  ;readnew,Base+"/snap_"+exts,part_ids,"ID  ",parttype=4
  ;ind = sort(part_ids)
  ;part_ids = part_ids[ind]
  
  part_ids = all_part_ids

  part_ids_size = (size(part_ids))[1]

  ; select particles from subhalo that are stars at redshift 0
  partind = 0L
  mouseind = 0L
  include_id = lonarr(mouse_ids_size)
  while (partind LT part_ids_size AND mouseind LT mouse_ids_size) do begin
    if (part_ids[partind] EQ mouseIDs[mouseind]) then begin
      include_id[mouseind] = 1
      ++partind
      ++mouseind
    endif else if (part_ids[partind] LT mouseIDs[mouseind]) then ++partind else ++mouseind
  endwhile
  ind = where(include_id EQ 1)
  mouseIDs = mouseIDs[ind]
  mouse_ids_size = n_elements(mouseIDs)
  ind = 0L
  include_id = 0L

  allgalind = 0L
  mouseind = 0L
  include_id = lonarr(all_gal_ids_size)
  while (allgalind LT all_gal_ids_size AND mouseind LT mouse_ids_size) do begin
    if (all_gal_ids[allgalind] EQ mouseIDs[mouseind]) then begin
      include_id[allgalind] = 1
      ++allgalind
      ++mouseind
    endif else if (all_gal_ids[allgalind] LT mouseIDs[mouseind]) then ++allgalind else ++mouseind
  endwhile
  ind = where(include_id EQ 1) 
  if (n_elements(ind) GT 1) then begin
    gal_ids = all_gal_ids[ind]
    gal_age = all_gal_age[ind]
    gal_ids_size = n_elements(gal_ids)
  endif

  allbcgind = 0L
  mouseind = 0L
  include_id = lonarr(all_bcg_ids_size)
  while (allbcgind LT all_bcg_ids_size AND mouseind LT mouse_ids_size) do begin
    if (all_bcg_ids[allbcgind] EQ mouseIDs[mouseind]) then begin
      include_id[allbcgind] = 1
      ++allbcgind
      ++mouseind
    endif else if (all_bcg_ids[allbcgind] LT mouseIDs[mouseind]) then ++allbcgind else ++mouseind
  endwhile
  ind = where(include_id EQ 1) 
  if (n_elements(ind) GT 1) then begin
    bcg_ids = all_bcg_ids[ind]
    bcg_age = all_bcg_age[ind]
    bcg_ids_size = n_elements(bcg_ids)
  endif else begin
    bcg_ids = 0L
    bcg_ids_size = 0L
  endelse 

  alliclind = 0L
  mouseind = 0L
  include_id = lonarr(all_icl_ids_size)
  while (alliclind LT all_icl_ids_size AND mouseind LT mouse_ids_size) do begin
    if (all_icl_ids[alliclind] EQ mouseIDs[mouseind]) then begin
      include_id[alliclind] = 1
      ++alliclind
      ++mouseind
    endif else if (all_icl_ids[alliclind] LT mouseIDs[mouseind]) then ++alliclind else ++mouseind
  endwhile
  ind = where(include_id EQ 1) 
  if (n_elements(ind) GT 1) then begin
    icl_ids = all_icl_ids[ind]
    icl_age = all_icl_age[ind]
    icl_ids_size = n_elements(icl_ids)
  endif

  print, gal_ids_size, " satellite galaxy, ", bcg_ids_size, " BCG, and ", icl_ids_size, " ICL particles (stars and gas) in subhalo"

  halo_stellar_mass = dblarr(numsnaps)
  halo_dist = dblarr(numsnaps)
  halo_pos = dblarr(3,numsnaps)  
  main_pos = dblarr(3,numsnaps)
  halo_vel = dblarr(3,numsnaps)  
  main_vel = dblarr(3,numsnaps)
  halo_index = lonarr(numsnaps)
  halo_index[*] = -2 

  cursnap = snapshot
  cursub = mousesub
  while (cursnap LT numsnaps AND cursub GE 0) do begin
    if (num_subhalos[cursnap] GT 0) then begin
      halo_stellar_mass[cursnap] = (*subhalo_stellar_mass[cursnap])[cursub]
      halo_dist[cursnap] = sqrt(total(((*subhalo_pos[cursnap])[*,cursub]-(*subhalo_pos[cursnap])[*,0])^2,/double))
      halo_pos[*,cursnap] = (*subhalo_pos[cursnap])[*,cursub]
      main_pos[*,cursnap] = (*subhalo_pos[cursnap])[*,0]
      halo_vel[*,cursnap] = ((*subhalo_vel[cursnap])[*,cursub])/snaptimes[cursnap] ; converting to comoving velocity
      main_vel[*,cursnap] = ((*subhalo_vel[cursnap])[*,0])/snaptimes[cursnap] ; converting to comoving velocity 
      halo_index[cursnap] = cursub
    endif
    if (have_descendants[cursnap] EQ 1) then begin
      cursub = (*subhalo_descendants[cursnap])[cursub]
    endif else cursub = -1
    ++cursnap 
  endwhile

  ;window, 2, retain = 2
  ;ind = where(halo_index GE 1)
  ;!p.multi=[0,1,2]
  ;plot, snaptimes[ind], halo_stellar_mass[ind], xrange=[0.0,1.0]
  ;plot, snaptimes[ind], halo_dist[ind], xrange=[0.0,1.0]
  ;wset, 0
  ;!p.multi=[0,1,1]
endif
;--------------------------------------------
if (command EQ -7) then begin
  have_in_cache = intarr(numsnaps)
  cache = bytarr(numsnaps,3,numbin+1,numbin+1) 

  gal_ids = all_gal_ids
  gal_ids_size = all_gal_ids_size
  gal_age = all_gal_age
  bcg_ids = all_bcg_ids
  bcg_ids_size = all_bcg_ids_size
  bcg_age = all_bcg_age
  icl_ids = all_icl_ids
  icl_ids_size = all_icl_ids_size
  icl_gae = all_icl_age 

  halo_stellar_mass = 0
  halo_dist = 0
  halo_pos = 0
  halo_index = -2
endif
;--------------------------------------------
if (command EQ -8) then begin
  if (stopcache EQ -1) then begin
    print, "cache to snapshot:"
    read, stopcache

    skip_input = 1
  endif else begin
    if (snapshot EQ stopcache-1) then begin
      snapshot += 1
      stopcache = -1 
    endif else begin
      skip_input = 1
      snapshot += 1
    endelse
  endelse
endif
;--------------------------------------------
if (command EQ -9) then begin
  curkey = byte(get_kbrd(1))
  if (curkey EQ 43 AND snapshot LT numsnaps-1) then snapshot += 1
  if (curkey EQ 45 AND snapshot GT 1) then snapshot -= 1
  if (curkey EQ 43 OR curkey EQ 45) then skip_input = 1
endif
;--------------------------------------------
if (command EQ -10) then begin
  ;readnew,Base+"/snap_"+exts,part_mass,"MASS",parttype=4
  ;star_part_mass = part_mass[0]
  ;part_mass = 0L

  ;halo_stellar_mass = fltarr(numsnaps)
  ;halo_dist = fltarr(numsnaps)
  ;halo_pos = fltarr(3,numsnaps)
  ;halo_index = lonarr(numsnaps)
  ;halo_index[*] = -1

  for cursnap=snapshot,numsnaps-1 do begin
    if cursnap ge 1000 then begin
      exts='0000'
      exts=exts+strcompress(string(cursnap),/remove_all)
      exts=strmid(exts,strlen(exts)-4,4)
    endif else begin
      exts='000'
      exts=exts+strcompress(string(cursnap),/remove_all)
      exts=strmid(exts,strlen(exts)-3,3)
    endelse 
   
    ;read subhalo info      
    skip = 0L
    skip_sub = 0L
    fnr = 0L

  repeat begin

    f = Base + "/groups_" + exts +"/subhalo_tab_"+exts +"."+strcompress(string(fnr),/remove_all)

    Ngroups = 0L
    TotNgroups = 0L
    Nids = 0L
    TotNids = 0LL
    NTask = 0L
    Nsubgroups = 0L
    TotNsubgroups = 0L

    openr,1,f
    readu,1, Ngroups, TotNgroups, Nids, TotNids, NTask, Nsubgroups, TotNsubgroups

    if fnr eq 0 then begin
        GroupLen = lonarr(TotNgroups)
        GroupOffset = lonarr(TotNgroups)
        GroupMass = fltarr(TotNgroups)
        GroupPos = fltarr(3, TotNgroups)
        Group_M_Mean200 = fltarr(TotNgroups)
        Group_R_Mean200 = fltarr(TotNgroups)
        Group_M_Crit200 = fltarr(TotNgroups)
        Group_R_Crit200 = fltarr(TotNgroups)
        Group_M_TopHat200 = fltarr(TotNgroups)
        Group_R_TopHat200 = fltarr(TotNgroups)
        Group_VelDisp_Mean200 = fltarr(TotNgroups)
        Group_VelDisp_Crit200 = fltarr(TotNgroups)
        Group_VelDisp_TopHat200 = fltarr(TotNgroups)
        GroupContaminationCount = lonarr(TotNgroups)
        GroupContaminationMass = fltarr(TotNgroups)
        GroupNsubs = lonarr(TotNgroups)
        GroupFirstSub = lonarr(TotNgroups)

        SubhaloLen = lonarr(TotNsubgroups)
        SubhaloOffset = lonarr(TotNsubgroups)
        SubhaloParent = lonarr(TotNsubgroups)
        SubhaloMass = fltarr(TotNsubgroups)
        SubhaloPos = fltarr(3, TotNsubgroups)
        SubhaloVel = fltarr(3, TotNsubgroups)
        SubhaloCM = fltarr(3, TotNsubgroups)
        SubhaloSpin = fltarr(3, TotNsubgroups)
        SubhaloVelDisp = fltarr(TotNsubgroups)
        SubhaloVmax = fltarr(TotNsubgroups)
        SubhaloVmaxRad = fltarr(TotNsubgroups)
        SubhaloHalfmassRad = fltarr(TotNsubgroups)
        SubhaloIDMostbound = lonarr(TotNsubgroups)
        SubhaloGrNr = lonarr(TotNsubgroups)
        SubhaloMassTab = fltarr(6, TotNsubgroups)
    endif

    if Ngroups gt 0 then begin
        
        locLen = lonarr(Ngroups)
        locOffset = lonarr(Ngroups)
        locMass = fltarr(Ngroups)
        locPos = fltarr(3, Ngroups)
        loc_M_Mean200 = fltarr(Ngroups)
        loc_R_Mean200 = fltarr(Ngroups)
        loc_M_Crit200 = fltarr(Ngroups)
        loc_R_Crit200 = fltarr(Ngroups)
        loc_M_TopHat200 = fltarr(Ngroups)
        loc_R_TopHat200 = fltarr(Ngroups)
        loc_VelDisp_Mean200 = fltarr(Ngroups)
        loc_VelDisp_Crit200 = fltarr(Ngroups)
        loc_VelDisp_TopHat200 = fltarr(Ngroups)
        locContaminationCount = lonarr(Ngroups)
        locContaminationMass = fltarr(Ngroups)
        locNsubs = lonarr(Ngroups)
        locFirstSub = lonarr(Ngroups)

        readu,1, loclen 
        readu,1, locOffset
        readu,1, locMass
        readu,1, locPos
        readu,1, loc_M_Mean200
        readu,1, loc_R_Mean200
        readu,1, loc_M_Crit200
        readu,1, loc_R_Crit200
        readu,1, loc_M_TopHat200
        readu,1, loc_R_TopHat200
        if FLAG_Group_VelDisp ne 0 then begin
          readu,1, loc_VelDisp_Mean200
          readu,1, loc_VelDisp_Crit200
          readu,1, loc_VelDisp_TopHat200
        endif
        readu,1, locContaminationCount
        readu,1, locContaminationMass
        readu,1, locNsubs
        readu,1, locFirstSub

        GroupLen(skip:skip+Ngroups-1) = locLen(*)
        GroupOffset(skip:skip+Ngroups-1) = locOffset(*)
        GroupMass(skip:skip+Ngroups-1) = locMass(*)
        GroupPos(*, skip:skip+Ngroups-1) = locPos(*,*)
        Group_M_Mean200(skip:skip+Ngroups-1) = loc_M_Mean200(*)
        Group_R_Mean200(skip:skip+Ngroups-1) = loc_R_Mean200(*)
        Group_M_Crit200(skip:skip+Ngroups-1) = loc_M_Crit200(*)
        Group_R_Crit200(skip:skip+Ngroups-1) = loc_R_Crit200(*)
        Group_M_TopHat200(skip:skip+Ngroups-1) = loc_M_TopHat200(*)
        Group_R_TopHat200(skip:skip+Ngroups-1) = loc_R_TopHat200(*)
        Group_VelDisp_Mean200(skip:skip+Ngroups-1) = loc_VelDisp_Mean200(*)
        Group_VelDisp_Crit200(skip:skip+Ngroups-1) = loc_VelDisp_Crit200(*)
        Group_VelDisp_TopHat200(skip:skip+Ngroups-1) = loc_VelDisp_TopHat200(*)
        GroupContaminationCount(skip:skip+Ngroups-1) = locContaminationCount(*) 
        GroupContaminationMass(skip:skip+Ngroups-1) = locContaminationMass(*)
        GroupNsubs(skip:skip+Ngroups-1) = locNsubs(*)
        GroupFirstsub(skip:skip+Ngroups-1) = locFirstSub(*)

        skip+= Ngroups
    endif


    if Nsubgroups gt 0 then begin
        
        locLen = lonarr(Nsubgroups)
        locOffset = lonarr(Nsubgroups)
        locParent = lonarr(Nsubgroups)
        locMass = fltarr(Nsubgroups)
        locPos = fltarr(3, Nsubgroups)
        locVel = fltarr(3, Nsubgroups)
        locCM = fltarr(3, Nsubgroups)
        locSpin= fltarr(3, Nsubgroups)
        locVelDisp = fltarr(Nsubgroups)
        locVmax = fltarr(Nsubgroups)
        locVmaxRad = fltarr(Nsubgroups)
        locHalfMassRad = fltarr(Nsubgroups)
        locIDMostBound = lonarr(Nsubgroups)
        locGrNr = lonarr(Nsubgroups)
        locMassTab = fltarr(6, Nsubgroups)

        readu,1, loclen 
        readu,1, locOffset
        readu,1, locParent
        readu,1, locMass
        readu,1, locPos
        readu,1, locVel
        readu,1, locCM
        readu,1, locSpin
        readu,1, locVelDisp
        readu,1, locVmax
        readu,1, locVmaxRad
        readu,1, locHalfMassRad
        readu,1, locIDMostBound
        readu,1, locGrNr
        readu,1, locMassTab

        SubhaloLen(skip_sub:skip_sub+Nsubgroups-1) = locLen(*)
        SubhaloOffset(skip_sub:skip_sub+Nsubgroups-1) = locOffset(*)
        SubhaloParent(skip_sub:skip_sub+Nsubgroups-1) = locParent(*)
        SubhaloMass(skip_sub:skip_sub+Nsubgroups-1) = locMass(*)
        SubhaloPos(*, skip_sub:skip_sub+Nsubgroups-1) = locPos(*,*)
        SubhaloVel(*, skip_sub:skip_sub+Nsubgroups-1) = locVel(*,*)
        SubhaloCM(*, skip_sub:skip_sub+Nsubgroups-1) = locCM(*,*)
        SubhaloSpin(*, skip_sub:skip_sub+Nsubgroups-1) = locSpin(*,*)
        SubhaloVeldisp(skip_sub:skip_sub+Nsubgroups-1) = locVeldisp(*)
        SubhaloVmax(skip_sub:skip_sub+Nsubgroups-1) = locVmax(*)
        SubhaloVmaxRad(skip_sub:skip_sub+Nsubgroups-1) = locVmaxRad(*)
        SubhaloHalfmassRad(skip_sub:skip_sub+Nsubgroups-1) = locHalfmassRad(*)
        SubhaloIDMostBound(skip_sub:skip_sub+Nsubgroups-1) = locIDMostBound(*)
        SubhaloGrNr(skip_sub:skip_sub+Nsubgroups-1) = locGrNr(*)
        SubhaloMassTab(*, skip_sub:skip_sub+Nsubgroups-1) = locMassTab(*,*)

        skip_sub+= Nsubgroups
    endif

    close, 1

    fnr++

  endrep until fnr eq NTask

  print
  print, "TotNgroups   =", TotNgroups
  print, "TotNsubgroups=", TotNsubgroups
  print
  print, "Largest group of length ", GroupLen(0)," has", GroupNsubs(0)," substructures"

  sfrflag = 0
  bhflag = 0

  num_subhalos[cursnap] = TotNsubgroups
  
  ptr_free, subhalo_stellar_mass[cursnap]
  subhalo_stellar_mass[cursnap] = ptr_new(dblarr(TotNsubgroups))
  *subhalo_stellar_mass[cursnap] = SubhaloMassTab[4,*]  

  ptr_free, subhalo_pos[cursnap]
  subhalo_pos[cursnap] = ptr_new(dblarr(3,TotNsubgroups))
  *subhalo_pos[cursnap] = SubhaloPos

  ptr_free, subhalo_vel[cursnap]
  subhalo_vel[cursnap] = ptr_new(dblarr(3,TotNsubgroups))
  *subhalo_vel[cursnap] = SubhaloVel

  ;--------------------------------------------------

  skip = 0L
  fnr = 0L

  repeat begin

    f = Base + "/groups_" + exts +"/subhalo_ids_"+exts +"."+strcompress(string(fnr),/remove_all)

    idNgroups = 0L
    idTotNgroups = 0L
    idNids = 0L
    idTotNids = 0LL
    idNTask = 0L
    idOffset = 0L

    openr,1,f
    readu,1, idNgroups, idTotNgroups, idNids, idTotNids, idNTask, idOffset

    if fnr eq 0 then begin
        IDs = lonarr(idTotNids)
    endif

    if idNids gt 0 then begin
        
        locIDs = lonarr(idNids)
        readu,1, locIDs

        IDs(skip:skip+idNids-1) = locIDs(*)
        skip+= idNids

    endif

    close, 1

    fnr++

  endrep until fnr eq idNTask

  print
  print, "Total IDs in grps=", idTotNids
  print

  IDs = ulong(IDs)
  
    ;done reading subhalo info, now get subhalo  
   
    subhalo = lonarr(idTotNids) 
    subhalo[*] = -1
    for i=0,TotNsubgroups-1 do begin
      subhalo[SubhaloOffset[i]:SubhaloOffset[i]+SubhaloLen[i]-1] = i
    endfor
 
    ind = sort(IDs)
    IDs = IDs[ind]
    subhalo = subhalo[ind]
    ids_size = n_elements(IDs)

    readnew,Base+"/snap_"+exts,head,"HEAD",parttype=4
    snaptimes[cursnap] = head.time

    part_ids = all_part_ids[where(all_part_age LT snaptimes[cursnap])]
    part_ids_size = (size(part_ids))[1] 

    idsind = 0L
    partind = 0L
    include_id = intarr(ids_size)
    while (idsind LT ids_size AND partind LT part_ids_size) do begin
      if (IDs[idsind] EQ part_ids[partind]) then begin
        include_id(idsind) = 1
        ++idsind
        ++partind
      endif else if (IDs[idsind] LT part_ids[partind]) then ++idsind else ++partind
    endwhile
    ind = where(include_id EQ 1)
    IDs = IDs[ind]
    subhalo = subhalo[ind]
    ids_size = n_elements(IDs) 
    include_id = 0L
    part_ids = 0L

    if (cursnap GT snapshot) then begin
    ;find descendants in this snap
   
      part_from_to = lonarr(old_TotNsubgroups,TotNsubgroups)      
      old_idsind = 0L
      idsind = 0L
      while (old_idsind LT old_ids_size AND idsind LT ids_size) do begin
        if (old_IDs[old_idsind] EQ IDs[idsind]) then begin
          if (old_subhalo[old_idsind] GE 0 AND subhalo[idsind] GE 0) then part_from_to[old_subhalo[old_idsind],subhalo[idsind]] += 1
          ++old_idsind
          ++idsind
        endif else if (old_IDs[old_idsind] LT IDs[idsind]) then ++old_idsind else ++idsind
      endwhile

      ptr_free, subhalo_descendants[cursnap-1]
      subhalo_descendants[cursnap-1] = ptr_new(lonarr(old_TotNsubgroups))

      for i=0L,old_TotNsubgroups-1 do begin
        maxpart = max(part_from_to[i,*],descendant)
        if (descendant EQ 0 AND maxpart GT 0) then if (max(part_from_to[i,1:*],temp_descendant) GT maxpart*0.15) then descendant = temp_descendant + 1
        if (maxpart EQ 0) then descendant = -1
        (*subhalo_descendants[cursnap-1])[i] = descendant 
      endfor
      part_from_to = 0L
      have_descendants[cursnap-1] = 1      

    endif
    
    old_TotNsubgroups = TotNsubgroups
    old_IDs = IDs
    old_subhalo = subhalo
    old_ids_size = ids_size
  endfor 

  ;window, 2, retain = 2
  ;ind = where(halo_index GE 1)
  ;!p.multi=[0,1,2]
  ;plot, snaptimes[ind], halo_stellar_mass[ind], xrange=[0.0,1.0]
  ;plot, snaptimes[ind], halo_dist[ind], xrange=[0.0,1.0]
  ;wset, 0
  ;!p.multi=[0,1,1]   
endif
;-------------------------------------------
if (command EQ -11 AND snapshot GE 0) then if(have_descendants[snapshot] EQ 1 AND num_subhalos[snapshot] GT 0) then begin
  have_in_cache = intarr(numsnaps)
  cache = bytarr(numsnaps,3,numbin+1,numbin+1) 

  gal_ids = all_gal_ids
  gal_ids_size = all_gal_ids_size
  gal_age = all_gal_age
  bcg_ids = all_bcg_ids
  bcg_ids_size = all_bcg_ids_size
  bcg_age = all_bcg_age
  icl_ids = all_icl_ids
  icl_ids_size = all_icl_ids_size
  icl_age = all_icl_age 

  halo_stellar_mass = 0
  halo_dist = 0
  halo_pos = 0
  halo_index = -2
 
  numsubs = num_subhalos[snapshot] 
 
  cur_rad = dblarr(numsubs) 
  min_rad = dblarr(numsubs)
  cur_stellar_mass = dblarr(numsubs)
  final_stellar_mass = dblarr(numsubs)
  gal_mass = dblarr(numsubs)
  bcg_mass = dblarr(numsubs)
  icl_mass = dblarr(numsubs)

;----------------------------------------------

    if snapshot ge 1000 then begin
      exts='0000'
      exts=exts+strcompress(string(snapshot),/remove_all)
      exts=strmid(exts,strlen(exts)-4,4)
    endif else begin
      exts='000'
      exts=exts+strcompress(string(snapshot),/remove_all)
      exts=strmid(exts,strlen(exts)-3,3)
    endelse 
   
    ;read subhalo info      
    skip = 0L
    skip_sub = 0L
    fnr = 0L

  repeat begin

    f = Base + "/groups_" + exts +"/subhalo_tab_"+exts +"."+strcompress(string(fnr),/remove_all)

    Ngroups = 0L
    TotNgroups = 0L
    Nids = 0L
    TotNids = 0LL
    NTask = 0L
    Nsubgroups = 0L
    TotNsubgroups = 0L

    openr,1,f
    readu,1, Ngroups, TotNgroups, Nids, TotNids, NTask, Nsubgroups, TotNsubgroups

    if fnr eq 0 then begin
        GroupLen = lonarr(TotNgroups)
        GroupOffset = lonarr(TotNgroups)
        GroupMass = fltarr(TotNgroups)
        GroupPos = fltarr(3, TotNgroups)
        Group_M_Mean200 = fltarr(TotNgroups)
        Group_R_Mean200 = fltarr(TotNgroups)
        Group_M_Crit200 = fltarr(TotNgroups)
        Group_R_Crit200 = fltarr(TotNgroups)
        Group_M_TopHat200 = fltarr(TotNgroups)
        Group_R_TopHat200 = fltarr(TotNgroups)
        Group_VelDisp_Mean200 = fltarr(TotNgroups)
        Group_VelDisp_Crit200 = fltarr(TotNgroups)
        Group_VelDisp_TopHat200 = fltarr(TotNgroups)
        GroupContaminationCount = lonarr(TotNgroups)
        GroupContaminationMass = fltarr(TotNgroups)
        GroupNsubs = lonarr(TotNgroups)
        GroupFirstSub = lonarr(TotNgroups)

        SubhaloLen = lonarr(TotNsubgroups)
        SubhaloOffset = lonarr(TotNsubgroups)
        SubhaloParent = lonarr(TotNsubgroups)
        SubhaloMass = fltarr(TotNsubgroups)
        SubhaloPos = fltarr(3, TotNsubgroups)
        SubhaloVel = fltarr(3, TotNsubgroups)
        SubhaloCM = fltarr(3, TotNsubgroups)
        SubhaloSpin = fltarr(3, TotNsubgroups)
        SubhaloVelDisp = fltarr(TotNsubgroups)
        SubhaloVmax = fltarr(TotNsubgroups)
        SubhaloVmaxRad = fltarr(TotNsubgroups)
        SubhaloHalfmassRad = fltarr(TotNsubgroups)
        SubhaloIDMostbound = lonarr(TotNsubgroups)
        SubhaloGrNr = lonarr(TotNsubgroups)
        SubhaloMassTab = fltarr(6, TotNsubgroups)
    endif

    if Ngroups gt 0 then begin
        
        locLen = lonarr(Ngroups)
        locOffset = lonarr(Ngroups)
        locMass = fltarr(Ngroups)
        locPos = fltarr(3, Ngroups)
        loc_M_Mean200 = fltarr(Ngroups)
        loc_R_Mean200 = fltarr(Ngroups)
        loc_M_Crit200 = fltarr(Ngroups)
        loc_R_Crit200 = fltarr(Ngroups)
        loc_M_TopHat200 = fltarr(Ngroups)
        loc_R_TopHat200 = fltarr(Ngroups)
        loc_VelDisp_Mean200 = fltarr(Ngroups)
        loc_VelDisp_Crit200 = fltarr(Ngroups)
        loc_VelDisp_TopHat200 = fltarr(Ngroups)
        locContaminationCount = lonarr(Ngroups)
        locContaminationMass = fltarr(Ngroups)
        locNsubs = lonarr(Ngroups)
        locFirstSub = lonarr(Ngroups)

        readu,1, loclen 
        readu,1, locOffset
        readu,1, locMass
        readu,1, locPos
        readu,1, loc_M_Mean200
        readu,1, loc_R_Mean200
        readu,1, loc_M_Crit200
        readu,1, loc_R_Crit200
        readu,1, loc_M_TopHat200
        readu,1, loc_R_TopHat200
        if FLAG_Group_VelDisp ne 0 then begin
          readu,1, loc_VelDisp_Mean200
          readu,1, loc_VelDisp_Crit200
          readu,1, loc_VelDisp_TopHat200
        endif
        readu,1, locContaminationCount
        readu,1, locContaminationMass
        readu,1, locNsubs
        readu,1, locFirstSub

        GroupLen(skip:skip+Ngroups-1) = locLen(*)
        GroupOffset(skip:skip+Ngroups-1) = locOffset(*)
        GroupMass(skip:skip+Ngroups-1) = locMass(*)
        GroupPos(*, skip:skip+Ngroups-1) = locPos(*,*)
        Group_M_Mean200(skip:skip+Ngroups-1) = loc_M_Mean200(*)
        Group_R_Mean200(skip:skip+Ngroups-1) = loc_R_Mean200(*)
        Group_M_Crit200(skip:skip+Ngroups-1) = loc_M_Crit200(*)
        Group_R_Crit200(skip:skip+Ngroups-1) = loc_R_Crit200(*)
        Group_M_TopHat200(skip:skip+Ngroups-1) = loc_M_TopHat200(*)
        Group_R_TopHat200(skip:skip+Ngroups-1) = loc_R_TopHat200(*)
        Group_VelDisp_Mean200(skip:skip+Ngroups-1) = loc_VelDisp_Mean200(*)
        Group_VelDisp_Crit200(skip:skip+Ngroups-1) = loc_VelDisp_Crit200(*)
        Group_VelDisp_TopHat200(skip:skip+Ngroups-1) = loc_VelDisp_TopHat200(*)
        GroupContaminationCount(skip:skip+Ngroups-1) = locContaminationCount(*) 
        GroupContaminationMass(skip:skip+Ngroups-1) = locContaminationMass(*)
        GroupNsubs(skip:skip+Ngroups-1) = locNsubs(*)
        GroupFirstsub(skip:skip+Ngroups-1) = locFirstSub(*)

        skip+= Ngroups
    endif


    if Nsubgroups gt 0 then begin
        
        locLen = lonarr(Nsubgroups)
        locOffset = lonarr(Nsubgroups)
        locParent = lonarr(Nsubgroups)
        locMass = fltarr(Nsubgroups)
        locPos = fltarr(3, Nsubgroups)
        locVel = fltarr(3, Nsubgroups)
        locCM = fltarr(3, Nsubgroups)
        locSpin= fltarr(3, Nsubgroups)
        locVelDisp = fltarr(Nsubgroups)
        locVmax = fltarr(Nsubgroups)
        locVmaxRad = fltarr(Nsubgroups)
        locHalfMassRad = fltarr(Nsubgroups)
        locIDMostBound = lonarr(Nsubgroups)
        locGrNr = lonarr(Nsubgroups)
        locMassTab = fltarr(6, Nsubgroups)

        readu,1, loclen 
        readu,1, locOffset
        readu,1, locParent
        readu,1, locMass
        readu,1, locPos
        readu,1, locVel
        readu,1, locCM
        readu,1, locSpin
        readu,1, locVelDisp
        readu,1, locVmax
        readu,1, locVmaxRad
        readu,1, locHalfMassRad
        readu,1, locIDMostBound
        readu,1, locGrNr
        readu,1, locMassTab

        SubhaloLen(skip_sub:skip_sub+Nsubgroups-1) = locLen(*)
        SubhaloOffset(skip_sub:skip_sub+Nsubgroups-1) = locOffset(*)
        SubhaloParent(skip_sub:skip_sub+Nsubgroups-1) = locParent(*)
        SubhaloMass(skip_sub:skip_sub+Nsubgroups-1) = locMass(*)
        SubhaloPos(*, skip_sub:skip_sub+Nsubgroups-1) = locPos(*,*)
        SubhaloVel(*, skip_sub:skip_sub+Nsubgroups-1) = locVel(*,*)
        SubhaloCM(*, skip_sub:skip_sub+Nsubgroups-1) = locCM(*,*)
        SubhaloSpin(*, skip_sub:skip_sub+Nsubgroups-1) = locSpin(*,*)
        SubhaloVeldisp(skip_sub:skip_sub+Nsubgroups-1) = locVeldisp(*)
        SubhaloVmax(skip_sub:skip_sub+Nsubgroups-1) = locVmax(*)
        SubhaloVmaxRad(skip_sub:skip_sub+Nsubgroups-1) = locVmaxRad(*)
        SubhaloHalfmassRad(skip_sub:skip_sub+Nsubgroups-1) = locHalfmassRad(*)
        SubhaloIDMostBound(skip_sub:skip_sub+Nsubgroups-1) = locIDMostBound(*)
        SubhaloGrNr(skip_sub:skip_sub+Nsubgroups-1) = locGrNr(*)
        SubhaloMassTab(*, skip_sub:skip_sub+Nsubgroups-1) = locMassTab(*,*)

        skip_sub+= Nsubgroups
    endif

    close, 1

    fnr++

  endrep until fnr eq NTask

  print
  print, "TotNgroups   =", TotNgroups
  print, "TotNsubgroups=", TotNsubgroups
  print
  print, "Largest group of length ", GroupLen(0)," has", GroupNsubs(0)," substructures"

  sfrflag = 0
  bhflag = 0

  ;--------------------------------------------------

  skip = 0L
  fnr = 0L

  repeat begin

    f = Base + "/groups_" + exts +"/subhalo_ids_"+exts +"."+strcompress(string(fnr),/remove_all)

    idNgroups = 0L
    idTotNgroups = 0L
    idNids = 0L
    idTotNids = 0LL
    idNTask = 0L
    idOffset = 0L

    openr,1,f
    readu,1, idNgroups, idTotNgroups, idNids, idTotNids, idNTask, idOffset

    if fnr eq 0 then begin
        IDs = lonarr(idTotNids)
    endif

    if idNids gt 0 then begin
        
        locIDs = lonarr(idNids)
        readu,1, locIDs

        IDs(skip:skip+idNids-1) = locIDs(*)
        skip+= idNids

    endif

    close, 1

    fnr++

  endrep until fnr eq idNTask

  print
  print, "Total IDs in grps=", idTotNids
  print

  IDs = ulong(IDs)
  
    ;done reading subhalo info, now get subhalo  
   
    subhalo = lonarr(idTotNids) 
    subhalo[*] = -1
    for i=0,TotNsubgroups-1 do begin
      subhalo[SubhaloOffset[i]:SubhaloOffset[i]+SubhaloLen[i]-1] = i
    endfor
 
    ind = sort(IDs)
    IDs = IDs[ind]
    subhalo = subhalo[ind]
    numids = n_elements(IDs)

;----------------------------------------------

readnew,Base+"/snap_"+exts,part_pos,"POS ",parttype=4
readnew,Base+"/snap_"+exts,part_ids,"ID  ",parttype=4
ind = sort(part_ids)
part_ids = part_ids[ind]
part_pos = part_pos[*,ind]
part_ids_size = (size(part_ids))[1]

partind = 0L
idsind = 0L
where_part = lonarr(part_ids_size)
where_part[*] = -1
while (partind LT part_ids_size AND idsind LT numids) do begin
  if (part_ids[partind] EQ IDs[idsind]) then begin
    where_part[partind] = idsind
    ++partind
    ++idsind
  endif else if (part_ids[partind] LT IDs[idsind]) then ++partind else ++idsind
endwhile
ind = where(where_part GE 0)
part_subhalo = lonarr(part_ids_size)
part_subhalo[*] = -2
if (ind[0] GE 0) then part_subhalo[ind] = subhalo[where_part[ind]]

halflightrad = dblarr(TotNsubgroups)
for i=0L,TotNsubgroups-1 do begin
  ind = where(part_subhalo EQ i)
  if (ind[0] GE 0) then halflightrad[i] = median((part_pos[0,ind]-SubhaloPos[0,i])^2 + (part_pos[1,ind]-SubhaloPos[1,i])^2 + (part_pos[2,ind]-SubhaloPos[2,i])^2)  
endfor
halflightrad = sqrt(halflightrad)*snaptimes[snapshot]

part_pos = 0L
part_ids = 0L
where_part = 0L
part_subhalo = 0L

;--------------------------------------------------

galind = 0L
idsind = 0L
where_part = lonarr(gal_ids_size)
where_part[*] = -1
while (galind LT gal_ids_size AND idsind LT numids) do begin
  if (gal_ids[galind] EQ IDs[idsind]) then begin
    where_part[galind] = idsind
    ++galind
    ++idsind
  endif else if (gal_ids[galind] LT IDs[idsind]) then ++galind else ++idsind
endwhile
ind = where(where_part GE 0)
gal_subhalo = lonarr(gal_ids_size)
gal_subhalo[*] = -2
if (ind[0] GE 0) then gal_subhalo[ind] = subhalo[where_part[ind]]

bcgind = 0L
idsind = 0L
where_part = lonarr(bcg_ids_size)
where_part[*] = -1
while (bcgind LT bcg_ids_size AND idsind LT numids) do begin
  if (bcg_ids[bcgind] EQ IDs[idsind]) then begin
    where_part[bcgind] = idsind
    ++bcgind
    ++idsind
  endif else if (bcg_ids[bcgind] LT IDs[idsind]) then ++bcgind else ++idsind
endwhile
ind = where(where_part GE 0)
bcg_subhalo = lonarr(bcg_ids_size)
bcg_subhalo[*] = -2
if (ind[0] GE 0) then bcg_subhalo[ind] = subhalo[where_part[ind]]

iclind = 0L
idsind = 0L
where_part = lonarr(icl_ids_size)
where_part[*] = -1
while (iclind LT icl_ids_size AND idsind LT numids) do begin
  if (icl_ids[iclind] EQ IDs[idsind]) then begin
    where_part[iclind] = idsind
    ++iclind
    ++idsind
  endif else if (icl_ids[iclind] LT IDs[idsind]) then ++iclind else ++idsind
endwhile
ind = where(where_part GE 0)
icl_subhalo = lonarr(icl_ids_size)
icl_subhalo[*] = -2
if (ind[0] GE 0) then icl_subhalo[ind] = subhalo[where_part[ind]]

galmass = dblarr(TotNsubgroups)
bcgmass = dblarr(TotNsubgroups)
iclmass = dblarr(TotNsubgroups)

for i=0L,gal_ids_size-1 do if (gal_subhalo[i] GE 0 AND gal_age[i] LT snaptimes[snapshot]) then galmass[gal_subhalo[i]] += 1.0D
for i=0L,bcg_ids_size-1 do if (bcg_subhalo[i] GE 0 AND bcg_age[i] LT snaptimes[snapshot]) then bcgmass[bcg_subhalo[i]] += 1.0D
for i=0L,icl_ids_size-1 do if (icl_subhalo[i] GE 0 AND icl_age[i] LT snaptimes[snapshot]) then iclmass[icl_subhalo[i]] += 1.0D
galmass *= star_part_mass
bcgmass *= star_part_mass
iclmass *= star_part_mass

;---------------------------------------------- 

  openw, 31, "infalling_gal_props.txt", width=256
  for i=0L,numsubs-1 do begin
    halo_stellar_mass = dblarr(numsnaps)
    halo_dist = dblarr(numsnaps)
    halo_pos = dblarr(3,numsnaps)  
    halo_index = lonarr(numsnaps)
    halo_index[*] = -2 

    cursub = i
    cursnap = snapshot
    while (cursnap LT numsnaps AND cursub GE 0) do begin
    if (num_subhalos[cursnap] GT 0) then begin
      halo_stellar_mass[cursnap] = (*subhalo_stellar_mass[cursnap])[cursub]
      halo_dist[cursnap] = sqrt(total(((*subhalo_pos[cursnap])[*,cursub]-(*subhalo_pos[cursnap])[*,0])^2,/double))
      halo_pos[cursnap] = (*subhalo_pos[cursnap])[*,cursub]
      halo_index[cursnap] = cursub
    endif
    if (have_descendants[cursnap] EQ 1) then begin
      cursub = (*subhalo_descendants[cursnap])[cursub]
    endif else cursub = -1
    ++cursnap 
    endwhile

    cur_rad[i] = halo_dist[snapshot]
    ind = where(halo_index GT 0)
    if (ind[0] EQ -1) then min_rad[i] = 0 else min_rad[i] = min(halo_dist[ind])
    cur_stellar_mass[i] = halo_stellar_mass[snapshot]
    if (halo_index[numsnaps-1] GT 0) then final_stellar_mass[i] = halo_stellar_mass[numsnaps-1] else final_stellar_mass[i] = 0  
  
    printf, 31, i, cur_stellar_mass[i], galmass[i], bcgmass[i], iclmass[i], final_stellar_mass[i], cur_rad[i], cur_rad[i]/Group_R_crit200[0], min_rad[i], halflightrad[i]
  endfor
  close, 31

endif
;--------------------------------------------
if (command EQ -13) then begin
  have_in_cache = intarr(numsnaps)
  cache = bytarr(numsnaps,3,numbin+1,numbin+1)

  cursor,mousex,mousey,/down, /DEVICE
  mousex = mousex*xbin + xmin
  mousey = mousey*ybin + ymin
  print, "changing center to x = ", mousex, ", y = ", mousey
  
  centerpos[0] = mousex
  centerpos[1] = mousey
endif
;--------------------------------------------
if (command EQ -14) then begin
  have_in_cache = intarr(numsnaps)
  cache = bytarr(numsnaps,3,numbin+1,numbin+1)

  print, "ENTER new sidelength in Mpc/h (current value is ", fieldsize, ")"
  read, fieldsize
  print, "changed fieldsize to ", fieldsize
endif
;--------------------------------------------
endrep until (snapshot GE 0)

endwhile

end
