#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri May  3 15:32:24 2019

@author: nh444
"""
from __future__ import print_function

import snap
import numpy as np
from get_uncontaminated_groups import get_uncontaminated_groups


def main():
    h_scale = 0.6774
    basedir = "/data/ERCblackholes1/nh444/zooms/MDRInt/"
    # List of tuple pairs containing directory name and number of last snapshot
    inputs = [
#              ("c96_MDRInt", 25),
#              ("c128_MDRInt", 25),
#              ("c160_MDRInt", 25),
#              ("c192_MDRInt", 25), ("c224_MDRInt", 25), ("c256_MDRInt", 25),
#              ("c288_MDRInt", 25), ("c320_MDRInt", 25),
#              ("c352_MDRInt", 25), ("c384_MDRInt", 25),
#              ("c448_MDRInt", 25), ("c480_MDRInt", 25),
#              ("c512_MDRInt", 25),
              ("c112_MDRInt", 99),
              ("c144_MDRInt", 99), ("c176_MDRInt", 99),
              ("c208_MDRInt", 99), ("c240_MDRInt", 99), ("c272_MDRInt", 99),
              ("c304_MDRInt", 99), ("c336_MDRInt", 99), ("c368_MDRInt", 99),
              ("c400_MDRInt", 99), ("c410_MDRInt", 99), ("c432_MDRInt", 99),
              ("c464_MDRInt", 99),  # "c496_MDRInt_SUBF",
              ]
    basedir = "/data/ERCblackholes1/nh444/zooms/MDRInt/snap100to26/"
    inputs = [
              ("c112_MDRInt_snap100to26", 25),
              ("c144_MDRInt_snap100to26", 25), ("c176_MDRInt_snap100to26", 25),
              ("c208_MDRInt_snap100to26", 25), ("c240_MDRInt_snap100to26", 25), ("c272_MDRInt_snap100to26", 25),
              ("c304_MDRInt_snap100to26", 25), ("c336_MDRInt_snap100to26", 25), ("c368_MDRInt_snap100to26", 25),
              ("c400_MDRInt_snap100to26", 25), ("c410_MDRInt_snap100to26", 25), ("c432_MDRInt_snap100to26", 25),
              ("c464_MDRInt_snap100to26", 25),  # "c496_MDRInt_SUBF",
              ]

#    filepath = "/data/curie4/nh444/project1/stars/BCG_growth_histories_all_1e14.hdf5"
#    filepath = "/data/curie4/nh444/project1/stars/BCG_growth_histories_all_3e13.hdf5"
#    filepath = "/data/curie4/nh444/project1/stars/BCG_growth_histories_test.hdf5"
    filepath = "/data/curie4/nh444/project1/stars/BCG_growth_histories_1e14.hdf5"
#    filepath = "/data/curie4/nh444/project1/stars/BCG_growth_histories_1e13.hdf5"

#    filepath = "/data/curie4/nh444/project1/stars/BCG_growth_histories_1e14_99snaps.hdf5"
#    filepath = "/data/curie4/nh444/project1/stars/BCG_growth_histories_1e14_snap100to26.hdf5"

#    save_growth_histories(basedir, inputs, filepath, M200min=1e14,
#                          h_scale=h_scale)
#    plot_growth_histories(filepath, yname='Mstar_50_NSE',
#                          M200lims_list=[[1e13, 5e15]],
#                          normalise=False, cumulative=False, h_scale=h_scale)
    plot_growth_histories(filepath, yname='Mstar_30',
                          M200lims_list=[[1e14, 5e15]],
                          assembled=True, insitu=False,
                          cumulative=True, use_lookback_time=False,
                          stack=True, plot_scatter=False, normalise=True,
                          h_scale=h_scale)
#    plot_growth_histories(filepath, yname='SFR_50', out_suffix="_SFR_50kpc",
#                          cumulative=False, normalise=False,
#                          ylims=[0.1, 3000],
#                          mass_selected=True, redshifts=np.arange(0.2, 2.1, 0.2),
#                          get_BCG_props_args=dict(basedir="/data/curie4/nh444/project1/stars/MDRInt/", dirnames=dirnames),
#                          get_BCG_props_kwargs=dict(outsuffix="_SFR", h_scale=h_scale),
#                          plot_obs=True, use_lookback_time=True,
#                          stack=True, plot_scatter=False,
#                          h_scale=h_scale)
#    plot_growth_histories(filepath, yname='Mstar_100', M200lims=[1e14, 5e15],
#                          stack=True, normalise=False, plot_obs=True,
#                          use_lookback_time=True,
#                          insitu=True,
#                          h_scale=h_scale)
#    plot_growth_histories(filepath, yname='M200', M200lims=[1e14, 5e15],
#                          stack=True, normalise=False, insitu=False,
#                          plot_obs=True, h_scale=h_scale)
#    plot_creation_histories(basedir, inputs, h_scale=h_scale)


def save_growth_histories(basedir, inputs, outfile, M200min=3e13,
                          aperture_rad=[10, 30, 50, 100],
                          h_scale=0.6774, omega_m=0.3089, verbose=True):
    """Calculate BCG mass growth histories and save to file.

    Arguments:
        basedir: Path containing input directories.
        inputs: List of tuple pairs of directory name and last snapshot no.
        outfile: Full path of hdf5 output file.
        M200min: Minimum group mass to consider.
        aperture_rad: (iterable) Stellar mass is calculated within
                      spherical apertures with these radii in physical kpc.
    """
    import os
    import h5py
    import sublink
    from sim_python import cosmo
    from timeit import default_timer as timer

    cm = cosmo.cosmology(h_scale, omega_m)
    starttime = timer()

    # Define redshift bins for storing frequent data (e.g. creation history)
    # Use constant redshift spacing similar to the snapshots.
    zbins = np.linspace(0.0, 15.0, num=601)  # spacing 0.025
    # Define subfind catalogue fields to load for merger tree branch.
    fields = ['SnapNum', 'SubfindID', 'SubhaloGrNr',
              'SubhaloMassType', 'SubhaloMassInRadType', 'SubhaloMassInHalfRadType',
              'SubhaloSFR', 'SubhaloSFRinRad', 'SubhaloSFRinHalfRad']
    if os.path.exists(outfile):
        os.rename(outfile, outfile + ".bkup")
    with h5py.File(outfile, 'w') as f:
        f.attrs['h_scale'] = h_scale
        f.attrs['omega_m'] = omega_m
        # Create four groups. One for the 'assembled' mass histories,
        # one for the 'created' mass, one for in-situ star formation and
        # another for other quantities (e.g. host halo mass).
        assembled = f.create_group("assembled")
        created = f.create_group("created")
        insitu = f.create_group("in-situ_SF")
        insitu_gas = f.create_group("in-situ_SF_gas")
        other = f.create_group("other")
        for input_ in inputs:
            dirname = input_[0]
            indir = basedir + dirname + "/"
            # Number of last snapshot
            maxsnapnum = input_[1]

            # Get subhalo sample.
            sim = snap.snapshot(indir, maxsnapnum, mpc_units=True)
            M200_maxsnap = sim.cat.group_m_crit200
            # Get indices of groups in halo mass range.
            grpind = np.where(M200_maxsnap > M200min / 1e10 * h_scale)[0]
            # Remove groups that are contaminated by low-res particles.
            grpind = get_uncontaminated_groups(sim, grpind)
            if len(grpind) == 0:
                if verbose:
                    print("Found no uncontaminated groups in halo mass range.")
                continue
            # The BCGs are taken to be the main halo of each FoF group.
            subnums = sim.cat.group_firstsub[grpind]

            # Get scale factor of each snapshot up to 'snapnum'
            a = get_scale_factors_for_snapnums(indir, maxsnapnum)
            # Redshift
            z = (1.0 / a) - 1.0

            # Calculate and store the created mass history, which is
            # the formation time of all the constituent particles of the
            # BCG in the last snapshot.
            if verbose:
                print("Calculating creation history.")
            # Loop over subhaloes.
            for subidx, subnum in enumerate(subnums):
                # Create a HDF5 group to store the creation histories.
                grp_c = created.create_group(dirname + "_{:d}".format(subnum))
                grp_c.attrs['M200'] = M200_maxsnap[grpind[subidx]]
                grp_c.create_dataset('Redshift', data=np.c_[zbins[:-1]])

                # Get indices of star particles bound to this subhalo.
                partind = get_stars_in_subhalo(sim, subnum, keep_ages=True)

                # Store mass creation history for all bound stars.
                grp_c.create_dataset(
                        'StellarMassTot',
                        data=np.c_[get_creation_history(sim, partind, zbins)])
                # Calculate creation history for stars in various apertures.
                # Calculate radial distance in (ckpc/h).
                sim.load_starpos()
                r = sim.nearest_dist_3D(sim.starpos[partind],
                                        sim.cat.sub_pos[subnum])
                del sim.starpos
                # for stars within twice the stellar half-mass radius.
                rind = np.where(r <= sim.cat.sub_halfmassradtype[subnum, 4] * 2.0)[0]
                MstarInRad = get_creation_history(sim, partind[rind], zbins)
                grp_c.create_dataset('StellarMassInRad',
                                     data=np.c_[MstarInRad])
                # Do the same but using the initial stellar masses
                # (i.e. no stellar evolution)
                MstarInRad_NSE = get_creation_history(sim, partind[rind],
                                                      zbins, NSE=True)
                grp_c.create_dataset('StellarMassInRad_NSE',
                                     data=np.c_[MstarInRad_NSE])
                # For stars in given aperture radii (in physical kpc).
                for radidx, rad in enumerate(aperture_rad):
                    rind = np.where(
                            r / (1 + sim.header.redshift) / h_scale <= rad)[0]
                    Mstar_rad = get_creation_history(sim, partind[rind], zbins)
                    grp_c.create_dataset('Mstar_{:d}'.format(int(rad)),
                                         data=np.c_[Mstar_rad])
                    Mstar_rad_NSE = get_creation_history(sim, partind[rind],
                                                         zbins, NSE=True)
                    grp_c.create_dataset('Mstar_{:d}_NSE'.format(int(rad)),
                                         data=np.c_[Mstar_rad_NSE])

            # Next store the assembled mass history and other quantities.
            for subidx, subnum in enumerate(subnums):
                # Create group for this subhalo in the main hdf5 groups.
                grp_a = assembled.create_group(dirname +
                                               "_{:d}".format(subnum))
                grp_other = other.create_group(dirname +
                                               "_{:d}".format(subnum))
                grp_insitu = insitu.create_group(dirname +
                                                 "_{:d}".format(subnum))
                grp_insitu_gas = insitu_gas.create_group(
                        dirname + "_{:d}".format(subnum))

                # Retrieve main progenitor branch of subhalo.
                if verbose:
                    print("Retrieving main progenitor branch.")
                # Note that the loadTree function returns a dictionary.
                tree = sublink.loadTree(indir + "sublink/", maxsnapnum, subnum,
                                        fields=fields, onlyMPB=True)
                # Save redshift etc. to hdf5 file.
                grp_a.create_dataset('SnapNum', data=np.c_[tree['SnapNum']],
                                     dtype=int)
                for grp in (grp_a, grp_other, grp_insitu, grp_insitu_gas):
                    grp.create_dataset('Redshift',
                                       data=np.c_[z[tree['SnapNum']]])
                    grp.attrs['M200'] = M200_maxsnap[grpind[subidx]]
                    if (grp.attrs['M200'] != created[dirname + "_{:d}".format(subnum)].attrs['M200']):
                        raise ValueError(
                                "The value of M200 ("+str(grp.attrs['M200']) +
                                ") does not match the value stored for the "
                                "creation history (" +
                                str(created[dirname + "_{:d}".format(subnum)].attrs['M200'])+").")
                grp_a.create_dataset('SubhaloGroup',
                                     data=np.c_[tree['SubhaloGrNr']])
                # Save desired subfind properties.
                grp_a.create_dataset(
                        'StellarMass',
                        data=np.c_[tree['SubhaloMassType'][:, 4]])
                grp_a.create_dataset(
                        'StellarMassInRad',
                        data=np.c_[tree['SubhaloMassInRadType'][:, 4]])
                grp_a.create_dataset(
                        'StellarMassInHalfRad',
                        data=np.c_[tree['SubhaloMassInHalfRadType'][:, 4]])
                grp_a.create_dataset('SFR',
                                     data=np.c_[tree['SubhaloSFR']])
                grp_a.create_dataset('SFRinRad',
                                     data=np.c_[tree['SubhaloSFRinRad']])
                grp_a.create_dataset('SFRinHalfRad',
                                     data=np.c_[tree['SubhaloSFRinHalfRad']])

                # Loop over the snapshots to:
                #   - Calculate the stellar mass in the given apertures.
                #   - Get the host halo mass.
                #   - Calculate the mass of stars formed since the last snapshot.
                if len(aperture_rad) > 0:
                    if verbose:
                        print("Calculating stellar mass within",
                              aperture_rad, "pkpc")
                    # Array to store stellar masses in given apertures.
                    Mstar_rad = np.zeros((len(tree['SnapNum']),
                                          len(aperture_rad)))
                    # Same but for initial stellar masses (no stellar evolution)
                    Mstar_rad_NSE = np.zeros((len(tree['SnapNum']),
                                              len(aperture_rad)))
                    # Stellar mass within 10% of R500
                    Mstar_10p = np.zeros(len(tree['SnapNum']), dtype=float)
                    Mstar_10p_NSE = np.zeros(len(tree['SnapNum']), dtype=float)
                    # Stars within twice the stellar half-mass radius.
                    MstarInRad_NSE = np.zeros(len(tree['SnapNum']), dtype=float)
                    # Mass of stars formed in-situ since the previous snapshot.
                    Mstar_insitu_rad = np.zeros((len(tree['SnapNum']),
                                                 len(aperture_rad)))
                    Mstar_insitu_rad_NSE = np.zeros((len(tree['SnapNum']),
                                                     len(aperture_rad)))
                    # Host FoF group halo mass.
                    M500 = np.zeros(len(tree['SnapNum']), dtype=float)
                    M200 = np.zeros(len(tree['SnapNum']), dtype=float)
                    # Gas properties.
                    # Instantaneous star formation rate in Msun/yr.
                    SFR_rad = np.zeros((len(tree['SnapNum']),
                                        len(aperture_rad)))
                    # Instantaneous specific star formation rate in Msun/yr.
                    sSFR_rad = np.zeros((len(tree['SnapNum']),
                                        len(aperture_rad)))
                    # Time in Gyr between subsequent snapshots.
                    tdiff = np.zeros(len(tree['SnapNum']), dtype=float)
                    # By default the tree arrays are sorted in descending
                    # snapshot number (ascending in redshift).
                    for snapidx, snapnum in enumerate(tree['SnapNum']):
                        if verbose:
                            print("Starting snapshot", snapnum)
                        sim = snap.snapshot(indir, snapnum, mpc_units=True)
                        s = tree['SubfindID'][snapidx]  # Current subhalo ID
                        # Get particles bound to this subhalo.
                        partind = get_stars_in_subhalo(sim, s, keep_ages=True)
                        # Calculate radial distance in (ckpc/h)
                        sim.load_starpos()
                        r = sim.nearest_dist_3D(sim.starpos[partind],
                                                sim.cat.sub_pos[s])
                        del sim.starpos
                        # Calculate stellar masses in given apertures.
                        sim.load_starmass()
                        sim.load_star_initial_mass()
                        # Scale factor of current snapshot.
                        a_now = sim.header.time
                        if a_now != a[snapnum]:
                            raise ValueError("Scale factors don't match. "
                                             "a_now: " + str(a_now) +
                                             " a[snapnum]: " + str(a[snapnum]))
                        # Get scale factor of previous (earlier) snapshot.
                        if snapnum == 0:
                            # There are no earlier snapshots so use a=0.
                            a_prev = 0
                        else:
                            # Time of the next smallest snapshot number
                            # (i.e. the next highest redshift) in the main
                            # progenitor branch.
                            if snapidx < len(tree['SnapNum']) - 1:
                                a_prev = a[tree['SnapNum'][snapidx + 1]]
                            else:
                                a_prev = a[snapnum - 1]
                        if a_prev >= a_now:
                            raise ValueError("a_prev is larger than a_now. " +
                                             "a_now: " + str(a_now) +
                                             " a_prev: " + str(a_prev))
                        # Calculate quantities within the given apertures.
                        for radidx, rad in enumerate(aperture_rad):
                            # Get indices of stars within this radius.
                            rind = np.where(r / (1 + sim.header.redshift) / h_scale <= rad)[0]
                            ind = partind[rind]
                            Mstar_rad[snapidx, radidx] = np.sum(sim.starmass[ind])
                            Mstar_rad_NSE[snapidx, radidx] = np.sum(sim.star_inimass[ind])
                            # Get stars within this radius that were formed
                            # between this snapshot and the earlier one.
                            ageind = np.where((sim.age[ind] < a_now) &
                                              (sim.age[ind] > a_prev))[0]
                            Mstar_insitu_rad[snapidx, radidx] = np.sum(
                                    sim.starmass[ind[ageind]])
                            Mstar_insitu_rad_NSE[snapidx, radidx] = np.sum(
                                    sim.star_inimass[ind[ageind]])
                        # Stellar mass within 10% of R500
                        rind = np.where(r <= 0.1 * sim.cat.group_r_crit500[
                                sim.cat.sub_grnr[s]])
                        Mstar_10p[snapidx] = np.sum(
                                sim.starmass[partind[rind]])
                        Mstar_10p_NSE[snapidx] = np.sum(
                                sim.star_inimass[partind[rind]])
                        # Stellar mass within twice the stellar half-mass
                        # radius but without stellar evolution.
                        rind = np.where(r <= sim.cat.sub_halfmassradtype[s, 4] * 2.0)[0]
                        MstarInRad_NSE = np.sum(
                                sim.star_inimass[partind[rind]])
                        grnr = tree['SubhaloGrNr'][snapidx]
                        M500[snapidx] = sim.cat.group_m_crit500[grnr]
                        M200[snapidx] = sim.cat.group_m_crit200[grnr]
                        del sim.starmass
                        del sim.star_inimass
                        del sim.age

                        # Get the total star formation rate of the gas.
                        gasind = get_gas_in_subhalo(sim, s)
                        sim.load_gaspos()
                        r = sim.nearest_dist_3D(sim.gaspos[gasind],
                                                sim.cat.sub_pos[s])
                        del sim.gaspos
                        sim.load_sfr()  # Msun/yr
                        for radidx, rad in enumerate(aperture_rad):
                            rind = np.where(r / (1 + sim.header.redshift) / h_scale <= rad)[0]
                            SFR_rad[snapidx, radidx] = np.sum(sim.sfr[gasind[rind]])
                            sSFR_rad[snapidx, radidx] = SFR_rad[snapidx, radidx] / Mstar_rad[snapidx, radidx]
                        del sim.sfr
                        # Calculate the time between this snapshot and the
                        # next (lower redshift) so we can estimate the stellar
                        # mass gain due to this SFR between this snapshot and
                        # the next.
                        if snapnum != maxsnapnum:
                            # Get scale factor of next (later) snapshot.
                            if snapidx > 0:
                                a_next = a[tree['SnapNum'][snapidx - 1]]
                            else:
                                # a_next = a[snapnum + 1]
                                raise Exception("snapidx=0 should correspond "
                                                "to maxsnapnum.")
                            if a_next < a_now:
                                raise ValueError(
                                        "a_next is smaller than a_now. "
                                        "a_now: " + str(a_now) + " a_next: " +
                                        str(a_next))
                            tdiff[snapidx] = (cm.age(1.0/a_next - 1.0) -
                                              cm.age(1.0/a_now - 1.0))  # Gyr
                            if tdiff[snapidx] < 0:
                                raise ValueError(
                                        "Negative time difference calculated for "
                                        "snapshot {:d}".format(snapnum))
                        del sim
                    # Save arrays to hdf5 file.
                    for radidx, rad in enumerate(aperture_rad):
                        grp_a.create_dataset('Mstar_{:d}'.format(int(rad)),
                                             data=np.c_[Mstar_rad[:, radidx]])
                        grp_a.create_dataset('Mstar_{:d}_NSE'.format(int(rad)),
                                             data=np.c_[Mstar_rad_NSE[:, radidx]])
                        grp_insitu.create_dataset(
                                'Mstar_{:d}'.format(int(rad)),
                                data=np.c_[Mstar_insitu_rad[:, radidx]])
                        grp_insitu.create_dataset(
                                'Mstar_{:d}_NSE'.format(int(rad)),
                                data=np.c_[Mstar_insitu_rad_NSE[:, radidx]])
                        grp_insitu_gas.create_dataset(
                                'SFR_{:d}'.format(int(rad)),
                                data=np.c_[SFR_rad[:, radidx]])
                        grp_insitu_gas.create_dataset(
                                'sSFR_{:d}'.format(int(rad)),
                                data=np.c_[sSFR_rad[:, radidx]])
                        # Multiply the SFR by the time between snapshots and
                        # save mass in code units (1e10 Msun/h).
                        Mstar_insitu_gas_rad = SFR_rad[:, radidx] * tdiff / 10.0 * h_scale
                        # The in-situ mass gain estimated from the stars is
                        # stored as the mass gain between each snapshot and the
                        # previous (higher z). However the in-situ mass gain
                        # calculated from the gas is calculated between each
                        # snapshot and the next one, so flip it.
                        Mstar_insitu_gas_rad[:-1] = Mstar_insitu_gas_rad[1:]
                        Mstar_insitu_gas_rad[-1] = 0.0
                        grp_insitu_gas.create_dataset(
                                'Mstar_{:d}_NSE'.format(int(rad)),
                                data=np.c_[Mstar_insitu_gas_rad])
                    # data=np.c_[np.flip(np.cumsum(np.flip(Mstar_insitu_rad[:, radidx])))])
                    grp_a.create_dataset('Mstar_10p', data=np.c_[Mstar_10p])
                    grp_a.create_dataset('Mstar_10p_NSE',
                                         data=np.c_[Mstar_10p_NSE])
                    grp_a.create_dataset('StellarMassInRad_NSE',
                                         data=np.c_[MstarInRad_NSE])
                    grp_other.create_dataset('M500', data=np.c_[M500])
                    grp_other.create_dataset('M200', data=np.c_[M200])

    print("Took", timer() - starttime, "seconds.")
    print("Output to", outfile)


def plot_growth_histories(infile, xname='Redshift', yname='StellarMassInRad',
                          assembled=True, created=True, insitu=True,
                          mass_selected=False, redshifts=[],
                          get_BCG_props_args={}, get_BCG_props_kwargs={},
                          cumulative=True,
                          use_lookback_time=False,
                          plot_obs=False, plot_sims=False,
                          M200lims_list=[[1e14, 1e16]],
                          normalise=True, stack=True, plot_scatter=False, lw=4,
                          xlims=None, ylims=None, add_title=True, leg_len=None,
                          outdir="/data/curie4/nh444/project1/stars/",
                          out_suffix="", h_scale=0.6774, omega_m=0.3089):
    """Plot BCG mass growth history using output of save_growth_histories().

    Arguments:
        infile: Full path of hdf5 file.
        xname, yname: Names of hdf5 datasets to plot on x and y axes.
        assembled: Plot the assembled mass history of the main progenitor.
        created: Plot the creation mass history of the stars in the BCGs
                 at the final snapshot.
        insitu: Plot mass growth due to in-situ star formation.
        mass_selected: Plot the median quantity vs redshift for a halo mass
                       selected sample at each redshift.
        cumulative: Plot the cumulative mass growth of the BCGs from late to
                    early times. Otherwise plot the mass gain per time bin.
        use_lookback_time: x axis is lookback time in Gyr. Else redshift.
        plot_obs: Plot observational constraints for comparison.
        plot_sims: Plot other simulation results for comparison.
        M200lims: Minimum and maximum group mass to plot.
        normalise: Normalise y axis by its value in the last snapshot.
        stack: Plot median growth history.
        plot_scatter: Plot the individual growth histories as well as
                      the median of the stack.
        xlims: (list) Lower and upper limits of the x axis.
        ylims: (list) Lower and upper limits of the y axis.
        add_title: Add title to axis showing halo mass range.
        leg_len: (int or None) Length of first legend (upper left). Other
                 entries go into second legend (lower right). If None, only
                 one legend is used.
        out_suffix: suffix on output file name.
    """
    import matplotlib.pyplot as plt
    import h5py
    from sim_python import cosmo
    if mass_selected:
        from stars.plot_stellar_mass import get_BCG_SFR
    cm = cosmo.cosmology(h_scale, omega_m)

    fig, ax = plt.subplots(figsize=(7, 7))
    unit = r"[$M_{\odot}$]"
    if yname == 'Mstar_30':
        ytitle = r"M$_{\mathrm{\star, 30 kpc}}$"
    elif yname == 'Mstar_30_NSE':
        ytitle = r"M$_{\mathrm{\star \, NSE, \, 30 kpc}}$"
    elif yname == 'Mstar_50':
        ytitle = r"M$_{\mathrm{\star, 50 kpc}}$"
    elif yname == 'Mstar_50_NSE':
        ytitle = r"M$_{\mathrm{\star \, NSE, \, 50 kpc}}$"
    elif yname == 'Mstar_100':
        ytitle = r"M$_{\mathrm{\star, 100 kpc}}$"
    elif yname == 'StellarMassInRad':
        ytitle = r"M$_{\mathrm{\star, \, 2 r_{1/2}}}$"
    elif yname == 'M200':
        ytitle = r"M$_{\mathrm{200}}$"
    elif yname == 'M500':
        ytitle = r"M$_{\mathrm{500}}$"
    elif yname[:4] == 'sSFR':
        ytitle = r"sSFR"
        unit = r"[yr$^{-1}$]"
    elif yname[:3] == 'SFR':
        ytitle = r"SFR"
        unit = r"[M$_{\odot}$ yr$^{-1}$]"
    else:
        ytitle = r"M$_{\mathrm{\star}}$"
    if normalise:
        ytitle = ytitle + "$(z)$ / " + ytitle + "$(z=0)$"
    else:
        ytitle += " " + unit
    ax.set_ylabel(ytitle)

    ax.set_xlabel("Redshift")
    if xlims is None:
        ax.set_xlim(0.0, 3.0)
    else:
        ax.set_xlim(xlims[0], xlims[1])
    if yname[:3] in ("SFR", "sSF"):
        ax.set_yscale('log')
    elif yname in ("M200", "M500"):
        ax.set_yscale('log')
    else:
        if normalise:
            if ylims is None:
                ax.set_ylim(0, 1.3)
        elif not cumulative:
            if ylims is None:
                ax.set_ylim(0, 4e11)
        else:
            ax.set_yscale('log')
            if ylims is None:
                ax.set_ylim(1e10, 5e12)
    if ylims is not None:
        ax.set_ylim(ylims[0], ylims[1])

    if use_lookback_time:
        ax.set_xlabel("Lookback Time [Gyr]")
        ax.set_xlim(0.0, 12.0)
        # Create a second axis with redshift.
        ax_z = ax.twiny()
        ax_z.set_xlabel("Redshift")
        ax_z.set_xlim(0.0, 12.0)
        zlabels = [0.1, 0.5, 1.0, 2.0, 3.0]
        ax_z.set_xticks(cm.lookback(np.array(zlabels)), minor=False)
        ax_z.set_xticklabels(['{:.1f}'.format(zlabel) for zlabel in zlabels])

    def plot_from_file(h5GrpName='assembled', label="Median",
                       M200lims=[1e14, 1e16],
                       xbins=None, cumulative=False, differential=False,
                       scatter=True, CI=True, ls='solid', col='#24439b'):
        """ Plot a given quantity versus redshift.

        Arguments:
            scatter: If stack, also plots the individual histories.
            CI: Plot the confidence region about the stacked (median) history.
        """
        M200all = []
        if stack:
            xall = []
            yall = []
            nstack = 0
        # Get factor to multiply array from file to get into correct units.
        if yname[:3] == "SFR":
            # Msun/yr --> Msun/yr
            unit_fac = 1.0
        elif yname[:4] == "sSFR":
            unit_fac = 1.0 / (1e10 / h_scale)
        else:
            # 1e10 Msun/h --> Msun
            unit_fac = 1e10 / h_scale

        with h5py.File(infile, 'r') as f:
            if f.attrs['h_scale'] != h_scale:
                raise ValueError("The value of h_scale does not match the "
                                 "value stored in the input file.")
            if f.attrs['omega_m'] != omega_m:
                raise ValueError("The value of omega_m does not match the "
                                 "value stored in the input file.")
            grp = f[h5GrpName]
            grp_names = grp.keys()
            for grp_name in grp_names:
                M200 = grp[grp_name].attrs['M200'] * 1e10 / h_scale
                if M200 > M200lims[0] and M200 < M200lims[1]:
                    M200all.append(M200)
                    z = grp[grp_name]['Redshift'].value[:, 0]  # redshifts
                    if use_lookback_time:
                        x = cm.lookback(z)
                    else:
                        x = grp[grp_name][xname].value[:, 0]
                    y = grp[grp_name][yname].value[:, 0] * unit_fac
                    # Remove any infinities.
                    ind = np.isfinite(y)
                    x = x[ind]
                    y = y[ind]

                    # The in-situ mass gain is that between each snapshot and
                    # the previous (higher z) one. But we should plot at the
                    # midpoint instead.
                    if h5GrpName[:7] == "in-situ":
                        x = (x[1:] + x[:-1]) / 2.0
                        y = y[:-1]
#                    try:
#                        y = grp[grp_name][yname].value[:, 0] * 1e10 / h_scale
#                    except KeyError:
#                        y = grp[grp_name]['SubhaloMassInRadType'].value[:, 0] * 1e10 / h_scale
                    if np.any(np.diff(z) < 0):
                        print("z =", z)
                        raise ValueError("The code expects the redshift array "
                                         "to be in ascending order.")
                    if xbins is None:
                        if cumulative:
                            y = np.cumsum(y[::-1])[::-1]
                        elif differential:
                            # Get the difference between successive points.
                            y = -1.0 * np.diff(y)
                            # Append zero to make array the correct length.
                            y = np.append(y, 0.0)
                        if normalise:
                            # Determine the normalisation factor.
                            if h5GrpName[:7] == "in-situ":
                                # Normalise by the final assembled mass at z=0.
                                norm = 1.0 / (f['assembled'][grp_name][yname].value[0, 0] * unit_fac)
                            else:
                                # Normalise by the z=0 value.
                                norm = 1.0 / y[0]
                            y *= norm
                        if stack and scatter:
                            ax.plot(x, y, ls=ls, lw=3, c=col, alpha=0.3)
                        elif not stack:
                            ax.plot(x, y, ls=ls, lw=3, c=col,
                                    label=str(grp_name).replace("_", r"\_"))
                    if stack:
                        xall.append(x)
                        yall.append(y)
                        nstack += 1

        print("Median M200 =", np.median(M200all) / 1e14)
        if stack and len(xall) > 0:
            # Get the history with the most time points.
            xstack = xall[0]
            for x in xall:
                if len(x) > len(xstack):
                    xstack = x
            # Interpolate all histories onto the same time resolution so that
            # we can stack them.
            ystack = np.interp(xstack, xall[0], yall[0], right=0)
            for i in range(nstack):
                ystack = np.vstack((ystack, np.interp(
                        xstack, xall[i], yall[i], right=0)))
            # Get the median of the stack.
            ymed = np.median(ystack, axis=0)
            # Plotting.
            if xbins is None:
                ax.plot(xstack, ymed,
                        ls=ls, lw=lw, c=col, label=label)
                if CI:
                    # Plot 16% and 84% percentiles
                    plt.fill_between(xstack,
                                     np.percentile(ystack, 16.0, axis=0),
                                     np.percentile(ystack, 84.0, axis=0),
                                     facecolor=col, alpha=0.1, edgecolor=col,
                                     hatch='///')
            else:
                # Let's rebin the stack rather than the individual curves since
                # the latter can have negative mass growth in some bins.
                if differential:
                    # Get the difference between successive points.
                    ymed = -1.0 * np.diff(ymed)
                    ymed = np.append(ymed, 0.0)  # make it the correct length.
                # Sum up bins into the larger bins.
                ymed = np.histogram(xstack, bins=xbins, weights=ymed)[0]
                if cumulative:
                    ymed = np.cumsum(ymed[::-1])[::-1]
                ax.step(xbins[:-1], ymed, where='post',
                        ls=ls, lw=lw, c=col, label=label)

        return M200all
    ls = ['-', '--', ':']
    for Midx, M200lims in enumerate(M200lims_list):
        if Midx == 0:
            CI = True
        else:
            CI = False
        if yname in ("M500", "M200"):
            M200all = plot_from_file(h5GrpName='other', M200lims=M200lims,
                                     ls=ls[Midx], scatter=plot_scatter, CI=CI)
        elif yname[:3] in ("SFR", "sSF"):
            if yname[:3] == 'SFR' and Midx == 0:
                label = r"median SFR($<$" + "{:d}".format(
                        int(yname[4:])) + " kpc)"
                label = "Main Progenitors"
            elif yname[:3] == 'sSF' and Midx == 0:
                label = r"median sSFR($<$" + "{:d}".format(
                        int(yname[5:])) + " kpc)"
            else:
                label = None
            M200all = plot_from_file(h5GrpName='in-situ_SF_gas', label=label,
                                     M200lims=M200lims, ls=ls[Midx],
                                     scatter=plot_scatter, CI=CI)
        elif cumulative:  # cumulative stellar mass growth
            if assembled:
                # The assembled mass is already cumulative.
                M200all = plot_from_file(h5GrpName='assembled',
                                         label="Assembled Mass" if Midx==0 else None,
                                         M200lims=M200lims, ls=ls[Midx],
                                         scatter=plot_scatter, CI=CI)
            if created:
                M200all = plot_from_file(h5GrpName='created',
                                         label="Created Mass" if Midx==0 else None,
                                         cumulative=True, M200lims=M200lims,
                                         scatter=plot_scatter, CI=CI,
                                         col='orange', ls=ls[Midx])
            if insitu:
                M200all = plot_from_file(h5GrpName='in-situ_SF',
                                         label="In-Situ SF (stars)" if Midx==0 else None,
                                         M200lims=M200lims, cumulative=True,
                                         scatter=plot_scatter, CI=CI,
                                         col='r', ls=':')
                M200all = plot_from_file(h5GrpName='in-situ_SF_gas',
                                         label="In-Situ SF (gas)" if Midx==0 else None,
                                         M200lims=M200lims, cumulative=True,
                                         scatter=plot_scatter, CI=CI,
                                         col='g', ls='dashdot')
        else:
            # Need to re-bin into wider time bins to reduce noise and to
            # ensure that all quantities have the same width bins.
            if xname == 'Redshift':
                xbins = np.linspace(0.0, 5, num=26)
            else:
                raise NotImplementedError("xname '" + str(xname) +
                                          "' not recognised.")
            if use_lookback_time:
                xbins = np.linspace(0.0, 14.0, 8)
            if assembled:
                # Take the differential of the cumulative assembled mass.
                M200all = plot_from_file(h5GrpName='assembled',
                                         label="Assembled Mass" if Midx==0 else None,
                                         xbins=xbins, M200lims=M200lims,
                                         differential=True, ls=ls[Midx],
                                         scatter=plot_scatter, CI=CI)
            if created:
                M200all = plot_from_file(h5GrpName='created',
                                         label="Created Mass" if Midx==0 else None,
                                         xbins=xbins, M200lims=M200lims,
                                         scatter=plot_scatter, CI=CI,
                                         col='orange', ls=ls[Midx])
            if insitu:
                M200all = plot_from_file(h5GrpName='in-situ_SF',
                                         label="In-Situ Star Formation (stars)" if Midx==0 else None,
                                         xbins=xbins, M200lims=M200lims,
                                         scatter=plot_scatter, CI=CI,
                                         col='r', ls=':')
                M200all = plot_from_file(h5GrpName='in-situ_SF_gas',
                                         label="In-Situ Star Formation (gas)" if Midx==0 else None,
                                         xbins=xbins, M200lims=M200lims,
                                         scatter=plot_scatter, CI=CI,
                                         col='g', ls='dashdot')
        if mass_selected and Midx == 0:
            get_BCG_props_kwargs['M200lim'] = M200lims
            get_BCG_props_kwargs['h_scale'] = h_scale
            c = 'orange'  # '#ED7600'
            for zidx, redshift in enumerate(redshifts):
                get_BCG_props_kwargs['redshift'] = redshift
                if yname[:3] in ("SFR", "sSF"):
                    SFR, m200, m500 = get_BCG_SFR(
                            get_BCG_props_args['basedir'],
                            get_BCG_props_args['dirnames'], int(yname[4:]),
                            **get_BCG_props_kwargs)
                    if SFR is not None:
                        if use_lookback_time:
                            x = cm.lookback(redshift)
                        else:
                            x = redshift
                        med = np.median(SFR)
                        print("z=", redshift, "med =", med)
                        yerr = [[med - np.percentile(SFR, 16.0)],
                                [np.percentile(SFR, 84.0) - med]]
                        ax.errorbar(x, med, yerr=yerr, marker='D', ms=6, lw=2,
                                    c=c, mec=c, mfc=c if Midx == 0 else 'none',
                                    zorder=4,
                                    label=r"$M_{200}(z) >" +
                                    "{:.0f}".format(M200lims[0] / 1e14) +
                                    r"\times 10^{14} \; M_{\odot}$" if zidx == 0 else None)
#                                    label=r"$\mathrm{log}_{10}M_{200}(z) = [" +
#                                    "{:.1f}, {:.1f}".format(
#                                            np.log10(M200lims[0]),
#                                            np.log10(M200lims[1])) +
#                                    r"] \; M_{\odot}$" if zidx == 0 else None)
    if plot_obs:
        plot_obs_data(ax, yname, cm=cm, normalise=normalise,
                      use_lookback_time=use_lookback_time, h_scale=h_scale)
    if plot_sims:
        plot_sims_data(ax, yname, cm=cm, normalise=normalise,
                       use_lookback_time=use_lookback_time,
                       lw=lw, h_scale=h_scale)

    if add_title and len(M200lims_list) == 1 and len(M200all) > 0:
        ax.set_title(r"{:.1f} ".format(np.log10(np.min(M200all))) +
                     "$<$ log$_{10}$M$_{200} (z=0)$ $<$ " +
                     "{:.1f}".format(np.log10(np.max(M200all))),
                     fontsize='small')

    leg_kwargs = dict(fontsize='small', borderaxespad=1)
#    if not cumulative:
#        # leg_kwargs['title'] = "Mass Gain per bin"
#        leg_kwargs['frameon'] = True
    handles, labels = ax.get_legend_handles_labels()
    if leg_len is None:
        ax.legend(handles, labels, loc='best', **leg_kwargs)
    else:
        leg1 = ax.legend(handles[:leg_len], labels[:leg_len],
                         loc='upper left', **leg_kwargs)
        ax.add_artist(leg1)
        ax.legend(handles[leg_len:], labels[leg_len:], loc='lower right',
                  **leg_kwargs)

    outfile = outdir + "BCG_growth_history" + out_suffix + ".pdf"
    fig.savefig(outfile, bbox_inches='tight')
    print("Saved to", outfile)


def plot_creation_histories(basedir, inputs, M200min=3e13,
                            normalise=True,
                            # aperture_rad=[30, 50, 100],
                            h_scale=0.6774, verbose=True):
    import matplotlib.pyplot as plt
    import sublink

    fig, ax = plt.subplots(figsize=(7, 7))
    ax.set_xlabel("Redshift")
    ax.set_yscale('log')

    fields = ['SnapNum', 'SubfindID', 'SubhaloMassInHalfRadType',
              'SubhaloMassType', 'SubhaloMassInRadType']
    for input_ in inputs:
        dirname = input_[0]
        indir = basedir + dirname + "/"
        # Number of last snapshot
        maxsnapnum = input_[1]

        # Get scale factor of each snapshot up to 'snapnum'
        a = get_scale_factors_for_snapnums(indir, maxsnapnum)
        # Redshift
        z = (1.0 / a) - 1.0

        # Get subhalo sample.
        sim = snap.snapshot(indir, maxsnapnum, mpc_units=True)
        M200 = sim.cat.group_m_crit200
        grpind = np.where(M200 > M200min / 1e10 * h_scale)[0]
        grpind = get_uncontaminated_groups(sim, grpind)
        if len(grpind) == 0:
            continue
        subnums = sim.cat.group_firstsub[grpind]
        # Loop over subhaloes.
        for subidx, subnum in enumerate(subnums):
            # Plot mass growth of main progenitor.
            # Retrieve main progenitor branch of subhalo.
            tree = sublink.loadTree(indir + "sublink/", maxsnapnum, subnum,
                                    fields=fields, onlyMPB=True)
            if normalise:
                norm = tree['SubhaloMassInRadType'][0, 4]
            else:
                norm = 1.0
            p = ax.plot(z[tree['SnapNum']],
                        tree['SubhaloMassInRadType'][:, 4] / norm,
                        ls='-', lw=2)
            aperture_rad = [sim.cat.sub_halfmassradtype[subnum, 4] * 2.0 /
                            (1 + sim.header.redshift) / h_scale]

            # Get star particles bound to this subhalo.
            partind = np.arange(sim.cat.sub_offsettab[subnum, 4],
                                sim.cat.sub_offsettab[subnum, 4] +
                                sim.cat.sub_lentab[subnum, 4],
                                dtype=int)
            # Exclude GFM wind particles, which are type 4 but
            # count towards the gas mass not the stellar mass.
            # They have a formation time ('age') less than zero.
            sim.load_stellar_ages()
            partind = partind[np.where(sim.age[partind] > 0.0)[0]]
            # Calculate radial distance in ckpc/h
            sim.load_starpos()
            r = sim.nearest_dist_3D(sim.starpos[partind],
                                    sim.cat.sub_pos[subnum])
            del sim.starpos
            # Load initial mass of star particles (i.e. no mass loss
            # due to stellar evolution).
            # sim.load_star_initial_mass()
            sim.load_starmass()
            # Loop over aperture radii.
            for radidx, rad in enumerate(aperture_rad):
                # Get stars within given aperture.
                ind = np.where(r /
                               (1 + sim.header.redshift) / h_scale <= rad)[0]
                # Formation redshift of these stars.
                zf = (1.0 / sim.age[partind[ind]]) - 1.0
                # Initial masses of these stars.
                mf = sim.starmass[partind[ind]]
                # Plot cumulative mass.
                indsort = zf.argsort()[::-1]
                cumsum = np.cumsum(mf[indsort])
                if normalise:
                    norm = cumsum[-1]
                else:
                    norm = 1.0
                ax.plot(zf[indsort], cumsum / norm,
                        ls='--', lw=2, c=p[0].get_color())


def get_scale_factors_for_snapnums(indir, maxsnapnum):
    """Get array of the scale factor of each snapshot from 0 to maxsnapnum"""
    snapnums = range(maxsnapnum + 1)
    a = np.zeros(maxsnapnum + 1)
    for i, snapnum in enumerate(snapnums):
        try:
            sim = snap.snapshot(indir, snapnum, header_only=True,
                                read_params=False, mpc_units=True)
            a[i] = sim.header.time
        except IOError:
            # Snapshot doesn't exist.
            a[i] = 0.0
    return a


def get_stars_in_subhalo(sim, subnum, keep_ages=False):
    # Get particles bound to this subhalo.
    partind = np.arange(sim.cat.sub_offsettab[subnum, 4],
                        sim.cat.sub_offsettab[subnum, 4] +
                        sim.cat.sub_lentab[subnum, 4],
                        dtype=int)
    # Exclude GFM wind particles, which are type 4 but
    # count towards the gas mass not the stellar mass.
    # They have a formation time ('age') less than zero.
    sim.load_stellar_ages()
    partind = partind[np.where(sim.age[partind] > 0.0)[0]]
    if not keep_ages:
        del sim.age

    return partind


def get_gas_in_subhalo(sim, subnum):
    # Get gas cells bound to this subhalo.
    partind = np.arange(sim.cat.sub_offsettab[subnum, 0],
                        sim.cat.sub_offsettab[subnum, 0] +
                        sim.cat.sub_lentab[subnum, 0],
                        dtype=int)

    return partind


def get_creation_history(sim, partind, zbins, NSE=False):
    # Formation redshift of these stars.
    zf = (1.0 / sim.age[partind]) - 1.0
    if NSE:
        # Use initial masses of these stars.
        try:
            sim.star_inimass
        except AttributeError:
            sim.load_star_initial_mass()
        mf = sim.star_inimass[partind]
    else:
        # Use final stellar masses.
        try:
            sim.starmass
        except AttributeError:
            sim.load_starmass()
        mf = sim.starmass[partind]
    # Bin them up.
    # return np.cumsum(np.histogram(zf, bins=zbins, weights=mf)[0][::-1])[::-1]
    return np.histogram(zf, bins=zbins, weights=mf)[0]


def plot_sims_data(ax, yname, cm=None, normalise=True, use_lookback_time=False,
                   lw=4, h_scale=0.6774):
    from matplotlib.patches import Polygon

    if use_lookback_time and cm is None:
        raise ValueError("'cm' is a required argument if use_lookback_time "
                         "evaluates to True.")
    if yname == "StellarMassInRad":
        if use_lookback_time and normalise:
#            # Confidence region.
#            deL = np.loadtxt("/data/vault/nh444/ObsData/stellar_mass/"
#                             "de_Lucia_2007_median_growth_confidence.csv",
#                             delimiter=",")
#            patch = Polygon(deL, color='0.5', ec='none', alpha=0.5)
#            ax.axes.add_patch(patch)
            # Median assembled
            deL = np.loadtxt("/data/vault/nh444/ObsData/stellar_mass/"
                             "de_Lucia_2007_median_growth.csv", delimiter=",")
            ax.plot(deL[:, 0], deL[:, 1], c='0.1', lw=lw, ls=':',
                    label="De Lucia \& Blaizot 2007")
#            # Median created
#            deL = np.loadtxt("/data/vault/nh444/ObsData/stellar_mass/"
#                             "de_Lucia_2007_median_created.csv", delimiter=",")
#            ax.plot(deL[:, 0], deL[:, 1], c='0.5', lw=lw, ls='--')
# =============================================================================
#     if yname == "sSFR_50":
#         if use_lookback_time:
#             RF = np.genfromtxt("/data/vault/nh444/ObsData/SFR/BCGs/"
#                                "Ragone-Figueroa2018_sSFR_vs_z.csv",
#                                delimiter=",")
#             ax.plot(RF[:, 0], RF[:, 1], c='0.5', lw=lw, ls='--',
#                     label="Ragone-Figueroa+ 18")
# =============================================================================
# =============================================================================
#     if yname == "Mstar_30":
#         if use_lookback_time:
#             RF = np.genfromtxt("/data/vault/nh444/ObsData/stellar_mass/"
#                                "Ragone-Figueroa2018_M30_vs_z.csv",
#                                delimiter=",")
#             if normalise:
#                 norm = 1.0 / 10**RF[0, 1]
#             else:
#                 norm = 0.72 / h_scale
#             ax.plot(RF[:, 0], 10**RF[:, 1] * norm,
#                     c='0.5', lw=lw, ls=':',
#                     label="Ragone-Figueroa+ 18")
# =============================================================================


def plot_obs_data(ax, yname, cm=None, normalise=True, use_lookback_time=False,
                  h_scale=0.6774):
    if use_lookback_time and cm is None:
        raise ValueError("'cm' is a required argument if use_lookback_time "
                         "evaluates to True.")

    SalpShift = 10**-0.24  # conversion factor from Salpeter to Chabrier IMF

    mew = 1.4
    Bell_kwargs = {'marker': 'o', 'ms': 8, 'c': 'k', 'mec': 'k', 'mfc': 'none',
                   'mew': mew, 'ls': 'none'}
    Lidman_kwargs = {'marker': 'p', 'ms': 10, 'c': 'k', 'mec': 'k', 'mfc': '0.5',
                     'mew': mew, 'ls': 'none'}
    Lin13_kwargs = {'marker': 's', 'ms': 8, 'ls': 'none', 'c': 'k',
                    'mew': mew, 'label': "Lin+ 13 ($z_n = 0.05$)"}

    if yname in ("Mstar_30", "Mstar_50") and normalise:
        Lin = np.genfromtxt("/data/vault/nh444/ObsData/stellar_mass/" +
                            "Lin2013_BCG_vs_z.csv", delimiter=",")
        x = Lin[:, 0]  # redshift
        if use_lookback_time:
            x = cm.lookback(x)
        if normalise:
            y = Lin[:, 1] / Lin[-1, 1]
            yerr_low = y * np.sqrt((Lin[:, 2] / Lin[:, 1])**2 +
                                   (Lin[-1, 2] / Lin[-1, 1])**2)
            yerr_high = y * np.sqrt((Lin[:, 3] / Lin[:, 1])**2 +
                                    (Lin[-1, 3] / Lin[-1, 1])**2)
            yerr_low[-1] = 0.0
            yerr_high[-1] = 0.0
            ax.errorbar(x, y, yerr=[yerr_low, yerr_high], **Lin13_kwargs)
        else:
            raise NotImplementedError("Need to account for different H0.")

        if normalise:
            Lid = np.genfromtxt("/data/vault/nh444/ObsData/stellar_mass/" +
                                "Lidman_BCG_mass_growth.csv", delimiter=",")
            x = Lid[:, 0]  # redshift
            if use_lookback_time:
                x = cm.lookback(x)
            ax.errorbar(x, Lid[:, 3],
                        yerr=Lid[:, 4],
                        label="Lidman+ 12 ($z_n = 0.2$)", **Lidman_kwargs)
#            # reference bin
#            ax.errorbar(x[0], Lid[0, 3], marker='p',
#                        ms=8, mec='r', mew=1.8, mfc='none', ls='none',
#                        zorder=5)

        Bell = np.genfromtxt("/data/vault/nh444/ObsData/stellar_mass/" +
                             "Bellstedt_2016_redshift.csv", delimiter=",")
        x = Bell[:, 0]  # redshift
        if use_lookback_time:
            x = cm.lookback(x)
        if normalise:
            norm = Bell[0, 1]
        else:
            raise NotImplementedError("Need to account for different H0.")
            norm = 1.0
        ax.errorbar(x, Bell[:, 1] / norm,
                    yerr=[Bell[:, 2] / norm, Bell[:, 3] / norm],
                    label="Bellstedt+ 16 ($z_n = 0.18$)", **Bell_kwargs)
#        # reference bin
#        ax.errorbar(x[0], Bell[0, 1] / norm,
#                    marker='o', ms=8, mec='r', mew=1.8, mfc='none', ls='none',
#                    zorder=5)

    if yname == "M200":
        Bell = np.genfromtxt("/data/vault/nh444/ObsData/stellar_mass/" +
                             "Bellstedt_2016.csv", delimiter=",")
        # Use photometric redshifts where spectroscopic are not given.
        Bell[np.isnan(Bell[:, 1]), 1] = Bell[np.isnan(Bell[:, 1]), 2]
        x = Bell[:, 1]  # redshift
        # We compare to their estimate of the BCG growth relative to their
        # lowest redshift bin (z=0-0.25)
        ind = np.where(x < 0.25)[0]
        if use_lookback_time:
            x = cm.lookback(x)
        ax.errorbar(x[ind], Bell[ind, 8] * 1e15 * 0.7 / h_scale,
                    yerr=Bell[ind, 9] * 1e15 * 0.7 / h_scale,
                    label="Bellstedt+ 16 ($z < 0.25$)", **Bell_kwargs)

        Lin = np.loadtxt("/data/vault/nh444/ObsData/stellar_mass/" +
                         "Lin2013_halo_mass_vs_z.csv", delimiter=",")
        x = Lin[:, 0]  # redshift
        if use_lookback_time:
            x = cm.lookback(x)
        y = Lin[:, 1] * 1e14 * 0.7 / h_scale  # M200 in Msun
        if normalise:
            y /= y[0]
        ax.errorbar(x, y, ls='-', lw=3, c='k', label='Lin+ 13')

    if yname[:3] in ["SFR", "sSF"]:
        # McDonald+ 2016: Means in redshift bins with uncertainties.
        dat = np.loadtxt("/data/vault/nh444/ObsData/SFR/BCGs/McDonald_2016_BCG_SFR.csv", delimiter=",")
        if use_lookback_time:
            # Convert redshifts to lookback times.
            dat[:, 0] = cm.lookback(dat[:, 0])
            dat[:, 1] = cm.lookback(dat[:, 1])
        # convert stellar masses and SFRs from Salpeter to Chabrier IMF
        dat[:, [3, 4, 5]] *= SalpShift
        if yname[:3] == "SFR":
            # SFR derived from luminosity has h^-2 scaling
            dat[:, [4, 5]] *= (0.7 / h_scale)**2
            # lower-left corner of rectangle
            xys = np.column_stack((dat[:, 0], dat[:, 4]))
            widths = dat[:, 1] - dat[:, 0]
            heights = dat[:, 5] - dat[:, 4]
        elif yname[:3] == "sSF":
            dat[:, [6, 7]] /= 1e9  # Convert from Gyr^-1 to yr^-1
            # lower-left corner of rectangle
            xys = np.column_stack((dat[:, 0], dat[:, 6]))
            widths = dat[:, 1] - dat[:, 0]
            heights = dat[:, 7] - dat[:, 6]
        import matplotlib.patches as patches
        for i, (xy, width, height) in enumerate(zip(xys, widths, heights)):
            rect = patches.Rectangle(
                    xy, width, height, lw=2, fill=False, edgecolor='0.1',
                    zorder=3, label="McDonald+ 2016" if i == 0 else None)
            ax.add_patch(rect)

        # Bonaventura+ 2017: Median stacked SFR
        if yname[:3] == "SFR":
            Bon = np.genfromtxt("/data/vault/nh444/ObsData/SFR/BCGs/" +
                                "Bonaventura17_BCG_SFR.csv", delimiter=",")
            # SFR derived from luminosity has h^-2 scaling
            Bon[:, [1, 2, 3]] *= (0.7 / h_scale)**2
            y = Bon[:, 1]
            yerr_low, yerr_high = Bon[:, 2], Bon[:, 3]
        elif yname[:3] == "sSF":
            Bon = np.genfromtxt("/data/vault/nh444/ObsData/SFR/BCGs/" +
                                "Bonaventura17_BCG_sSFR.csv", delimiter=",")
            y = Bon[:, 1] / 1e9  # yr^-1
            yerr_low, yerr_high = np.zeros_like(y), np.zeros_like(y)
        if use_lookback_time:
            x = cm.lookback(Bon[:, 0])
        else:
            x = Bon[:, 0]
        ax.errorbar(x, y, yerr=[yerr_low, yerr_high],
                    marker='o', ms=10, c='0.1', lw=2, ls='none',
                    label="Bonaventura+ 17")

        # Fraser-McKelvie+ 2014: Plot median SFR at median redshift.
        Fras = np.genfromtxt("/data/vault/nh444/ObsData/SFR/BCGs/" +
                             "Fraser-McKelvie2014.csv", delimiter=",")
        # Only consider clusters with identified BCGs (redshift not NaN).
        ind = np.where(np.isfinite(Fras[:, 1]))[0]
        # Treat SFR non-detections as zero.
        Fras[np.isnan(Fras[:, 6]), 6] = 0.0
        Fras[:, 6] *= (0.7 / h_scale)**2
        if use_lookback_time:
            x = cm.lookback(np.median(Fras[ind, 1]))
        else:
            x = np.median(Fras[ind, 1])
        if yname[:3] == "SFR":
            med = np.median(Fras[ind, 6])
            ax.errorbar(x, med,
                        yerr=[[med - np.percentile(Fras[ind, 6], 16.0)],
                              [np.percentile(Fras[ind, 6], 84.0) - med]],
                        marker='s', ms=10, c='0.4', lw=2, ls='none',
                        label="Fraser-McKelvie+ 14")

        # Liu 2012 - SFRs measured within 3'' DIAMETER (~10 kpc at z=0.2)
        Liu = np.genfromtxt("/data/vault/nh444/ObsData/SFR/BCGs/" +
                            "Liu2012_BCG_SFR.csv", delimiter=",")
        Liu[:, 12] *= (0.7/h_scale)**2
        ind = np.where(Liu[:, 4] < 0.2)[0]
        # Approximate Petrosian stellar masses.
        if use_lookback_time:
            x = cm.lookback(np.median(Liu[ind, 4]))
        else:
            x = np.median(Liu[ind, 4])
        if yname[:3] == "SFR":
            med = np.median(Liu[ind, 12])
            ax.errorbar(x, med,
                        yerr=[[med - np.percentile(Liu[ind, 12], 16.0)],
                              [np.percentile(Liu[ind, 12], 84.0) - med]],
                        ls='none', lw=2, marker='d', ms=12,
                        c='0.7', mfc='0.7', mec='none',
                        label=r"Liu+ 12 ($z < 0.2$)")


def get_SFRs(basedir, simdirs, zoomdirs, zooms, z, apsize=50,
             M500lim=[1e10, 1e18], M200lim=[1e10, 1e18], h_scale=0.6774):
    """ Get BCG SFRs at given redshift for given halo mass limits.
    """
    import os
    import h5py

    fout_base = "stellar_masses_"
    basedir = os.path.normpath(basedir)+"/"

    apsize = int(apsize)
    if apsize == 10:
        key = "SFR_BCG_10kpc"
    elif apsize == 30:
        key = "SFR_BCG_30kpc"
    elif apsize == 50:
        key = "SFR_BCG_50kpc"
    elif apsize == 100:
        key = "SFR_BCG_100kpc"
    else:
        raise ValueError("HDF5 key unknown for apsize "+str(apsize))
    SFR = []
    for simidx, simdir in enumerate(simdirs):
        simdir = os.path.normpath(simdir)+"/"

        suffix = "{:.2f}".format(z)
        filename = basedir+simdir+fout_base+"z"+suffix.zfill(5)+".hdf5"
        if not os.path.exists(filename):
            raise IOError("File does not exist: "+str(filename))
        with h5py.File(filename) as f:
            M500 = f['M500'].value[:, 0] / h_scale
            M200 = f['M200'].value[:, 0] / h_scale
            ind = np.where((M500 >= M500lim[0]) & (M500 <= M500lim[1]) &
                           (M200 >= M200lim[0]) & (M200 <= M200lim[1]))[0]
            if len(ind) > 0:
                SFR = np.concatenate((SFR, f[key].value[ind, 0]))


if __name__ == "__main__":
    main()
