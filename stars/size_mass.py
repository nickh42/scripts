#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed May 30 14:49:30 2018
Plots galaxy size versus stellar mass compared to IllustrisTNG and EAGLE
@author: nh444
"""
import numpy as np
import matplotlib.pyplot as plt
import snap
from scipy.stats import binned_statistic
import matplotlib.ticker as ticker

h_scale = 0.679
obs_dir = "/data/vault/nh444/ObsData/"
outdir = "/data/curie4/nh444/project1/stars/"

basedir = "/data/curie4/nh444/project1/"
simdirs = [#"boxes/L40_512_LDRIntRadioEff_noBH/",
           "boxes/L40_512_MDRIntRadioEff/",
           #"boxes/L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff/",
           #"zoom_ins/LDRIntRadioEffBHVel/c384_box_Arepo_new",
           #"zoom_ins/LDRIntRadioEffBHVel/c384_box_Arepo_new_LowSoft",
           #"zoom_ins/MDRInt/c256_MDRInt/",
#           "zoom_ins/MDRInt/c384_MDRInt/",
#           "zoom_ins/MDRInt/c448_MDRInt/",
#           "zoom_ins/MDRInt/c512_MDRInt/",
          ]
zooms = [
        "c96_MDRInt", "c128_MDRInt", "c160_MDRInt", "c192_MDRInt",
        "c224_MDRInt", "c256_MDRInt", "c288_MDRInt", "c320_MDRInt",
        "c352_MDRInt", "c384_MDRInt", "c448_MDRInt", "c480_MDRInt",
        "c512_MDRInt",
        "c112_MDRInt", "c144_MDRInt", "c176_MDRInt", "c208_MDRInt",
        "c240_MDRInt", "c272_MDRInt", "c304_MDRInt", "c336_MDRInt",
        "c368_MDRInt", "c400_MDRInt", "c410_MDRInt", "c432_MDRInt",
        "c464_MDRInt",
        "c496_MDRInt_SUBF",
        ]
simdirs += ["zoom_ins/MDRInt/" + zoom + "/" for zoom in zooms]
snapnums = [25] * 14 + [99] * 14
mpc_units = True
labels = [#"no BH",
          "FABLE",
          #"small soft",
          #"c384 large soft",
          #"c384 small soft",
          #"c256",
#          "c384",
#          "c448",
#          "c512",
          ]

stack = True

# Default colours.
col = ["#c7673e", "#78a44f", "#6b87c8",
       "#934fce", "#e6ab02", "#be558f", "#47af8d"]
# dark blue, light blue, orange, dark red.
col = ['#24439b', '#50BFD7', '#ED7600', '#B94400'] + col
lw = 4

avg_mstar = 4.9e6 / h_scale  # median star particle mass in Msun

fig, ax = plt.subplots(figsize=(7, 7))
ax.set_xscale('log')
ax.set_yscale('log')
ax.set_ylabel(r"R$_{1/2,\star}$ [kpc]")
ax.set_xlabel(r"M$_{\star}$ [M$_{\odot}$]")

bins = np.logspace(8, 11.8, num=20)  # 0.3 dex stellar mass bins
x = np.sqrt(bins[1:]*bins[:-1])  # bin centres.

# =============================================================================
# for simidx, simdir in enumerate(simdirs):
#     sim = snap.snapshot(basedir+simdir, snapnums[simidx],
#                         mpc_units=mpc_units, extra_sub_props=True)
# 
#     subind = np.where(sim.cat.sub_massinradtab[:, 4] / h_scale > 0.01)[0]
#     # stellar half-mass radius
#     R = (sim.cat.sub_halfmassradtype[subind, 4] /
#          (1.0 + sim.header.redshift) / h_scale)
#     # mass in twice stellar half-mass radius
#     M = sim.cat.sub_massinradtab[subind, 4] * 1e10 / h_scale
# 
#     if stack and simidx == 0:
#         M_all = M
#         R_all = R
#     elif stack:
#         M_all = np.concatenate((M_all, M))
#         R_all = np.concatenate((R_all, R))
#     else:
#         ax.plot(np.log10(M), np.log10(R), marker=",", ls='none',
#                 c=col[simidx])
#         med, bin_edges, blah = binned_statistic(np.log10(M), np.log10(R),
#                                                 statistic='median', bins=bins)
#         ax.plot(x, med, lw=lw, c=col[simidx], label=labels[simidx])
# =============================================================================
if stack:
    col_idx = 0
    # ax.plot(np.log10(M_all), np.log10(R_all), marker=",", ls='none', c=col[0])
    med, _, _ = binned_statistic(M_all, R_all,
                                 statistic='median', bins=bins)
    ax.plot(x, med, lw=lw, c=col[col_idx], ls='--')
    ax.plot(x[x > avg_mstar*100], med[x > avg_mstar*100],
            lw=lw, c=col[col_idx], label="FABLE")
    # Plot 68% confidence interval.
    p = lambda a: np.percentile(a, 16.0)
    low, _, _ = binned_statistic(M_all, R_all,
                                 statistic=p, bins=bins)
    p = lambda a: np.percentile(a, 84.0)
    high, _, _ = binned_statistic(M_all, R_all,
                                  statistic=p, bins=bins)
    ax.fill_between(x, low, high, facecolor=col[col_idx], alpha=0.2)
col = ['#F2B202', '#7570b3', '#e7298a']
Ill = np.loadtxt(obs_dir + "stellar_mass/" +
                 "Illustris_size-mass_from_Pillepich18.csv", delimiter=",")
ax.plot(Ill[:, 0] * (0.6774 / h_scale)**2, Ill[:, 1] * (0.6774 / h_scale),
        c=col[0], lw=lw, ls='--', zorder=5, label="Illustris")

TNG = np.loadtxt(obs_dir + "stellar_mass/" +
                 "IllustrisTNG_size-mass.csv", delimiter=",")
ax.plot(10**TNG[:, 0] * (0.6774 / h_scale)**2,
        10**TNG[:, 1] * (0.6774 / h_scale), c=col[1], lw=lw, ls='dashdot',
        label="IllustrisTNG")

EAGLE = np.loadtxt(obs_dir + "stellar_mass/" +
                   "EAGLE_size-mass.csv", delimiter=",")
ax.plot(10**EAGLE[:, 0] * (0.6777 / h_scale)**2,
        10**EAGLE[:, 1] * (0.6777 / h_scale),
        c=col[2], lw=lw, ls='dotted',
        label="EAGLE")

Bal = np.loadtxt(obs_dir+"stellar_mass/" +
                 "Baldry_2012_mass-size_from_Pillepich2018.csv", delimiter=",")
ax.plot(Bal[:, 0] * (0.7 / h_scale)**2, Bal[:, 1] * (0.7 / h_scale),
        marker='d', ms=8, c='0.3', ls='none',
        label="Baldry+ 2012 (blue)")
ax.plot(Bal[:, 2] * (0.7 / h_scale)**2, Bal[:, 3] * (0.7 / h_scale),
        marker='o', ms=8, c='0.3', ls='none',
        label="Baldry+ 2012 (red)")

ax.axhline(2.8125, c='0.5', alpha=0.5, ls='--', lw=lw)

ax.set_ylim(1.0, 100.0)
ax.yaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))
ax.legend(loc='upper left', fontsize='small', borderaxespad=0.8)
fig.savefig(outdir+"size-mass_relation.pdf", bbox_inches='tight')
print "Saved to", outdir+"size-mass_relation.pdf"
