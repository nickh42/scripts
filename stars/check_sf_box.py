import pylab as pyl
import snap

simdirs = ["/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c128_box_Arepo/",
           "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c128_fixed/"]
simdirs = ["/data/emergence4/deboras/Elliptical_ZoomIns/L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff/",
          "/data/curie4/nh444/project1/L40_512_LDRIntRadioEffBHVel/",
          ]

cols = ["blue","red"]

snapnum = 20

isim = 0
for simdir in simdirs:
  sim = snap.snapshot(simdir,snapnum,mpc_units=True)

  print "z=", sim.header.redshift, "t=", sim.header.time

  sim.load_sfr()
  sim.load_mass()
  sim.load_pos()
  sim.load_bhmass()

  print "sfr total =", sim.sfr.sum(dtype=pyl.float64)
  
  #rstar = sim.nearest_dist_3D(sim.pos[sim.species_start[4]:sim.species_end[4]],[100000,100000,100000])
  rstar = sim.nearest_dist_3D(sim.pos[sim.species_start[4]:sim.species_end[4]],sim.cat.group_pos[0])
  R = 5.0 # Mpc
  indstar = pyl.where(rstar < R*1000.0)[0]
  
  rbh = sim.nearest_dist_3D(sim.pos[sim.species_start[5]:sim.species_end[5]],sim.cat.group_pos[0])
  indbh = rbh.argmin()
  
  print "stellar mass group 0 =", sim.cat.group_masstab[0,4]
  print "stellar mass r < "+str(R)+" Mpc/h =", sim.mass[sim.species_start[4]+indstar].sum(dtype=pyl.float64)
  print "stellar mass total =", sim.mass[sim.species_start[4]:sim.species_end[4]].sum(dtype=pyl.float64)

  print "central black hole mass =", sim.bhmass[indbh]

  isim += 1