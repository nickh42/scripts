import pylab as pyl
import snap

simdirs = [#"/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c128_box_Arepo/",
           #"/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c96_FeedbackFix/",
           #"/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c128_fixed/",
           #"/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c96_Arepo_March15/",
           "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c384_box_Arepo_new/",
           ]
have_fof_cat = True

simdirs = ["/data/emergence3/deboras/Elliptical_ZoomIns/FP512_RadioWeak_OldWinds/"]
have_fof_cat = False

cols = ["blue","red"]

snapnum = 25

fig =  pyl.figure()
ax = fig.add_subplot(111)

fig3 =  pyl.figure()
ax3 = fig3.add_subplot(111)

isim = 0
for simdir in simdirs:
  sim = snap.snapshot(simdir,snapnum,mpc_units=True)

  print "z=", sim.header.redshift, "t=", sim.header.time

  sim.get_temp()
  sim.load_gaspos()
  sim.load_gasmass()
  sim.load_sfr()
  sim.load_rho()
  sim.load_mass()
  sim.load_highresgasmass()
  sim.load_pos()
  sim.load_bhmass()
  
  ind = pyl.where(sim.hrgm > 0.5*sim.gasmass)[0]
  if have_fof_cat:
    sim.get_hostsubhalos()
    indmh = pyl.where(sim.hostsubhalos[sim.species_start[0]:sim.species_end[0]] == sim.cat.group_firstsub[0])[0]

  print "sfr high-res. region =", sim.sfr[ind].sum(dtype=pyl.float64)
  print "sfr total =", sim.sfr.sum(dtype=pyl.float64)
  
  if have_fof_cat:
    rstar = sim.nearest_dist_3D(sim.pos[sim.species_start[4]:sim.species_end[4]],[100000,100000,100000])
    rstar = sim.nearest_dist_3D(sim.pos[sim.species_start[4]:sim.species_end[4]],sim.cat.group_pos[0])
    R = 5.0 # Mpc
    indstar = pyl.where(rstar <= R*1000.0)[0]
  
    rbh = sim.nearest_dist_3D(sim.pos[sim.species_start[5]:sim.species_end[5]],sim.cat.group_pos[0])
    indbh = rbh.argmin()
  
    print "stellar mass group 0 =", sim.cat.group_masstab[0,4]
    print "stellar mass r < "+str(R)+" Mpc/h =", sim.mass[sim.species_start[4]+indstar].sum(dtype=pyl.float64)
  print "stellar mass total =", sim.mass[sim.species_start[4]:sim.species_end[4]].sum(dtype=pyl.float64)

  #print "central black hole mass =", sim.bhmass[indbh]

  quantity = pyl.log10(sim.rho[ind]/sim.rho_mean_baryons)
  bin_edges = pyl.linspace(-2,8,200)
  dist_m, bin_edges_m = pyl.histogram(quantity, bins=bin_edges, weights=sim.gasmass[ind])
  dist_m /= sim.gasmass[ind].sum(dtype=pyl.float64) + sim.mass[sim.species_start[4]:sim.species_end[4]].sum(dtype=pyl.float64)
  bin_centers = 0.5*bin_edges_m[1:] + 0.5*bin_edges_m[0:-1]

  ax.plot(bin_centers,dist_m,c=cols[isim])
  ax.set_xlabel("log(rho/rho\_mean\_baryons)")
  ax.set_ylabel("mass")

  #indsf = where((sim.sfr > 1.0e-3) & (absolute(sim.gaspos[:,2] - 100000.0) < 100000.0))[0]

  #fig =  figure()
  #ax = fig.add_subplot(111)
  #ax.plot(sim.gaspos[:,0], sim.gaspos[:,1], c="red", marker=".", ls="None")
  #ax.plot(sim.gaspos[indsf,0], sim.gaspos[indsf,1], c="red", marker=".", ls="None")
  #fig.show()

  fig2 = pyl.figure(figsize=(8,8))
  ax2 = fig2.add_subplot(111)
  ax2.plot(sim.pos[ind,0], sim.pos[ind,1], c="blue", marker=".", ls="None", label="High-res gas")
  if have_fof_cat: ax2.plot(sim.pos[indmh,0], sim.pos[indmh,1], c="green", marker=".", ls="None", label="High-res gas main halo")
  ax2.plot(sim.pos[sim.species_start[4]:sim.species_end[4],0], sim.pos[sim.species_start[4]:sim.species_end[4],1], c="red", marker=".", ls="None", label="All stars")
  if have_fof_cat: ax2.plot(sim.pos[sim.species_start[4]+indstar,0], sim.pos[sim.species_start[4]+indstar,1], c="purple", marker=".", ls="None", label=r"stars (r $\leq$"+str(R)+" Mpc/h)")
  ax2.plot(sim.pos[sim.species_start[5]:sim.species_end[5],0], sim.pos[sim.species_start[5]:sim.species_end[5],1], c="black", marker=".", ls="None", label="BHs")
  ax2.legend(frameon=True)
  fig2.show()

  #ax3.plot(sim.cat.group_mass, sim.cat.group_masstab[:,4]/sim.cat.group_mass,c=cols[isim],ls="none",marker="o")
  ax3.set_xlabel("Group mass")
  ax3.set_ylabel("Stellar mass fraction")

  isim += 1

fig.show()
fig3.show()