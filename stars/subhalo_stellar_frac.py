#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue May 29 16:52:36 2018
Plots the stellar mass fraction in subhaloes as a function of total subhalo mass
@author: nh444
"""
import snap
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import binned_statistic

h_scale = 0.679
outdir = "/data/curie4/nh444/project1/stars/" ## where stellar masses are saved
obs_dir = "/data/vault/nh444/ObsData/"

cbasedir = "/data/curie4/nh444/project1/zoom_ins/MDRInt/"
csimdirs = [#"c96_MDRInt", "c128_MDRInt", "c192_MDRInt",
               "c256_MDRInt", "c320_MDRInt",
               "c384_MDRInt", "c448_MDRInt", "c512_MDRInt",
               "c400_MDRInt", "c410_MDRInt",]
csnapnums = [25]*(len(csimdirs)-2)+[99,99]
#csimdirs = ["c384_MDRInt",
#           "c448_MDRInt",
#           "c400_MDRInt",
#           "c410_MDRInt",
#           ]
#csnapnums = [25,
#            25,
#            99,
#            99,
#            ]
cmpc_units = True

fbasedir = "/data/curie4/nh444/project1/boxes/"
fsimdir = "L40_512_MDRIntRadioEff/"
fsnapnum = 25
fmpc_units=False

def main():
    #subhalo_mass_function_field_cluster()
    #subhalo_stellar_frac_field_cluster()
    
#==============================================================================
#     basedir = "/data/curie4/nh444/project1/boxes/"
#     simdirs = ["L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff/",
#                 "L40_512_LDRIntRadioEffBHVel/",
#                 "L40_512_LDRIntRadioEff_SoftSmall/",
#                 ]
#     snapnums = [5]*len(simdirs)
#     labels = ["$\epsilon$(z=0)=0.3375 $z_{\epsilon}=3$",
#               "$\epsilon$(z=0)=2.4 $z_{\epsilon}=5$",
#               "$\epsilon$(z=0)=2.4 $z_{\epsilon}=1$"
#               ]
#     mpc_units = False
#==============================================================================
    basedir = "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/"
    simdirs = ["c384_box_Arepo_new",
               "c384_box_Arepo_new_LowSoft",
               ]
    snapnums = [25, 25]
    labels = ["$\epsilon_{com} = 16.9$ kpc/h $\epsilon_{maxphys} = 2.8$ kpc/h",
              "$\epsilon_{com} = 1.6$ kpc/h $\epsilon_{maxphys} = 0.4$ kpc/h",
              ]
    mpc_units=True
    subhalo_stellar_frac(basedir, simdirs, snapnums, labels=labels, mpc_units=mpc_units)
    #subhalo_mass_function(basedir, simdirs, snapnums, labels=labels, mpc_units=mpc_units)
    
def subhalo_mass_function(basedir, simdirs, snapnums, labels=None, mpc_units=True):
    if labels is None:
        labels = [simdir.replace("_","\_") for simdir in simdirs]
        
    fig, ax = plt.subplots(figsize=(7,7))
    ax.set_xscale('log')
    ax.set_xlabel("M$_{\mathrm{sub}} (M_{\odot}$)")
    
    bins = np.logspace(5, 14, num=200)
    hist = np.zeros(len(bins)-1, dtype=np.float32)
    for simidx, simdir in enumerate(simdirs):
        sim = snap.snapshot(basedir+simdir, snapnums[simidx], mpc_units=cmpc_units)
        
        subind = np.where(sim.cat.sub_masstab[:,4] > 0.0)[0]
        hist, bin_edges = np.histogram(sim.cat.sub_mass[subind]*1e10/h_scale, bins=bins)
    
        ax.step(bins[1:], hist, label=labels[simidx])
        
    ax.legend(loc='best')
    
def subhalo_stellar_frac(basedir, simdirs, snapnums, labels=None, mpc_units=True):
    if labels is None:
        labels = [simdir.replace("_","\_") for simdir in simdirs]
        
    bins = np.logspace(9, 13.6, num=23) ## 0.2 dex per bin
    x = np.sqrt(bins[1:]*bins[:-1])
    
    fig, ax = plt.subplots(figsize=(7,7))
    ax.set_xscale('log')
    ax.set_yscale('log')
    #ax.set_xlim(1e9, 5e13)
    ax.set_ylim(5e-4, 0.3)
    ax.set_xlabel("M$_{\mathrm{sub}} (M_{\odot}$)")
    ax.set_ylabel("M$_{\star}$/M$_{\mathrm{sub}}")
    for simidx, simdir in enumerate(simdirs):
        sim = snap.snapshot(basedir+simdir, snapnums[simidx], mpc_units=cmpc_units)
        
        ## Plot median
        n, bin_edges, blah = binned_statistic(sim.cat.sub_mass*1e10/h_scale, [], bins=bins, statistic='count')
        frac, bin_edges, blah = binned_statistic(sim.cat.sub_mass*1e10/h_scale, sim.cat.sub_masstab[:,4]/sim.cat.sub_mass, bins=bins, statistic='median')
        idx = np.min(np.where(n < 10)[0])
        p = ax.plot(x[:idx], frac[:idx], lw=3, label=labels[simidx])
        ax.plot(x[idx-1:], frac[idx-1:], lw=3, c=p[0].get_color(), ls='--')
        ## Plot scatter:
        #ax.plot(sim.cat.sub_mass*1e10/h_scale, sim.cat.sub_masstab[:,4]/sim.cat.sub_mass, marker="o", mec='none', ms=4, alpha=0.5, c=p[0].get_color(), ls='none')
    
    ## Plot lines of constant stellar mass
    xmin, xmax = ax.get_xlim()
    c = '0.8'
    ax.plot([xmin, xmax], [1e9/xmin, 1e9/xmax], c=c, lw=2, ls='--', zorder=0, label="M$_{\star} = [10^9, 10^{10}, 10^{11}]$ M$_{\odot}$")
    ax.plot([xmin, xmax], [1e10/xmin, 1e10/xmax], c=c, lw=2, ls='--', zorder=0)
    ax.plot([xmin, xmax], [1e11/xmin, 1e11/xmax], c=c, lw=2, ls='--', zorder=0)
    
    plt.legend(loc='best', fontsize='small', frameon=True)
    
    fig.savefig(outdir+"subhalo_stellar_fracs.pdf", bbox_inches='tight')

    
def subhalo_mass_function_field_cluster():
    fig, ax = plt.subplots(figsize=(7,7))
    ax.set_xscale('log')
    ax.set_xlabel("M$_{\mathrm{sub}} (M_{\odot}$)")
    
    bins = np.logspace(5, 14, num=200)
    hist = np.zeros(len(bins)-1, dtype=np.float32)
    for simidx, simdir in enumerate(csimdirs):
        sim = snap.snapshot(cbasedir+simdir, csnapnums[simidx], mpc_units=cmpc_units)
        
        subind = np.where(sim.cat.sub_masstab[:,4] > 0.0)[0]
        curhist, bin_edges = np.histogram(sim.cat.sub_mass[subind]*1e10/h_scale, bins=bins)
        hist = hist + curhist
    
    ax.step(bins[1:], hist, label="zooms")
    
    sim = snap.snapshot(fbasedir+fsimdir, fsnapnum, mpc_units=fmpc_units)
    subind = np.where(sim.cat.sub_masstab[:,4] > 0.0)[0]
    curhist, bin_edges = np.histogram(sim.cat.sub_mass[subind]*1e10/h_scale, bins=bins)
    ax.step(bins[1:], curhist, label="box")
    
    ax.legend(loc='best')
    
def subhalo_stellar_frac_field_cluster(scatter=False):
    """
    Plot subhalo stellar fractions for galaxies in the field (boxes) and in
    different radial regions of clusters (zooms)
    """
    ## First do the zooms for different radial regions
    myRlims = [[0.0, 1.0], [1.0, 2.0], [2.0, 5.0]] ## R200
    msubs = [] ## for list of arrays containing subhalo masses
    mstar_subs = []
    groupnum = 0
    for simidx, simdir in enumerate(csimdirs):
        sim = snap.snapshot(cbasedir+simdir, csnapnums[simidx], mpc_units=cmpc_units)
        R = sim.cat.group_r_crit200[groupnum]
        
        subind = np.where(sim.cat.sub_masstab[:,4] > 0.0)[0]
        r = sim.nearest_dist_3D(sim.cat.sub_pos[subind], sim.cat.group_pos[groupnum])
        for i, Rlims in enumerate(myRlims):
            ind = subind[np.where((r >= Rlims[0]*R) & (r < Rlims[1]*R))[0]]
            if simidx==0:
                msubs.append(sim.cat.sub_mass[ind] * 1e10 / h_scale)
                mstar_subs.append(sim.cat.sub_masstab[ind, 4] * 1e10 / h_scale)
            elif simidx>0:
                msubs[i] = np.concatenate((msubs[i], sim.cat.sub_mass[ind] * 1e10 / h_scale))
                mstar_subs[i] = np.concatenate((mstar_subs[i], sim.cat.sub_masstab[ind, 4] * 1e10 / h_scale))
    
    
    ## Initialise figure
    fig, ax = plt.subplots(figsize=(7,7))
    ax.set_xscale('log')
    ax.set_yscale('log')
    ax.set_xlim(1e9, 5e13)
    ax.set_ylim(5e-4, 0.3)
    ax.set_xlabel("M$_{\mathrm{sub}} (M_{\odot}$)")
    ax.set_ylabel("M$_{\star}$/M$_{\mathrm{sub}}")
    col = ['#B94400', '#FF9819', '#FFE666']
    fieldcol = '#6395EC'
    
    ## Plot lines of constant stellar mass
    xmin, xmax = ax.get_xlim()
    c = '0.8'
    ax.plot([xmin, xmax], [1e10/xmin, 1e10/xmax], c=c, lw=2, ls='--')
    ax.plot([xmin, xmax], [1e11/xmin, 1e11/xmax], c=c, lw=2, ls='--')
    
    ## Plot C-EAGLE
    dat = np.genfromtxt(obs_dir+"stellar_mass/"+"CEAGLE_subhalo_fracs.csv", delimiter=",")
    ax.plot(10**dat[:,0], 10**dat[:,1], c=col[0], ls='dotted', lw=3)
    ax.plot(10**dat[:,0], 10**dat[:,2], c=col[1], ls='dotted', lw=3)
    ax.plot(10**dat[:,0], 10**dat[:,3], c=col[2], ls='dotted', lw=3)
    ax.plot(10**dat[:,0], 10**dat[:,4], c=fieldcol, ls='dotted', lw=3)
    
    ## Plot the zooms
    bins = np.logspace(9, 13.6, num=23) ## 0.2 dex per bin
    x = np.sqrt(bins[1:]*bins[:-1])
    for i, Rlims in enumerate(myRlims):
        if scatter:
            plt.plot(msubs[i], mstar_subs[i]/msubs[i], marker=",", c=col[i], ls='none')
        n, bin_edges, blah = binned_statistic(msubs[i], [], bins=bins, statistic='count')
        frac, bin_edges, blah = binned_statistic(msubs[i], mstar_subs[i]/msubs[i], bins=bins, statistic='median')
        idx = np.min(np.where(n < 10)[0])
        #plt.step(bins[1:], frac, lw=3, c=col[i], label="{:.1f} - {:.1f}".format(Rlims[0], Rlims[1]))
        plt.plot(x[:idx], frac[:idx], lw=3, c=col[i], label="{:.1f} - {:.1f}".format(Rlims[0], Rlims[1]))
        plt.plot(x[idx-1:], frac[idx-1:], lw=3, c=col[i], ls='--')
        
    ## Do box
    sim = snap.snapshot(fbasedir+fsimdir, fsnapnum, mpc_units=fmpc_units)
    subind = np.where(sim.cat.sub_masstab[:,4] > 0.0)[0]
    if scatter:
        plt.plot(sim.cat.sub_mass[subind]*1e10/h_scale, sim.cat.sub_masstab[subind,4]/sim.cat.sub_mass[subind], marker=",", c=fieldcol, ls='none')
    n, bin_edges, blah = binned_statistic(sim.cat.sub_mass[subind]*1e10/h_scale, [], bins=bins, statistic='count')
    frac, bin_edges, blah = binned_statistic(sim.cat.sub_mass[subind]*1e10/h_scale, sim.cat.sub_masstab[subind,4]/sim.cat.sub_mass[subind], bins=bins, statistic='median')
    idx = np.min(np.where(n < 10)[0])
    plt.plot(x[:idx], frac[:idx], lw=3, c=fieldcol, label="Field")
    plt.plot(x[idx-1:], frac[idx-1:], lw=3, c=fieldcol, ls='--')
    
    plt.legend(loc='best', title="$r/r_{200} = $", fontsize='small')
    
    fig.savefig(outdir+"subhalo_stellar_fracs_field_cluster.pdf", bbox_inches='tight')

if __name__ == "__main__":
    main()
