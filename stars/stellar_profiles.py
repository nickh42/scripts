#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Aug  6 12:20:41 2018
Plot stellar profiles
THIS SCRIPT IS CURRENTLY DEFUNCT - use stellar_mass_profiles.py instead
@author: nh444
"""

def main():
    basedir = "/home/nh444/data/project1/zoom_ins/MDRInt/"
    simdirs = [#"c96_MDRInt", "c128_MDRInt", "c192_MDRInt", "c224_MDRInt", "c256_MDRInt",
          #"c288_MDRInt", "c320_MDRInt",
          #"c352_MDRInt", "c384_MDRInt",
          #"c400_MDRInt", "c410_MDRInt",
          "c448_MDRInt",#"c512_MDRInt",
          ]
    mpc_units = True
    
    col = ["#c7673e","#78a44f","#6b87c8","#934fce","#e6ab02","#be558f","#47af8d"]
    col = ['#24439b', '#ED7600'] ## blue orange
    mec = col
    mfc = ['none']*len(simdirs)
    ms = 8
    ls = ['solid']*len(simdirs)
       
    snapnums = range(4, 26)
    #snapnums = range(0,100)
    outdir = "/data/curie4/nh444/project1/stars/"
    
def plot_BCG_density(basedir, simdirs, snapnums, projected=False, mpc_units=True):
    """
    THIS FUNCTION IS UNFINISHED (see file doc string)
    Plot the stellar mass density as a function of radius for the BCG (main subhalo)
    in the most massive FoF group of each 'simdir' for each snapshot 'snapnum'
    Args:
        projected: If True, calculate stellar mass per projected radial bin, otherwise 3D radial bin
    """
    import snap
    import numpy as np
    
    for snapnum in snapnums:
        print "Starting snapshot", snapnum
        for simdir in simdirs:
            print "Starting simdir", simdir
            sim = snap.snapshot(basedir+simdir, snapnum, mpc_units=mpc_units)
            
            print "Loading positions and masses..."
            sim.load_starpos()
            sim.load_starmass()
            
            group = 0
            centre = sim.cat.sub_pos[sim.cat.group_firstsub[group]]
            if projected:
                rel_pos = sim.rel_pos(sim.starpos, centre)
                r = np.sqrt(rel_pos[:,0]**2 + rel_pos[:,1]**2)
            else:
                r = sim.nearest_dist_3D(sim.starpos, centre)
                
                
            
if __name__ == "__main__":
    main()