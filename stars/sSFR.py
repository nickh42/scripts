#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 13 18:13:01 2017
Specific star formation rate as a funcion of stellar mass for galaxies
@author: nh444
"""

import numpy as np
import matplotlib.pyplot as plt
import snap
from scipy.stats import binned_statistic
h_scale = 0.679

indirs = ["/data/curie4/nh444/project1/boxes/L40_512_NoDutyRadioWeak_HalfThermal_FixEta/",
          "/data/curie4/nh444/project1/boxes/L40_512_NoDutyRadioInt_HalfThermal_FixEta/",
          "/data/curie4/nh444/project1/boxes/L40_512_LowDutyRadioWeak_HalfThermal_FixEta/",
          "/data/curie4/nh444/project1/boxes/L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff/",
          ]
labels = ["Ref (small soft)",
          "Stronger radio (small soft)",
          "Quasar duty cycle (small soft)",
          "Combined (small soft)",
          ]
snapnum = 25
mpc_units=False
aperture = False
outname = "/data/curie4/nh444/project1/stars/sSFR_total_models_smallsoft.pdf"

indirs = [#"/data/curie4/nh444/project1/boxes/L40_512_LDRIntRadioEff_noBH/",
          #"/data/curie4/nh444/project1/boxes/L40_512_NoDutyRadioWeak/",
          #"/data/curie4/nh444/project1/boxes/L40_512_NoDutyRadioInt/",
          #"/data/curie4/nh444/project1/boxes/L40_512_DutyRadioWeak/",
          "/data/curie4/nh444/project1/boxes/L40_512_MDRIntRadioEff/",
          #"/data/curie4/nh444/project1/zoom_ins/MDRInt/c384_MDRInt/",
          #"/data/curie4/nh444/project1/zoom_ins/MDRInt/c448_MDRInt/",
          #"/data/curie4/nh444/project1/zoom_ins/MDRInt/c512_MDRInt/",
          ]
labels = [#"No BHs",
          #"Ref",
          #"Stronger radio",
          #"Quasar duty cycle",
          "Box",
          #"c384",
          #"c448",
          #"c512",
          ]
snapnum = 25
mpc_units=False
aperture = True
outname = "/data/curie4/nh444/project1/stars/sSFR.pdf"

fig, ax = plt.subplots(figsize=(7,7))
ax.set_xscale('log')
ax.set_yscale('log')
ax.set_xlabel("Stellar Mass [M$_{\odot}$]")
ax.set_ylabel("sSFR [Gyr$^{-1}$]")
Mmin, Mmax = [8, 13] ## log stellar mass (Msun)
ax.set_ylim(1e-5,3)
bins = np.logspace(Mmin, Mmax, num=(Mmax-Mmin)/0.2)

### Observations ###
GSWLC = np.loadtxt("/data/vault/nh444/ObsData/SFR/GSWLC-X1_SFR.csv", delimiter=",")
ind = np.where((GSWLC[:,2] > Mmin) & (GSWLC[:,12] > 0.0))[0]
logmstar = GSWLC[ind,2] + np.log10(0.7/h_scale)
logsfr = GSWLC[ind,4] ## 4 for UV/optical, 8 or 10 for mid-IR
#plt.errorbar(10**logmstar, 10**logsfr / 10**logmstar *1e9, marker=',', c='0.8', ls='none', label="GSWLC (UV/Opt)", zorder=0)
#from matplotlib.colors import LogNorm
counts,xbins,ybins=np.histogram2d(logmstar, logsfr-logmstar+9, bins=100)#, normed=LogNorm())
cs = plt.contour(10**xbins[:-1],10**ybins[:-1],counts.transpose(), colors='0.7', lw=2)
cs.collections[0].set_label("Salim+16 GSWLC (UV/Opt)")
stat = binned_statistic(10**logmstar, 10**logsfr / 10**logmstar *1e9, statistic='mean', bins=bins)[0]
plt.errorbar((bins[1:]*bins[:-1])**0.5, stat, c='0.6', lw=2, label="Salim+16 GSWLC (UV/Opt) mean", zorder=0)
stat = binned_statistic(10**logmstar, 10**GSWLC[ind,8] / 10**logmstar *1e9, statistic='mean', bins=bins)[0]
#plt.errorbar((bins[1:]*bins[:-1])**0.5, stat, c='0.3', lw=2, ls='dashed', label="GSWLC (Mid-IFR) mean", zorder=0)
#ind2 = np.where(GSWLC[ind,10] > -98) ## missing values are -99
#stat = binned_statistic(10**logmstar[ind2], 10**GSWLC[ind[ind2],10] / 10**logmstar[ind2] *1e9, statistic='mean', bins=bins)[0]
#plt.errorbar((bins[1:]*bins[:-1])**0.5, stat, c='0.6', lw=2, ls='dashed', label="GSWLC (Mid-IFR) mean", zorder=0)
    
#dat = np.loadtxt("/data/vault/nh444/ObsData/SFR/Duarte17_cat.csv", delimiter=",")
#logsSFR = dat[:,3] + np.log10(h_scale/0.7) ## sSFR (Gyr^-1)
#logmstar = dat[:,2] - logsSFR + 9 ## M = SFR/sSFR (Msun)
##plt.errorbar(10**logmstar, 10**logsSFR, marker=',', c='0.8', ls='none', label="Duarte+ 2017", zorder=0)
#counts,xbins,ybins=np.histogram2d(logmstar, logsSFR, bins=100, normed=LogNorm())
#cs = plt.contour(10**xbins[:-1],10**ybins[:-1],counts.transpose(), colors='0.3', lw=2)
#cs.collections[0].set_label(r"Duarte+ 2017 (H$\alpha$, SF)")
##med = binned_statistic(10**logmstar, 10**logsSFR, statistic='median', bins=bins)[0]
##plt.errorbar((bins[1:]*bins[:-1])**0.5, med, c='r', lw=2, label="Duarte+ 2017", zorder=0)

#dat = np.loadtxt("/data/vault/nh444/ObsData/SFR/Terrazas_2017_SFR_MBH.csv", delimiter=",", usecols=(1,2))
#Mstar = 10**dat[:,0] * (0.7/h_scale)
#SFR = 10**dat[:,1]
#plt.errorbar(Mstar, SFR / Mstar *1e9, marker='o', c='orange', mec='none', ms=5, ls='none', label="Terrazas+ 2017")

Salim_SF = lambda Mstar: 10**(-0.35*(np.log10(Mstar) - 10.0) - 9.83) ## give Mstar in Msun (h=0.7), returns SFR/Mstar in yr^-1 
x = np.logspace(Mmin, Mmax, num=100)
plt.errorbar(x, Salim_SF(x * h_scale/0.7) * h_scale/0.7 * 1e9, c='0.3', lw=1.5, label="Salim+07 (pure SF)")
Salim_AGN = lambda Mstar: np.concatenate((10**(-0.17*(np.log10(Mstar[Mstar<=10**9.4]) - 10.0) - 9.65), 10**(-0.53*(np.log10(Mstar[Mstar>10**9.4]) - 10.0) - 9.87)))
plt.errorbar(x, Salim_AGN(x * h_scale/0.7) * h_scale/0.7 * 1e9, c='0.3', lw=1.5, ls='dashed', label="Salim+07 (SF/AGN)")

col = plt.rcParams['axes.prop_cycle'].by_key()['color']

for dirnum, indir in enumerate(indirs):
    sim = snap.snapshot(indir, snapnum, mpc_units=mpc_units, extra_sub_props=True)
    
    subSFR_tot = sim.cat.sub_sfr * 1e9 ## Msun / Gyr (no h since mass and time both are h^-1)
    subSFR_inrad = sim.cat.sub_sfrinrad * 1e9
    
    subMstar_tot = sim.cat.sub_masstab[:,4] * 1e10 / h_scale
    subMstar_inrad = sim.cat.sub_massinradtab[:,4] * 1e10 / h_scale
    
    #print "bins =", bins
    #print "subMstar_inrad =", subMstar_inrad
    if aperture:
        subMstar = subMstar_inrad
        subSFR = subSFR_inrad
    else:
        subMstar = subMstar_tot
        subSFR = subSFR_tot
        
    ind = np.where((subMstar > 1e10) & (subSFR > 0.0))[0]
    #ax.plot(subMstar[ind], subSFR[ind] / subMstar[ind], marker=',', ls='none')
    counts,xbins,ybins=np.histogram2d(np.log10(subMstar[ind]), np.log10(subSFR[ind] / subMstar[ind]), bins=12)
    cs = plt.contour(10**xbins[:-1], 10**ybins[:-1], counts.transpose(), colors=col[dirnum], lw=2)
    cs.collections[0].set_label(labels[dirnum])
    
    ## Plot mean in stellar mass bins
    ind = np.where((subMstar > 0.0) & (subSFR > 0.0))[0]
    stat = binned_statistic(subMstar[ind], subSFR[ind] / subMstar[ind], statistic='mean', bins=bins)[0]
    ax.plot((bins[1:]*bins[:-1])**0.5, stat, c=col[dirnum], lw=3, label=labels[dirnum])
    ## Plot symbols for bins with fewer than X points
    count = binned_statistic(subMstar[ind], subSFR[ind] / subMstar[ind], statistic='count', bins=bins)[0]
    ## index of first bin with fewer than 10 counts
    idx = np.min(np.where(count < 50)[0])
    assert np.sum(count[:idx]) > 0, "the first non-empty bin has fewer than X counts"
    ind2 = ind[np.where(subMstar[ind] >= bins[idx])[0]]
    ax.plot(subMstar[ind2], subSFR[ind2] / subMstar[ind2], marker='o', mfc=col[dirnum], mec='none', ls='none')


ax.legend(loc='best', fontsize='x-small')
fig.savefig(outname)
print "Plot saved to", outname
