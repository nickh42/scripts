def main():
    
    basedir = "/data/emergence4/deboras/Elliptical_ZoomIns/"
    simdir = "L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff/"
    snapnums = [25, 24, 23, 22, 21, 20]
    #z = [0.001, 0.197, 0.4, 0.6, 0.8, 1.0]
    
    obs_dir = "/data/curie4/nh444/project1/L-T_relations/obs/"
    N=30
    delta = 500.0
    ref = "crit"
    use_fof = True ## Use halo file for M_delta and R_delta 
        
    #mfracs = mass_fracs(basedir, simdir, snapnums, obs_dir, N=N, delta=delta, ref=ref, use_fof=use_fof)
    
    basedir = "/data/deboras/ewald/"
    simdir = "m_10002_h_94_501_z3/csfbh/"
    snapnums = [63]
    mfracs = mass_fracs(basedir, simdir, snapnums, obs_dir, N=N, delta=delta, ref=ref, use_fof=use_fof, masstab=True, mpc_units=True)
    
    #mfracs.get_mass_fracs()
    #mfracs.plot_mstarfrac_v_mass() #requires get_mass_fracs
    #mfracs.plot_mstarfrac_v_redshift(nplot=7) ## plot 'nplot' most massive (M_delta)
    #mfracs.plot_mstar_v_redshift()

    ''' Stellar mass profiles '''
    #mfracs.get_mstar_profiles(rmax=500, bins=1000)
    #mfracs.plot_mstar_profiles(nplot=6, rmax=300.0, bins=50) #requires get_mstar_prof

    ''' Stellar mass components'''
    mfracs.get_ids_stellar_components(max(snapnums), cutoff_rad=30.0, include_fuzzy=False)
    mfracs.get_new_stars_vs_distance(snapnum_min=1, check_before=0) #requires get_ids
    mfracs.plot_new_stars_vs_distance() #requires get_new_stars_vs_distance

class mass_fracs:
    def __init__(self, basedir, simdir, snapnums, obs_dir, N=20, delta=500.0, ref="crit", use_fof=True, snap_basename="snap", mpc_units=False, masstab=False):

        import os
        import cosmo
        
        self.basedir = os.path.dirname(basedir)+"/"
        self.simdir = os.path.dirname(simdir)+"/"
        self.snapnums = snapnums
        self.obs_dir = obs_dir
        self.N = N
        self.delta = delta
        self.ref = ref
        self.use_fof = use_fof
        self.snap_basename = snap_basename
        self.mpc_units = mpc_units
        self.masstab = masstab

        import readsnap as rs
        self.z = []
        for snapnum in snapnums:
            header = rs.snapshot_header(self.basedir + self.simdir + "snap_" + str(snapnum).zfill(3))
            self.z.append(header.redshift)
        self.cm = cosmo.cosmology(header.hubble, header.omega_m)

        self.fout_base = "stellar_mass_M"+str(int(delta))+"_"
        
        self.salp2chab = 0.58 ## Factor to multiply stellar masses to convert from 
        ## studies using Salpeter IMF to a Chabrier 2003 IMF - factor from
        ## Madau 2014 "Cosmic SFH" (0.61) or Chiu 2016/Hilton 2013 (0.58)
        self.lin2chab = 0.76 ## Factor to convert Lin 2003 stellar masses to 
        ## equivalent if using a Chabrier IMF (0.76 from Chiu 2016)
        self.gonz2chab = 0.76 ## As above for Gonzalez 2014 paper

        self.colours = ["#9fd550",
                        "#be53be",
                        "#65b97f",
                        "#7979cf",
                        "#c0933d",
                        "#d05352"]

    def get_mass_fracs(self):
        import numpy as np
        import snap
        import readsnap as rs

        delta = self.delta
        N = self.N
        
        for j, snapnum in enumerate(self.snapnums):
            ID = np.arange(N)
            M_delta = np.zeros(N)
            R_delta = np.zeros(N)
            M_delta_stars = np.zeros(N)

            h = self.cm.hubble_z(self.z[j])

            snapshot = snap.snapshot(self.basedir+self.simdir, snapnum, mpc_units=self.mpc_units, masstab=self.masstab, read_params=False)
            pos = getattr(snapshot.cat, 'group_pos')[:N]
            if not hasattr(snapshot, 'mass'):
                snapshot.load_mass()
            if self.use_fof and hasattr(snapshot.cat, 'group_m_crit'+str(int(delta))):
                M_delta = getattr(snapshot.cat, 'group_m_crit'+str(int(delta)))[:N]
                R_delta = getattr(snapshot.cat, 'group_r_crit'+str(int(delta)))[:N]
                #print "M_"+ref+","+str(delta)+"[0] =", M_delta[0]

            for i in range(0, N):
                if M_delta[i] == 0.0:
                    M_delta[i], R_delta[i] = snapshot.get_so_mass_rad(i, delta, ref=self.ref, only_sel=False)
                ind = snapshot.get_particles_sphere(pos[i], R_delta[i], parttype=4)
                M_delta_stars[i] = snapshot.mass[ind].sum() * h
                print "M_"+self.ref+","+str(int(delta))+"["+str(i)+"] =", M_delta[i]
                print "R_"+self.ref+","+str(int(delta))+"["+str(i)+"] =", R_delta[i]
                print "M_stars_"+self.ref+"_"+str(int(delta))+" =", M_delta_stars[i]

            ## Sort
            sorter = np.argsort(M_delta)[::-1]
            ID = ID[sorter]
            M_delta = M_delta[sorter]
            R_delta = R_delta[sorter]
            M_delta_stars = M_delta_stars[sorter]
            pos = pos[sorter]
            np.savetxt(self.fout_base+str(snapnum).zfill(3)+".txt", np.c_[ID, M_delta, R_delta, M_delta_stars, pos[:,0], pos[:,1], pos[:,2]], fmt=['%d','%.5e','%.5e','%.5e','%f','%f','%f'], header="ID, M_"+str(int(delta))+"(1e10Msun), R_"+str(int(delta))+"(kpc), M_"+str(int(delta))+"_stars(1e10Msun), x, y, z")

    def plot_mstarfrac_v_mass(self):
        import numpy as np
        import matplotlib.pyplot as plt
        import matplotlib.ticker as ticker
        from matplotlib.backends.backend_pdf import PdfPages

        delta = self.delta
        N = self.N
        obs_dir = self.obs_dir
        fout_base = self.fout_base
        z = self.z
        outfilename = "stellar_mass_fractions.pdf"
        
        gonz = np.genfromtxt(obs_dir+"Gonzalez_2014_stellar_gas_masses.csv", delimiter=",")
        chiu = np.genfromtxt(obs_dir+"Suhada2012_Chiu2016_XMM-BCS.csv", delimiter=",")
        chiu2 = np.genfromtxt(obs_dir+"Chiu_2016_baryon_fracs.csv", delimiter=",")
        krav = np.genfromtxt(obs_dir+"kravtsov_stellar_masses.csv", delimiter=",")
        gio = np.genfromtxt(obs_dir+"giodini2009_star_fracs.txt")
        lin = np.genfromtxt(obs_dir+"lin_mohr_table1.txt", usecols=range(0,19))
        van = np.genfromtxt(obs_dir+"van_der_burg_stellar_masses.csv", delimiter=",")
        anders = np.genfromtxt(obs_dir+"Anderson2014_t1.csv", delimiter=",")

        with PdfPages(outfilename) as pdf:
            for j, snapnum in enumerate(self.snapnums):
                fig = plt.figure(figsize=(12,13))

                data = np.genfromtxt(fout_base+str(snapnum).zfill(3)+".txt")
                M_delta = data[:,1]
                R_delta = data[:,2]
                M_delta_stars = data[:,3]

                plt.plot(M_delta*1e10, M_delta_stars/M_delta, 'o', label=str(N)+" most massive halos", mec='none', mfc='darkgreen')
                plt.title(self.simdir[8:-1]+"\n z = "+str(z[j]))
                plt.xlim(3e12,1e15)
                plt.ylim(0.004, 0.7)
                plt.xscale('log')
                plt.yscale('log')
                plt.ylabel(r"Stellar mass fraction within r$_{"+str(int(delta))+"}$", fontsize=16)
                plt.xlabel(r"M$_{"+str(int(delta))+"}$ [M$_{\odot}$/h]", fontsize=16)

                ## Set axis labels to decimal format not scientific when using log scale
                plt.gca().yaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))

                ## Plot best-fit from Chiu 2016
                M1, M2 = plt.gca().get_xlim()
                f1, f2, chiu_label = self.chiu16_fstar_relation(M1, M2, z=z[j])
                plt.plot([M1, M2], [f1,f2], label=chiu_label, c='0.8')

                if z[j] <= 0.15:
                    y_anders = (10**((anders[5:,0]+anders[5:,1])/2.0))/(10**anders[5:,2])
                    plt.errorbar(10**anders[5:,2]*0.704, y_anders, yerr=[abs(y_anders - (10**(anders[5:,0])/10**anders[5:,2])), abs(y_anders - (10**(anders[5:,0])/10**anders[5:,2]))], c='blue', marker='D', linestyle='none', ms=3, label="Anderson et al. 2014 (LBGs) 0.04 < z < 0.20")
                    plt.errorbar(gonz[:11,14]*0.702*1e14, gonz[:11,29]*self.gonz2chab, xerr=[gonz[:11,15]*0.702*1e14, gonz[:11,15]*0.702*1e14], yerr=[gonz[:11,30]*self.gonz2chab, abs(gonz[:11,31]*self.gonz2chab)], c='#47d147', mec='none', marker='s', linestyle='none', ms=3, label="Gonzalez et al. 2014 (BCG + sats + ICL) 0.05 < z < 0.12")
                    mask = (chiu[:,3] < 0.15)
                    plt.errorbar(chiu[mask,21]*1e13*0.7, chiu[mask,30]/chiu[mask,21]/10, xerr=chiu[mask,22]*1e13*0.7, yerr=chiu[mask,30]/chiu[mask,21]/10*((chiu[mask,31]/chiu[mask,30])**2 + (chiu[mask,22]/chiu[mask,21])**2)**0.5, c='#ff9980', mec='none', marker='D', linestyle='none', ms=3, label="Chiu, Saro, Mohr et al. 2016 (BCG + sats) 0.1 < z < 0.15")
                    plt.errorbar(krav[:,5]*1e14*0.7, (krav[:,10]+krav[:,13])/krav[:,5]/100, yerr=(krav[:,10]+krav[:,13])/krav[:,5]/100*(((krav[:,11]**2+krav[:,14]**2)**0.5/(krav[:,10]+krav[:,13]))**2)**0.5, c='#00cc99', mec='none', marker='D', linestyle='none', ms=3, label="Kravtsov et al. 2014 (BCG + sats + ICL) z < 0.1")
                    plt.errorbar(2.55e13*(lin[:,2]**1.58)*0.7, lin[:,16]*0.01*self.lin2chab, yerr=[(lin[:,18])*0.01,(lin[:,17])*0.01*self.lin2chab], c='#ff9cff', mec='none', marker='D', linestyle='none', ms=3, label="Lin et al. 2003 (BCG + sats) 0.016 < z < 0.09")
                    #data3 = np.loadtxt("./prop_file_Z_0.3.txt")
                    #plt.plot(data3[:,21]*1.0e10, (data3[:,20]/data3[:,21]), marker='s', mfc='none', mec='green', ms=8, linestyle='none', mew=1, label="Output of scaling code for z=0.001")

                if z[j] >= 0.1 and z[j] <= 1.0:
                    plt.errorbar(gio[:,0]*0.72, gio[:,3]*self.salp2chab, xerr=[(gio[:,0]-gio[:,1])*0.72, (gio[:,2]-gio[:,0])*0.72], yerr=[(gio[:,3]-gio[:,4])*self.salp2chab,(gio[:,5]-gio[:,3])*self.salp2chab], c='#6ea3ff', mec='none', marker='^', linestyle='none', ms=5, label="Giodini et al. 2009 (BCG + sats) 0.1 < z < 1.0")
                if z[j] >= 0.1 and z[j] <= 0.5:
                    mask = (chiu[:,3] < 0.5)
                    plt.errorbar(chiu[mask,21]*1e13*0.7, chiu[mask,30]/chiu[mask,21]/10, xerr=chiu[mask,22]*1e13*0.7, yerr=chiu[mask,30]/chiu[mask,21]/10*((chiu[mask,31]/chiu[mask,30])**2 + (chiu[mask,22]/chiu[mask,21])**2)**0.5, c='#ff9980', mec='none', marker='D', linestyle='none', ms=3, label="Chiu, Saro, Mohr et al. 2016 (BCG + sats) 0.1 < z < 0.5")

                if z[j] >= 0.5 and z[j] <= 0.8:
                    mask = (chiu[:,3] > 0.5) & (chiu[:,3] < 0.8)
                    plt.errorbar(chiu[mask,21]*1e13*0.7, chiu[mask,30]/chiu[mask,21]/10, xerr=chiu[mask,22]*1e13*0.7, yerr=chiu[mask,30]/chiu[mask,21]/10*((chiu[mask,31]/chiu[mask,30])**2 + (chiu[mask,22]/chiu[mask,21])**2)**0.5, c='#ff9980', mec='none', marker='D', linestyle='none', ms=3, label="Chiu, Saro, Mohr et al. 2016 (BCG + sats) 0.5 < z < 0.8")

                if z[j] >= 0.8 and z[j] <= 1.1:
                    mask = (chiu[:,3] > 0.8)
                    plt.errorbar(chiu[mask,21]*1e13*0.7, chiu[mask,30]/chiu[mask,21]/10, xerr=chiu[mask,22]*1e13*0.7, yerr=chiu[mask,30]/chiu[mask,21]/10*((chiu[mask,31]/chiu[mask,30])**2 + (chiu[mask,22]/chiu[mask,21])**2)**0.5, c='#ff9980', mec='none', marker='D', linestyle='none', ms=3, label="Chiu, Saro, Mohr et al. 2016 (BCG + sats) 0.8 < z < 1.0")

                if z[j] >= 0.6 and z[j] <= 1.4:
                    mask = (chiu2[:,1] > 0.5)
                    plt.errorbar(chiu2[mask,2]*1e14*0.683, chiu2[mask,10]/chiu2[mask,2]/100, xerr=chiu2[mask,3]*1e14*0.683, yerr=chiu2[mask,10]/chiu2[mask,2]/100*((chiu2[mask,11]/chiu2[mask,2])**2 + (chiu2[mask,3]/chiu2[mask,2])**2)**0.5, c='#bf80ff', mec='none', marker='D', linestyle='none', ms=3, label="Chiu, Mohr, McDonald et al. 2016 (BCG + sats) 0.6 < z < 1.4")

                if z[j] > 0.8 and z[j] < 1.4:
                    ## Convert M200 to M500 by factor 0.631 as in paper
                    plt.errorbar(van[:,7]*1e14*0.7*0.631, van[:,17]/(van[:,7]*0.631)/100, xerr=[van[:,8]*1e14*0.7*0.631, abs(van[:,9])*1e14*0.7*0.631], yerr=van[:,17]/(van[:,7]*0.631)/100*((van[:,18]/van[:,17])**2+(van[:,8]/van[:,7])**2)**0.5, c='#00cc99', mec='none', marker='D', linestyle='none', ms=3, label="van der Burg et al. 2014 (BCG + sats) 0.86 < z 1.34")
                    ## Plot best-fit from Chiu 2016
                    M1, M2 = plt.gca().get_xlim()
                    f1, f2, chiu_label = self.chiu16_fstar_relation(M1, M2, z=0.9, sample="SPT")
                    plt.plot([M1, M2], [f1,f2], label=chiu_label, c='0.8', linestyle='dashed')

                plt.legend(loc='upper left', frameon=False, borderaxespad=1, numpoints=1, fontsize=14)

                pdf.savefig()
                plt.close()
            print "Plot saved to", outfilename

    def chiu16_fstar_relation(self, M1, M2, z=0.0, sample="combined"):
        ## Compute stellar mass fraction for two given M500 according to
        ## best-fit relation of Chiu 2006 "Baryon content of massive galaxy clusters"
        import cosmo
        cm = cosmo.cosmology(0.7, 0.3)
        if sample=="combined":
            alpha = 0.0099
            beta = -0.37
            gamma = 0.26
            label = "Chiu et al. 2016 best-fit to combined sample ($f_{\star} \propto M_{500}^{-0.37 \pm 0.09}(1+z)^{0.26 \pm 0.24}$)"
        elif sample=="SPT":
            alpha = 0.011
            beta = -0.09
            gamma = 1.07
            label = "Chiu et al. 2016 best-fit to SPT sample at z = 0.9 ($f_{\star} \propto M_{500}^{-0.09 \pm 0.27}(1+z)^{1.07 \pm 1.08}$)"
        else:
            print "invalid keyword 'sample' for Chiu relation"
            return None, None, None

        f1 = alpha * (1.0+z)**gamma * (M1*cm.hubble_z(z)/6.0e14)**beta
        f2 = alpha * (1.0+z)**gamma * (M2*cm.hubble_z(z)/6.0e14)**beta
        return f1, f2, label


    def plot_mstarfrac_v_redshift(self, nplot=8):
        import numpy as np
        import matplotlib.pyplot as plt
        import matplotlib.ticker as ticker

        delta = self.delta
        fout_base = self.fout_base
        z = self.z

        chiu = np.genfromtxt(self.obs_dir+"Suhada2012_Chiu2016_XMM-BCS.csv", delimiter=",")

        fig = plt.figure(figsize=(10,10))
        fig.patch.set_facecolor('white')
        
        M500_max = 4.2
        mask = (chiu[:,21] < M500_max) ##M500 mask
        plt.errorbar(chiu[mask,3], chiu[mask,30]/chiu[mask,21]/10, xerr=[chiu[mask,4], abs(chiu[mask,5])], yerr=chiu[mask,30]/chiu[mask,21]/10*((chiu[mask,31]/chiu[mask,30])**2 + (chiu[mask,22]/chiu[mask,21])**2)**0.5, c='#ff9980', mec='none', marker='D', linestyle='none', ms=6, label="Chiu, Saro, Mohr et al. 2016 (BCG + sats) "+str(min(chiu[mask,21]))+" < M$_{500}$ (10$^{13}$M$_{\odot}$) < "+str(M500_max))

        for j, snapnum in enumerate(self.snapnums):
            data = np.genfromtxt(fout_base+str(snapnum).zfill(3)+".txt")
            M_delta = data[:,1]
            R_delta = data[:,2]
            M_delta_stars = data[:,3]
            #mask = (M_delta > 1800)
            plt.plot(np.full_like(M_delta[:nplot], z[j]), M_delta_stars[:nplot]/M_delta[:nplot], linestyle='none', marker=r"$\odot$", mfc='none', mec='green', ms=6, mew=0.6)

        plt.title(self.simdir[8:-1]+"\n z = "+str(z[j]))
        plt.xlim(0,0.5)
        plt.ylim(0.004, 0.3)
        plt.yscale('log')
        plt.ylabel(r"Stellar mass fraction within r$_{"+str(int(delta))+"}$", fontsize=16)
        plt.xlabel("z", fontsize=16)
        
        ## Set axis labels to decimal format not scientific when using log scale
        plt.gca().yaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))
        plt.legend(loc='upper left', frameon=False, borderaxespad=1, numpoints=1, fontsize=14)

        plt.savefig("stellar_mass_fraction_v_redshift.pdf")

    def plot_mstar_v_redshift(self):
        import numpy as np
        import matplotlib.pyplot as plt
        import matplotlib.ticker as ticker
        import sys
        import cosmo

        delta = self.delta
        fout_base = self.fout_base
        z = self.z
        assert (delta == 500.0), "Requires delta = 500.0 for computed masses."
        
        fig = plt.figure(figsize=(10,10))
        fig.patch.set_facecolor('white')
        
        chiu = np.genfromtxt(self.obs_dir+"Suhada2012_Chiu2016_XMM-BCS.csv", delimiter=",")
        van = np.genfromtxt(self.obs_dir+"van_der_burg_stellar_masses.csv", delimiter=",")
        h_chiu = cosmo.cosmology(0.7, 0.3).hubble_z(chiu[:,3])
        h_van = cosmo.cosmology(0.7, 0.3).hubble_z(van[:,1])
        M_pivot = 8e13
        B = 0.69 #scaling parameter from Chiu et al 2016
        plt.errorbar(chiu[:,3], h_chiu*chiu[:,30]*(chiu[:,21]*1e13/M_pivot)**(-1.0*B), xerr=[chiu[:,4], abs(chiu[:,5])], yerr=h_chiu*chiu[:,30]*(chiu[:,21]*1e13/M_pivot)**(-1.0*B)*((chiu[:,31]/chiu[:,30])**2 + (B*chiu[:,22]/chiu[:,21])**2)**0.5, c='#ff9980', mec='none', marker='D', linestyle='none', ms=6, label="Chiu et al. 2016 (BCG + sats)  "+'{:.1f}'.format(min(chiu[:,21]))+" < M$_{500}$ (10$^{13} $M$_{\odot}$) < "+'{:.1f}'.format(max(chiu[:,21])))
        ## Convert M200 to M500 by factor 0.631 as in van der burg paper
        plt.errorbar(van[:,1], van[:,17]*h_van*(van[:,7]*0.631*1e14/M_pivot)**(-1.0*B), yerr=van[:,17]*h_van*(van[:,7]*0.631*1e14/M_pivot)**(-1.0*B)*((van[:,18]/van[:,17])**2+(van[:,8]/van[:,7]*B)**2)**0.5, c='#00cc99', mec='none', marker='D', linestyle='none', ms=3, label="van der Burg et al. 2014 (BCG + sats)  "+'{:.1f}'.format(min(van[:,7]*0.631*10))+" < M$_{500}$ (10$^{13} $M$_{\odot}$) < "+'{:.1f}'.format(max(van[:,7]*0.631*10)))
        
        M_min_obs = min(chiu[:,21])*1e13 ## lowest M500
        z_min = chiu[np.argmin(chiu[:,21]),3] ## redshift of lowest M500
        M_min = M_min_obs * cosmo.cosmology(0.7, 0.3).hubble_z(z_min)
        print z_min
        if min(z) > z_min:
            print "Need snapshot of redshift z<"+str(z_min)+" for determining mass sample"
            sys.exit()
        else:
            snapnum = self.snapnums[z.index(min(z))]
            data = np.loadtxt(fout_base+str(snapnum).zfill(3)+".txt")
            mask = (data[:,1]*1e10 > M_min)
            pos_match = np.stack((data[mask,4], data[mask,5], data[mask,6]), axis=1)

        for j, snapnum in enumerate(self.snapnums):
            data = np.genfromtxt(fout_base+str(snapnum).zfill(3)+".txt")
            ID = data[:,0]
            M_delta = data[:,1]
            R_delta = data[:,2]
            M_delta_stars = data[:,3]

            pos = np.stack((data[mask,4], data[mask,5], data[mask,6]), axis=1)
            pos_rel = (np.reshape(pos_match, (1,-1)) - np.reshape(pos, (-1,1))) ## Create matrix of differences in positions
            ind = np.abs(pos_rel).argmin(axis=0)
            ind[len(pos_match):] = 0 ## This isn't ideal - will mean that first element is always included in mask below
            #print ind[:len(pos_match)]
            mask = np.zeros_like(M_delta, dtype=bool) ## boolean array of False
            mask[ind] = True
            print "Median M500 =", np.median(M_delta[mask]*1e10), "at z = ", z[j]

            plt.plot(np.full_like(M_delta[mask], z[j]), M_delta_stars[mask]*1e10/1e12*(M_delta[mask]*1e10/M_pivot)**(-1.0*B), linestyle='none', marker=r"$\odot$", mfc='none', mec='blue', ms=6, mew=0.6, label="Halos of M$_{500}$ > "+'{:.1f}'.format(M_min_obs/1e13)+r" $\times$ 10$^{13} $M$_{\odot}$ at z = 0.0" if j == 0 else "")
        
        plt.title(self.simdir[8:-1])
        plt.xlim(0.05,1.5)
        plt.ylim(0.2, 10)
        plt.xscale('log')
        plt.yscale('log')
        plt.ylabel(r"$M_{\star}(\frac{M_{500}}{M_{piv}})^{-0.69}$ [10$^{12}$ M$_{\odot}$ / h]", fontsize=16)
        plt.xlabel("z", fontsize=16)
        
        plt.axhline(y=M_pivot/1e14*0.7, linestyle='dashdot', c='black')
        plt.text(0.06, M_pivot/1e14*0.7+0.03, "$f_{\star}$ $\simeq$ 0.01", fontsize=14)
        plt.axhline(y=M_pivot/1e13*0.7, linestyle='dashed', c='black')
        plt.text(0.06, M_pivot/1e13*0.7-0.7, "$f_{\star}$ $\simeq$ 0.1", fontsize=14)
  
        ## Set axis labels to decimal format not scientific when using log scale
        plt.gca().yaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))
        plt.gca().xaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))
        plt.legend(loc='upper left', frameon=False, borderaxespad=1, numpoints=1, fontsize=13)

        plt.savefig("stellar_mass_v_redshift.pdf")

    def get_mstar_profiles(self, rmin = 0.0, rmax = None, bins=100):
        ## Gets the total stellar mass of the most massive subhalo in the N most massive halos
        import snap
        import numpy as np
        import matplotlib.pyplot as plt

        N = self.N
        delta = self.delta
        
        for j, snapnum in enumerate(self.snapnums):
            snapshot = snap.snapshot(self.basedir+self.simdir, snapnum, mpc_units=self.mpc_units, masstab=self.masstab, read_params=False)
            print "Loading masses..."
            snapshot.load_mass()
            print "Loading positions..."
            snapshot.load_pos()
            print "Finding host subhalos..."
            snapshot.get_hostsubhalos()
            
            pos = getattr(snapshot.cat, 'group_pos')[:N]
            if self.use_fof and hasattr(snapshot.cat, 'group_r_crit'+str(int(delta))):
                M_delta = getattr(snapshot.cat, 'group_m_crit'+str(int(delta)))[:N]
                R_delta = getattr(snapshot.cat, 'group_r_crit'+str(int(delta)))[:N]
                #print "M_"+ref+","+str(delta)+"[0] =", M_delta[0]
            else:
                M_delta = np.zeros(N)
                R_delta = np.zeros(N)
                for i in range(0, N):
                    M_delta[i], R_delta[i] = snapshot.get_so_mass_rad(i, delta, ref=self.ref, only_sel=False)

            for i in range(0,N):
                ## Indices of star particles within R500 of i'th most massive halo
                ind_full = np.asarray(snapshot.get_particles_sphere(pos[i,:], R_delta[i], parttype=4, longarray=True, verbose=False))

                ## Indices which are star particles and whose host subhalo's parent
                ## is itself i.e. it's within the main halo
                ind = (ind_full == True) & (snapshot.hostsubhalos == snapshot.cat.group_firstsub[i])

                print "Found", np.sum(ind), "star particles in halo", i, "within", R_delta[i], "kpc"
                mstar = snapshot.mass[ind]
                r = np.sqrt(((snapshot.pos[ind, :] - snapshot.cat.sub_pos[snapshot.hostsubhalos[ind],:])**2).sum(axis=1))
                ## Scale the radii
                #r /= R_delta[i]
                #r /= snapshot.cat.sub_halfmassrad[snapshot.hostsubhalos[ind]]
                if rmax is None:
                    rmax = R_delta[i]
                values, bin_edges = np.histogram(r, range=(rmin, rmax), bins=bins, weights=mstar)
                values = np.append(values, values[-1])
                #bin_cent = (bin_edges[:-1] + bin_edges[1:]) / 2.0
                #plt.hist(r, range=(0,100), bins=bins, histtype='step', cumulative=False)#, weights=mstar
                if i == 0:
                    profiles = np.column_stack((bin_edges, values))
                else:
                    profiles = np.column_stack((profiles, bin_edges, values))

            #plt.show()
            np.savetxt("stellar_mass_profiles_"+str(snapnum).zfill(3)+".txt", profiles, fmt='%.5e', header="Radial bin centre (kpc), M_"+str(int(delta))+"_stars (Msun)"+str(np.arange(0,N))[1:-1])
            del snapshot

    def plot_mstar_profiles(self, nplot=0, rmax=0, bins=0):
        import numpy as np
        import matplotlib.pyplot as plt
        from matplotlib.backends.backend_pdf import PdfPages
        from scipy.interpolate import interp1d
        import matplotlib
        matplotlib.rcParams['lines.linewidth'] = 1.5

        print "Plotting to stellar_mass_profiles.pdf..."
        with PdfPages("stellar_mass_profiles.pdf") as pdf:
            for j, snapnum in enumerate(self.snapnums):
                fig = plt.figure(figsize=(8,10))
                plt.xlabel("R (kpc)")
                plt.ylabel("M$_{\star}$(R) (10$^{10}$ M$_{\odot}$)")
                plt.title(self.simdir[8:-1]+"\nz = "+'{:.3f}'.format(self.z[j]), fontsize=12)
                profiles = np.loadtxt("stellar_mass_profiles_"+str(snapnum).zfill(3)+".txt")
                if nplot == 0:
                    nplot = self.N
                if len(profiles[0,:]) < nplot:
                    print "WARNING: Requested", nplot, "halos but only", len(profiles[0,:]), "have been computed using get_mstar_profiles()"
                    nplot = len(profiles[0,:])
                for i in np.arange(0, nplot*2, 2):
                    binleft = profiles[:,i]
                    bincent = (binleft[1:] + binleft[:-1]) / 2.0
                    values = profiles[:-1,i+1]
                    if bins > 0:
                        if rmax > 0:
                            r_int = np.linspace(min(bincent), rmax, num=bins, endpoint=True)
                        else:
                            r_int = np.linspace(min(bincent), max(bincent), num=bins, endpoint=True)
                        f = interp1d(bincent, values, kind='cubic')
                        plt.plot(r_int, f(r_int), label="Group "+str(int(i/2)))
                    else:
                        plt.plot(bincent, values, label="Group "+str(int(i/2)))
                    if rmax > 0:
                        plt.xlim(0.0, rmax)
                    plt.legend(loc='best', frameon=False, borderaxespad=1, numpoints=1, fontsize=14)

                pdf.savefig()

    def get_ids_stellar_components(self, snapnum, group = 0, cd_cutoff_method=0, cutoff_rad=30.0, include_fuzzy=True):
        ## cutoff_method - method used to distinguish BCG
        ##               - 0 Straight cut of cutoff_rad around most massive subhalo

        import snap
        import sys
        import numpy as np

        self.group = group ## group ID
        self.snapnum_now = snapnum

        if cd_cutoff_method == 0:
            print "Using BCG cutoff_rad =", cutoff_rad, "kpc"
        else:
            print "ERROR: cd_cutoff_method not recognised."
            sys.exit(1)

        snapshot = snap.snapshot(self.basedir+self.simdir, snapnum, mpc_units=self.mpc_units, masstab=self.masstab, read_params=False)
        print "Loading IDs..."
        snapshot.load_ids()
        print "Loading positions..."
        snapshot.load_pos()
        print "Loading stellar ages..."
        snapshot.load_stellar_ages()
        print "Finding host subhalos..."
        snapshot.get_hostsubhalos()

        ## Get star particles
        snapshot.get_parttype()
        part_ids = snapshot.ids[snapshot.parttype == 4]
        part_pos = snapshot.pos[snapshot.parttype == 4]
        part_age = snapshot.age
        assert len(part_age) == len(part_ids), "Stars ID list is different length to stars ages list"
        part_hostsubhalos = snapshot.hostsubhalos[snapshot.parttype == 4]

        ## Order by IDs
        ind = np.argsort(part_ids)
        part_ids = part_ids[ind]
        part_pos = part_pos[ind]
        part_age = part_age[ind]
        part_hostsubhalos = part_hostsubhalos[ind]

        ## Position of most massive subhalo in group
        centerpos = snapshot.cat.sub_pos[snapshot.cat.group_firstsub[group]]

        ## Get IDs of star particles in satellites (any subhalo except the first)
        gal_ind = (part_hostsubhalos > snapshot.cat.group_firstsub[group]) & (part_hostsubhalos < snapshot.cat.group_firstsub[group] + snapshot.cat.group_nsubs[group])
        gal_ids = part_ids[gal_ind]
        gal_ids_size = len(gal_ids)
        gal_age = part_age[gal_ind]
        gal_pos = part_pos[gal_ind]

        ## Get IDs of star particles in BCG or ICL (ie. main subhalo)
        icl_ids = part_ids[part_hostsubhalos == snapshot.cat.group_firstsub[group]]
        if include_fuzzy:
            if not hasattr(snapshot.cat, "group_offsettab"):
                #print "\033[93mWARNING:\033[0m group_offsettab not found. Gadget type catalogue?"
                print "Computing fuzzy ids..."
                icl_ids = np.concatenate((icl_ids, snapshot.ids[snapshot.cat.sub_offset[snapshot.cat.group_firstsub[group]+snapshot.cat.group_nsubs[group]-1]+snapshot.cat.sub_len[snapshot.cat.group_firstsub[group]+snapshot.cat.group_nsubs[group]-1]:snapshot.cat.group_len[group]-1]))
            else:
                ## Include star particles in group but no subhalo - may be zero
                grp_offset = snapshot.cat.group_offsettab.sum(axis=1)
                offset = grp_offset[group]+snapshot.cat.group_offsettab[group,:4].sum()
                grp_ids = snapshot.ids[offset:offset+snapshot.cat.group_lentab[group, 4]-1] # IDs of star particles in group
                ind = (snapshot.hostsubhalos[offset:offset+snapshot.cat.group_lentab[group, 4]-1] == -1) # Not in a subhalo
                icl_ids = np.concatenate((icl_ids, grp_ids[ind]))

        #icl_ids = np.sort(icl_ids)
        ## get indices where part_ids are in icl_ids
        matcher = set(icl_ids) ## set for quicker matching - does this affect icl_ids?
        ind = [i for i, x in enumerate(part_ids) if x in matcher]
        icl_age = part_age[ind]
        icl_pos = part_pos[ind]
        icl_rad = snapshot.nearest_dist_3D(icl_pos, centerpos)
        print "Stars in BCG + ICL =", len(icl_ids)

        ## Exclude BCG particles to get ICL and BCG only IDs
        if cd_cutoff_method <= 1:
            ind = np.where(icl_rad <= cutoff_rad)
            bcg_ids = icl_ids[ind]
            bcg_ids_size = len(bcg_ids)
            bcg_age = icl_age[ind]
            bcg_pos = icl_pos[ind]
            ind = np.where(icl_rad > cutoff_rad)
            icl_ids = icl_ids[ind]
            icl_ids_size = len(icl_ids)
            icl_age = icl_age[ind]
            icl_pos = icl_pos[ind]

        self.gal_ids = gal_ids
        self.gal_age = gal_age
        self.gal_pos = gal_pos
        self.bcg_ids = bcg_ids
        self.bcg_age = bcg_age
        self.bcg_pos = bcg_pos
        self.icl_ids = icl_ids
        self.icl_age = icl_age
        self.icl_pos = icl_pos
        
        print "gal stars =", gal_ids_size
        print "bcg stars =", bcg_ids_size
        print "icl stars =", icl_ids_size
        print "gal star fraction =", float(gal_ids_size) / (gal_ids_size+bcg_ids_size+icl_ids_size)
        print "bcg star fraction =", float(bcg_ids_size) / (gal_ids_size+bcg_ids_size+icl_ids_size)
        print "icl star fraction =", float(icl_ids_size) / (gal_ids_size+bcg_ids_size+icl_ids_size)

    def get_props_component_stars_cursnap(self, snapnum, check_before=0, new_thresh = 0.05):
        ## new_thresh = 0.05 - new stars are younger than cur_time*new_thresh
        
        import sys
        import snap
        import numpy as np
        
        ## Get IDs of stars that will end up in galaxies, BCG or ICL
        assert hasattr(self, "gal_ids"), "Need IDs of stellar components"

        snapshot = snap.snapshot(self.basedir+self.simdir, snapnum, mpc_units=self.mpc_units, masstab=self.masstab, read_params=False)
        print "Loading stellar ages..."
        try:
            snapshot.load_stellar_ages()
        except SystemExit:
            print "\033[93mSkipping:\033[0m snapshot", snapnum
            return
        print "Loading IDs..."
        snapshot.load_ids()
        print "Loading positions..."
        snapshot.load_pos()
        print "Finding host subhalos..."
        snapshot.get_hostsubhalos()

        ## Get star particles
        snapshot.get_parttype()
        part_ids = snapshot.ids[snapshot.parttype == 4]
        part_pos = snapshot.pos[snapshot.parttype == 4]
        part_age = snapshot.age
        assert len(part_age) == len(part_ids), "Stars ID list is different length to stars ages list"
        part_hostsubhalos = snapshot.hostsubhalos[snapshot.parttype == 4]

        ## Get star particles in current snapshot with matching IDs
        match_gal = set(self.gal_ids)
        ind = [i for i, x in enumerate(part_ids) if x in match_gal]
        cur_gal_ids = part_ids[ind]
        cur_gal_pos = part_pos[ind]
        cur_gal_age = part_age[ind]
        cur_gal_hostsubhalo = part_hostsubhalos[ind]

        match_bcg = set(self.bcg_ids)
        ind = [i for i, x in enumerate(part_ids) if x in match_bcg]
        cur_bcg_ids = part_ids[ind]
        cur_bcg_pos = part_pos[ind]
        cur_bcg_age = part_age[ind]
        cur_bcg_hostsubhalo = part_hostsubhalos[ind]

        match_icl = set(self.icl_ids)
        ind = [i for i, x in enumerate(part_ids) if x in match_icl]
        cur_icl_ids = part_ids[ind]
        cur_icl_pos = part_pos[ind]
        cur_icl_age = part_age[ind]
        cur_icl_hostsubhalo = part_hostsubhalos[ind]

        time = snapshot.header.time
        #centerpos = snapshot.cat.sub_pos[snapshot.cat.group_firstsub[self.group]]
        centerpos = snapshot.cat.group_pos[self.group]
        if check_before == 0:
            print "Finding new stars after formation..."
            ind = np.where(cur_gal_age > (1.0-new_thresh)*time) #'age'=formation time
            gal_new_ids = cur_gal_ids[ind]
            print "Calculating radii..."
            gal_new_pos = cur_gal_pos[ind]
            gal_new_hostsubhalo = cur_gal_hostsubhalo[ind]
            ## Initialise centers of hostsubhalos to center of group
            hostcenter = np.column_stack((np.full_like(gal_new_pos[:,0], centerpos[0]),np.full_like(gal_new_pos[:,1], centerpos[1]),np.full_like(gal_new_pos[:,2], centerpos[2])))
            ## get center of hostsubhalo for parts in a subhalo
            hostcenter[(gal_new_hostsubhalo > -1),:] = snapshot.cat.sub_pos[gal_new_hostsubhalo[(gal_new_hostsubhalo > -1)],:]
            gal_new_rad = snapshot.nearest_dist_3D(gal_new_pos, hostcenter)

            ind = np.where(cur_bcg_age > (1.0-new_thresh)*time)
            bcg_new_ids = cur_bcg_ids[ind]
            bcg_new_pos = cur_bcg_pos[ind]
            bcg_new_hostsubhalo = cur_bcg_hostsubhalo[ind]
            hostcenter = np.column_stack((np.full_like(bcg_new_pos[:,0], centerpos[0]),np.full_like(bcg_new_pos[:,1], centerpos[1]),np.full_like(bcg_new_pos[:,2], centerpos[2])))
            hostcenter[(bcg_new_hostsubhalo > -1),:] = snapshot.cat.sub_pos[bcg_new_hostsubhalo[(bcg_new_hostsubhalo > -1)],:]
            bcg_new_rad =  snapshot.nearest_dist_3D(bcg_new_pos, hostcenter)

            ind = np.where(cur_icl_age > (1.0-new_thresh)*time)
            icl_new_ids = cur_icl_ids[ind]
            icl_new_pos = cur_icl_pos[ind]
            icl_new_hostsubhalo = cur_icl_hostsubhalo[ind]
            hostcenter = np.column_stack((np.full_like(icl_new_pos[:,0], centerpos[0]),np.full_like(icl_new_pos[:,1], centerpos[1]),np.full_like(icl_new_pos[:,2], centerpos[2])))
            hostcenter[(icl_new_hostsubhalo > -1),:] = snapshot.cat.sub_pos[icl_new_hostsubhalo[(icl_new_hostsubhalo > -1)],:]
            icl_new_rad =  snapshot.nearest_dist_3D(icl_new_pos, hostcenter)
            
        elif (check_before == 1 and snapnum < self.snapnum_now):
            ## Get gas particles that produce stars in sats, bcg or icl later on
            print "Finding new stars before formation (gas)..."
            gas_ids = snapshot.ids[snapshot.parttype == 0]
            
            ind = [i for i, x in enumerate(gas_ids) if x in match_gal]
            cur_gal_gas_ids = gas_ids[ind]
            matcher = set(cur_gal_gas_ids)
            ind = [i for i, x in enumerate(self.gal_ids) if x in matcher]
            cur_gal_gas_age = self.gal_age[ind]
            cur_gal_gas_pos = self.gal_pos[ind]

            ind = [i for i, x in enumerate(gas_ids) if x in match_bcg]
            cur_bcg_gas_ids = gas_ids[ind]
            matcher = set(cur_bcg_gas_ids)
            ind = [i for i, x in enumerate(self.bcg_ids) if x in matcher]
            cur_bcg_gas_age = self.bcg_age[ind]
            cur_bcg_gas_pos = self.bcg_pos[ind]

            ind = [i for i, x in enumerate(gas_ids) if x in match_icl]
            cur_icl_gas_ids = gas_ids[ind]
            matcher = set(cur_icl_gas_ids)
            ind = [i for i, x in enumerate(self.icl_ids) if x in matcher]
            cur_icl_gas_age = self.icl_age[ind]
            cur_icl_gas_pos = self.icl_pos[ind]
            
            ind = np.where(cur_gal_gas_age < time*(1 + new_thresh))
            gal_new_ids = cur_gal_gas_ids[ind]
            gal_new_rad = snapshot.nearest_dist_3D(cur_gal_gas_pos[ind], centerpos)

            ind = np.where(cur_bcg_gas_age < time*(1 + new_thresh))
            bcg_new_ids = cur_bcg_gas_ids[ind]
            bcg_new_rad = snapshot.nearest_dist_3D(cur_bcg_gas_pos[ind], centerpos)

            ind = np.where(cur_icl_gas_age < time*(1 + new_thresh))
            icl_new_ids = cur_icl_gas_ids[ind]
            icl_new_rad = snapshot.nearest_dist_3D(cur_icl_gas_pos[ind], centerpos)

        elif (check_before == 1 and snapnum == self.snapnum_now):
            print "\033[93mWARNING:\033[0m: Cannot find gas formation particles for last snapshot "+str(self.snapnum_now)
            return
        else:
            print "Check_before: 0 or 1"
            sys.exit(1)

        h = self.cm.hubble_z(snapshot.header.redshift)
            
        if hasattr(self, "gal_new_rad"):
            self.gal_new_rad = np.concatenate((self.gal_new_rad, gal_new_rad / h))
            self.bcg_new_rad = np.concatenate((self.bcg_new_rad, bcg_new_rad / h))
            self.icl_new_rad = np.concatenate((self.icl_new_rad, icl_new_rad / h))
        else:
            self.gal_new_rad = gal_new_rad / h
            self.bcg_new_rad = bcg_new_rad / h
            self.icl_new_rad = icl_new_rad / h

        print "Done snapshot", snapnum

    def get_new_stars_vs_distance(self, snapnum_min=0, check_before=0, new_thresh = 0.05):
        import numpy as np

        if hasattr(self, "gal_new_rad"):
            del self.gal_new_rad
            del self.bcg_new_rad
            del self.icl_new_rad

        assert hasattr(self, "gal_ids"), "Need IDs of stellar components for snap "+str(snapnum)
        for snapnum in range(snapnum_min, self.snapnum_now+1):
            self.get_props_component_stars_cursnap(snapnum, check_before=check_before, new_thresh = new_thresh)

        for name in ["gal", "bcg", "icl"]:
            np.savetxt("new_"+name+"_stars_distance_snap"+str(self.snapnum_now)+"_grp"+str(self.group)+".txt", getattr(self, name+"_new_rad"), header="Radial distance (kpc/h) from birth halo centre for stars which end up in "+name+" of group "+str(self.group)+" in snapshot "+str(self.snapnum_now))

    def plot_new_stars_vs_distance(self):
        import matplotlib.pyplot as plt
        import matplotlib.ticker as ticker
        import numpy as np
        
        if not hasattr(self, "snapnum_now"):
            self.snapnum_now = max(self.snapnums)
        if not hasattr(self, "group"):
            self.group = 0

        try:
            self.gal_new_rad = np.loadtxt("new_gal_stars_distance_snap"+str(self.snapnum_now)+"_grp"+str(self.group)+".txt")
        except IOError:
            print "Cannot find new_gal_stars_distance_snap"+str(self.snapnum_now)+"_grp"+str(self.group)+".txt"
            print "Have you run get_new_stars_vs_distance?"
            sys.exit(1)
        self.bcg_new_rad = np.loadtxt("new_bcg_stars_distance_snap"+str(self.snapnum_now)+"_grp"+str(self.group)+".txt")
        self.icl_new_rad = np.loadtxt("new_icl_stars_distance_snap"+str(self.snapnum_now)+"_grp"+str(self.group)+".txt")
        
        minR = 0.1
        maxR = 10000
        bins = np.logspace(np.log10(minR), np.log10(maxR), num=100, endpoint=True)
        plt.hist(self.gal_new_rad, bins=bins, histtype='step', label="Satellite Galaxies")
        plt.hist(self.bcg_new_rad, bins=bins, histtype='step', label="BCG")
        plt.hist(self.icl_new_rad, bins=bins, histtype='step', label="ICL")
        plt.xscale('log')
        plt.xlim(minR, maxR)
        plt.xlabel("Distance from halo centre [kpc/h]")
        plt.ylabel("Stars formed per bin")
        plt.gca().yaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: r'{}$\times$10$^{{{}}}$'.format('{:.0e}'.format(y).split('e')[0], int('{:e}'.format(y).split('e')[1]))))
        plt.gca().xaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))
        plt.legend(loc='best', frameon=False, borderaxespad=1, numpoints=1, fontsize=14)
        plt.show()
            


if __name__ == "__main__":
    main()
