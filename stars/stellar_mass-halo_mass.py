#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 11 18:02:28 2017
Plot stellar mass vs halo mass
@author: nh444
"""
import snap
import matplotlib.pyplot as plt
from matplotlib.ticker import NullFormatter
import numpy as np
h = 0.679
outdir = "/data/curie4/nh444/project1/stars/"
title=None

basedir = "/data/curie4/nh444/project1/boxes/"
mpc_units = False
subdirs = ["L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff",
           "L40_512_LDRIntRadioEffBHVel",
           "L40_512_LDRIntRadioEff_SoftSmall",
           ]
labels = ["small softenings", "larger softenings"]
labels = ["$\epsilon$(z=0)=0.3375 $z_{\epsilon}=3$", "$\epsilon$(z=0)=2.4 $z_{\epsilon}=5$", "$\epsilon$(z=0)=2.4 $z_{\epsilon}=1$"]
outname = "stellar_mass-halo_mass_softening_z4.pdf"
title = ""

snapnum = 8
centrals = False

xbins = np.logspace(6, 13, num=50) ## halo mass Msun
ybins = np.logspace(6, 13, num=50) ## stellar mass Msun

# axes dimensions
left, width = 0.1, 0.65
bottom, height = 0.1, 0.65
left_h = left + width + 0.02
bottom_h = bottom + height + 0.02
rect_scatter = [left, bottom, width, height]
rect_histx = [left, bottom_h, width, 0.2]
rect_histy = [left_h, bottom, 0.2, height]

fig = plt.figure(figsize=(10,10))
axScat = plt.axes(rect_scatter)
axHistx = plt.axes(rect_histx)
axHisty = plt.axes(rect_histy)
axes = fig.get_axes()

axScat.set_xlabel("$M_{500}^{\mathrm{Crit}}$ [M$_{\odot}$]")
if centrals:
    axScat.set_ylabel("Central total stellar mass [M$_{\odot}$]")
else:
    axScat.set_ylabel("Total bound stellar mass [M$_{\odot}$]")
axScat.set_yscale('log')
axScat.set_xscale('log')
axHistx.set_xscale('log')
axHisty.set_yscale('log')

nxmax = 0
nymax = 0

for dirnum, subdir in enumerate(subdirs):
    sim = snap.snapshot(basedir+subdir, snapnum, mpc_units=mpc_units, extra_sub_props=True)
    
    if centrals:
        grpind = np.where(sim.cat.group_firstsub > 0)[0] ## some will be -1 i.e. no subhalo
        mstar = sim.cat.sub_masstab[sim.cat.group_firstsub[grpind],4]
        ind = np.where(mstar > 0.0)[0]
        mstar = mstar[ind] * 1e10 / h
        M500 = sim.cat.group_m_crit500[grpind[ind]] * 1e10 / h
    else:
        mstar = sim.cat.group_masstab[:,4]
        grpind = np.where(mstar > 0.0)[0]
        mstar = mstar[grpind] * 1e10 / h
        M500 = sim.cat.group_m_crit500[grpind] * 1e10 / h
    p, = axScat.plot(M500, mstar, mec='none', alpha=0.5, marker='o', ms=2, ls='none', label=labels[dirnum])
    nx, bin_edges, count = axHistx.hist(M500, bins=xbins, histtype='step')
    ny, bin_edges, count = axHisty.hist(mstar, bins=ybins, orientation='horizontal', histtype='step')

    if np.max(nx) > nxmax: nxmax = np.max(nx)
    if np.max(ny) > nymax: nymax = np.max(ny)

axHistx.set_xlim(axScat.get_xlim())
axHistx.set_ylim(0, int(nxmax*1.1)+1)
axHisty.set_ylim(axScat.get_ylim())
axHisty.set_xlim(0, int(nymax*1.1)+1)
for label in axHisty.get_xticklabels():
    label.set_rotation(90)

nullfmt = NullFormatter() # for no labels
axHistx.xaxis.set_major_formatter(nullfmt)
axHisty.yaxis.set_major_formatter(nullfmt)

if title is not None:
    axHistx.set_title(title+" z={:.1f}".format(sim.header.redshift))
    
axScat.legend(loc='upper left', frameon=False)
fig.savefig(outdir+outname)
print "Output to", outdir+outname










