#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  3 20:26:25 2017

@author: nh444
"""
import numpy as np
import scipy.stats as stats
import matplotlib.pyplot as plt
import snap

#==============================================================================
# indirs = ["/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c256_box_Arepo/",
#           "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c256_fixed/"]
# labels=["c256 old arepo", "c256 new arepo"]
# outname = "/data/curie4/nh444/project1/stars/sfr_vs_radius_c256_old_new_arepo_main_halo.pdf"
#==============================================================================

indirs = ["/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c128_box_Arepo/",
          "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c128_new/",
          "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c128_new_ThermFeedbackFix/",
          "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c128_new_FeedbackFix/"]
labels=["c128 old arepo", "c128 new arepo", "c128 new arepo feedback fix", "c128 new arepo feedback mode fix"]
outname = "/data/curie4/nh444/project1/stars/sfr_vs_radius_c128_old_new_arepo.pdf"

#==============================================================================
# indirs = ["/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c96_box_Arepo/",
#           "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c96/",
#           "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c96_ThermFeedbackFix/",
#           "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c96_FeedbackFix/",
#           ]
# labels=["c96 old arepo", "c96 new arepo", "c96 new arepo feedback fix", "c96 new arepo feedback mode fix"]
# outname = "/data/curie4/nh444/project1/stars/sfr_vs_radius_c96_old_new_arepo.pdf"
#==============================================================================

mpc_units=True
usehighres=True
main_halo_only=False
group = 0
subhalo = 0
snapnums = range(20,26)


col = ["#c7673e", "#78a44f", "#6b87c8","#934fce","#be558f","#47af8d"]
from matplotlib.backends.backend_pdf import PdfPages
pdf_pages = PdfPages(outname)
for snapnum in snapnums:
    fig, ax1 = plt.subplots()
    ax2 = ax1.twinx()
    for i, indir in enumerate(indirs):
        sim = snap.snapshot(indir, snapnum, mpc_units=mpc_units)
        sim.load_sfr()
        sim.load_gaspos()
        sim.load_bhpos()
        sim.load_bhmass()
        
        #sim.get_hostsubhalos()
        #sim.get_sub_bh_props(save_pos=True)
        #r = sim.nearest_dist_3D(sim.gaspos, sim.sub_bhpos1[sim.cat.group_firstsub[group]+subhalo])
        #print "BH mass =", sim.sub_bhmass1[sim.cat.group_firstsub[group]+subhalo], "pos =", sim.sub_bhpos1[sim.cat.group_firstsub[group]+subhalo]
        #print "subhalo mass =", sim.cat.sub_mass[sim.cat.group_firstsub[group]+subhalo], "pos =", sim.cat.sub_pos[sim.cat.group_firstsub[group]+subhalo]
        
        ## Use most massive blackhole within R500 of given group (because it's not always bound to the main subhalo)
        R = sim.cat.group_r_crit500[group]
        bhrad = sim.nearest_dist_3D(sim.bhpos, sim.cat.group_pos[group])
        bhind = np.where(bhrad <= R)[0]
        bhidx = bhind[np.argmax(sim.bhmass[bhind])]
        r = sim.nearest_dist_3D(sim.gaspos, sim.bhpos[bhidx])
        #r = sim.nearest_dist_3D(sim.gaspos, sim.cat.sub_pos[sim.cat.group_firstsub[group]+subhalo])
        print "BH mass =", sim.bhmass[bhidx], "pos =", sim.bhpos[bhidx]
        print "group mass =", sim.cat.group_mass[group], "pos =", sim.cat.group_pos[group]
        
        bins = np.logspace(np.log10(3.0e-3*R), np.log10(100.0*R), num=70)
        ind = np.where(sim.sfr >= 0.0)[0]
        if usehighres:
            sim.load_gasmass()
            sim.load_highresgasmass()
            ind = np.intersect1d(ind, np.where(sim.hrgm > 0.5*sim.gasmass)[0])
        if main_halo_only:
            if not hasattr(sim, "hostsubhalos"):
                sim.get_hostsubhalos()
            ind = np.intersect1d(ind,np.where(sim.hostsubhalos[sim.species_start[0]:sim.species_end[0]] == sim.cat.group_firstsub[group]+subhalo)[0])
        #sfr, bin_edges = np.histogram(r[ind], bins=bins, weights=sim.sfr[ind])
        sfr, bin_edges, n = stats.binned_statistic(r[ind], sim.sfr[ind], statistic='sum', bins=bins)
        #print "sfr =", sfr
        ax1.plot(bin_edges[:-1]/R, np.cumsum(sfr), c=col[i], label=labels[i], lw=2)
        count, bin_edges, n = stats.binned_statistic(r[ind], sim.sfr[ind], statistic='count', bins=bins)
        dens = count / (4.0/3.0*np.pi*(bin_edges[1:]**3 - bin_edges[:-1]**3))
        ax2.plot(bin_edges[:-1]/R, dens, c=col[i], ls='dashed', lw=2)
                
    ax1.set_xscale('log')
    ax2.set_yscale('log')
    ax1.set_ylabel("Cumulative Total SFR")
    ax2.set_ylabel("Number density ($h^{-3}$ kpc$^{-3}$)")
    ax1.set_xlabel("r/r$_{500}$")
    ax1.legend(frameon=False)#, loc='upper left')
    ax1.set_title("snap "+str(snapnum)+" t={:.2f}".format(sim.header.time)+" z={:.1f}".format(sim.header.redshift))
    #fig.savefig(outname, bbox_inches='tight')
    pdf_pages.savefig(fig)
print "Plot saved to", outname
pdf_pages.close()
