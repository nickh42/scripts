#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  3 20:26:25 2017

@author: nh444
"""
import numpy as np
import matplotlib.pyplot as plt
import snap

indirs = ["/data/emergence4/deboras/Elliptical_ZoomIns/L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff/",
          "/data/curie4/nh444/project1/L40_512_LDRIntRadioEffBHVel_broken/",
          #"/data/curie4/nh444/project1/L40_512_LDRIntRadioEffBHVel_ThermFeedbackFix/",
          "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c96_box_Arepo/",
          #"/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c96/",
          "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c96_FeedbackFix/",
          "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c96_FeedbackFix_OldGrav/",
          "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c128_box_Arepo/",
          #"/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c128_new/",
          "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c128_new_FeedbackFix/",
          "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c128_new_FeedbackFix_OldGrav/",
          #"/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c256_box_Arepo/",
          #"/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c256_fixed/",
          ]

mpc_units=True
usehighres=True
labels=["box old arepo", "box new arepo",
        "c96 old arepo", #"c96 new arepo",
        "c96 new feedback mode fix", "c96 new feedback mode fix, old grav",
        "c128 old arepo", #"c128 new arepo",
        "c128 new feedback mode fix", "c128 new feedback mode fix, old grav",
        "c256 old arepo", "c256 new arepo",
        ]
outdir = "/data/curie4/nh444/project1/stars/"
filebases = ["sfr-HaloMass_box_old_arepo", "sfr-HaloMass_box_new_arepo", #"sfr-HaloMass_box_new_arepo_ThermFeedbackFix",
             "sfr-HaloMass_c96_old_arepo", #"sfr-HaloMass_c96_new_arepo",
             "sfr-HaloMass_c96_new_arepo_fixed", "sfr-HaloMass_c96_new_arepo_fixed_oldgrav",
             "sfr-HaloMass_c128_old_arepo", #"sfr-HaloMass_c128_new_arepo",
             "sfr-HaloMass_c128_new_arepo_fixed", "sfr-HaloMass_c128_new_arepo_fixed_oldgrav",
             "sfr-HaloMass_c256_old_arepo", "sfr-HaloMass_c256_new_arepo",
             ]
#==============================================================================
# indirs = ["/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c128_box_Arepo/",
#           "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c128_fixed/"]
# mpc_units=True
# usehighres=True
# labels=["c128 old arepo", "c128 new arepo"]
# outname = "/data/curie4/nh444/project1/stars/sfr_c128_old_new_arepo.pdf"
# indirs = ["/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c256_box_Arepo/",
#           "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c256_fixed/"]
# labels=["c256 old arepo", "c256 new arepo"]
# outname = "/data/curie4/nh444/project1/stars/sfr_c256_old_new_arepo.pdf"
#==============================================================================
calculate_data = False
snapnum = 21
main_halo_only = True
figname = "sfr-HaloMass_old_new_arepo_main_halo_snap"+str(snapnum)+".pdf"

if calculate_data:
    for i, indir in enumerate(indirs):
        if main_halo_only:
            datname = filebases[i]+"_main_halo_snap"+str(snapnum)+".dat"
        else:
            datname = filebases[i]+"_snap"+str(snapnum)+".dat"
        sim = snap.snapshot(indir, snapnum, mpc_units=mpc_units)
        print "Loading sim data..."
        sim.load_gaspos()
        sim.load_sfr()
        if usehighres:
            sim.load_gasmass()
            sim.load_highresgasmass()
        if main_halo_only:
            sim.get_hostsubhalos()
        sfr = []
        mass = []
        N = sim.cat.ngroups
        for group in range(N):
            if sim.cat.group_m_crit500[group] / sim.header.hubble*1e10 > 1e12:
                ind = np.where(sim.nearest_dist_3D(sim.gaspos, sim.cat.group_pos[group]) <= sim.cat.group_r_crit500[group])[0]
                if usehighres:
                    ind = np.intersect1d(ind, np.where(sim.hrgm > 0.5*sim.gasmass)[0])
                if main_halo_only:
                    ind = np.intersect1d(ind, np.where(sim.hostsubhalos[sim.species_start[0]:sim.species_end[0]] == sim.cat.group_firstsub[group])[0])
                sfr.append(sim.sfr[ind].sum(dtype=np.float64))
                mass.append(sim.cat.group_m_crit500[group] / sim.header.hubble * 1e10)
                print "Got group", group
        data = np.column_stack((mass, sfr))
        np.savetxt(outdir+datname, data)

markers = ['.']*2+['s']*3+['D']*3+['^']*3
ms = [6]*2+[7]*3+[6]*3+[8]*3
col = ["#c7673e", "#78a44f", "#6b87c8","#934fce","#be558f","#47af8d"]
mfc = ["#c7673e", "#78a44f"]+["none"]*(len(indirs)-2)
mec = ["none", "none"]+["#c7673e","#78a44f","#6b87c8"]*(len(indirs)-2)
fig, ax1 = plt.subplots()
for i in range(len(indirs)):
    if main_halo_only:
        datname = filebases[i]+"_main_halo_snap"+str(snapnum)+".dat"
    else:
        datname = filebases[i]+"_snap"+str(snapnum)+".dat"
    data = np.loadtxt(outdir+datname)
    ax1.plot(data[:,0], data[:,1], ls='none', marker=markers[i], mec=mec[i], mfc=mfc[i], ms=ms[i], mew=1.5, label=labels[i])
ax1.set_ylabel("Total SFR")
ax1.set_xlabel("M$_{500}$ [Msun]")
ax1.set_xscale('log')
ax1.set_yscale('log')
ax1.set_xlim(xmax=3e13)
ax1.legend(numpoints=1, loc='lower right', fontsize=10)
#fig.suptitle("z={:.1f} t={:.2f}".format(sim.header.redshift, sim.header.time))
fig.savefig(outdir+figname, bbox_inches='tight')
print "Figure output to", outdir+figname

