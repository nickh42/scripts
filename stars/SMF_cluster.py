#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 20 21:03:48 2017

@author: nh444
"""
def main():   
    h_scale = 0.679
    outdir = "/data/curie4/nh444/project1/stars/" ## where stellar masses are saved
    
    basedir = "/data/curie4/nh444/project1/boxes/"
    simdirs = ["L40_512_MDRIntRadioEff/"]
    snapnums = [25]
    
    #basedir = "/home/nh444/data/project1/zoom_ins/LDRIntRadioEffBHVel/"
    #simdirs = ["c96_box_Arepo_new", "c128_box_Arepo_new"]#, "c160_box_Arepo_new", "c256_box_Arepo_new", "c320_box_Arepo_new", "c384_box_Arepo_new", "c448_box_Arepo_new",]
    #simdirs = ["c384_box_Arepo_new"]#, "c320_box_Arepo_new", "c448_box_Arepo_new"]
    
    basedir = "/data/curie4/nh444/project1/zoom_ins/MDRInt/"
    simdirs = [#"c96_MDRInt", "c128_MDRInt", "c192_MDRInt", "c224_MDRInt", "c256_MDRInt",
               #"c288_MDRInt", 
               #"c320_MDRInt",
               #"c352_MDRInt", "c384_MDRInt",               
               "c448_MDRInt","c512_MDRInt",
               "c400_MDRInt", "c410_MDRInt",
               ]
    simdirs = ["c96_MDRInt", "c128_MDRInt", "c160_MDRInt",
               "c192_MDRInt", "c224_MDRInt", "c256_MDRInt",
               "c288_MDRInt", "c320_MDRInt",
               "c352_MDRInt", "c384_MDRInt",
               "c448_MDRInt", "c480_MDRInt", "c512_MDRInt",
               "c112_MDRInt", "c144_MDRInt", "c176_MDRInt", "c208_MDRInt",
               "c240_MDRInt", "c272_MDRInt", "c304_MDRInt", "c336_MDRInt",
               "c368_MDRInt", "c400_MDRInt", "c410_MDRInt", "c432_MDRInt",
               "c464_MDRInt", "c496_MDRInt_SUBF",
          ]
#    snapnums = [25]*(len(simdirs)-2)+[99,99]
#    snapnums = [22]*(len(simdirs)-2)+[87,87]
    snapnums = [25]*13+[99]*14
#    simdirs = ["c384_MDRInt",
#               "c448_MDRInt",
#               "c400_MDRInt",
#               "c410_MDRInt",
#               ]
#    snapnums = [25,
#                25,
#                99,
#                99,
#                ]

    mpc_units = True
    
#    list_halo_masses(basedir, simdirs, snapnums, mpc_units=mpc_units)
#    plot_SMF_cluster_vs_obs(outdir=outdir, h_scale=h_scale)
#    plot_SMF_cluster_vs_vanderBurg(outdir=outdir, h_scale=h_scale)
    #compare_SMF_cluster(sats_only=True, outdir=outdir, h_scale=h_scale)
    #compare_cum_mass_cluster(sats_only=True, outdir=outdir, h_scale=h_scale)
    #compare_cum_mass_obs(sats_only=True, outdir=outdir, h_scale=h_scale)
    fbasedir = "/data/curie4/nh444/project1/boxes/"
    fsimdirs = ["L40_512_MDRIntRadioEff/"]
    fsnapnum = 25
    compare_SMF_field_cluster(fbasedir, fsimdirs, fsnapnum,  # field
                              basedir, simdirs, snapnums,  # clusters/groups
                              outdir=outdir,
                              fsats_only=False, csats_only=False,
                              M500lims=[3e14, 1e16],
                              outsuffix="_R500_no_BCGs_scaled_by_tot_mass",
                              bound=False, no_BCGs=True,
                              norm_by_sat_mass=False, norm_by_total_mass=True,
                              aperture=False, h_scale=h_scale)
#    compare_cum_mass_field_cluster(outdir=outdir,
#                                   bound=False, sats_only=False, outsuffix="_all",
#                                   aperture=False, h_scale=h_scale)
    # Kravtsov+18 comparison
#    plot_SMF_normed_by_halo_mass(bound=False, sats_only=True,
#                                  outdir=outdir, outsuffix="",
#                                  aperture=False, h_scale=h_scale)


def compare_SMF_field_cluster(fbasedir, fsimdirs, fsnapnum,  # field
                              cbasedir, csimdirs, csnapnums,  # clusters/groups
                              fmpc_units=False, cmpc_units=True,
                              fsats_only=False, csats_only=False,
                              bound=False, no_BCGs=False, projected=False,
                              M200lims=None, M500lims=None,
                              norm_by_sat_mass=False,
                              norm_by_total_mass=False,
                              outdir="/data/curie4/nh444/project1/stars/",
                              outsuffix="",
                              aperture=False, h_scale=0.6774):
    """Compare SMFs in clusters versus the field.

    Plot galaxy stellar mass distribution (number per log mass bin) in clusters
    versus the field, normalised either to the total number of galaxies above a
    certain stellar mass limit or the total DM+baryon mass
    Arguments:
        bound: (bool) Consider only bound cluster galaxies, otherwise all.
        no_BCGs (bool): Consider all galaxies except the cluster BCG.
        csats_only: Count only satellite galaxies in clusters, otherwise
                    satellites and BCGs.
        fsats_only: Count only satellite galaxies in the field, not centrals.
                    Otherwise all galaxies are counted.
        norm_by_sat_mass: Normalise the SMFs by the total stellar mass of
                          satellites.
        norm_by_total_mass: Normalise the SMFs by the total (DM+Baryon) mass.
                      Otherwise they are normalised to have the same number
                      of galaxies integrated over the SMF.
    """
    import matplotlib.pyplot as plt
    import numpy as np
    import snap

    # Initialise figure
    fig, ax = plt.subplots(figsize=(7, 7))
    ax.set_yscale('log')
    ax.set_xlabel(r"log$_{10}$(M$_{\star} / M_{\odot}$)")
    if norm_by_sat_mass:
        ax.set_ylabel(r"dn/dlog$_{10}$(M$_{\star}$) /"
                      r"(M$_{\mathrm{tot, sats}}$/$10^{10} M_{\odot}$)")
    elif norm_by_total_mass:
        ax.set_ylabel(r"dn/dlog$_{10}$(M$_{\star}$) /"
                      r"(M$_{\mathrm{tot}}$/$10^{10} M_{\odot}$)")
    else:
        ax.set_ylabel(r"dn/dlog$_{10}$(M$_{\star}$) (scaled)")
    if fsats_only and csats_only:
        ax.set_title("Satellites only")
    elif bound:
        ax.set_title("Satellites + Centrals")
    elif no_BCGs:
        if fsats_only:
            ax.set_title("No centrals")
        else:
            ax.set_title("No BCGs")
    elif not fsats_only and not csats_only:
        ax.set_title("All galaxies")

    calc_30kpc = aperture
    count_M = 1e8*h_scale

    myRlims = [[0.0, 1.0], [1.0, 2.0], [2.0, 5.0]]
    myRlims = [[0.0, 0.5], [0.5, 1.0], [1.0, 2.0]]
    col = ['#B94400', '#FF9819', '#FFE666']
    for i, Rlims in enumerate(myRlims):
        dat = get_SMF_cluster(cbasedir, csimdirs, csnapnums, count_M=count_M,
                              M200lims=M200lims, M500lims=M500lims,
                              bound=bound, sats_only=csats_only,
                              no_BCGs=no_BCGs,
                              calc_total_mass=norm_by_total_mass,
                              Rlims=Rlims, RlimType="R500",
                              projected=projected,
                              mpc_units=cmpc_units, calc_30kpc=calc_30kpc,
                              h_scale=h_scale, verbose=True)
        norm = 1.0
        if norm_by_sat_mass:
            if aperture:
                raise NotImplementedError("Need dat.totmstar_30kpc.")
            norm = 1.0 / (dat.totmstar / 1e10)
        elif norm_by_total_mass:
            # normalise SMF by the total mass in the volume.
            assert dat.totmass > 0.0, "dat.totmass is zero!"
            norm = 1.0 / (dat.totmass / 1e10)
        elif i == 0:
            norm = 1.0
            norm_count = float(dat.count)
        elif dat.count > 0:
            print "norm_count =", norm_count
            print "dat.count =", dat.count
            norm = norm_count / float(dat.count)
        if aperture:
            n_all = dat.n_in30kpc
        else:
            n_all = dat.n_total
        n = np.sum(n_all, axis=0)  # No. of galaxies per bin in all clusters.
        smf = np.ma.masked_equal(n, 0.0) / dat.dlogm * norm
        idx = np.min(np.where(n < 10)[0])
        ax.plot(dat.logm_binsp[:idx], smf[:idx], c=col[i], lw=3, ls='solid',
                dash_capstyle='round', label="{:.1f} - {:.1f} ({:d})".format(
                        Rlims[0], Rlims[1], dat.count))
        ax.plot(dat.logm_binsp[idx-1:], smf[idx-1:], c=col[i], lw=3,
                ls='dashed', dash_capstyle='round')

    logm_min = 8.0  # Msun
    logm_max = 12.0
    dlogm = 0.2
    nbins = int(np.round((logm_max - logm_min)/dlogm + 1))
    logm_bins = np.arange(nbins, dtype=np.float32)*dlogm + logm_min  # Msun (no h) but temporarily converted to Msun/h when calculating SMF below
    logm_binsp = np.arange(nbins-1, dtype=np.float32)*dlogm + logm_min + 0.5*dlogm ## Msun (no h)

    for simidx, simdir in enumerate(fsimdirs):
        sim = snap.snapshot(fbasedir+simdir, fsnapnum, extra_sub_props=True,
                            mpc_units=fmpc_units)
        cur_logm_bins = logm_bins + np.log10(h_scale)  # bins in units of Msun/h

        mask = np.ones(sim.cat.nsubs, dtype=np.bool)
        if fsats_only:
            mask[sim.cat.group_firstsub] = 0
        subind = np.nonzero(mask)[0]
        del mask
        subind = subind[np.where(sim.cat.sub_masstab[subind,4] > 0.0)[0]]
        mstar = sim.cat.sub_masstab[subind, 4] * 1e10
        #totmstar = np.sum(mstar) / h_scale
        count = np.count_nonzero(mstar >= count_M)
        #mstar_inrad = sim.cat.sub_massinradtab[subind,4] * 1e10
        #mstar_inVmaxrad = sim.cat.sub_massinmaxradtab[subind,4] * 1e10
        #mstar_inSPrad = sim.cat.sub_massinSPrad[subind] * 1e10
        if calc_30kpc:
            assert False, "not working yet"

        n_total = np.zeros(nbins-1, dtype=np.float32)
        #n_inrad = np.zeros(nbins-1, dtype=np.float32)
        #n_inVmaxrad = np.zeros(nbins-1, dtype=np.float32)
        #n_inSPrad = np.zeros(nbins-1, dtype=np.float32)
        n_in30kpc = np.zeros(nbins-1, dtype=np.float32)

        for i in range(0, nbins-2):
            n_total[i] = np.count_nonzero((np.log10(mstar) >= cur_logm_bins[i]) & (np.log10(mstar) < cur_logm_bins[i+1]))
            #n_inrad[i] = np.count_nonzero((np.log10(mstar_inrad) >= cur_logm_bins[i]) & (np.log10(mstar_inrad) < cur_logm_bins[i+1]))
            #n_inVmaxrad[i] = np.count_nonzero((np.log10(mstar_inVmaxrad) >= cur_logm_bins[i]) & (np.log10(mstar_inVmaxrad) < cur_logm_bins[i+1]))
            #n_inSPrad[i] = np.count_nonzero((np.log10(mstar_inSPrad) >= cur_logm_bins[i]) & (np.log10(mstar_inSPrad) < cur_logm_bins[i+1]))
            #if calc_30kpc:
                #n_in30kpc[i] = np.count_nonzero((np.log10(mstar_30kpc) >= cur_logm_bins[i]) & (np.log10(mstar_30kpc) < cur_logm_bins[i+1]))
        
        smf_total = 1.0/dlogm * n_total
        #smf_inrad = 1.0/dlogm * n_inrad
        #smf_inVmaxrad = 1.0/dlogm * n_inVmaxrad
        #smf_inSPrad = 1.0/dlogm * n_inSPrad
        smf_in30kpc = 1.0/dlogm * n_in30kpc

        if norm_by_sat_mass:
            # Normalise SMF by total mass of all satellites in the mass range.
            ind = np.where((np.log10(mstar / h_scale) > logm_min) &
                           (np.log10(mstar / h_scale) < logm_max))[0]
            norm = 1.0 / (np.sum(mstar[ind]) / h_scale / 1e10)
        elif norm_by_total_mass:
            # normalise SMF by the total mass in the volume
            print "Calculating total mass in field..."
            sim.load_mass()
            totmass = np.sum(sim.mass) * 1e10 / h_scale
            del sim.mass
            norm = 1.0 / (totmass / 1e10)
        else:
            norm = norm_count / float(count)
        if aperture:
            smf = np.ma.masked_equal(smf_in30kpc, 0.0) * norm
            n = n_in30kpc
        else:
            smf = np.ma.masked_equal(smf_total, 0.0) * norm
            n = n_total
        
        col = ['#6395EC', "#c7673e","#78a44f","#6b87c8","#934fce","#e6ab02","#be558f","#47af8d"]
        lw = 3
        ls='solid'
        mstarpart = 4.9e6 ## median star particle mass in Msun/h
        lowidx = np.max(np.where(logm_binsp <= np.log10(mstarpart / h_scale * 100.0))[0]) ## galaxies of stellar mass less than 100 star particles
        ax.plot(logm_binsp[:lowidx], smf[:lowidx], c=col[simidx], lw=lw, dashes=(0.5,6), dash_capstyle='round')
        idx = np.min(np.where((n < 10) & (logm_binsp > 8.0))[0])## if have very low mass bins they can have <10 objects so idx=0 and nothing is plotted
        ax.plot(logm_binsp[lowidx-1:idx], smf[lowidx-1:idx], c=col[simidx], label="Field ({:d})".format(count), lw=lw, ls=ls, dash_capstyle='round')
        ax.plot(logm_binsp[idx-1:], smf[idx-1:], c=col[simidx], lw=lw, ls='dashed', dash_capstyle='round')
        print logm_binsp, smf

    ax.legend(loc='best', title="$r/r_{500} = $")
    outfile = outdir+"SMF_field_cluster_comparison"+outsuffix+".pdf"
    fig.savefig(outfile)
    print "Saved to", outfile


def compare_cum_mass_field_cluster(bound=False, sats_only=False,
                                   outdir="/data/curie4/nh444/project1/stars/", outsuffix="",
                                   aperture=False, h_scale=0.6774):
    """
    Plot cumulative stellar mass distribution (cumulative mass per log mass bin) in clusters
    versus the field
    ## If bound, consider only bound cluster galaxies, otherwise all galaxies
    ## If sats_only, count only satellite galaxies, otherwise satellites and centrals
    ## If norm_by_mass, normalise the SMFs by the total (DM+Baryon) mass
    """
    import matplotlib.pyplot as plt
    import numpy as np
    
    ## Initialise figure
    fig, ax = plt.subplots(figsize=(7,7))
    ax.set_yscale('log')
    ax.set_xlabel("log$_{10}$(M$_{\star} / M_{\odot}$)")
    ax.set_ylabel("M$_{\star}$")
    if sats_only:
        ax.set_title("Satellites only")
    elif bound:
        ax.set_title("Satellites + Centrals")
    else:
        ax.set_title("All galaxies")
    
    calc_30kpc = aperture
    count_M = 1e8*h_scale
    
    cbasedir = "/data/curie4/nh444/project1/zoom_ins/MDRInt/"
    csimdirs = [#"c96_MDRInt", "c128_MDRInt", "c192_MDRInt",
               "c256_MDRInt"]
    csnapnums = [25]*len(csimdirs)
    cmpc_units = True
    
    myRlims = [[0.0, 1.0], [1.0, 5.0], [5.0, 10.0]]
    col = ['#B94400', '#FF9819', '#FFE666']
    for i, Rlims in enumerate(myRlims):
        dat = get_SMF_cluster(cbasedir, csimdirs, csnapnums, count_M=count_M,
                              bound=bound, sats_only=sats_only,
                              Rlims=Rlims, RlimType="R500", projected=False,
                              mpc_units=cmpc_units, calc_30kpc=calc_30kpc,
                              h_scale=h_scale, verbose=True)
        plt.plot(dat.logm_binsp, np.cumsum(dat.m_total), lw=3, ls='solid', c=col[i], label="{:.1f} - {:.1f} ({:d}) (c256)".format(Rlims[0], Rlims[1], dat.count))
        
    fbasedir = "/data/curie4/nh444/project1/boxes/"
    fsimdirs = ["L40_512_MDRIntRadioEff/"]
    fsnapnums = [25]
    fmpc_units = False
    for i, Rlims in enumerate(myRlims):
        dat = get_SMF_cluster(fbasedir, fsimdirs, fsnapnums, count_M=count_M,
                              bound=bound, sats_only=sats_only,
                              Rlims=Rlims, RlimType="R500", projected=False,
                              mpc_units=fmpc_units, calc_30kpc=calc_30kpc,
                              h_scale=h_scale, verbose=True)
        plt.plot(dat.logm_binsp, np.cumsum(dat.m_total), lw=3, ls='--', c=col[i], label="{:.1f} - {:.1f} ({:d}) (Box grp0)".format(Rlims[0], Rlims[1], dat.count))

    ax.legend(loc='best', title="$r/r_{500} = $", fontsize='x-small')
    outfile = outdir+"cum_mass_field_cluster_comparison"+outsuffix+".pdf"
    fig.savefig(outfile)
    print "Saved to", outfile


def plot_SMF_normed_by_halo_mass(bound=False, sats_only=True,
                                  outdir="/data/curie4/nh444/project1/stars/", outsuffix="",
                                  aperture=False, h_scale=0.6774):
    """
    Compare to the SMF normalised by total halo mass given in Kravtsov+18
    ## If bound, consider only bound cluster galaxies, otherwise all galaxies
    ## If sats_only, count only satellite galaxies, otherwise satellites and centrals
    """
    import matplotlib.pyplot as plt
    import numpy as np
    
    ## Initialise figure
    fig, ax = plt.subplots(figsize=(7,7))
    ax.set_yscale('log')
    ax.set_xlabel("log$_{10}$(M$_{\star} / M_{\odot}$)")
    ax.set_ylabel("dn/dlog$_{10}$(M$_{\star}$) (scaled)")
    if sats_only:
        ax.set_title("Satellites only")
    elif bound:
        ax.set_title("Satellites + Centrals")
    else:
        ax.set_title("All galaxies")
        
    schechter = lambda M, phi, alpha, Ms: phi * (M / Ms)**alpha / np.exp(M/Ms)
    mbins = np.logspace(8, 12, num=100)
    plt.errorbar(np.log10(mbins), schechter(mbins, 6.15e-25, -1.09, 1.23e11), lw=2, label="Kravtsov+18")
    
    count_M = 1e8*h_scale
       
    basedir = "/data/curie4/nh444/project1/zoom_ins/MDRInt/"
    simdirs = ["c320_MDRInt",
               "c384_MDRInt",
               "c448_MDRInt",
               "c400_MDRInt",
               "c410_MDRInt",
               "c512_MDRInt",
               ]
    snapnums = [25,
                25,
                25,
                99,
                99,
                25,
                ]
    mpc_units = True
    dat = get_SMF_cluster(basedir, simdirs, snapnums, count_M=count_M,
                          bound=bound, sats_only=sats_only,
                          Rlims=[0.0, 0.7], RlimType="R500", projected=False,
                          norm_by_M500=True,
                          mpc_units=mpc_units, calc_30kpc=aperture,
                          h_scale=h_scale, verbose=True)
    if aperture:
        smf = dat.smf_in30kpc
        n = dat.n_in30kpc
    else:
        #smf = dat.smf_total
        n = dat.n_total ## number / M500
        smf = n / np.diff(10**dat.logm_bins)
    print "smf =", smf
    print "dat.logm_binsp =", dat.logm_binsp
    ax.plot(dat.logm_binsp, smf, lw=3, ls='solid', dash_capstyle='round')
    #idx = np.min(np.where(n < 10)[0])
    #p = ax.plot(dat.logm_binsp[:idx], smf[:idx], lw=3, ls='solid', dash_capstyle='round')
    #ax.plot(dat.logm_binsp[idx-1:], smf[idx-1:], c=p[0].get_color(), lw=3, ls='dashed', dash_capstyle='round')
    

    ax.legend(loc='best', title="$r/r_{500} = $")
    outfile = outdir+"SMF_cluster_norm_by_total_mass"+outsuffix+".pdf"
    fig.savefig(outfile)
    print "Saved to", outfile
    
def compare_SMF_cluster(sats_only=False, outdir="/data/curie4/nh444/project1/stars/", h_scale=0.6774):
    """
    Compares cluster SMFs for different clusters
    """
    import matplotlib.pyplot as plt
    
    outfile = outdir+"SMF_cluster_comparison.pdf"
    
    ## Initialise figure
    fig, ax = plt.subplots(figsize=(7,7))
    ax.set_yscale('log')
    ax.set_xlabel("log$_{10}$(M$_{\star} / M_{\odot}$)")
    ax.set_ylabel("dn/dlog$_{10}$(M$_{\star}$)/cluster")
    ax.set_ylim(1, 1e3)
    if sats_only:
        ax.set_title("Satellites only")
    else:
        ax.set_title("All galaxies")
    
    ## Plot SMFs
    mpc_units = True
    basedir = "/data/curie4/nh444/project1/zoom_ins/MDRInt/"
    simdirs = ["c384_MDRInt"]#, "c448_MDRInt", "c400_MDRInt", "c410_MDRInt"]
    snapnums = [25]#, 25, 99, 99]
    plot_SMF_cluster_ax(ax, basedir, simdirs, snapnums, label="c384",
                        Rlims=[0.0, 1.0], RlimType="R500",
                        sats_only=sats_only, compare_apertures=False,
                        mpc_units=mpc_units, h_scale=h_scale)

    simdirs = ["c410_MDRInt"]
    snapnums = [99]
    plot_SMF_cluster_ax(ax, basedir, simdirs, snapnums, label="c410",
                        Rlims=[0.0, 1.0], RlimType="R500",
                        sats_only=sats_only, compare_apertures=False,
                        mpc_units=mpc_units, h_scale=h_scale)
    

    ## Save figure
    ax.legend(loc='best')
    fig.savefig(outfile)
    print "Plot saved to", outfile
    
def compare_cum_mass_cluster(sats_only=False, outdir="/data/curie4/nh444/project1/stars/", h_scale=0.6774):
    """
    Compares the cumulative mass function (total stellar mass per bin) for different clusters
    """
    import matplotlib.pyplot as plt
    import numpy as np
    
    outfile = outdir+"SMF_cluster_comparison_cumulative.pdf"
    
    ## Initialise figure
    fig, ax = plt.subplots(figsize=(7,7))
    ax.set_yscale('log')
    ax.set_xlabel("log$_{10}$(M$_{\star} / M_{\odot}$)")
    ax.set_ylabel("M$_{\star}$")
    if sats_only:
        ax.set_title("Satellites only")
    else:
        ax.set_title("All galaxies")
    
    ## Plot
    mpc_units = True
    basedir = "/data/curie4/nh444/project1/zoom_ins/MDRInt/"
#==============================================================================
#     simdirs = ["c384_MDRInt"]#, "c448_MDRInt", "c400_MDRInt", "c410_MDRInt"]
#     snapnums = [25]#, 25, 99, 99]
#     dat = get_SMF_cluster(basedir, simdirs, snapnums, mpc_units=mpc_units, sats_only=sats_only,
#                           Rlims=[0.0, 1.0], RlimType="R500", projected=False, h_scale=h_scale)
#     plt.plot(dat.logm_binsp, np.cumsum(dat.m_total), lw=3, ls='solid', label="c384")
#     
#     simdirs = ["c410_MDRInt"]
#     snapnums = [99]
#     dat = get_SMF_cluster(basedir, simdirs, snapnums, mpc_units=mpc_units, sats_only=sats_only,
#                           Rlims=[0.0, 1.0], RlimType="R500", projected=False, h_scale=h_scale)
#     plt.plot(dat.logm_binsp, np.cumsum(dat.m_total), lw=3, ls='solid', label="c410")
#==============================================================================
    simdirs = ["c128_MDRInt"]#, "c448_MDRInt", "c400_MDRInt", "c410_MDRInt"]
    snapnums = [25]#, 25, 99, 99]
    dat = get_SMF_cluster(basedir, simdirs, snapnums, mpc_units=mpc_units, sats_only=sats_only,
                          Rlims=[0.0, 1.0], RlimType="R500", projected=False, h_scale=h_scale)
    plt.plot(dat.logm_binsp, np.cumsum(dat.m_total), lw=3, ls='solid', label="c128")
    #print dat.n_total[(dat.n_total > 0.0)]
    
    simdirs = ["c96_MDRInt"]
    snapnums = [25]
    dat = get_SMF_cluster(basedir, simdirs, snapnums, mpc_units=mpc_units, sats_only=sats_only,
                          Rlims=[0.0, 1.0], RlimType="R500", projected=False, h_scale=h_scale)
    plt.plot(dat.logm_binsp, np.cumsum(dat.m_total), lw=3, ls='solid', label="c96")
    
    ## Save figure
    ax.legend(loc='best')
    fig.savefig(outfile)
    print "Plot saved to", outfile

def compare_cum_mass_obs(sats_only=False,
                         outdir="/data/curie4/nh444/project1/stars/",
                         h_scale=0.6774):
    """
    Compares the cumulative mass function to that estimated from observed cluster SMF
    """
    import matplotlib.pyplot as plt
    import numpy as np
    
    outfile = outdir+"SMF_cluster_comparison_cumulative.pdf"
    
    ## Initialise figure
    fig, ax = plt.subplots(figsize=(7,7))
    ax.set_yscale('log')
    ax.set_xlabel("log$_{10}$(M$_{\star} / M_{\odot}$)")
    ax.set_ylabel("M$_{\star}$")
    
    ## Plot
#    mpc_units = True
#    basedir = "/data/curie4/nh444/project1/zoom_ins/MDRInt/"
#    simdirs = ["c384_MDRInt"]#, "c448_MDRInt", "c400_MDRInt", "c410_MDRInt"]
#    snapnums = [25]#, 25, 99, 99]
#    dat = get_SMF_cluster(basedir, simdirs, snapnums, mpc_units=mpc_units, sats_only=sats_only,
#                          Rlims=[0.0, 1.0], RlimType="R500", projected=False, h_scale=h_scale)
#    p = plt.plot(dat.logm_binsp, np.cumsum(dat.m_total), lw=3, ls='solid', label="c384")
#    plt.plot(dat.logm_binsp, np.cumsum(dat.n_total*10**dat.logm_binsp), marker='o', mfc=p[0].get_color(), mec='none', ls='none')
#    
#    simdirs = ["c410_MDRInt"]
#    snapnums = [99]
#    dat = get_SMF_cluster(basedir, simdirs, snapnums, mpc_units=mpc_units, sats_only=sats_only,
#                          Rlims=[0.0, 1.0], RlimType="R500", projected=False, h_scale=h_scale)
#    p = plt.plot(dat.logm_binsp, np.cumsum(dat.m_total), lw=3, ls='solid', label="c410")
#    plt.plot(dat.logm_binsp, np.cumsum(dat.n_total*10**dat.logm_binsp), marker='o', mfc=p[0].get_color(), mec='none', ls='none')
    
    ## Yang+2009 comparison:
    mpc_units = True
    basedir = "/data/curie4/nh444/project1/zoom_ins/MDRInt/"
    simdirs = ["c384_MDRInt",
               "c448_MDRInt",
               "c400_MDRInt",
               "c410_MDRInt",
               ]
    snapnums = [25,
                25,
                99,
                99,
                ]
    dat = get_SMF_cluster(basedir, simdirs, snapnums, mpc_units=mpc_units, sats_only=sats_only, Rlims=None, projected=False, h_scale=h_scale)
    p = plt.plot(dat.logm_binsp, np.cumsum(dat.m_total)/len(simdirs), lw=3, ls='solid', label="Fable")
    plt.plot(dat.logm_binsp, np.cumsum(dat.n_total/len(simdirs)*10**dat.logm_binsp), marker='o', mfc=p[0].get_color(), mec='none', ls='none')
    
    Yang = np.genfromtxt("/data/vault/nh444/ObsData/SMF/clusters/Yang2009_from_CEAGLE.csv", delimiter=",")
    logm = Yang[:,0]+np.log10(0.6777**2 / h_scale**2)
    plt.errorbar(logm, np.cumsum(Yang[:,1]*0.1*10**logm), ls='none', marker='D', c=p[0].get_color(), mec=p[0].get_color(), mfc='none', label="Yang+ 2009 $14.4 \leq$ M$_{200c}$ $< 14.7$")

    ## Save figure
    ax.legend(loc='best')
    fig.savefig(outfile)
    print "Plot saved to", outfile
    
def plot_SMF_cluster_vs_vanderBurg(outdir="/data/curie4/nh444/project1/stars/",
                                   h_scale=0.6774):
    """
    Compare the cluster satellite SMF to van der Burg et al. 2018 for
    clusters at z=0.5-0.7 in different radial regions
    """
    import matplotlib.pyplot as plt
    import numpy as np

    fig, ax = plt.subplots(figsize=(7, 7))
    ax.set_yscale('log')
    ax.set_xlabel(r"log$_{10}$(M$_{\star} / M_{\odot}$)")
    ax.set_ylabel(r"dn/dlog$_{10}$(M$_{\star}$)/cluster")
    ax.set_ylim(0.1, 2e3)
    col = ['#24439b', '#ED7600', '0.4']

    mpc_units = True
    basedir = "/data/curie4/nh444/project1/zoom_ins/MDRInt/"
    simdirs = [#"c96_MDRInt", "c128_MDRInt", "c160_MDRInt",
               #"c192_MDRInt", "c224_MDRInt", "c256_MDRInt",
               "c288_MDRInt", "c320_MDRInt",
               "c352_MDRInt", "c384_MDRInt",
               "c448_MDRInt", "c480_MDRInt", "c512_MDRInt",
               #"c112_MDRInt", "c144_MDRInt", "c176_MDRInt", "c208_MDRInt",
               #"c240_MDRInt", "c272_MDRInt", "c304_MDRInt", "c336_MDRInt",
               "c368_MDRInt",
               "c400_MDRInt", "c410_MDRInt",
               "c432_MDRInt", "c464_MDRInt",
               "c496_MDRInt_SUBF",
               ]
    snapnums = [25]*(len(simdirs)-3)+[99]*3
    snapnums = [22]*(len(simdirs)-3)+[87]*3

    projected = True
    scatter = False
    M500lims = [3e14, np.inf]

    # R < 2 R500
    Rlims = [0.0, 2.0]
    RlimType = "R500"
    plot_SMF_cluster_ax(ax, basedir, simdirs, snapnums, col=col[0], sats_only=True,
                        M500lims=M500lims, scatter=scatter,
                        Rlims=Rlims, RlimType=RlimType, projected=projected,
                        compare_apertures=False, mpc_units=mpc_units, h_scale=h_scale)
    
    dat = np.genfromtxt("/data/vault/nh444/ObsData/SMF/clusters/vanderBurg_2018_all_gal.csv", delimiter=",")
    ax.errorbar(dat[:,0]+np.log10(0.7**2 / h_scale**2), dat[:,1], yerr=[dat[:,4], dat[:,2]], lw=2, ls='none', c=col[0], marker='D', mec=col[0], mfc='none', label="$r < 2 r_{500}$")

    # < 0.5 R500
    Rlims = [0.0, 0.5]
    RlimType = "R500"
    plot_SMF_cluster_ax(ax, basedir, simdirs, snapnums, col=col[1], sats_only=True,
                        M500lims=M500lims, scatter=scatter,
                        Rlims=Rlims, RlimType=RlimType, projected=projected,
                        compare_apertures=False, mpc_units=mpc_units, h_scale=h_scale)
    
    dat = np.genfromtxt("/data/vault/nh444/ObsData/SMF/clusters/vanderBurg_2018_all_gal.csv", delimiter=",")
    ax.errorbar(dat[:,0]+np.log10(0.7**2 / h_scale**2), dat[:,6], yerr=[dat[:,9], dat[:,7]], lw=2, ls='none', c=col[1], marker='D', mec=col[1], mfc='none', label="$r < 0.5 r_{500}$")

    # 1.5-2 R500
    Rlims = [1.5, 2.0]
    RlimType="R500"
    plot_SMF_cluster_ax(ax, basedir, simdirs, snapnums, col=col[2], sats_only=True,
                        M500lims=M500lims, scatter=scatter,
                        Rlims=Rlims, RlimType=RlimType, projected=projected,
                        compare_apertures=False, mpc_units=mpc_units, h_scale=h_scale)
    
    dat = np.genfromtxt("/data/vault/nh444/ObsData/SMF/clusters/vanderBurg_2018_all_gal.csv", delimiter=",")
    ax.errorbar(dat[:, 0] + np.log10(0.7**2 / h_scale**2), dat[:, 21],
                yerr=[dat[:, 24], dat[:, 22]], lw=2, ls='none', c=col[2],
                marker='D', mec=col[2], mfc='none',
                label="$1.5 r_{500} < r < 2 r_{500}$")

    # 1-1.5 R500
#    Rlims = [1.0, 1.5]
#    RlimType="R500"
#    plot_SMF_cluster_ax(ax, basedir, simdirs, snapnums, col=col[2], sats_only=True,
#                        Rlims=Rlims, RlimType=RlimType, projected=projected,
#                        compare_apertures=False, mpc_units=mpc_units, h_scale=h_scale)
#    
#    dat = np.genfromtxt("/data/vault/nh444/ObsData/SMF/clusters/vanderBurg_2018_all_gal.csv", delimiter=",")
#    ax.errorbar(dat[:,0]+np.log10(0.7**2 / h_scale**2), dat[:,16], yerr=[dat[:,19], dat[:,17]], lw=2, ls='none', c=col[2], marker='D', mec=col[2], mfc='none', label="$1.0 r_{500} < r < 1.5 r_{500}$")

    # Save figure
    outfile = outdir+"SMF_cluster_vs_vanderBurg.pdf"
    ax.legend(loc='best', borderaxespad=1, frameon=True,
              title="van der Burg et al. (2018)")
    fig.savefig(outfile)
    print "Plot saved to", outfile


def plot_SMF_cluster_vs_obs(outdir="/data/curie4/nh444/project1/stars/",
                            h_scale=0.6774):
    """
    Compare the cluster satellite SMF to various observational data sets
    on the same figure
    """
    import matplotlib.pyplot as plt

    outfile = outdir+"SMF_cluster_vs_obs.pdf"

    # Initialise figure
    fig, ax = plt.subplots(figsize=(7, 7))
    ax.set_yscale('log')
    ax.set_xlabel(r"log$_{10}$(M$_{\star} / M_{\odot}$)")
    ax.set_ylabel(r"dn/dlog$_{10}$(M$_{\star}$)/cluster")
    ax.set_ylim(0.1, 1e3)

    # Plot SMFs
    simdirs = ["c96_MDRInt", "c128_MDRInt",
               "c160_MDRInt", "c192_MDRInt", "c224_MDRInt", "c256_MDRInt",
               "c288_MDRInt", "c320_MDRInt",
               "c352_MDRInt", "c384_MDRInt",
               "c448_MDRInt", "c480_MDRInt", "c512_MDRInt",
               "c112_MDRInt", "c144_MDRInt",
               "c176_MDRInt", "c208_MDRInt",
               "c240_MDRInt", "c272_MDRInt", "c304_MDRInt", "c336_MDRInt",
               "c368_MDRInt", "c400_MDRInt", "c410_MDRInt", "c432_MDRInt",
               "c464_MDRInt", "c496_MDRInt_SUBF",
               ]
    snapnums = [25]*13 + [99]*14
    # Yang+2009 comparison:
    mpc_units = True
    basedir = "/data/curie4/nh444/project1/zoom_ins/MDRInt/"
# =============================================================================
# #    simdirs = ["c352_MDRInt", "c384_MDRInt", #"c448_MDRInt",
# #               "c336_MDRInt", "c368_MDRInt",
# #               "c400_MDRInt",
# #               #"c410_MDRInt", "c432_MDRInt", "c464_MDRInt",
# #               ]
# #    snapnums = [25]*2 + [99]*3
#     plot_SMF_cluster_ax(ax, basedir, simdirs, snapnums,
#                         scatter=True, col='royalblue',
#                         Yang09=True, sats_only=True, compare_apertures=False,
#                         M200lims=[10**14.4 / h_scale, 10**14.7 / h_scale],
#                         mpc_units=mpc_units, h_scale=h_scale,
#                         label="$14.4 \leq$ log$_{10}$M$_{200} [M_{\odot}/h]$ $< 14.7$")
#     ## WINGS comparison: M200 > 10^14.5 Msun according to Bahe17
# #    simdirs = ["c352_MDRInt", "c384_MDRInt", "c448_MDRInt",
# #               "c480_MDRInt", "c512_MDRInt",
# #               "c336_MDRInt", "c368_MDRInt", "c400_MDRInt", "c410_MDRInt",
# #               "c432_MDRInt", "c464_MDRInt", "c496_MDRInt_SUBF"]
# #    snapnums = [25]*5 + [99]*7
# #    plot_SMF_cluster_ax(ax, basedir, simdirs, snapnums, col='green',
# #                        WINGS=True, sats_only=True, compare_apertures=False,
# #                        mpc_units=mpc_units, h_scale=h_scale,
# #                        label="log$_{10}$M$_{200} [M_{\odot}]$ $> 14.5$")
#     ## Wang and White 2012 comparison: M200 ~ 10^13.7 from their fig. 2
# #    simdirs = ["c96_MDRInt", "c128_MDRInt",
# #               "c160_MDRInt", "c192_MDRInt", "c224_MDRInt", "c256_MDRInt",
# #               #"c288_MDRInt", "c320_MDRInt",
# #               #"c352_MDRInt", "c384_MDRInt",
# #               #"c448_MDRInt", "c480_MDRInt", "c512_MDRInt",
# #               "c112_MDRInt", "c144_MDRInt",
# #               "c176_MDRInt", "c208_MDRInt",
# #               "c240_MDRInt", "c272_MDRInt", #"c304_MDRInt", #"c336_MDRInt",
# #               #"c368_MDRInt", "c400_MDRInt", "c410_MDRInt", "c432_MDRInt",
# #               #"c464_MDRInt", "c496_MDRInt_SUBF",
# #          ]
# #    snapnums = [25]*6 + [99]*6
#     plot_SMF_cluster_ax(ax, basedir, simdirs, snapnums,
#                         scatter=False, col='k', Wang=True,
#                         sats_only=True, compare_apertures=False,
#                         M200lims=[10**13.4, 10**14.3],
#                         mpc_units=mpc_units, h_scale=h_scale,
#                         label="$13.3 <$ log$_{10}$M$_{200} [M_{\odot}]$ $< 14.1$")
# 
# #    plot_SMF_cluster_ax(ax, basedir, simdirs, snapnums, col='orange', XXL=True,
# #                        sats_only=True, compare_apertures=False,
# #                        mpc_units=mpc_units, outdir=outdir, h_scale=h_scale,
# #                        label="XXL")
# 
# =============================================================================
    plot_SMF_cluster_ax(ax, basedir, simdirs, snapnums,
                        scatter=False, col='g',
                        Kravtsov=True, sats_only=True, compare_apertures=False,
                        M200lims=[8e13, 1e15],
                        mpc_units=mpc_units, h_scale=h_scale,
                        label=r"$13.9 \leq$ log$_{10}$M$_{500} [M_{\odot}]$ $< 15.0$")

    # Save figure
    ax.legend(loc='best', fontsize='x-small')
    fig.savefig(outfile)
    print "Plot saved to", outfile


def plot_SMF_cluster_ax(ax, basedir, simdirs, snapnums, mpc_units=True,
                        Yang09=False, WINGS=False, Wang=False, XXL=False,
                        Kravtsov=False,
                        col=None, scatter=False, sats_only=True,
                        Rlims=None, RlimType="R200", projected=False,
                        M200lims=None, M500lims=None,
                        compare_apertures=False, aperture=False, label=None,
                        verbose=False, h_scale=0.6774):
    """
    Given a matplotlib axes 'ax', plot onto it the average cluster SMF
    corresponding to the simdirs and (optionally) compare with z=0 data
    (Yang+09 if 'Yang09', WINGS from Vulcani+11 if 'WINGS',
    Wang and White 2012 if 'Wang' and Guglielmo+ 18 if 'XXL').
    """
    import numpy as np
    import matplotlib.pyplot as plt

    if Rlims is None:
        Rlims = [None, None]

    if compare_apertures or aperture:
        calc_30kpc = True
    else:
        calc_30kpc = False

    # If a comparison to data requires rescaling the data to match the number
    # of satellites above a given mass in the simulations, specify the limit
    # count_M in Msun/h and the code will calculate the number above this mass.
    if Yang09:
        count_M = 0  # not needed
        Rlims = None  # no aperture
        projected = True
    if WINGS:
        count_M = 10**9.8 * 0.7  # Msun/h
        Rlims = [0.0, 0.6]
        RlimType = "R200"
        projected = True
    elif Wang:
        count_M = 0  # 10**9.4 * 0.73 ## Msun/h - needs to be implemented below
        Rlims = [0.0, 300.0]
        RlimType = "pkpc"
        projected = True
    elif XXL:
        count_M = 0
        Rlims = [0.0, 3.0]
        RlimType = "R200"
        projected = True
    elif Kravtsov:
        count_M = 0
        Rlims = [0.05, 0.7]
        RlimType = "R500"
        projected = True
    else:
        count_M = 0  # not needed

    dat = get_SMF_cluster(basedir, simdirs, snapnums, mpc_units=mpc_units,
                          sats_only=sats_only, calc_30kpc=calc_30kpc,
                          count_M=count_M, Rlims=Rlims, RlimType=RlimType,
                          M200lims=M200lims, M500lims=M500lims,
                          projected=projected, h_scale=h_scale)

    # salpshift=-0.25  # log -0.18 was used in Puchwein & Springel 2013
    kroupashift = -0.05

    if Yang09:
        #CEAGLE = np.genfromtxt("/data/vault/nh444/ObsData/SMF/clusters/CEAGLE_Yang09_clusterSMF.csv", delimiter=",")
        #plt.errorbar(CEAGLE[:,0]+np.log10(0.6777 / h_scale), CEAGLE[:,1], lw=3, c='lightblue', label="CEAGLE")
        Yang = np.genfromtxt("/data/vault/nh444/ObsData/SMF/clusters/Yang2009_from_CEAGLE.csv", delimiter=",")
        plt.errorbar(Yang[:,0]+kroupashift+np.log10(0.6777**2 / h_scale**2),
                     Yang[:,1], yerr=[Yang[:,4], Yang[:,5]],
                     lw=2, ls='none', c=col,
                     marker='o', mec=col, mfc='none',
                     label="Yang+ 2009") # $14.4 \leq$ log$_{10}$M$_{200} [M_{\odot}/h]$ $< 14.7$
        
#        ## modified Schechter function from Yang+09, where M is stellar mass in units Msun/h^2
#        modschechter = lambda M, phi, alpha, Ms: phi * (M / Ms)**(alpha + 1.0) / np.exp((M/Ms)**2)
#        mbins = np.logspace(8, 12, num=100)*h_scale**2
#        plt.errorbar(np.log10(mbins/h_scale**2)+kroupashift,
#                     modschechter(mbins, 24.91, -1.59, 10**(11.364-0.25)),
#                     ls='--', lw=2, c=col,
#                     label="Yang+ 2009 \n $14.4 \leq$ log$_{10}$M$_{200} [M_{\odot}/h]$ $< 15.0$")

        #plt.errorbar(np.log10(mbins/h_scale**2)+kroupashift, modschechter(mbins, 16.49, -1.4, 10**(11.277-0.25)), lw=2, label="$14.1 \leq$ M$_{200c}$ $< 14.4$")
        #plt.errorbar(np.log10(mbins/h_scale**2)+kroupashift, modschechter(mbins, 9.72, -1.34, 10**(11.209-0.25)), lw=2, label="$13.8 \leq$ M$_{200c}$ $< 14.1$")
        #plt.errorbar(np.log10(mbins/h_scale**2)+kroupashift, modschechter(mbins, 6.15, -1.22, 10**(11.122-0.25)), lw=2, label="$13.5 \leq$ M$_{200c}$ $< 13.8$")
        #plt.errorbar(np.log10(mbins/h_scale**2)+kroupashift, modschechter(mbins, 3.61, -1.16, 10**(11.026-0.25)), lw=2, label="$13.2 \leq$ M$_{200c}$ $< 13.5$")
        #from scipy.integrate import quad
        #Mstar, err = quad(modschechter, 1e8, 1e12, args=(24.91, -1.59, 10**(11.364-0.25)))
        
        ## THe following gives the wrong units (number per Mpc^3 per dex)
        #chiu = lambda M, phi, alpha, Ms: np.log(10)*phi * 10**((alpha+1)*(M-Ms)) * np.exp(-10**(M-Ms))
        #plt.errorbar(np.log10(mbins), chiu(mbins, 12.3, -0.32, 10**11.06), c='r')
    elif WINGS:
        print "Got count", dat.count
        #fac = 1.0/21 ## 
        fac = dat.count / 1894.42 / len(simdirs) ## rescale the WINGS SMF so that the number of galaxies above 10^9.8 Msun is the same as in the simulations
        WINGS = np.genfromtxt("/data/vault/nh444/ObsData/SMF/clusters/WINGS_Vulcani2011.csv", delimiter=",")
        plt.errorbar(WINGS[:,0]+kroupashift+np.log10(0.7**2 / h_scale**2),
                     WINGS[:,6]*fac, yerr=[WINGS[:,9]*fac, WINGS[:,10]*fac],
                     ls='none', c=col, marker='D', mec=col, mfc='none',
                     label="Vulcani+ 2011 log$_{10}$M$_{200}$ $> 14.5$ (scaled)")
        #schechter = lambda M, phi, alpha, Ms: phi * (M / Ms)**alpha / np.exp(M/Ms)
        #mbins = np.logspace(8, 12, num=100)
        #plt.errorbar(np.log10(mbins), fac*schechter(mbins, 1.552, -0.987, 10**11.667), lw=2, label="WINGS")
    elif Wang:
        WW = np.genfromtxt("/data/vault/nh444/ObsData/SMF/clusters/Wang_White_2012.csv", delimiter=",")
        plt.errorbar(WW[:,0]+np.log10(0.73**2 / h_scale**2), WW[:,1],
                     yerr=[WW[:,2], WW[:,3]], ls='none', c=col,
                     marker='D', mec=col, mfc='none',
                     label="Wang \& White 2012")  # (log$_{10}$M$_{200} [M_{\odot}] \sim 13.7$)
    elif XXL:
        # Guglielmo+ 18 calculate the total number of galaxies per 0.2 dex
        # mass bin divided by the bin width and normalise it by the total
        # integrated stellar mass. I will need to do the same if I want to
        # compare to their data.
        Gug = np.genfromtxt("/data/vault/nh444/ObsData/SMF/clusters/XXL_Guglielmo2018_z01-02.csv", delimiter=",")
        plt.errorbar(Gug[:, 0]+np.log10(0.693**2 / h_scale**2),
                     Gug[:, 1]*100.0, ls='none', c=col, marker='s',
                     mec=col, mfc='none', label="Guglielmo et al. 2018")
    elif Kravtsov:
        # Approximate Schechter fit parameters from email.
        schechter = lambda M, phi, alpha, Ms: phi * (M / Ms)**alpha / np.exp(M/Ms)
        mbins = np.logspace(9.7, 12, num=100)  # M*>~5e9 Msun
        plt.errorbar(np.log10(mbins),
                     3.0*schechter(mbins, 1.0, -1.1, 1.2e11), lw=2,
                     c='g', label="Kravtsov+ 18")

    if compare_apertures:
        if scatter:
            raise NotImplementedError("Plotting the scatter is not currently "
                                      "compatible with compare_apertures.")
        ns = (dat.n_total, dat.n_in30kpc, dat.n_inrad)#n_inSPrad
        #aperls = ['solid', 'solid', 'solid', 'dashed']#, 'dashed', 'dotted', 'dashdot']
        aperlabels = [" Total bound", " spherical 30 pkpc", " $2 r_{\star, 0.5}$", " Photometric radius"] #" (half-mass radius)", " (V$_{\mathrm{max}}$ radius)"
        from palettable.colorbrewer.qualitative import Dark2_8 as pallette
        apercol = pallette.hex_colors
        for nidx, n in enumerate(ns):
            smf, n = calc_average_SMF(n, dat.dlogm)
            idx = np.min(np.where(n[nidx] < 10)[0])
            plt.plot(dat.logm_binsp[:idx], smf[:idx], c=apercol[nidx],
                     label=aperlabels[nidx], lw=2.5, ls='solid')
            plt.plot(dat.logm_binsp[idx-1:], smf[idx-1:], c=apercol[nidx],
                     lw=2.5, ls='dashed', dash_capstyle='round')
    else:
        if aperture:
            n_all = dat.n_in30kpc
        else:
            n_all = dat.n_inrad
        # Plot the average SMF.
        smf, n_avg = calc_average_SMF(n_all, dat.dlogm)
        idx = np.min(np.where(n_avg < 10)[0])
        p = plt.plot(dat.logm_binsp[:idx], smf[:idx], c=col, lw=3, ls='solid',
                     label=label)
        plt.plot(dat.logm_binsp[idx-1:], smf[idx-1:], c=p[0].get_color(),
                 lw=3, ls='dashed', dash_capstyle='round')
        if scatter:
            # Plot individual SMFs.
            for i in range(np.shape(n_all)[0]):
                n = n_all[i, :]
                smf = np.ma.masked_equal(n, 0.0) / dat.dlogm
                plt.plot(dat.logm_binsp, smf, c=col, alpha=0.5,
                         lw=2, ls='solid')


def get_SMF_cluster(basedir, simdirs, snapnums, mpc_units=True,
                    dlogm=0.2, M200lims=None, M500lims=None,
                    h_scale=0.6774, verbose=True, **kwargs):
    """Returns the cluster SMFs and total galaxy count.

    snapnums: snapshot number for each directory in simdirs.
    dlogm: Logarithmic spacing between stellar mass bins in dex.
    M200lims: (list length 2) Lower and upper limits on M200 in Msun.
    M500lims: (list length 2) Lower and upper limits on M500 in Msun.

    Remaining keyword arguments are sent to calc_SMF_cluster().
    """
    import numpy as np
    import snap
    from get_uncontaminated_groups import get_uncontaminated_groups

    if M200lims is None:
        M200lims = [0.0, np.inf]
    if M500lims is None:
        M500lims = [0.0, np.inf]

    if len(snapnums) != len(simdirs):
        raise ValueError("Need exactly one snapshot number for each simdir.")

    # Get data structure (namedtuple) to store SMF data.
    dat_all = get_SMF_data_structure()

    dat_all.totmass = 0.0
    dat_all.totmstar = 0.0
    dat_all.totmstar_inrad = 0.0
    count = 0
    M200_all = []  # List of halo mass in Msun
    Nclus = 0
    for idx, simdir in enumerate(simdirs):
        # Load subfind catalogue with all subhalo properties.
        sim = snap.snapshot(basedir+simdir, snapnums[idx],
                            extra_sub_props=True,
                            mpc_units=mpc_units)
        # Get indices of groups in halo mass range.
        M200 = sim.cat.group_m_crit200 * 1e10 / h_scale
        M500 = sim.cat.group_m_crit500 * 1e10 / h_scale
        grpind = np.where((M200 > M200lims[0]) & (M200 < M200lims[1]) &
                          (M500 > M500lims[0]) & (M500 < M500lims[1]))[0]
        if len(grpind) == 0:  # skip
            continue
        # Remove groups that are contaminated by low-res particles.
        grpind = get_uncontaminated_groups(sim, grpind)
        for groupnum in grpind:
            dat = calc_SMF_cluster(sim, groupnum,
                                   h_scale=h_scale, verbose=verbose, **kwargs)
            if verbose:
                print simdir, "grp", groupnum, \
                       "log10(M200c) = {:.2f}".format(
                               np.log10(sim.cat.group_m_crit200[groupnum] *
                                        1e10 / h_scale))
            M200_all.append(sim.cat.group_m_crit200[groupnum] * 1e10 / h_scale)
            if Nclus == 0:
                n_total = dat.n_total
                n_inrad = dat.n_inrad
                n_inVmaxrad = dat.n_inVmaxrad
                n_inSPrad = dat.n_inSPrad
                n_in30kpc = dat.n_in30kpc
            else:
                n_total = np.vstack((n_total, dat.n_total))
                n_inrad = np.vstack((n_inrad, dat.n_inrad))
                n_inVmaxrad = np.vstack((n_inVmaxrad, dat.n_inVmaxrad))
                n_inSPrad = np.vstack((n_inSPrad, dat.n_inSPrad))
                n_in30kpc = np.vstack((n_in30kpc, dat.n_in30kpc))
            # Total mass of all particle types within given volume.
            dat_all.totmass += dat.totmass
            dat_all.totmstar += dat.totmstar
            dat_all.totmstar_inrad += dat.totmstar_inrad
            count += dat.count
            Nclus += 1

    if Nclus > 0:
        print "Average log(M200c [Msun/h]) = {:.2f}".format(
                np.mean(np.log10(M200_all) + np.log10(h_scale)))
        print "log(average M200c [Msun]) = {:.2f}".format(
                np.log10(np.mean(M200_all)))
        print
    else:
        print "No objects found in desired mass range."

    dat_all.logm_bins = dat.logm_bins
    dat_all.logm_binsp = dat.logm_binsp
    dat_all.dlogm = dat.dlogm
    dat_all.count = count
    dat_all.n_total = n_total
    dat_all.n_inrad = n_inrad
    dat_all.n_inVmaxrad = n_inVmaxrad
    dat_all.n_inSPrad = n_inSPrad
    dat_all.n_in30kpc = n_in30kpc

    return dat_all


def calc_average_SMF(n, dlogm):
    """Calculate the average SMF across clusters.

    Given a 2D array containing the number of galaxies in each stellar mass bin
    for multiple objects calculate the average SMF.
    n: 2-D array containing the number of galaxies in each bin.
    dlogm: Logarithmic spacing of the stellar mass bins in dex.
    """
    import numpy as np

    Nclus = np.shape(n)[0]
    ntot = np.sum(n, axis=0)
    smf = np.ma.masked_equal(ntot, 0.0) / dlogm / Nclus

    return smf, ntot


def calc_SMF_cluster(sim, grpnum, logm_bins=None, dlogm=0.2,
                     bound=True, no_BCGs=False, sats_only=True,
                     calc_total_mass=False, calc_30kpc=True,
                     count_M=None, norm_by_M500=False,
                     Rlims=None, RlimType="R200",
                     projected=False, zaxis=0,
                     h_scale=0.6774, verbose=False):
    """Calculate the SMF of a single cluster.

    logm_bins: log(stellar mass [Msun]) bins. If None, these are calculated
               with logarithmic spacing of dlogm in dex.
    bound: (bool) Only consider subhalos bound to FoF group 0.
    sats_only: (bool) Only consider satellites not the main halo.
    calc_30kpc: (bool) Calculate stellar masses in a 30kpc aperture.
    calc_total_mass: (bool) Calculate the total mass (of all particle types)
                      within the specified volume.
    count_M: Lower stellar mass limit for the total galaxy count in Msun/h.
    norm_by_M500: (bool) Normalise the SMF of each cluster by its M500 before
                  stacking.
    Rlim: (list length 2) Min. and max. radius of satellites. Units are defined
          by RlimType. Set to None for all bound satellites (no aperture).
          Rlims[1]=None is interpreted as infinity.
    RlimType: (str) Defines units of Rlim, either "R200" or "pkpc" or "ckpc".
    projected: (bool) Calculate SMF in a 2D radius (Rlim), otherwise 3D.
    zaxis: (int) Index for the projection axis (0, 1 or 2).
    """
    import numpy as np
    import readsnap as rs

    if Rlims is None:
        Rlims = [None, None]

    # Get the subhalo indices for this group.
    subnums = get_subnums(
            sim, grpnum, bound=bound, no_BCGs=no_BCGs, sats_only=sats_only,
            Rlims=Rlims, RlimType=RlimType, projected=projected, zaxis=zaxis,
            h_scale=h_scale)

    # Get data structure to store SMF data.
    dat = get_SMF_data_structure()

    if logm_bins is None:
        # Define stellar mass bins in units of Msun.
        logm_min = 8.0
        logm_max = 12.0
        dat.dlogm = dlogm
        nbins = int(np.round((logm_max - logm_min)/dlogm + 1))
        logm_bins = np.arange(nbins, dtype=np.float32)*dlogm + logm_min
        # Middle of bins.
        dat.logm_binsp = (logm_min + 0.5*dlogm + dlogm *
                          np.arange(nbins-1, dtype=np.float32))
    else:
        nbins = len(logm_bins)
        logm_min = np.min(logm_bins)
        logm_max = np.max(logm_bins)
        # Make sure stellar mass bins are sorted in ascending order.
        logm_bins = np.sort(logm_bins)
        # Calculate distance between bins.
        diff = np.diff(logm_bins)
        if np.any(np.abs(diff - diff[0]) > 1e-3):
            raise ValueError("Could not calculate constant logarithmic "
                             "spacing between stellar mass bins.")
        else:
            dat.dlogm = diff[0]
    # Temporarily convert bins to units of Msun/h.
    cur_logm_bins = logm_bins + np.log10(h_scale)
    if count_M is None:
        count_M = np.min(cur_logm_bins)

    # Create arrays to store SMFs for different stellar mass apertures.
    n_total = np.zeros(nbins-1, dtype=np.float32)
    n_inrad = np.zeros(nbins-1, dtype=np.float32)
    n_inVmaxrad = np.zeros(nbins-1, dtype=np.float32)
    n_inSPrad = np.zeros(nbins-1, dtype=np.float32)
    n_in30kpc = np.zeros(nbins-1, dtype=np.float32)
    dat.totmass = 0.0

    if calc_total_mass:
        # Calculate total mass of all particles types.
        if Rlims[0] is None:
            raise ValueError("Rlims[0] must be defined if calc_total_mass "
                             "is True.")
        if Rlims[1] is None:
            raise ValueError("Rlims[1] must be defined if calc_total_mass "
                             "is True.")
        if projected:
            axes = [0, 1, 2]  # axis indices
            assert zaxis in axes, "zaxis must be 0, 1 or 2"
            # remove z axis index, use remaining two for projection axes
            axes.remove(zaxis)

        for parttype in range(6):
            partmass = rs.read_block(sim.snapname, "MASS", parttype=parttype,
                                     cosmic_ray_species=sim.num_cr_species)
            partpos = rs.read_block(sim.snapname, "POS ", parttype=parttype,
                                    cosmic_ray_species=sim.num_cr_species)
            if sim.mpc_units:
                partpos *= 1000.0
            if projected:
                rel_pos = sim.rel_pos(partpos, sim.cat.group_pos[grpnum])
                r = np.sqrt((rel_pos[:, axes[0]])**2 +
                            (rel_pos[:, axes[1]])**2)
            else:
                r = sim.nearest_dist_3D(partpos, sim.cat.group_pos[grpnum])
            del partpos
            Rlims_cur = get_Rlims_in_code_units(Rlims, RlimType, sim,
                                                grpnum=grpnum, h_scale=h_scale)
            dat.totmass += 1e10 / h_scale * np.sum(
                    partmass[np.where((r >= Rlims_cur[0]) &
                                      (r < Rlims_cur[1]))[0]])
            del partmass
    mstar = sim.cat.sub_masstab[subnums, 4] * 1e10
    mstar_inrad = sim.cat.sub_massinradtab[subnums, 4] * 1e10
    mstar_inVmaxrad = sim.cat.sub_massinmaxradtab[subnums, 4] * 1e10
    mstar_inSPrad = sim.cat.sub_massinSPrad[subnums] * 1e10

    # Store the total stellar mass of satellites in the given mass range.
    ind = np.where((np.log10(mstar / h_scale) > logm_min) &
                   (np.log10(mstar / h_scale) < logm_max))[0]
    dat.totmstar = np.sum(mstar[ind] / h_scale)
    ind = np.where((np.log10(mstar_inrad / h_scale) > logm_min) &
                   (np.log10(mstar_inrad / h_scale) < logm_max))[0]
    dat.totmstar_inrad = np.sum(mstar_inrad[ind] / h_scale)
    del ind

    # Store the total stellar mass per bin in Msun
    dat.m_total, _ = np.histogram(np.log10(mstar), bins=cur_logm_bins,
                                  weights=mstar / h_scale)

    # Count the cumulative number of galaxies above mass 'count_M'
    count = np.count_nonzero(mstar >= count_M)

    if calc_30kpc:
        # Calculate the stellar mass of galaxies within a 30 pkpc aperture.
        if verbose:
            print "Loading masses and positions..."
        # Convert 30 kpc proper distance aperture to comoving distance.
        R = 30.0 * h_scale * (1 + sim.header.redshift)
        pos = rs.read_block(sim.snapname, "POS ", parttype=4,
                            cosmic_ray_species=sim.num_cr_species)
        if sim.mpc_units:
            pos *= 1000.0
        mass = rs.read_block(sim.snapname, "MASS", parttype=4,
                             cosmic_ray_species=sim.num_cr_species)
        # age is actually stellar formation time in units of scale factor.
        age = rs.read_block(sim.snapname, "GAGE",
                            cosmic_ray_species=sim.num_cr_species)
        mstar_30kpc = []
        if verbose:
            print "Calculating subhalo stellar masses within 30 kpc"
        for subnum in subnums:
            r = sim.nearest_dist_3D(pos[sim.cat.sub_offsettab[subnum, 4]:
                                        sim.cat.sub_offsettab[subnum, 4] +
                                        sim.cat.sub_lentab[subnum, 4]],
                                    sim.cat.sub_pos[subnum])
            # GFM wind particles are star particles with formation time <0
            # subfind counts these as gas when calculating subhalo masses.
            ageind = np.where(age[sim.cat.sub_offsettab[subnum, 4]:
                                  sim.cat.sub_offsettab[subnum, 4] +
                                  sim.cat.sub_lentab[subnum, 4]] > 0.0)[0]
            starind = (sim.cat.sub_offsettab[subnum, 4] +
                       np.intersect1d(np.where(r <= R), ageind))
            mstar_30kpc.append(np.sum(mass[starind]) * 1e10)
        mstar_30kpc = np.array(mstar_30kpc)
        dat.totmstar_30kpc = np.sum(mstar_30kpc / h_scale)

    for i in range(0, nbins-2):
        n_total[i] = np.count_nonzero((np.log10(mstar) >= cur_logm_bins[i]) &
                                      (np.log10(mstar) < cur_logm_bins[i+1]))
        n_inrad[i] = np.count_nonzero(
                (np.log10(mstar_inrad) >= cur_logm_bins[i]) &
                (np.log10(mstar_inrad) < cur_logm_bins[i+1]))
        n_inVmaxrad[i] = np.count_nonzero(
                (np.log10(mstar_inVmaxrad) >= cur_logm_bins[i]) &
                (np.log10(mstar_inVmaxrad) < cur_logm_bins[i+1]))
        n_inSPrad[i] = np.count_nonzero(
                (np.log10(mstar_inSPrad) >= cur_logm_bins[i]) &
                (np.log10(mstar_inSPrad) < cur_logm_bins[i+1]))
        if calc_30kpc:
            n_in30kpc[i] += np.count_nonzero(
                    (np.log10(mstar_30kpc) >= cur_logm_bins[i]) &
                    (np.log10(mstar_30kpc) < cur_logm_bins[i+1]))

    if norm_by_M500:
        n_total = n_total / (sim.cat.group_m_crit500[grpnum] * 1e10 / h_scale)
        n_inrad = n_inrad / (sim.cat.group_m_crit500[grpnum] * 1e10 / h_scale)
        n_inVmaxrad = n_inVmaxrad / (sim.cat.group_m_crit500[grpnum] *
                                     1e10 / h_scale)
        n_inSPrad = n_inSPrad / (sim.cat.group_m_crit500[grpnum]*1e10/h_scale)
        n_in30kpc = n_in30kpc / (sim.cat.group_m_crit500[grpnum]*1e10/h_scale)

    dat.logm_bins = logm_bins
    dat.count = count
    dat.n_total = n_total
    dat.n_inrad = n_inrad
    dat.n_inVmaxrad = n_inVmaxrad
    dat.n_inSPrad = n_inSPrad
    dat.n_in30kpc = n_in30kpc
    dat.smf_total = np.ma.masked_equal(n_total, 0.0) / dlogm
    dat.smf_inrad = np.ma.masked_equal(n_inrad, 0.0) / dlogm
    dat.smf_in30kpc = np.ma.masked_equal(n_in30kpc, 0.0) / dlogm
    dat.smf_inVmaxrad = np.ma.masked_equal(n_inVmaxrad, 0.0) / dlogm
    dat.smf_inSPrad = np.ma.masked_equal(n_inSPrad, 0.0) / dlogm

    return dat


def calc_LF_cluster(sim, grpnum, logM_bins=None, dlogM=0.2,
                    bound=True, no_BCGs=False, sats_only=True,
                    count_M=None,
                    Rlims=None, RlimType="R200",
                    projected=False, zaxis=0,
                    h_scale=0.6774, verbose=False):
    raise NotImplementedError("calc_LF_cluster() not yet implemented.")


def get_subnums(sim, grpnum, bound=True, no_BCGs=False, sats_only=True,
                Rlims=None, RlimType="R200", projected=False, zaxis=0,
                h_scale=0.6774):
    import numpy as np

    if Rlims is None:
        Rlims = [None, None]
    if sats_only:
        # Consider all subhaloes in group other than the main halo.
        subnums = np.arange(sim.cat.group_firstsub[grpnum] + 1,
                            sim.cat.group_firstsub[grpnum] +
                            sim.cat.group_nsubs[grpnum])
    elif bound:
        # Consider all bound subhaloes (including main halo).
        subnums = np.arange(sim.cat.group_firstsub[grpnum],
                            sim.cat.group_firstsub[grpnum] +
                            sim.cat.group_nsubs[grpnum])
    elif no_BCGs:
        # Consider all subhaloes (even unbound) except for the BCG.
        subnums = range(sim.cat.nsubs)
        subnums.pop(sim.cat.group_firstsub[grpnum])  # remove BCG from list.
        subnums = np.array(subnums)
    else:
        # Consider all subhaloes regardless of whether they are bound.
        subnums = np.arange(sim.cat.nsubs)
    # Ignore subhaloes without any bound star particles.
    subnums = subnums[np.where(sim.cat.sub_masstab[subnums, 4] > 0.0)[0]]
    # Get galaxies within aperture.
    if Rlims[0] is not None or Rlims[1] is not None:
        Rlims_cur = get_Rlims_in_code_units(Rlims, RlimType, sim,
                                            grpnum=grpnum, h_scale=h_scale)
        if projected:
            axes = [0, 1, 2]  # axis indices
            assert zaxis in axes, "zaxis must be 0, 1 or 2"
            # remove z axis index, use remaining two for projection axes
            axes.remove(zaxis)
            rel_pos = sim.rel_pos(sim.cat.sub_pos[subnums],
                                  sim.cat.group_pos[grpnum])
            r = np.sqrt((rel_pos[:, axes[0]])**2 +
                        (rel_pos[:, axes[1]])**2)
        else:
            r = sim.nearest_dist_3D(sim.cat.sub_pos[subnums],
                                    sim.cat.group_pos[grpnum])
        # Restrict to subhaloes within radial range.
        subnums = subnums[np.where((r >= Rlims_cur[0]) &
                                   (r < Rlims_cur[1]))[0]]

    return subnums


def get_Rlims_in_code_units(Rlims, RlimType, sim, grpnum=None, h_scale=0.6774):
    import numpy as np

    if Rlims[0] is None:
        Rlims[0] = 0.0
    if Rlims[1] is None:
        Rlims[1] = np.inf
    Rlims = np.array(Rlims)
    if RlimType == "R200":
        # scale to R200 in code units (ckpc/h)
        if grpnum is None:
            raise ValueError("grpnum is required for RlimType 'R200'.")
        Rlims_cur = Rlims * sim.cat.group_r_crit200[grpnum]
    elif RlimType == "R500":
        # scale to R500 in code units (ckpc/h)
        Rlims_cur = Rlims * sim.cat.group_r_crit500[grpnum]
    elif RlimType == "pkpc":
        # convert to code units (ckpc/h)
        Rlims_cur = Rlims * h_scale * (1+sim.header.redshift)
    elif RlimType == "ckpc":
        # convert to code units (ckpc/h)
        Rlims_cur = Rlims * h_scale
    else:
        raise ValueError("RlimType of " + str(RlimType) +
                         " not recognised.")

    return Rlims_cur


def get_SMF_data_structure():
    # Create namedtuple to store the data since this is easier to reference
    # compared to returning a regular tuple.
    from collections import namedtuple
    dat = namedtuple('dat', ['logm_bins', 'dlogm', 'logm_binsp', 'smf_total',
                             'smf_inrad', 'smf_in30kpc', 'smf_inVmaxrad',
                             'smf_inSPrad', 'n_total', 'n_inrad', 'n_in30kpc',
                             'n_inVmaxrad', 'n_inSPrad', 'm_total', 'count',
                             'totmstar', 'totmstar_inrad', 'totmstar_30kpc',
                             'totmass'])
    return dat


def list_halo_masses(basedir, simdirs, snapnums, mpc_units=True, h_scale=0.6774):
    import snap
    import numpy as np
    
    assert len(simdirs) == len(snapnums), "Need snapshot number for each simdir"
    logM200 = []
    print "Masses in Msun / h ({:.2f} dex)".format(np.log10(h_scale))
    print "Sim         log10(M500c)  log10(M200c)"
    for idx, simdir in enumerate(simdirs):
        sim = snap.snapshot(basedir+simdir, snapnums[idx], mpc_units=mpc_units)
        
        #print simdir, "log10(M200c*h) = {:.2f}".format(np.log10(sim.cat.group_m_crit200[0] * 1e10))
        print simdir, "    {:.2f}       ".format(np.log10(sim.cat.group_m_crit500[0] * 1e10)), "{:.2f}".format(np.log10(sim.cat.group_m_crit200[0] * 1e10))
        logM200.append(np.log10(sim.cat.group_m_crit200[0] * 1e10))
    
    print "Average log(Mass) = {:.2f}".format(np.mean(logM200))
    
if __name__ == "__main__":
    main()