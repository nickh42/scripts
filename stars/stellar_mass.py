#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
This module contains various functions for calculating stellar properties of
a simulation, including the galaxy stellar mass function, mass fractions and
tracking of star particles backwards in time.
@author: nh444
"""


def main():
    """
    Use this main function for doing multiple analyses on one or more
    simulations. If you just want, e.g. GSMF, use the separate scripts provided
    e.g. SMF.py, which will also plot multiple sim SMFs for comparison
    """
    import os

    # General settings
    obs_dir = "/home/nh444/vault/ObsData/L-T/"
    N = 300
    delta = 500.0
    ref = "crit"
    use_fof = True  # Use halo file for M_delta and R_delta
    h_scale = 0.679
    filenames = []

    basedir = "/data/emergence3/deboras/Elliptical_ZoomIns/"
    simdirs = ["L40_512_DutyInt1RadioWeak_Thermal_FixEta/",
               "L40_512_DutyIntRadioWeak_Thermal/",
               "L40_512_LowDutyRadioWeak_HalfThermal_FixEta/",
               "L40_512_LowDutyRadioWeak_HalfThermal_FixEta_BHChangeVel/",
               "L40_512_NoDutyRadioInt_HalfThermal_FixEta/",
               "L40_512_NoDutyRadioIntLow_HalfThermal_FixEta/",
               "L40_512_NoDutyRadioIntLow_HalfThermal_FixEta_BHChangeVel/",
               "L40_512_NoDutyRadioWeak_HalfThermal_FixEta/",
               "L40_512_NoDutyRadioWeak_HalfThermal_FixEta_BHChange/",
               "L40_512_NoDutyRadioWeak_HalfThermal_FixEta_BHChangeVel/",
               "L40_512_NoDutyRadioWeak_HalfThermal_FixEtaRadEff/",
               "L40_512_Temp7DutyRadioWeak_HalfThermal_FixEta_BHChangeVel/",
               "L40_512_TempDutyRadioWeak_HalfThermal_FixEta_BHChangeVel/",
               "L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff/",
               "L40_512_NoDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_Prex/"]

    simdirs = ["L40_512_NoDutyRadioWeak_HalfThermal_FixEta/",
               "L40_512_NoDutyRadioWeak_HalfThermal_FixEtaRadEff/",
               "L40_512_NoDutyRadioWeak_HalfThermal_FixEta_BHChangeVel/",
               "L40_512_NoDutyRadioWeak_HalfThermal_FixEta_BHChange/",
               "L40_512_LowDutyRadioWeak_HalfThermal_FixEta/",
               "L40_512_LowDutyRadioWeak_HalfThermal_FixEta_BHChangeVel/",
               "L40_512_TempDutyRadioWeak_HalfThermal_FixEta_BHChangeVel/",
               "L40_512_NoDutyRadioIntLow_HalfThermal_FixEta/",
               "L40_512_NoDutyRadioIntLow_HalfThermal_FixEta_BHChangeVel/",
               "L40_512_NoDutyRadioInt_HalfThermal_FixEta/",
               "L40_512_NoDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_Prex/",
               "L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff/"]
    labels = [r"NDRW",
              r"NDRWRadEff",
              r"NDRWBHVel",
              r"NDRWBHAcc",
              r"LDRW",
              r"LDRWBHVel",
              r"TDRWBHVel",
              r"NDRIntLow",
              r"NDRIntLowBHVel",
              r"NDRInt",
              r"NDRIntRadioEffBHVelPre",
              r"LDRIntRadioEffBHVel"]
    simdirs = ["L40_512_NoDutyRadioWeak_HalfThermal_FixEta/",
               "L40_512_LowDutyRadioWeak_HalfThermal_FixEta_BHChangeVel/",
               "L40_512_NoDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_Prex/",
               "L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff/",
               "L40_512_LowDutyRadioWeak_HalfThermal_FixEta/",
               ]
    labels = ["Continuous QM feedback",
              r"QM duty cycle ($\delta t = 0.02$)",
              r"QM duty cycle ($\delta t = 0.01$)",
              "Continuous QM feedback without pressure criterion"]
    simdirs = ["L40_512_NoDutyRadioWeak_HalfThermal_FixEta/",
               "L40_512_NoDutyRadioInt_HalfThermal_FixEta/",
               "L40_512_LowDutyRadioWeak_HalfThermal_FixEta/",
               "L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff/",
               ]
    labels = ["Ref",
              "Stronger radio",
              "Quasar duty cycle",
              "Best"]

    basedir = "/data/curie4/nh444/project1/boxes/"
    simdirs = ["L40_512_MDRIntRadioEff/",
               ]
    labels = [None]

    zoombasedir = "/home/nh444/data/project1/zoom_ins/"
    zoomdirs = ["MDRInt/"]
    zooms = [["c96_MDRInt", #"c128_MDRInt", "c192_MDRInt",
#              "c224_MDRInt", "c256_MDRInt", "c288_MDRInt",
#              "c320_MDRInt", "c352_MDRInt",
#              "c384_MDRInt",
#              "c448_MDRInt",  # "c480_MDRInt",
#              "c512_MDRInt",
              ]]
    snapnums = range(20, 26)
    outsuffix = "_MDRInt"
    #zooms =[["c400_MDRInt"]]
    #snapnums = range(1, 100)
    # outsuffix="_c400"

    outdir = "/data/curie4/nh444/project1/stars/"
    masstab = False
    combine_plots = False

    # Millenium runs
    #basedir = "/data/deboras/ewald/"
    #simdirs = ["m_10002_h_94_501_z3/csfbh/"]
    #snapnums = [63]
    #masstab = True
    #mpc_units = True

    #outfile = "stars_formed_vs_distance.pdf"
    #outfile = "stellar_fractions.pdf"
    #outfile = outdir+"SMFs.pdf"

    #plot_mstarfrac_v_mass_comparison(outdir, simdirs, 25, obs_dir, outfilename = "stellar_mass_fractions.pdf", z=0, delta=delta, for_paper=True, scat_idx=11, labels=labels)

    for simdir in simdirs:
        mstar = stellar_mass(basedir, simdir, outdir, snapnums, obs_dir, N=N, delta=delta,
                             ref=ref, h_scale=h_scale, use_fof=use_fof, masstab=masstab, mpc_units=False)

        # mstar.show_sSFR_distribution()

        ### Calculate stellar masses in various apertures ###
        mstar.get_stellar_masses(M500min=10**11.6, zaxis=0, get_SFR=True, verbose=False)

        ### Stellar mass function###
        # mstar.get_SMF(bcgs_only=True)
        # mstar.plot_SMF(plot="all")
        #plotfile = mstar.plot_SMF(plot="even", bcgs_only=True, for_paper=True)
        # mstar.plot_SMF(plot="odd")
        #os.system("ln -s "+outdir+run+"SMF/SMF_zeven_"+run[:-1]+".pdf /data/vault/nh444/project1/Figures/SMFs/")

        ### Stellar mass fractions###
        # mstar.get_mass_fracs()
        # plotfile = mstar.plot_mstarfrac_v_mass(for_paper=True) #requires get_mass_fracs
        # mstar.plot_mstarfrac_v_redshift(nplot=7) ## plot 'nplot' most massive (M_delta)
        # mstar.plot_mstar_v_redshift()

        ### Stellar mass profiles ###
        #mstar.get_mstar_profiles(rmax=500, bins=1000)
        # plotfile = mstar.plot_mstar_profiles(nplot=6, rmax=300.0, bins=200) #requires get_mstar_prof

        ### Stellar mass components###
        #check_before = 0
        # If 0, we get the properties of new stars in the first snapshot after their formation
        # If 1, we get the properties of new stars from the snapshot prior to their formation
        #group = 0
        #snapnum_min = 2
        # outsuffix="_c384"
        #mstar.get_ids_stellar_components(max(snapnums), group=group, cutoff_rad=30.0, include_fuzzy=False)
        #mstar.get_new_star_props(outsuffix=outsuffix, snapnum_min=snapnum_min, check_before=check_before, include_fuzzy=False, get_dist=True)
        # plotfile = mstar.plot_new_stars_vs_distance(snapnum_min=snapnum_min, nbins=100, maxR=4000, fit=False, logx=False, normed=False, check_before=check_before, group=group, for_paper=True) #requires get_new_stars_vs_distance
        #os.system("ln -s "+plotfile+" /data/vault/nh444/project1/Figures/stardist/"+simdir[8:-1]+".pdf")
        #mstar.plot_new_stars_map(24, ngrid=1024, outsuffix=outsuffix, group=group, check_before=check_before, include_fuzzy=False)
        # for i in range(min(snapnums), max(snapnums)+1):
        #mstar.plot_new_stars_map(i, ngrid=1024, group=group, check_before=0, include_fuzzy=False)

        ### Combine plots into one pdf ###
        #if combine_plots: filenames.append(plotfile)

# ==============================================================================
#     for i, zoomdir in enumerate(zoomdirs):
#         if zoomdir is not "":
#             for zoom in zooms[i]:
#                 mstar = stellar_mass(zoombasedir+zoomdir, zoom+"/", outdir+zoomdir, snapnums, obs_dir, N=1, delta=delta, ref=ref, h_scale=h_scale, use_fof=use_fof, masstab=masstab, mpc_units=True)
#                 mstar.get_stellar_masses(N=1, zaxis=0, verbose=True)
#                 #mstar.show_sSFR_distribution()
# ==============================================================================
        #mstar.show_satellite_mass_distribution([0], cumulative=False, fractional=False)
        #mstar.show_satellite_mass_radius([0], cumulative=True, fractional=False)

        ### Stellar mass components###
        #check_before = 0
        # If 0, we get the properties of new stars in the first snapshot after their formation
        # If 1, we get the properties of new stars from the snapshot prior to their formation
        #group = 0
        #mstar.get_ids_stellar_components(max(snapnums), group=group, cutoff_rad=50.0, include_fuzzy=False)
        #mstar.get_new_star_props(outsuffix=outsuffix, snapnum_min=min(snapnums), check_before=check_before, include_fuzzy=False, get_dist=True)
        # mstar.plot_new_stars_vs_distance(snapnum_min=min(snapnums), snapnum_max=max(snapnums), nbins=100, norm_by_vol=False, fit=False, logx=False, normed=False, check_before=check_before, group=group, outsuffix=outsuffix, for_paper=True) #requires get_new_stars_vs_distance
        #mstar.plot_new_stars_map(69, dxyimg=5000.0, ngrid=1024, outsuffix=outsuffix, group=group, check_before=check_before, include_fuzzy=False)
        #mstar.plot_stars_map(25, dxyimg=1500.0, circrad=[1.0], ngrid=1024, outsuffix=outsuffix, group=group, check_before=check_before, include_fuzzy=False)
        #mstar.plot_stars_infalling_subhalo([79, 84, 89, 94, 99], 99, 4, dxyimg=800.0, group=group, cd_cutoff_method=0, cutoff_rad=50.0, include_fuzzy=False, outsuffix=outsuffix)
        #mstar.plot_stars_infalling_subhalo([94, 95, 96, 97, 98, 99], 99, 34, dxyimg=800.0, group=group, cd_cutoff_method=0, cutoff_rad=50.0, include_fuzzy=False, outsuffix=outsuffix)
        #mstar.plot_stars_infalling_subhalo([79], 99, 3, dxyimg=800.0, group=group, cd_cutoff_method=0, cutoff_rad=50.0, include_fuzzy=False, outsuffix=outsuffix)
        #mstar.plot_infall_times(group=group, normed=True, check_before=check_before, outsuffix=outsuffix)

    # if combine_plots:
        #os.system("gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile="+outfile+" "+" ".join(filenames))
        #print "Combined PDF saved to", outfile


def chiu16_fstar_relation(M1, M2, z=0.0, sample="combined"):
    # Compute stellar mass fraction for two given M500 according to
    # best-fit relation of Chiu 2006 "Baryon content of massive galaxy clusters"
    import sim_python.cosmo as cosmo
    cm = cosmo.cosmology(0.7, 0.3)
    if sample == "combined":
        alpha = 0.0099
        beta = -0.37
        gamma = 0.26
        label = "Chiu et al. 2016 best-fit to combined sample ($f_{\star} \propto M_{500}^{-0.37 \pm 0.09}(1+z)^{0.26 \pm 0.24}$)"
    elif sample == "SPT":
        alpha = 0.011
        beta = -0.09
        gamma = 1.07
        label = "Chiu et al. 2016 best-fit to SPT sample at z = 0.9 ($f_{\star} \propto M_{500}^{-0.09 \pm 0.27}(1+z)^{1.07 \pm 1.08}$)"
    else:
        print "invalid keyword 'sample' for Chiu relation"
        return None, None, None

    f1 = alpha * (1.0+z)**gamma * (M1*cm.hubble_z(z)/6.0e14)**beta
    f2 = alpha * (1.0+z)**gamma * (M2*cm.hubble_z(z)/6.0e14)**beta
    return f1, f2, label


def plot_SMF_observations(ax, z, colour1='0.5', colour2='0.75', comp_sims=True, comp_obs=True, thirtykpc=False, twicehalfrad=False, chabrier=True, for_paper=False, h_scale=0.679):
    """
    Plot observed GSMFs for redshift 'z' on a matplotlib axes 'ax'
    colour1 is the default colour for datapoints
    colour2 is the colour for obs at redshift just below z
     - e.g. for z=2, obs at 1.5<z<2 are colour2 and 2.0<z<2.5 are colour1
     - set colour2=None to not plot these points at all
    """
    import numpy as np

    obsdir = "/data/vault/nh444/ObsData/SMF/"

    ms = 8

    # convert observational data to Chabrier or Salpeter IMF by shifting log(Mstar) by a given amount based on Table 2 of Bernardi 2010
    # convert observational data to Chabrier IMF by shifting log(Mstar) by this amount:
    if chabrier:
        chabshift = 0.0
        salpshift = -0.25  # log -0.18 was used in Puchwein & Springel 2013
        kroupashift = -0.05
    else:  # convert to Salpeter
        chabshift = 0.25
        salpshift = 0.0
        kroupashift = 0.3

    d = 0.01
    if comp_sims:
        if z < 0.1:
            if thirtykpc == twicehalfrad:
                Illustris = np.loadtxt(obsdir+"Illustris_z0_SMF.txt")
                ax.errorbar(Illustris[:, 0]+np.log10(0.704**2/h_scale**2), Illustris[:, 1]+np.log10(h_scale**3/0.704**3),
                            dash_capstyle='round', lw=2.5, c='0.5', ms=ms, label="Illustris [total]")  # (Genel+ 2014) [m$_{*,tot}$]
            if twicehalfrad:
                Illustris = np.genfromtxt(
                    obsdir+"Illustris_z0_SMF_2r.csv", delimiter=",")
                ax.errorbar(Illustris[:, 0]+np.log10(0.704**2/h_scale**2), Illustris[:, 1]+np.log10(h_scale**3/0.704**3),
                            dash_capstyle='round', dashes=(0.5, 6), lw=3, c='0.5', ms=ms, label="Illustris [$< 2 r_{\star, 0.5}$]")

            #TNG = np.genfromtxt(obsdir+"IllustrisTNG100_z0.csv", delimiter=",")
            #ax.errorbar(TNG[:,0]+np.log10(0.6774**2/h_scale**2), np.log10(TNG[:,1])+np.log10(h_scale**3/0.6774**3), alpha=0.5, dash_capstyle='round', dashes=(3,6), lw=2.5, c='orange', ms=ms, label="IllustrisTNG100 [$< 2 r_{\star, 0.5}$] (Pillepich+ 2017)")
            #TNG = np.genfromtxt(obsdir+"IllustrisTNG300_30kpc_z0.csv", delimiter=",")
            #ax.errorbar(TNG[:,0]+np.log10(0.6774**2/h_scale**2), np.log10(TNG[:,1])+np.log10(h_scale**3/0.6774**3), alpha=0.5, lw=2.5, dash_capstyle='round', dashes=(3,6), c='orange', ms=ms, label="IllustrisTNG300 [$<30$ kpc] (Pillepich+ 2017)")
            #TNG = np.genfromtxt(obsdir+"IllustrisTNG300_2r_z0.csv", delimiter=",")
            #ax.errorbar(TNG[:,0]+np.log10(0.6774**2/h_scale**2), TNG[:,1]+np.log10(h_scale**3/0.6774**3), alpha=0.5, lw=2.5, dash_capstyle='round', c='orange', ms=ms, label="IllustrisTNG300 [$< 2 r_{\star, 0.5}$] (Pillepich+ 2017)")
        if thirtykpc:
            EAGLE_label = "EAGLE [$<$30 pkpc]"  # (Furlong+ 2015)"
            EAGLEcol = '0.5'  # 'darkblue'
            Edashes = (4, 6)  # (0.5,6)

            if z > 0.0 and z < 0.2:
                EAGLE = np.genfromtxt(
                    obsdir+"EAGLE/EAGLE_SMF_z0.1.csv", delimiter=",")
                ax.errorbar(EAGLE[:, 0]+np.log10(0.6777**2/h_scale**2), EAGLE[:, 1]+np.log10(h_scale**3/0.6777**3),
                            dash_capstyle='round', dashes=Edashes, lw=2.5, c=EAGLEcol, ms=ms, label=EAGLE_label)  # +" [z=0.1]"
            if z > 0.75 and z < 1.25:
                EAGLE = np.genfromtxt(
                    obsdir+"EAGLE/EAGLE_SMF_z1.csv", delimiter=",")
                ax.errorbar(EAGLE[:, 0]+np.log10(0.6777**2/h_scale**2), EAGLE[:, 1]+np.log10(h_scale**3/0.6777**3),
                            alpha=0.5, dash_capstyle='round', dashes=Edashes, lw=2.5, c=EAGLEcol, ms=ms, label=EAGLE_label)
            if z > 1.75 and z < 2.25:
                EAGLE = np.genfromtxt(
                    obsdir+"EAGLE/EAGLE_SMF_z2.csv", delimiter=",")
                ax.errorbar(EAGLE[:, 0]+np.log10(0.6777**2/h_scale**2), EAGLE[:, 1]+np.log10(h_scale**3/0.6777**3),
                            alpha=0.5, dash_capstyle='round', dashes=Edashes, lw=2.5, c=EAGLEcol, ms=ms, label=EAGLE_label)
            if z > 2.75 and z < 3.25:
                EAGLE = np.genfromtxt(
                    obsdir+"EAGLE/EAGLE_SMF_z3.csv", delimiter=",")
                ax.errorbar(EAGLE[:, 0]+np.log10(0.6777**2/h_scale**2), EAGLE[:, 1]+np.log10(h_scale**3/0.6777**3),
                            alpha=0.5, dash_capstyle='round', dashes=Edashes, lw=2.5, c=EAGLEcol, ms=ms, label=EAGLE_label)
            if z > 3.75 and z < 4.25:
                EAGLE = np.genfromtxt(
                    obsdir+"EAGLE/EAGLE_SMF_z4.csv", delimiter=",")
                ax.errorbar(EAGLE[:, 0]+np.log10(0.6777**2/h_scale**2), EAGLE[:, 1]+np.log10(h_scale**3/0.6777**3),
                            alpha=0.5, dash_capstyle='round', dashes=Edashes, lw=2.5, c=EAGLEcol, ms=ms, label=EAGLE_label)
            if z > 4.75 and z < 5.25:
                EAGLE = np.genfromtxt(
                    obsdir+"EAGLE/EAGLE_SMF_z5.csv", delimiter=",")
                ax.errorbar(EAGLE[:, 0]+np.log10(0.6777**2/h_scale**2), EAGLE[:, 1]+np.log10(h_scale**3/0.6777**3),
                            alpha=0.5, dash_capstyle='round', dashes=Edashes, lw=2.5, c=EAGLEcol, ms=ms, label=EAGLE_label)
            if z > 5.75 and z < 6.25:
                EAGLE = np.genfromtxt(
                    obsdir+"EAGLE/EAGLE_SMF_z6.csv", delimiter=",")
                ax.errorbar(EAGLE[:, 0]+np.log10(0.6777**2/h_scale**2), EAGLE[:, 1]+np.log10(h_scale**3/0.6777**3),
                            alpha=0.5, dash_capstyle='round', dashes=Edashes, lw=2.5, c=EAGLEcol, ms=ms, label=EAGLE_label)
            if z > 6.75 and z < 7.25:
                EAGLE = np.genfromtxt(
                    obsdir+"EAGLE/EAGLE_SMF_z7.csv", delimiter=",")
                ax.errorbar(EAGLE[:, 0]+np.log10(0.6777**2/h_scale**2), EAGLE[:, 1]+np.log10(h_scale**3/0.6777**3),
                            alpha=0.5, dash_capstyle='round', dashes=Edashes, lw=2.5, c=EAGLEcol, ms=ms, label=EAGLE_label)

    if comp_obs:
        if z < 0.5+d:
            # median z=0.07 but full range extends to z=0.3
            Li = np.loadtxt(obsdir+"Li_White_2009.txt")
            ax.errorbar(Li[:, 0]+chabshift+np.log10(0.73**2/h_scale**2), Li[:, 1]+np.log10(h_scale**3/0.73**3), yerr=[Li[:, 2], Li[:, 3]], capsize=ms /
                        2.0, marker='s', ls='none', c=colour1, mec=colour1, mfc='none', ms=ms, label="Li \& White 2009 [z$\sim$0.1]")  # yerr=[Li[:,2], Li[:,3]],
            souz = np.loadtxt(obsdir+"DSouza2015.txt")  # z = 0.1
            ax.errorbar(souz[:, 0]+chabshift+np.log10(0.72**2/h_scale**2), souz[:, 1]+np.log10(h_scale**3/0.72**3), yerr=souz[:, 2], capsize=ms /
                        2.0, marker='s', ls='none', c=colour1, mfc=colour1, mec='none', ms=ms, label="D'Souza+ 2015 [z$\sim$0.1]")  # yerr=souz[:,2]
            # z~0.1 (no range given)
            Bern = np.loadtxt(obsdir+"Bernardi2013_MsF_SerExp.dat")
            ax.errorbar(Bern[:, 0]+chabshift+np.log10(0.7**2/h_scale**2), Bern[:, 1]+np.log10(h_scale**3/0.7**3), yerr=Bern[:, 2], capsize=ms /
                        2.0, marker='o', ls='none', c=colour1, mfc=colour1, mec='none', ms=ms, label="Bernardi+ 2013 [z$\sim$0.1]")  # yerr=Bern[:,2],
            # z<0.06 lower mass bins should be lower limits - see paper
            Baldry = np.loadtxt(obsdir+"Baldry2012.txt")
            ax.errorbar(Baldry[:, 0]+chabshift+np.log10(0.7**2/h_scale**2), Baldry[:, 5]+np.log10(h_scale**3/0.7**3), yerr=Baldry[:, 6],
                        capsize=ms/2.0, marker='D', ls='none', c=colour1, mfc=colour1, mec='none', ms=ms*0.85, label="Baldry+ 2012 [z$<$0.06]")
            if not for_paper:
                #DES = np.genfromtxt(obsdir+"DES_2017_z0-0.2.csv", delimiter=",")
                #ax.errorbar(DES[:,0]+salpshift+np.log10(0.7**2/h_scale**2), DES[:,6]+np.log10(h_scale**3/0.7**3), yerr=[DES[:,9], DES[:,10]], marker='^', ls='none', c=colour, mfc=colour, mec='none', ms=ms, label="DES-SVA1")
                DES = np.genfromtxt(
                    obsdir+"DES_2017_z0-0.2_from_plot.csv", delimiter=",")
                ax.errorbar(DES[:, 2]+salpshift+np.log10(0.7**2/h_scale**2), DES[:, 3]+np.log10(h_scale**3/0.7**3),
                            marker='^', ls='none', c=colour1, mfc=colour1, mec='none', ms=ms+1, label="DES-SVA1 [0.0$<$z$<$0.2]")
            # data = np.loadtxt(obsdir+"SMF_Genel_data_z1.txt", skiprows=1) # Ilbert 2010 and Mortlock 2011
            #ax.errorbar(data[:,0]+2*np.log10(0.704/h_scale), data[:,1]+3*np.log10(h_scale/0.704), yerr=[data[:,2], data[:,3]], linestyle='none', marker='*', c='red', mec='none')

        # dictionary for marker styles, sizes and error bar cap sizes for each dataset: format for each is [marker, ms, capsize]
        mrk = {'Muzz': ['v', ms*1.2, ms*0.5], 'Ilb': ['^',
                                                      ms*1.2, ms*0.5], 'Tom': ['*', ms*1.5, ms*0.5]}

        if z > 0.2-d and z < 0.5+d:
            Muzz = np.loadtxt(obsdir+"Muzzin_2013_Vmax_0.2z0.5.dat")
            ax.errorbar(Muzz[:, 0]+kroupashift+np.log10(0.7**2/h_scale**2), Muzz[:, 2]+np.log10(h_scale**3/0.7**3), yerr=[Muzz[:, 4], Muzz[:, 3]], marker=mrk['Muzz']
                        [0], ls='none', c=colour1, mfc=colour1, mec='none', ms=mrk['Muzz'][1], capsize=mrk['Muzz'][2], label="Muzzin+ 2013 [0.2$<$z$<$0.5]")
        if z > 0.2-d and z < 0.5+d:
            Ilb = np.loadtxt(obsdir+"Ilbert2013/MF_Vmax_All_z0.2_0.5.dat")
            ax.errorbar(Ilb[:, 0]+chabshift+np.log10(0.7**2/h_scale**2), Ilb[:, 1]+np.log10(h_scale**3/0.7**3), yerr=[Ilb[:, 3], Ilb[:, 2]], marker=mrk['Ilb']
                        [0], ls='none', c=colour1, mfc=colour1, mec='none', ms=mrk['Ilb'][1], capsize=mrk['Ilb'][2], label="Ilbert+ 2013 [0.2$<$z$<$0.5]")
        if z > 0.2-d and z < 0.5+d:
            ZFOURGE = np.genfromtxt(
                obsdir+"ZFOURGE_Tomczak_2014.csv", delimiter=",")  # 0.2_<_z_<_0.5
            ax.errorbar(ZFOURGE[:, 0]+chabshift+np.log10(0.7**2/h_scale**2), ZFOURGE[:, 1]+np.log10(h_scale**3/0.7**3), yerr=[np.absolute(ZFOURGE[:, 3]), ZFOURGE[:, 2]],
                        marker=mrk['Tom'][0], ls='none', c=colour1, mfc=colour1, mec='none', ms=mrk['Tom'][1], capsize=mrk['Tom'][2], label="Tomczak+ 2014 [0.2$<$z$<$0.5]")
        if z > 0.5-d and z < 0.75+d:
            ZFOURGE = np.genfromtxt(
                obsdir+"ZFOURGE_Tomczak_2014.csv", delimiter=",")  # 0.5_<_z_<_0.75
            ax.errorbar(ZFOURGE[:, 0]+chabshift+np.log10(0.7**2/h_scale**2), ZFOURGE[:, 4]+np.log10(h_scale**3/0.7**3), yerr=[np.absolute(ZFOURGE[:, 6]), ZFOURGE[:, 5]],
                        marker=mrk['Tom'][0], ls='none', c=colour1, mfc=colour1, mec='none', ms=mrk['Tom'][1], capsize=mrk['Tom'][2], label="Tomczak+ 2014 [0.5$<$z$<$0.75]")
        if z > 0.5-d and z < 0.8+d:
            Ilb = np.loadtxt(obsdir+"Ilbert2013/MF_Vmax_All_z0.5_0.8.dat")
            ax.errorbar(Ilb[:, 0]+chabshift+np.log10(0.7**2/h_scale**2), Ilb[:, 1]+np.log10(h_scale**3/0.7**3), yerr=[Ilb[:, 3], Ilb[:, 2]], marker=mrk['Ilb']
                        [0], ls='none', c=colour1, mfc=colour1, mec='none', ms=mrk['Ilb'][1], capsize=mrk['Ilb'][2], label="Ilbert+ 2013 [0.5$<$z$<$0.8]")
        if z > 0.5-d and z < 1.0+d and colour2 is not None:
            Muzz = np.loadtxt(obsdir+"Muzzin_2013_Vmax_0.5z1.0.dat")
            ax.errorbar(Muzz[:, 0]+kroupashift+np.log10(0.7**2/h_scale**2), Muzz[:, 2]+np.log10(h_scale**3/0.7**3), yerr=[Muzz[:, 4], Muzz[:, 3]], marker=mrk['Muzz']
                        [0], ls='none', c=colour2, mfc=colour2, mec='none', ms=mrk['Muzz'][1], capsize=mrk['Muzz'][2], label="Muzzin+ 2013 [0.5$<$z$<$1.0]")
        if z > 0.72-d and z < 1.0+d and colour2 is not None:
            ZFOURGE = np.genfromtxt(
                obsdir+"ZFOURGE_Tomczak_2014.csv", delimiter=",")  # 0.75_<_z_<_1.0
            ax.errorbar(ZFOURGE[:, 0]+chabshift+np.log10(0.7**2/h_scale**2), ZFOURGE[:, 7]+np.log10(h_scale**3/0.7**3), yerr=[np.absolute(ZFOURGE[:, 9]), ZFOURGE[:, 8]],
                        marker=mrk['Tom'][0], ls='none', c=colour2, mec=colour2, mfc='none', ms=mrk['Tom'][1], capsize=mrk['Tom'][2], label="Tomczak+ 2014 [0.75$<$z$<$1.0]")
        if z > 0.8-d and z < 1.1+d:
            Ilb = np.loadtxt(obsdir+"Ilbert2013/MF_Vmax_All_z0.8_1.1.dat")
            ax.errorbar(Ilb[:, 0]+chabshift+np.log10(0.7**2/h_scale**2), Ilb[:, 1]+np.log10(h_scale**3/0.7**3), yerr=[Ilb[:, 3], Ilb[:, 2]], marker=mrk['Ilb']
                        [0], ls='none', c=colour1, mfc=colour1, mec='none', ms=mrk['Ilb'][1], capsize=mrk['Ilb'][2], label="Ilbert+ 2013 [0.8$<$z$<$1.1]")
        if z > 1.0-d and z < 1.25+d:
            ZFOURGE = np.genfromtxt(
                obsdir+"ZFOURGE_Tomczak_2014.csv", delimiter=",")  # 1_<_z_<_1.25
            ax.errorbar(ZFOURGE[:, 0]+chabshift+np.log10(0.7**2/h_scale**2), ZFOURGE[:, 10]+np.log10(h_scale**3/0.7**3), yerr=[np.absolute(ZFOURGE[:, 12]), ZFOURGE[:, 11]],
                        marker=mrk['Tom'][0], ls='none', c=colour1, mfc=colour1, mec='none', ms=mrk['Tom'][1], capsize=mrk['Tom'][2], label="Tomczak+ 2014 [1.0$<$z$<$1.25]")
        # if z>1.0-d and z<1.5+d:
            #Mort = np.genfromtxt(obsdir+"Mortlock_2011.csv", delimiter=",")
            #ax.errorbar(Mort[:,0]+salpshift+np.log10(0.7**2/h_scale**2), Mort[:,1]+np.log10(h_scale**3/0.7**3), yerr=Mort[:,2], marker='D', ls='none', c=colour, mfc=colour, mec='none', ms=ms)
        # if z>0.8-d and z<1.4+d:
            # Bram = np.genfromtxt(obsdir+"NMBS_Brammer_2011.csv", delimiter=",")#0.8_<_z_<_1.4
            #ax.errorbar(Bram[:,0]+kroupashift+np.log10(0.7**2/h_scale**2), Bram[:,7]+np.log10(h_scale**3/0.7**3), yerr=Bram[:,8], marker='v', ls='none', c=colour, mfc=colour, mec='none', ms=ms)
        # if z>0.9-d and z<1.1+d:
            #VIMOS = np.genfromtxt(obsdir+"VIMOS_Davidzon_2013.csv", delimiter=",")
            #ax.errorbar(VIMOS[6:,0]+chabshift+np.log10(0.7**2/h_scale**2), VIMOS[6:,13]+np.log10(h_scale**3/0.7**3), yerr=[np.absolute(VIMOS[6:,15]), VIMOS[6:,14]], marker='o', ls='none', c=colour, mfc=colour, mec='none', ms=ms)
        # if z>1.1-d and z<1.3+d:
            #VIMOS = np.genfromtxt(obsdir+"VIMOS_Davidzon_2013.csv", delimiter=",")
            #ax.errorbar(VIMOS[8:,0]+chabshift+np.log10(0.7**2/h_scale**2), VIMOS[8:,16]+np.log10(h_scale**3/0.7**3), yerr=[np.absolute(VIMOS[8:,18]), VIMOS[8:,17]], marker='o', ls='none', c=colour, mfc=colour, mec='none', ms=ms)
        if z > 1.0-d and z < 1.5+d:
            Muzz = np.loadtxt(obsdir+"Muzzin_2013_Vmax_1.0z1.5.dat")
            ax.errorbar(Muzz[:, 0]+kroupashift+np.log10(0.7**2/h_scale**2), Muzz[:, 2]+np.log10(h_scale**3/0.7**3), yerr=[Muzz[:, 4], Muzz[:, 3]], marker=mrk['Muzz']
                        [0], ls='none', c=colour1, mfc=colour1, mec='none', ms=mrk['Muzz'][1], capsize=mrk['Muzz'][2], label="Muzzin+ 2013 [1.0$<$z$<$1.5]")
        if z > 1.1-d and z < 1.5+d:
            Ilb = np.loadtxt(obsdir+"Ilbert2013/MF_Vmax_All_z1.1_1.5.dat")
            ax.errorbar(Ilb[:, 0]+chabshift+np.log10(0.7**2/h_scale**2), Ilb[:, 1]+np.log10(h_scale**3/0.7**3), yerr=[Ilb[:, 3], Ilb[:, 2]], marker=mrk['Ilb']
                        [0], ls='none', c=colour1, mfc=colour1, mec='none', ms=mrk['Ilb'][1], capsize=mrk['Ilb'][2], label="Ilbert+ 2013 [1.1$<$z$<$1.5]")
        if z > 1.25-d and z < 1.5+d:
            ZFOURGE = np.genfromtxt(
                obsdir+"ZFOURGE_Tomczak_2014.csv", delimiter=",")  # 1.25_<_z_<_1.5
            ax.errorbar(ZFOURGE[:, 0]+chabshift+np.log10(0.7**2/h_scale**2), ZFOURGE[:, 13]+np.log10(h_scale**3/0.7**3), yerr=[np.absolute(ZFOURGE[:, 15]), ZFOURGE[:, 14]],
                        marker=mrk['Tom'][0], ls='none', c=colour1, mfc=colour1, mec='none', ms=mrk['Tom'][1], capsize=mrk['Tom'][2], label="Tomczak+ 2014 [1.25$<$z$<$1.5]")
        if z > 1.5-d and z < 2.0+d and colour2 is not None:
            ZFOURGE = np.genfromtxt(
                obsdir+"ZFOURGE_Tomczak_2014.csv", delimiter=",")  # 1.5_<_z_<_2.0
            ax.errorbar(ZFOURGE[:, 0]+chabshift+np.log10(0.7**2/h_scale**2), ZFOURGE[:, 16]+np.log10(h_scale**3/0.7**3), yerr=[np.absolute(ZFOURGE[:, 18]), ZFOURGE[:, 17]],
                        marker=mrk['Tom'][0], ls='none', c=colour2, mec=colour2, mfc='none', ms=mrk['Tom'][1], capsize=mrk['Tom'][2], label="Tomczak+ 2014 [1.5$<$z$<$2.0]")
        if z > 1.5-d and z < 2.0+d and colour2 is not None:
            Muzz = np.loadtxt(obsdir+"Muzzin_2013_Vmax_1.5z2.0.dat")
            ax.errorbar(Muzz[:, 0]+kroupashift+np.log10(0.7**2/h_scale**2), Muzz[:, 2]+np.log10(h_scale**3/0.7**3), yerr=[Muzz[:, 4], Muzz[:, 3]], marker=mrk['Muzz']
                        [0], ls='none', c=colour2, mfc=colour2, mec='none', ms=mrk['Muzz'][1], capsize=mrk['Muzz'][2], label="Muzzin+ 2013 [1.5$<$z$<$2.0]")
        if z > 1.5-d and z < 2.0+d and colour2 is not None:
            Ilb = np.loadtxt(obsdir+"Ilbert2013/MF_Vmax_All_z1.5_2.0.dat")
            ax.errorbar(Ilb[:, 0]+chabshift+np.log10(0.7**2/h_scale**2), Ilb[:, 1]+np.log10(h_scale**3/0.7**3), yerr=[Ilb[:, 3], Ilb[:, 2]], marker=mrk['Ilb']
                        [0], ls='none', c=colour2, mfc=colour2, mec='none', ms=mrk['Ilb'][1], capsize=mrk['Ilb'][2], label="Ilbert+ 2013 [1.5$<$z$<$2.0]")

        if z > 2.0-d and z < 2.5+d:
            ZFOURGE = np.genfromtxt(
                obsdir+"ZFOURGE_Tomczak_2014.csv", delimiter=",")  # 2.0_<_z_<_2.5
            ax.errorbar(ZFOURGE[:, 0]+chabshift+np.log10(0.7**2/h_scale**2), ZFOURGE[:, 19]+np.log10(h_scale**3/0.7**3), yerr=[np.absolute(ZFOURGE[:, 21]), ZFOURGE[:, 20]],
                        marker=mrk['Tom'][0], ls='none', c=colour1, mfc=colour1, mec='none', ms=mrk['Tom'][1], capsize=mrk['Tom'][2], label="Tomczak+ 2014 [2.0$<$z$<$2.5]")
            # Mort = np.genfromtxt(obsdir+"Mortlock_2011.csv", delimiter=",")#2.0_<_z_<_2.5
            #ax.errorbar(Mort[:,0]+salpshift+np.log10(0.7**2/h_scale**2), Mort[:,5]+np.log10(h_scale**3/0.7**3), yerr=Mort[:,6], marker='D', ls='none', c=colour, mfc=colour, mec='none', ms=ms)

            Ilb = np.loadtxt(obsdir+"Ilbert2013/MF_Vmax_All_z2.0_2.5.dat")
    #        lolims = (Ilb[:,2] < 0.0) ## negative errors mean a lower limit
    #        Ilb[lolims,3]=0.0 ## no lower error bar, just upwards arrow
    #        Ilb[lolims,2]=0.15 ## sets size of arrow
    #        ax.errorbar(Ilb[:,0]+chabshift+np.log10(0.7**2/h_scale**2), Ilb[:,1]+np.log10(h_scale**3/0.7**3), yerr=[Ilb[:,3], Ilb[:,2]], lolims=lolims, marker=mrk['Ilb'][0], ls='none', c=colour1, mfc=colour1, mec='none', ms=ms, label="Ilbert+ 2013 [2.0$<$z$<$2.5]")
            lolims = (Ilb[:, 2] < 0.0)  # negative errors mean a lower limit
            ax.errorbar(Ilb[~lolims, 0]+chabshift+np.log10(0.7**2/h_scale**2), Ilb[~lolims, 1]+np.log10(h_scale**3/0.7**3), yerr=[Ilb[~lolims, 3], Ilb[~lolims, 2]],
                        marker=mrk['Ilb'][0], ls='none', c=colour1, mfc=colour1, mec='none', ms=mrk['Ilb'][1], capsize=mrk['Ilb'][2], label="Ilbert+ 2013 [2.0$<$z$<$2.5]")
            ax.errorbar(Ilb[lolims, 0]+chabshift+np.log10(0.7**2/h_scale**2), Ilb[lolims, 1]+np.log10(h_scale**3/0.7**3), yerr=0.15,
                        lolims=True, marker=mrk['Ilb'][0], ls='none', c=colour1, mfc=colour1, mec='none', ms=mrk['Ilb'][1], capsize=mrk['Ilb'][2])
        if z > 2.0-d and z < 2.5+d:
            Muzz = np.loadtxt(obsdir+"Muzzin_2013_Vmax_2.0z2.5.dat")
            ax.errorbar(Muzz[:, 0]+kroupashift+np.log10(0.7**2/h_scale**2), Muzz[:, 2]+np.log10(h_scale**3/0.7**3), yerr=[Muzz[:, 4], Muzz[:, 3]], marker=mrk['Muzz']
                        [0], ls='none', c=colour1, mfc=colour1, mec='none', ms=mrk['Muzz'][1], capsize=mrk['Muzz'][2], label="Muzzin+ 2013 [2.0$<$z$<$2.5]")
        if z > 2.5-d and z < 3.0+d and colour2 is not None:
            Muzz = np.loadtxt(obsdir+"Muzzin_2013_Vmax_2.5z3.0.dat")
            ax.errorbar(Muzz[:, 0]+kroupashift+np.log10(0.7**2/h_scale**2), Muzz[:, 2]+np.log10(h_scale**3/0.7**3), yerr=[Muzz[:, 4], Muzz[:, 3]], marker=mrk['Muzz']
                        [0], ls='none', c=colour2, mfc=colour2, mec='none', ms=mrk['Muzz'][1], capsize=mrk['Muzz'][2], label="Muzzin+ 2013 [2.5$<$z$<$3.0]")

            Ilb = np.loadtxt(obsdir+"Ilbert2013/MF_Vmax_All_z2.5_3.0.dat")
            lolims = (Ilb[:, 2] < 0.0)  # negative errors mean a lower limit
            Ilb[lolims, 3] = 0.0  # no lower error bar, just upwards arrow
            Ilb[lolims, 2] = 0.15  # sets size of arrow
            ax.errorbar(Ilb[:, 0]+chabshift+np.log10(0.7**2/h_scale**2), Ilb[:, 1]+np.log10(h_scale**3/0.7**3), yerr=[Ilb[:, 3], Ilb[:, 2]], lolims=lolims,
                        marker=mrk['Ilb'][0], ls='none', c=colour2, mfc=colour2, mec='none', ms=mrk['Ilb'][1], capsize=mrk['Ilb'][2], label="Ilbert+ 2013 [2.5$<$z$<$3.0]")
        # if z>=1.8 and z<2.5:
            # San = np.genfromtxt(obsdir+"Santini_2012_z1.8-2.5.csv", delimiter=",")#1.8_<_z_<_2.5 taken from Ilbert 2013
            #ax.errorbar(San[:,0]+chabshift+np.log10(0.7**2/h_scale**2), San[:,1]+np.log10(h_scale**3/0.7**3), yerr=[np.absolute(San[:,3]), San[:,2]], marker='D', ls='none', c=colour, mfc=colour, mec='none', ms=ms)
        if z >= 3.0-d and z < 3.5+d:
            if not for_paper:
                Cap = np.genfromtxt(
                    obsdir+"UKIDSS_Caputi_2011.csv", delimiter=",")  # 3.0_<_z_<_3.5
                # increase by 0.093 (factor 1.24) to convert from BC07 template to BC03
                ax.errorbar(Cap[:, 0]+0.093+salpshift+np.log10(0.7**2/h_scale**2), Cap[:, 1]+np.log10(h_scale**3/0.7**3), yerr=[np.absolute(
                    Cap[:, 3]), Cap[:, 2]], marker='o', ls='none', c=colour1, mfc=colour1, mec='none', ms=ms, label="Caputi+ 2011 [3.0$<$z$<$3.5]")
            # data = np.loadtxt(obsdir+"SMF_Genel_data_z3.txt", skiprows=1)##Santini 2012 2.5_<_z_<_3.5
            #ax.errorbar(data[:,0]+2*np.log10(0.704/h_scale), data[:,1]+3*np.log10(h_scale/0.704), yerr=[data[:,2], data[:,3]], linestyle='none', marker='*', c=colour, mec='none')
        if z >= 3.0-d and z < 4.0+d:
            if z >= 3.0-d and z < 3.5+d:
                col = colour1
            else:
                col = colour2
            Ilb = np.loadtxt(obsdir+"Ilbert2013/MF_Vmax_All_z3.0_4.0.dat")
            lolims = (Ilb[:, 2] < 0.0)  # negative errors mean a lower limit
            Ilb[lolims, 3] = 0.0  # no lower error bar, just upwards arrow
            Ilb[lolims, 2] = 0.15  # sets size of arrow
            ax.errorbar(Ilb[:, 0]+chabshift+np.log10(0.7**2/h_scale**2), Ilb[:, 1]+np.log10(h_scale**3/0.7**3), yerr=[Ilb[:, 3], Ilb[:, 2]], lolims=lolims,
                        marker=mrk['Ilb'][0], ls='none', c=col, mfc=col, mec='none', ms=mrk['Ilb'][1], capsize=mrk['Ilb'][2], label="Ilbert+ 2013 [3.0$<$z$<$4.0]")

            Muzz = np.loadtxt(obsdir+"Muzzin_2013_Vmax_3.0z4.0.dat")
            ax.errorbar(Muzz[:, 0]+kroupashift+np.log10(0.7**2/h_scale**2), Muzz[:, 2]+np.log10(h_scale**3/0.7**3), yerr=[Muzz[:, 4], Muzz[:, 3]],
                        marker=mrk['Muzz'][0], ls='none', c=col, mfc=col, mec='none', ms=mrk['Muzz'][1], capsize=mrk['Muzz'][2], label="Muzzin+ 2013 [3.0$<$z$<$4.0]")

            Cap2015 = np.genfromtxt(
                obsdir+"Caputi_2015.csv", delimiter=",")  # 3.0_<_z_<_4.0
            lolims = np.isnan(Cap2015[:, 3])
            Cap2015[lolims, 2] = 0.2  # size of arrows on lower limit
            ax.errorbar(Cap2015[:, 0]+salpshift+np.log10(0.7**2/h_scale**2), Cap2015[:, 1]+np.log10(h_scale**3/0.7**3), yerr=[np.absolute(
                Cap2015[:, 3]), Cap2015[:, 2]], lolims=lolims, marker='o', ls='none', c=col, mfc=col, mec='none', ms=ms, label="Caputi+ 2011 [3.0$<$z$<$4.0]")
# ==============================================================================
#         if z>=4.0 and z<7.5:
#             assert int(np.round(z))!=0, "SMF_Genel_data_z0.txt not supported."
#             try:
#                 data = np.loadtxt(obsdir+"SMF_Genel_data_z"+str(int(np.round(z)))+".txt", skiprows=1)
#             except IOError:
#                 print "Could not find data for redshift z="+str(z)+" at "+obsdir+"SMF_Genel_data_z"+str(int(z))+".txt"
#             logm_data = data[:,0]+2*np.log10(0.704/h_scale)
#             logsmf_data = data[:,1]+3*np.log10(h_scale/0.704)
#             ax.errorbar(logm_data, logsmf_data, yerr=[data[:,2], data[:,3]], linestyle='none', marker='s', c=colour1, mec='none')#label="z="+str(z)
#
# ==============================================================================


class stellar_mass:
    def __init__(self, basedir, simdir, outdir, snapnums, obs_dir="/data/vault/nh444/ObsData/stellar_mass/", N=20, delta=500.0, ref="crit", h_scale=0.679, use_fof=True, snap_basename="snap", mpc_units=False, masstab=False, verbose=False):

        import os
        import sim_python.cosmo as cosmo

        self.basedir = os.path.dirname(basedir)+"/"
        if simdir[-1] != "/":
            self.simdir = simdir+"/"
        else:
            self.simdir = simdir
        self.outdir = os.path.dirname(outdir)+"/"
        if not os.path.exists(self.outdir+self.simdir):
            os.makedirs(self.outdir+self.simdir)
        self.snapnums = snapnums
        self.obs_dir = obs_dir
        self.N = N
        self.delta = delta
        self.ref = ref
        self.h_scale = h_scale
        self.use_fof = use_fof
        self.snap_basename = snap_basename
        self.mpc_units = mpc_units
        self.masstab = masstab
        self.verbose = verbose

        import readsnap as rs
        self.z = []
        for snapnum in snapnums:
            header = rs.snapshot_header(
                self.basedir + self.simdir + "snap_" + str(snapnum).zfill(3))
            self.z.append(header.redshift)
        self.h = header.hubble
        self.cm = cosmo.cosmology(header.hubble, header.omega_m)

        self.fout_base = "stellar_mass_M"+str(int(delta))+"_"

        self.salp2chab = 0.58  # Factor to multiply stellar masses to convert from
        # studies using Salpeter IMF to a Chabrier 2003 IMF - factor from
        # Madau 2014 "Cosmic SFH" (0.61) or Chiu 2016/Hilton 2013 (0.58)
        self.lin2chab = 0.76  # Factor to convert Lin 2003 stellar masses to
        # equivalent if using a Chabrier IMF (0.76 from Chiu 2016)
        self.gonz2chab = 0.76  # As above for Gonzalez 2014 paper

        self.colours = ["#d05352", "#65b97f", "#7979cf", "#be53be", "#9fd550",
                        "#c0933d", "#44c7e2", "#d9d458", "#ce568b", "#5c8532"]
        #from palettable.colorbrewer.qualitative import Dark2_4_r as pallette
        #self.colours = pallette.hex_colors
        #from palettable.colorbrewer.sequential import RdPu_4_r as pallette
        #self.colours = pallette.hex_colors
        #from palettable.cmocean.sequential import Dense_5 as pallette
        # self.colours=pallette.hex_colors[-4:]
        from palettable.colorbrewer.sequential import YlGnBu_6 as pallette
        self.colours = pallette.hex_colors[-4:]

        # as YlGnBu_5 but last colour is darker
        # self.colours = ['#A1DAB4', '#41B6C4', '#2C7FB8', '#1F2B7A']
        # self.colours = ['#41B6C4', '#2C7FB8', '#253494', '#1F2B7A']

        # self.colours = ['#95CEB5', '#50BFD7', '#287AC1', '#192473'] ## blues
        # self.colours = ['#44AA99', '#77AADD', '#332288', '#E7298A']
        # self.colours = ["#1395BA", "#0D3C55", "#ECB93E", "#F16C20"]##viget
        # self.colours = ["#F2BE40", "#D9501D", '#6395EC', '#4D4396'] ## orange-blue - combination of Viget and Hydrangea, slightly altered
        #from palettable.colorbrewer.sequential import PuRd_5_r as pallette
        # self.colours=pallette.hex_colors

    def get_SMF(self, dlogm=0.15, bcgs_only=False, calc_30kpc=True, verbose=False):
        # Note I've just converted an IDL script to Python, it ought to be optimised
        # If calc_30kpc, calculates stellar masses in a 30kpc aperture
        import numpy as np
        import snap
        import os
        if calc_30kpc:
            import readsnap as rs

        logm_min = 5.0  # Msun (no h)
        logm_max = 14.0
        nbins = int(np.round((logm_max - logm_min)/dlogm + 1))
        logm_bins = np.arange(nbins, dtype=np.float32)*dlogm + logm_min
        logm_binsp = np.arange(nbins-1, dtype=np.float32) * \
            dlogm + logm_min + 0.5*dlogm

        for j, snapnum in enumerate(self.snapnums):
            s = snap.snapshot(self.basedir+self.simdir, snapnum, extra_sub_props=True,
                              mpc_units=self.mpc_units, masstab=self.masstab)
            # bins in units of Msun/h
            cur_logm_bins = logm_bins + np.log10(self.h_scale)
            # h^-3 Mpc^3 (comoving volume)
            vol = (s.box_sidelength/1000.0)**3.0
            if not os.path.exists(self.outdir+self.simdir+"SMF/"):
                os.makedirs(self.outdir+self.simdir+"SMF/")

            if bcgs_only:  # only main subhalos
                outfile = self.outdir+self.simdir+"SMF/SMF_" + \
                    str(snapnum).zfill(3)+"_bcgs_only.txt"
                # Only consider groups with a subhalo
                ind = np.where((s.cat.group_firstsub > -1) &
                               (s.cat.group_firstsub < s.cat.nsubs))
                mstar = s.cat.sub_masstab[s.cat.group_firstsub[ind], 4] * 1e10
                mstar_inrad = s.cat.sub_massinradtab[s.cat.group_firstsub[ind], 4] * 1e10
                mstar_inVmaxrad = s.cat.sub_massinmaxradtab[s.cat.group_firstsub[ind], 4] * 1e10
                mstar_inSPrad = s.cat.sub_massinSPrad[s.cat.group_firstsub[ind]] * 1e10
                if calc_30kpc:
                    if verbose:
                        print "Loading masses, positions and stellar ages..."
                    # 30 kpc proper distance aperture converted to comoving distance
                    R = 30.0 * self.h_scale * (1 + s.header.redshift)
                    pos = rs.read_block(
                        s.snapname, "POS ", parttype=4, cosmic_ray_species=s.num_cr_species)
                    if s.mpc_units:
                        pos *= 1000.0
                    mass = rs.read_block(
                        s.snapname, "MASS", parttype=4, cosmic_ray_species=s.num_cr_species)
                    # GFM_StellarFormationTime
                    age = rs.read_block(s.snapname, "GAGE",
                                        cosmic_ray_species=s.num_cr_species)
                    mstar_30kpc = []
                    if verbose:
                        print "Calculating subhalo stellar masses within 30 kpc"
                    for subnum in s.cat.group_firstsub[ind]:
                        r = s.nearest_dist_3D(
                            pos[s.cat.sub_offsettab[subnum, 4]:s.cat.sub_offsettab[subnum, 4]+s.cat.sub_lentab[subnum, 4]], s.cat.sub_pos[subnum])
                        # GFM wind particles are star particles with formation time is <0, subfind counts these as gas when calculating subhalo mass table
                        ageind = np.where(
                            age[s.cat.sub_offsettab[subnum, 4]:s.cat.sub_offsettab[subnum, 4]+s.cat.sub_lentab[subnum, 4]] > 0.0)[0]
                        starind = s.cat.sub_offsettab[subnum, 4] + \
                            np.intersect1d(np.where(r <= R), ageind)
                        mstar_30kpc.append(np.sum(mass[starind]) * 1e10)
                    mstar_30kpc = np.array(mstar_30kpc)
            else:  # all subhalos
                outfile = self.outdir+self.simdir + \
                    "SMF/SMF_"+str(snapnum).zfill(3)+".txt"
                mstar = s.cat.sub_masstab[:, 4] * 1e10
                mstar_inrad = s.cat.sub_massinradtab[:, 4] * 1e10
                mstar_inVmaxrad = s.cat.sub_massinmaxradtab[:, 4] * 1e10
                mstar_inSPrad = s.cat.sub_massinSPrad * 1e10
                if calc_30kpc:
                    if verbose:
                        print "Loading masses, positions and stellar ages..."
                    # 30 kpc proper distance aperture converted to comoving distance in h^-1 kpc
                    R = 30.0 * self.h_scale * (1 + s.header.redshift)
                    pos = rs.read_block(
                        s.snapname, "POS ", parttype=4, cosmic_ray_species=s.num_cr_species)
                    if s.mpc_units:
                        pos *= 1000.0
                    mass = rs.read_block(
                        s.snapname, "MASS", parttype=4, cosmic_ray_species=s.num_cr_species)
                    # GFM_StellarFormationTime
                    age = rs.read_block(s.snapname, "GAGE",
                                        cosmic_ray_species=s.num_cr_species)
                    mstar_30kpc = []
                    if verbose:
                        print "Calculating subhalo stellar masses within 30 kpc"
                    # s.get_hostsubhalos()
                    #hostsubhalos = s.hostsubhalos[s.species_start[4]:s.species_end[4]]
                    #mstar_total = []
                    for subnum in range(s.cat.nsubs):
                        r = s.nearest_dist_3D(
                            pos[s.cat.sub_offsettab[subnum, 4]:s.cat.sub_offsettab[subnum, 4]+s.cat.sub_lentab[subnum, 4]], s.cat.sub_pos[subnum])
                        # GFM wind particles are star particles with formation time is <0, subfind counts these as gas when calculating subhalo mass table
                        ageind = np.where(
                            age[s.cat.sub_offsettab[subnum, 4]:s.cat.sub_offsettab[subnum, 4]+s.cat.sub_lentab[subnum, 4]] > 0.0)[0]
                        starind = s.cat.sub_offsettab[subnum, 4] + \
                            np.intersect1d(np.where(r <= R), ageind)
                        #starind = np.intersect1d(s.cat.sub_offsettab[subnum,4] + np.where(r <= R), ageind)
                        #starind = s.cat.sub_offsettab[subnum,4] + np.where(r <= R)
                        mstar_30kpc.append(np.sum(mass[starind]) * 1e10)
                        #starind = np.intersect1d(s.cat.sub_offsettab[subnum,4] + np.where(r <= np.inf), ageind)
                        #mstar_total.append(np.sum(mass[starind]) * 1e10)
                        #subind = np.where(hostsubhalos == subnum)[0]
                        #mstar_total.append(np.sum(mass[subind]) * 1e10)
                        #r = s.nearest_dist_3D(pos[subind], s.cat.sub_pos[subnum])
                        #mstar_30kpc.append(np.sum(mass[subind[np.where(r <= R)[0]]]) * 1e10)
                    mstar_30kpc = np.array(mstar_30kpc)
                    #mstar_total = np.array(mstar_total)

            #mstar = mstar[(mstar > 0)]
            #mstar_inrad = mstar_inrad[(mstar_inrad > 0)]
            #print "mstar =", mstar[:10]
            #print "mstar_total =", mstar_total[:10]
            #print "mstar_30kpc =", mstar_30kpc[:10]
            #print "mstar_30kpc / mstar =", mstar_30kpc[:20] / mstar[:20]
            #print "max(mstar_30kpc / mstar) =", np.nanmax(mstar_30kpc / mstar)
            #print "mstar / mstar_total =", mstar[:20] / mstar_total[:20]
            #print "mstar_30kpc / mstar_total =", mstar_30kpc[:20] / mstar_total[:20]
            #print "shape mstar =", np.shape(mstar)
            #print "shape mstar_30kpc =", np.shape(mstar_30kpc)
            n_total = np.zeros(nbins-1, dtype=np.float32)
            n_inrad = np.zeros(nbins-1, dtype=np.float32)
            n_inVmaxrad = np.zeros(nbins-1, dtype=np.float32)
            n_inSPrad = np.zeros(nbins-1, dtype=np.float32)
            n_in30kpc = np.zeros(nbins-1, dtype=np.float32)
            smf_total = np.zeros(nbins-1, dtype=np.float32)
            smf_inrad = np.zeros(nbins-1, dtype=np.float32)
            smf_inVmaxrad = np.zeros(nbins-1, dtype=np.float32)
            smf_inSPrad = np.zeros(nbins-1, dtype=np.float32)
            smf_in30kpc = np.zeros(nbins-1, dtype=np.float32)

            for i in range(0, nbins-2):
                n_total[i] = np.count_nonzero((np.log10(mstar) >= cur_logm_bins[i]) & (
                    np.log10(mstar) < cur_logm_bins[i+1]))
                n_inrad[i] = np.count_nonzero((np.log10(mstar_inrad) >= cur_logm_bins[i]) & (
                    np.log10(mstar_inrad) < cur_logm_bins[i+1]))
                n_inVmaxrad[i] = np.count_nonzero((np.log10(mstar_inVmaxrad) >= cur_logm_bins[i]) & (
                    np.log10(mstar_inVmaxrad) < cur_logm_bins[i+1]))
                n_inSPrad[i] = np.count_nonzero((np.log10(mstar_inSPrad) >= cur_logm_bins[i]) & (
                    np.log10(mstar_inSPrad) < cur_logm_bins[i+1]))
                if calc_30kpc:
                    n_in30kpc[i] = np.count_nonzero((np.log10(mstar_30kpc) >= cur_logm_bins[i]) & (
                        np.log10(mstar_30kpc) < cur_logm_bins[i+1]))
            smf_total = 1.0/dlogm/vol * n_total
            smf_inrad = 1.0/dlogm/vol * n_inrad
            smf_inVmaxrad = 1.0/dlogm/vol * n_inVmaxrad
            smf_inSPrad = 1.0/dlogm/vol * n_inSPrad
            smf_in30kpc = 1.0/dlogm/vol * n_in30kpc

            np.savetxt(outfile, np.c_[logm_binsp + np.log10(self.h_scale), smf_total, smf_inrad, smf_inVmaxrad, smf_inSPrad, smf_in30kpc, n_total, n_inrad, n_inVmaxrad, n_inSPrad, n_in30kpc], fmt=['%.5f', '%.7e', '%.7e', '%.7e', '%.7e',
                                                                                                                                                                                                     '%.7e', '%.7e', '%.7e', '%.7e', '%.7e', '%.7e'], header="logm_bins (Msun/h), smf_total (h^3), smf_inrad (h^3), smf_inVmaxrad (h^3), smf_inSPrad (h^3), smf_in30kpc (h^3), n_total, n_inrad, n_inVmaxrad, n_inSPrad, n_in30kpc")
            if verbose:
                print "Saved to", outfile

    def plot_SMF(self, plot="all", for_paper=False, bcgs_only=False, bcgs_and_total=True, lw=3,
                 chabrier=True, log=False, xlims=[10.0, 12.2], ylims=[-5, -2.0], outdir='default', verbose=False):
        """
        bcgs_only = plot SMF that includes only central galaxies (bcgs)
        bcgs_and_total = plot two SMFs - one for all subhalos and one bcgs only
        log - x and y are log values
        """
        import snap
        import matplotlib.pyplot as plt
        import numpy as np

        assert plot in ["all", "odd", "even"]

        if outdir == 'default':
            outdir = self.outdir+self.simdir+"SMF/"
        elif outdir[-1] != "/":
            outdir = outdir+"/"

        fig, ax = plt.subplots(figsize=(7, 7))
        if log:
            plt.xlim(xlims[0], xlims[1])
            plt.ylim(ylims[0], ylims[1])
            # , fontsize=14)
            plt.xlabel("log$_{10}$(M$_{\star}$ [M$_{\odot}$])", labelpad=10)
            # , fontsize=14)#"log($\Phi$) [Mpc$^{-3}$/log(M$_*$)]"
            plt.ylabel("log$_{10}$($\phi$ [Mpc$^{-3}$ dex$^{-1}$])")
        else:
            plt.xscale('log')
            plt.yscale('log')
            plt.xlim(1e10, 10**12.15)
            plt.ylim(1e-5, 1e-2)
            plt.xlabel("M$_{\star}$ [M$_{\odot}$]")  # , fontsize=14)
            # , fontsize=14)#"log($\Phi$) [Mpc$^{-3}$/log(M$_*$)]"
            plt.ylabel("$\phi$ [Mpc$^{-3}$ dex$^{-1}$]")
        #if not for_paper: plt.annotate(self.simdir[:-1].replace("_", "\_"), xy=(0, 0), xytext=(12, 20), va='top', xycoords='axes fraction', textcoords='offset points', fontsize=12)

        #ls=['dashdot', 'dotted', 'dashed', 'solid']
        #ls=['solid', 'dashed', 'dotted', 'dashdot']

        z = []
        for snapnum in self.snapnums:
            z.append(snap.snapshot(self.basedir+self.simdir, snapnum, mpc_units=self.mpc_units,
                                   masstab=self.masstab, read_params=False, header_only=True).header.redshift)

        if plot == "odd" or plot == "even":
            z = np.round(z)
            z = [int(x) for x in z]

            def iseven(i): return int(round(i)) % 2 == 0

            def whicheven(x): return [iseven(i) for i in x]
            ind = whicheven(z)

        if plot == "even":
            snapnums = [self.snapnums[i] for i, x in enumerate(ind) if x]
            z = [z[i] for i, x in enumerate(ind) if x]
            plotfile = outdir+"SMF_zeven_"+self.simdir[:-1]
        elif plot == "odd":
            snapnums = [self.snapnums[i] for i, x in enumerate(ind) if not x]
            z = [z[i] for i, x in enumerate(ind) if not x]
            plotfile = outdir+"SMF_zodd_"+self.simdir[:-1]
        else:
            snapnums = self.snapnums
            plotfile = outdir+"SMF_"+self.simdir[:-1]

        for j, snapnum in enumerate(snapnums):
            print "Plotting z =", z[j]
            if for_paper:
                label = "z={:.0f}".format(z[j])
            else:
                label = "z={:.1f}".format(z[j])
            if bcgs_and_total:
                SMF_all = np.loadtxt(
                    self.outdir+self.simdir+"SMF/SMF_"+str(snapnum).zfill(3)+".txt")
                SMF_bcgs = np.loadtxt(
                    self.outdir+self.simdir+"SMF/SMF_"+str(snapnum).zfill(3)+"_bcgs_only.txt")

                colour = self.colours[min(j, len(self.colours))]
                if log:
                    plt.plot(SMF_all[:, 0] - np.log10(self.h_scale), np.log10(SMF_all[:, 1] *
                                                                              self.h_scale**3), c=colour, ls='solid', lw=lw, dash_capstyle='round', label=label)
                    plt.plot(SMF_bcgs[:, 0] - np.log10(self.h_scale), np.log10(SMF_bcgs[:, 1]
                                                                               * self.h_scale**3), c=colour, ls='dashed', lw=lw, dash_capstyle='round')
                else:
                    plt.plot(10**SMF_all[:, 0] / self.h_scale, SMF_all[:, 1] * self.h_scale **
                             3, c=colour, ls='solid', lw=lw, dash_capstyle='round', label=label)
                    plt.plot(10**SMF_bcgs[:, 0] / self.h_scale, SMF_bcgs[:, 1] *
                             self.h_scale**3, c=colour, ls='dashed', lw=lw, dash_capstyle='round')

            else:
                if bcgs_only:
                    SMF = np.loadtxt(
                        self.outdir+self.simdir+"SMF/SMF_"+str(snapnum).zfill(3)+"_bcgs_only.txt")
                else:
                    SMF = np.loadtxt(self.outdir+self.simdir +
                                     "SMF/SMF_"+str(snapnum).zfill(3)+".txt")
                if log:
                    mbins = SMF[:, 0] - np.log10(self.h_scale)
                    smf_total = np.log10(SMF[:, 1] * self.h_scale**3)
                    smf_inrad = np.log10(SMF[:, 2] * self.h_scale**3)
                    #smf_in30kpc = np.log10(SMF[:,5] * self.h_scale**3)
                else:
                    mbins = 10**SMF[:, 0] / self.h_scale
                    smf_total = SMF[:, 1] * self.h_scale**3
                    smf_inrad = SMF[:, 2] * self.h_scale**3
                    #smf_in30kpc = SMF[:,5] * self.h_scale**3
                if verbose:
                    n_total = SMF[:, 6]
                    n_inrad = SMF[:, 7]
                    #n_in30kpc = SMF[:,10]
                    print "mbins =", mbins
                    print "n_total =", n_total
                    print "n_inrad =", n_inrad

                if j == 0:
                    print "Using colours..."
                    print self.colours
                colour = self.colours[min(j, len(self.colours))]
                ax.plot(mbins, smf_total, c=colour, lw=lw,
                        dash_capstyle='round', label=label, ls='solid')
                ax.plot(mbins, smf_inrad, c=colour, lw=lw,
                        dash_capstyle='round', ls='dashed')
                #ax.plot(mbins, smf_in30kpc, c=colour, lw=lw, dash_capstyle='round', ls='dashed')

            plot_SMF_observations(ax, z[j], colour1=colour, colour2=None, comp_sims=False,
                                  chabrier=chabrier, for_paper=for_paper, h_scale=self.h_scale)

        handles, labels = ax.axes.get_legend_handles_labels()
        n = len(snapnums)  # don't want to plot legend for all obs
        leg = ax.legend(handles[:n], labels[:n], loc='upper right',
                        frameon=False, borderaxespad=1, numpoints=1)

        if for_paper:
            plt.tight_layout(pad=0.45)
            plt.savefig(plotfile+".pdf")  # , bbox_inches='tight')
        else:
            fig.patch.set_facecolor('black')
            ax = plt.gca()
            ax.set_axis_bgcolor('black')
            ax.xaxis.label.set_color('white')
            ax.tick_params(axis='x', which='both', colors='white')
            ax.yaxis.label.set_color('white')
            ax.tick_params(axis='y', which='both', colors='white')
            for text in leg.get_texts():
                text.set_color("white")
            import matplotlib
            for child in ax.get_children():
                if isinstance(child, matplotlib.spines.Spine):
                    child.set_color('white')
            # facecolor=fig.get_facecolor(), edgecolor='none')
            plt.savefig(plotfile+".png", bbox_inches='tight', transparent=True)

        print "SMF saved to", plotfile
        return plotfile

    def get_stellar_masses(self, M500min=None, N=1, RminLowRes=0.0,
                           incl_wind=False, zaxis=0,
                           get_BCG_descendant=True,
                           use_kdTree=True, nthreads=4,
                           get_SFR=False, outsuffix="", verbose=True):
        """Calculate masses of different stellar components within r500.

        Calculate masses of different stellar components e.g. BCG, ICL,
        satellites within r500 for a sample of FoF groups defined either
        by a minimum M500 (M500min in Msun) or the N most massive.
        Args:
            M500min: Minimum halo mass of FoF group for sample. Overrides N.
            N: Use N most massive FoF groups for sample. Overridden by M500min.
            RminLowRes: Exclude FoF groups with Type 2 particles inside this
                        fraction of R500.
            incl_wind: Include wind particles when calculating stellar masses.
                       Warning: calculated masses won't match subfind
                       masses, since subfind subhalo stellar masses don't
                       count wind particles.
            get_BCG_descendant: get the subhalo number of the BCG's descendant
                                in the next snapshot.
            get_SFR: Also save the star formation rates of the gas in BCGs.
        """
        import numpy as np
        import snap
        import readsnap as rs
        import h5py
        from scipy.spatial import cKDTree
        from timeit import default_timer as timer
        import os

        fout_base = "stellar_masses_"

        snapnums = np.sort(self.snapnums)  # sort ascending
        for snapidx, snapnum in enumerate(snapnums):
            if verbose:
                print "Loading FoF catalogue..."
            sim = snap.snapshot(self.basedir+self.simdir, snapnum,
                                mpc_units=self.mpc_units, masstab=self.masstab,
                                extra_sub_props=get_SFR)
            z = sim.header.redshift
            h = sim.header.hubble
            if verbose:
                print "Loading masses and positions..."
            mass = rs.read_block(sim.snapname, "MASS", parttype=4,
                                 cosmic_ray_species=sim.num_cr_species)
            pos = rs.read_block(sim.snapname, "POS ", parttype=4,
                                cosmic_ray_species=sim.num_cr_species)
            if verbose:
                print "Getting host subhaloes.."
            sim.get_hostsubhalos()
            hostsubhalos = sim.hostsubhalos[sim.species_start[4]:
                                            sim.species_end[4]]
            if sim.mpc_units:
                pos *= 1000.0
            if not incl_wind:
                if verbose:
                    print "Loading stellar ages..."
                # GFM_StellarFormationTime
                age = rs.read_block(sim.snapname, "GAGE",
                                    cosmic_ray_species=sim.num_cr_species)
                # GFM wind particles are star particles with a formation
                # time <0, subfind counts these as gas when calculating
                # subhalo mass tables.
                ageind = np.where(age > 0.0)[0]
            else:
                ageind = np.arange(len(mass))
            mass = mass[ageind]
            pos = pos[ageind]
            hostsubhalos = hostsubhalos[ageind]
            if get_SFR:
                sim.load_gaspos()
                sim.load_sfr()

            axes = [0, 1, 2]  # axis indices
            # remove z axis index, use remaining two for projection axes
            axes.remove(zaxis)
            if use_kdTree:
                starttime = timer()
                print "Constructing k-d trees..."
                kdtree = cKDTree(pos, leafsize=5, boxsize=sim.box_sidelength)
                kdtree_2D = cKDTree(
                    pos[:, axes], leafsize=5, boxsize=sim.box_sidelength)
                print "Done. k-d trees for stars took", timer() - starttime, "seconds."
            if get_SFR:
                starttime = timer()
                print "Constructing k-d tree for gas..."
                kdtree_gas = cKDTree(sim.gaspos, leafsize=5, boxsize=sim.box_sidelength)
                print "Done. k-d tree for gas took", timer() - starttime, "seconds."
            if M500min is not None:
                grpind = np.where(sim.cat.group_m_crit500 *
                                  1e10/self.h_scale > M500min)[0]
            else:
                grpind = range(N)

            # Ignore groups with low res contamination
            if RminLowRes > 0:
                # Calculate distance of Type 2 particles
                sim.load_posarr(parttypes=[2])
                new_grpind = []
                for group in grpind:
                    # get distance of Type 2 particles relative to group
                    r = sim.nearest_dist_3D(sim.posarr['type2'],
                                            sim.cat.group_pos[group])
                    # keep group if nearest Type 2 is further
                    # than RminLowRes*R500 away
                    if np.min(r) > RminLowRes * sim.cat.group_r_crit500[group]:
                        new_grpind.append(group)
                # Make sure group 0 is in the list regardless of contamination
                if 0 not in new_grpind:
                    new_grpind.append(0)
                grpind = new_grpind

            # Total mass within r500 from subfind catalogue.
            M500 = sim.cat.group_m_crit500[grpind]*1e10
            # total mass of stars bound to group
            MstarTotal = np.zeros_like(M500)
            # total stellar mass within r500 of group
            Mstar_r500 = np.zeros_like(M500)
            # total stellar mass within projected r500 aperture
            Mstar_r500_proj = np.zeros_like(M500)
            # subhalo number of BCG
            BCG_subnum = np.zeros(len(M500), dtype=np.int32)
            # total mass of stars bound to the main subhalo (BCG)
            Mstar_BCG = np.zeros_like(M500)
            # total mass of stars bound to main subhalo within 3D r500
            Mstar_BCG_r500 = np.zeros_like(M500)
            # total mass of stars bound to main subhalo within
            # a 3D radius of twice the stellar half mass radius
            Mstar_BCG_inrad = np.zeros_like(M500)
            # mass of stars bound to main subhalo within 10 pkpc of its centre
            Mstar_BCG_10kpc = np.zeros_like(M500)
            # mass of stars bound to main subhalo within 30 pkpc of its centre
            Mstar_BCG_30kpc = np.zeros_like(M500)
            # mass of stars bound to main subhalo within 50 pkpc of its centre
            Mstar_BCG_50kpc = np.zeros_like(M500)
            # mass of stars bound to main subhalo within 100 pkpc of its centre
            Mstar_BCG_100kpc = np.zeros_like(M500)
            # mass of stars bound to main subhalo within
            # a 3D radius rcut as defined in Puchwein 2010 eq 1
            Mstar_BCG_rcut = np.zeros_like(M500)
            # total mass of stars bound to main subhalo within
            # 2D radius r500 of centre
            Mstar_BCG_r500_proj = np.zeros_like(M500)
            # mass of stars bound to main subhalo within
            # 10 pkpc circular aperture
            Mstar_BCG_10kpc_proj = np.zeros_like(M500)
            # mass of stars bound to main subhalo within
            # 30 pkpc circular aperture
            Mstar_BCG_30kpc_proj = np.zeros_like(M500)
            # mass of stars bound to main subhalo within
            # 50 pkpc circular aperture
            Mstar_BCG_50kpc_proj = np.zeros_like(M500)
            # mass of stars bound to main subhalo within
            # 100 pkpc circular aperture
            Mstar_BCG_100kpc_proj = np.zeros_like(M500)
            # mass of stars bound to main subhalo within
            # a 2D radius rcut as defined in Puchwein 2010 eq 1
            Mstar_BCG_rcut_proj = np.zeros_like(M500)
            # subhalo number of descdendant BCG in next snapshot
            BCG_subnum_next_snap = np.zeros(len(M500), dtype=np.int32)
            # total mass of star particles bound to subhalos in group
            Mstar_sats = np.zeros_like(M500)
            # total mass of star particles bound to subhalos
            # within r500 of group centre
            Mstar_sats_r500 = np.zeros_like(M500)
            # for all subhalos within r500 of group centre, sum the mass of
            # bound stars within twice the stellar half-mass radius
            Mstar_sats_r500_inrad = np.zeros_like(M500)
            # total mass of star particles bound to subhalos within
            # a projected radius r500 of group centre
            Mstar_sats_r500_proj = np.zeros_like(M500)
            # for all subhalos within a projected radius r500 of
            # group centre, sum the mass of bound stars within twice
            # the stellar half-mass radius.
            Mstar_sats_r500_proj_inrad = np.zeros_like(M500)
            if get_SFR:
                # total SFR of gas bound to main subhalo
                SFR_BCG_tot = np.zeros_like(M500)
                # SFR of gas bound to main subhalo within
                # a 3D radius of twice the stellar half mass radius.
                SFR_BCG_inrad = np.zeros_like(M500)
                # SFR of gas within X pkpc of its centre
                SFR_BCG_10kpc = np.zeros_like(M500)
                SFR_BCG_30kpc = np.zeros_like(M500)
                SFR_BCG_50kpc = np.zeros_like(M500)
                SFR_BCG_100kpc = np.zeros_like(M500)

            for i, group in enumerate(grpind):
                # take centre to be the group position since this is
                # where R500 and M500 are calculated.
                centre = sim.cat.group_pos[group]
                # total bound stellar mass
                MstarTotal[i] = sim.cat.group_masstab[group, 4] * 1e10

                # Calculate stellar mass within 3D and 2D aperture radius r500
                if use_kdTree:
                    # total stellar mass within 3D r500
                    kdind = kdtree.query_ball_point(
                                centre,
                                sim.cat.group_r_crit500[group],
                                n_jobs=nthreads)
                    Mstar_r500[i] = 1e10 * np.sum(mass[kdind])
                    # total stellar mass within 2D r500
                    kdind = kdtree_2D.query_ball_point(
                                centre[axes],
                                sim.cat.group_r_crit500[group],
                                n_jobs=nthreads)
                    Mstar_r500_proj[i] = (1e10 * np.sum(mass[kdind]))
                else:
                    # total stellar mass within r500
                    r_grp = sim.nearest_dist_3D(pos, centre)
                    ind = np.where(r_grp < sim.cat.group_r_crit500[group])[0]
                    Mstar_r500[i] = np.sum(mass[ind]) * 1e10

                    r2D = np.sqrt((sim.rel_pos(pos[:, axes[0]], centre))**2 +
                                  (sim.rel_pos(pos[:, axes[1]], centre))**2)
                    ind = np.where(r2D < sim.cat.group_r_crit500[group])[0]
                    Mstar_r500_proj[i] = 1e10 * np.sum(mass[ind])

                # Next get masses of the BCG for various definitions.
                # index of main subhalo (BCG)
                subnum = sim.cat.group_firstsub[group]
                BCG_subnum[i] = subnum
                # total stellar mass bound to main halo
                # (could extend beyond r500 of group)
                Mstar_BCG[i] = sim.cat.sub_masstab[subnum, 4] * 1e10
                # bound stellar mass within twice the stellar half mass radius
                Mstar_BCG_inrad[i] = sim.cat.sub_massinradtab[subnum, 4] * 1e10
                # only consider star particles bound to the subhalo 'subnum'
                ind = np.where(hostsubhalos == subnum)[0]
                if not incl_wind:
                    if (abs(np.sum(mass[ind]) * 1e10 - Mstar_BCG[i]) >
                        (1e-8 + 1e-5 * abs(Mstar_BCG[i]))):
                            raise ValueError("Subhalo mass and summed mass "
                                             "do not match.")
                if use_kdTree:
                    # Calculate BCG stellar masses within different radii
                    # within r500
                    kdind = kdtree.query_ball_point(
                        sim.cat.sub_pos[subnum],
                        sim.cat.group_r_crit500[group], n_jobs=nthreads)
                    if len(kdind) > 0:
                        Mstar_BCG_r500[i] = 1e10 * np.sum(
                            mass[np.intersect1d(ind, kdind,
                                                assume_unique=True)])
                    kdind = kdtree_2D.query_ball_point(
                        sim.cat.sub_pos[subnum, axes], sim.cat.group_r_crit500[group], n_jobs=nthreads)
                    if len(kdind) > 0:
                        Mstar_BCG_r500_proj[i] = np.sum(
                            mass[np.intersect1d(ind, kdind, assume_unique=True)]) * 1e10
                    # within 10 pkpc
                    # 30 kpc proper distance aperture converted to comoving distance in h^-1 kpc
                    kdind = kdtree.query_ball_point(
                        sim.cat.sub_pos[subnum], 10.0 * self.h_scale * (1 + sim.header.redshift), n_jobs=nthreads)
                    if len(kdind) > 0:
                        Mstar_BCG_10kpc[i] = np.sum(
                            mass[np.intersect1d(ind, kdind, assume_unique=True)]) * 1e10
                    # 30 kpc proper distance aperture converted to comoving distance in h^-1 kpc
                    kdind = kdtree_2D.query_ball_point(
                        sim.cat.sub_pos[subnum, axes], 10.0 * self.h_scale * (1 + sim.header.redshift), n_jobs=nthreads)
                    if len(kdind) > 0:
                        Mstar_BCG_10kpc_proj[i] = np.sum(
                            mass[np.intersect1d(ind, kdind, assume_unique=True)]) * 1e10
                    # within 30 pkpc
                    # 30 kpc proper distance aperture converted to comoving distance in h^-1 kpc
                    kdind = kdtree.query_ball_point(
                        sim.cat.sub_pos[subnum], 30.0 * self.h_scale * (1 + sim.header.redshift), n_jobs=nthreads)
                    if len(kdind) > 0:
                        Mstar_BCG_30kpc[i] = np.sum(
                            mass[np.intersect1d(ind, kdind, assume_unique=True)]) * 1e10
                    # 30 kpc proper distance aperture converted to comoving distance in h^-1 kpc
                    kdind = kdtree_2D.query_ball_point(
                        sim.cat.sub_pos[subnum, axes], 30.0 * self.h_scale * (1 + sim.header.redshift), n_jobs=nthreads)
                    if len(kdind) > 0:
                        Mstar_BCG_30kpc_proj[i] = np.sum(
                            mass[np.intersect1d(ind, kdind, assume_unique=True)]) * 1e10
                    # within 50 pkpc
                    kdind = kdtree.query_ball_point(
                        sim.cat.sub_pos[subnum], 50.0 * self.h_scale * (1 + sim.header.redshift), n_jobs=nthreads)
                    if len(kdind) > 0:
                        Mstar_BCG_50kpc[i] = np.sum(
                            mass[np.intersect1d(ind, kdind, assume_unique=True)]) * 1e10
                    kdind = kdtree_2D.query_ball_point(
                        sim.cat.sub_pos[subnum, axes], 50.0 * self.h_scale * (1 + sim.header.redshift), n_jobs=nthreads)
                    if len(kdind) > 0:
                        Mstar_BCG_50kpc_proj[i] = np.sum(
                            mass[np.intersect1d(ind, kdind, assume_unique=True)]) * 1e10
                    # within 100 pkpc
                    kdind = kdtree.query_ball_point(
                        sim.cat.sub_pos[subnum], 100.0 * self.h_scale * (1 + sim.header.redshift), n_jobs=nthreads)
                    if len(kdind) > 0:
                        Mstar_BCG_100kpc[i] = np.sum(
                            mass[np.intersect1d(ind, kdind, assume_unique=True)]) * 1e10
                    kdind = kdtree_2D.query_ball_point(
                        sim.cat.sub_pos[subnum, axes], 100.0 * self.h_scale * (1 + sim.header.redshift), n_jobs=nthreads)
                    if len(kdind) > 0:
                        Mstar_BCG_100kpc_proj[i] = np.sum(
                            mass[np.intersect1d(ind, kdind, assume_unique=True)]) * 1e10
                    # within rcut
                    # cutoff radius (ckpc/h) defined in Puchwein 2010 eq1
                    rcut = 27.3 * \
                        (sim.cat.group_m_crit200[group] /
                         1e5)**0.29 / sim.header.time
                    # 30 kpc proper distance aperture converted to comoving distance in h^-1 kpc
                    kdind = kdtree.query_ball_point(
                        sim.cat.sub_pos[subnum], rcut, n_jobs=nthreads)
                    if len(kdind) > 0:
                        Mstar_BCG_rcut[i] = np.sum(
                            mass[np.intersect1d(ind, kdind, assume_unique=True)]) * 1e10
                    # 30 kpc proper distance aperture converted to comoving distance in h^-1 kpc
                    kdind = kdtree_2D.query_ball_point(
                        sim.cat.sub_pos[subnum, axes], rcut, n_jobs=nthreads)
                    if len(kdind) > 0:
                        Mstar_BCG_rcut_proj[i] = np.sum(
                            mass[np.intersect1d(ind, kdind, assume_unique=True)]) * 1e10
                else:
                    rBCG = sim.nearest_dist_3D(
                        pos[ind], sim.cat.sub_pos[subnum])
                    Mstar_BCG_r500[i] = np.sum(
                        mass[ind[np.where(rBCG < sim.cat.group_r_crit500[group])]]) * 1e10
                    # 30 kpc proper distance aperture converted to comoving distance in h^-1 kpc
                    Mstar_BCG_10kpc[i] = np.sum(
                        mass[ind[np.where(rBCG < 10.0 * self.h_scale * (1 + sim.header.redshift))]]) * 1e10
                    # 30 kpc proper distance aperture converted to comoving distance in h^-1 kpc
                    Mstar_BCG_30kpc[i] = np.sum(
                        mass[ind[np.where(rBCG < 30.0 * self.h_scale * (1 + sim.header.redshift))]]) * 1e10
                    # 50 kpc proper distance aperture converted to comoving distance in h^-1 kpc
                    Mstar_BCG_50kpc[i] = np.sum(
                        mass[ind[np.where(rBCG < 50.0 * self.h_scale * (1 + sim.header.redshift))]]) * 1e10
                    # 50 kpc proper distance aperture converted to comoving distance in h^-1 kpc
                    Mstar_BCG_100kpc[i] = np.sum(
                        mass[ind[np.where(rBCG < 100.0 * self.h_scale * (1 + sim.header.redshift))]]) * 1e10
                    # cutoff radius (ckpc/h) defined in Puchwein 2010 eq1
                    rcut = 27.3 * \
                        (sim.cat.group_m_crit200[group] /
                         1e5)**0.29 / sim.header.time
                    Mstar_BCG_rcut[i] = np.sum(
                        mass[ind[np.where(rBCG < rcut)]]) * 1e10

                if get_SFR:
                    # total SFR of gas bound to main subhalo
                    SFR_BCG_tot[i] = sim.cat.sub_sfr[subnum]
                    # SFR of gas bound to main subhalo within
                    # a 3D radius of twice the stellar half mass radius.
                    SFR_BCG_inrad[i] = sim.cat.sub_sfrinrad[subnum]
                    # SFR of gas within 10 pkpc of main subhalo.
                    kdind = kdtree_gas.query_ball_point(
                            sim.cat.sub_pos[subnum], 10.0 * self.h_scale *
                            (1 + sim.header.redshift), n_jobs=nthreads)
                    if len(kdind) > 0:
                        SFR_BCG_10kpc[i] = np.sum(sim.sfr[kdind])
                    # SFR of gas within 30 pkpc of main subhalo.
                    kdind = kdtree_gas.query_ball_point(
                            sim.cat.sub_pos[subnum], 30.0 * self.h_scale *
                            (1 + sim.header.redshift), n_jobs=nthreads)
                    if len(kdind) > 0:
                        SFR_BCG_30kpc[i] = np.sum(sim.sfr[kdind])
                    # SFR of gas within 50 pkpc of main subhalo.
                    kdind = kdtree_gas.query_ball_point(
                            sim.cat.sub_pos[subnum], 50.0 * self.h_scale *
                            (1 + sim.header.redshift), n_jobs=nthreads)
                    if len(kdind) > 0:
                        SFR_BCG_50kpc[i] = np.sum(sim.sfr[kdind])
                    # SFR of gas within 100 pkpc of main subhalo.
                    kdind = kdtree_gas.query_ball_point(
                            sim.cat.sub_pos[subnum], 100.0 * self.h_scale *
                            (1 + sim.header.redshift), n_jobs=nthreads)
                    if len(kdind) > 0:
                        SFR_BCG_100kpc[i] = np.sum(sim.sfr[kdind])

                # Get the subhalo number of the BCG's descendant in the next snapshot
                if get_BCG_descendant and snapidx < len(snapnums)-1:  # don't do last snapshot
                    if i == 0:  # we haven't loaded sim_late yet
                        BCG_subnum_next_snap[i], sim_late = self.find_descendant_next_snapshot(
                            snapnum, subnum, sim_cur=sim, return_sim=True, satellite=False, verbose=verbose)
                    elif i > 0:  # we already have loaded sim_late
                        BCG_subnum_next_snap[i], sim_late = self.find_descendant_next_snapshot(
                            snapnum, subnum, sim_cur=sim, sim_late=sim_late, return_sim=True, satellite=False, verbose=verbose)

                # Total mass of all bound satellites
                Mstar_sats[i] = np.sum(
                    sim.cat.sub_masstab[subnum+1:subnum+sim.cat.group_nsubs[group], 4]) * 1e10

                # Get mass of satellites within 3D radius R500
                # 3D distances of all satellites
                r3D = sim.nearest_dist_3D(
                    sim.cat.sub_pos[subnum+1:subnum+sim.cat.group_nsubs[group]], centre)
                # satellites within r500
                subind = subnum + 1 + \
                    np.where(r3D < sim.cat.group_r_crit500[group])[0]
                Mstar_sats_r500[i] = np.sum(
                    sim.cat.sub_masstab[subind, 4]) * 1e10
                Mstar_sats_r500_inrad[i] = np.sum(
                    sim.cat.sub_massinradtab[subind, 4]) * 1e10

                # Get mass of satellites within a projected aperture of radius R500
                rel_pos = sim.rel_pos(
                    sim.cat.sub_pos[subnum+1:subnum+sim.cat.group_nsubs[group]], centre)
                r2D = np.sqrt((rel_pos[:, axes[0]]) **
                              2 + (rel_pos[:, axes[1]])**2)
                subind = subnum + 1 + \
                    np.where(r2D < sim.cat.group_r_crit500[group])[0]
                Mstar_sats_r500_proj[i] = np.sum(
                    sim.cat.sub_masstab[subind, 4]) * 1e10
                Mstar_sats_r500_proj_inrad[i] = np.sum(
                    sim.cat.sub_massinradtab[subind, 4]) * 1e10

                print "Group", group, "({:d}/{:d})".format(i+1, len(grpind))
                if verbose:
                    print "pos =", centre
                    print "M500 =", sim.cat.group_m_crit500[group]
                    print "R500 =", sim.cat.group_r_crit500[group]
                    print "MstarTotal =", MstarTotal[i]
                    print "Mstar_r500 =", Mstar_r500[i]
                    print "Mstar_r500_proj =", Mstar_r500_proj[i]
                    print "BCG_subnum =", BCG_subnum[i]
                    print "Mstar_BCG =", Mstar_BCG[i]
                    print "Mstar_BCG_r500 =", Mstar_BCG_r500[i]
                    print "Mstar_BCG_inrad =", Mstar_BCG_inrad[i]
                    print "Mstar_BCG_10kpc =", Mstar_BCG_10kpc[i]
                    print "Mstar_BCG_30kpc =", Mstar_BCG_30kpc[i]
                    print "Mstar_BCG_50kpc =", Mstar_BCG_50kpc[i]
                    print "Mstar_BCG_100kpc =", Mstar_BCG_100kpc[i]
                    print "Mstar_BCG_rcut =", Mstar_BCG_rcut[i]
                    print "BCG_subnum_next_snap =", BCG_subnum_next_snap[i]
                    print "rcut =", rcut
                    print "Mstar_sats =", Mstar_sats[i]
                    print "Mstar_sats_r500 =", Mstar_sats_r500[i]
                    print "Mstar_sats_r500_inrad =", Mstar_sats_r500_inrad[i]
                    print "Mstar_sats_r500_proj =", Mstar_sats_r500_proj[i]
                    print "Mstar_sats_r500_proj_inrad =", Mstar_sats_r500_proj_inrad[i]
                    print "Mstar_ICL =", Mstar_BCG[i] - \
                        Mstar_BCG_30kpc[i], Mstar_BCG[i] - Mstar_BCG_rcut[i]
                    print "Mstar_ICL/Mstar_BCG =", (Mstar_BCG[i] - Mstar_BCG_30kpc[i]) / \
                        Mstar_BCG[i], (Mstar_BCG[i] -
                                       Mstar_BCG_rcut[i]) / Mstar_BCG[i]
                    print "Mstar_ICL/MstarTotal =", (Mstar_BCG[i] - Mstar_BCG_30kpc[i]) / \
                        MstarTotal[i], (Mstar_BCG[i] -
                                        Mstar_BCG_rcut[i]) / MstarTotal[i]
                    print
            # np.savetxt(self.outdir+self.simdir+fout_base+str(snapnum).zfill(3)+".txt", np.c_[grpind, M500, sim.cat.group_r_crit500[grpind], sim.cat.group_m_crit200[grpind], MstarTotal, Mstar_r500, Mstar_BCG, Mstar_BCG_30kpc, Mstar_BCG_rcut, Mstar_sats, Mstar_sats_r500],
                    # fmt=['%d','%.5e','%f','%.5e','%.5e','%.5e','%.5e','%.5e','%.5e','%.5e','%.5e'],
                    # header="grpnum, Mstar_500[Msun/h], R_500[kpc/h], Mstar_500[Msun/h], MstarTotal[Msun/h], Mstar_r500[Msun/h], Mstar_BCG[Msun/h], Mstar_BCG_30kpc[Msun/h], Mstar_BCG_rcut[Msun/h], Mstar_sats[Msun/h], Mstar_sats_r500[Msun/h]")
            # with h5py.File(self.outdir+self.simdir+fout_base+str(snapnum).zfill(3)+".hdf5", "w") as f:
            suffix = "{:.2f}".format(z)
            if not os.path.exists(self.outdir+self.simdir):
                os.makedirs(self.outdir+self.simdir)
            outfile = self.outdir+self.simdir+fout_base+"z"+suffix.zfill(5)+outsuffix+".hdf5"
            with h5py.File(outfile, "w") as f:
                f.attrs['redshift'] = z
                f.attrs['h'] = h
                f.attrs['zaxis'] = zaxis
                f.attrs['MassUnits'] = "Msun/h"
                f.attrs['DistUnit'] = "kpc/h"

                f.create_dataset("GroupNum", data=np.c_[grpind])
                f.create_dataset("M500", data=np.c_[M500])
                f.create_dataset("R500", data=np.c_[
                                 sim.cat.group_r_crit500[grpind]])
                f.create_dataset("M200", data=np.c_[
                                 sim.cat.group_m_crit200[grpind] * 1e10])
                f.create_dataset("R200", data=np.c_[
                                 sim.cat.group_r_crit200[grpind]])
                f.create_dataset("MstarTotal", data=np.c_[MstarTotal])
                f.create_dataset("Mstar_r500", data=np.c_[Mstar_r500])
                f.create_dataset("Mstar_r500_proj",
                                 data=np.c_[Mstar_r500_proj])
                f.create_dataset("BCG_subnum", data=np.c_[BCG_subnum])
                f.create_dataset("Mstar_BCG", data=np.c_[Mstar_BCG])
                f.create_dataset("Mstar_BCG_r500", data=np.c_[Mstar_BCG_r500])
                f.create_dataset("Mstar_BCG_inrad",
                                 data=np.c_[Mstar_BCG_inrad])
                f.create_dataset("Mstar_BCG_10kpc",
                                 data=np.c_[Mstar_BCG_10kpc])
                f.create_dataset("Mstar_BCG_30kpc",
                                 data=np.c_[Mstar_BCG_30kpc])
                f.create_dataset("Mstar_BCG_50kpc",
                                 data=np.c_[Mstar_BCG_50kpc])
                f.create_dataset("Mstar_BCG_100kpc",
                                 data=np.c_[Mstar_BCG_100kpc])
                f.create_dataset("Mstar_BCG_rcut", data=np.c_[Mstar_BCG_rcut])
                f.create_dataset("Mstar_BCG_r500_proj",
                                 data=np.c_[Mstar_BCG_r500_proj])
                f.create_dataset("Mstar_BCG_10kpc_proj",
                                 data=np.c_[Mstar_BCG_10kpc_proj])
                f.create_dataset("Mstar_BCG_30kpc_proj",
                                 data=np.c_[Mstar_BCG_30kpc_proj])
                f.create_dataset("Mstar_BCG_50kpc_proj",
                                 data=np.c_[Mstar_BCG_50kpc_proj])
                f.create_dataset("Mstar_BCG_100kpc_proj",
                                 data=np.c_[Mstar_BCG_100kpc_proj])
                f.create_dataset("Mstar_BCG_rcut_proj",
                                 data=np.c_[Mstar_BCG_rcut_proj])
                f.create_dataset("BCG_subnum_next_snap",
                                 data=np.c_[BCG_subnum_next_snap])
                f.create_dataset("Mstar_sats", data=np.c_[Mstar_sats])
                f.create_dataset("Mstar_sats_r500",
                                 data=np.c_[Mstar_sats_r500])
                f.create_dataset("Mstar_sats_r500_inrad",
                                 data=np.c_[Mstar_sats_r500_inrad])
                f.create_dataset("Mstar_sats_r500_proj",
                                 data=np.c_[Mstar_sats_r500_proj])
                f.create_dataset("Mstar_sats_r500_proj_inrad",
                                 data=np.c_[Mstar_sats_r500_proj_inrad])
                if get_SFR:
                    f.create_dataset("SFR_BCG_tot",
                                     data=np.c_[SFR_BCG_tot])
                    f.create_dataset("SFR_BCG_inrad",
                                     data=np.c_[SFR_BCG_inrad])
                    f.create_dataset("SFR_BCG_10kpc",
                                     data=np.c_[SFR_BCG_10kpc])
                    f.create_dataset("SFR_BCG_30kpc",
                                     data=np.c_[SFR_BCG_30kpc])
                    f.create_dataset("SFR_BCG_50kpc",
                                     data=np.c_[SFR_BCG_50kpc])
                    f.create_dataset("SFR_BCG_100kpc",
                                     data=np.c_[SFR_BCG_100kpc])

            print "Saved to", outfile, "\n"

    def show_sSFR_distribution(self):
        import numpy as np
        import snap
        import matplotlib.pyplot as plt

        fig, ax = plt.subplots()
        ax.set_xlabel("log(sSFR [yr$^{-1}$])")
        ax.set_ylabel("dP/dlog(sSFR)")
        for snapidx, snapnum in enumerate(self.snapnums):
            sim = snap.snapshot(self.basedir+self.simdir, snapnum,
                                extra_sub_props=True, mpc_units=self.mpc_units, masstab=self.masstab)
            #subind = sim.cat.group_firstsub
            subind = np.where((sim.cat.sub_massinradtab[:, 4] * 1e10 / self.h_scale > 10**9.7) & (
                sim.cat.sub_massinradtab[:, 4] * 1e10 / self.h_scale < 10**10.1))[0]
            # Msun / yr (no h since mass and time both are h^-1)
            SFR = sim.cat.sub_sfrinrad[subind]
            sSFR = SFR / (sim.cat.sub_massinrad[subind] * 1e10 / self.h_scale)
            bins = np.linspace(-13, -9, num=100)
            hist, bin_edges = np.histogram(
                np.log10(sSFR[sSFR > 0.0]), bins=bins, density=True)
            ax.step(bin_edges[1:], hist)

    def show_satellite_mass_distribution(self, groups, cumulative=False, fractional=False):
        """
        Show mass distribution of satellites in each group
        """
        import numpy as np
        import snap
        import matplotlib.pyplot as plt

        logm_min = 7.0  # Msun
        logm_max = 13.0
        dlogm = 0.15
        nbins = int(np.round((logm_max - logm_min)/dlogm + 1))
        bins = np.logspace(logm_min, logm_max, num=nbins)

        for snapidx, snapnum in enumerate(self.snapnums):
            sim = snap.snapshot(self.basedir+self.simdir, snapnum,
                                mpc_units=self.mpc_units, masstab=self.masstab)
            h = self.h_scale
            fig, ax = plt.subplots(figsize=(8, 8))
            ax.set_xscale('log')
            ax.set_yscale('log')

            for group in groups:
                # take centre to be the group position since this is where R500 and M500 are calculated
                centre = sim.cat.group_pos[group]
                # index of main subhalo (BCG)
                subnum = sim.cat.group_firstsub[group]
                BCGmass = sim.cat.sub_masstab[subnum, 4] * 1e10 / h
                rsub = sim.nearest_dist_3D(sim.cat.sub_pos, centre)
                subind = np.where(rsub < sim.cat.group_r_crit500[group])[0]
                # distance of bound satellites
                rsat = sim.nearest_dist_3D(
                    sim.cat.sub_pos[subnum+1:subnum+sim.cat.group_nsubs[group]], centre)
                satind = subnum + 1 + \
                    np.where(rsat < sim.cat.group_r_crit500[group])[0]
                totmass = BCGmass + \
                    np.sum(sim.cat.sub_masstab[satind, 4] * 1e10 / h)
                print "satmass all =", np.sum(
                    sim.cat.sub_masstab[subnum+1:subnum+sim.cat.group_nsubs[group], 4] * 1e10)
                print "satmass r500 =", np.sum(
                    sim.cat.sub_masstab[satind, 4] * 1e10)
                if fractional:
                    weights = sim.cat.sub_masstab[subind,
                                                  4] * 1e10 / h / totmass
                else:
                    weights = sim.cat.sub_masstab[subind, 4] * 1e10 / h
                ax.hist(sim.cat.sub_masstab[subind, 4] * 1e10 / h, weights=weights, cumulative=cumulative,
                        bins=bins, label="Grp "+str(group)+" (all subhalos)", histtype='step', lw=1.5)
                if fractional:
                    weights = sim.cat.sub_masstab[satind,
                                                  4] * 1e10 / h / totmass
                else:
                    weights = sim.cat.sub_masstab[satind, 4] * 1e10 / h
                ax.hist(sim.cat.sub_masstab[satind, 4] * 1e10 / h, weights=weights, cumulative=cumulative, bins=bins, ls='--',
                        label="Grp {:d} {:.0f} (sats)".format(group, sim.cat.group_m_crit500[group]), histtype='step', lw=1.5)

            ax.legend(loc='best')

    def show_satellite_mass_radius(self, groups, cumulative=True, fractional=False):
        """
        Plot satellite stellar mass vs radial distance from the group centre
        """
        import numpy as np
        import snap
        import matplotlib.pyplot as plt

        rbins = np.logspace(-2, 1, num=100)

        for snapidx, snapnum in enumerate(self.snapnums):
            sim = snap.snapshot(self.basedir+self.simdir, snapnum,
                                mpc_units=self.mpc_units, masstab=self.masstab)
            h = self.h_scale
            fig, ax = plt.subplots(figsize=(8, 8))
            ax.set_xscale('log')
            ax.set_yscale('log')
            ax.set_xlabel("r/r$_{500}$")
            ax.set_ylabel("M$_{\star}$ [M$_{\odot}$]")

            for group in groups:
                # take centre to be the group position since this is where R500 and M500 are calculated
                centre = sim.cat.group_pos[group]
                # index of main subhalo (BCG)
                subnum = sim.cat.group_firstsub[group]
                BCGmass = sim.cat.sub_masstab[subnum, 4] * 1e10 / h
                rsub = sim.nearest_dist_3D(sim.cat.sub_pos, centre)
                #subind = np.where(rsub < sim.cat.group_r_crit500[group])[0]
                # distance of bound satellites
                rsat = sim.nearest_dist_3D(
                    sim.cat.sub_pos[subnum+1:subnum+sim.cat.group_nsubs[group]], centre)
                satind = subnum + 1 + \
                    np.where(rsat < sim.cat.group_r_crit500[group])[0]
                totmass = BCGmass + \
                    np.sum(sim.cat.sub_masstab[satind, 4] * 1e10 / h)
                print "satmass all =", np.sum(
                    sim.cat.sub_masstab[subnum+1:subnum+sim.cat.group_nsubs[group], 4] * 1e10)
                print "satmass r500 =", np.sum(
                    sim.cat.sub_masstab[satind, 4] * 1e10)
                if fractional:
                    weights = sim.cat.sub_masstab[:, 4] * 1e10 / h / totmass
                else:
                    weights = sim.cat.sub_masstab[:, 4] * 1e10 / h
                ax.hist(rsub / sim.cat.group_r_crit500[group], weights=weights, cumulative=cumulative,
                        bins=rbins, label="Grp "+str(group)+" (all subhalos)", histtype='step', lw=1.5)
                if fractional:
                    weights = sim.cat.sub_masstab[subnum+1:subnum +
                                                  sim.cat.group_nsubs[group], 4] * 1e10 / h / totmass
                else:
                    weights = sim.cat.sub_masstab[subnum+1:subnum +
                                                  sim.cat.group_nsubs[group], 4] * 1e10 / h
                ax.hist(rsat / sim.cat.group_r_crit500[group], weights=weights, cumulative=cumulative,
                        bins=rbins, ls='--', label="Grp {:d} (sats)".format(group), histtype='step', lw=1.5)

            ax.legend(loc='best')

    def plot_mstarfrac_v_mass(self, for_paper=False):
        import numpy as np
        import matplotlib.pyplot as plt
        import matplotlib.ticker as ticker
        from matplotlib.backends.backend_pdf import PdfPages
        from stars.stellar_mass import chiu16_fstar_relation

        delta = self.delta
        N = self.N
        h_scale = self.h_scale
        obs_dir = self.obs_dir
        fout_base = self.fout_base
        z = self.z
        outfilename = self.outdir+self.simdir+"stellar_mass_fractions.pdf"

        gonz = np.genfromtxt(
            obs_dir+"Gonzalez_2014_stellar_gas_masses.csv", delimiter=",")
        chiu = np.genfromtxt(
            obs_dir+"Suhada2012_Chiu2016_XMM-BCS.csv", delimiter=",")
        chiu2 = np.genfromtxt(
            obs_dir+"Chiu_2016_baryon_fracs.csv", delimiter=",")
        krav = np.genfromtxt(
            obs_dir+"kravtsov_stellar_masses.csv", delimiter=",")
        gio = np.genfromtxt(obs_dir+"giodini2009_star_fracs.txt")
        lin = np.genfromtxt(obs_dir+"lin_mohr_table1.txt",
                            usecols=range(0, 19))
        van = np.genfromtxt(
            obs_dir+"van_der_burg_stellar_masses.csv", delimiter=",")
        #anders = np.genfromtxt(obs_dir+"Anderson2014_t1.csv", delimiter=",")

        with PdfPages(outfilename) as pdf:
            for j, snapnum in enumerate(self.snapnums):
                plt.figure(figsize=(12, 13))

                data = np.genfromtxt(
                    self.outdir+self.simdir+fout_base+str(snapnum).zfill(3)+".txt")
                M_delta = data[:, 1] * 1e10 / h_scale
                #R_delta = data[:,2]
                M_delta_stars = data[:, 3] * 1e10 / h_scale

                plt.plot(M_delta, M_delta_stars/M_delta, 'o', label=str(N) +
                         " most massive halos ("+self.simdir[8:-1]+")", mec='none', mfc='darkgreen')
                if not for_paper:
                    plt.title(self.simdir[8:-1]+"\n z = "+str(z[j]))
                plt.xlim(3e12, 1e15)
                plt.ylim(0.004, 0.7)
                plt.xscale('log')
                plt.yscale('log')
                plt.ylabel(
                    r"Stellar mass fraction within r$_{"+str(int(delta))+"}$", fontsize=16)
                plt.xlabel(r"M$_{"+str(int(delta)) +
                           "}$ [$h^{-1}$ M$_{\odot}$]", fontsize=16)

                # Set axis labels to decimal format not scientific when using log scale
                plt.gca().yaxis.set_major_formatter(ticker.FuncFormatter(lambda y, pos: (
                    '{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y), 0)))).format(y)))

                # Plot best-fit from Chiu 2016
                M1, M2 = plt.gca().get_xlim()
                f1, f2, chiu_label = chiu16_fstar_relation(M1, M2, z=z[j])
                plt.plot([M1, M2], [f1, f2], label=chiu_label, c='0.8')

                if z[j] <= 0.15:
                    #y_anders = (10**((anders[5:,0]+anders[5:,1])/2.0))/(10**anders[5:,2])
                    #plt.errorbar(10**anders[5:,2]*0.704, y_anders, yerr=[abs(y_anders - (10**(anders[5:,0])/10**anders[5:,2])), abs(y_anders - (10**(anders[5:,0])/10**anders[5:,2]))], c='blue', marker='D', linestyle='none', ms=3, label="Anderson et al. 2014 (LBGs) 0.04 < z < 0.20")
                    plt.errorbar(gonz[:11, 14]*0.702/h_scale*1e14, gonz[:11, 29]*self.gonz2chab, xerr=[gonz[:11, 15]*0.702/h_scale*1e14, gonz[:11, 15]*0.702/h_scale*1e14], yerr=[gonz[:11, 30] *
                                                                                                                                                                                  self.gonz2chab, abs(gonz[:11, 31]*self.gonz2chab)], c='#47d147', mec='none', marker='s', linestyle='none', ms=3, label="Gonzalez et al. 2014 (BCG + sats + ICL) 0.05 < z < 0.12")
                    mask = (chiu[:, 3] < 0.15)
                    plt.errorbar(chiu[mask, 21]*1e13*0.7/h_scale, chiu[mask, 30]/chiu[mask, 21]/10, xerr=chiu[mask, 22]*1e13*0.7/h_scale, yerr=chiu[mask, 30]/chiu[mask, 21]/10*((chiu[mask, 31]/chiu[mask, 30])**2 + (
                        chiu[mask, 22]/chiu[mask, 21])**2)**0.5, c='#ff9980', mec='none', marker='D', linestyle='none', ms=3, label="Chiu, Saro, Mohr et al. 2016 (BCG + sats) 0.1 < z < 0.15")
                    plt.errorbar(krav[:, 5]*1e14*0.7/h_scale, (krav[:, 10]+krav[:, 13])/krav[:, 5]/100, yerr=(krav[:, 10]+krav[:, 13])/krav[:, 5]/100*(((krav[:, 11]**2+krav[:, 14]**2)**0.5/(
                        krav[:, 10]+krav[:, 13]))**2)**0.5, c='#00cc99', mec='none', marker='D', linestyle='none', ms=3, label="Kravtsov et al. 2014 (BCG + sats + ICL) z < 0.1")
                    plt.errorbar(2.55e13*(lin[:, 2]**1.58)*0.7/h_scale, lin[:, 16]*0.01*self.lin2chab, yerr=[(lin[:, 18])*0.01, (lin[:, 17])*0.01 *
                                                                                                             self.lin2chab], c='#ff9cff', mec='none', marker='D', linestyle='none', ms=3, label="Lin et al. 2003 (BCG + sats) 0.016 < z < 0.09")
                    #data3 = np.loadtxt("./prop_file_Z_0.3.txt")
                    #plt.plot(data3[:,21]*1.0e10, (data3[:,20]/data3[:,21]), marker='s', mfc='none', mec='green', ms=8, linestyle='none', mew=1, label="Output of scaling code for z=0.001")

                if z[j] >= 0.1 and z[j] <= 1.0:
                    plt.errorbar(gio[:, 0]*0.72/h_scale, gio[:, 3]*self.salp2chab, xerr=[(gio[:, 0]-gio[:, 1])*0.72/h_scale, (gio[:, 2]-gio[:, 0])*0.72/h_scale], yerr=[(gio[:, 3]-gio[:, 4]) *
                                                                                                                                                                        self.salp2chab, (gio[:, 5]-gio[:, 3])*self.salp2chab], c='#6ea3ff', mec='none', marker='^', linestyle='none', ms=5, label="Giodini et al. 2009 (BCG + sats) 0.1 < z < 1.0")
                if z[j] >= 0.1 and z[j] <= 0.5:
                    mask = (chiu[:, 3] < 0.5)
                    plt.errorbar(chiu[mask, 21]*1e13*0.7/h_scale, chiu[mask, 30]/chiu[mask, 21]/10, xerr=chiu[mask, 22]*1e13*0.7/h_scale, yerr=chiu[mask, 30]/chiu[mask, 21]/10*((chiu[mask, 31]/chiu[mask, 30])**2 + (
                        chiu[mask, 22]/chiu[mask, 21])**2)**0.5, c='#ff9980', mec='none', marker='D', linestyle='none', ms=3, label="Chiu, Saro, Mohr et al. 2016 (BCG + sats) 0.1 < z < 0.5")

                if z[j] >= 0.5 and z[j] <= 0.8:
                    mask = (chiu[:, 3] > 0.5) & (chiu[:, 3] < 0.8)
                    plt.errorbar(chiu[mask, 21]*1e13*0.7/h_scale, chiu[mask, 30]/chiu[mask, 21]/10, xerr=chiu[mask, 22]*1e13*0.7/h_scale, yerr=chiu[mask, 30]/chiu[mask, 21]/10*((chiu[mask, 31]/chiu[mask, 30])**2 + (
                        chiu[mask, 22]/chiu[mask, 21])**2)**0.5, c='#ff9980', mec='none', marker='D', linestyle='none', ms=3, label="Chiu, Saro, Mohr et al. 2016 (BCG + sats) 0.5 < z < 0.8")

                if z[j] >= 0.8 and z[j] <= 1.1:
                    mask = (chiu[:, 3] > 0.8)
                    plt.errorbar(chiu[mask, 21]*1e13*0.7/h_scale, chiu[mask, 30]/chiu[mask, 21]/10, xerr=chiu[mask, 22]*1e13*0.7/h_scale, yerr=chiu[mask, 30]/chiu[mask, 21]/10*((chiu[mask, 31]/chiu[mask, 30])**2 + (
                        chiu[mask, 22]/chiu[mask, 21])**2)**0.5, c='#ff9980', mec='none', marker='D', linestyle='none', ms=3, label="Chiu, Saro, Mohr et al. 2016 (BCG + sats) 0.8 < z < 1.0")

                if z[j] >= 0.6 and z[j] <= 1.4:
                    mask = (chiu2[:, 1] > 0.5)
                    plt.errorbar(chiu2[mask, 2]*1e14*0.683/h_scale, chiu2[mask, 10]/chiu2[mask, 2]/100, xerr=chiu2[mask, 3]*1e14*0.683/h_scale, yerr=chiu2[mask, 10]/chiu2[mask, 2]/100*((chiu2[mask, 11]/chiu2[mask, 2])**2 + (
                        chiu2[mask, 3]/chiu2[mask, 2])**2)**0.5, c='#bf80ff', mec='none', marker='D', linestyle='none', ms=3, label="Chiu, Mohr, McDonald et al. 2016 (BCG + sats) 0.6 < z < 1.4")

                if z[j] > 0.8 and z[j] < 1.4:
                    # Convert M200 to M500 by factor 0.631 as in paper
                    plt.errorbar(van[:, 7]*1e14*0.7*0.631/h_scale, van[:, 17]/(van[:, 7]*0.631)/100, xerr=[van[:, 8]*1e14*0.7*0.631/h_scale, abs(van[:, 9])*1e14*0.7*0.631/h_scale], yerr=van[:, 17]/(van[:, 7]*0.631)/100*(
                        (van[:, 18]/van[:, 17])**2+(van[:, 8]/van[:, 7])**2)**0.5, c='#00cc99', mec='none', marker='D', linestyle='none', ms=3, label="van der Burg et al. 2014 (BCG + sats) 0.86 < z 1.34")
                    # Plot best-fit from Chiu 2016
                    M1, M2 = plt.gca().get_xlim()  # /h_scale?
                    f1, f2, chiu_label = chiu16_fstar_relation(
                        M1, M2, z=0.9, sample="SPT")
                    plt.plot([M1, M2], [f1, f2], label=chiu_label,
                             c='0.8', linestyle='dashed')

                plt.legend(loc='upper left', frameon=False,
                           borderaxespad=1, numpoints=1, fontsize=14)
                if not for_paper:
                    pdf.savefig()
                else:
                    pdf.savefig(bbox_inches='tight')
                plt.close()
            print "Plot saved to", outfilename
        return outfilename

    def plot_mstarfrac_v_redshift(self, nplot=8):
        import numpy as np
        import matplotlib.pyplot as plt
        import matplotlib.ticker as ticker

        delta = self.delta
        fout_base = self.fout_base
        z = self.z

        chiu = np.genfromtxt(
            self.obs_dir+"Suhada2012_Chiu2016_XMM-BCS.csv", delimiter=",")

        fig = plt.figure()  # figsize=(10,10))
        fig.patch.set_facecolor('white')

        M500_max = 4.2
        mask = (chiu[:, 21] < M500_max)  # M500 mask
        plt.errorbar(chiu[mask, 3], chiu[mask, 30]/chiu[mask, 21]/10, xerr=[chiu[mask, 4], abs(chiu[mask, 5])], yerr=chiu[mask, 30]/chiu[mask, 21]/10*((chiu[mask, 31]/chiu[mask, 30])**2 + (chiu[mask, 22]/chiu[mask, 21])**2)
                     ** 0.5, c='#ff9980', mec='none', marker='D', linestyle='none', ms=6, label="Chiu, Saro, Mohr et al. 2016 (BCG + sats) "+str(min(chiu[mask, 21]))+" < M$_{500}$ (10$^{13}$M$_{\odot}$) < "+str(M500_max))

        for j, snapnum in enumerate(self.snapnums):
            data = np.genfromtxt(fout_base+str(snapnum).zfill(3)+".txt")
            M_delta = data[:, 1]
            #R_delta = data[:,2]
            M_delta_stars = data[:, 3]
            #mask = (M_delta > 1800)
            plt.plot(np.full_like(M_delta[:nplot], z[j]), M_delta_stars[:nplot]/M_delta[:nplot],
                     linestyle='none', marker=r"$\odot$", mfc='none', mec='green', ms=6, mew=0.6)

        plt.title(self.simdir[8:-1]+"\n z = "+str(z[j]))
        plt.xlim(0, 0.5)
        plt.ylim(0.004, 0.3)
        plt.yscale('log')
        plt.ylabel(
            r"Stellar mass fraction within r$_{"+str(int(delta))+"}$", fontsize=16)
        plt.xlabel("z", fontsize=16)

        # Set axis labels to decimal format not scientific when using log scale
        plt.gca().yaxis.set_major_formatter(ticker.FuncFormatter(lambda y, pos: (
            '{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y), 0)))).format(y)))
        plt.legend(loc='upper left', frameon=False,
                   borderaxespad=1, numpoints=1, fontsize=14)

        plotfile = self.outdir+self.simdir+"stellar_mass_fraction_v_redshift.pdf"
        plt.savefig(plotfile)
        return plotfile

    def plot_mstar_v_redshift(self):
        import numpy as np
        import matplotlib.pyplot as plt
        import matplotlib.ticker as ticker
        import sys
        import cosmo

        delta = self.delta
        fout_base = self.fout_base
        z = self.z
        assert (delta == 500.0), "Requires delta = 500.0 for computed masses."

        fig = plt.figure()  # figsize=(10,10))
        fig.patch.set_facecolor('white')

        chiu = np.genfromtxt(
            self.obs_dir+"Suhada2012_Chiu2016_XMM-BCS.csv", delimiter=",")
        van = np.genfromtxt(
            self.obs_dir+"van_der_burg_stellar_masses.csv", delimiter=",")
        h_chiu = cosmo.cosmology(0.7, 0.3).hubble_z(chiu[:, 3])
        h_van = cosmo.cosmology(0.7, 0.3).hubble_z(van[:, 1])
        M_pivot = 8e13
        B = 0.69  # scaling parameter from Chiu et al 2016
        plt.errorbar(chiu[:, 3], h_chiu*chiu[:, 30]*(chiu[:, 21]*1e13/M_pivot)**(-1.0*B), xerr=[chiu[:, 4], abs(chiu[:, 5])], yerr=h_chiu*chiu[:, 30]*(chiu[:, 21]*1e13/M_pivot)**(-1.0*B)*((chiu[:, 31]/chiu[:, 30])**2 + (B*chiu[:, 22]/chiu[:, 21])
                                                                                                                                                                                            ** 2)**0.5, c='#ff9980', mec='none', marker='D', linestyle='none', ms=6, label="Chiu et al. 2016 (BCG + sats)  "+'{:.1f}'.format(min(chiu[:, 21]))+" < M$_{500}$ (10$^{13} $M$_{\odot}$) < "+'{:.1f}'.format(max(chiu[:, 21])))
        # Convert M200 to M500 by factor 0.631 as in van der burg paper
        plt.errorbar(van[:, 1], van[:, 17]*h_van*(van[:, 7]*0.631*1e14/M_pivot)**(-1.0*B), yerr=van[:, 17]*h_van*(van[:, 7]*0.631*1e14/M_pivot)**(-1.0*B)*((van[:, 18]/van[:, 17])**2+(van[:, 8]/van[:, 7]*B)**2)**0.5, c='#00cc99',
                     mec='none', marker='D', linestyle='none', ms=3, label="van der Burg et al. 2014 (BCG + sats)  "+'{:.1f}'.format(min(van[:, 7]*0.631*10))+" < M$_{500}$ (10$^{13} $M$_{\odot}$) < "+'{:.1f}'.format(max(van[:, 7]*0.631*10)))

        M_min_obs = min(chiu[:, 21])*1e13  # lowest M500
        z_min = chiu[np.argmin(chiu[:, 21]), 3]  # redshift of lowest M500
        M_min = M_min_obs * cosmo.cosmology(0.7, 0.3).hubble_z(z_min)
        print z_min
        if min(z) > z_min:
            print "Need snapshot of redshift z<" + \
                str(z_min)+" for determining mass sample"
            sys.exit()
        else:
            snapnum = self.snapnums[z.index(min(z))]
            data = np.loadtxt(fout_base+str(snapnum).zfill(3)+".txt")
            mask = (data[:, 1]*1e10 > M_min)
            pos_match = np.stack(
                (data[mask, 4], data[mask, 5], data[mask, 6]), axis=1)

        for j, snapnum in enumerate(self.snapnums):
            data = np.genfromtxt(fout_base+str(snapnum).zfill(3)+".txt")
            #ID = data[:,0]
            M_delta = data[:, 1]
            #R_delta = data[:,2]
            M_delta_stars = data[:, 3]

            pos = np.stack(
                (data[mask, 4], data[mask, 5], data[mask, 6]), axis=1)
            # Create matrix of differences in positions
            pos_rel = (np.reshape(pos_match, (1, -1)) -
                       np.reshape(pos, (-1, 1)))
            ind = np.abs(pos_rel).argmin(axis=0)
            # This isn't ideal - will mean that first element is always included in mask below
            ind[len(pos_match):] = 0
            #print ind[:len(pos_match)]
            mask = np.zeros_like(M_delta, dtype=bool)  # boolean array of False
            mask[ind] = True
            print "Median M500 =", np.median(
                M_delta[mask]*1e10), "at z = ", z[j]

            plt.plot(np.full_like(M_delta[mask], z[j]), M_delta_stars[mask]*1e10/1e12*(M_delta[mask]*1e10/M_pivot)**(-1.0*B), linestyle='none', marker=r"$\odot$", mfc='none',
                     mec='blue', ms=6, mew=0.6, label="Halos of M$_{500}$ > "+'{:.1f}'.format(M_min_obs/1e13)+r" $\times$ 10$^{13} $M$_{\odot}$ at z = 0.0" if j == 0 else "")

        plt.title(self.simdir[8:-1])
        plt.xlim(0.05, 1.5)
        plt.ylim(0.2, 10)
        plt.xscale('log')
        plt.yscale('log')
        plt.ylabel(
            r"$M_{\star}(\frac{M_{500}}{M_{piv}})^{-0.69}$ [10$^{12}$ $h^{-1}$ M$_{\odot}$]", fontsize=16)
        plt.xlabel("z", fontsize=16)

        plt.axhline(y=M_pivot/1e14*0.7, linestyle='dashdot', c='black')
        plt.text(0.06, M_pivot/1e14*0.7+0.03,
                 "$f_{\star}$ $\simeq$ 0.01", fontsize=14)
        plt.axhline(y=M_pivot/1e13*0.7, linestyle='dashed', c='black')
        plt.text(0.06, M_pivot/1e13*0.7-0.7,
                 "$f_{\star}$ $\simeq$ 0.1", fontsize=14)

        # Set axis labels to decimal format not scientific when using log scale
        plt.gca().yaxis.set_major_formatter(ticker.FuncFormatter(lambda y, pos: (
            '{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y), 0)))).format(y)))
        plt.gca().xaxis.set_major_formatter(ticker.FuncFormatter(lambda y, pos: (
            '{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y), 0)))).format(y)))
        plt.legend(loc='upper left', frameon=False,
                   borderaxespad=1, numpoints=1, fontsize=13)

        plotfile = self.outdir+self.simdir+"stellar_mass_v_redshift.pdf"
        plt.savefig(plotfile)
        return plotfile

    def get_mstar_profiles(self, rmin=1e-3, rmax=None, rminType="R500", nbins=100,
                           M500min=None, M500max=None, N=0, RminLowRes=0.0,
                           calc_BCG_prof=True):
        """
        Saves stellar mass as a function of radius from the centre of groups for
        groups in the given mass range, or the first N groups if N>0
        Args:
            rmin and rmax: min. and max. radius, units are defined by rminType
            rminType: units of rmin and rmax: "R500" for units of r500, "pkpc" for proper kpc
            RminLowRes: Exclude FoF groups with Type 2 particles inside this
                        fraction of R500.
            calc_BCG_prof: calculate profile for stars bound to main halo only
        Notes:
            use_hdf5 has been removed, hdf5 is now always used
        """
        import snap
        import numpy as np
        import h5py
        import os

        assert rminType in [
            "R500", "pkpc"], "rminType must be one of ['R500', 'pkpc']"

        if M500min is None:
            M500min = 0.0
        if M500max is None:
            M500max = np.inf
        if rmax is None:
            if rminType == "R500":
                rmax = 5.0
            elif rminType == "pkpc":
                rmax = 1000.0

        for snapnum in self.snapnums:
            print "Starting snapshot", snapnum
            sim = snap.snapshot(self.basedir+self.simdir, snapnum,
                                mpc_units=self.mpc_units, masstab=self.masstab,
                                read_params=False)
            print "Loading masses..."
            sim.load_starmass()
            print "Loading positions..."
            sim.load_starpos()
            if calc_BCG_prof:  # determine parent subhalos
                print "Finding host subhalos..."
                sim.get_hostsubhalos()

            if N > 0:
                groups = range(0, N)
            else:  # M500min and M500max set above if given as None
                groups = np.where((sim.cat.group_m_crit500 * 1e10 /
                                   self.h_scale > M500min)
                                  & (sim.cat.group_m_crit500 * 1e10 /
                                     self.h_scale < M500max))[0]
            # Ignore groups with low res contamination
            if RminLowRes > 0:
                # Calculate distance of Type 2 particles
                sim.load_posarr(parttypes=[2])
                new_groups = []
                for group in groups:
                    # get distance of Type 2 particles relative to group
                    r = sim.nearest_dist_3D(sim.posarr['type2'],
                                            sim.cat.group_pos[group])
                    # keep group if nearest Type 2 is further
                    # than RminLowRes*R500 away
                    if np.min(r) > RminLowRes * sim.cat.group_r_crit500[group]:
                        new_groups.append(group)
                # Make sure group 0 is in the list regardless of contamination
                if 0 not in new_groups:
                    new_groups.append(0)
                groups = new_groups
            if self.verbose:
                print "Using groups", groups

            bins = np.logspace(np.log10(rmin), np.log10(
                rmax), num=nbins+1)  # r500 units

            z = sim.header.redshift
            suffix = "{:.2f}".format(z)
            if not os.path.exists(self.outdir+self.simdir):
                os.makedirs(self.outdir+self.simdir)
            outname = self.outdir+self.simdir + \
                "stellar_mass_profiles_z"+suffix.zfill(5)+".hdf5"
            print "Outputting to", outname
            with h5py.File(outname, "w") as f:
                f.attrs['rminType'] = rminType
                if rminType == "R500":
                    f.create_dataset('r_r500', data=bins)
                elif rminType == "pkpc":
                    f.create_dataset('r_pkpc', data=bins)
                f.attrs['redshift'] = z
                f.attrs['h'] = sim.header.hubble

                for k, group in enumerate(groups):
                    if self.verbose:
                        print "Doing group", group, " ({:d}/{:d})".format(
                            k+1, len(groups))

                    # calculate profile:
                    centre = sim.cat.group_pos[group]
                    rel_pos = sim.rel_pos(sim.starpos, centre)
                    if rminType == "R500":
                        rel_pos /= sim.cat.group_r_crit500[group]
                    elif rminType == "pkpc":
                        rel_pos /= sim.header.hubble * (1+z)
                    # 3-D radial distance
                    r = np.sqrt((rel_pos**2).sum(axis=rel_pos.ndim-1))
                    mstar3D, bin_edges = np.histogram(
                        r, bins=bins, weights=sim.starmass)
                    del r
                    # projected distance along z
                    r_proj = np.sqrt(rel_pos[:, 0]**2 + rel_pos[:, 1]**2)
                    mstar_proj, bin_edges = np.histogram(
                        r_proj, bins=bins, weights=sim.starmass)
                    del r_proj
                    del rel_pos

                    if calc_BCG_prof:  # calculate profile for only subhalo 0 stars
                        subnum = sim.cat.group_firstsub[group]
                        hostsubhalos = sim.hostsubhalos[sim.species_start[4]
                            :sim.species_end[4]]
                        ind = np.where(hostsubhalos == subnum)[0]
                        pos = sim.starpos[ind]
                        mass = sim.starmass[ind]
                        rel_pos = sim.rel_pos(pos, sim.cat.sub_pos[subnum])
                        if rminType == "R500":
                            rel_pos /= sim.cat.group_r_crit500[group]
                        elif rminType == "pkpc":
                            rel_pos /= sim.header.hubble * (1+z)
                        # 3-D radial distance
                        r = np.sqrt((rel_pos**2).sum(axis=rel_pos.ndim-1))
                        mstarBCG, bin_edges = np.histogram(
                            r, bins=bins, weights=mass)
                        del r
                        # projected distance along z
                        r_proj = np.sqrt(rel_pos[:, 0]**2 + rel_pos[:, 1]**2)
                        mstarBCG_proj, bin_edges = np.histogram(
                            r_proj, bins=bins, weights=mass)
                        del r_proj
                        del rel_pos

                    h5grp = f.create_group(str(group))
                    h5grp.create_dataset('mstar', data=mstar3D)
                    h5grp.create_dataset('mstar_proj', data=mstar_proj)
                    if calc_BCG_prof:
                        h5grp.create_dataset('mstar_main_halo', data=mstarBCG)
                        h5grp.create_dataset(
                            'mstar_proj_main_halo', data=mstarBCG_proj)
                    h5grp.attrs['R500'] = sim.cat.group_r_crit500[group]
                    h5grp.attrs['M500'] = sim.cat.group_m_crit500[group]
                    h5grp.attrs['R200'] = sim.cat.group_r_crit200[group]
                    h5grp.attrs['M200'] = sim.cat.group_m_crit200[group]
            del sim
            print "Profiles output to", outname

    def plot_mstar_profiles(self, nplot=0, rmax=0, bins=0):
        import numpy as np
        import matplotlib.pyplot as plt
        from matplotlib.backends.backend_pdf import PdfPages
        from scipy.interpolate import interp1d
        import matplotlib
        matplotlib.rcParams['lines.linewidth'] = 1.5

        outfile = "stellar_mass_profiles.pdf"
        print "Plotting to "+outfile+"..."
        with PdfPages(outfile) as pdf:
            for j, snapnum in enumerate(self.snapnums):
                plt.figure()  # figsize=(8,10))
                plt.xlabel("R (kpc)")
                plt.ylabel("M$_{\star}$(R) (10$^{10}$ M$_{\odot}$)")
                plt.title(self.simdir[8:-1]+"\nz = " +
                          '{:.3f}'.format(self.z[j]), fontsize=12)
                profiles = np.loadtxt(
                    "stellar_mass_profiles_"+str(snapnum).zfill(3)+".txt")
                if nplot == 0:
                    nplot = self.N
                if len(profiles[0, :]) < nplot:
                    print "WARNING: Requested", nplot, "halos but only", len(
                        profiles[0, :]), "have been computed using get_mstar_profiles()"
                    nplot = len(profiles[0, :])
                for i in np.arange(1, nplot*2 + 1, 2):
                    binleft = profiles[:, i]
                    bincent = (binleft[1:] + binleft[:-1]) / 2.0
                    values = profiles[:-1, i+1]
                    if bins > 0:
                        if rmax > 0:
                            r_int = np.linspace(
                                min(bincent), rmax, num=bins, endpoint=True)
                        else:
                            r_int = np.linspace(min(bincent), max(
                                bincent), num=bins, endpoint=True)
                        f = interp1d(bincent, values, kind='cubic')
                        plt.plot(r_int, f(r_int), label="Group "+str(int(i/2)))
                    else:
                        plt.plot(bincent, values, label="Group "+str(int(i/2)))
                    if rmax > 0:
                        plt.xlim(0.0, rmax)
                    plt.legend(loc='best', frameon=False,
                               borderaxespad=1, numpoints=1, fontsize=14)

                pdf.savefig()
        return outfile

    def get_ids_stellar_components(self, snapnum, group=0, cd_cutoff_method=0, cutoff_rad=30.0, include_fuzzy=False):
        """
        This function finds the IDs, positions and ages of stars belonging to the
        BCG, ICL or satellites in the given snapshot for the given group and
        saves them to self
        ## cutoff_method = method used to distinguish BCG
        ##               = 0 Straight cut of cutoff_rad around most massive subhalo
        """

        import snap
        import numpy as np

        self.group = group  # group ID
        self.snapnum_final = snapnum

        if cd_cutoff_method == 0:
            print "Using BCG cutoff_rad =", cutoff_rad, "kpc"
        else:
            assert False, "cd_cutoff_method=" + \
                str(cd_cutoff_method)+" not recognised."

        print "Loading sim..."
        sim = snap.snapshot(self.basedir+self.simdir, snapnum,
                            mpc_units=self.mpc_units, masstab=self.masstab, read_params=False)

        print "Loading IDs..."
        sim.load_starids()
        print "Loading star positions..."
        sim.load_starpos()
        print "Loading stellar ages..."
        sim.load_stellar_ages()
        print "Finding host subhalos..."
        sim.get_hostsubhalos()

        assert len(sim.age) == len(
            sim.starids), "Stars ID list is different length to stars ages list"
        hostsubhalos = sim.hostsubhalos[sim.species_start[4]
            :sim.species_end[4]]

        # Position of most massive subhalo in group - used for cutoff radius
        centerpos = sim.cat.sub_pos[sim.cat.group_firstsub[group]]
        # Position of group for finding it again later
        self.groupcentre = sim.cat.group_pos[group]

        # Get IDs of star particles in satellites (any subhalo except the first)
        gal_ind = np.where((hostsubhalos > sim.cat.group_firstsub[group]) & (
            hostsubhalos < sim.cat.group_firstsub[group] + sim.cat.group_nsubs[group]))[0]
        gal_ids = sim.starids[gal_ind]
        gal_ids_size = len(gal_ids)
        gal_age = sim.age[gal_ind]
        gal_pos = sim.starpos[gal_ind]
        #from collections import Counter
        #print "duplicates =", [k for k,v in Counter(gal_ids).items() if v>1]
        #print "gal_ids[(gal_age >= 0.07963846) & (gal_age <= 0.07963848)] = ", gal_ids[(gal_age >= 0.07963846) & (gal_age <= 0.07963848)]

        # Get IDs of star particles in BCG or ICL (ie. main subhalo)
        icl_ids = sim.starids[hostsubhalos == sim.cat.group_firstsub[group]]
        if include_fuzzy:
            print "Computing fuzzy ids..."
            if not hasattr(sim.cat, "group_offsettab"):
                icl_ids = np.concatenate((icl_ids, sim.ids[sim.cat.sub_offset[sim.cat.group_firstsub[group]+sim.cat.group_nsubs[group]-1] +
                                                           sim.cat.sub_len[sim.cat.group_firstsub[group]+sim.cat.group_nsubs[group]-1]:sim.cat.group_len[group]-1]))
            else:
                # Include star particles in group but no subhalo - may be zero
                grp_offset = sim.cat.group_offsettab.sum(axis=1)
                offset = grp_offset[group] + \
                    sim.cat.group_offsettab[group, :4].sum()
                # IDs of star particles in group
                grp_ids = sim.ids[offset:offset +
                                  sim.cat.group_lentab[group, 4]-1]
                # Not in a subhalo
                ind = (sim.hostsubhalos[offset:offset +
                                        sim.cat.group_lentab[group, 4]-1] == -1)
                icl_ids = np.concatenate((icl_ids, grp_ids[ind]))

        # get indices where starids are in icl_ids
        # set for quicker matching - does this affect icl_ids?
        matcher = set(icl_ids)
        ind = [i for i, x in enumerate(sim.starids) if x in matcher]
        icl_age = sim.age[ind]
        icl_pos = sim.starpos[ind]
        icl_rad = sim.nearest_dist_3D(icl_pos, centerpos)
        print "Stars in BCG + ICL =", len(icl_ids)

        # Exclude BCG particles to get ICL and BCG only IDs
        if cd_cutoff_method == 0:
            ind = np.where(icl_rad <= cutoff_rad)
            bcg_ids = icl_ids[ind]
            bcg_ids_size = len(bcg_ids)
            bcg_age = icl_age[ind]
            bcg_pos = icl_pos[ind]
            ind = np.where(icl_rad > cutoff_rad)
            icl_ids = icl_ids[ind]
            icl_ids_size = len(icl_ids)
            icl_age = icl_age[ind]
            icl_pos = icl_pos[ind]
        else:
            assert False, "cd_cutoff_method=" + \
                str(cd_cutoff_method)+" not recognised."

        # Save and sort by IDs
        gal_sort = np.argsort(gal_ids)
        bcg_sort = np.argsort(bcg_ids)
        icl_sort = np.argsort(icl_ids)
        self.gal_ids = gal_ids[gal_sort]
        self.gal_age = gal_age[gal_sort]
        self.gal_pos = gal_pos[gal_sort]
        self.bcg_ids = bcg_ids[bcg_sort]
        self.bcg_age = bcg_age[bcg_sort]
        self.bcg_pos = bcg_pos[bcg_sort]
        self.icl_ids = icl_ids[icl_sort]
        self.icl_age = icl_age[icl_sort]
        self.icl_pos = icl_pos[icl_sort]

        # Create sets of the IDs (for quicker matching)
        self.match_gal = set(self.gal_ids)
        self.match_bcg = set(self.bcg_ids)
        self.match_icl = set(self.icl_ids)

        self.cutoff_rad = cutoff_rad

        print "gal stars =", gal_ids_size
        print "bcg stars =", bcg_ids_size
        print "icl stars =", icl_ids_size
        print "gal star fraction =", float(
            gal_ids_size) / (gal_ids_size+bcg_ids_size+icl_ids_size)
        print "bcg star fraction =", float(
            bcg_ids_size) / (gal_ids_size+bcg_ids_size+icl_ids_size)
        print "icl star fraction =", float(
            icl_ids_size) / (gal_ids_size+bcg_ids_size+icl_ids_size)

        self.sim = sim  # needed for some functions

    def get_new_star_props(self, snapnum_min=0, check_before=0, include_fuzzy=False, get_dist=True, outsuffix="", new_thresh=0.0):
        """
        Runs the function get_new_star_props_cursnap() on each snapshot from 'snapnum_min'
        to 'self.snapnum_final' (defined in function get_ids_stellar_components(), which must
        be run before this function). We save to a HDF5 file the IDs, positions, masses etc.
        of stars in each snapshot which later become part of the BCG/ICL/satellites 
        in the snapshot 'snapnum_final'
        """
        import numpy as np
        import h5py

        assert hasattr(
            self, "gal_ids"), "Need IDs of stellar components - have you run get_ids_stellar_components?"
        assert hasattr(
            self, "snapnum_final"), "Need snapnum_final - have you run get_ids_stellar_components?"

        filepath = self.outdir+self.simdir+"new_star_props_cb" + \
            str(check_before)+"_grp"+str(self.group)+outsuffix+".hdf5"
        with h5py.File(filepath, "w") as outfile:
            outfile.attrs['group_final'] = self.group
            outfile.attrs['groupcentre_final'] = self.groupcentre
            outfile.attrs['snapnum_final'] = self.snapnum_final
            outfile.attrs['check_before'] = check_before
            outfile.attrs['include_fuzzy'] = include_fuzzy
            if hasattr(self, "cutoff_rad"):
                outfile.attrs['cutoff_rad'] = self.cutoff_rad
            if new_thresh > 0.0:
                self.new_thresh = new_thresh
                outfile.attrs['new_thresh'] = self.new_thresh

            for snapnum in range(snapnum_min, self.snapnum_final+1):
                status = self.get_new_star_props_cursnap(
                    snapnum, check_before=check_before, include_fuzzy=include_fuzzy, get_dist=get_dist, new_thresh=new_thresh)
                if status is not False:
                    group = outfile.create_group("snap_"+str(snapnum).zfill(3))
                    group.attrs['group_cur'] = self.group_cur
                    group.attrs['groupcentre_cur'] = self.sim.cat.group_pos[self.group_cur]
                    group.attrs['redshift'] = self.sim.header.redshift
                    group.attrs['time'] = self.sim.header.time
                    group.attrs['hubble_z'] = self.h_cur
                    group.attrs['nall'] = self.sim.header.nall
                    group.attrs['M200'] = self.sim.cat.group_m_crit200[self.group_cur]
                    group.attrs['R200'] = self.sim.cat.group_r_crit200[self.group_cur]
                    group.attrs['M500'] = self.sim.cat.group_m_crit500[self.group_cur]
                    group.attrs['R500'] = self.sim.cat.group_r_crit500[self.group_cur]

                    group.create_dataset(
                        'cur_gal_ids', data=np.c_[self.cur_gal_ids])
                    group.create_dataset(
                        'cur_gal_pos', data=np.c_[self.cur_gal_pos])
                    group.create_dataset('cur_gal_mass', data=np.c_[
                                         self.cur_gal_mass])
                    group.create_dataset('cur_gal_hostsubhalo', data=np.c_[
                                         self.cur_gal_hostsubhalo])
                    group.create_dataset(
                        'cur_bcg_ids', data=np.c_[self.cur_bcg_ids])
                    group.create_dataset(
                        'cur_bcg_pos', data=np.c_[self.cur_bcg_pos])
                    group.create_dataset('cur_bcg_mass', data=np.c_[
                                         self.cur_bcg_mass])
                    group.create_dataset('cur_bcg_hostsubhalo', data=np.c_[
                                         self.cur_bcg_hostsubhalo])
                    group.create_dataset(
                        'cur_icl_ids', data=np.c_[self.cur_icl_ids])
                    group.create_dataset(
                        'cur_icl_pos', data=np.c_[self.cur_icl_pos])
                    group.create_dataset('cur_icl_mass', data=np.c_[
                                         self.cur_icl_mass])
                    group.create_dataset('cur_icl_hostsubhalo', data=np.c_[
                                         self.cur_icl_hostsubhalo])

                    group.create_dataset(
                        'gal_new_ids', data=np.c_[self.gal_new_ids])
                    group.create_dataset(
                        'gal_new_pos', data=np.c_[self.gal_new_pos])
                    group.create_dataset('gal_new_mass', data=np.c_[
                                         self.gal_new_mass])
                    group.create_dataset('gal_new_hostsubhalo', data=np.c_[
                                         self.gal_new_hostsubhalo])
                    if hasattr(self, "gal_new_rad"):
                        group.create_dataset(
                            'gal_new_rad', data=np.c_[self.gal_new_rad])
                    group.create_dataset(
                        'bcg_new_ids', data=np.c_[self.bcg_new_ids])
                    group.create_dataset(
                        'bcg_new_pos', data=np.c_[self.bcg_new_pos])
                    group.create_dataset('bcg_new_mass', data=np.c_[
                                         self.bcg_new_mass])
                    group.create_dataset('bcg_new_hostsubhalo', data=np.c_[
                                         self.bcg_new_hostsubhalo])
                    if hasattr(self, "bcg_new_rad"):
                        group.create_dataset(
                            'bcg_new_rad', data=np.c_[self.bcg_new_rad])
                    group.create_dataset(
                        'icl_new_ids', data=np.c_[self.icl_new_ids])
                    group.create_dataset(
                        'icl_new_pos', data=np.c_[self.icl_new_pos])
                    group.create_dataset('icl_new_mass', data=np.c_[
                                         self.icl_new_mass])
                    group.create_dataset('icl_new_hostsubhalo', data=np.c_[
                                         self.icl_new_hostsubhalo])
                    if hasattr(self, "icl_new_rad"):
                        group.create_dataset(
                            'icl_new_rad', data=np.c_[self.icl_new_rad])
        print "File output to", filepath

    def get_new_star_props_cursnap(self, snapnum, check_before=0, include_fuzzy=True, get_dist=False, new_thresh=0.0):
        """
        Find, for the given snapshot, the properties of:
            ## if check_before==0:
                the star particles in the current snapshot that end up in satellites,
                BCG or ICL in the snapshot 'snapnum_final' (which is defined in
                get_ids_stellar_components()), plus those which are formed
                between this snapshot and the previous one
            ## if check_before==1:
                the gas cells in the current snapshot that produce stars that
                end up in satellites, BCG or ICL in the snapshot 'snapnum_final'
                (which is defined in get_ids_stellar_components()), plus those
                which produce such a star between this snapshot and the next one
        ## include_fuzzy - whether particles in the main halo are considered or just subhalo particles
        ## get_dist - If true, calculate radial distances of new stars/gas
        """

        import snap
        import numpy as np

        # Get IDs of stars that will end up in galaxies, BCG or ICL
        assert hasattr(self, "gal_ids"), "Need IDs of satellite stars"
        assert hasattr(self, "bcg_ids"), "Need IDs of BCG stars"
        assert hasattr(self, "icl_ids"), "Need IDs of ICL stars"

        if include_fuzzy:
            host_thresh = -2
        else:
            host_thresh = -1

        # host_thresh = -3 ##include all particles (radii will be incorrect)

        sim = snap.snapshot(self.basedir+self.simdir, snapnum,
                            mpc_units=self.mpc_units, masstab=self.masstab, read_params=False)
        self.h_cur = self.cm.hubble_z(sim.header.redshift)
        print "Analysing snapshot", snapnum, "at z =", sim.header.redshift
        print "Loading stellar ages..."
        try:
            sim.load_stellar_ages()
        except SystemExit:
            print "\033[93mSkipping:\033[0m sim", snapnum
            return False
        print "Finding host subhalos..."
        try:
            sim.get_hostsubhalos()
        except AttributeError:  # If 'cat' missing
            print "\033[93mSkipping:\033[0m sim", snapnum
            return False

        time = sim.header.time

        # I need to find the progenitor of the final group in this snapshot
        # I tried the following but it tends to pick small groups close to
        # the position of the final group
        # Only consider 2% of the groups either side of group in snapnum_final
        #self.group_cur = np.argmin(sim.nearest_dist_3D(sim.cat.group_pos[max(0,int(self.group-sim.cat.ngroups*0.02)):min(int(self.group+sim.cat.ngroups*0.02), sim.cat.ngroups),:], self.groupcentre))
        # For now let's just use the same group index as the final group
        # I will need to construct some form of halo tracker to do it properly
        self.group_cur = self.group

        centerpos = sim.cat.group_pos[self.group_cur]
        if check_before == 0 and snapnum > 0:
            print "Finding new stars by first snapshot after formation..."
            print "Loading IDs..."
            sim.load_starids()
            print "Loading star positions..."
            sim.load_starpos()
            print "Loading stellar masses..."
            sim.load_starmass()
            assert len(sim.age) == len(
                sim.starids), "Stars ID list is different length to stars ages list"
            hostsubhalos = sim.hostsubhalos[sim.species_start[4]
                :sim.species_end[4]]

            # Get time of previous snapshot (snapnum-1)
            time_prev = snap.snapshot(
                self.basedir+self.simdir, snapnum-1, read_params=False, header_only=True).header.time
            if new_thresh > 0.0:
                time_prev = time_prev + (time - time_prev)*(1-new_thresh)
                self.new_thresh = new_thresh

            # First get the age etc. of star particles in the current snapshot
            # which have the same ID as a star in snapnum_final

            # Stars which end up in satellites:
            ind = [i for i, x in enumerate(sim.starids) if x in self.match_gal]
            self.cur_gal_ids = sim.starids[ind]
            self.cur_gal_pos = sim.starpos[ind]
            self.cur_gal_age = sim.age[ind]
            self.cur_gal_hostsubhalo = hostsubhalos[ind]
            self.cur_gal_mass = sim.starmass[ind]
            print "len(cur_gal_age) =", len(self.cur_gal_age)
            # Sort by IDs
            cur_gal_sort = np.argsort(self.cur_gal_ids)
            self.cur_gal_ids = self.cur_gal_ids[cur_gal_sort]
            self.cur_gal_pos = self.cur_gal_pos[cur_gal_sort]
            self.cur_gal_hostsubhalo = self.cur_gal_hostsubhalo[cur_gal_sort]
            self.cur_gal_mass = self.cur_gal_mass[cur_gal_sort]
            '''
            : WARNING: If you need to reference self.gal_ids etc. then remember
            : that they are sorted by IDs so you will need to do the same
            : for these arrays
            '''

            # Stars which end up in BCG:
            ind = [i for i, x in enumerate(sim.starids) if x in self.match_bcg]
            self.cur_bcg_ids = sim.starids[ind]
            self.cur_bcg_pos = sim.starpos[ind]
            self.cur_bcg_age = sim.age[ind]
            self.cur_bcg_hostsubhalo = hostsubhalos[ind]
            self.cur_bcg_mass = sim.starmass[ind]
            # Sort by IDs
            cur_bcg_sort = np.argsort(self.cur_bcg_ids)
            self.cur_bcg_ids = self.cur_bcg_ids[cur_bcg_sort]
            self.cur_bcg_pos = self.cur_bcg_pos[cur_bcg_sort]
            self.cur_bcg_hostsubhalo = self.cur_bcg_hostsubhalo[cur_bcg_sort]
            self.cur_bcg_mass = self.cur_bcg_mass[cur_bcg_sort]

            # Stars which end up in ICL:
            ind = [i for i, x in enumerate(sim.starids) if x in self.match_icl]
            self.cur_icl_ids = sim.starids[ind]
            self.cur_icl_pos = sim.starpos[ind]
            self.cur_icl_age = sim.age[ind]
            self.cur_icl_hostsubhalo = hostsubhalos[ind]
            self.cur_icl_mass = sim.starmass[ind]
            # Sort by IDs
            cur_icl_sort = np.argsort(self.cur_icl_ids)
            self.cur_icl_ids = self.cur_icl_ids[cur_icl_sort]
            self.cur_icl_pos = self.cur_icl_pos[cur_icl_sort]
            self.cur_icl_hostsubhalo = self.cur_icl_hostsubhalo[cur_icl_sort]
            self.cur_icl_mass = self.cur_icl_mass[cur_icl_sort]

            # Of these stars, find those that were created between this snapshot
            # and the previous snapshot

            # New stars which end up in satellites:
            ind = np.where((self.cur_gal_age > time_prev) & (
                self.cur_gal_age < time) & (self.cur_gal_hostsubhalo > host_thresh))
            self.gal_new_ids = self.cur_gal_ids[ind]
            print "len(gal_new_ids) =", len(self.gal_new_ids)
            self.gal_new_pos = self.cur_gal_pos[ind]
            self.gal_new_hostsubhalo = self.cur_gal_hostsubhalo[ind]
            self.gal_new_mass = self.cur_gal_mass[ind]

            # Calculate the radial distance of these new stars from the group centre
            if get_dist:
                # Initialise centers of hostsubhalos to center of group
                self.hostcenter = np.column_stack((np.full_like(self.gal_new_pos[:, 0], centerpos[0]), np.full_like(
                    self.gal_new_pos[:, 1], centerpos[1]), np.full_like(self.gal_new_pos[:, 2], centerpos[2])))
                # center of hostsubhalo for parts in a subhalo
                self.hostcenter[(self.gal_new_hostsubhalo > -1),
                                :] = sim.cat.sub_pos[self.gal_new_hostsubhalo[(self.gal_new_hostsubhalo > -1)], :]
                self.gal_new_rad = sim.nearest_dist_3D(
                    self.gal_new_pos, self.hostcenter)
                if len(self.gal_new_hostsubhalo) > 0:
                    print "fraction in subhaloes = "+'{:f}'.format(float(len(self.gal_new_hostsubhalo[(
                        self.gal_new_hostsubhalo > -1)])) / len(self.gal_new_hostsubhalo))
                    print "fraction in main halo = "+'{:f}'.format(float(len(self.gal_new_hostsubhalo[(
                        self.gal_new_hostsubhalo == -1)])) / len(self.gal_new_hostsubhalo))
                    print "fraction outside halo = "+'{:f}'.format(float(len(self.gal_new_hostsubhalo[(
                        self.gal_new_hostsubhalo < -1)])) / len(self.gal_new_hostsubhalo))

            # New stars which end up in the BCG:
            ind = np.where((self.cur_bcg_age > time_prev) & (
                self.cur_bcg_age < time) & (self.cur_bcg_hostsubhalo > host_thresh))
            self.bcg_new_ids = self.cur_bcg_ids[ind]
            self.bcg_new_pos = self.cur_bcg_pos[ind]
            self.bcg_new_hostsubhalo = self.cur_bcg_hostsubhalo[ind]
            self.bcg_new_mass = self.cur_bcg_mass[ind]
            if get_dist:
                self.hostcenter = np.column_stack((np.full_like(self.bcg_new_pos[:, 0], centerpos[0]), np.full_like(
                    self.bcg_new_pos[:, 1], centerpos[1]), np.full_like(self.bcg_new_pos[:, 2], centerpos[2])))
                self.hostcenter[(self.bcg_new_hostsubhalo > -1),
                                :] = sim.cat.sub_pos[self.bcg_new_hostsubhalo[(self.bcg_new_hostsubhalo > -1)], :]
                self.bcg_new_rad = sim.nearest_dist_3D(
                    self.bcg_new_pos, self.hostcenter)

            # New stars which end up in the ICL:
            ind = np.where((self.cur_icl_age > time_prev) & (
                self.cur_icl_age < time) & (self.cur_icl_hostsubhalo > host_thresh))
            self.icl_new_ids = self.cur_icl_ids[ind]
            self.icl_new_pos = self.cur_icl_pos[ind]
            self.icl_new_hostsubhalo = self.cur_icl_hostsubhalo[ind]
            self.icl_new_mass = self.cur_icl_mass[ind]
            if get_dist:
                self.hostcenter = np.column_stack((np.full_like(self.icl_new_pos[:, 0], centerpos[0]), np.full_like(
                    self.icl_new_pos[:, 1], centerpos[1]), np.full_like(self.icl_new_pos[:, 2], centerpos[2])))
                self.hostcenter[(self.icl_new_hostsubhalo > -1),
                                :] = sim.cat.sub_pos[self.icl_new_hostsubhalo[(self.icl_new_hostsubhalo > -1)], :]
                self.icl_new_rad = sim.nearest_dist_3D(
                    self.icl_new_pos, self.hostcenter)

        elif (check_before == 1 and snapnum < self.snapnum_final):
            time_next = snap.snapshot(
                self.basedir+self.simdir, snapnum+1, read_params=False, header_only=True).header.time
            if new_thresh > 0.0:
                time_next = time + (time_next - time)*(new_thresh)
                self.new_thresh = new_thresh

            # Get gas particles that will (at some time) produce stars in sats, bcg or icl in the final snapshot
            print "Finding new stars before formation (gas)..."

            gas_hostsubhalos = sim.hostsubhalos[sim.species_start[0]
                :sim.species_end[0]]

            print "Loading IDs..."
            sim.load_gasids()
            print "len(gas_ids) =", len(gas_ids)
            print "Loading gas positions..."
            sim.load_gaspos()
            print "Loading gas masses..."
            sim.load_gasmass()

            ind = [i for i, x in enumerate(sim.gas_ids) if x in self.match_gal]
            # IDs of gas parents of gal stars
            self.cur_gal_ids = sim.gas_ids[ind]
            print "len(cur_gal_ids) =", len(self.cur_gal_ids)
            self.cur_gal_pos = sim.gaspos[ind]
            self.cur_gal_hostsubhalo = gas_hostsubhalos[ind]
            self.cur_gal_mass = sim.gasmass[ind]
            # Sort by IDs
            cur_gal_sort = np.argsort(self.cur_gal_ids)
            self.cur_gal_ids = self.cur_gal_ids[cur_gal_sort]
            self.cur_gal_pos = self.cur_gal_pos[cur_gal_sort]
            self.cur_gal_hostsubhalo = self.cur_gal_hostsubhalo[cur_gal_sort]
            self.cur_gal_mass = self.cur_gal_mass[cur_gal_sort]
            # Get gas 'age' i.e. time when produces star particle
            matcher = set(self.cur_gal_ids)
            ind = [i for i, x in enumerate(self.gal_ids) if x in matcher]
            self.cur_gal_age = self.gal_age[ind]
            print "len(cur_gal_age) =", len(self.cur_gal_age)

            ind = [i for i, x in enumerate(sim.gas_ids) if x in self.match_bcg]
            self.cur_bcg_ids = sim.gas_ids[ind]
            self.cur_bcg_pos = sim.gaspos[ind]
            self.cur_bcg_hostsubhalo = gas_hostsubhalos[ind]
            self.cur_bcg_mass = sim.gasmass[ind]
            # Sort by IDs
            cur_bcg_sort = np.argsort(self.cur_bcg_ids)
            self.cur_bcg_ids = self.cur_bcg_ids[cur_bcg_sort]
            self.cur_bcg_pos = self.cur_bcg_pos[cur_bcg_sort]
            self.cur_bcg_hostsubhalo = self.cur_bcg_hostsubhalo[cur_bcg_sort]
            self.cur_bcg_mass = self.cur_bcg_mass[cur_bcg_sort]
            matcher = set(self.cur_bcg_ids)
            ind = [i for i, x in enumerate(self.bcg_ids) if x in matcher]
            self.cur_bcg_age = self.bcg_age[ind]

            ind = [i for i, x in enumerate(sim.gas_ids) if x in self.match_icl]
            self.cur_icl_ids = sim.gas_ids[ind]
            self.cur_icl_pos = sim.gaspos[ind]
            self.cur_icl_hostsubhalo = gas_hostsubhalos[ind]
            self.cur_icl_mass = sim.gasmass[ind]
            # Sort by IDs
            cur_icl_sort = np.argsort(self.cur_icl_ids)
            self.cur_icl_ids = self.cur_icl_ids[cur_icl_sort]
            self.cur_icl_pos = self.cur_icl_pos[cur_icl_sort]
            self.cur_icl_hostsubhalo = self.cur_icl_hostsubhalo[cur_icl_sort]
            self.cur_icl_mass = self.cur_icl_mass[cur_icl_sort]
            matcher = set(self.cur_icl_ids)
            ind = [i for i, x in enumerate(self.icl_ids) if x in matcher]
            self.cur_icl_age = self.icl_age[ind]

            # Find gas particles which form stars before the next snapshot
            ind = np.where((self.cur_gal_age > time) & (
                self.cur_gal_age < time_next) & (self.cur_gal_hostsubhalo > host_thresh))
            self.gal_new_ids = self.cur_gal_ids[ind]
            print "len(gal_new_ids) =", len(self.gal_new_ids)
            self.gal_new_pos = self.cur_gal_pos[ind]
            self.gal_new_hostsubhalo = self.cur_gal_hostsubhalo[ind]
            self.gal_new_mass = self.cur_gal_mass[ind]
            if get_dist:
                # Initialise centers of hostsubhalos to center of group
                self.hostcenter = np.column_stack((np.full_like(self.gal_new_pos[:, 0], centerpos[0]), np.full_like(
                    self.gal_new_pos[:, 1], centerpos[1]), np.full_like(self.gal_new_pos[:, 2], centerpos[2])))
                # get center of hostsubhalo for subhalo particles
                self.hostcenter[(self.gal_new_hostsubhalo > -1),
                                :] = sim.cat.sub_pos[self.gal_new_hostsubhalo[(self.gal_new_hostsubhalo > -1)], :]
                self.gal_new_rad = sim.nearest_dist_3D(
                    self.gal_new_pos, self.hostcenter)
                if len(self.gal_new_hostsubhalo) > 0:
                    print "fraction in subhaloes = "+'{:f}'.format(float(len(self.gal_new_hostsubhalo[(
                        self.gal_new_hostsubhalo > -1)])) / len(self.gal_new_hostsubhalo))
                    print "fraction in main halo = "+'{:f}'.format(float(len(self.gal_new_hostsubhalo[(
                        self.gal_new_hostsubhalo == -1)])) / len(self.gal_new_hostsubhalo))
                    print "fraction outside halo = "+'{:f}'.format(float(len(self.gal_new_hostsubhalo[(
                        self.gal_new_hostsubhalo < -1)])) / len(self.gal_new_hostsubhalo))

            ind = np.where((self.cur_bcg_age > time) & (
                self.cur_bcg_age < time_next) & (self.cur_bcg_hostsubhalo > host_thresh))
            self.bcg_new_ids = self.cur_bcg_ids[ind]
            self.bcg_new_pos = self.cur_bcg_pos[ind]
            self.bcg_new_hostsubhalo = self.cur_bcg_hostsubhalo[ind]
            self.bcg_new_mass = self.cur_bcg_mass[ind]
            if get_dist:
                self.hostcenter = np.column_stack((np.full_like(self.bcg_new_pos[:, 0], centerpos[0]), np.full_like(
                    self.bcg_new_pos[:, 1], centerpos[1]), np.full_like(self.bcg_new_pos[:, 2], centerpos[2])))
                self.hostcenter[(self.bcg_new_hostsubhalo > -1),
                                :] = sim.cat.sub_pos[self.bcg_new_hostsubhalo[(self.bcg_new_hostsubhalo > -1)], :]
                self.bcg_new_rad = sim.nearest_dist_3D(
                    self.bcg_new_pos, self.hostcenter)

            ind = np.where((self.cur_icl_age > time) & (
                self.cur_icl_age < time_next) & (self.cur_icl_hostsubhalo > host_thresh))
            self.icl_new_ids = self.cur_icl_ids[ind]
            self.icl_new_pos = self.cur_icl_pos[ind]
            self.icl_new_hostsubhalo = self.cur_icl_hostsubhalo[ind]
            self.icl_new_mass = self.cur_icl_mass[ind]
            if get_dist:
                self.hostcenter = np.column_stack((np.full_like(self.icl_new_pos[:, 0], centerpos[0]), np.full_like(
                    self.icl_new_pos[:, 1], centerpos[1]), np.full_like(self.icl_new_pos[:, 2], centerpos[2])))
                self.hostcenter[(self.icl_new_hostsubhalo > -1),
                                :] = sim.cat.sub_pos[self.icl_new_hostsubhalo[(self.icl_new_hostsubhalo > -1)], :]
                self.icl_new_rad = sim.nearest_dist_3D(
                    self.icl_new_pos, self.hostcenter)

        elif (check_before == 1 and snapnum == self.snapnum_final):
            print "\033[31mWARNING:\033[0m: Cannot find gas formation particles for last snapshot " + \
                str(self.snapnum_final)
            return
        elif check_before == 0 and snapnum <= 0:
            assert False, "cannot have check_before=0 and snapnum<=0"
        else:
            assert False, "Check_before must be 0 or 1"

        self.sim = sim  # needed when saving group properties in get_new_stars_props
        print "Done snapshot", snapnum
        return True

    def plot_new_stars_map(self, snapnum, group=0, ngrid=1024, proj="xy",
                           dxyimg=None, dzimg=None, centre=None,
                           check_before=0, include_fuzzy=False, outsuffix=""):
        """
        If check_before==0, plots stars formed between this snapshot and 
        the previous which end up in satellites, BCG or ICL at the last snapshot
        If check_before==1, plots gas which forms stars between this snapshot and
        the next that end up in satellites, BCH or ICL in the last snapshot
        ## ngrid is the number of pixels on a side for the density maps
        ## proj defines the axes which make the image plane
        ## dxyimg is the sidelength of the map in kpc code units
        ## dzimg is the projection depth in kpc code units
        """
        import matplotlib.pyplot as plt
        import numpy as np
        import h5py
        import snap
        import readsnap as rs

        filepath = self.outdir+self.simdir+"new_star_props_cb" + \
            str(check_before)+"_grp"+str(group)+outsuffix+".hdf5"
        print "Opening", filepath
        with h5py.File(filepath, "r") as infile:
            #groupcentre = np.array(infile.attrs['groupcentre_final'])
            grp = infile["snap_"+str(snapnum).zfill(3)]
            group_cur = grp.attrs['group_cur']
            gal_new_pos = np.array(grp['gal_new_pos'])
            gal_new_mass = np.array(grp['gal_new_mass'])
            gal_new_ids = np.array(grp['gal_new_ids'])
            bcg_new_pos = np.array(grp['bcg_new_pos'])
            bcg_new_mass = np.array(grp['bcg_new_mass'])
            bcg_new_ids = np.array(grp['bcg_new_ids'])
            icl_new_pos = np.array(grp['icl_new_pos'])
            icl_new_mass = np.array(grp['icl_new_mass'])
            icl_new_ids = np.array(grp['icl_new_ids'])
            R200 = grp.attrs['R200']

        if dxyimg is None:
            dxyimg = R200  # image has side length 2*dxyimg
            print "Using field length R200 =", R200

        sim = snap.snapshot(self.basedir+self.simdir, snapnum,
                            mpc_units=self.mpc_units, masstab=self.masstab)

        if centre is None:
            centre = sim.cat.group_pos[group_cur]
        if not hasattr(sim, "starmass"):
            sim.load_starmass()
        if not hasattr(sim, "starpos"):
            sim.load_starpos()
        all_ids = rs.read_block(
            sim.snapname, "ID  ", parttype=4, cosmic_ray_species=sim.num_cr_species)

        # Get the indices of stars not in cur_gal, cur_bcg or cur_icl
        myset = set(np.concatenate(
            (gal_new_ids, bcg_new_ids, icl_new_ids))[:, 0])
        ind = [i for i, x in enumerate(all_ids) if x not in myset]

        fig = plt.figure(figsize=(10, 10))
        ax = fig.add_subplot(111, aspect="equal")
        if check_before == 0:
            plt.title(self.simdir[:-1].replace("_", "\_") +
                      " Group "+str(group)+" at z = {:.2f}".format(sim.header.redshift) +
                      # " t = "+'{:.3f}'.format(sim.header.time)+
                      "\nstars formed since z = {:.2f}".format(snap.snapshot(self.basedir+self.simdir, snapnum-1, header_only=True, read_params=False).header.redshift))
        elif check_before == 1:
            plt.title(self.simdir[:-1].replace("_", "\_") +
                      " Group "+str(group)+" at z = {:.2f}".format(sim.header.redshift) +
                      "\ngas forming stars before z = {:.2f}".format(snap.snapshot(self.basedir+self.simdir, snapnum+1, header_only=True, read_params=False).header.redshift))

        self.plot_map(ax, sim, centre, sim.starpos[ind], sim.starmass[ind], gal_new_pos, gal_new_mass,
                      bcg_new_pos, bcg_new_mass, icl_new_pos, icl_new_mass,
                      ngrid=ngrid, dxyimg=dxyimg, dzimg=dzimg, proj=proj)

        plotfile = self.outdir+self.simdir+"new_star_map"+outsuffix+"_snap" + \
            str(snapnum)+"_cb"+str(check_before)+"_grp"+str(group)+".pdf"
        fig.savefig(plotfile)
        print "Saved map to ", plotfile
        return plotfile

    def plot_stars_map(self, snapnum, group=0, ngrid=1024, proj="xy",
                       dxyimg=None, dzimg=None, centre=None, circrad=[],
                       check_before=0, include_fuzzy=False, outsuffix=""):
        """
        Plot a stellar density map at the given snapshot 'snapnum' colour coded
        according to whether stars end up in satellites, BCG or ICL at z=0
        ## circrad is a list of radii (r500) for which to draw circles
        """
        import matplotlib.pyplot as plt
        import numpy as np
        import h5py
        import snap
        import readsnap as rs

        filepath = self.outdir+self.simdir+"new_star_props_cb" + \
            str(check_before)+"_grp"+str(group)+outsuffix+".hdf5"
        print "Opening", filepath
        with h5py.File(filepath, "r") as infile:
            groupcentre = np.array(infile.attrs['groupcentre_final'])
            grp = infile["snap_"+str(snapnum).zfill(3)]
            group_cur = grp.attrs['group_cur']
            gal_pos = np.array(grp['cur_gal_pos'])
            gal_mass = np.array(grp['cur_gal_mass'])
            gal_ids = np.array(grp['cur_gal_ids'])
            bcg_pos = np.array(grp['cur_bcg_pos'])
            bcg_mass = np.array(grp['cur_bcg_mass'])
            bcg_ids = np.array(grp['cur_bcg_ids'])
            icl_pos = np.array(grp['cur_icl_pos'])
            icl_mass = np.array(grp['cur_icl_mass'])
            icl_ids = np.array(grp['cur_icl_ids'])
            R200 = grp.attrs['R200']
            R500 = grp.attrs['R500']
        if len(circrad) > 0:
            circrad = [crad*R500 for crad in circrad]

        if dxyimg is None:
            dxyimg = R200  # image has side length 2*dxyimg
            print "Using field length R200 =", R200

        # Load snapshot to get stars not in sats, ICL or BCG
        sim = snap.snapshot(self.basedir+self.simdir, snapnum,
                            mpc_units=self.mpc_units, masstab=self.masstab)

        if centre is None:
            centre = sim.cat.group_pos[group_cur]
            print "Using centre", centre
            print "Final group centre was", groupcentre
        if not hasattr(sim, "starmass"):
            sim.load_starmass()
        if not hasattr(sim, "starpos"):
            sim.load_starpos()
        all_ids = rs.read_block(
            sim.snapname, "ID  ", parttype=4, cosmic_ray_species=sim.num_cr_species)

        # Get the indices of stars not in cur_gal, cur_bcg or cur_icl
        myset = set(np.concatenate((gal_ids, bcg_ids, icl_ids))[:, 0])
        ind = [i for i, x in enumerate(all_ids) if x not in myset]

        fig = plt.figure(figsize=(10, 10))
        ax = fig.add_subplot(111, aspect="equal")
        plt.title(self.simdir[:-1].replace("_", "\_") +
                  " Group "+str(group)+" at z = {:.2f}".format(sim.header.redshift))

        self.plot_map(ax, sim, centre, sim.starpos[ind], sim.starmass[ind], gal_pos, gal_mass,
                      bcg_pos, bcg_mass, icl_pos, icl_mass, circrad=circrad,
                      ngrid=ngrid, dxyimg=dxyimg, dzimg=dzimg, proj=proj)

        plotfile = self.outdir+self.simdir+"star_map"+outsuffix+"_snap" + \
            str(snapnum)+"_cb"+str(check_before)+"_grp"+str(group)+".pdf"
        fig.savefig(plotfile, bbox_inches='tight')
        print "Saved map to ", plotfile
        return plotfile

    def find_descendant(self, snapnum_early, snapnum_late, subnum, sim_cur=None, verbose=False):
        assert snapnum_early < snapnum_late, "first snapshot number must be lower than second snapshot number"
        for snapnum in range(snapnum_early, snapnum_late):
            subnum, sim_cur = self.find_descendant_next_snapshot(
                snapnum, subnum, sim_cur=sim_cur, return_sim=True, verbose=verbose)

        return subnum

    def find_descendant_next_snapshot(self, snapnum_cur, subnum, sim_cur=None, sim_late=None, return_sim=False, satellite=True, verbose=False):
        """
        For the subhalo 'subnum' in the snapshot number 'snapnum_cur', find its
        descendant in the next snapshot (snapnum_cur+1), returning the subhalo number.
        This follows the method of Puchwein+10, where we search for the subhalo
        in the next snapshot that contains the largest mass of the original subhalo's
        star particles
        Args:
            ### sim_cur is a snap.snapshot instance for the current snapshot, but this will
                be loaded if not given
            ### sim_late is a snap.snapshot instance for the next snapshot (snapnum_cur+1), but this will
                be loaded if not given
            ### if return_sim, function also returns the snap.snapshot instance for the
                next snapshot
            ### if satellite, avoid choosing the main halo as the descendant if there
                is another possible descendant with at least 10% of the original galaxy's stars

        """
        import snap
        import numpy as np

        snapnum_late = snapnum_cur+1

        if sim_cur is None:
            sim_cur = snap.snapshot(
                self.basedir+self.simdir, snapnum_cur, mpc_units=self.mpc_units, masstab=self.masstab)
        if not hasattr(sim_cur, "starids"):
            if verbose:
                print "Loading IDs..."
            sim_cur.load_starids()
        if not hasattr(sim_cur, "hostsubhalos"):
            if verbose:
                print "Loading hostsubhalos..."
            sim_cur.get_hostsubhalos()

        # Get set of IDs of star particles in subhalo in current snapshot
        match_ids = set(sim_cur.starids[np.where(
            sim_cur.hostsubhalos[sim_cur.species_start[4]:sim_cur.species_end[4]] == subnum)[0]])

        # Now find subhalo in later snapshot that contains the largest mass of these star particles
        if sim_late is None:
            sim_late = snap.snapshot(
                self.basedir+self.simdir, snapnum_late, mpc_units=self.mpc_units, masstab=self.masstab)
        if not hasattr(sim_late, "starids"):
            if verbose:
                print "Loading IDs..."
            sim_late.load_starids()
        if not hasattr(sim_late, "starmass"):
            if verbose:
                print "Loading stellar mass..."
            sim_late.load_starmass()
        if not hasattr(sim_late, "hostsubhalos"):
            if verbose:
                print "Loading hostsubhalos..."
            sim_late.get_hostsubhalos()
        hostsubhalos = sim_late.hostsubhalos[sim_late.species_start[4]
            :sim_late.species_end[4]]

        # Get indices of star particles with matching IDs
        ind = np.array([i for i, x in enumerate(
            sim_late.starids) if x in match_ids])
        # Get subhalos which contain at least one of the object's original stars
        sub_candidates = np.unique(hostsubhalos[ind])
        if verbose:
            print "sub_candidates =", sub_candidates
        # Get the total mass of stars in each candidate subhalo
        sub_cand_mass = np.zeros(len(sub_candidates), dtype=np.float32)
        for subidx in range(len(sub_candidates)):
            sub_cand_mass[subidx] = np.sum(
                sim_late.starmass[ind[np.where(hostsubhalos[ind] == sub_candidates[subidx])[0]]])
        if verbose:
            print "sub_cand_mass =", sub_cand_mass

        # The descendant is the subhalo with the largest mass of the object's original stars
        subnum_new = sub_candidates[np.argmax(sub_cand_mass)]
        if verbose:
            print "subnum_new =", subnum_new

        # If this returns the main halo, get the next candidate instead
        # if it contains more than 10 per cent of the original object's stellar mass
        if satellite and len(sub_candidates) > 1:
            # If new subnum is main halo of its parent group and the previous subnum is not:
            if subnum_new == sim_late.cat.firstsub[sim_late.cat.sub_parent[subnum_new]] and subnum != sim_cur.cat.firstsub[sim_cur.cat.sub_parent[subnum]]:
                idx = np.argsort(sub_cand_mass)[-2]
                if sub_candidates[idx] != 0 and sub_cand_mass[idx] > 0.1*sim_cur.cat.sub_masstab[sub_candidates[idx], 4]:
                    subnum_new = sub_candidates[idx]

        if return_sim:
            return subnum_new, sim_late
        else:
            return subnum_new

    def get_subhalo(self, snapnum, subnum):
        import snap
        import numpy as np

        sim = snap.snapshot(self.basedir+self.simdir, snapnum,
                            mpc_units=self.mpc_units, masstab=self.masstab)
        h = sim.header.hubble

        print "Loading IDs..."
        sim.load_starids()
        print "Loading star positions..."
        sim.load_starpos()
        print "Loading stellar masses..."
        sim.load_starmass()
        print "Getting hostsubhalos..."
        sim.get_hostsubhalos()
        hostsubhalos = sim.hostsubhalos[sim.species_start[4]
            :sim.species_end[4]]

        # Create namedtuple to store the data since this is easier to reference
        # compared to returning a regular tuple
        from collections import namedtuple
        dat = namedtuple('dat', ['totmass', 'early_gal_ids', 'early_gal_pos', 'early_gal_mass',
                                 'early_icl_ids', 'early_icl_pos', 'early_icl_mass',
                                 'early_bcg_ids', 'early_bcg_pos', 'early_bcg_mass',
                                 'early_other_pos', 'early_other_mass'])

        # Get indices of stars bound to given subhalo in early snapshot
        subind = np.where(hostsubhalos == subnum)[0]
        dat.totmass = np.sum(sim.starmass[subind])*1e10/h
        print "Subhalo has stellar mass {:.3e} Msun".format(
            sim.cat.sub_masstab[subnum, 4]*1e10/h)
        print "Subhalo has stellar mass {:.3e} Msun".format(dat.totmass)
        #mass1 = []
        #mass2 = []
        # for subnum2 in range(sim.cat.group_nsubs[0]):
        #subind2 = np.where(hostsubhalos == subnum2)[0]
        #mass1.append(sim.cat.sub_masstab[subnum2, 4])
        # mass2.append(np.sum(sim.starmass[subind2]))
        #fig2, ax2 = plt.subplots()
        #ratio = np.array(mass1) / np.array(mass2)
        #ax2.hist(ratio[np.isfinite(ratio)], bins=1000)
        #ax2.plot(mass1, ratio, ls='none', marker='o')
        #print "mass ratio =", np.sum(mass1) / np.sum(mass2)

        # Get stars in subhalo which end up in satellites
        ind = [i for i, x in enumerate(
            sim.starids[subind]) if x in self.match_gal]
        dat.early_gal_ids = sim.starids[subind[ind]]
        dat.early_gal_pos = sim.starpos[subind[ind]]
        dat.early_gal_mass = sim.starmass[subind[ind]]
        print "Mass ending up in satellites: {:.3e} Msun".format(
            np.sum(dat.early_gal_mass)*1e10/h)

        # Get stars in subhalo which end up in ICL
        ind = [i for i, x in enumerate(
            sim.starids[subind]) if x in self.match_icl]
        dat.early_icl_ids = sim.starids[subind[ind]]
        dat.early_icl_pos = sim.starpos[subind[ind]]
        dat.early_icl_mass = sim.starmass[subind[ind]]
        print "Mass ending up in ICL: {:.3e} Msun".format(
            np.sum(dat.early_icl_mass)*1e10/h)

        # Get stars in subhalo which end up in BCG
        ind = [i for i, x in enumerate(
            sim.starids[subind]) if x in self.match_bcg]
        dat.early_bcg_ids = sim.starids[subind[ind]]
        dat.early_bcg_pos = sim.starpos[subind[ind]]
        dat.early_bcg_mass = sim.starmass[subind[ind]]
        print "Mass ending up in BCG: {:.3e} Msun".format(
            np.sum(dat.early_bcg_mass)*1e10/h)

        # Get the indices of stars not in cur_gal, cur_bcg or cur_icl
        myset = set(np.concatenate(
            (dat.early_gal_ids, dat.early_icl_ids, dat.early_bcg_ids)))
        ind = [i for i, x in enumerate(sim.starids) if x not in myset]
        dat.early_other_pos = sim.starpos[ind]
        dat.early_other_mass = sim.starmass[ind]

        # Shouldn't need the particle data anymore:
        del sim.starpos
        del sim.starmass
        del sim.hostsubhalos

        return sim, dat

    def plot_stars_infalling_subhalo(self, snapnums, snapnum_late, subnum,
                                     group=0, cd_cutoff_method=0, cutoff_rad=30.0,
                                     include_fuzzy=False, ngrid=1024, proj="xy",
                                     dxyimg=None, dzimg=None,
                                     outsuffix=""):
        """
        Plots star maps at each snapshot, highlighting the stars belonging to the
        specified subhalo 'subnum' in the earliest snapshot. These highlighted stars
        are coloured according to whether they end up in satellites, BCG or ICL 
        in the snapshot 'snapnum_late'. An additional map shows the distribution
        of these different components in 'snapnum_late'.
        """
        import numpy as np
        import matplotlib.pyplot as plt

        plotfile = self.outdir+self.simdir+"infall_map"+outsuffix+"_snap" + \
            str(min(snapnums))+"-"+str(snapnum_late) + \
            "_grp"+str(group)+"_sub"+str(subnum)+".pdf"

        snapnums = np.sort(snapnums)
        # Use the following function to get the IDs of stars in each component
        # at the latest snapshot
        self.get_ids_stellar_components(
            snapnum_late, group=group, cd_cutoff_method=cd_cutoff_method, cutoff_rad=cutoff_rad, include_fuzzy=include_fuzzy)
        sim_late = self.sim

        fig, axes = plt.subplots(
            1, len(snapnums)+1, figsize=(7*(len(snapnums)+1), 7))
        fig.suptitle(self.simdir[:-1].replace("_", "\_")+" Group "+str(group))

        # Loop over each snapshot except the last
        for idx, snapnum in enumerate(snapnums):
            # Use get_subhalo to get the properties of star particles belonging
            # to the given subhalo in the current snapshot
            sim_early, dat = self.get_subhalo(snapnum, subnum)
            h = sim_early.header.hubble
            if idx == 0:
                # Save the IDs of stars in the earliest snapshot
                # Create sets of the IDs (for quicker matching)
                match_gal = set(dat.early_gal_ids)
                match_bcg = set(dat.early_bcg_ids)
                match_icl = set(dat.early_icl_ids)
                match_all = set(np.concatenate(
                    (dat.early_gal_ids, dat.early_icl_ids, dat.early_bcg_ids)))
                initial_z = sim_early.header.redshift

            ### Plot map at the earlier snapshot ###
            # For the centre of the map we'll just use the position of the group
            # with the same index at the early snapshot as in the late snapshot
            centre = sim_early.cat.group_pos[group]
            if dxyimg is None:  # use R200 at the later snapshot
                dxyimg = sim_late.cat.group_r_crit200[group]
            col = self.plot_map(axes[idx], sim_early, centre, dat.early_other_pos, dat.early_other_mass,
                                dat.early_gal_pos, dat.early_gal_mass,
                                dat.early_bcg_pos, dat.early_bcg_mass, dat.early_icl_pos, dat.early_icl_mass,
                                ngrid=ngrid, dxyimg=dxyimg, dzimg=dzimg, proj=proj)
            axes[idx].set_title("z = {:.2f} ({:d})".format(
                sim_early.header.redshift, subnum))
            # Add labels giving the masses
            spacing = 0.05
            axes[idx].text(0.95, 0.05+2*spacing, "M$_{\mathrm{sat}}="+"{:.2e}".format(np.sum(dat.early_gal_mass)*1e10/h)+"\, \mathrm{M}_{\odot}$",
                           horizontalalignment='right', color=col[0],
                           verticalalignment='center', transform=axes[idx].transAxes)
            axes[idx].text(0.95, 0.05+spacing, "M$_{\mathrm{ICL}}="+"{:.2e}".format(np.sum(dat.early_icl_mass)*1e10/h)+"\, \mathrm{M}_{\odot}$",
                           horizontalalignment='right', color=col[1],
                           verticalalignment='center', transform=axes[idx].transAxes)
            axes[idx].text(0.95, 0.05, "M$_{\mathrm{BCG}}="+"{:.2e}".format(np.sum(dat.early_bcg_mass)*1e10/h)+"\, \mathrm{M}_{\odot}$",
                           horizontalalignment='right', color=col[2],
                           verticalalignment='center', transform=axes[idx].transAxes)
            axes[idx].text(0.95, 0.05+3*spacing, "M$_{\mathrm{gal}}="+"{:.2e}".format(dat.totmass)+"\, \mathrm{M}_{\odot}$",
                           horizontalalignment='right', color='white',
                           verticalalignment='center', transform=axes[idx].transAxes)

            # Get the subhalo number for the descendant in the next snapshot
            if idx < len(snapnums)-1:
                print "Finding descendant of subhalo", subnum
                subnum = self.find_descendant(
                    snapnum, snapnums[idx+1], subnum, sim_cur=sim_early)
                print "New subhalo is", subnum

            del sim_early

        ### Plot map at the latest snapshot ###
        if not hasattr(sim_late, "starids"):
            print "Loading IDs..."
            sim_late.load_starids()
        # should already be loaded by get_ids_stellar_components()
        if not hasattr(sim_late, "starpos"):
            print "Loading star positions..."
            sim_late.load_starpos()
        if not hasattr(sim_late, "starmass"):
            print "Loading stellar masses..."
            sim_late.load_starmass()

        ind = [i for i, x in enumerate(sim_late.starids) if x in match_gal]
        late_gal_pos = sim_late.starpos[ind]
        late_gal_mass = sim_late.starmass[ind]

        ind = [i for i, x in enumerate(sim_late.starids) if x in match_icl]
        late_icl_pos = sim_late.starpos[ind]
        late_icl_mass = sim_late.starmass[ind]

        ind = [i for i, x in enumerate(sim_late.starids) if x in match_bcg]
        late_bcg_pos = sim_late.starpos[ind]
        late_bcg_mass = sim_late.starmass[ind]

        # Get the indices of other stars
        ind = [i for i, x in enumerate(sim_late.starids) if x not in match_all]

        centre = sim_late.cat.group_pos[group]
        self.plot_map(axes[-1], sim_late, centre, sim_late.starpos[ind], sim_late.starmass[ind],
                      late_gal_pos, late_gal_mass,
                      late_bcg_pos, late_bcg_mass, late_icl_pos, late_icl_mass,
                      ngrid=ngrid, dxyimg=dxyimg, dzimg=dzimg, proj=proj)
        axes[-1].set_title("z = {:.2f} (all stars from z = {:.2f})".format(
            sim_late.header.redshift, initial_z))
        del sim_late

        plt.tight_layout()
        fig.savefig(plotfile, bbox_inches='tight')
        print "Saved map to ", plotfile
        return plotfile

    def plot_map(self, ax, sim, centre, all_pos, all_mass, gal_pos, gal_mass, bcg_pos, bcg_mass,
                 icl_pos, icl_mass, ngrid=1024, proj="xy", dxyimg=None, dzimg=None, circrad=[]):
        """
        Given a matplotlib axes instance 'ax' this function plots a mass density map
        of stars for different components (satellites, BCG, ICL and other)
        ## ax is a matplotlib axes instance
        ## sim is a snap.snapshot instance
        ## centre is the centre of the image in code units
        ## all_pos is an array of positions for all stars (regardless of component)
        ## all_mass are the corresponding stellar masses
        ## gal_pos and gal_mass are the positions and masses of satellite stars
        ## bcg_pos and bcg_mass are the positions and masses of BCG stars
        ## icl_pos and icl_mass are the positions and masses of ICL stars
        ## ngrid is the number of pixels on a side for the density maps
        ## proj defines the axes which make the image plane
        ## dxyimg is the sidelength of the map in kpc code units
        ## dzimg is the projection depth in kpc code units
        ## circrad is a list of radii (code units) of circles to draw at centre
        """
        import matplotlib.pyplot as plt
        import numpy as np
        from projectparticles.put_grid import put_grid_cic2D

        if dzimg is None:
            dzimg = 0.5*sim.box_sidelength
        if dxyimg is None:
            dxyimg = 0.5*sim.box_sidelength

        ax.set_xlabel("ckpc/h")
        ax.set_ylabel("ckpc/h")

        if proj == "xy":
            # All stars
            # closest (incl periodicity) distance along z axis
            plotpos_z = sim.nearest_dist_1D(all_pos[:, 2], centre[2])
            ind = np.where(plotpos_z < dzimg)[0]
            plotpos_x = sim.rel_pos(all_pos[ind, 0], centre[0])
            plotpos_y = sim.rel_pos(all_pos[ind, 1], centre[1])
            dens_map_all = put_grid_cic2D(
                ngrid, 0, 0, 2*dxyimg, plotpos_x, plotpos_y, all_mass[ind], 0)
            # such that x-coordinate correspond to x-axis and y-coordinate correspond to y-axis
            dens_map_all = dens_map_all.transpose()

            # stars ending up in satellite galaxies
            if len(gal_pos[:, 2]) > 0:
                # closest (incl periodicity) distance along z axis
                plotpos_z = sim.nearest_dist_1D(gal_pos[:, 2], centre[2])
                ind = np.where(plotpos_z < dzimg)[0]
                plotpos_x = sim.rel_pos(gal_pos[ind, 0], centre[0])
                plotpos_y = sim.rel_pos(gal_pos[ind, 1], centre[1])
                dens_map_gal = put_grid_cic2D(
                    ngrid, 0, 0, 2*dxyimg, plotpos_x, plotpos_y, gal_mass[ind], 0)
                # such that x-coordinate correspond to x-axis and y-coordinate correspond to y-axis
                dens_map_gal = dens_map_gal.transpose()
            else:
                print "\033[31m" + \
                    "No new stars in satellites galaxies"+"\033[0m"
                dens_map_gal = np.zeros((ngrid, ngrid))

            # stars ending up in BCG
            if len(bcg_pos[:, 2]) > 0:
                # closest (incl periodicity) distance along z axis
                plotpos_z = sim.nearest_dist_1D(bcg_pos[:, 2], centre[2])
                ind = np.where(plotpos_z < dzimg)[0]
                plotpos_x = sim.rel_pos(bcg_pos[ind, 0], centre[0])
                plotpos_y = sim.rel_pos(bcg_pos[ind, 1], centre[1])
                dens_map_bcg = put_grid_cic2D(
                    ngrid, 0, 0, 2*dxyimg, plotpos_x, plotpos_y, bcg_mass[ind], 0)
                # such that x-coordinate correspond to x-axis and y-coordinate correspond to y-axis
                dens_map_bcg = dens_map_bcg.transpose()
            else:
                print "\033[31m"+"No new stars in BCG"+"\033[0m"
                dens_map_bcg = np.zeros((ngrid, ngrid))

            # stars ending up in ICL
            if len(icl_pos[:, 2]) > 0:
                # closest (incl periodicity) distance along z axis
                plotpos_z = sim.nearest_dist_1D(icl_pos[:, 2], centre[2])
                ind = np.where(plotpos_z < dzimg)[0]
                plotpos_x = sim.rel_pos(icl_pos[ind, 0], centre[0])
                plotpos_y = sim.rel_pos(icl_pos[ind, 1], centre[1])
                dens_map_icl = put_grid_cic2D(
                    ngrid, 0, 0, 2*dxyimg, plotpos_x, plotpos_y, icl_mass[ind], 0)
                # such that x-coordinate correspond to x-axis and y-coordinate correspond to y-axis
                dens_map_icl = dens_map_icl.transpose()
            else:
                print "\033[31m"+"No new stars in ICL"+"\033[0m"
                dens_map_icl = np.zeros((ngrid, ngrid))

        else:
            assert False, "ERROR: proj = "+str(proj)+" not recognised."

        centrex, centrey = 0, 0

        # Make zero value pixels transparent with masked arrays
        dens_map_all = np.ma.masked_where(dens_map_all == 0, dens_map_all)
        dens_map_gal = np.ma.masked_where(dens_map_gal == 0, dens_map_gal)
        dens_map_bcg = np.ma.masked_where(dens_map_bcg == 0, dens_map_bcg)
        dens_map_icl = np.ma.masked_where(dens_map_icl == 0, dens_map_icl)

        #cmap = plt.get_cmap("afmhot")
        # cmap.set_bad('black')
        #ax.imshow(np.log10(dens_map_all), cmap=cmap, aspect="equal", origin="lower", extent=(centrex-dxyimg,centrex+dxyimg,centrey-dxyimg,centrey+dxyimg), interpolation='none')
        #ax.imshow(np.log10(dens_map_gal), cmap="PuRd", aspect="equal", origin="lower", extent=(centrex-dxyimg,centrex+dxyimg,centrey-dxyimg,centrey+dxyimg), interpolation='none')
        #ax.imshow(np.log10(dens_map_icl), cmap="PuBu", aspect="equal", origin="lower", extent=(centrex-dxyimg,centrex+dxyimg,centrey-dxyimg,centrey+dxyimg), interpolation='none')
        #ax.imshow(np.log10(dens_map_bcg), cmap="BuGn", aspect="equal", origin="lower", extent=(centrex-dxyimg,centrex+dxyimg,centrey-dxyimg,centrey+dxyimg), interpolation='none')

        # First fill with a solid colour
        # 0 for black, 255 for white
        fill = np.full(dens_map_gal.shape + (3,), 0, dtype=np.uint8)
        ax.imshow(fill, aspect="equal", origin="lower", extent=(
            centrex-dxyimg, centrex+dxyimg, centrey-dxyimg, centrey+dxyimg))

        # Now plot density maps of each component
        # sats, icl, bcg, others (white or 0.3)
        col = ['magenta', 'deepskyblue', 'limegreen', 'white']
        #vmin, vmax = np.min(np.log10(dens_map_all)), np.max(np.log10(dens_map_all))
        ax.imshow(np.log10(dens_map_all), cmap=self.make_transparent_colormap(col[3]), aspect="equal", origin="lower", extent=(
            centrex-dxyimg, centrex+dxyimg, centrey-dxyimg, centrey+dxyimg), interpolation='none')
        ax.imshow(np.log10(dens_map_icl), cmap=self.make_transparent_colormap(col[1]), aspect="equal", origin="lower", extent=(
            centrex-dxyimg, centrex+dxyimg, centrey-dxyimg, centrey+dxyimg), interpolation='none')
        ax.imshow(np.log10(dens_map_gal), cmap=self.make_transparent_colormap(col[0]), aspect="equal", origin="lower", extent=(
            centrex-dxyimg, centrex+dxyimg, centrey-dxyimg, centrey+dxyimg), interpolation='none')
        ax.imshow(np.log10(dens_map_bcg), cmap=self.make_transparent_colormap(col[2]), aspect="equal", origin="lower", extent=(
            centrex-dxyimg, centrex+dxyimg, centrey-dxyimg, centrey+dxyimg), interpolation='none')

        artist_all = plt.Line2D(
            (0, 1), (0, 0), mfc=col[3], marker='s', ms=10, mec='none', linestyle='')
        artist_gal = plt.Line2D(
            (0, 1), (0, 0), mfc=col[0], marker='s', ms=10, mec='none', linestyle='')
        artist_icl = plt.Line2D(
            (0, 1), (0, 0), mfc=col[1], marker='s', ms=10, mec='none', linestyle='')
        artist_bcg = plt.Line2D(
            (0, 1), (0, 0), mfc=col[2], marker='s', ms=10, mec='none', linestyle='')
        # Squares in legend:
        #ax.legend([artist_gal, artist_icl, artist_bcg],['Satellites', 'ICL', 'BCG'], frameon=True, numpoints=1)
        # Legend labels coloured:
        leg = ax.legend([artist_gal, artist_icl, artist_bcg, artist_all], ['Satellites', 'ICL', 'BCG', 'Other'],
                        markerscale=0, handlelength=0, handletextpad=0, frameon=False, borderpad=0.5, borderaxespad=1)
        for idx, text in enumerate(leg.get_texts()):  # set text colour
            text.set_color(col[idx])

        for crad in circrad:
            circle = plt.Circle((0, 0), crad, fc='none',
                                ec='white', ls='dashed', lw=3)
            ax.add_artist(circle)

        return col

    def make_transparent_colormap(self, col):
        """
        Given a matplotlib color 'col' (e.g. 'green') this function returns
        a colormap filled with that color but extending from alpha=0 to alpha=1
        """
        import numpy as np
        from matplotlib.colors import colorConverter
        from matplotlib.colors import ListedColormap
        N = 256  # number of colours in colormap

        # First get the rgba values for the colour
        rgba = colorConverter.to_rgba(col)
        # Initialise an array for the colormap using the first rgba value (r)
        cmap = np.full((N, 4), rgba[0])
        # Fill in g and b values
        cmap[:, 1] = rgba[1]
        cmap[:, 2] = rgba[2]
        # Fill the alpha values with a linear sequence from 0 to 1
        cmap[:, 3] = np.linspace(0, 1, N)
        # Create colormap from the array
        cmap = ListedColormap(cmap)

        return cmap

    def make_cmap_transparent(self, cmap_base):
        """
        Given a matplotlib colormap instance ('cmap_base') this function sets
        the alpha channel to a linear sequence from 0 to 1
        """
        import numpy as np
        from matplotlib.colors import ListedColormap

        cmap = cmap_base(np.arange(cmap_base.N))
        # Set alpha to linear scale
        cmap[:, -1] = np.linspace(0, 1, cmap_base.N)
        cmap = ListedColormap(cmap)

        return cmap

    def plot_infall_times(self, group=0, nbins=20, normed=True, check_before=0, outsuffix=""):
        """
        Here we estimate the infall time of stars which end up in satellites, ICL or BCG
        The infall time of a star is estimated as the time of the latest snapshot
        at which the star is not bound to the main halo or a subhalo in the same FoF group
        """
        import snap
        import matplotlib.pyplot as plt
        import numpy as np
        import h5py

        filepath = self.outdir+self.simdir+"new_star_props_cb" + \
            str(check_before)+"_grp"+str(group)+outsuffix+".hdf5"
        print "Opening", filepath
        with h5py.File(filepath, "r") as infile:
            # get the snapshot numbers
            snapnums = [int(key.replace("snap_", "")) for key in infile.keys()]
            # Initialise arrays containing infall times to -1
            # setting the size to the size of arrays in the latest snapshots
            # as these will be the largest
            grp = infile["snap_"+str(max(snapnums)).zfill(3)]
            gal_infall_z = np.full(len(grp['cur_gal_hostsubhalo']), -1.0)
            bcg_infall_z = np.full(len(grp['cur_bcg_hostsubhalo']), -1.0)
            icl_infall_z = np.full(len(grp['cur_icl_hostsubhalo']), -1.0)

            # Loop over all the snapshots (Except the last)
            for snapnum in snapnums[:-1]:
                print "Loading snapshot", snapnum
                # Load the FoF catalogue to determine if stars are bound to main halo
                sim = snap.snapshot(self.basedir+self.simdir, snapnum,
                                    mpc_units=self.mpc_units, masstab=self.masstab)

                grp = infile["snap_"+str(snapnum).zfill(3)]
                z = grp.attrs['redshift']
                assert z == sim.header.redshift, "redshift in hdf5 file and snapshot do not match."

                # Get host subhaloes of stars which end up in satellite galaxies
                gal_host = grp['cur_gal_hostsubhalo'].value
                # Get the indices of these stars which do not belong to the FoF group
                ind = np.where((gal_host < sim.cat.group_firstsub[group]) | (
                    gal_host >= sim.cat.group_firstsub[group+1]))[0]
                # For these stars, set the infall time as the time of this snapshot
                gal_infall_z[ind] = z

                bcg_host = grp['cur_bcg_hostsubhalo'].value
                ind = np.where((bcg_host < sim.cat.group_firstsub[group]) | (
                    bcg_host >= sim.cat.group_firstsub[group+1]))[0]
                bcg_infall_z[ind] = z

                icl_host = grp['cur_icl_hostsubhalo'].value
                ind = np.where((icl_host < sim.cat.group_firstsub[group]) | (
                    icl_host >= sim.cat.group_firstsub[group+1]))[0]
                icl_infall_z[ind] = z

        print "Found an infall time for {:.1f} per cent of satellite stars.".format(
            np.sum(gal_infall_z > -1)*100.0/len(gal_infall_z))
        print "Found an infall time for {:.1f} per cent of ICL stars.".format(
            np.sum(icl_infall_z > -1)*100.0/len(icl_infall_z))
        print "Found an infall time for {:.1f} per cent of BCG stars.".format(
            np.sum(bcg_infall_z > -1)*100.0/len(bcg_infall_z))

        fig, ax = plt.subplots(figsize=(7, 6))
        ax.set_xlabel("Scale factor a of infall")
        ax.set_xlim(0, 1)
        if normed:
            ax.set_ylabel("fraction per bin")
        else:
            ax.set_ylabel("number per bin")

        # Add redshifts to upper x axis
        zticks = [0, 0.25, 0.5, 1, 1.5, 2, 3, 4, 5]
        ax2 = ax.twiny()
        ax2.set_xlabel("redshift z of infall")
        ax2.set_xlim(0, 1)
        ax2.set_xticks(1.0/(1.0+np.array(zticks)))
        ax2.set_xticklabels([str(ztick) for ztick in zticks])
        ax2.set_title(self.simdir[:-1].replace("_",
                                               "\_")+" Group "+str(group), y=1.14)
        plt.tight_layout()

        bins = np.linspace(0, 1, num=nbins)
#        ax.hist(1.0/(1.0+gal_infall_z[gal_infall_z > -1]), histtype='step', bins=bins, label="Satellites", lw=3, normed=normed)
#        ax.hist(1.0/(1.0+bcg_infall_z[bcg_infall_z > -1]), histtype='step', bins=bins, label="BCG", lw=3, normed=normed)
#        ax.hist(1.0/(1.0+icl_infall_z[icl_infall_z > -1]), histtype='step', bins=bins, label="ICL", lw=3, normed=normed)
        nsats, bin_edges = np.histogram(
            1.0/(1.0+gal_infall_z[gal_infall_z > -1]), bins=bins)
        nbcg, bin_edges = np.histogram(
            1.0/(1.0+bcg_infall_z[bcg_infall_z > -1]), bins=bins)
        nicl, bin_edges = np.histogram(
            1.0/(1.0+icl_infall_z[icl_infall_z > -1]), bins=bins)
        if normed:
            print nsats
            nsats = nsats / float(np.sum(nsats))
            print nsats
            nbcg = nbcg / float(np.sum(nbcg))
            nicl = nicl / float(np.sum(nicl))
        ax.step(bins[1:], nsats, lw=3, label="Satellites")
        ax.step(bins[1:], nbcg, lw=3, label="BCG")
        ax.step(bins[1:], nicl, lw=3, label="ICL")
        ax.legend(fontsize='small')

        plotfile = self.outdir+self.simdir+"infall_dist" + \
            outsuffix+"_grp"+str(group)+".pdf"
        fig.savefig(plotfile)
        print "Saved map to ", plotfile
        return plotfile

    def plot_new_stars_vs_distance(self, snapnum_min=0, snapnum_max=100, nbins=0, minR=0.1, maxR=10000, norm_by_vol=False, fit=False, logx=False, normed=False, check_before=0, group=0, outsuffix="", for_paper=False):
        import sys
        import matplotlib.pyplot as plt
        import matplotlib.ticker as ticker
        import numpy as np
        import h5py

        if not hasattr(self, "snapnum_final"):
            self.snapnum_final = max(self.snapnums)
        if not hasattr(self, "group"):
            self.group = group

        if hasattr(self, "gal_new_rad_all"):
            del self.gal_new_rad_all
            del self.bcg_new_rad_all
            del self.icl_new_rad_all

        filepath = self.outdir+self.simdir+"new_star_props_cb" + \
            str(check_before)+"_grp"+str(group)+outsuffix+".hdf5"
        print "Opening", filepath
        with h5py.File(filepath, "r") as infile:
            for snapnum in range(snapnum_min, snapnum_max+1):
                if "snap_"+str(snapnum).zfill(3) in infile.keys():
                    group = infile["snap_"+str(snapnum).zfill(3)]
                    z = group.attrs['redshift']
                    try:
                        self.gal_new_rad = group['gal_new_rad'].value
                    except:
                        assert False, "Could not load gal_new_rad. Have you ran get_new_star_props with use_dist=true?"
                    self.bcg_new_rad = group['bcg_new_rad']
                    self.icl_new_rad = group['icl_new_rad']
                    # Concatenate onto list of radii for all snapshots and convert radii from comoving to physical coordinates
                    if hasattr(self, "gal_new_rad_all") and hasattr(self, "gal_new_rad"):
                        self.gal_new_rad_all = np.concatenate(
                            (self.gal_new_rad_all, self.gal_new_rad / (1.0 + z)))  # pkpc/h
                        self.bcg_new_rad_all = np.concatenate(
                            (self.bcg_new_rad_all, self.bcg_new_rad / (1.0 + z)))
                        self.icl_new_rad_all = np.concatenate(
                            (self.icl_new_rad_all, self.icl_new_rad / (1.0 + z)))
                    elif hasattr(self, "gal_new_rad"):
                        self.gal_new_rad_all = self.gal_new_rad / \
                            (1.0 + z)  # pkpc/h
                        self.bcg_new_rad_all = self.bcg_new_rad / (1.0 + z)
                        self.icl_new_rad_all = self.icl_new_rad / (1.0 + z)

        if len(self.gal_new_rad_all) == 0:
            print "ERROR: gal_new_rad_all is empty. Something wrong.."
            sys.exit(1)
        gal_new_rad_all = self.gal_new_rad_all[(
            self.gal_new_rad_all > max(0, minR)) & (self.gal_new_rad_all < maxR)]
        bcg_new_rad_all = self.bcg_new_rad_all[(
            self.bcg_new_rad_all > max(0, minR)) & (self.bcg_new_rad_all < maxR)]
        icl_new_rad_all = self.icl_new_rad_all[(
            self.icl_new_rad_all > max(0, minR)) & (self.icl_new_rad_all < maxR)]

        Nstars = len(gal_new_rad_all) + \
            len(bcg_new_rad_all) + len(icl_new_rad_all)
        gal_frac = float(len(gal_new_rad_all)) / Nstars
        bcg_frac = float(len(bcg_new_rad_all)) / Nstars
        icl_frac = float(len(icl_new_rad_all)) / Nstars

        print "Plotting..."
        plt.figure()
        if nbins == 0:  # estimate number of bins via Freedman-Diaconis rule
            n = len(icl_new_rad_all)
            x = np.log10(icl_new_rad_all)
            q75, q25 = np.percentile(x, [75, 25])
            nbins = np.ceil((np.max(x) - np.min(x)) /
                            (2 * (q75 - q25) / float(n)**(1.0/3.0)))

        if logx:
            mybins = np.linspace(np.log10(minR), np.log10(
                maxR), num=nbins, endpoint=True)
            gal_values, gal_bins = np.histogram(
                np.log10(gal_new_rad_all), bins=mybins, density=True)
            if norm_by_vol:
                gal_values /= 4.0*np.pi/3.0 * \
                    ((10**gal_bins[1:])**3 - (10**gal_bins[:-1])**3)
            plt.step(gal_bins[1:], gal_values, color=self.colours[0],
                     lw=1.6, label="Satellite Galaxies")
            bcg_values, bcg_bins = np.histogram(
                np.log10(bcg_new_rad_all), bins=mybins, density=True)
            plt.step(bcg_bins[1:], bcg_values,
                     color=self.colours[1], lw=1.6, label="BCG")
            icl_values, icl_bins = np.histogram(
                np.log10(icl_new_rad_all), bins=mybins, density=True)
            plt.step(icl_bins[1:], icl_values,
                     color=self.colours[2], lw=1.6, label="ICL")
            # Plot BCG + ICL
            #plt.hist(np.log10(np.concatenate((icl_new_rad_all, bcg_new_rad_all))), bins=mybins, histtype='step', label="BCG + ICL", normed=normed, color=self.colours[3], lw=1.6)
            plt.xlabel("log(Distance from halo centre [kpc/h])")
        else:
            mybins = np.logspace(np.log10(minR), np.log10(
                maxR), num=nbins, endpoint=True)
            gal_values, gal_bins = np.histogram(
                gal_new_rad_all, bins=mybins, density=normed)
            if norm_by_vol:
                gal_values = gal_values / \
                    (4.0*np.pi/3.0*(gal_bins[1:]**3 - gal_bins[:-1]**3))
            plt.step(gal_bins[1:], gal_values, color=self.colours[0], lw=1.6,
                     label="Satellite Galaxies "+'({:.0f}\%)'.format(gal_frac*100))

            bcg_values, bcg_bins = np.histogram(
                bcg_new_rad_all, bins=mybins, density=normed)
            if norm_by_vol:
                bcg_values = bcg_values / \
                    (4.0*np.pi/3.0*(bcg_bins[1:]**3 - bcg_bins[:-1]**3))
            plt.step(bcg_bins[1:], bcg_values, color=self.colours[1],
                     lw=1.6, label="BCG "+'({:.0f}\%)'.format(bcg_frac*100))

            icl_values, icl_bins = np.histogram(
                icl_new_rad_all, bins=mybins, density=normed)
            if norm_by_vol:
                icl_values = icl_values / \
                    (4.0*np.pi/3.0*(icl_bins[1:]**3 - icl_bins[:-1]**3))
            plt.step(icl_bins[1:], icl_values, color=self.colours[2],
                     lw=1.6, label="ICL "+'({:.0f}\%)'.format(icl_frac*100))

            #plt.hist(np.concatenate((icl_new_rad_all, bcg_new_rad_all)), bins=mybins, histtype='step', label="BCG + ICL", normed=normed, color=self.colours[3], lw=1.6)
            plt.xscale('log')
            plt.xlabel("Distance from halo centre [$h^{-1}$ kpc]")
            plt.gca().xaxis.set_major_formatter(ticker.FuncFormatter(lambda y, pos: (
                '{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y), 0)))).format(y)))

        if fit:
            import scipy.stats as stats
            import scipy.optimize
            from scipy.optimize import curve_fit
            if logx:
                '''
                ## Skewed normal distribution fit
                gal_values, gal_bins = np.histogram(np.log10(self.gal_new_rad_all[self.gal_new_rad_all > 0]), bins=mybins, density=True)
                #plt.step(gal_bins[1:], gal_values, c='r')
                skew_guess = stats.skew(np.log10(self.gal_new_rad_all[self.gal_new_rad_all > 0]))
                print "skew_guess = ", skew_guess
                popt, pcov = curve_fit(self.normskew, gal_bins[:-1], gal_values, p0=[0.5, 1, skew_guess])
                print "popt =", popt
                print "pcov =", pcov
                if normed:
                    plt.plot(x, self.normskew(x, popt[0], popt[1], popt[2]), 'r-')

                ## Exponentially modified Gaussian fit
                #gal_mean, gal_std = stats.norm.fit(np.log10(self.gal_new_rad_all[self.gal_new_rad_all > 0]))
                #przt = scipy.optimize.fmin(self.llh, np.array([gal_mean, gal_std, 1e-6]), (self.eg_pdf, np.log10(self.gal_new_rad_all[self.gal_new_rad_all > 0])))#, maxfun=1000)
                #print "przt =", przt
                #print self.llh(np.array([gal_mean, gal_std, 1e-6]), self.eg_pdf, np.log10(self.gal_new_rad_all[self.gal_new_rad_all > 0]))
                #print self.llh(przt, self.eg_pdf, np.log10(self.gal_new_rad_all[self.gal_new_rad_all > 0]))
                #if normed:
                    #plt.plot(x, self.eg_pdf(przt, x))
                '''
                # Gaussian fit
                # Note: fitting to all data, not just (minR, maxR) (little difference)
                gal_mean, gal_std = stats.norm.fit(
                    np.log10(self.gal_new_rad_all[self.gal_new_rad_all > 0]))
                bcg_mean, bcg_std = stats.norm.fit(
                    np.log10(self.bcg_new_rad_all[self.bcg_new_rad_all > 0]))
                #icl_mean, icl_std = stats.norm.fit(np.log10(self.icl_new_rad_all[self.icl_new_rad_all > 0]))
                print "gal_mean =", gal_mean
                print "gal_stdev =", gal_std
                print "bcg_mean =", bcg_mean
                print "bcg_stdev =", bcg_std
                #x = np.linspace(np.log10(minR), np.log10(maxR), num=nbins*10, endpoint=True)
                x = (mybins[1:] + mybins[:-1]) / 2.0
                gal_pdf = stats.norm.pdf(x, gal_mean, gal_std)
                bcg_pdf = stats.norm.pdf(x, bcg_mean, bcg_std)
                #icl_pdf = stats.norm.pdf(x, icl_mean, icl_std)
                #icl_values, icl_bins = np.histogram(np.log10(icl_new_rad_all), bins=mybins, density=normed)
                guess = [np.max(icl_values), bcg_mean, bcg_std, np.max(
                    icl_values)/5.0, bcg_mean+2.0, bcg_std*2.0]
                print "guess = ", guess
                popt, pcov = curve_fit(
                    self.normal_double, (icl_bins[1:]+icl_bins[:-1])/2.0, icl_values, p0=guess)
                icl_fit = self.normal_double(x, *popt)
                print "popt = ", popt
                print "stdev =", np.sqrt(np.diag(pcov))
                icl_fit_1 = popt[0] * np.exp(-((x - popt[1])/popt[2])**2.0)
                icl_fit_2 = popt[3] * np.exp(-((x - popt[4])/popt[5])**2.0)

                # Logistic distribution fit
                gal_log_mean, gal_log_std = stats.logistic.fit(
                    np.log10(gal_new_rad_all))
                gal_log_pdf = stats.logistic.pdf(x, gal_log_mean, gal_log_std)
                bcg_log_mean, bcg_log_std = stats.logistic.fit(
                    np.log10(bcg_new_rad_all))
                bcg_log_pdf = stats.logistic.pdf(x, bcg_log_mean, bcg_log_std)
                #icl_log_mean, icl_log_std = stats.logistic.fit(np.log10(icl_new_rad_all))
                #icl_log_pdf = stats.logistic.pdf(x, icl_log_mean, icl_log_std)
                icl_values, icl_bins = np.histogram(
                    np.log10(icl_new_rad_all), bins=mybins, density=normed)
                guess = [np.max(icl_values), bcg_log_mean, bcg_log_std, np.max(
                    icl_values)/5.0, bcg_log_mean+2.0, bcg_log_std*2.0]
                print "guess =", guess
                # , sigma=np.sqrt(icl_values), absolute_sigma=True)
                popt_log, pcov_log = curve_fit(
                    self.logistic_double, (icl_bins[1:] + icl_bins[:-1])/2.0, icl_values, p0=guess)
                icl_log_fit = self.logistic_double(x, *popt_log)
                print "popt_log = ", popt_log
                print "stdev =", np.sqrt(np.diag(pcov_log))

                # Integration
                import scipy.integrate

                def f1(x): return popt[0] * \
                    np.exp(-((x - popt[1])/popt[2])**2.0)

                def f2(x): return popt[3] * \
                    np.exp(-((x - popt[4])/popt[5])**2.0)
                I1 = scipy.integrate.quad(f1, -np.inf, np.inf)
                I2 = scipy.integrate.quad(f2, -np.inf, np.inf)
                print "Gaussian 1 integral =", I1
                print "Gaussian 2 integral =", I2

                '''
                ## Gaussian mixture
                from sklearn.mixture import GMM
                n = 3
                N = np.arange(1,n+1) # fit models with 1 to n components
                models = [None for i in range(n)]
                #X = np.log10(self.icl_new_rad_all[self.icl_new_rad_all > 0]) #data
                X = np.log10(np.concatenate((self.icl_new_rad_all[self.icl_new_rad_all > 0],self.bcg_new_rad_all[self.bcg_new_rad_all > 0]))) #data
                for i in range(n):
                    print "Fitting", N[i], "gaussians..."
                    models[i] = GMM(N[i]).fit(X)
                AIC = [m.aic(X) for m in models]
                BIC = [m.bic(X) for m in models]
                M_best = models[np.argmin(AIC)] # best model (minimum AIC)
                #x = mybins
                logprob, responsibilities = M_best.score_samples(x)
                pdf = np.exp(logprob)
                pdf_individual = responsibilities * pdf[:, np.newaxis]
                if normed:
                    plt.plot(x, pdf, '-k', label="best fit GMM n="+str(n))
                    plt.plot(x, pdf_individual, '--k')
                else:
                    plt.plot(x, pdf*len(X)/nbins*(np.log10(maxR)-np.log10(minR)), '-k', label="best fit GMM n="+str(n))
                    plt.plot(x, pdf_individual*len(X)/nbins*(np.log10(maxR)-np.log10(minR)), '--k')
                '''
                '''
                ## Integration
                import scipy.integrate
                for i in range(len(M_best.means_)):
                    f = lambda x : M_best.weights_[i] * stats.norm.pdf(x, loc=M_best.means_[i], scale=M_best.covars_[i])
                    integral, error = scipy.integrate.quad(f, np.log10(minR), np.log10(maxR))
                    print "integral =", integral, "+/-", error
                '''

                if normed:
                    #plt.plot(x, gal_log_pdf, 'r-', label=r"Logistic: $\mu$ = "+'{:.3f}'.format(gal_log_mean)+" $\sigma$ = "+'{:.3f}'.format(gal_log_std*1.8138))
                    #plt.plot(x, bcg_log_pdf, 'g-', label=r"Logistic: $\mu$ = "+'{:.3f}'.format(bcg_log_mean)+" $\sigma$ = "+'{:.3f}'.format(bcg_log_std*1.8138))
                    #plt.plot(x, icl_log_fit, 'b-', label="Logistic: $\mu_1$ = "+'{:.3f}'.format(popt_log[1])+" $\sigma_1$ = "+'{:.3f}'.format(popt_log[2])+" $\mu_2$ = "+'{:.3f}'.format(popt_log[4])+" $\sigma_2$ = "+'{:.3f}'.format(popt[5]))
                    #plt.plot(x, gal_pdf, 'r--', label=r"Gaussian: $\mu$ = "+'{:.3f}'.format(gal_mean)+" $\sigma$ = "+'{:.3f}'.format(gal_std))
                    #plt.plot(x, bcg_pdf, 'g-', label=r"Gaussian: $\mu$ = "+'{:.3f}'.format(bcg_mean)+" $\sigma$ = "+'{:.3f}'.format(bcg_std))
                    plt.plot(x, icl_fit, 'b-', label="$\mu_1$ = "+'{:.3f}'.format(popt[1])+" $\sigma_1$ = "+'{:.3f}'.format(popt[2])+"\n$\mu_2$ = "+'{:.3f}'.format(
                        popt[4])+" $\sigma_2$ = "+'{:.3f}'.format(popt[5])+"\nInt1 = "+'{:.3f}'.format(I1[0])+" Int2 = "+'{:.3f}'.format(I2[0]))
                    plt.plot(x, icl_fit_1, 'b--')
                    plt.plot(x, icl_fit_2, 'b--')

                # else:
                    #plt.plot(x, gal_pdf*len(gal_new_rad_all)/nbins*(np.log10(maxR)-np.log10(minR)), 'r--', label=r"$\mu$ = "+'{:.3f}'.format(gal_mean)+" $\sigma$ = "+'{:.3f}'.format(gal_std))
                    #plt.plot(x, bcg_pdf*len(bcg_new_rad_all)/nbins*(np.log10(maxR)-np.log10(minR)), 'g--', label=r"$\mu$ = "+'{:.3f}'.format(bcg_mean)+" $\sigma$ = "+'{:.3f}'.format(bcg_std))
                    #plt.plot(x, icl_fit, 'b--')
                    #plt.plot(x, gal_log_pdf*len(gal_new_rad_all)/nbins*(np.log10(maxR)-np.log10(minR)), 'r-', label=r"$\mu$ = "+'{:.3f}'.format(gal_log_mean)+" $\sigma$ = "+'{:.3f}'.format(gal_log_std*1.8138))
                    #plt.plot(x, bcg_log_pdf*len(bcg_new_rad_all)/nbins*(np.log10(maxR)-np.log10(minR)), 'g-', label=r"$\mu$ = "+'{:.3f}'.format(bcg_log_mean)+" $\sigma$ = "+'{:.3f}'.format(bcg_log_std*1.8138))
                    #plt.plot(x, icl_log_fit, 'b-')

            elif not logx:
                gal_shape, gal_loc, gal_scale = stats.lognorm.fit(
                    self.gal_new_rad_all[self.gal_new_rad_all > 0], floc=0)
                # np.exp(gal_loc+(gal_shape**2.0)/2.0)
                print "gal_mean =", gal_scale
                print "gal_shape =", gal_shape
                bcg_shape, bcg_loc, bcg_scale = stats.lognorm.fit(
                    self.bcg_new_rad_all[self.bcg_new_rad_all > 0], floc=0)
                print "bcg_mean =", bcg_scale
                print "bcg_shape =", bcg_shape
                x = 10**(np.log10(mybins[:-1]) +
                         ((np.log10(mybins[1:])-np.log10(mybins[:-1]))/2.0))
                gal_pdf = stats.lognorm.pdf(
                    x, gal_shape, loc=gal_loc, scale=gal_scale)
                bcg_pdf = stats.lognorm.pdf(
                    x, bcg_shape, loc=bcg_loc, scale=bcg_scale)
                if normed:
                    plt.plot(x, gal_pdf, 'r-', label=r"$\mu$ = "+'{:.3f}'.format(
                        np.log10(gal_scale))+" $\sigma$ = "+'{:.3f}'.format(gal_shape))
                    plt.plot(x, bcg_pdf, 'g-', label=r"$\mu$ = "+'{:.3f}'.format(
                        np.log10(bcg_scale))+" $\sigma$ = "+'{:.3f}'.format(bcg_shape))
                else:
                    plt.plot(x, gal_pdf*(mybins[1:] - mybins[:-1])*len(gal_new_rad_all), 'r-', label=r"$\mu$ = "+'{:.3f}'.format(
                        np.log10(gal_scale))+" $\sigma$ = "+'{:.3f}'.format(gal_shape))
                    plt.plot(x, bcg_pdf*(mybins[1:] - mybins[:-1])*len(bcg_new_rad_all), 'g-', label=r"$\mu$ = "+'{:.3f}'.format(
                        np.log10(bcg_scale))+" $\sigma$ = "+'{:.3f}'.format(bcg_shape))

        if normed:
            plt.ylabel("Probability Density")
        elif norm_by_vol:
            plt.ylabel("Stars formed per unit volume per bin", labelpad=10)
        else:
            plt.ylabel("Stars formed per bin", labelpad=10)
            #ymin, ymax = plt.gca().get_ylim()
            #plt.yticks(np.arange(0, np.ceil(ymax / 1000.0)*1000.0 + 1000.0, 4000.0))
            #plt.gca().yaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: r'{}$\times$10$^{{{}}}$'.format('{:.0e}'.format(y).split('e')[0], int('{:e}'.format(y).split('e')[1]))))
        plt.tight_layout()
        plt.legend(loc='best', frameon=False,
                   borderaxespad=1, numpoints=1, fontsize=12)
        if not for_paper:
            plt.title(self.simdir[:-1]+"\nGroup "+str(self.group), fontsize=12)
        plotfile = self.outdir+self.simdir+"stars_formed_v_distance_cb" + \
            str(check_before)+"_snap"+str(snapnum_min)+"-" + \
            str(self.snapnum_final)+"_grp"+str(self.group)+".pdf"
        plt.savefig(plotfile, bbox_inches='tight')
        print "Plot saved to", plotfile
        return plotfile

    def normskew(self, x, loc, w, skew):
        # loc would be the mean in an unskewed distribution
        import scipy.stats as stats
        t = (x-loc) / w
        return 2 * stats.norm.pdf(t) * stats.norm.cdf(skew*t)

    # define pdf for Exponentially modified Gaussian distribution
    def eg_pdf(self, p, x):
        import scipy.special as sse
        import numpy as np
        m = p[0]  # mu
        s = p[1]  # sigma
        l = p[2]  # lambda
        return 0.5*l*np.exp(0.5*l*(2*m+l*s*s-2*x))*sse.erfc((m+l*s*s-x)/(np.sqrt(2)*s))

    # negative log-likelihood function
    def llh(self, p, f, x):
        import numpy as np
        return -np.sum(np.log(f(p, x)))

    def normal_double(self, x, *p):
        import numpy as np
        amp1 = p[0]
        m1 = p[1]
        s1 = p[2]
        amp2 = p[3]
        m2 = p[4]
        s2 = p[5]
        y1 = amp1 * np.exp(-((x - m1)/s1)**2.0)
        y2 = amp2 * np.exp(-((x - m2)/s2)**2.0)
        return y1 + y2

    def gaussian(self, x, amp, m, s):
        import scipy.stats as stats
        return amp * stats.norm.pdf(x, loc=m, scale=s)

    def logistic_double(self, x, *p):
        import numpy as np
        amp1 = p[0]
        m1 = p[1]
        s1 = p[2]
        amp2 = p[3]
        m2 = p[4]
        s2 = p[5]
        y1 = amp1 / s1 * np.exp(-(x-m1)/s1) / (1 + np.exp(-(x-m1)/s1))**2.0
        y2 = amp2 / s2 * np.exp(-(x-m2)/s2) / (1 + np.exp(-(x-m2)/s2))**2.0
        return y1 + y2


if __name__ == "__main__":
    main()
