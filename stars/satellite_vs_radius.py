#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Jun  7 10:39:03 2019
Plot satellite radial distributions.
@author: nh444
"""
from __future__ import print_function

def main():
    import numpy as np

    outdir = "/data/curie4/nh444/project1/stars/"
#    outdir = "/data/vault/nh444/VaultDocs/meetings/andrey1/"
    basedir = "/data/curie4/nh444/project1/zoom_ins/MDRInt/"
    simdirs = [
            "c96_MDRInt", "c128_MDRInt", "c160_MDRInt",
            "c192_MDRInt", "c224_MDRInt", "c256_MDRInt",
            "c288_MDRInt", "c320_MDRInt",
            "c352_MDRInt", "c384_MDRInt",
            "c448_MDRInt", "c480_MDRInt", "c512_MDRInt",
            "c112_MDRInt", "c144_MDRInt", "c176_MDRInt", "c208_MDRInt",
            "c240_MDRInt", "c272_MDRInt", "c304_MDRInt", "c336_MDRInt",
            "c368_MDRInt", "c400_MDRInt", "c410_MDRInt", "c432_MDRInt",
            "c464_MDRInt", "c496_MDRInt_SUBF",
            ]
    snapnums = [25]*13+[99]*14
    # snapnums = [99, 99]
    mpc_units = True

    Rbins = np.logspace(-1, 1, num=21)
    RbinType = "R500"
    logm_bins = np.linspace(9, 11.5, num=6)
    plot_sat_radial_dist(basedir, simdirs, snapnums,
                         logm_bins, Rbins=Rbins, RbinType=RbinType,
                         projected=False, zaxis=0,
                         normalise=False,
                         M500lims=[3e14, 1e16],
                         outsuffix="_M500_gt_3e14",
                         mpc_units=mpc_units, outdir=outdir)
#    logm_bins = np.linspace(9, 11, num=5)
#    Rbins = np.logspace(-1, 1, num=9)
#    RbinType = "R500"
#    plot_sat_radial_dist(basedir, simdirs, snapnums,
#                         logm_bins, Rbins=Rbins, RbinType=RbinType,
#                         projected=False, zaxis=0,
#                         ratio=True,
#                         M500lims=[3e14, 1e16],
#                         outsuffix="_M500_gt_3e14_ratio",
#                         mpc_units=mpc_units, outdir=outdir)


def plot_sat_radial_dist(basedir, simdirs, snapnums,
                         logm_bins, Rbins=None, RbinType="R500",
                         projected=False, zaxis=0,
                         M200lims=None, M500lims=None,
                         sats_only=True,
                         normalise=False, ratio=False,
                         mpc_units=True, h_scale=0.6774, verbose=True,
                         outdir="/data/curie4/nh444/project1/stars/",
                         outsuffix=""):
    """Plot the radial number density of satellites.

    snapnums: snapshot number for each directory in simdirs.
    logm_bins: Stellar mass bins in log10([Msun]). Should be a list of lists,
               each of length 2 giving the lower and upper limits of each bin.
    sats_only: Count only bound satellite galaxies. Otherwise all galaxies.
    M200lims: (list length 2) Lower and upper limits on M200 in Msun.
    M500lims: (list length 2) Lower and upper limits on M500 in Msun.
    projected: (bool) Calculate SMF in a 2D radius (Rlim), otherwise 3D.
    zaxis: (int) Index for the projection axis (0, 1 or 2).
    normalise: (bool) Normalise the curves such that the total number of
               galaxies in each stellar mass bin is the same.
    ratio: (bool) Plot the number density curve of each stellar mass bin as a
           ratio of the first.
    """
    import matplotlib.pyplot as plt
    import numpy as np
    import snap
    from get_uncontaminated_groups import get_uncontaminated_groups

    if M200lims is None:
        M200lims = [0.0, np.inf]
    if M500lims is None:
        M500lims = [0.0, np.inf]

    if len(snapnums) != len(simdirs):
        raise ValueError("Need exactly one snapshot number for each simdir.")

    axes = [0, 1, 2]  # axis indices
    assert zaxis in axes, "zaxis must be 0, 1 or 2"
    # remove z axis index, use remaining two for projection axes
    axes.remove(zaxis)

    logm_bins = np.array(logm_bins)
    if Rbins is None:
        Rbins = np.logspace(-1, 1, num=11)

    if RbinType == "R500":
        scale_radius = "R$_{500}$"
    elif RbinType == "R200":
        scale_radius = "R$_{200}$"
    else:
        raise ValueError("RbinType of " + str(RbinType) +
                         " not recognised.")

    fig, ax = plt.subplots(figsize=(7, 7))
    if ratio:
        ax.set_ylim(0.6, 10)
    else:
        ax.set_yscale('log')
    ax.set_xscale('log')
    ax.set_xlim(Rbins[0], Rbins[-1])
    ax.set_xlabel("R / " + scale_radius)
    if projected:
        ax.set_ylabel(r"Surface Number Density " +
                      ("Ratio" if ratio else "[" + scale_radius + "$^{-2}$]"))
    else:
        ax.set_ylabel(r"Number Density " +
                      ("Ratio" if ratio else "[" + scale_radius + "$^{-3}$]"))
    if normalise:
        ax.text(0.07, 0.93, r"normalised to log$_{10}$M$_{\star}$ = " +
                "[{:.1f}, {:.1f}]".format(logm_bins[0], logm_bins[1]),
                ha='left', va='top', transform=ax.transAxes)

    # Create 2D array for total number of satellites per radius and mass bin.
    hist_stack = np.zeros((Rbins.shape[0] - 1, logm_bins.shape[0] - 1))
    nstack = 0
    for idx, simdir in enumerate(simdirs):
        # Load subfind catalogue with all subhalo properties.
        sim = snap.snapshot(basedir+simdir, snapnums[idx],
                            # extra_sub_props=True,
                            mpc_units=mpc_units)
        # Get indices of groups in halo mass range.
        M200 = sim.cat.group_m_crit200 * 1e10 / h_scale
        M500 = sim.cat.group_m_crit500 * 1e10 / h_scale
        grpind = np.where((M200 > M200lims[0]) & (M200 < M200lims[1]) &
                          (M500 > M500lims[0]) & (M500 < M500lims[1]))[0]
        if len(grpind) == 0:
            continue
        # Remove groups that are contaminated by low-res particles.
        grpind = get_uncontaminated_groups(sim, grpind)
        for grpnum in grpind:
            if verbose:
                print(simdir, "grp", grpnum,
                      "log10(M200c) = {:.2f}".format(
                               np.log10(sim.cat.group_m_crit200[grpnum] *
                                        1e10 / h_scale)))
            # Calculate radial distance of all satellites.
            if sats_only:
                # Consider all subhaloes in group other than the main halo.
                subnums = np.arange(sim.cat.group_firstsub[grpnum] + 1,
                                    sim.cat.group_firstsub[grpnum] +
                                    sim.cat.group_nsubs[grpnum])
            else:
                # Consider all subhaloes (even unbound) except for the BCG.
                subnums = range(sim.cat.nsubs)
                # remove BCG from list.
                subnums.pop(sim.cat.group_firstsub[grpnum])
                subnums = np.array(subnums)
            Mstar = sim.cat.sub_massinradtab[subnums, 4] * 1e10 / h_scale
            if projected:
                rel_pos = sim.rel_pos(sim.cat.sub_pos[subnums],
                                      sim.cat.group_pos[grpnum])
                r = np.sqrt((rel_pos[:, axes[0]])**2 +
                            (rel_pos[:, axes[1]])**2)
            else:
                r = sim.nearest_dist_3D(sim.cat.sub_pos[subnums],
                                        sim.cat.group_pos[grpnum])
            # Get radial bins into code units.
            if RbinType == "R500":
                Rbins_cur = Rbins * sim.cat.group_r_crit500[grpnum]
            elif RbinType == "R200":
                Rbins_cur = Rbins * sim.cat.group_r_crit200[grpnum]
            else:
                raise ValueError("RbinType of " + str(RbinType) +
                                 " not recognised.")
            # Bin by radial distance and stellar mass.
            hist_cur, _, _ = np.histogram2d(r, np.log10(Mstar),
                                            bins=[Rbins_cur, logm_bins])
            # Sum
            hist_stack += hist_cur
            nstack += 1
            # I could use np.stack with axis=-1 to stack along a new third axis
    hist_stack /= nstack
    if normalise:
        # Get average number of galaxies in each mass bin.
        num_per_logm_bin = np.sum(hist_stack, axis=0)
    if not ratio:
        # Divide by comoving area/volume of each radial bin
        # in SCALED units.
        if projected:
            area = np.pi * (Rbins[1:]**2 - Rbins[:-1]**2)
            hist_stack /= area[:, None]
        else:
            vol = 4.0/3.0 * np.pi * (Rbins[1:]**3 - Rbins[:-1]**3)
            hist_stack /= vol[:, None]
    for i in range(logm_bins.shape[0] - 1):
        if normalise:
            # Normalise to the first bin.
            norm = num_per_logm_bin[0] / num_per_logm_bin[i]
        else:
            norm = 1.0
        label = r"log$_{10}$M$_{\star}$ = " + "[{:.1f}, {:.1f}]".format(
                        logm_bins[i], logm_bins[i+1])
        if ratio:
            y = (np.ma.masked_equal(hist_stack[:, i], 0.0) /
                 np.ma.masked_equal(hist_stack[:, -1], 0.0))
        else:
            y = np.ma.masked_equal(hist_stack[:, i], 0.0) * norm
        if ratio:
            # Plot bin edges.
            ax.step(Rbins[:-1], y, where='post', lw=3, label=label)
        else:
            # Plot bin centres.
            ax.plot((Rbins[1:] * Rbins[:-1])**0.5, y, lw=3, label=label)
    ax.legend(fontsize='x-small')
    fig.savefig(outdir + "sat_density_radius" + outsuffix + ".pdf",
                bbox_inches='tight')
    print("Saved to", outdir + "sat_density_radius" + outsuffix + ".pdf")


if __name__ == "__main__":
    main()
