#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  3 20:26:25 2017
Plot SFR or SFRD versus redshift from either sfr.txt or snapshots
@author: nh444
"""
import numpy as np
import matplotlib.pyplot as plt
import snap
final_snap = 25
h_scale = 0.679

indirs = ["/data/emergence4/deboras/Elliptical_ZoomIns/L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff/",
          "/data/curie4/nh444/project1/boxes/L40_512_LDRIntRadioEffBHVel/",
          "/data/curie4/nh444/project1/boxes/L40_512_LDRIntRadioEff_SoftSmall/",
           ]
final_snap = 7
mpc_units=False
usehighres=False
main_halo_only=False
labels = ["small softenings", "larger softenings"]
labels = ["$\epsilon$(z=0)=0.3375 $z_{\epsilon}=3$", "$\epsilon$(z=0)=2.4 $z_{\epsilon}=5$", "$\epsilon$(z=0)=2.4 $z_{\epsilon}=1$"]
outname = "/data/curie4/nh444/project1/stars/sfr_box_LowHigh_Softening.pdf"
tmax = 1.0


indirs = ["/data/curie4/nh444/project1/boxes/L40_512_LDRIntRadioEff_noBH/",
          "/data/curie4/nh444/project1/boxes/L40_512_NoDutyRadioWeak/",
          "/data/curie4/nh444/project1/boxes/L40_512_NoDutyRadioInt/",
          "/data/curie4/nh444/project1/boxes/L40_512_DutyRadioWeak/",
          "/data/curie4/nh444/project1/boxes/L40_512_LDRIntRadioEffBHVel/",
          "/data/curie4/nh444/project1/boxes/L40_512_LDRHighRadioEff/",
          "/data/curie4/nh444/project1/boxes/L40_512_MDRIntRadioEff/",
          ]
labels = ["No BHs",
          "Weak radio",
          "Stronger radio",
          "Quasar duty cycle",
          "Fiducial",#"Combined",
          "Longer radio duty",
          "Longer quasar duty",
          ]
final_snap = 25
mpc_units=False
usehighres=False
main_halo_only=False
outname = "/data/curie4/nh444/project1/stars/SFRD_models.pdf"
tmax = 1.0

indirs = ["/data/curie4/nh444/project1/boxes/L40_512_MDRIntRadioEff/",
          ]
labels = ["fiducial",
          ]
final_snap = 25
mpc_units=False
usehighres=False
main_halo_only=False
outname = "/data/curie4/nh444/project1/stars/SFRD.pdf"
tmax = 1.0

#==============================================================================
# indirs = [#"/data/curie4/nh444/project1/boxes/L40_512_NoDutyRadioWeak_HalfThermal_FixEta/",
#           #"/data/curie4/nh444/project1/boxes/L40_512_NoDutyRadioInt_HalfThermal_FixEta/",
#           #"/data/curie4/nh444/project1/boxes/L40_512_LowDutyRadioWeak_HalfThermal_FixEta/",
#           "/data/curie4/nh444/project1/boxes/L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff/",
#           ]
# labels = [#"Ref (small soft)",
#           #"Stronger radio (small soft)",
#           #"Quasar duty cycle (small soft)",
#           "Combined (small soft)",
#           ]
# final_snap = 25
# mpc_units=False
# usehighres=False
# main_halo_only=False
# outname = "/data/curie4/nh444/project1/stars/sfr_halo_mass_soft.pdf"
# tmax = 1.0
#==============================================================================

#==============================================================================
# indirs = ["/data/curie4/nh444/project1/boxes/L40_512_DutyIntRadioWeak/",
#         "/data/curie4/nh444/project1/boxes/L40_512_DutyIntRadioWeak_Thermal/",
#         "/data/curie4/nh444/project1/boxes/L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff/",
#         "/data/curie4/nh444/project1/boxes/L40_512_DutyInt1RadioWeak_Thermal_FixEta/",
#         "/data/curie4/nh444/project1/boxes/L40_512_LDRIntRadioEffBHVel/",
#            ]
# labels = [r"E$_{\mathrm{SNII},51}= 1$, $U_{therm} = 0$ (small $\epsilon$)",
#           r"E$_{\mathrm{SNII},51}= 1$, $U_{therm} = E_k$ (small $\epsilon$)",
#           r"E$_{\mathrm{SNII},51}= 1.5$, $U_{therm} = 0.5 E_k$ (small $\epsilon$)",#U$_{\mathrm{wind}} = 2.25$ $\frac{3}{2} \sigma_{DM,1D}^{2}$
#           r"E$_{\mathrm{SNII},51}= 2$, $U_{therm} = E_k$ (small $\epsilon$)",
#           r"E$_{\mathrm{SNII},51}= 1.5$, $U_{therm} = 0.5 E_k$ (larger $\epsilon$)",
#           ]
# final_snap = 25
# mpc_units=False
# usehighres=False
# main_halo_only=False
# outname = "/data/curie4/nh444/project1/stars/sfr_winds_comparison.pdf"
# tmax = 1.0
#==============================================================================

#==============================================================================
# indirs = ["/data/curie4/nh444/project1/boxes/L40_512_LowDutyRadioWeak_HalfThermal_FixEta/",
#           "/data/emergence4/deboras/Elliptical_ZoomIns/L40_512_LowDutyRadioWeak_HalfThermal_FixEta_BHChangeVel/",
#           "/data/curie4/nh444/project1/boxes/L40_512_DutyRadioWeak/",
#            ]
# final_snap = 25
# mpc_units=False
# usehighres=False
# main_halo_only=False
# labels = ["small soft, $\epsilon_r = 0.2$", "small soft, $\epsilon_r = 0.1$", "large soft, $\epsilon_r = 0.1$"]
# outname = "/data/curie4/nh444/project1/stars/sfr_box_LowHigh_Softening_DutyRadioWeakBHVel.pdf"
# tmax = 1.0
#==============================================================================


#==============================================================================
# indirs = ["/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c128_box_Arepo/",
#           #"/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c128_new/",
#           #"/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c128_new_noBHVel/",
#           #"/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c128_fixed/",
#           #"/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c128_new_ThermFeedbackFix/",
#           "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c128_new_FeedbackFix/",
#           "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c128_new_FeedbackFix_OldGrav/",
#           "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c128_Arepo_March15/",
#           "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c128_Arepo_May31/",
#           ]
# final_snap = 25
# mpc_units=True
# usehighres=True
# main_halo_only=False
# labels=["c128 old arepo", #"c128 Jan arepo", "c128 new arepo no BHVel", "c128 less new arepo", "c128 Jan 2xfeedback fix",
#         "c128 Jan arepo", "c128 Jan arepo old grav", "c128 March arepo", "c128 May arepo"]
# outname = "/data/curie4/nh444/project1/stars/sfr_c128_old_new_arepo.pdf"
# tmax=1.0
#==============================================================================

#==============================================================================
# indirs = ["/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c256_box_Arepo/",
#           "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c256_fixed/"]
# final_snap = 25
# mpc_units=True
# usehighres=True
# main_halo_only=False
# labels=["c256 old arepo", "c256 new arepo"]
# outname = "/data/curie4/nh444/project1/stars/sfr_c256_old_new_arepo.pdf"
# tmax=1.0
#==============================================================================

#==============================================================================
# indirs = ["/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c256/",
#           "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel_fix_metal_cooling/c256/"]
# final_snap = 25
# mpc_units=True
# usehighres=True
# main_halo_only=False
# labels=["c256", "c256 fixed metal cool"]
# outname = "/data/curie4/nh444/project1/stars/sfr_c256_fix_metal_cooling.pdf"
# tmax=1.0
#==============================================================================

#==============================================================================
# indirs = ["/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c96_box_Arepo/",
#           #"/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c96_box_Arepo_RKLSF",
#           #"/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c96_box_Arepo_2/",
#           "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c96_box_Arepo_new/",
#           "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c96_box_Arepo_new_PM1024/",
#           "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c96_box_Arepo_new_Ngb",
#           ##"",#"/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c96/",
#           ##"/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c96_ThermFeedbackFix/",
#           ##"/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c96_Arepo_March15/",
#           #"/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c96_FeedbackFix/",
#           #"/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c96_FeedbackFix_OldGrav/",
#           ##"/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c96_new_FeedbackFix_OldGrav/",
#           #"/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c96_FeedbackFix_OldGrav_3",
#           #"/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c96_FeedbackFix_OldGrav_2/",
#           #"/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c96_FeedbackFix_OldGrav_2_MHGG",
#           ##"/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c96_new_FeedbackFix_OldGrav/",
#           ]
# outname = "/data/curie4/nh444/project1/stars/sfr_c96_old_new_arepo.pdf"
# labels=["old arepo", #"old arepo RK + LSF",
#         #"new arepo", #"c96 new 2xfeedback fix", #"c96 March Arepo",
#         #"old arepo no IND\_GRAV\_SOFT",
#         "old arepo modified", "old arepo modified PMGRID 1024",
#         "old arepo modified Ngb 256",
#         "new arepo", "+ old gravity solver", #"+ old gravity solver [Jan arepo]",
#         #"+ IND\_GRAV\_SOFT",
#         "+ RCUT + IND\_GRAV\_SOFT", "+ MH + GG", "c96 Jan feedback mode fix, old grav"]
#==============================================================================

#==============================================================================
# indirs = ["/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c96_box_Arepo_new/",
#            "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c96_box_Arepo_new_BHVel/",
#            "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c96_box_Arepo_new_Ngb/",
#            #"/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c96_box_Arepo_new_DRAINGAS_2/",
#            #"/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c96_box_Arepo_new_PM1024/",
#            "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c96_box_Arepo_new_LowSoft/",
#            "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c96_box_Arepo_new_LowSoft_BHVel/",
#            "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c96_box_Arepo_new_LowSoft_BHVel_Ngb/",
#            #"/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c96_box_Arepo_new_LowSoft_BHVel_Ngb_PM1024_DRAINGAS2/",
#            ]
# outname = "/data/curie4/nh444/project1/stars/sfr_c96_old_arepo_BHVel_NumNgb.pdf"
# labels = ["c96 Large Soft", "c96 Large Soft with BHVel", "c96 Large Soft 256 Ngb", #"c96 DRAINGAS 2",  "c96 PMGRID 1024",
#           "c96 Small Soft", "c96 Small Soft BHVel", "c96 Small Soft BHVel Ngb 256", "c96 Small Soft BHVel 256 Ngb DRAINGAS=2 PM=1024"]
#==============================================================================


#==============================================================================
# indirs = ["/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c128_box_Arepo/",
#          "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c128_box_Arepo_RKLSF/",
#          "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c128_new_FeedbackFix/",
#          "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c128_new_FeedbackFix_OldGrav/",
#          ]
# outname = "/data/curie4/nh444/project1/stars/sfr_c128_old_new_arepo.pdf"
# labels = ["old arepo",
#           "old arepo RK + LSF",
#           "new arepo",
#           "+ old gravity solver",
#           "+ RCUT + IND\_GRAV\_SOFT",
#           "+ MH + GG",
#           ]
#==============================================================================

#==============================================================================
# indirs = [#"/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c256_box_Arepo_new_LowSoft/", "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c256_box_Arepo_new/",
#           "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c320_box_Arepo_new_LowSoft/", "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c320_box_Arepo_new/",
#           #"/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c384_box_Arepo_new_LowSoft/", "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c384_box_Arepo_new/",
#           ]
# outname = "/data/curie4/nh444/project1/stars/sfr_c320_small_large_softening.pdf"
# labels = [#"c256 small soft", "c256 large soft",
#           "c320 small soft", "c320 large soft",
#           #"c384 small soft", "c384 large soft",
#           ]
# final_snap = 25
# mpc_units=True
# usehighres=True
# main_halo_only=False
# tmax=1.0
#==============================================================================

from_txt = False
from_snap = True

redshift_lin = False ## plot vs z on a linear scale rather than time (Scale factor)
redshift_log = True ## plot vs z+1 on a log scale rather than time (Scale factor)

density=True ## plot SFR density rather than absolute SFR
plot_obs = True
plot_BHmass = False
byStellarMass = True ## plot SFR contribution from subhaloes of different stellar masses
byHaloMass = False
comp_aper = False
assert sum([byStellarMass, byHaloMass, comp_aper]) <= 1, "Cannot set more than one of byStellarMass, byHaloMass, comp_aper"

#from palettable.colorbrewer.qualitative import Dark2_6_r as pallette
#col=pallette.hex_colors
col = ["#c7673e", "#78a44f", "#6b87c8","#934fce","#E6AB02","#be558f","#47af8d"]
if byStellarMass or byHaloMass:
    col = ['k']+col
lw = 3.5
    
UnitLength_in_cm = 3.085678e21 ## 1.0 kpc
if mpc_units: UnitLength_in_cm *= 1000.0
UnitMass_in_Msun = 1e10
UnitVelocity_in_cm_per_s = 1.0e5
if redshift_lin:
    zmax = 8.1
elif redshift_log:
    zmax = 10
else:
    zmax = 100
fig, ax1 = plt.subplots(figsize=(7,7))
if plot_BHmass:
    ax2 = ax1.twinx()
    ax2.set_yscale('log')
for i, indir in enumerate(indirs):
    if indir=="": continue
    if from_txt:
        sfrtime, total_sm, totsfr, rate_in_msunperyear, total_sum_mass_stars, cum_mass_stars = np.loadtxt(indir+"sfr.txt", unpack=True)
        #zsfr = 1.0/sfrtime - 1.0
        totsfr = np.array(totsfr) ## Msun / yr (no h since mass and time both are h^-1)
        if density:
            sim = snap.snapshot(indir, final_snap, mpc_units=mpc_units, header_only=True)
            totsfr /= (sim.box_sidelength / h_scale / 1000.0)**3 ## SFR per unit comoving volume (Msun / yr / Mpc^3)
        
        mask = (sfrtime < tmax) & (1.0/sfrtime - 1.0 <= zmax) & (totsfr>0.0)
        if redshift_lin:
            x = 1.0 / sfrtime - 1.0
        elif redshift_log:
            x = 1.0 / sfrtime
        else:
            x = sfrtime
        ax1.plot(x[mask], totsfr[mask], lw=lw, c=col[i], label=labels[i])#+" sfr.txt")
    
        if plot_BHmass:
            BHtime, TotNumBHs, total_mass_holes, total_mdot, mdot_in_msun_per_year, total_mass_real, total_mdoteddington = np.loadtxt(indir+"blackholes.txt", unpack=True)
            #zBH = 1.0/BHtime - 1.0
            mask = (BHtime < tmax) & (1.0/BHtime - 1.0 <= zmax)
            if redshift_lin:
                x = 1.0/BHtime - 1.0
            elif redshift_log:
                x = 1.0/BHtime
            else:
                x = BHtime
            ax2.plot(x[mask], total_mass_holes[mask], lw=lw, c=col[i])#, label=labels[i]+" BH.txt")
    elif from_snap:
        print "Starting directory", indir
        time = []
        z = []
        totsfr = []
        totbhmass = []
        haloSFR_StellarMass = []
        haloSFR_HaloMass = []
        haloSFR_tot = []
        haloSFR_inrad = []
        for snapnum in range(0, final_snap+1):
            print "Starting snap", snapnum
            sim = snap.snapshot(indir, snapnum, mpc_units=mpc_units, extra_sub_props=True)
            if (sim.header.time <= tmax) & (sim.header.redshift < zmax+1):
                time.append(sim.header.time)
                z.append(sim.header.redshift)
                sim.load_sfr()
                ind = np.where(sim.sfr > 0.0)[0]
                if plot_BHmass:
                    sim.load_bhmass()
                    indbh = np.where(sim.bhmass > 0.0)[0]
                if usehighres:
                    sim.load_gasmass()
                    sim.load_highresgasmass()
                    ind = np.intersect1d(ind, np.where(sim.hrgm > 0.5*sim.gasmass)[0], assume_unique=True)
                if main_halo_only:
                    assert not byStellarMass and not byHaloMass, "main_halo_only not compatible with byStellarMass or byHaloMass"
                    if not hasattr(sim,"cat"): ## ignore this snapshot and delete it
                        np.delete(time, -1)
                        np.delete(z, -1)
                    else:
                        sim.get_hostsubhalos()
                        ind = np.intersect1d(ind,np.where(sim.hostsubhalos[sim.species_start[0]:sim.species_end[0]] == sim.cat.group_firstsub[0])[0])
                        print "t=", sim.header.time, sim.cat.sub_pos[sim.cat.group_firstsub[0]]
                        indbh = np.intersect1d(indbh, np.where(sim.hostsubhalos[sim.species_start[5]:sim.species_end[5]] == sim.cat.group_firstsub[0])[0])
                totsfr.append(sim.sfr[ind].sum(dtype=np.float64))
                if plot_BHmass:
                    totbhmass.append(sim.bhmass[indbh].sum(dtype=np.float64))
                    
                if byStellarMass and hasattr(sim,"cat"):
                    massarr = [7,8,9,10,11,12,13] ## M_star (log Msun)
                    msub = sim.cat.sub_masstab[:,4]*1e10/h_scale
                    haloSFR_StellarMass.append(np.histogram(np.log10(msub[msub > 0.0]), bins=massarr, weights=sim.cat.sub_sfr[msub > 0.0])[0])
                if byHaloMass and hasattr(sim,"cat"):
                    massarr = [9,10,11,12,13,14] ## M_halo (log Msun)
                    msub = sim.cat.sub_mass*1e10/h_scale
                    haloSFR_HaloMass.append(np.histogram(np.log10(msub[msub > 0.0]), bins=massarr, weights=sim.cat.sub_sfr[msub > 0.0])[0])
                if comp_aper:
                    haloSFR_tot.append(np.sum(sim.cat.sub_sfr))
                    haloSFR_inrad.append(np.sum(sim.cat.sub_sfrinrad))
        totsfr = np.array(totsfr) ## Msun / yr (no h since mass and time both are h^-1)
        totbhmass = np.array(totbhmass)
        haloSFR_StellarMass = np.array(haloSFR_StellarMass)
        haloSFR_HaloMass = np.array(haloSFR_HaloMass)
        haloSFR_tot = np.array(haloSFR_tot)
        haloSFR_inrad = np.array(haloSFR_inrad)
        z = np.array(z)
        if density:
            totsfr /= (sim.box_sidelength / h_scale / 1000.0)**3 ## SFR per unit comoving volume (Msun / yr / Mpc^3)
            haloSFR_StellarMass /= (sim.box_sidelength / h_scale / 1000.0)**3
            haloSFR_HaloMass /= (sim.box_sidelength / h_scale / 1000.0)**3
            haloSFR_tot /= (sim.box_sidelength / h_scale / 1000.0)**3
            haloSFR_inrad /= (sim.box_sidelength / h_scale / 1000.0)**3
        if redshift_lin:
            x = z
        elif redshift_log:
            x = z+1.0
        else:
            x = time
            
        if not comp_aper:
            ax1.plot(x, totsfr, lw=lw, label=labels[i], c=col[i])
        if plot_BHmass: ax2.plot(x, totbhmass, lw=3, ls='dashed', c=col[i])#, label=labels[i]+" BH")
        if byStellarMass:
            for j, SFR in enumerate(haloSFR_StellarMass.T):##transposed
                ax1.plot(x, SFR, lw=lw, c=col[i+1+j], label="M$_{\star}$=10$^{"+str(massarr[j])+"-"+str(massarr[j+1])+"}$ M$_{\odot}$")
            ax1.plot(x, np.sum(haloSFR_StellarMass, axis=1), lw=lw, c='k', ls='dashed')
        if byHaloMass:
            for j, SFR in enumerate(haloSFR_HaloMass.T):##transposed
                ax1.plot(x, SFR, lw=lw, c=col[i+1+j], label="M$_{halo}$=10$^{"+str(massarr[j])+"-"+str(massarr[j+1])+"}$ M$_{\odot}$")
            ax1.plot(x, np.sum(haloSFR_HaloMass, axis=1), lw=lw, c='k', ls='dashed')
        if comp_aper:
            ax1.plot(x, haloSFR_tot, lw=lw, label="Total", c=col[i+1])
            ax1.plot(x, haloSFR_inrad, lw=lw, label="$2 r_{\star, 1/2}$", c=col[i+2])
            
if plot_obs and density and (redshift_lin or redshift_log):
    SalShift = 10**-0.24 ## SFRD conversion factor from Salpeter to Chabrier (-0.25 dex from Bouwens+15 but use -0.24 for consistency with SMFs)
    
    c = '0.5'
    ms=7
    
    obs_dir = "/data/vault/nh444/ObsData/SFR/"
    Ill = np.loadtxt(obs_dir+"Illustris_SFRD.csv", delimiter=",")
    if redshift_lin: x = 10**Ill[:,0] - 1
    elif redshift_log: x = 10**Ill[:,0]
    ax1.errorbar(x, 10**Ill[:,1]*(h_scale/0.704)**3, c=c, ls='dashed', label="Illustris")
    
    EAGLE = np.loadtxt(obs_dir+"EAGLE_SFRD.csv", delimiter=",")
    if redshift_lin: x = EAGLE[:,0]
    elif redshift_log: x = EAGLE[:,0] + 1
    ax1.errorbar(x, EAGLE[:,1]*(h_scale/0.6777)**3, c=c, ls='dotted', label="EAGLE")
    
    Kar = np.loadtxt(obs_dir+"Karim2011_SFRD.csv", delimiter=",")
    if redshift_lin: x = Kar[:,0]
    elif redshift_log: x = Kar[:,0] + 1
    ax1.errorbar(x, Kar[:,4]*(h_scale/0.7)**3, yerr=[np.abs(Kar[:,6])*(h_scale/0.7)**3, Kar[:,5]*(h_scale/0.7)**3], ls='none', marker='o', ms=ms, c=c, mfc=c, mec='none', label="Karim+ 2011 (radio)")
    
    Bo = np.loadtxt(obs_dir+"Bouwens2015_SFRD.csv", delimiter=",")
    if redshift_lin: x = Bo[:,0]
    elif redshift_log: x = Bo[:,0] + 1
    fac = (h_scale/0.7)**3 * SalShift ## conversion to h=h_scale and Chabrier IMF
    p = ax1.errorbar(x, 10**Bo[:,4]*fac, yerr=[10**Bo[:,4]*fac - 10**(Bo[:,4] - np.abs(Bo[:,6]))*fac, 10**(Bo[:,4] + np.abs(Bo[:,5]))*fac - 10**Bo[:,4]*fac], ls='none', marker='D', ms=ms*0.8, c=c, mfc=c, mec='none', label="Bouwen+ 2015 (UV)")
    #ax1.errorbar(x, 10**Bo[:,1]*fac, yerr=[10**Bo[:,1]*fac - 10**(Bo[:,1] - np.abs(Bo[:,3]))*fac, 10**(Bo[:,1] + np.abs(Bo[:,2]))*fac - 10**Bo[:,1]*fac], ls='none', marker='D', c=p[0].get_color(), mfc='none', label="Bouwen+ 2015 (UV, no dust)")
    
    Mad = np.genfromtxt(obs_dir+"Madau_2014_SFRD.csv", delimiter=",")
    if redshift_lin:
        x = (Mad[:,1] + Mad[:,2])/2.0 ## midpoint of redshift range
        xerrlow = x-Mad[:,1]
        xerrhigh = Mad[:,2]-x
    elif redshift_log:
        x = ((Mad[:,1]+1)*(Mad[:,2]+1))**0.5 ## log midpoint of z+1 range
        xerrlow = x-(Mad[:,1]+1)
        xerrhigh = (Mad[:,2]+1)-x
    
    fac = (h_scale/0.7)**3 * SalShift ## conversion to h=h_scale and Chabrier IMF
    idx = 27 ## row index where table becomes IR rather than UV data (after Sanders 2003)
    ax1.errorbar(x[:idx], 10**Mad[:idx,4]*fac, yerr=[10**Mad[:idx,4]*fac - 10**(Mad[:idx,4] - np.abs(Mad[:idx,6]))*fac, 10**(Mad[:idx,4] + np.abs(Mad[:idx,5]))*fac - 10**Mad[:idx,4]*fac], xerr=[xerrlow[:idx],xerrhigh[:idx]], ls='none', marker='>', ms=ms*1.1, c=c, mfc=c, mec='none', label="Madau \& Dickinson 2014 (UV-comp)")
    ax1.errorbar(x[idx:], 10**Mad[idx:,4]*fac, yerr=[10**Mad[idx:,4]*fac - 10**(Mad[idx:,4] - np.abs(Mad[idx:,6]))*fac, 10**(Mad[idx:,4] + np.abs(Mad[idx:,5]))*fac - 10**Mad[idx:,4]*fac], xerr=[xerrlow[idx:],xerrhigh[idx:]], ls='none', marker='<', ms=ms*1.1, c=c, mfc=c, mec='none', label="Madau \& Dickinson 2014 (IR-comp)")


if density:
    ax1.set_ylabel("SFRD [M$_{\odot}$ yr$^{-1}$ Mpc$^{-3}$]")
    ax1.set_yscale('log')
else:
    ax1.set_ylabel("Total SFR [M$_{\odot}$ yr$^{-1}$]")
if redshift_lin:
    ax1.set_xlabel(r"$z$")
    ax1.set_xlim(0, zmax)
    #import matplotlib.ticker as ticker
    #ax1.xaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))
elif redshift_log:
    ax1.set_xlabel(r"$1+z$")
    ax1.set_xscale('log')
    #xmin, xmax = ax1.get_xlim()
    #ax1.set_xlim(10, xmin)
    ax1.set_xlim(1, zmax)
    import matplotlib.ticker as ticker
    ax1.xaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))
else:
    ax1.set_xlabel("time")
if plot_BHmass:
    ax2.set_ylabel("Total mass BH")
    ax2.legend(frameon=False, loc='lower right')
if byStellarMass or byHaloMass:
    ax1.legend(frameon=False, loc='best', borderaxespad=1, ncol=2, fontsize='xx-small')
else:
    ax1.legend(frameon=False, loc='best', borderaxespad=1, ncol=1, fontsize='x-small')
fig.savefig(outname, bbox_inches='tight')
print "Plot saved to", outname

