#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 15 15:14:04 2016
This script uses stellar_mass.py to make and plot galaxy stellar mass functions.
It also contains a function for plotting multiple SMFs on the same plot
for comparison.
@author: nh444
"""
obs_dir = "/home/nh444/vault/ObsData/L-T/"
use_fof = True ## Use halo file for M_delta and R_delta
h_scale = 0.679
              
def main():
    outdir = "/data/curie4/nh444/project1/stars/"
    mpc_units = False
    masstab = False
    
    xlims, ylims = [9, 12.5], [-5, -1.5]
    
    basedir = "/data/curie4/nh444/project1/boxes/"
    dirnames = ["L40_512_NoDutyRadioWeak_HalfThermal_FixEta/",
                "L40_512_NoDutyRadioInt_HalfThermal_FixEta/",
                "L40_512_LowDutyRadioWeak_HalfThermal_FixEta/",
                "L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff/",
                #"L40_512_LDRIntRadioEffBHVel/",
               ]
    labels = ["Ref",
              "Stronger radio",
              "Quasar duty cycle",
              "Combined",
              #"high soft",
              ]
    suffix = "_models_lowSoft_z0"
              
    basedir = "/data/curie4/nh444/project1/boxes/"
    dirnames = [#"L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff/",
                "L40_512_LDRIntRadioEffBHVel/",
               ]
    labels = [#"small softenings",
              #"larger softenings",
              "Combined"]
    suffix = "_diff_rad_z0" ## needs underscore
    
    basedir = "/data/curie4/nh444/project1/boxes/"
    dirnames = ["L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff/",
                "L40_512_LDRIntRadioEffBHVel/",
               ]
    labels = ["small softenings",
              "larger softenings"]
    suffix = "_small_large_soft" ## needs underscore
    xlims, ylims = [7, 11], [-3.5, -1]
    
    basedir = "/data/curie4/nh444/project1/boxes/"
    dirnames = ["L40_512_LDRIntRadioEff_noBH/",
                "L40_512_NoDutyRadioWeak/",
                "L40_512_NoDutyRadioInt/",
                "L40_512_DutyRadioWeak/",
                "L40_512_LDRIntRadioEffBHVel/",
                "L40_512_LDRHighRadioEff/",
                "L40_512_MDRIntRadioEff/",
               ]
    labels = ["No BHs",
              "Weak radio",
              "Stronger radio",
              "Quasar duty cycle",
              "Fiducial",#"Combined",
              "Longer radio duty",
              "Longer quasar duty",
              ]
    suffix = "_models_z0"
    xlims, ylims = [9, 12.2], [-5, -1.7]
    
    basedir = "/data/curie4/nh444/project1/boxes/"
    dirnames = ["L40_512_DutyIntRadioWeak/",
                "L40_512_DutyIntRadioWeak_Thermal/",
                "L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff/",
                "L40_512_DutyInt1RadioWeak_Thermal_FixEta/",
                "L40_512_LDRIntRadioEffBHVel/",
                #"L40_512_LowDutyRadioWeak_HalfThermal_FixEta/",
                #"L40_512_NoDutyRadioWeak_HalfThermal_FixEta/",
               ]
    labels = [r"E$_{\mathrm{SNII},51}= 1$, $U_{therm} = 0$ (small $\epsilon$)",
              r"E$_{\mathrm{SNII},51}= 1$, $U_{therm} = E_k$ (small $\epsilon$)",
              r"E$_{\mathrm{SNII},51}= 1.5$, $U_{therm} = 0.5 E_k$ (small $\epsilon$)",#U$_{\mathrm{wind}} = 2.25$ $\frac{3}{2} \sigma_{DM,1D}^{2}$
              r"E$_{\mathrm{SNII},51}= 2$, $U_{therm} = E_k$ (small $\epsilon$)",
              r"E$_{\mathrm{SNII},51}= 1.5$, $U_{therm} = 0.5 E_k$ (larger $\epsilon$)",
              #r"LowDutyRadioWeak\_HalfThermal\_FixEta",
              r"NoDutyRadioWeak\_HalfThermal\_FixEta",
              ]
    suffix = "_winds_comparison_z0" ## needs underscore
    xlims, ylims = [7, 12.2], [-5, -1]
    
#==============================================================================
#     basedir = "/data/emergence3/deboras/Elliptical_ZoomIns/"
#     dirnames = ["L40_512_NoDutyRadioWeak_HalfThermal_FixEta/",
#                 "L40_512_NoDutyRadioWeak_HalfThermal_FixEta_BHChangeVel/",
#                 "L40_512_LowDutyRadioWeak_HalfThermal_FixEta/",
#                 "L40_512_LowDutyRadioWeak_HalfThermal_FixEta_BHChangeVel/",
#                 "L40_512_NoDutyRadioIntLow_HalfThermal_FixEta/",
#                 "L40_512_NoDutyRadioIntLow_HalfThermal_FixEta_BHChangeVel/",
#                ]
#     labels = ["NoDutyRadioWeak",
#                 "NoDutyRadioWeak\_BHChangeVel",
#                 "LowDutyRadioWeak",
#                 "LowDutyRadioWeak\_BHChangeVel",
#                 "NoDutyRadioIntLow",
#                 "NoDutyRadioIntLow\_BHChangeVel",
#               ]
#     suffix = "_BHVel_z0"
#==============================================================================

#==============================================================================
#     ### Calculate SMFs:
#     dlogm = 0.2
#     from stellar_mass import stellar_mass
#     snapnums = [4, 5, 6, 8, 10, 15, 20, 25]
#     snapnums = [25,20,15,10]
#     for dirname in dirnames:
#         mstar = stellar_mass(basedir, dirname, outdir, snapnums, obs_dir, h_scale=h_scale, use_fof=use_fof, masstab=masstab, mpc_units=mpc_units)
#     
#         mstar.get_SMF(bcgs_only=False, calc_30kpc=True, dlogm=dlogm, verbose=True) ## Calculate SMF and save as txt file
#         #mstar.get_SMF(bcgs_only=True, calc_30kpc=True, dlogm=dlogm, verbose=True) ## Calculate SMF and save as txt file
#         #mstar.plot_SMF(plot="all", for_paper=True, bcgs_only=False, bcgs_and_total=False, log=True) ## Plot all snapshots
#         #mstar.plot_SMF(plot="even", bcgs_only=True, for_paper=True) ## plot snapshots with even redshifts
#         #mstar.plot_SMF(plot="odd") ## plot snapshots with odd redshifts
#     
#==============================================================================
    #plot_SMF_comparison(dirnames, 16, ratio=False, for_paper=False, bcgs_only=False, thirtykpc=False, twicehalfrad=False, compare_apertures=False, comp_sims=True, datadir=outdir, outdir=outdir, suffix=suffix, labels=labels, basedir=basedir, mpc_units=mpc_units, masstab=masstab, xlims=xlims, ylims=ylims, h_scale=h_scale)
    #for snapnum in [1,2,3,4,5,6,8,10,15,20,25]:
        #plot_SMF_comparison(dirnames, snapnum, for_paper=False, bcgs_only=False, aperture=True, compare_apertures=False, datadir=outdir, outdir=outdir, suffix=suffix, labels=labels, basedir=basedir, mpc_units=mpc_units, masstab=masstab, xlims=xlims, ylims=ylims, h_scale=h_scale)
        
    basedir = "/data/curie4/nh444/project1/boxes/"
    dirnames = ["L40_512_LDRIntRadioEffBHVel/"]
    labels = ["$<$30 pkpc"]
    outname = "SMF_comparison_vs_z.pdf"
    snapnums = [4,5,6,8,10,15,20,25]
    snapnums = [10,15,20,25]
    
    basedir = "/data/curie4/nh444/project1/boxes/"
    dirnames = [#"L40_512_LDRIntRadioEff_noBH/",
                #"L40_512_NoDutyRadioWeak/",
                #"L40_512_NoDutyRadioInt/",
                #"L40_512_DutyRadioWeak/",
                "L40_512_LDRIntRadioEffBHVel/",
                "L40_512_LDRHighRadioEff/",
                "L40_512_MDRIntRadioEff/",
               ]
    labels = [#"No BHs",
              #"Weak radio",
              #"Stronger radio",
              #"Quasar duty cycle",
              "Fiducial",#"Combined",
              "Longer radio duty",
              "Longer quasar duty",
              ]
    outname = "SMF_comparison_vs_z_models_new.pdf"
    #outname = "SMF_vs_z.pdf"
    
    basedir = "/data/curie4/nh444/project1/boxes/"
    dirnames = ["L40_512_NoDutyRadioWeak_HalfThermal_FixEta/",
                "L40_512_NoDutyRadioInt_HalfThermal_FixEta/",
                "L40_512_LowDutyRadioWeak_HalfThermal_FixEta/",
                "L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff/",
               ]
    labels = ["Ref",
              "Stronger radio",
              "Quasar duty cycle",
              "Combined",
              ]
    outname = "SMF_comparison_vs_z_models_lowSoft.pdf"
    
    dirnames = [#"L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff/",
                "L40_512_LDRIntRadioEffBHVel/",
                #"L40_512_LDRIntRadioEff_SoftSmall/",
                "L40_512_LDRIntRadioEff_SoftSmaller/",
               ]
    labels = [#"$\epsilon$(z=0)=0.3375 $z_{\epsilon}=3$",
              "fiducial softening",#"Fiducial: $\epsilon$(z=0)=2.4 $z_{\epsilon}=5$",
              #"$\epsilon$(z=0)=2.4 $z_{\epsilon}=1$",
              "intermediate softening",#"Intermediate: $\epsilon$(z=0)=0.96 $z_{\epsilon}=4$",
              ]
    outname = "SMF_comparison_vs_z_soft.pdf"
    snapnums = [1,3,5,8, 10, 15, 20, 25]
    xlims, ylims = [6.5, 11], [-5, -0.5]
    
    ### Calculate SMFs:
#    dlogm = 0.2
#    from stellar_mass import stellar_mass
#    for dirname in dirnames:
#        mstar = stellar_mass(basedir, dirname, outdir, snapnums, obs_dir, h_scale=h_scale, use_fof=use_fof, masstab=masstab, mpc_units=mpc_units)
#        mstar.get_SMF(bcgs_only=False, calc_30kpc=True, dlogm=dlogm, verbose=True) ## Calculate SMF and save as txt file

    #plot_SMF_vs_redshift(dirnames, snapnums, labels, xlims=xlims, ylims=ylims, outname=outname, datadir=outdir, outdir=outdir, basedir=basedir, mpc_units=mpc_units, masstab=masstab)
    
    ## Plot separately:
    col = ['#24439b', '#ED7600']
    for snapidx, snapnum in enumerate(snapnums):
        plot_SMF_comparison(dirnames, snapnum, mpc_units=mpc_units,
                            suffix="_soft", col=col,
                            for_paper=False, bcgs_only=False,
                            comp_sims=True, compare_apertures=False, use_nperbin=True, ratio=False,
                            thirtykpc=False, twicehalfrad=False,
                            datadir=outdir, outdir=outdir, basedir=basedir,
                            labels=labels, xlims=xlims, ylims=ylims, h_scale=h_scale)
        
def plot_SMF_vs_redshift(dirnames, snapnums, labels, onelabel=False, xlims=[8, 12.5], ylims=[-5, -1.5], outname="SMF_comparison_vs_z.pdf",
                         datadir="/data/curie4/nh444/project1/stars/", outdir="/data/curie4/nh444/project1/stars/", basedir="/data/emergence3/deboras/Elliptical_ZoomIns/", mpc_units=False, masstab=False):
    """
    This function plots GSMFs for given snapshots in a grid using the function plot_SMF_comparison()
    """
    import matplotlib.pyplot as plt
    fig, axes = plt.subplots(len(snapnums)/2, 2, figsize=(14,7*len(snapnums)/2))
    axes = axes.flatten()[::-1] ## 1D instead of 2D and reverse
    for snapidx, snapnum in enumerate(snapnums):
        plot_SMF_comparison(dirnames, snapnum, mpc_units=mpc_units, axes=axes[snapidx],
                            for_paper=False, bcgs_only=False,
                            comp_sims=True, compare_apertures=False, use_nperbin=True, ratio=False,
                            thirtykpc=False, twicehalfrad=False,
                            datadir=outdir, outdir=outdir, basedir=basedir,
                            labels=labels, xlims=xlims, ylims=ylims, h_scale=h_scale)
        if onelabel:
            labels = [""]*len(dirnames)
    plt.tight_layout()
    fig.savefig(outdir+outname)
    print "Saved to", outdir+outname
    
def plot_SMF_comparison(dirnames, snapnum, axes=None, ratio=True, z=None, datadir="/data/curie4/nh444/project1/stars/", outdir="/data/curie4/nh444/project1/stars/", basedir = "/data/emergence3/deboras/Elliptical_ZoomIns/", mpc_units=False, masstab=False,
                        suffix="", col=None, apercol=None, lw=3, labels=None, for_paper=False, thirtykpc=False, twicehalfrad=False, compare_apertures=False, use_nperbin=True, comp_sims=True, comp_obs=True,
                        blkwht=False, bcgs_only=True, chabrier=True, xlims=[9, 12.2], ylims=[-5, -1.5], h_scale=0.7):
    import snap
    import matplotlib.pyplot as plt
    import numpy as np
    import os
    """
    ax is a matplotlib axes object - if None, will create one
    ratio - add subplot showing the ratio of the SMFs
    """
    ax1 = axes ## a matplotlib axes object
    if ax1 is None:
        new_ax = True
    else:
        new_ax = False
        if ratio:
            print "WARNING: currently can't define both axes and ratio=True"
            ratio = False

    if labels is None:
        labels = [dirname.replace("_","\_") for dirname in dirnames]
    if col is None and for_paper:
        #from palettable.colorbrewer.qualitative import Dark2_8 as pallette
        #col=pallette.hex_colors
        #from palettable.colorbrewer.sequential import RdPu_5 as pallette
        from palettable.colorbrewer.sequential import YlGnBu_5 as pallette
        #from palettable.colorbrewer.qualitative import Paired_4 as pallette
        col=pallette.hex_colors[-4:]
    elif col is None:
        if blkwht:
            col=["#56b4e9",
                 "#cb484e",
                 "#b153cd",
                 "#e2a82c"]
        else:
            col = ["#c7673e","#78a44f","#6b87c8","#934fce","#e6ab02","#be558f","#47af8d"]
    if use_nperbin:
        ls = ['solid']*len(dirnames)
    else:
        ls=['dashdot', 'dotted', 'dashed', 'solid']
    
    if compare_apertures:
        thirtykpc = True ## needed for function that plots obs data
        twicehalfrad = True
        aperls = ['solid', 'solid', 'solid', 'dashed']#, 'dashed', 'dotted', 'dashdot'] ## linestyles for when compare_radii is True
        if apercol is None:
            #apercol = ["#c7673e","#78a44f","#6b87c8","#934fce","#e6ab02","#be558f","#47af8d"]
            from palettable.colorbrewer.sequential import RdPu_3 as pallette
            apercol = pallette.hex_colors[-3:]
            apercol = ['#faa2b7','#e168a0','#c51b8a'] ## RdPu adjusted

    
    if new_ax:
        if ratio:
            #fig, (ax1, ax2)  = plt.subplots(2, 1, figsize=(7,11))
            fig = plt.figure(figsize=(7,9.5))
            #ax1 = plt.subplot2grid((2, 1), (0, 0))
            #ax2 = plt.subplot2grid((2, 1), (1, 0))
            import matplotlib.gridspec as gridspec
            gs = gridspec.GridSpec(4, 1)
            ax1 = plt.subplot(gs[:-1, 0])
            ax2 = plt.subplot(gs[-1, 0])
        else:
            fig, ax1 = plt.subplots(figsize=(7,7))

    if new_ax:
        if ratio:
            ax2.set_xlabel("log$_{10}$($M_{\star}$ [M$_{\odot}$])")
            ax2.set_ylabel("$\phi$ [$\phi_{\mathrm{"+labels[0]+"}}$]")
        else: ax1.set_xlabel("log$_{10}$($M_{\star}$ [M$_{\odot}$])")
        ax1.set_ylabel("log$_{10}$($\phi$ [Mpc$^{-3}$ dex$^{-1}$])")
    #ax1.set_xlim(9, 12.2)
    #ax1.set_xlim(7, 12.2)
    #ax1.set_ylim(-5, -1.5)
    #ax1.set_ylim(-5, -1)
    ax1.set_xlim(xlims[0], xlims[1])
    ax1.set_ylim(ylims[0], ylims[1])
    if ratio:
        ax2.set_xlim(xlims[0], xlims[1])
        ax2.set_ylim(0.0, 1.5)

    ## If redshift not given, get it from the first dirname
    if z is None:
        z = snap.snapshot(basedir+dirnames[0], snapnum, mpc_units=mpc_units, masstab=masstab, read_params=False, header_only=True).header.redshift
    print "redshift", z
    if not for_paper:
        if thirtykpc:
            ax1.set_title("$z = {:.1f}$".format(z)+" [$<$30 pkpc]")
        elif twicehalfrad:
            ax1.set_title("$z = {:.1f}$".format(z)+" [$2 r_{\star, 1/2}$]")
        else:
            ax1.set_title("$z = {:.1f}$".format(z)+" [total]")
   
    ### Observations ###
    from stars.stellar_mass import plot_SMF_observations
    plot_SMF_observations(ax1, z, colour1='0.5', colour2='0.75', comp_sims=comp_sims, comp_obs=comp_obs, thirtykpc=thirtykpc, twicehalfrad=twicehalfrad, chabrier=chabrier, for_paper=for_paper, h_scale=h_scale)
    
    for j, dirname in enumerate(dirnames):
        print "Plotting", dirname
        if bcgs_only: filepath = datadir+dirname+"SMF/SMF_"+str(snapnum).zfill(3)+"_bcgs_only.txt"
        else: filepath = datadir+dirname+"SMF/SMF_"+str(snapnum).zfill(3)+".txt"
        if os.path.exists(filepath):
            SMF = np.loadtxt(filepath)
        elif len(dirnames) > 1:
            continue
        else:
            assert False, "path "+filepath+" does not exist"
            
        ## columns are: logm_bins, smf_total (h^3), smf_inrad (h^3), smf_inVmaxrad (h^3), smf_inSPrad (h^3), smf_in30kpc (h^3), n_total, n_inrad, n_inVmaxrad, n_inSPrad, n_in30kpc
        logm_bins = SMF[:,0] - np.log10(h_scale) ## centre of the bin in log10(mass [Msun/h])
        smf_total = SMF[:,1] * h_scale**3
        smf_inrad = SMF[:,2] * h_scale**3
        #smf_inVmaxrad = SMF[:,3] * h_scale**3
        #smf_inSPrad = SMF[:,4] * h_scale**3 ## mass within surface brightness limit radius (default 20.7 mag/arcsec^2 in K-band)
        smf_in30kpc = SMF[:,5] * h_scale**3
        n_total = SMF[:,6]
        n_inrad = SMF[:,7]
        n_in30kpc = SMF[:,10]
        if use_nperbin:
            mstar = 4.9e6 ## median star particle mass in Msun/h
            ## it only varies by ~0.2 dex from z=6 to z=0
        if compare_apertures:
            smfs = (smf_total, smf_inrad, smf_in30kpc)#, smf_inSPrad)
            n = (n_total, n_inrad, n_in30kpc)#, n_inSPrad)
            aperlabels = ["total", "$2 r_{\star, 1/2}$", "30 pkpc"]#, " Photometric radius"] #" (half-mass radius)", " (V$_{\mathrm{max}}$ radius)"
            for smfidx, smf in enumerate(smfs):
                if use_nperbin:
                    lowidx = np.max(np.where(logm_bins <= np.log10(mstar / h_scale * 100.0))[0]) ## galaxies of stellar mass less than 100 star particles
                    ax1.plot(logm_bins[:lowidx], np.log10(smf[:lowidx]), c=apercol[smfidx], lw=lw, dashes=(0.5,6), dash_capstyle='round')
                    idx = np.min(np.where((n[smfidx] < 10) & (logm_bins > 8.0))[0])## if have very low mass bins they can have <10 objects so idx=0 and nothing is plotted
                    ax1.plot(logm_bins[lowidx-1:idx], np.log10(smf[lowidx-1:idx]), c=apercol[smfidx], label=aperlabels[smfidx], lw=lw, ls='solid', dash_capstyle='round')
                    ax1.plot(logm_bins[idx-1:], np.log10(smf[idx-1:]), c=apercol[smfidx], lw=lw, ls='dashed', dash_capstyle='round')
                else:
                    ax1.plot(logm_bins, np.log10(smf), c=apercol[smfidx], label=aperlabels[smfidx], lw=lw, ls=aperls[smfidx], dash_capstyle='round')
        else:
            if thirtykpc:
                smf = np.ma.masked_equal(smf_in30kpc, 0.0)
                n = n_in30kpc
            elif twicehalfrad:
                smf = np.ma.masked_equal(smf_inrad, 0.0)
                n = n_inrad
            else:
                smf = np.ma.masked_equal(smf_total, 0.0)
                n = n_total
            if ratio and j==0:
                smf_ref = np.ma.masked_equal(smf, 0.0)
            
            if use_nperbin:
                lowidx = np.max(np.where(logm_bins <= np.log10(mstar / h_scale * 100.0))[0]) ## galaxies of stellar mass less than 100 star particles
                ax1.plot(logm_bins[:lowidx], np.log10(smf[:lowidx]), c=col[j], lw=lw, dashes=(0.5,6), dash_capstyle='round')
                idx = np.min(np.where((n < 10) & (logm_bins > 8.0))[0])## if have very low mass bins they can have <10 objects so idx=0 and nothing is plotted
                ax1.plot(logm_bins[lowidx-1:idx], np.log10(smf[lowidx-1:idx]), c=col[j], label=labels[j], lw=lw, ls=ls[j], dash_capstyle='round')
                ax1.plot(logm_bins[idx-1:], np.log10(smf[idx-1:]), c=col[j], lw=lw, ls='dashed', dash_capstyle='round')
            else:
                ax1.plot(logm_bins, np.log10(smf), c=col[j], label=labels[j], lw=lw, ls=ls[j], dash_capstyle='round')
                #ax1.plot(logm_bins, np.log10(smf_total), c=col[j], label=labels[j], lw=lw, ls='solid', dash_capstyle='round')
                #ax1.plot(logm_bins, np.log10(smf_in30kpc), c=col[j], label=labels[j], lw=lw, ls='dashed', dash_capstyle='round')
            
            if ratio:
                #ind = np.where((smf_ref > 1e-7) & (smf > 0.0))[0] ## to avoid dividing by zero
                if use_nperbin:
                    lowidx = np.max(np.where(logm_bins <= np.log10(mstar / h_scale * 100.0))[0]) ## galaxies of stellar mass less than 100 star particles
                    ax2.plot(logm_bins[:lowidx], smf[:lowidx] / smf_ref[:lowidx], c=col[j], lw=lw, dashes=(0.5,6), dash_capstyle='round')
                    idx = np.min(np.where((n < 10) & (logm_bins > 8.0))[0])## if have very low mass bins they can have <10 objects so idx=0 and nothing is plotted
                    print len(smf), len(smf[lowidx-1:idx])
                    print len(smf_ref), len(smf_ref[lowidx-1:idx])
                    ax2.plot(logm_bins[lowidx-1:idx], smf[lowidx-1:idx] / smf_ref[lowidx-1:idx], c=col[j], label=labels[j], lw=lw, ls=ls[j], dash_capstyle='round')
                    highidx = np.min(np.where((n == 0) & (logm_bins > 8.0))[0]) ## don't plot points beyond the first zero bin
                    ax2.plot(logm_bins[idx-1:highidx+1], smf[idx-1:highidx+1] / smf_ref[idx-1:highidx+1], c=col[j], lw=lw, ls='dashed', dash_capstyle='round')
                    #print smf[idx-1:] / smf_ref[idx-1:]
                    #print smf[idx-1:]#, smf_ref[ind[idx-1:]]
                else:
                    ax2.plot(logm_bins, smf / smf_ref, c=col[j], label=labels[j], lw=lw, ls=ls[j], dash_capstyle='round')
        
    if new_ax:
        leg = ax1.legend(frameon=False, borderaxespad=1.5, numpoints=1, loc='best', fontsize='small')
    else:
        leg = ax1.legend(frameon=False, borderaxespad=0.5, numpoints=1, loc='best', fontsize='small')
    if new_ax:
        if bcgs_only: plotfile = outdir+"SMF_comparison_snap_"+str(snapnum).zfill(3)+"_bcgs_only"+suffix
        else: plotfile = outdir+"SMF_comparison_snap_"+str(snapnum).zfill(3)+suffix
        if blkwht:
            fig.patch.set_facecolor('black')
            ax1.set_axis_bgcolor('black')
            ax1.xaxis.label.set_color('white')
            ax1.tick_params(axis='x', which='both', colors='white')
            ax1.yaxis.label.set_color('white')
            ax1.tick_params(axis='y', which='both', colors='white')
            for text in leg.get_texts():
                text.set_color("white")
            import matplotlib
            for child in ax1.get_children():
                if isinstance(child, matplotlib.spines.Spine):
                    child.set_color('white')
            fig.savefig(plotfile+".png", bbox_inches='tight', transparent=True)# facecolor=fig.get_facecolor(), edgecolor='none')
        else:
            plt.tight_layout(pad=0.45)
            fig.savefig(plotfile+".pdf", bbox_inches='tight')
        print "SMF saved to", plotfile
        return plotfile

if __name__ == "__main__":
    main()
