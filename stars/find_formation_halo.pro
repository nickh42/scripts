
; cosmology stuff

PRO setcosmo, omegamatter, hubble ;call by e.g.: setcosmo, 0.3, 0.7
  common cosmoparams, omegam, omegal, h
  omegam = omegamatter
  omegal = 1.0D - omegam
  h = hubble
END

PRO printcosmo
  common cosmoparams, omegam, omegal, h
  print, "omegam = ", omegam
  print, "omegal = ", omegal
  print, "h = ", h
END

FUNCTION difftime, scalefactor ; dt/da in (Mpc/h)/(km/s)
  common cosmoparams, omegam, omegal, h
  H0 = 100.0D*h
  RETURN, 1.0D/(H0*sqrt(omegam/(scalefactor*scalefactor*scalefactor)+omegal)*scalefactor)*h
END

FUNCTION time, scalefactor1, scalefactor2 ; time in (Mpc/h)/(km/s), i.e. in internal Gadget units if Mpc/h are used for lenght
  RETURN, qsimp('difftime',scalefactor1,scalefactor2,/DOUBLE)
END

;--------------------------------------------------

; interpolates orbits using x = a + b*(t-t1) + c*(t-t1)^2 + d*(t-t1)^3
; and x1=x(t1), v1=v(t1), x2=x(t2), v2=v(t2)
; see orbit_interpolation.nb

FUNCTION interpolate_orbit, x1, v1, t1, x2, v2, t2, t
  cur_t = t - t1
  DT = t2 - t1
  RETURN, x1 + v1*cur_t - cur_t^2*(2*DT*v1 + DT*v2 + 3*x1 - 3*x2)/DT^2 - cur_t^3*(-DT*v1 - DT*v2 - 2*x1 + 2*x2)/DT^3
END

;-------------------------------------------------

Num = 63                ; get icl and gal star ids from this snapshot
numsnaps  =  64         ; number of snapshots

; Simulation directory:

;Base= "/scratch/simdat/m_6664_h_48_194_z3_csf/"
;Base= "/gpfs/mpa/ewp/sims/zoom_3_sims/m_3629_h_259_610_z3_csfbh/"
;Base= "/gpfs/mpa/ewp/sims/zoom_3_sims_new/m_5443_h_352_482_z3_csfbh/"
Base= "/gpfs/mpa/ewp/sims/zoom_2_sims/m_1975_h_506_1096_z2_csf/"
;Base= "/gpfs/mpa/ewp/sims/zoom_2_sims/m_1975_h_506_1096_z2_csfbh/"
;Base= "/gpfs/mpa/ewp/sims/zoom_3_sims/m_1975_h_506_1096_z3_csf/"
;Base= "/gpfs/mpa/ewp/sims/zoom_3_sims/m_1975_h_506_1096_z3_csfbh/"
;Base= "/gpfs/mpa/ewp/sims/test_runs/m_1975_h_506_1096_z3_csfbh_small_fof/"
;Base= "/gpfs/mpa/ewp/sims/test_runs/m_1975_h_506_1096_z3_csfbh_small_fof_seeds/"
;Base= "/gpfs/mpa/ewp/sims/zoom_2_sims/m_22498_h_197_0_z2_csfbh/"
;Base= "/gpfs/mpa/ewp/sims/zoom_3_sims/m_14999_h_355_1278_z3_csf/"
;Base= "/gpfs/mpa/ewp/sims/zoom_3_sims/m_10002_h_94_501_z3_csf/"
;Base= "/gpfs/mpa/ewp/sims/zoom_3_sims/m_10002_h_94_501_z3_csfbh/"
;Base= "/gpfs/mpa/ewp/sims/zoom_2_sims/m_33617_h_146_0_z2_csf/"
;Base= "/gpfs/mpa/ewp/sims/zoom_2_sims/m_33617_h_146_0_z2_csfbh/"

; Group cat directory:

firstgroupsdir = "groups_"
;firstgroupsdir = "postproc_"       ; required if cd_cutoff_method = 2

;check_before = 0 ;find halos were stars reside after formation 
check_before = 1 ;additionally find halos were gas particles reside before formaing a star

;have_masstab = 0 ;no masstab in subhalo_tab files
have_masstab = 1 ;masstab is present

;use_all_snaps = 0 
use_all_snaps = 1

allow_fall_in_halo_in_clus = 0
;allow_fall_in_halo_in_clus = 1

include_fuzzy = 0       ; set to 1 to incluse stars from the FoF group that are not bound to any subhalo

group = 0L              ; FoF group to analyze, currently only 0 supported 

cd_cutoff_method = 1          ; 0 for cutoff_rad, 1 for scaled cutoff_rad, 2 for binding energy
cutoff_rad = 0.03D            ; used if cd_cutoff_method = 0
cutoff_rad_norm = 0.0091*3.0  ; cutoff_rad at M_200,crit = 10^15 M_sun/h, used if cd_cutoff_method = 1

;--------------------------------------------------

FLAG_Group_VelDisp = 0  ; Set this to one if the SO-properties computed by SUBFIND for
                        ; the FOF halos contain velocity dispersions

;-------------------------------------------------

print, "cd_cutoff_method = ", cd_cutoff_method

if num ge 1000 then begin
   exts='0000'
   exts=exts+strcompress(string(Num),/remove_all)
   exts=strmid(exts,strlen(exts)-4,4)
endif else begin
   exts='000'
   exts=exts+strcompress(string(Num),/remove_all)
   exts=strmid(exts,strlen(exts)-3,3)
endelse


skip = 0L
skip_sub = 0L
fnr = 0L

repeat begin

    f = Base + "/" + firstgroupsdir + exts +"/subhalo_tab_"+exts +"."+strcompress(string(fnr),/remove_all)
    
    Ngroups = 0L
    TotNgroups = 0L
    Nids = 0L
    TotNids = 0LL
    NTask = 0L
    Nsubgroups = 0L
    TotNsubgroups = 0L

    openr,1,f
    readu,1, Ngroups, TotNgroups, Nids, TotNids, NTask, Nsubgroups, TotNsubgroups

    if fnr eq 0 then begin
        GroupLen = lonarr(TotNgroups)
        GroupOffset = lonarr(TotNgroups)
        GroupMass = fltarr(TotNgroups)
        GroupPos = fltarr(3, TotNgroups)
        Group_M_Mean200 = fltarr(TotNgroups)
        Group_R_Mean200 = fltarr(TotNgroups)
        Group_M_Crit200 = fltarr(TotNgroups)
        Group_R_Crit200 = fltarr(TotNgroups)
        Group_M_TopHat200 = fltarr(TotNgroups)
        Group_R_TopHat200 = fltarr(TotNgroups)
        Group_VelDisp_Mean200 = fltarr(TotNgroups)
        Group_VelDisp_Crit200 = fltarr(TotNgroups)
        Group_VelDisp_TopHat200 = fltarr(TotNgroups)
        GroupContaminationCount = lonarr(TotNgroups)
        GroupContaminationMass = fltarr(TotNgroups)
        GroupNsubs = lonarr(TotNgroups)
        GroupFirstSub = lonarr(TotNgroups)

        SubhaloLen = lonarr(TotNsubgroups)
        SubhaloOffset = lonarr(TotNsubgroups)
        SubhaloParent = lonarr(TotNsubgroups)
        SubhaloMass = fltarr(TotNsubgroups)
        SubhaloPos = fltarr(3, TotNsubgroups)
        SubhaloVel = fltarr(3, TotNsubgroups)
        SubhaloCM = fltarr(3, TotNsubgroups)
        SubhaloSpin = fltarr(3, TotNsubgroups)
        SubhaloVelDisp = fltarr(TotNsubgroups)
        SubhaloVmax = fltarr(TotNsubgroups)
        SubhaloVmaxRad = fltarr(TotNsubgroups)
        SubhaloHalfmassRad = fltarr(TotNsubgroups)
        SubhaloIDMostbound = lonarr(TotNsubgroups)
        SubhaloGrNr = lonarr(TotNsubgroups)
    endif

    if Ngroups gt 0 then begin
        
        locLen = lonarr(Ngroups)
        locOffset = lonarr(Ngroups)
        locMass = fltarr(Ngroups)
        locPos = fltarr(3, Ngroups)
        loc_M_Mean200 = fltarr(Ngroups)
        loc_R_Mean200 = fltarr(Ngroups)
        loc_M_Crit200 = fltarr(Ngroups)
        loc_R_Crit200 = fltarr(Ngroups)
        loc_M_TopHat200 = fltarr(Ngroups)
        loc_R_TopHat200 = fltarr(Ngroups)
        loc_VelDisp_Mean200 = fltarr(Ngroups)
        loc_VelDisp_Crit200 = fltarr(Ngroups)
        loc_VelDisp_TopHat200 = fltarr(Ngroups)
        locContaminationCount = lonarr(Ngroups)
        locContaminationMass = fltarr(Ngroups)
        locNsubs = lonarr(Ngroups)
        locFirstSub = lonarr(Ngroups)

        readu,1, loclen 
        readu,1, locOffset
        readu,1, locMass
        readu,1, locPos
        readu,1, loc_M_Mean200
        readu,1, loc_R_Mean200
        readu,1, loc_M_Crit200
        readu,1, loc_R_Crit200
        readu,1, loc_M_TopHat200
        readu,1, loc_R_TopHat200
        if FLAG_Group_VelDisp ne 0 then begin
          readu,1, loc_VelDisp_Mean200
          readu,1, loc_VelDisp_Crit200
          readu,1, loc_VelDisp_TopHat200
        endif
        readu,1, locContaminationCount
        readu,1, locContaminationMass
        readu,1, locNsubs
        readu,1, locFirstSub

        GroupLen(skip:skip+Ngroups-1) = locLen(*)
        GroupOffset(skip:skip+Ngroups-1) = locOffset(*)
        GroupMass(skip:skip+Ngroups-1) = locMass(*)
        GroupPos(*, skip:skip+Ngroups-1) = locPos(*,*)
        Group_M_Mean200(skip:skip+Ngroups-1) = loc_M_Mean200(*)
        Group_R_Mean200(skip:skip+Ngroups-1) = loc_R_Mean200(*)
        Group_M_Crit200(skip:skip+Ngroups-1) = loc_M_Crit200(*)
        Group_R_Crit200(skip:skip+Ngroups-1) = loc_R_Crit200(*)
        Group_M_TopHat200(skip:skip+Ngroups-1) = loc_M_TopHat200(*)
        Group_R_TopHat200(skip:skip+Ngroups-1) = loc_R_TopHat200(*)
        Group_VelDisp_Mean200(skip:skip+Ngroups-1) = loc_VelDisp_Mean200(*)
        Group_VelDisp_Crit200(skip:skip+Ngroups-1) = loc_VelDisp_Crit200(*)
        Group_VelDisp_TopHat200(skip:skip+Ngroups-1) = loc_VelDisp_TopHat200(*)
        GroupContaminationCount(skip:skip+Ngroups-1) = locContaminationCount(*) 
        GroupContaminationMass(skip:skip+Ngroups-1) = locContaminationMass(*)
        GroupNsubs(skip:skip+Ngroups-1) = locNsubs(*)
        GroupFirstsub(skip:skip+Ngroups-1) = locFirstSub(*)

        skip+= Ngroups
    endif


    if Nsubgroups gt 0 then begin
        
        locLen = lonarr(Nsubgroups)
        locOffset = lonarr(Nsubgroups)
        locParent = lonarr(Nsubgroups)
        locMass = fltarr(Nsubgroups)
        locPos = fltarr(3, Nsubgroups)
        locVel = fltarr(3, Nsubgroups)
        locCM = fltarr(3, Nsubgroups)
        locSpin= fltarr(3, Nsubgroups)
        locVelDisp = fltarr(Nsubgroups)
        locVmax = fltarr(Nsubgroups)
        locVmaxRad = fltarr(Nsubgroups)
        locHalfMassRad = fltarr(Nsubgroups)
        locIDMostBound = lonarr(Nsubgroups)
        locGrNr = lonarr(Nsubgroups)

        readu,1, loclen 
        readu,1, locOffset
        readu,1, locParent
        readu,1, locMass
        readu,1, locPos
        readu,1, locVel
        readu,1, locCM
        readu,1, locSpin
        readu,1, locVelDisp
        readu,1, locVmax
        readu,1, locVmaxRad
        readu,1, locHalfMassRad
        readu,1, locIDMostBound
        readu,1, locGrNr

        SubhaloLen(skip_sub:skip_sub+Nsubgroups-1) = locLen(*)
        SubhaloOffset(skip_sub:skip_sub+Nsubgroups-1) = locOffset(*)
        SubhaloParent(skip_sub:skip_sub+Nsubgroups-1) = locParent(*)
        SubhaloMass(skip_sub:skip_sub+Nsubgroups-1) = locMass(*)
        SubhaloPos(*, skip_sub:skip_sub+Nsubgroups-1) = locPos(*,*)
        SubhaloVel(*, skip_sub:skip_sub+Nsubgroups-1) = locVel(*,*)
        SubhaloCM(*, skip_sub:skip_sub+Nsubgroups-1) = locCM(*,*)
        SubhaloSpin(*, skip_sub:skip_sub+Nsubgroups-1) = locSpin(*,*)
        SubhaloVeldisp(skip_sub:skip_sub+Nsubgroups-1) = locVeldisp(*)
        SubhaloVmax(skip_sub:skip_sub+Nsubgroups-1) = locVmax(*)
        SubhaloVmaxRad(skip_sub:skip_sub+Nsubgroups-1) = locVmaxRad(*)
        SubhaloHalfmassRad(skip_sub:skip_sub+Nsubgroups-1) = locHalfmassRad(*)
        SubhaloIDMostBound(skip_sub:skip_sub+Nsubgroups-1) = locIDMostBound(*)
        SubhaloGrNr(skip_sub:skip_sub+Nsubgroups-1) = locGrNr(*)

        skip_sub+= Nsubgroups
    endif

    close, 1

    fnr++

endrep until fnr eq NTask

print
print, "TotNgroups   =", TotNgroups
print, "TotNsubgroups=", TotNsubgroups
print
print, "Largest group of length ", GroupLen(0)," has", GroupNsubs(0)," substructures"

sfrflag = 0
bhflag = 0

;--------------------------------------------------

skip = 0L
fnr = 0L

repeat begin

    f = Base + "/" + firstgroupsdir + exts +"/subhalo_ids_"+exts +"."+strcompress(string(fnr),/remove_all)

    idNgroups = 0L
    idTotNgroups = 0L
    idNids = 0L
    idTotNids = 0LL
    idNTask = 0L
    idOffset = 0L

    openr,1,f
    readu,1, idNgroups, idTotNgroups, idNids, idTotNids, idNTask, idOffset

    if fnr eq 0 then begin
        IDs = lonarr(idTotNids)
    endif

    if idNids gt 0 then begin
        
        locIDs = lonarr(idNids)
        readu,1, locIDs

        IDs(skip:skip+idNids-1) = locIDs(*)
        skip+= idNids

    endif

    close, 1

    fnr++

endrep until fnr eq idNTask

print
print, "Total IDs in grps=", idTotNids
print

IDs = ulong(IDs)

;--------------------------------------------------

if (cd_cutoff_method EQ 1) then begin
  print, "scaling cutoff_rad with cluster mass  M_crit,200 = ", Group_M_Crit200[group]/1.0e5, " [10^15 M_sun / h]"
  cutoff_rad = cutoff_rad_norm * (Group_M_Crit200[group]/1.0e5)^0.29205
endif
if (cd_cutoff_method LE 1) then print, "cutoff_rad = ", cutoff_rad, " Mpc/h"

if (cd_cutoff_method EQ 2) then begin

f = Base + "/" + firstgroupsdir + exts +"/cDidx_1.dat"
openr,1,f
  
curgroup = 0L
thisgroup = 0L
nstars = 0L
vdsearch = 0L
buff = 0L
Rgrav = 0D
numcD = 0L
while (curgroup LE group) do begin
  readu,1,thisgroup
  if (curgroup NE thisgroup) then print, "Error finding group in cD file"
  readu,1,nstars
  readu,1,vdssearch
  for i=1,14 do readu,1,buff
  readu,1,Rgrav
  readu,1,numcD
  tempcdids = lon64arr(numcD)
  readu,1,tempcdids
  ++curgroup
endwhile
cd_ids = ulonarr(numcD)
cd_ids[*] = tempcdids
tempcdids = 0L
close,1

ind = sort(cd_ids)
cd_ids = cd_ids[ind]
cd_ids_size = n_elements(cd_ids)

print, "read cD data for group", thisgroup
print, "stars in first subhalo = ", nstars
if (vdsearch EQ 1) then print, "fit was done for ICL"
if (vdsearch EQ 2) then print, "fit was done for BCG"
if (vdsearch EQ 3) then begin
  print, "NO FIT TO VELOCITY DISTRIBUTIONS WAS POSSIBLE!!!"
  stop
endif
print, "Rgrav = ", Rgrav
print, "all cD particles = ", numcD

endif

;--------------------------------------------------

numallsubids = 0LL
numallsubids = total(subhalolen[groupfirstsub[group]:groupfirstsub[group]+groupnsubs[group]-1],/integer)
;print,numallsubids

readnew,Base+"/snap_"+exts,head,"HEAD",parttype=4
setcosmo, head.Omega0, head.HubbleParam
printcosmo
head = 0L

readnew,Base+"/snap_"+exts,part_pos,"POS ",parttype=4
readnew,Base+"/snap_"+exts,part_ids,"ID  ",parttype=4
readnew,Base+"/snap_"+exts,part_age,"AGE ",parttype=4
ind = sort(part_ids)
part_ids = part_ids[ind]
part_pos = part_pos[*,ind]
part_age = part_age[ind]
part_ids_size = (size(part_ids))[1]

centerpos = SubhaloPos[*,groupfirstsub[group]]

time_first_star = min(part_age)

;-------------------------------------------------

gal_ids = IDs[subhalooffset[groupfirstsub[group]+1]:subhalooffset[groupfirstsub[group]+groupnsubs[group]-1]+SubhaloLen[groupfirstsub[group]+groupnsubs[group]-1]-1]
gal_ids_size = (size(gal_ids))[1]
gal_exclude_id = lonarr(gal_ids_size)
gal_exclude_id[*] = 1
ind = sort(gal_ids)
gal_ids = gal_ids[ind]
gal_exclude_id = gal_exclude_id[ind]

gal_age = dblarr(gal_ids_size)

galind = 0L
partind = 0L
while (galind LT gal_ids_size AND partind LT part_ids_size) do begin
  if (gal_ids[galind] EQ part_ids[partind]) then begin
    gal_exclude_id[galind] = 0
    gal_age[galind] = part_age[partind]
    ++galind
    ++partind
  endif else if (gal_ids[galind] LT part_ids[partind]) then ++galind else ++partind
endwhile
gal_ids = gal_ids[where(gal_exclude_id EQ 0)]
gal_age = gal_age[where(gal_exclude_id EQ 0)]
gal_ids_size = (size(gal_ids))[1]
gal_exclude_id = 0L

icl_ids = IDs[subhalooffset[groupfirstsub[group]]:subhalooffset[groupfirstsub[group]]+SubhaloLen[groupfirstsub[group]]-1]
if (include_fuzzy EQ 1) then icl_ids = [icl_ids, IDs[subhalooffset[groupfirstsub[group]+groupnsubs[group]-1]+SubhaloLen[groupfirstsub[group]+groupnsubs[group]-1]:grouplen[group]-1]]
ind = sort(icl_ids)
icl_ids = icl_ids[ind]
icl_ids_size = (size(icl_ids))[1]
icl_exclude_id = lonarr(icl_ids_size)
icl_exclude_id[*] = 1

icl_age = dblarr(icl_ids_size)
icl_rad = dblarr(icl_ids_size)

iclind = 0L
partind = 0L
while (iclind LT icl_ids_size AND partind LT part_ids_size) do begin
  if (icl_ids[iclind] EQ part_ids[partind]) then begin
    icl_exclude_id[iclind] = 0
    icl_age[iclind] = part_age[partind]
    icl_rad[iclind] = sqrt((part_pos[0,partind]-centerpos[0])^2+(part_pos[1,partind]-centerpos[1])^2+(part_pos[2,partind]-centerpos[2])^2) 
    ++iclind
    ++partind
  endif else if (icl_ids[iclind] LT part_ids[partind]) then ++iclind else ++partind
endwhile
icl_ids = icl_ids[where(icl_exclude_id EQ 0)]
icl_age = icl_age[where(icl_exclude_id EQ 0)]
icl_rad = icl_rad[where(icl_exclude_id EQ 0)] 
icl_ids_size = (size(icl_ids))[1]
icl_exclude_id = 0L
print, "stars in BCG+ICL = ", icl_ids_size

;--------------------------------------------------

if (cd_cutoff_method LE 1) then begin

is_bcg = lonarr(icl_ids_size)
is_bcg[where(icl_rad LT cutoff_rad)] = 1
 
bcg_ids = icl_ids[where(is_bcg EQ 1)]
bcg_age = icl_age[where(is_bcg EQ 1)]
bcg_ids_size = (size(bcg_ids))[1]

icl_ids = icl_ids[where(is_bcg EQ 0)]
icl_age = icl_age[where(is_bcg EQ 0)] 
icl_ids_size = (size(icl_ids))[1]

is_bcg = 0L

endif else begin

iclind = 0L
cdind = 0L
icl_exclude_id = lonarr(icl_ids_size)
icl_exclude_id[*] = 0
while (iclind LT icl_ids_size AND cdind LT cd_ids_size) do begin
  if (icl_ids[iclind] EQ cd_ids[cdind]) then begin
    icl_exclude_id[iclind] = 1
    ++iclind
    ++cdind
  endif else if (icl_ids[iclind] LT cd_ids[cdind]) then ++iclind else ++cdind
endwhile

bcg_ids = icl_ids[where(icl_exclude_id EQ 1)]
bcg_age = icl_age[where(icl_exclude_id EQ 1)]
bcg_ids_size = (size(bcg_ids))[1]

icl_ids = icl_ids[where(icl_exclude_id EQ 0)]
icl_age = icl_age[where(icl_exclude_id EQ 0)]
icl_ids_size = (size(icl_ids))[1]

print, total(icl_exclude_id,/integer), " cD stars removed from ICL"
icl_exclude_id = 0L

endelse

icl_rad = 0L

;--------------------------------------------------

print, "gal stars = ", gal_ids_size
print, "gal star fraction = ", double(gal_ids_size)/double(gal_ids_size+bcg_ids_size+icl_ids_size)
print, "bcg stars = ", bcg_ids_size
print, "bcg star fraction = ", double(bcg_ids_size)/double(gal_ids_size+bcg_ids_size+icl_ids_size)
print, "icl stars = ", icl_ids_size
print, "icl star fraction = ", double(icl_ids_size)/double(gal_ids_size+bcg_ids_size+icl_ids_size)

idsind = 0L
partind = 0L
stars_in_group = 0L
sorted_IDs = IDs[0:grouplen[group]-1]
sorted_IDs = sorted_IDs[sort(sorted_IDs)]
ids_size = n_elements(sorted_IDs)
while (idsind LT ids_size AND partind LT part_ids_size) do begin
  if (sorted_IDs[idsind] EQ part_ids[partind]) then begin
    stars_in_group += 1
    ++idsind
    ++partind
  endif else if (sorted_IDs[idsind] LT part_ids[partind]) then ++idsind else ++partind
endwhile
sorted_IDs = 0L
print, "all stars in group = ", stars_in_group

;--------------------------------------------------

all_part_ids = part_ids
all_part_ids_size = part_ids_size
part_ids = 0L
part_ids_size = 0

;--------------------------------------------------

openr,1,"outputs_selection.txt"
snaptimes = dblarr(64)
readf,1,snaptimes
close,1

minsnap = (where(snaptimes GT time_first_star))[0]
maxsnap = Num

snapsoftphys = snaptimes*0.015
snapsoftphys[where(snapsoftphys GT 0.0025)] = 0.0025

;----- get galaxy merger tree ---------------------

num_subhalos = lonarr(numsnaps)

have_main_progenitors = intarr(numsnaps)
subhalo_main_progenitor = ptrarr(numsnaps) 

subhalo_mass_tab = ptrarr(numsnaps)
subhalo_pos = ptrarr(numsnaps)
subhalo_vel = ptrarr(numsnaps)
subhalo_halfmassrad = ptrarr(numsnaps)
subhalo_halflightrad = ptrarr(numsnaps) 
subhalo_grnr = ptrarr(numsnaps)

for cursnap=minsnap,numsnaps-1 do begin
    print
    print, "doing snapshot ", cursnap
    print
    if cursnap ge 1000 then begin
      exts='0000'
      exts=exts+strcompress(string(cursnap),/remove_all)
      exts=strmid(exts,strlen(exts)-4,4)
    endif else begin
      exts='000'
      exts=exts+strcompress(string(cursnap),/remove_all)
      exts=strmid(exts,strlen(exts)-3,3)
    endelse 
   
    ;read subhalo info      
    skip = 0L
    skip_sub = 0L
    fnr = 0L

  repeat begin

    f = Base + "/groups_" + exts +"/subhalo_tab_"+exts +"."+strcompress(string(fnr),/remove_all)

    Ngroups = 0L
    TotNgroups = 0L
    Nids = 0L
    TotNids = 0LL
    NTask = 0L
    Nsubgroups = 0L
    TotNsubgroups = 0L

    openr,1,f
    readu,1, Ngroups, TotNgroups, Nids, TotNids, NTask, Nsubgroups, TotNsubgroups

    if fnr eq 0 then begin
        GroupLen = lonarr(TotNgroups)
        GroupOffset = lonarr(TotNgroups)
        GroupMass = fltarr(TotNgroups)
        GroupPos = fltarr(3, TotNgroups)
        Group_M_Mean200 = fltarr(TotNgroups)
        Group_R_Mean200 = fltarr(TotNgroups)
        Group_M_Crit200 = fltarr(TotNgroups)
        Group_R_Crit200 = fltarr(TotNgroups)
        Group_M_TopHat200 = fltarr(TotNgroups)
        Group_R_TopHat200 = fltarr(TotNgroups)
        Group_VelDisp_Mean200 = fltarr(TotNgroups)
        Group_VelDisp_Crit200 = fltarr(TotNgroups)
        Group_VelDisp_TopHat200 = fltarr(TotNgroups)
        GroupContaminationCount = lonarr(TotNgroups)
        GroupContaminationMass = fltarr(TotNgroups)
        GroupNsubs = lonarr(TotNgroups)
        GroupFirstSub = lonarr(TotNgroups)

        SubhaloLen = lonarr(TotNsubgroups)
        SubhaloOffset = lonarr(TotNsubgroups)
        SubhaloParent = lonarr(TotNsubgroups)
        SubhaloMass = fltarr(TotNsubgroups)
        SubhaloPos = fltarr(3, TotNsubgroups)
        SubhaloVel = fltarr(3, TotNsubgroups)
        SubhaloCM = fltarr(3, TotNsubgroups)
        SubhaloSpin = fltarr(3, TotNsubgroups)
        SubhaloVelDisp = fltarr(TotNsubgroups)
        SubhaloVmax = fltarr(TotNsubgroups)
        SubhaloVmaxRad = fltarr(TotNsubgroups)
        SubhaloHalfmassRad = fltarr(TotNsubgroups)
        SubhaloIDMostbound = lonarr(TotNsubgroups)
        SubhaloGrNr = lonarr(TotNsubgroups)
        SubhaloMassTab = fltarr(6, TotNsubgroups)
    endif

    if Ngroups gt 0 then begin
        
        locLen = lonarr(Ngroups)
        locOffset = lonarr(Ngroups)
        locMass = fltarr(Ngroups)
        locPos = fltarr(3, Ngroups)
        loc_M_Mean200 = fltarr(Ngroups)
        loc_R_Mean200 = fltarr(Ngroups)
        loc_M_Crit200 = fltarr(Ngroups)
        loc_R_Crit200 = fltarr(Ngroups)
        loc_M_TopHat200 = fltarr(Ngroups)
        loc_R_TopHat200 = fltarr(Ngroups)
        loc_VelDisp_Mean200 = fltarr(Ngroups)
        loc_VelDisp_Crit200 = fltarr(Ngroups)
        loc_VelDisp_TopHat200 = fltarr(Ngroups)
        locContaminationCount = lonarr(Ngroups)
        locContaminationMass = fltarr(Ngroups)
        locNsubs = lonarr(Ngroups)
        locFirstSub = lonarr(Ngroups)

        readu,1, loclen 
        readu,1, locOffset
        readu,1, locMass
        readu,1, locPos
        readu,1, loc_M_Mean200
        readu,1, loc_R_Mean200
        readu,1, loc_M_Crit200
        readu,1, loc_R_Crit200
        readu,1, loc_M_TopHat200
        readu,1, loc_R_TopHat200
        if FLAG_Group_VelDisp ne 0 then begin
          readu,1, loc_VelDisp_Mean200
          readu,1, loc_VelDisp_Crit200
          readu,1, loc_VelDisp_TopHat200
        endif
        readu,1, locContaminationCount
        readu,1, locContaminationMass
        readu,1, locNsubs
        readu,1, locFirstSub

        GroupLen(skip:skip+Ngroups-1) = locLen(*)
        GroupOffset(skip:skip+Ngroups-1) = locOffset(*)
        GroupMass(skip:skip+Ngroups-1) = locMass(*)
        GroupPos(*, skip:skip+Ngroups-1) = locPos(*,*)
        Group_M_Mean200(skip:skip+Ngroups-1) = loc_M_Mean200(*)
        Group_R_Mean200(skip:skip+Ngroups-1) = loc_R_Mean200(*)
        Group_M_Crit200(skip:skip+Ngroups-1) = loc_M_Crit200(*)
        Group_R_Crit200(skip:skip+Ngroups-1) = loc_R_Crit200(*)
        Group_M_TopHat200(skip:skip+Ngroups-1) = loc_M_TopHat200(*)
        Group_R_TopHat200(skip:skip+Ngroups-1) = loc_R_TopHat200(*)
        Group_VelDisp_Mean200(skip:skip+Ngroups-1) = loc_VelDisp_Mean200(*)
        Group_VelDisp_Crit200(skip:skip+Ngroups-1) = loc_VelDisp_Crit200(*)
        Group_VelDisp_TopHat200(skip:skip+Ngroups-1) = loc_VelDisp_TopHat200(*)
        GroupContaminationCount(skip:skip+Ngroups-1) = locContaminationCount(*) 
        GroupContaminationMass(skip:skip+Ngroups-1) = locContaminationMass(*)
        GroupNsubs(skip:skip+Ngroups-1) = locNsubs(*)
        GroupFirstsub(skip:skip+Ngroups-1) = locFirstSub(*)

        skip+= Ngroups
    endif


    if Nsubgroups gt 0 then begin
        
        locLen = lonarr(Nsubgroups)
        locOffset = lonarr(Nsubgroups)
        locParent = lonarr(Nsubgroups)
        locMass = fltarr(Nsubgroups)
        locPos = fltarr(3, Nsubgroups)
        locVel = fltarr(3, Nsubgroups)
        locCM = fltarr(3, Nsubgroups)
        locSpin= fltarr(3, Nsubgroups)
        locVelDisp = fltarr(Nsubgroups)
        locVmax = fltarr(Nsubgroups)
        locVmaxRad = fltarr(Nsubgroups)
        locHalfMassRad = fltarr(Nsubgroups)
        locIDMostBound = lonarr(Nsubgroups)
        locGrNr = lonarr(Nsubgroups)
        locMassTab = fltarr(6, Nsubgroups)

        readu,1, loclen 
        readu,1, locOffset
        readu,1, locParent
        readu,1, locMass
        readu,1, locPos
        readu,1, locVel
        readu,1, locCM
        readu,1, locSpin
        readu,1, locVelDisp
        readu,1, locVmax
        readu,1, locVmaxRad
        readu,1, locHalfMassRad
        readu,1, locIDMostBound
        readu,1, locGrNr
        readu,1, locMassTab

        SubhaloLen(skip_sub:skip_sub+Nsubgroups-1) = locLen(*)
        SubhaloOffset(skip_sub:skip_sub+Nsubgroups-1) = locOffset(*)
        SubhaloParent(skip_sub:skip_sub+Nsubgroups-1) = locParent(*)
        SubhaloMass(skip_sub:skip_sub+Nsubgroups-1) = locMass(*)
        SubhaloPos(*, skip_sub:skip_sub+Nsubgroups-1) = locPos(*,*)
        SubhaloVel(*, skip_sub:skip_sub+Nsubgroups-1) = locVel(*,*)
        SubhaloCM(*, skip_sub:skip_sub+Nsubgroups-1) = locCM(*,*)
        SubhaloSpin(*, skip_sub:skip_sub+Nsubgroups-1) = locSpin(*,*)
        SubhaloVeldisp(skip_sub:skip_sub+Nsubgroups-1) = locVeldisp(*)
        SubhaloVmax(skip_sub:skip_sub+Nsubgroups-1) = locVmax(*)
        SubhaloVmaxRad(skip_sub:skip_sub+Nsubgroups-1) = locVmaxRad(*)
        SubhaloHalfmassRad(skip_sub:skip_sub+Nsubgroups-1) = locHalfmassRad(*)
        SubhaloIDMostBound(skip_sub:skip_sub+Nsubgroups-1) = locIDMostBound(*)
        SubhaloGrNr(skip_sub:skip_sub+Nsubgroups-1) = locGrNr(*)
        SubhaloMassTab(*, skip_sub:skip_sub+Nsubgroups-1) = locMassTab(*,*)

        skip_sub+= Nsubgroups
    endif

    close, 1

    fnr++

  endrep until fnr eq NTask

  print
  print, "TotNgroups   =", TotNgroups
  print, "TotNsubgroups=", TotNsubgroups
  print
  print, "Largest group of length ", GroupLen(0)," has", GroupNsubs(0)," substructures"

  sfrflag = 0
  bhflag = 0

  num_subhalos[cursnap] = TotNsubgroups
  
  ptr_free, subhalo_mass_tab[cursnap]
  subhalo_mass_tab[cursnap] = ptr_new(dblarr(6,TotNsubgroups))
  *subhalo_mass_tab[cursnap] = SubhaloMassTab  

  ptr_free, subhalo_pos[cursnap]
  subhalo_pos[cursnap] = ptr_new(dblarr(3,TotNsubgroups))
  *subhalo_pos[cursnap] = SubhaloPos

  ptr_free, subhalo_vel[cursnap]
  subhalo_vel[cursnap] = ptr_new(dblarr(3,TotNsubgroups))
  *subhalo_vel[cursnap] = SubhaloVel

  ptr_free, subhalo_halfmassrad[cursnap]
  subhalo_halfmassrad[cursnap] = ptr_new(dblarr(TotNsubgroups))
  *subhalo_halfmassrad[cursnap] = SubhaloHalfmassRad

  ptr_free, subhalo_grnr[cursnap]
  subhalo_grnr[cursnap] = ptr_new(lonarr(TotNsubgroups))
  *subhalo_grnr[cursnap] = SubhaloGrNr

  ;--------------------------------------------------

  skip = 0L
  fnr = 0L

  repeat begin

    f = Base + "/groups_" + exts +"/subhalo_ids_"+exts +"."+strcompress(string(fnr),/remove_all)

    idNgroups = 0L
    idTotNgroups = 0L
    idNids = 0L
    idTotNids = 0LL
    idNTask = 0L
    idOffset = 0L

    openr,1,f
    readu,1, idNgroups, idTotNgroups, idNids, idTotNids, idNTask, idOffset

    if fnr eq 0 then begin
        IDs = lonarr(idTotNids)
    endif

    if idNids gt 0 then begin
        
        locIDs = lonarr(idNids)
        readu,1, locIDs

        IDs(skip:skip+idNids-1) = locIDs(*)
        skip+= idNids

    endif

    close, 1

    fnr++

  endrep until fnr eq idNTask

  print
  print, "Total IDs in grps=", idTotNids
  print

  IDs = ulong(IDs)
  
    ;done reading subhalo info, now get subhalo  
   
    subhalo = lonarr(idTotNids) 
    subhalo[*] = -1
    for i=0,TotNsubgroups-1 do begin
      subhalo[SubhaloOffset[i]:SubhaloOffset[i]+SubhaloLen[i]-1] = i
    endfor
 
    ind = sort(IDs)
    IDs = IDs[ind]
    subhalo = subhalo[ind]
    ids_size = n_elements(IDs)

    readnew,Base+"/snap_"+exts,head,"HEAD",parttype=4
    snaptimes[cursnap] = head.time

    if (use_all_snaps EQ 1) then begin
      readnew,Base+"/snap_"+exts,part_pos,"POS ",parttype=4
      readnew,Base+"/snap_"+exts,part_ids,"ID  ",parttype=4
      ind = sort(part_ids)
      part_ids = part_ids[ind]
      part_pos = part_pos[*,ind]
      part_ids_size = (size(part_ids))[1]
    endif else begin
      part_ids = all_part_ids[where(part_age LT snaptimes[cursnap])]
      part_ids_size = (size(part_ids))[1]
    endelse 

    idsind = 0L
    partind = 0L
    include_id = intarr(ids_size)
    where_part = lonarr(part_ids_size)
    where_part[*] = -1
    while (idsind LT ids_size AND partind LT part_ids_size) do begin
      if (IDs[idsind] EQ part_ids[partind]) then begin
        include_id(idsind) = 1
        where_part[partind] = idsind
        ++idsind
        ++partind
      endif else if (IDs[idsind] LT part_ids[partind]) then ++idsind else ++partind
    endwhile

    if (use_all_snaps EQ 1) then begin
      ind = where(where_part GE 0)
      part_subhalo = lonarr(part_ids_size)
      part_subhalo[*] = -2
      if (ind[0] GE 0) then part_subhalo[ind] = subhalo[where_part[ind]]
      where_part = 0L
    endif 

    ind = where(include_id EQ 1)
    IDs = IDs[ind]
    subhalo = subhalo[ind]
    ids_size = n_elements(IDs) 
    include_id = 0L
    part_ids = 0L
    
    halflightrad = dblarr(TotNsubgroups) ; get half light (stellar mass) radius 
    if (use_all_snaps EQ 1) then begin
      for i=0L,TotNsubgroups-1 do begin
        ind = where(part_subhalo EQ i)
        if (ind[0] GE 0) then halflightrad[i] = median((part_pos[0,ind]-SubhaloPos[0,i])^2 + (part_pos[1,ind]-SubhaloPos[1,i])^2 + (part_pos[2,ind]-SubhaloPos[2,i])^2)  
      endfor
      halflightrad = sqrt(halflightrad)*snaptimes[cursnap] ;physical halflight radius
      part_pos = 0L
      part_subhalo = 0L
    endif

    ptr_free, subhalo_halflightrad[cursnap]
    subhalo_halflightrad[cursnap] = ptr_new(dblarr(TotNsubgroups))
    *subhalo_halflightrad[cursnap] = halflightrad
    halflightrad = 0L

    if (cursnap GT minsnap) then begin
    ;find main progenitors in this snap
   
      part_from_to = lonarr(old_TotNsubgroups,TotNsubgroups)      
      old_idsind = 0L
      idsind = 0L
      while (old_idsind LT old_ids_size AND idsind LT ids_size) do begin
        if (old_IDs[old_idsind] EQ IDs[idsind]) then begin
          if (old_subhalo[old_idsind] GE 0 AND subhalo[idsind] GE 0) then part_from_to[old_subhalo[old_idsind],subhalo[idsind]] += 1
          ++old_idsind
          ++idsind
        endif else if (old_IDs[old_idsind] LT IDs[idsind]) then ++old_idsind else ++idsind
      endwhile

      old_starnum = lonarr(old_TotNsubgroups) 
      for old_idsind=0L,old_ids_size-1 do begin
        if (old_subhalo[old_idsind] GE 0) then old_starnum[old_subhalo[old_idsind]] += 1 
      endfor

      ptr_free, subhalo_main_progenitor[cursnap]
      subhalo_main_progenitor[cursnap] = ptr_new(lonarr(TotNsubgroups))
      for i=0L,TotNsubgroups-1 do begin
        maxpart = max(part_from_to[*,i],progenitor)
        if (maxpart LE 0) then progenitor = -1
        (*subhalo_main_progenitor[cursnap])[i] = progenitor
      endfor
      have_main_progenitors[cursnap] = 1

      part_from_to = 0L
      old_starnum = 0L

    endif

    old_TotNsubgroups = TotNsubgroups
    old_IDs = IDs
    old_subhalo = subhalo
    old_ids_size = ids_size
  endfor

;----- find main BCG progenitor -------------------

BCG_progenitor = lonarr(numsnaps)
BCG_progenitor[*] = -1
BCG_progenitor[Num] = 0
cursnap = Num
while (cursnap GT minsnap AND BCG_progenitor[cursnap] GE 0) do begin
  curprog = (*subhalo_main_progenitor[cursnap])[BCG_progenitor[cursnap]]
  if (curprog GE 0) then BCG_progenitor[cursnap-1] = curprog
  --cursnap
endwhile

BCG_progenitor_grnr = lonarr(numsnaps)
for i=0L,numsnaps-1 do begin
  if (BCG_progenitor[i] GE 0) then BCG_progenitor_grnr[i] = (*subhalo_grnr[i])[BCG_progenitor[i]]
endfor

print, "BCG progenitors [snap, sub, gr]"
for i=0L,numsnaps-1 do print, i, BCG_progenitor[i], BCG_progenitor_grnr[i]
print

;----- find main BCG progenitor orbit -------------

finenum = 32
BCG_progenitor_pos_fine = dblarr(3,(numsnaps-1)*finenum+1)
snaptimes_fine = dblarr((numsnaps-1)*finenum+1)
for i=minsnap,Num-1 do begin
  if (BCG_progenitor[i] GE 0) then begin
    time1 = 0.0D
    time2 = time(snaptimes[i],snaptimes[i+1])
    scale1=snaptimes[i]
    scale2=snaptimes[i+1]
    for j=0,finenum-1 do begin
      cur_time = time(snaptimes[i],snaptimes[i]+double(j)/double(finenum)*(snaptimes[i+1]-snaptimes[i]))
      cur_scale = snaptimes[i]+double(j)/double(finenum)*(snaptimes[i+1]-snaptimes[i])
      snaptimes_fine[i*finenum+j] = cur_scale
      BCG_progenitor_pos_fine[0,i*finenum+j] = interpolate_orbit( (*subhalo_pos[i])[0,BCG_progenitor[i]], (*subhalo_vel[i])[0,BCG_progenitor[i]]/scale1, time1, (*subhalo_pos[i+1])[0,BCG_progenitor[i+1]], (*subhalo_vel[i+1])[0,BCG_progenitor[i+1]]/scale2, time2, cur_time)
      BCG_progenitor_pos_fine[1,i*finenum+j] = interpolate_orbit( (*subhalo_pos[i])[1,BCG_progenitor[i]], (*subhalo_vel[i])[1,BCG_progenitor[i]]/scale1, time1, (*subhalo_pos[i+1])[1,BCG_progenitor[i+1]], (*subhalo_vel[i+1])[1,BCG_progenitor[i+1]]/scale2, time2, cur_time)
      BCG_progenitor_pos_fine[2,i*finenum+j] = interpolate_orbit( (*subhalo_pos[i])[2,BCG_progenitor[i]], (*subhalo_vel[i])[2,BCG_progenitor[i]]/scale1, time1, (*subhalo_pos[i+1])[2,BCG_progenitor[i+1]], (*subhalo_vel[i+1])[2,BCG_progenitor[i+1]]/scale2, time2, cur_time)
   endfor
  endif
endfor
snaptimes_fine[Num*finenum] = snaptimes[Num]
BCG_progenitor_pos_fine[*,Num*finenum] = (*subhalo_pos[Num])[*,BCG_progenitor[Num]]

;----- find formation and fall-in and strip galaxies

have_descendants = intarr(numsnaps)
subhalo_descendants = ptrarr(numsnaps)

gal_formation_halo_snap = lonarr(gal_ids_size)
gal_formation_halo_snap[*] = -1
bcg_formation_halo_snap = lonarr(bcg_ids_size)
bcg_formation_halo_snap[*] = -1
icl_formation_halo_snap = lonarr(icl_ids_size)
icl_formation_halo_snap[*] = -1

gal_formation_halo_sub = lonarr(gal_ids_size)
gal_formation_halo_sub[*] = -2
bcg_formation_halo_sub = lonarr(bcg_ids_size)
bcg_formation_halo_sub[*] = -2
icl_formation_halo_sub = lonarr(icl_ids_size)
icl_formation_halo_sub[*] = -2

gal_fall_in_halo_snap = lonarr(gal_ids_size)
gal_fall_in_halo_snap[*] = -1
bcg_fall_in_halo_snap = lonarr(bcg_ids_size)
bcg_fall_in_halo_snap[*] = -1
icl_fall_in_halo_snap = lonarr(icl_ids_size)
icl_fall_in_halo_snap[*] = -1

gal_fall_in_halo_sub = lonarr(gal_ids_size)
gal_fall_in_halo_sub[*] = -2
bcg_fall_in_halo_sub = lonarr(bcg_ids_size)
bcg_fall_in_halo_sub[*] = -2
icl_fall_in_halo_sub = lonarr(icl_ids_size)
icl_fall_in_halo_sub[*] = -2

gal_fall_in_halo_mass = fltarr(gal_ids_size)
gal_fall_in_halo_mass[*] = -1
bcg_fall_in_halo_mass = fltarr(bcg_ids_size)
bcg_fall_in_halo_mass[*] = -1
icl_fall_in_halo_mass = fltarr(icl_ids_size)
icl_fall_in_halo_mass[*] = -1

bcg_strip_halo_snap = lonarr(bcg_ids_size)
bcg_strip_halo_snap[*] = -1
bcg_strip_halo_sub = lonarr(bcg_ids_size)
bcg_strip_halo_sub[*] = -2 

icl_strip_halo_snap = lonarr(icl_ids_size)
icl_strip_halo_snap[*] = -1
icl_strip_halo_sub = lonarr(icl_ids_size)
icl_strip_halo_sub[*] = -2

gal_has_been_in_BCG = intarr(gal_ids_size)
bcg_has_been_in_BCG = intarr(bcg_ids_size)
icl_has_been_in_BCG = intarr(icl_ids_size)

if (check_before EQ 1) then begin
  gal_before_formation_snap = lonarr(gal_ids_size)
  gal_before_formation_snap[*] = -1
  gal_before_formation_sub = lonarr(gal_ids_size)
  gal_before_formation_sub[*] = -3
  
  bcg_before_formation_snap = lonarr(bcg_ids_size)
  bcg_before_formation_snap[*] = -1
  bcg_before_formation_sub = lonarr(bcg_ids_size)
  bcg_before_formation_sub[*] = -3

  icl_before_formation_snap = lonarr(icl_ids_size)
  icl_before_formation_snap[*] = -1
  icl_before_formation_sub = lonarr(icl_ids_size)
  icl_before_formation_sub[*] = -3
endif

for cursnap=minsnap,numsnaps-1 do begin
    print
    print, "doing snapshot ", cursnap
    print
    if cursnap ge 1000 then begin
      exts='0000'
      exts=exts+strcompress(string(cursnap),/remove_all)
      exts=strmid(exts,strlen(exts)-4,4)
    endif else begin
      exts='000'
      exts=exts+strcompress(string(cursnap),/remove_all)
      exts=strmid(exts,strlen(exts)-3,3)
    endelse 
   
    ;read subhalo info      
    skip = 0L
    skip_sub = 0L
    fnr = 0L

  repeat begin

    f = Base + "/groups_" + exts +"/subhalo_tab_"+exts +"."+strcompress(string(fnr),/remove_all)

    Ngroups = 0L
    TotNgroups = 0L
    Nids = 0L
    TotNids = 0LL
    NTask = 0L
    Nsubgroups = 0L
    TotNsubgroups = 0L

    openr,1,f
    readu,1, Ngroups, TotNgroups, Nids, TotNids, NTask, Nsubgroups, TotNsubgroups

    if fnr eq 0 then begin
        GroupLen = lonarr(TotNgroups)
        GroupOffset = lonarr(TotNgroups)
        GroupMass = fltarr(TotNgroups)
        GroupPos = fltarr(3, TotNgroups)
        Group_M_Mean200 = fltarr(TotNgroups)
        Group_R_Mean200 = fltarr(TotNgroups)
        Group_M_Crit200 = fltarr(TotNgroups)
        Group_R_Crit200 = fltarr(TotNgroups)
        Group_M_TopHat200 = fltarr(TotNgroups)
        Group_R_TopHat200 = fltarr(TotNgroups)
        Group_VelDisp_Mean200 = fltarr(TotNgroups)
        Group_VelDisp_Crit200 = fltarr(TotNgroups)
        Group_VelDisp_TopHat200 = fltarr(TotNgroups)
        GroupContaminationCount = lonarr(TotNgroups)
        GroupContaminationMass = fltarr(TotNgroups)
        GroupNsubs = lonarr(TotNgroups)
        GroupFirstSub = lonarr(TotNgroups)

        SubhaloLen = lonarr(TotNsubgroups)
        SubhaloOffset = lonarr(TotNsubgroups)
        SubhaloParent = lonarr(TotNsubgroups)
        SubhaloMass = fltarr(TotNsubgroups)
        SubhaloPos = fltarr(3, TotNsubgroups)
        SubhaloVel = fltarr(3, TotNsubgroups)
        SubhaloCM = fltarr(3, TotNsubgroups)
        SubhaloSpin = fltarr(3, TotNsubgroups)
        SubhaloVelDisp = fltarr(TotNsubgroups)
        SubhaloVmax = fltarr(TotNsubgroups)
        SubhaloVmaxRad = fltarr(TotNsubgroups)
        SubhaloHalfmassRad = fltarr(TotNsubgroups)
        SubhaloIDMostbound = lonarr(TotNsubgroups)
        SubhaloGrNr = lonarr(TotNsubgroups)
        SubhaloMassTab = fltarr(6, TotNsubgroups)
    endif

    if Ngroups gt 0 then begin
        
        locLen = lonarr(Ngroups)
        locOffset = lonarr(Ngroups)
        locMass = fltarr(Ngroups)
        locPos = fltarr(3, Ngroups)
        loc_M_Mean200 = fltarr(Ngroups)
        loc_R_Mean200 = fltarr(Ngroups)
        loc_M_Crit200 = fltarr(Ngroups)
        loc_R_Crit200 = fltarr(Ngroups)
        loc_M_TopHat200 = fltarr(Ngroups)
        loc_R_TopHat200 = fltarr(Ngroups)
        loc_VelDisp_Mean200 = fltarr(Ngroups)
        loc_VelDisp_Crit200 = fltarr(Ngroups)
        loc_VelDisp_TopHat200 = fltarr(Ngroups)
        locContaminationCount = lonarr(Ngroups)
        locContaminationMass = fltarr(Ngroups)
        locNsubs = lonarr(Ngroups)
        locFirstSub = lonarr(Ngroups)

        readu,1, loclen 
        readu,1, locOffset
        readu,1, locMass
        readu,1, locPos
        readu,1, loc_M_Mean200
        readu,1, loc_R_Mean200
        readu,1, loc_M_Crit200
        readu,1, loc_R_Crit200
        readu,1, loc_M_TopHat200
        readu,1, loc_R_TopHat200
        if FLAG_Group_VelDisp ne 0 then begin
          readu,1, loc_VelDisp_Mean200
          readu,1, loc_VelDisp_Crit200
          readu,1, loc_VelDisp_TopHat200
        endif
        readu,1, locContaminationCount
        readu,1, locContaminationMass
        readu,1, locNsubs
        readu,1, locFirstSub

        GroupLen(skip:skip+Ngroups-1) = locLen(*)
        GroupOffset(skip:skip+Ngroups-1) = locOffset(*)
        GroupMass(skip:skip+Ngroups-1) = locMass(*)
        GroupPos(*, skip:skip+Ngroups-1) = locPos(*,*)
        Group_M_Mean200(skip:skip+Ngroups-1) = loc_M_Mean200(*)
        Group_R_Mean200(skip:skip+Ngroups-1) = loc_R_Mean200(*)
        Group_M_Crit200(skip:skip+Ngroups-1) = loc_M_Crit200(*)
        Group_R_Crit200(skip:skip+Ngroups-1) = loc_R_Crit200(*)
        Group_M_TopHat200(skip:skip+Ngroups-1) = loc_M_TopHat200(*)
        Group_R_TopHat200(skip:skip+Ngroups-1) = loc_R_TopHat200(*)
        Group_VelDisp_Mean200(skip:skip+Ngroups-1) = loc_VelDisp_Mean200(*)
        Group_VelDisp_Crit200(skip:skip+Ngroups-1) = loc_VelDisp_Crit200(*)
        Group_VelDisp_TopHat200(skip:skip+Ngroups-1) = loc_VelDisp_TopHat200(*)
        GroupContaminationCount(skip:skip+Ngroups-1) = locContaminationCount(*) 
        GroupContaminationMass(skip:skip+Ngroups-1) = locContaminationMass(*)
        GroupNsubs(skip:skip+Ngroups-1) = locNsubs(*)
        GroupFirstsub(skip:skip+Ngroups-1) = locFirstSub(*)

        skip+= Ngroups
    endif


    if Nsubgroups gt 0 then begin
        
        locLen = lonarr(Nsubgroups)
        locOffset = lonarr(Nsubgroups)
        locParent = lonarr(Nsubgroups)
        locMass = fltarr(Nsubgroups)
        locPos = fltarr(3, Nsubgroups)
        locVel = fltarr(3, Nsubgroups)
        locCM = fltarr(3, Nsubgroups)
        locSpin= fltarr(3, Nsubgroups)
        locVelDisp = fltarr(Nsubgroups)
        locVmax = fltarr(Nsubgroups)
        locVmaxRad = fltarr(Nsubgroups)
        locHalfMassRad = fltarr(Nsubgroups)
        locIDMostBound = lonarr(Nsubgroups)
        locGrNr = lonarr(Nsubgroups)
        locMassTab = fltarr(6, Nsubgroups)

        readu,1, loclen 
        readu,1, locOffset
        readu,1, locParent
        readu,1, locMass
        readu,1, locPos
        readu,1, locVel
        readu,1, locCM
        readu,1, locSpin
        readu,1, locVelDisp
        readu,1, locVmax
        readu,1, locVmaxRad
        readu,1, locHalfMassRad
        readu,1, locIDMostBound
        readu,1, locGrNr
        readu,1, locMassTab

        SubhaloLen(skip_sub:skip_sub+Nsubgroups-1) = locLen(*)
        SubhaloOffset(skip_sub:skip_sub+Nsubgroups-1) = locOffset(*)
        SubhaloParent(skip_sub:skip_sub+Nsubgroups-1) = locParent(*)
        SubhaloMass(skip_sub:skip_sub+Nsubgroups-1) = locMass(*)
        SubhaloPos(*, skip_sub:skip_sub+Nsubgroups-1) = locPos(*,*)
        SubhaloVel(*, skip_sub:skip_sub+Nsubgroups-1) = locVel(*,*)
        SubhaloCM(*, skip_sub:skip_sub+Nsubgroups-1) = locCM(*,*)
        SubhaloSpin(*, skip_sub:skip_sub+Nsubgroups-1) = locSpin(*,*)
        SubhaloVeldisp(skip_sub:skip_sub+Nsubgroups-1) = locVeldisp(*)
        SubhaloVmax(skip_sub:skip_sub+Nsubgroups-1) = locVmax(*)
        SubhaloVmaxRad(skip_sub:skip_sub+Nsubgroups-1) = locVmaxRad(*)
        SubhaloHalfmassRad(skip_sub:skip_sub+Nsubgroups-1) = locHalfmassRad(*)
        SubhaloIDMostBound(skip_sub:skip_sub+Nsubgroups-1) = locIDMostBound(*)
        SubhaloGrNr(skip_sub:skip_sub+Nsubgroups-1) = locGrNr(*)
        SubhaloMassTab(*, skip_sub:skip_sub+Nsubgroups-1) = locMassTab(*,*)

        skip_sub+= Nsubgroups
    endif

    close, 1

    fnr++

  endrep until fnr eq NTask

  print
  print, "TotNgroups   =", TotNgroups
  print, "TotNsubgroups=", TotNsubgroups
  print
  print, "Largest group of length ", GroupLen(0)," has", GroupNsubs(0)," substructures"

  sfrflag = 0
  bhflag = 0

  ;--------------------------------------------------

  skip = 0L
  fnr = 0L

  repeat begin

    f = Base + "/groups_" + exts +"/subhalo_ids_"+exts +"."+strcompress(string(fnr),/remove_all)

    idNgroups = 0L
    idTotNgroups = 0L
    idNids = 0L
    idTotNids = 0LL
    idNTask = 0L
    idOffset = 0L

    openr,1,f
    readu,1, idNgroups, idTotNgroups, idNids, idTotNids, idNTask, idOffset

    if fnr eq 0 then begin
        IDs = lonarr(idTotNids)
    endif

    if idNids gt 0 then begin
        
        locIDs = lonarr(idNids)
        readu,1, locIDs

        IDs(skip:skip+idNids-1) = locIDs(*)
        skip+= idNids

    endif

    close, 1

    fnr++

  endrep until fnr eq idNTask

  print
  print, "Total IDs in grps=", idTotNids
  print

  IDs = ulong(IDs)
  
    ;done reading subhalo info, now get subhalo  
   
    subhalo = lonarr(idTotNids) 
    subhalo[*] = -1 ;; -1 if not a subhalo but either a group or no halo
    for i=0,TotNsubgroups-1 do begin ;;fill array with host subhalos
      subhalo[SubhaloOffset[i]:SubhaloOffset[i]+SubhaloLen[i]-1] = i
    endfor
 
    ind = sort(IDs)
    IDs = IDs[ind] ;;IDs of all particles in groups or subgroups
    subhalo = subhalo[ind]
    ids_size = n_elements(IDs)

    if (check_before EQ 1 AND cursnap LT numsnaps-1) then begin
      gal_gas_ind = where(gal_age GE snaptimes[cursnap] AND gal_age LT snaptimes[cursnap+1])
      if (gal_gas_ind[0] GE 0) then gal_gas_size = (size(gal_gas_ind))[1] else gal_gas_size = 0
      bcg_gas_ind = where(bcg_age GE snaptimes[cursnap] AND bcg_age LT snaptimes[cursnap+1])
      if (bcg_gas_ind[0] GE 0) then bcg_gas_size = (size(bcg_gas_ind))[1] else bcg_gas_size = 0
      icl_gas_ind = where(icl_age GE snaptimes[cursnap] AND icl_age LT snaptimes[cursnap+1])
      if (icl_gas_ind[0] GE 0) then icl_gas_size = (size(icl_gas_ind))[1] else icl_gas_size = 0

      if (gal_gas_size GT 0) then begin
        gasind = 0L
        idsind = 0L
        where_part = lonarr(gal_gas_size)
        where_part[*] = -1
        ;; find gas particles (of correct age above) which match IDs with galaxy IDs
        while (gasind LT gal_gas_size AND idsind LT ids_size) do begin
          if (gal_ids[gal_gas_ind[gasind]] EQ IDs[idsind]) then begin
            where_part[gasind] = idsind
            ++gasind
            ++idsind
          endif else if (gal_ids[gal_gas_ind[gasind]] LT IDs[idsind]) then ++gasind else ++idsind
        endwhile
        ind = where(where_part GE 0)
        gas_subhalo = lonarr(gal_gas_size)
        gas_subhalo[*] = -2 ;; -2 if not in any halo
        if (ind[0] GE 0) then gas_subhalo[ind] = subhalo[where_part[ind]]

        gal_before_formation_snap[gal_gas_ind] = cursnap
        gal_before_formation_sub[gal_gas_ind] = gas_subhalo ;; host subhalo ID for each gas particle before star conversion (-2 is not host subhalo)
      endif 

      if (bcg_gas_size GT 0) then begin
        gasind = 0L
        idsind = 0L
        where_part = lonarr(bcg_gas_size)
        where_part[*] = -1
        while (gasind LT bcg_gas_size AND idsind LT ids_size) do begin
          if (bcg_ids[bcg_gas_ind[gasind]] EQ IDs[idsind]) then begin
            where_part[gasind] = idsind
            ++gasind
            ++idsind
          endif else if (bcg_ids[bcg_gas_ind[gasind]] LT IDs[idsind]) then ++gasind else ++idsind
        endwhile
        ind = where(where_part GE 0)
        gas_subhalo = lonarr(bcg_gas_size)
        gas_subhalo[*] = -2
        if (ind[0] GE 0) then gas_subhalo[ind] = subhalo[where_part[ind]]

        bcg_before_formation_snap[bcg_gas_ind] = cursnap
        bcg_before_formation_sub[bcg_gas_ind] = gas_subhalo
      endif

      if (icl_gas_size GT 0) then begin
        gasind = 0L
        idsind = 0L
        where_part = lonarr(icl_gas_size)
        where_part[*] = -1
        while (gasind LT icl_gas_size AND idsind LT ids_size) do begin
          if (icl_ids[icl_gas_ind[gasind]] EQ IDs[idsind]) then begin
            where_part[gasind] = idsind
            ++gasind
            ++idsind
          endif else if (icl_ids[icl_gas_ind[gasind]] LT IDs[idsind]) then ++gasind else ++idsind
        endwhile
        ind = where(where_part GE 0)
        gas_subhalo = lonarr(icl_gas_size)
        gas_subhalo[*] = -2
        if (ind[0] GE 0) then gas_subhalo[ind] = subhalo[where_part[ind]]

        icl_before_formation_snap[icl_gas_ind] = cursnap
        icl_before_formation_sub[icl_gas_ind] = gas_subhalo
      endif

      ind = 0L
      where_part = 0L
      gas_subhalo = 0L
      gal_gas_ind = 0L
      bcg_gas_ind = 0L
      icl_gas_ind = 0L
    endif

    readnew,Base+"/snap_"+exts,head,"HEAD",parttype=4
    snaptimes[cursnap] = head.time

    if (use_all_snaps EQ 1) then begin
      readnew,Base+"/snap_"+exts,part_ids,"ID  ",parttype=4
      ind = sort(part_ids)
      part_ids = part_ids[ind]
      part_ids_size = (size(part_ids))[1]
    endif else begin
      part_ids = all_part_ids[where(part_age LT snaptimes[cursnap])]
      part_ids_size = (size(part_ids))[1]
    endelse 

    idsind = 0L
    partind = 0L
    include_id = intarr(ids_size)
    where_part = lonarr(part_ids_size)
    where_part[*] = -1
    while (idsind LT ids_size AND partind LT part_ids_size) do begin
      if (IDs[idsind] EQ part_ids[partind]) then begin
        include_id(idsind) = 1
        where_part[partind] = idsind
        ++idsind
        ++partind
      endif else if (IDs[idsind] LT part_ids[partind]) then ++idsind else ++partind
    endwhile

    ind = where(include_id EQ 1)
    IDs = IDs[ind]
    subhalo = subhalo[ind]
    ids_size = n_elements(IDs) 
    include_id = 0L
    part_ids = 0L

    if (cursnap GT minsnap) then begin
    ;find descendants in this snap
   
      part_from_to = lonarr(old_TotNsubgroups,TotNsubgroups)      
      old_idsind = 0L
      idsind = 0L
      while (old_idsind LT old_ids_size AND idsind LT ids_size) do begin
        if (old_IDs[old_idsind] EQ IDs[idsind]) then begin
          if (old_subhalo[old_idsind] GE 0 AND subhalo[idsind] GE 0) then part_from_to[old_subhalo[old_idsind],subhalo[idsind]] += 1
          ++old_idsind
          ++idsind
        endif else if (old_IDs[old_idsind] LT IDs[idsind]) then ++old_idsind else ++idsind
      endwhile

      ptr_free, subhalo_descendants[cursnap-1]
      subhalo_descendants[cursnap-1] = ptr_new(lonarr(old_TotNsubgroups))

      old_starnum = lonarr(old_TotNsubgroups) 
      for old_idsind=0L,old_ids_size-1 do begin
        if (old_subhalo[old_idsind] GE 0) then old_starnum[old_subhalo[old_idsind]] += 1 
      endfor

      for i=0L,old_TotNsubgroups-1 do begin
        maxpart = max(part_from_to[i,*],descendant)
        ind = where(indgen(TotNsubgroups) NE BCG_progenitor[cursnap])
        if (descendant EQ BCG_progenitor[cursnap] AND maxpart GT 0.1*old_starnum[i]) then if (max(part_from_to[i,ind],temp_descendant) GT 0.1*old_starnum[i]) then descendant = ind[temp_descendant]
        if (maxpart LE 0.1*old_starnum[i]) then descendant = -1
        (*subhalo_descendants[cursnap-1])[i] = descendant 
      endfor
      have_descendants[cursnap-1] = 1
 
      part_from_to = 0L
      old_starnum = 0L

    endif

    old_TotNsubgroups = TotNsubgroups
    old_IDs = IDs
    old_subhalo = subhalo
    old_ids_size = ids_size

    ; find formation and fall-in galaxies
    galind = 0L
    idsind = 0L
    where_part = lonarr(gal_ids_size)
    where_part[*] = -1
    while (galind LT gal_ids_size AND idsind LT ids_size) do begin
      if (gal_ids[galind] EQ IDs[idsind]) then begin
        where_part[galind] = idsind
        ++galind
        ++idsind
      endif else if (gal_ids[galind] LT IDs[idsind]) then ++galind else ++idsind
    endwhile
    ind = where(where_part GE 0)
    gal_subhalo = lonarr(gal_ids_size)
    gal_subhalo[*] = -2
    if (ind[0] GE 0) then gal_subhalo[ind] = subhalo[where_part[ind]]

    ind = where(gal_formation_halo_snap EQ -1)
    if (ind[0] GE 0) then begin
      ind2 = where(gal_subhalo[ind] GE -1)
      if (ind2[0] GE 0) then begin
        gal_formation_halo_snap[ind[ind2]] = cursnap
        gal_formation_halo_sub[ind[ind2]] = gal_subhalo[ind[ind2]]
      endif
    endif

    subfprob = 0
    if (BCG_progenitor[cursnap] GE 0) then begin
      if (GroupFirstSub[BCG_progenitor_grnr[cursnap]] NE BCG_progenitor[cursnap]) then subfprob = 1
    endif

    if (BCG_progenitor[cursnap] GE 0) then begin
      ind = where(gal_subhalo EQ BCG_progenitor[cursnap])
      if (ind[0] GE 0) then gal_has_been_in_BCG[ind] = 1
    endif

    ind = where(gal_subhalo GE 0 AND gal_has_been_in_BCG EQ 0)
    if (ind[0] GE 0 AND (subfprob EQ 1 OR allow_fall_in_halo_in_clus EQ 0)) then begin
      ind3 = where((*subhalo_grnr[cursnap])[gal_subhalo[ind]] NE BCG_progenitor_grnr[cursnap])
      if (ind3[0] GE 0) then ind = ind[ind3] else ind[0] = -1 
      ind3 = 0L
    endif
    if (ind[0] GE 0) then begin
      ind2 = where(gal_fall_in_halo_mass[ind] LE SubhaloMass[gal_subhalo[ind]])
      if (ind2[0] GE 0) then begin
        gal_fall_in_halo_mass[ind[ind2]] = SubhaloMass[gal_subhalo[ind[ind2]]]
        gal_fall_in_halo_snap[ind[ind2]] = cursnap
        gal_fall_in_halo_sub[ind[ind2]] = gal_subhalo[ind[ind2]]
      endif
    endif

    bcgind = 0L
    idsind = 0L
    where_part = lonarr(bcg_ids_size)
    where_part[*] = -1
    while (bcgind LT bcg_ids_size AND idsind LT ids_size) do begin
      if (bcg_ids[bcgind] EQ IDs[idsind]) then begin
        where_part[bcgind] = idsind
        ++bcgind
        ++idsind
      endif else if (bcg_ids[bcgind] LT IDs[idsind]) then ++bcgind else ++idsind
    endwhile
    ind = where(where_part GE 0)
    bcg_subhalo = lonarr(bcg_ids_size)
    bcg_subhalo[*] = -2
    if (ind[0] GE 0) then bcg_subhalo[ind] = subhalo[where_part[ind]]

    ind = where(bcg_formation_halo_snap EQ -1)
    if (ind[0] GE 0) then begin
      ind2 = where(bcg_subhalo[ind] GE -1)
      if (ind2[0] GE 0) then begin
        bcg_formation_halo_snap[ind[ind2]] = cursnap
        bcg_formation_halo_sub[ind[ind2]] = bcg_subhalo[ind[ind2]]
      endif
    endif

    if (BCG_progenitor[cursnap] GE 0) then begin
      ind = where(bcg_subhalo EQ BCG_progenitor[cursnap])
      if (ind[0] GE 0) then bcg_has_been_in_BCG[ind] = 1
    endif

    ind = where(bcg_subhalo GE 0 AND bcg_has_been_in_BCG EQ 0)
    if (ind[0] GE 0 AND (subfprob EQ 1 OR allow_fall_in_halo_in_clus EQ 0)) then begin
      ind3 = where((*subhalo_grnr[cursnap])[bcg_subhalo[ind]] NE BCG_progenitor_grnr[cursnap])
      if (ind3[0] GE 0) then ind = ind[ind3] else ind[0] = -1 
      ind3 = 0L
    endif
    if (ind[0] GE 0) then begin
      ind2 = where(bcg_fall_in_halo_mass[ind] LE SubhaloMass[bcg_subhalo[ind]])
      if (ind2[0] GE 0) then begin
        bcg_fall_in_halo_mass[ind[ind2]] = SubhaloMass[bcg_subhalo[ind[ind2]]]
        bcg_fall_in_halo_snap[ind[ind2]] = cursnap
        bcg_fall_in_halo_sub[ind[ind2]] = bcg_subhalo[ind[ind2]]
      endif
    endif

    ind = where(bcg_subhalo GE 0 AND bcg_has_been_in_BCG EQ 0)
    if (ind[0] GE 0 AND subfprob EQ 1) then begin
      ind3 = where((*subhalo_grnr[cursnap])[bcg_subhalo[ind]] NE BCG_progenitor_grnr[cursnap])
      if (ind3[0] GE 0) then ind = ind[ind3] else ind[0] = -1 
      ind3 = 0L
    endif
    if (ind[0] GE 0) then begin
      bcg_strip_halo_snap[ind] = cursnap
      bcg_strip_halo_sub[ind] = bcg_subhalo[ind]
    endif

    iclind = 0L
    idsind = 0L
    where_part = lonarr(icl_ids_size)
    where_part[*] = -1
    while (iclind LT icl_ids_size AND idsind LT ids_size) do begin
      if (icl_ids[iclind] EQ IDs[idsind]) then begin
        where_part[iclind] = idsind
        ++iclind
        ++idsind
      endif else if (icl_ids[iclind] LT IDs[idsind]) then ++iclind else ++idsind
    endwhile
    ind = where(where_part GE 0)
    icl_subhalo = lonarr(icl_ids_size)
    icl_subhalo[*] = -2
    if (ind[0] GE 0) then icl_subhalo[ind] = subhalo[where_part[ind]]

    ind = where(icl_formation_halo_snap EQ -1)
    if (ind[0] GE 0) then begin
      ind2 = where(icl_subhalo[ind] GE -1)
      if (ind2[0] GE 0) then begin
        icl_formation_halo_snap[ind[ind2]] = cursnap
        icl_formation_halo_sub[ind[ind2]] = icl_subhalo[ind[ind2]]
      endif
    endif

    if (BCG_progenitor[cursnap] GE 0) then begin
      ind = where(icl_subhalo EQ BCG_progenitor[cursnap])
      if (ind[0] GE 0) then icl_has_been_in_BCG[ind] = 1
    endif

    ind = where(icl_subhalo GE 0 AND icl_has_been_in_BCG EQ 0)
    if (ind[0] GE 0 AND (subfprob EQ 1 OR  allow_fall_in_halo_in_clus EQ 0)) then begin
      ind3 = where((*subhalo_grnr[cursnap])[icl_subhalo[ind]] NE BCG_progenitor_grnr[cursnap])
      if (ind3[0] GE 0) then ind = ind[ind3] else ind[0] = -1 
      ind3 = 0L
    endif
    if (ind[0] GE 0) then begin
      ind2 = where(icl_fall_in_halo_mass[ind] LE SubhaloMass[icl_subhalo[ind]])
      if (ind2[0] GE 0) then begin
        icl_fall_in_halo_mass[ind[ind2]] = SubhaloMass[icl_subhalo[ind[ind2]]]
        icl_fall_in_halo_snap[ind[ind2]] = cursnap
        icl_fall_in_halo_sub[ind[ind2]] = icl_subhalo[ind[ind2]]
      endif
    endif

    ind = where(icl_subhalo GE 0 AND icl_has_been_in_BCG EQ 0)
    if (ind[0] GE 0 AND subfprob EQ 1) then begin
      ind3 = where((*subhalo_grnr[cursnap])[icl_subhalo[ind]] NE BCG_progenitor_grnr[cursnap])
      if (ind3[0] GE 0) then ind = ind[ind3] else ind[0] = -1 
      ind3 = 0L
    endif
    if (ind[0] GE 0) then begin
      icl_strip_halo_snap[ind] = cursnap
      icl_strip_halo_sub[ind] = icl_subhalo[ind]
    endif
endfor
print

;----- find subhalo orbits ------------------------

print, "interpolating subhalo orbits"
subhalo_orbit = ptrarr(numsnaps)
for cursnap=minsnap,numsnaps-1 do begin
  if (have_descendants[cursnap] EQ 1) then begin
    ptr_free, subhalo_orbit[cursnap]
    subhalo_orbit[cursnap] = ptr_new(dblarr(num_subhalos[cursnap],3,finenum))

    time1 = 0.0D
    time2 = time(snaptimes[cursnap],snaptimes[cursnap+1])
    scale1=snaptimes[cursnap]
    scale2=snaptimes[cursnap+1]

    for j=0,finenum-1 do begin
      cur_time = time(scale1,scale1+double(j)/double(finenum)*(scale2-scale1))
      cur_scale = scale1+double(j)/double(finenum)*(scale2-scale1)
      if (snaptimes_fine[cursnap*finenum+j] EQ 0) then snaptimes_fine[cursnap*finenum+j] = cur_scale

      for i=0L,num_subhalos[cursnap]-1 do begin
        if ((*subhalo_descendants[cursnap])[i] GE 0) then begin  
          (*subhalo_orbit[cursnap])[i,0,j] = interpolate_orbit( (*subhalo_pos[cursnap])[0,i], (*subhalo_vel[cursnap])[0,i]/scale1, time1, (*subhalo_pos[cursnap+1])[0,(*subhalo_descendants[cursnap])[i]], (*subhalo_vel[cursnap+1])[0,(*subhalo_descendants[cursnap])[i]]/scale2, time2, cur_time)
          (*subhalo_orbit[cursnap])[i,1,j] = interpolate_orbit( (*subhalo_pos[cursnap])[1,i], (*subhalo_vel[cursnap])[1,i]/scale1, time1, (*subhalo_pos[cursnap+1])[1,(*subhalo_descendants[cursnap])[i]], (*subhalo_vel[cursnap+1])[1,(*subhalo_descendants[cursnap])[i]]/scale2, time2, cur_time)
          (*subhalo_orbit[cursnap])[i,2,j] = interpolate_orbit( (*subhalo_pos[cursnap])[2,i], (*subhalo_vel[cursnap])[2,i]/scale1, time1, (*subhalo_pos[cursnap+1])[2,(*subhalo_descendants[cursnap])[i]], (*subhalo_vel[cursnap+1])[2,(*subhalo_descendants[cursnap])[i]]/scale2, time2, cur_time)
        endif
      endfor
    endfor
  endif
endfor 

;----- check if galaxies dissolve -----------------

print, "check if galaxies dissolve"

have_dissolved = intarr(numsnaps)
subhalo_dissolved = ptrarr(numsnaps)
subhalo_dissolves_now = ptrarr(numsnaps)

cursnap=Num
while (cursnap GE minsnap) do begin
  ptr_free, subhalo_dissolved[cursnap]
  subhalo_dissolved[cursnap] = ptr_new(intarr(num_subhalos[cursnap]))
  ptr_free, subhalo_dissolves_now[cursnap]
  subhalo_dissolves_now[cursnap] = ptr_new(intarr(num_subhalos[cursnap]))

  (*subhalo_dissolves_now[cursnap])[*] = 0
  if (cursnap EQ Num) then begin
    (*subhalo_dissolved[cursnap])[*] = 0
    (*subhalo_dissolved[cursnap])[BCG_progenitor[cursnap]] = 1
    (*subhalo_dissolves_now[cursnap])[BCG_progenitor[cursnap]] = 1
  endif else begin
    for i=0L,num_subhalos[cursnap]-1 do begin
      cursubdes =(*subhalo_descendants[cursnap])[i] 
      if (cursubdes EQ -1) then begin
        (*subhalo_dissolved[cursnap])[i] = 1
        (*subhalo_dissolves_now[cursnap])[i] = 1
      endif else begin 
        if ((*subhalo_dissolved[cursnap+1])[cursubdes] EQ 1) then (*subhalo_dissolved[cursnap])[i] = 1
        if (cursubdes EQ BCG_progenitor[cursnap+1]) then (*subhalo_dissolves_now[cursnap])[i] = 1 
      endelse
    endfor
  endelse

  if (BCG_progenitor[cursnap] GE 0) then if ((*subhalo_dissolved[cursnap])[BCG_progenitor[cursnap]] NE 1) then print, "main BCG progenitor in snapshot ", cursnap, " does not dissolve"

  have_dissolved[cursnap] = 1

  --cursnap
endwhile

;----- find closest approach ----------------------

print, "find closest approach for subhalos"

subhalo_mindist = ptrarr(numsnaps)
subhalo_mindistnext = ptrarr(numsnaps)

cursnap=Num
while (cursnap GE minsnap) do begin
  ptr_free, subhalo_mindist[cursnap]
  subhalo_mindist[cursnap] = ptr_new(dblarr(num_subhalos[cursnap]))
  ptr_free, subhalo_mindistnext[cursnap]
  subhalo_mindistnext[cursnap] = ptr_new(dblarr(num_subhalos[cursnap]))
  (*subhalo_mindist[cursnap])[*] = -1
  (*subhalo_mindistnext[cursnap])[*] = -1
 
  for i=0L,num_subhalos[cursnap]-1 do begin
    if (BCG_progenitor[cursnap] GE 0) then begin
      (*subhalo_mindist[cursnap])[i] = sqrt(total(((*subhalo_pos[cursnap])[*,i] - (*subhalo_pos[cursnap])[*,BCG_progenitor[cursnap]])^2,/double))*snaptimes[cursnap]

      if (cursnap LT Num) then begin
        cursubdes =(*subhalo_descendants[cursnap])[i] 
        if (cursubdes GE 0 AND cursubdes NE BCG_progenitor[cursnap+1]) then begin
          orbitdist = dblarr(finenum)
          for finebin=0,finenum-1 do orbitdist[finebin] = sqrt(total(((*subhalo_orbit[cursnap])[i,*,finebin] - BCG_progenitor_pos_fine[*,cursnap*finenum+finebin])^2,/double))*snaptimes_fine[cursnap*finenum+finebin]
          (*subhalo_mindistnext[cursnap])[i] = min(orbitdist)
 
          (*subhalo_mindist[cursnap])[i] = min([(*subhalo_mindist[cursnap])[i],(*subhalo_mindist[cursnap+1])[cursubdes],(*subhalo_mindistnext[cursnap])[i]])
        endif
      endif
    endif
  endfor

  --cursnap
endwhile

;--------------------------------------------------

all_part_ids = 0L
all_part_ids_size = 0

outputnamebase = strmid(Base,0,strlen(Base)-1)
outputnamebase = strmid(outputnamebase,strpos(outputnamebase,"/",/reverse_search)+1) 
outputnamebase = "output/"+outputnamebase

;----- formation halo stuff -----------------------

gal_halo_mass = fltarr(6,gal_ids_size)
gal_halo_mass[*] = -1
bcg_halo_mass = fltarr(6,bcg_ids_size)
bcg_halo_mass[*] = -1
icl_halo_mass = fltarr(6,icl_ids_size)
icl_halo_mass[*] = -1

gal_halfmassrad = fltarr(gal_ids_size)
gal_halfmassrad[*] = -1
bcg_halfmassrad = fltarr(bcg_ids_size)
bcg_halfmassrad[*] = -1
icl_halfmassrad = fltarr(icl_ids_size)
icl_halfmassrad[*] = -1

gal_halflightrad = fltarr(gal_ids_size)
gal_halflightrad[*] = -1
bcg_halflightrad = fltarr(bcg_ids_size)
bcg_halflightrad[*] = -1
icl_halflightrad = fltarr(icl_ids_size)
icl_halflightrad[*] = -1

for galind=0L,gal_ids_size-1 do begin
  cursnap = gal_formation_halo_snap[galind]
  cursub = gal_formation_halo_sub[galind]
  if (cursnap GE 0 AND cursub GE 0) then begin
    gal_halo_mass[*,galind] = (*subhalo_mass_tab[cursnap])[*,cursub]
    gal_halfmassrad[galind] = (*subhalo_halfmassrad[cursnap])[cursub]
    gal_halflightrad[galind] = (*subhalo_halflightrad[cursnap])[cursub] 
  endif
endfor

for bcgind=0L,bcg_ids_size-1 do begin
  cursnap = bcg_formation_halo_snap[bcgind]
  cursub = bcg_formation_halo_sub[bcgind]
  if (cursnap GE 0 AND cursub GE 0) then begin
    bcg_halo_mass[*,bcgind] = (*subhalo_mass_tab[cursnap])[*,cursub]
    bcg_halfmassrad[bcgind] = (*subhalo_halfmassrad[cursnap])[cursub]
    bcg_halflightrad[bcgind] = (*subhalo_halflightrad[cursnap])[cursub]
  endif
endfor

for iclind=0L,icl_ids_size-1 do begin
  cursnap = icl_formation_halo_snap[iclind]
  cursub = icl_formation_halo_sub[iclind]
  if (cursnap GE 0 AND cursub GE 0) then begin
    icl_halo_mass[*,iclind] = (*subhalo_mass_tab[cursnap])[*,cursub]
    icl_halfmassrad[iclind] = (*subhalo_halfmassrad[cursnap])[cursub]
    icl_halflightrad[iclind] = (*subhalo_halflightrad[cursnap])[cursub]
  endif
endfor

gal_ind = where(gal_formation_halo_snap GE 0 AND gal_formation_halo_sub GE 0)
bcg_ind = where(bcg_formation_halo_snap GE 0 AND bcg_formation_halo_sub GE 0)
icl_ind = where(icl_formation_halo_snap GE 0 AND icl_formation_halo_sub GE 0)

print
print, "formation halo found for ", (size(gal_ind))[1], " of ",gal_ids_size," galaxy stars, not found for ", gal_ids_size -(size(gal_ind))[1]
print, "formation halo found for ", (size(bcg_ind))[1], " of ",bcg_ids_size," BCG stars, not found for ", bcg_ids_size -(size(bcg_ind))[1]
print, "formation halo found for ", (size(icl_ind))[1], " of ",icl_ids_size," ICL stars, not found for ", icl_ids_size -(size(icl_ind))[1] 
print
ind = where(bcg_formation_halo_snap GE 0 AND bcg_formation_halo_sub GE 0)
ind2 = where(bcg_formation_halo_sub[ind] EQ BCG_progenitor[bcg_formation_halo_snap[ind]])
print,(size(ind2))[1], " BCG stars were formed in BCG"
ind = where(icl_formation_halo_snap GE 0 AND icl_formation_halo_sub GE 0)
ind2 = where(icl_formation_halo_sub[ind] EQ BCG_progenitor[icl_formation_halo_snap[ind]]) 
print,(size(ind2))[1], " ICL stars were formed in BCG"
print

profilebinsize = 0.2
gal_mass_dist = histogram(alog10(total(gal_halo_mass[*,gal_ind],1)*1.0e10),binsize=profilebinsize,min=0.0,max=16.0,LOCATIONS=dist_masses)
bcg_mass_dist = histogram(alog10(total(bcg_halo_mass[*,bcg_ind],1)*1.0e10),binsize=profilebinsize,min=0.0,max=16.0)
icl_mass_dist = histogram(alog10(total(icl_halo_mass[*,icl_ind],1)*1.0e10),binsize=profilebinsize,min=0.0,max=16.0)
num_dist_bins = (size(gal_mass_dist))[1]
dist_masses = dist_masses+0.5*profilebinsize ; logarithimic masses in M_sun/h
outputname = outputnamebase+"_formation_masses.txt"
openw,11,outputname,width=256
for i=0,num_dist_bins-1 do printf, 11, dist_masses[i], gal_mass_dist[i], bcg_mass_dist[i], icl_mass_dist[i] 
close,11

profilebinsize = 0.2
gal_mass_dist = histogram(alog10(gal_halo_mass[4,gal_ind]*1.0e10),binsize=profilebinsize,min=0.0,max=16.0,LOCATIONS=dist_masses)
bcg_mass_dist = histogram(alog10(bcg_halo_mass[4,bcg_ind]*1.0e10),binsize=profilebinsize,min=0.0,max=16.0)
icl_mass_dist = histogram(alog10(icl_halo_mass[4,icl_ind]*1.0e10),binsize=profilebinsize,min=0.0,max=16.0)
num_dist_bins = (size(gal_mass_dist))[1]
dist_masses = dist_masses+0.5*profilebinsize ; logarithimic stellar masses in M_sun/h
outputname = outputnamebase+"_formation_stellar_masses.txt"
openw,11,outputname,width=256
for i=0,num_dist_bins-1 do printf, 11, dist_masses[i], gal_mass_dist[i], bcg_mass_dist[i], icl_mass_dist[i] 
close,11

profilebinsize = 0.25
gal_res_dist = histogram(gal_halfmassrad[gal_ind]/snapsoftphys[gal_formation_halo_snap[gal_ind]],binsize=profilebinsize,min=0.0,max=500.0,LOCATIONS=dist_res)
bcg_res_dist = histogram(bcg_halfmassrad[bcg_ind]/snapsoftphys[bcg_formation_halo_snap[bcg_ind]],binsize=profilebinsize,min=0.0,max=500.0)
icl_res_dist = histogram(icl_halfmassrad[icl_ind]/snapsoftphys[icl_formation_halo_snap[icl_ind]],binsize=profilebinsize,min=0.0,max=500.0)
num_dist_bins = (size(gal_res_dist))[1]
dist_res = dist_res+0.5*profilebinsize ; half mass radii in units of softening
outputname = outputnamebase+"_formation_halfmass_res.txt"
openw,11,outputname,width=256
for i=0,num_dist_bins-1 do printf, 11, dist_res[i], gal_res_dist[i], bcg_res_dist[i], icl_res_dist[i] 
close,11

profilebinsize = 0.25
gal_res_dist = histogram(gal_halflightrad[gal_ind]/snapsoftphys[gal_formation_halo_snap[gal_ind]],binsize=profilebinsize,min=0.0,max=500.0,LOCATIONS=dist_res)
bcg_res_dist = histogram(bcg_halflightrad[bcg_ind]/snapsoftphys[bcg_formation_halo_snap[bcg_ind]],binsize=profilebinsize,min=0.0,max=500.0)
icl_res_dist = histogram(icl_halflightrad[icl_ind]/snapsoftphys[icl_formation_halo_snap[icl_ind]],binsize=profilebinsize,min=0.0,max=500.0)
num_dist_bins = (size(gal_res_dist))[1]
dist_res = dist_res+0.5*profilebinsize ; half light radii in units of softening
outputname = outputnamebase+"_formation_halflight_res.txt"
openw,11,outputname,width=256
for i=0,num_dist_bins-1 do printf, 11, dist_res[i], gal_res_dist[i], bcg_res_dist[i], icl_res_dist[i] 
close,11

gal_scale_dist = histogram(gal_formation_halo_snap[gal_ind],min=0,max=63,LOCATIONS=dist_scale)
bcg_scale_dist = histogram(bcg_formation_halo_snap[bcg_ind],min=0,max=63)
icl_scale_dist = histogram(icl_formation_halo_snap[icl_ind],min=0,max=63)
num_dist_bins = (size(gal_scale_dist))[1]
outputname = outputnamebase+"_formation_scalefac.txt"
openw,11,outputname,width=256
for i=0,num_dist_bins-1 do printf, 11, snaptimes[dist_scale[i]], gal_scale_dist[i], bcg_scale_dist[i], icl_scale_dist[i] 
close,11

profilebinsize = 0.01
gal_scale_dist = histogram(gal_age,binsize=profilebinsize,min=0.0,max=1.0,LOCATIONS=dist_scale)
bcg_scale_dist = histogram(bcg_age,binsize=profilebinsize,min=0.0,max=1.0)
icl_scale_dist = histogram(icl_age,binsize=profilebinsize,min=0.0,max=1.0)
num_dist_bins = (size(gal_scale_dist))[1]
dist_scale = dist_scale+0.5*profilebinsize
outputname = outputnamebase+"_formation_scalefac_accurate.txt"
openw,11,outputname,width=256
for i=0,num_dist_bins-1 do printf, 11, dist_scale[i], gal_scale_dist[i], bcg_scale_dist[i], icl_scale_dist[i] 
close,11

if (check_before EQ 1) then begin
  gal_halo_mass = fltarr(6,gal_ids_size)
  gal_halo_mass[*] = -1
  bcg_halo_mass = fltarr(6,bcg_ids_size)
  bcg_halo_mass[*] = -1
  icl_halo_mass = fltarr(6,icl_ids_size)
  icl_halo_mass[*] = -1

  for galind=0L,gal_ids_size-1 do begin
    cursnap = gal_before_formation_snap[galind]
    cursub = gal_before_formation_sub[galind]
    if (cursnap GE 0 AND cursub GE 0) then gal_halo_mass[*,galind] = (*subhalo_mass_tab[cursnap])[*,cursub]
  endfor

  for bcgind=0L,bcg_ids_size-1 do begin
    cursnap = bcg_before_formation_snap[bcgind]
    cursub = bcg_before_formation_sub[bcgind]
    if (cursnap GE 0 AND cursub GE 0) then bcg_halo_mass[*,bcgind] = (*subhalo_mass_tab[cursnap])[*,cursub]
  endfor

  for iclind=0L,icl_ids_size-1 do begin
    cursnap = icl_before_formation_snap[iclind]
    cursub = icl_before_formation_sub[iclind]
    if (cursnap GE 0 AND cursub GE 0) then icl_halo_mass[*,iclind] = (*subhalo_mass_tab[cursnap])[*,cursub]
  endfor

  gal_ind = where(gal_before_formation_snap GE 0 AND gal_before_formation_sub GE 0)
  bcg_ind = where(bcg_before_formation_snap GE 0 AND bcg_before_formation_sub GE 0)
  icl_ind = where(icl_before_formation_snap GE 0 AND icl_before_formation_sub GE 0)

  print
  print, "pre-formation halo found for ", (size(gal_ind))[1], " of ",gal_ids_size," galaxy stars, not found for ", gal_ids_size -(size(gal_ind))[1]
  print, "pre-formation halo found for ", (size(bcg_ind))[1], " of ",bcg_ids_size," BCG stars, not found for ", bcg_ids_size -(size(bcg_ind))[1]
  print, "pre-formation halo found for ", (size(icl_ind))[1], " of ",icl_ids_size," ICL stars, not found for ", icl_ids_size -(size(icl_ind))[1] 
  print

  profilebinsize = 0.2
  gal_mass_dist = histogram(alog10(total(gal_halo_mass[*,gal_ind],1)*1.0e10),binsize=profilebinsize,min=0.0,max=16.0,LOCATIONS=dist_masses)
  bcg_mass_dist = histogram(alog10(total(bcg_halo_mass[*,bcg_ind],1)*1.0e10),binsize=profilebinsize,min=0.0,max=16.0)
  icl_mass_dist = histogram(alog10(total(icl_halo_mass[*,icl_ind],1)*1.0e10),binsize=profilebinsize,min=0.0,max=16.0)
  num_dist_bins = (size(gal_mass_dist))[1]
  dist_masses = dist_masses+0.5*profilebinsize ; logarithimic masses in M_sun/h
  outputname = outputnamebase+"_before_formation_masses.txt"
  openw,11,outputname,width=256
  for i=0,num_dist_bins-1 do printf, 11, dist_masses[i], gal_mass_dist[i], bcg_mass_dist[i], icl_mass_dist[i] 
  close,11
endif

gal_mass_dist[*] = -1
bcg_mass_dist[*] = -1
icl_mass_dist[*] = -1
gal_res_dist[*] = -1
bcg_res_dist[*] = -1
icl_res_dist[*] = -1
gal_scale_dist[*] = -1
bcg_scale_dist[*] = -1
icl_scale_dist[*] = -1
gal_ind = 0L
bcg_ind = 0L
icl_ind = 0L

;----- fall-in halo stuff -------------------------

gal_halo_mass[*] = -1
bcg_halo_mass[*] = -1
icl_halo_mass[*] = -1

gal_halfmassrad[*] = -1
bcg_halfmassrad[*] = -1
icl_halfmassrad[*] = -1

gal_halflightrad[*] = -1
bcg_halflightrad[*] = -1
icl_halflightrad[*] = -1

for galind=0L,gal_ids_size-1 do begin
  cursnap = gal_fall_in_halo_snap[galind]
  cursub = gal_fall_in_halo_sub[galind]
  if (cursnap GE 0 AND cursub GE 0) then begin
    gal_halo_mass[*,galind] = (*subhalo_mass_tab[cursnap])[*,cursub]
    gal_halfmassrad[galind] = (*subhalo_halfmassrad[cursnap])[cursub]
    gal_halflightrad[galind] = (*subhalo_halflightrad[cursnap])[cursub] 
  endif
endfor

for bcgind=0L,bcg_ids_size-1 do begin
  cursnap = bcg_fall_in_halo_snap[bcgind]
  cursub = bcg_fall_in_halo_sub[bcgind]
  if (cursnap GE 0 AND cursub GE 0) then begin
    bcg_halo_mass[*,bcgind] = (*subhalo_mass_tab[cursnap])[*,cursub]
    bcg_halfmassrad[bcgind] = (*subhalo_halfmassrad[cursnap])[cursub]
    bcg_halflightrad[bcgind] = (*subhalo_halflightrad[cursnap])[cursub]
  endif
endfor

for iclind=0L,icl_ids_size-1 do begin
  cursnap = icl_fall_in_halo_snap[iclind]
  cursub = icl_fall_in_halo_sub[iclind]
  if (cursnap GE 0 AND cursub GE 0) then begin
    icl_halo_mass[*,iclind] = (*subhalo_mass_tab[cursnap])[*,cursub]
    icl_halfmassrad[iclind] = (*subhalo_halfmassrad[cursnap])[cursub]
    icl_halflightrad[iclind] = (*subhalo_halflightrad[cursnap])[cursub]
  endif
endfor

gal_ind = where(gal_fall_in_halo_snap GE 0 AND gal_fall_in_halo_sub GE 0)
bcg_ind = where(bcg_fall_in_halo_snap GE 0 AND bcg_fall_in_halo_sub GE 0)
icl_ind = where(icl_fall_in_halo_snap GE 0 AND icl_fall_in_halo_sub GE 0)

print
print, "fall-in halo found for ", (size(gal_ind))[1], " of ",gal_ids_size," galaxy stars, not found for ", gal_ids_size -(size(gal_ind))[1]
ind = where(gal_fall_in_halo_snap GE 0 AND gal_fall_in_halo_sub GE 0)
partcount = 0L
for i=0L,(size(ind))[1]-1 do if ((*subhalo_grnr[gal_fall_in_halo_snap[ind[i]]])[gal_fall_in_halo_sub[ind[i]]] EQ BCG_progenitor_grnr[gal_fall_in_halo_snap[ind[i]]]) then ++partcount
print, "fall-in halo of ", partcount, " galaxy stars is in main cluster"
partcount = 0L
for i=0L,(size(ind))[1]-1 do if ((*subhalo_dissolved[gal_fall_in_halo_snap[ind[i]]])[gal_fall_in_halo_sub[ind[i]]] EQ 1) then ++partcount
print, "fall-in halo of ", partcount, " galaxy stars dissolves"
print

print, "fall-in halo found for ", (size(bcg_ind))[1], " of ",bcg_ids_size," BCG stars, not found for ", bcg_ids_size -(size(bcg_ind))[1]
ind = where((~ (bcg_fall_in_halo_snap GE 0 AND bcg_fall_in_halo_sub GE 0)) AND (bcg_formation_halo_snap GE 0 AND bcg_formation_halo_sub GE 0))
ind2 = where(bcg_formation_halo_sub[ind] EQ BCG_progenitor[bcg_formation_halo_snap[ind]])
print,(size(ind2))[1], " of the latter were formed in BCG" 
ind = where(bcg_fall_in_halo_snap GE 0 AND bcg_fall_in_halo_sub GE 0)
partcount = 0L
for i=0L,(size(ind))[1]-1 do if ((*subhalo_grnr[bcg_fall_in_halo_snap[ind[i]]])[bcg_fall_in_halo_sub[ind[i]]] EQ BCG_progenitor_grnr[bcg_fall_in_halo_snap[ind[i]]]) then ++partcount
print, "fall-in halo of ", partcount, " BCG stars is in main cluster" 
partcount = 0L
for i=0L,(size(ind))[1]-1 do if ((*subhalo_dissolved[bcg_fall_in_halo_snap[ind[i]]])[bcg_fall_in_halo_sub[ind[i]]] EQ 1) then ++partcount
print, "fall-in halo of ", partcount, " BCG stars dissolves"
print

print, "fall-in halo found for ", (size(icl_ind))[1], " of ",icl_ids_size," ICL stars, not found for ", icl_ids_size -(size(icl_ind))[1] 
ind = where((~ (icl_fall_in_halo_snap GE 0 AND icl_fall_in_halo_sub GE 0)) AND (icl_formation_halo_snap GE 0 AND icl_formation_halo_sub GE 0))
ind2 = where(icl_formation_halo_sub[ind] EQ BCG_progenitor[icl_formation_halo_snap[ind]])
print,(size(ind2))[1], " of the latter were formed in BCG"
ind = where(icl_fall_in_halo_snap GE 0 AND icl_fall_in_halo_sub GE 0)
partcount = 0L
for i=0L,(size(ind))[1]-1 do if ((*subhalo_grnr[icl_fall_in_halo_snap[ind[i]]])[icl_fall_in_halo_sub[ind[i]]] EQ BCG_progenitor_grnr[icl_fall_in_halo_snap[ind[i]]]) then ++partcount
print, "fall-in halo of ", partcount, " ICL stars is in main cluster"
partcount = 0L
for i=0L,(size(ind))[1]-1 do if ((*subhalo_dissolved[icl_fall_in_halo_snap[ind[i]]])[icl_fall_in_halo_sub[ind[i]]] EQ 1) then ++partcount
print, "fall-in halo of ", partcount, " ICL stars dissolves"  
print

profilebinsize = 0.2
gal_mass_dist = histogram(alog10(total(gal_halo_mass[*,gal_ind],1)*1.0e10),binsize=profilebinsize,min=0.0,max=16.0,LOCATIONS=dist_masses)
bcg_mass_dist = histogram(alog10(total(bcg_halo_mass[*,bcg_ind],1)*1.0e10),binsize=profilebinsize,min=0.0,max=16.0)
icl_mass_dist = histogram(alog10(total(icl_halo_mass[*,icl_ind],1)*1.0e10),binsize=profilebinsize,min=0.0,max=16.0)
num_dist_bins = (size(gal_mass_dist))[1]
dist_masses = dist_masses+0.5*profilebinsize ; logarithimic masses in M_sun/h
outputname = outputnamebase+"_fall_in_masses.txt"
openw,11,outputname,width=256
for i=0,num_dist_bins-1 do printf, 11, dist_masses[i], gal_mass_dist[i], bcg_mass_dist[i], icl_mass_dist[i] 
close,11

profilebinsize = 0.2
gal_mass_dist = histogram(alog10(gal_halo_mass[4,gal_ind]*1.0e10),binsize=profilebinsize,min=0.0,max=16.0,LOCATIONS=dist_masses)
bcg_mass_dist = histogram(alog10(bcg_halo_mass[4,bcg_ind]*1.0e10),binsize=profilebinsize,min=0.0,max=16.0)
icl_mass_dist = histogram(alog10(icl_halo_mass[4,icl_ind]*1.0e10),binsize=profilebinsize,min=0.0,max=16.0)
num_dist_bins = (size(gal_mass_dist))[1]
dist_masses = dist_masses+0.5*profilebinsize ; logarithimic stellar masses in M_sun/h
outputname = outputnamebase+"_fall_in_stellar_masses.txt"
openw,11,outputname,width=256
for i=0,num_dist_bins-1 do printf, 11, dist_masses[i], gal_mass_dist[i], bcg_mass_dist[i], icl_mass_dist[i] 
close,11

profilebinsize = 0.25
gal_res_dist = histogram(gal_halfmassrad[gal_ind]/snapsoftphys[gal_fall_in_halo_snap[gal_ind]],binsize=profilebinsize,min=0.0,max=500.0,LOCATIONS=dist_res)
bcg_res_dist = histogram(bcg_halfmassrad[bcg_ind]/snapsoftphys[bcg_fall_in_halo_snap[bcg_ind]],binsize=profilebinsize,min=0.0,max=500.0)
icl_res_dist = histogram(icl_halfmassrad[icl_ind]/snapsoftphys[icl_fall_in_halo_snap[icl_ind]],binsize=profilebinsize,min=0.0,max=500.0)
num_dist_bins = (size(gal_res_dist))[1]
dist_res = dist_res+0.5*profilebinsize ; half mass radii in units of softening
outputname = outputnamebase+"_fall_in_halfmass_res.txt"
openw,11,outputname,width=256
for i=0,num_dist_bins-1 do printf, 11, dist_res[i], gal_res_dist[i], bcg_res_dist[i], icl_res_dist[i] 
close,11

profilebinsize = 0.25
gal_res_dist = histogram(gal_halflightrad[gal_ind]/snapsoftphys[gal_fall_in_halo_snap[gal_ind]],binsize=profilebinsize,min=0.0,max=500.0,LOCATIONS=dist_res)
bcg_res_dist = histogram(bcg_halflightrad[bcg_ind]/snapsoftphys[bcg_fall_in_halo_snap[bcg_ind]],binsize=profilebinsize,min=0.0,max=500.0)
icl_res_dist = histogram(icl_halflightrad[icl_ind]/snapsoftphys[icl_fall_in_halo_snap[icl_ind]],binsize=profilebinsize,min=0.0,max=500.0)
num_dist_bins = (size(gal_res_dist))[1]
dist_res = dist_res+0.5*profilebinsize ; half light radii in units of softening
outputname = outputnamebase+"_fall_in_halflight_res.txt"
openw,11,outputname,width=256
for i=0,num_dist_bins-1 do printf, 11, dist_res[i], gal_res_dist[i], bcg_res_dist[i], icl_res_dist[i] 
close,11

gal_scale_dist = histogram(gal_fall_in_halo_snap[gal_ind],min=0,max=63,LOCATIONS=dist_scale)
bcg_scale_dist = histogram(bcg_fall_in_halo_snap[bcg_ind],min=0,max=63)
icl_scale_dist = histogram(icl_fall_in_halo_snap[icl_ind],min=0,max=63)
num_dist_bins = (size(gal_scale_dist))[1]
outputname = outputnamebase+"_fall_in_scalefac.txt"
openw,11,outputname,width=256
for i=0,num_dist_bins-1 do printf, 11, snaptimes[dist_scale[i]], gal_scale_dist[i], bcg_scale_dist[i], icl_scale_dist[i] 
close,11

gal_mass_dist[*] = -1
bcg_mass_dist[*] = -1
icl_mass_dist[*] = -1
gal_res_dist[*] = -1
bcg_res_dist[*] = -1
icl_res_dist[*] = -1
gal_scale_dist[*] = -1
bcg_scale_dist[*] = -1
icl_scale_dist[*] = -1
gal_ind = 0L
bcg_ind = 0L
icl_ind = 0L

;----- strip halo stuff ---------------------------

gal_halo_mass[*] = -1
bcg_halo_mass[*] = -1
icl_halo_mass[*] = -1

gal_halfmassrad[*] = -1
bcg_halfmassrad[*] = -1
icl_halfmassrad[*] = -1

gal_halflightrad[*] = -1
bcg_halflightrad[*] = -1
icl_halflightrad[*] = -1

bcg_mindistnext = fltarr(bcg_ids_size)
bcg_mindistnext[*] = -1
icl_mindistnext = fltarr(icl_ids_size)
icl_mindistnext[*] = -1

bcg_dissolves_now = intarr(bcg_ids_size)
bcg_dissolves_now[*] = -1
icl_dissolves_now = fltarr(icl_ids_size)
icl_dissolves_now[*] = -1

for bcgind=0L,bcg_ids_size-1 do begin
  cursnap = bcg_strip_halo_snap[bcgind]
  cursub = bcg_strip_halo_sub[bcgind]
  if (cursnap GE 0 AND cursub GE 0) then begin
    bcg_halo_mass[*,bcgind] = (*subhalo_mass_tab[cursnap])[*,cursub]
    bcg_halfmassrad[bcgind] = (*subhalo_halfmassrad[cursnap])[cursub]
    bcg_halflightrad[bcgind] = (*subhalo_halflightrad[cursnap])[cursub]
    bcg_mindistnext[bcgind] = (*subhalo_mindistnext[cursnap])[cursub]
    bcg_dissolves_now[bcgind] = (*subhalo_dissolves_now[cursnap])[cursub]
  endif
endfor

for iclind=0L,icl_ids_size-1 do begin
  cursnap = icl_strip_halo_snap[iclind]
  cursub = icl_strip_halo_sub[iclind]
  if (cursnap GE 0 AND cursub GE 0) then begin
    icl_halo_mass[*,iclind] = (*subhalo_mass_tab[cursnap])[*,cursub]
    icl_halfmassrad[iclind] = (*subhalo_halfmassrad[cursnap])[cursub]
    icl_halflightrad[iclind] = (*subhalo_halflightrad[cursnap])[cursub]
    icl_mindistnext[iclind] = (*subhalo_mindistnext[cursnap])[cursub]
    icl_dissolves_now[iclind] = (*subhalo_dissolves_now[cursnap])[cursub]
  endif
endfor

bcg_ind = where(bcg_strip_halo_snap GE 0 AND bcg_strip_halo_sub GE 0)
icl_ind = where(icl_strip_halo_snap GE 0 AND icl_strip_halo_sub GE 0)

print
print, "strip halo found for ", (size(bcg_ind))[1], " of ",bcg_ids_size," BCG stars, not found for ", bcg_ids_size -(size(bcg_ind))[1]
ind = where((~ (bcg_strip_halo_snap GE 0 AND bcg_strip_halo_sub GE 0)) AND (bcg_formation_halo_snap GE 0 AND bcg_formation_halo_sub GE 0))
ind2 = where(bcg_formation_halo_sub[ind] EQ BCG_progenitor[bcg_formation_halo_snap[ind]])
print,(size(ind2))[1], " of the latter were formed in BCG"
print, total(bcg_dissolves_now[bcg_ind],/integer), " BCG stars were stripped during disruption of their host"

print, "strip halo found for ", (size(icl_ind))[1], " of ",icl_ids_size," ICL stars, not found for ", icl_ids_size -(size(icl_ind))[1] 
ind = where((~ (icl_strip_halo_snap GE 0 AND icl_strip_halo_sub GE 0)) AND (icl_formation_halo_snap GE 0 AND icl_formation_halo_sub GE 0))
ind2 = where(icl_formation_halo_sub[ind] EQ BCG_progenitor[icl_formation_halo_snap[ind]])
print,(size(ind2))[1], " of the latter were formed in BCG"
print, total(icl_dissolves_now[icl_ind],/integer), " ICL stars were stripped during disruption of their host"
print

profilebinsize = 0.2
bcg_mass_dist = histogram(alog10(total(bcg_halo_mass[*,bcg_ind],1)*1.0e10),binsize=profilebinsize,min=0.0,max=16.0,LOCATIONS=dist_masses)
icl_mass_dist = histogram(alog10(total(icl_halo_mass[*,icl_ind],1)*1.0e10),binsize=profilebinsize,min=0.0,max=16.0)
num_dist_bins = (size(bcg_mass_dist))[1]
dist_masses = dist_masses+0.5*profilebinsize ; logarithimic masses in M_sun/h
outputname = outputnamebase+"_strip_masses.txt"
openw,11,outputname,width=256
for i=0,num_dist_bins-1 do printf, 11, dist_masses[i], bcg_mass_dist[i], icl_mass_dist[i] 
close,11

profilebinsize = 0.2
bcg_mass_dist = histogram(alog10(bcg_halo_mass[4,bcg_ind]*1.0e10),binsize=profilebinsize,min=0.0,max=16.0,LOCATIONS=dist_masses)
icl_mass_dist = histogram(alog10(icl_halo_mass[4,icl_ind]*1.0e10),binsize=profilebinsize,min=0.0,max=16.0)
num_dist_bins = (size(bcg_mass_dist))[1]
dist_masses = dist_masses+0.5*profilebinsize ; logarithimic stellar masses in M_sun/h
outputname = outputnamebase+"_strip_stellar_masses.txt"
openw,11,outputname,width=256
for i=0,num_dist_bins-1 do printf, 11, dist_masses[i], bcg_mass_dist[i], icl_mass_dist[i] 
close,11

profilebinsize = 0.25
bcg_res_dist = histogram(bcg_halfmassrad[bcg_ind]/snapsoftphys[bcg_strip_halo_snap[bcg_ind]],binsize=profilebinsize,min=0.0,max=500.0,LOCATIONS=dist_res)
icl_res_dist = histogram(icl_halfmassrad[icl_ind]/snapsoftphys[icl_strip_halo_snap[icl_ind]],binsize=profilebinsize,min=0.0,max=500.0)
num_dist_bins = (size(bcg_res_dist))[1]
dist_res = dist_res+0.5*profilebinsize ; half mass radii in units of softening
outputname = outputnamebase+"_strip_halfmass_res.txt"
openw,11,outputname,width=256
for i=0,num_dist_bins-1 do printf, 11, dist_res[i], bcg_res_dist[i], icl_res_dist[i] 
close,11

profilebinsize = 0.25
bcg_res_dist = histogram(bcg_halflightrad[bcg_ind]/snapsoftphys[bcg_strip_halo_snap[bcg_ind]],binsize=profilebinsize,min=0.0,max=500.0,LOCATIONS=dist_res)
icl_res_dist = histogram(icl_halflightrad[icl_ind]/snapsoftphys[icl_strip_halo_snap[icl_ind]],binsize=profilebinsize,min=0.0,max=500.0)
num_dist_bins = (size(bcg_res_dist))[1]
dist_res = dist_res+0.5*profilebinsize ; half light radii in units of softening
outputname = outputnamebase+"_strip_halflight_res.txt"
openw,11,outputname,width=256
for i=0,num_dist_bins-1 do printf, 11, dist_res[i], bcg_res_dist[i], icl_res_dist[i] 
close,11

bcg_scale_dist = histogram(bcg_strip_halo_snap[bcg_ind],min=0,max=63,LOCATIONS=dist_scale)
icl_scale_dist = histogram(icl_strip_halo_snap[icl_ind],min=0,max=63)
num_dist_bins = (size(bcg_scale_dist))[1]
outputname = outputnamebase+"_strip_scalefac.txt"
openw,11,outputname,width=256
for i=0,num_dist_bins-1 do printf, 11, snaptimes[dist_scale[i]], bcg_scale_dist[i], icl_scale_dist[i] 
close,11

; need to set mindistnext for dissolving galaxies !!!!!!!!!!
profilebinsize = 0.01
bcg_mindistnext_dist = histogram(bcg_mindistnext[bcg_ind],binsize=profilebinsize,min=0.0,max=5.0,LOCATIONS=dist_dist)
icl_mindistnext_dist = histogram(icl_mindistnext[icl_ind],binsize=profilebinsize,min=0.0,max=5.0)
num_dist_bins = (size(bcg_mindistnext_dist))[1]
dist_dist = dist_dist+0.5*profilebinsize ;
outputname = outputnamebase+"_strip_mindistnext.txt"
openw,11,outputname,width=256
for i=0,num_dist_bins-1 do printf, 11, dist_dist[i], bcg_mindistnext_dist[i], icl_mindistnext_dist[i] 
close,11

profilebinsize = 0.01
ind = where(bcg_dissolves_now[bcg_ind] EQ 0)
bcg_mindistnext_dist = histogram(bcg_mindistnext[bcg_ind[ind]],binsize=profilebinsize,min=0.0,max=5.0,LOCATIONS=dist_dist)
ind = where(icl_dissolves_now[icl_ind] EQ 0)
icl_mindistnext_dist = histogram(icl_mindistnext[icl_ind[ind]],binsize=profilebinsize,min=0.0,max=5.0)
num_dist_bins = (size(bcg_mindistnext_dist))[1]
dist_dist = dist_dist+0.5*profilebinsize ;
outputname = outputnamebase+"_strip_mindistnext_realstrip.txt"
openw,11,outputname,width=256
for i=0,num_dist_bins-1 do printf, 11, dist_dist[i], bcg_mindistnext_dist[i], icl_mindistnext_dist[i] 
close,11

gal_mass_dist[*] = -1
bcg_mass_dist[*] = -1
icl_mass_dist[*] = -1
gal_res_dist[*] = -1
bcg_res_dist[*] = -1
icl_res_dist[*] = -1
gal_scale_dist[*] = -1
bcg_scale_dist[*] = -1
icl_scale_dist[*] = -1
gal_ind = 0L
bcg_ind = 0L
icl_ind = 0L

end
