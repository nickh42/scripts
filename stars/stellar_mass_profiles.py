#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 16 17:36:50 2017

@author: nh444
"""

def main():
    # General settings
    obs_dir = "/data/vault/nh444/ObsData/mass_fracs/"
    N = 300
    delta = 500.0
    ref = "crit"
    use_fof = True  # Use halo file for M_delta and R_delta
    h_scale = 0.679
    masstab = False
    outdir = "/data/curie4/nh444/project1/stars/"

    #basedir = "/data/curie4/nh444/project1/boxes/"
    #simdirs = [#"L40_512_LDRIntRadioEff_noBH",
    #           #"L40_512_NoDutyRadioWeak/",
    #           #"L40_512_NoDutyRadioInt/",
    #           #"L40_512_DutyRadioWeak/",
    #           "L40_512_LDRIntRadioEffBHVel/",
    #           ]
    #labels = ["No BHs",
    #          "Ref",
    #          "Stronger radio",
    #          "Quasar duty cycle",
    #          "Combined",
    #          ]
    #
    #snapnums = [4, 5, 6, 8, 10, 15, 20, 25]
    #snapnums = [25]
    #
    #zoombasedir = "/home/nh444/data/project1/zoom_ins/"
    #zoomdirs = ["","","","",
    #            "LDRIntRadioEffBHVel/"] ## corresponding to each simdir
    #zooms = [["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c256_box_Arepo_new", "c384_box_Arepo_new", "c448_box_Arepo_new"]]
    #zooms = [[]]

    basedir = "/data/curie4/nh444/project1/boxes/"
    simdirs = ["L40_512_MDRIntRadioEff"]
    labels = ["fiducial"]

    zoombasedir = "/home/nh444/data/project1/zoom_ins/"
    zoomdirs = ["MDRInt/"]  # corresponding to each simdir
    zooms = [[#"c96_MDRInt", "c128_MDRInt", "c160_MDRInt",
#              "c192_MDRInt", "c224_MDRInt", "c256_MDRInt",
#              "c288_MDRInt", "c320_MDRInt",
#              "c352_MDRInt", "c384_MDRInt",
#              "c448_MDRInt", "c480_MDRInt", "c512_MDRInt",
              "c112_MDRInt", "c144_MDRInt", "c176_MDRInt", "c208_MDRInt",
              "c240_MDRInt", "c272_MDRInt", "c304_MDRInt", "c336_MDRInt",
              "c368_MDRInt", "c400_MDRInt", "c410_MDRInt", "c432_MDRInt",
              "c464_MDRInt",
              "c496_MDRInt_SUBF",
              ]]
    snapnums = range(4, 26)
    # snapnums = range(100)
    # snapnums = [95, 79]
    snapnums = range(79, 99, 4)[::-1]
    snapnums = range(20, 26)[::-1]
    snapnums = [99]

    # Calculate stellar profiles:
    M500min = 1e13
    M500max = None
    nbins = 1000
    # rmin, rmax, rminType = 1e-3, None, "R500"
    rmin, rmax, rminType = 1.0, 1000.0, "pkpc"
    from stars.stellar_mass import stellar_mass
#    for simdir in simdirs:
#        mstar = stellar_mass(basedir, simdir, outdir, snapnums, obs_dir, N=N,
#                             delta=delta, ref=ref, h_scale=h_scale,
#                             use_fof=use_fof, masstab=masstab,
#                             mpc_units=False, verbose=True)
#        mstar.get_mstar_profiles(rmin=rmin, rmax=rmax, rminType=rminType,
#                                 nbins=nbins, M500min=M500min, M500max=M500max)
    for i, zoomdir in enumerate(zoomdirs):
        if zoomdir is not "":
            for zoom in zooms[i]:
                mstar = stellar_mass(zoombasedir+zoomdir, zoom+"/",
                                     outdir+zoomdir,
                                     snapnums, obs_dir, delta=delta, ref=ref,
                                     h_scale=h_scale, use_fof=use_fof,
                                     masstab=masstab, mpc_units=True,
                                     verbose=True)
                mstar.get_mstar_profiles(rmin=rmin, rmax=rmax,
                                         rminType=rminType, RminLowRes=5.0,
                                         M500min=M500min, M500max=M500max,
                                         nbins=nbins)

#    plot_mass_profile_comparison(outdir, simdirs, zoomdirs, zooms, 0.2, nbins=50,
#                                 BCG_only=True, M200min=4.7e14,
#                                 rmax=500.0, rsoft=2.8125, Stott=True,
#                                 stacked=True, projected=True, density=True,
#                                 scaled=False, cumulative=False, outdir=outdir,
#                                 outname="stellar_mass_profiles_Stott_z02.pdf",
#                                 labels=labels)

#    plot_mass_profile_comparison(outdir, simdirs, zoomdirs, zooms, 0.2, nbins=50,
#                                 BCG_only=True, M500min=8e13, M500max=1e15,
#                                 rmax=500.0, rsoft=2.8125,
#                                 Stott=False, kravtsov=True, for_paper=True,
#                                 stacked=False, projected=True, density=True,
#                                 scaled=False, cumulative=False, outdir=outdir,
#                                 outname="stellar_mass_profiles_kravtsov_z02_2D.pdf",
#                                 labels=labels)

#    indir = "/data/curie4/nh444/project1/stars/MDRInt/"
#    simdirs = ["c352_MDRInt", "c400_MDRInt", "c410_MDRInt",
#              "c448_MDRInt", "c480_MDRInt", "c512_MDRInt",
#              ]
#    import numpy as np
#    redshifts = np.arange(0.0, 1.9, 0.2)
#    for simdir in simdirs:
#        plot_mass_profile_redshift(indir, simdir, redshifts, nbins=50, rmax=300.0,
#                                     N=1,#M500min=2.5e14,
#                                     outname="stellar_mass_profiles_redshift_"+simdir+".pdf",
#                                     outdir="/data/vault/nh444/VaultDocs/meetings/Fable_BCG_profs/",
#                                     BCG_only=True, projected=True, scaled=False, density=True,
#                                     h_scale=h_scale)


def plot_mass_profile_redshift(basedir, simdir, redshifts, nbins=0,
                               N=0, M500min=0.0, M200min=0.0, M500max=None,
                               rmax=None,
                               BCG_only=False, projected=False,
                               scaled=False, density=True, col=None,
                               outname="stellar_mass_profiles_redshift.pdf",
                               outdir="/data/curie4/nh444/project1/stars/",
                               plot_obs=True, for_paper=False, h_scale=0.679):
    """
    Plots stellar mass profiles output by stellar_mass.get_mstar_profiles()
    at different redshifts
    Args:
        BCG_only: plot profile of stars bound only to the main halo (BCG+ICL)
        projected: use projected radial bins, otherwise 3D
        scaled: if True, x axis is r/r500, otherwise it is r (kpc)
        density: plot mass per unit volume (or area if projected),
                 otherwise just mass
        cumulative: plot cumulative stellar mass, otherwise mass per bin
        labels: list of labels, one for each simdir in simdirs
        M500min: minimum M500 to plot (Msun)
        M200min: minimum M200 to plot (Msun)
        M500max: maximum M500 to plot (Msun). Set to None for max=infinity
        plot_obs: plot observed profiles
    """
    import numpy as np
    import matplotlib.pyplot as plt
    import os

    if rmax is None:
        rmax = np.inf

    basedir = os.path.normpath(basedir)+"/"

    if col is None:
        from palettable.cartocolors.qualitative import Prism_10 as palette
        col = palette.mpl_colors

    fig, ax = plt.subplots(figsize=(7, 7))
    if scaled:
        ax.set_xlabel("r / r$_{500}$")
    else:
        ax.set_xlabel("r [pkpc]")
    if density:
        if projected:
            ax.set_ylabel(r"$\rho_{\star}$ [M$_{\odot}$ pkpc$^{-2}$]")
        else:
            ax.set_ylabel(r"$\rho_{\star}$ [M$_{\odot}$ pkpc$^{-3}$]")
    else:
        ax.set_ylabel(r"M$_{\star}$(R) [M$_{\odot}$]")
    ax.set_xscale('log')
    ax.set_yscale('log')
    if not for_paper:
        ax.set_title(simdir.replace("_", r"\_"))

    for i, redshift in enumerate(redshifts):
        simdir = os.path.normpath(simdir)+"/"
        profile = load_profile(basedir, simdir, redshift, nbins=nbins,
                               N=N, M500min=M500min, M200min=M200min,
                               M500max=M500max,
                               projected=projected, scaled=scaled,
                               density=density, BCG_only=BCG_only,
                               h_scale=h_scale)
        xall = profile['x']
        yall = profile['y']
        M500 = profile['M500']
        # M200 = profile['M200']
        for x, y, M in zip(xall, yall, M500):
            mask = np.where(x <= rmax)[0]
            if for_paper:
                label = "z={:.1f}".format(redshift)
            else:
                label = (r"z={:.1f}".format(redshift) +
                         r" ({:.1e}".format(M) + r" M$_{\odot}$)")
            ax.plot(x[mask], y[mask], lw=3, c=col[i], label=label)
            if plot_obs and density:
                # Find normalisation at 1 kpc for z=1 and z=0.2
                if redshift > 0.95 and redshift < 1.05:
                    # Get index of bin closest to 1 kpc
                    idx = np.argmin(np.abs(x - 1.0))
                    normz1 = y[idx]
                if redshift > 0.15 and redshift < 0.25:
                    idx = np.argmin(np.abs(x - 1.0))
                    normz2 = y[idx]

    if plot_obs and density:
        # Plot observed surface brightness profiles
        c = '0.4'
        # observed profiles cover 1 kpc to ~200 kpc
        rbins = np.logspace(0, np.log10(200.0), num=1000)
        s = get_sersic(rbins, 57.9, 4.8)
        ax.plot(rbins, s * normz2 / s[0],
                c=c, lw=4, ls='dotted', label="Stott+11 $z=0.23$")
        s = get_sersic(rbins, 47.6, 5.4)
        ax.plot(rbins, s * normz1 / s[0],
                c=c, lw=4, ls='dashed', label="Stott+11 $z=1$")

    ax.legend(loc='best', fontsize='medium' if for_paper else 'x-small')
    fig.savefig(outdir+outname, bbox_inches='tight')
    print "Plot saved to", outdir+outname


def plot_mass_profile_comparison(basedir, simdirs, zoomdirs=None, zooms=None,
                                 redshift=0.0, nbins=0, rmax=None,
                                 projected=False, BCG_only=False,
                                 stacked=False, fit_sersic=False, n_fixed=None,
                                 fit_radii=None,
                                 N=0, M500min=0.0, M200min=0.0, M500max=None,
                                 scaled=True, density=False,
                                 cumulative=False, normalise=False,
                                 plot_label=None, plot_label_pos=(0.1, 0.1),
                                 rsoft=None, Stott=True, kravtsov=False,
                                 outname="stellar_mass_profiles.pdf",
                                 outdir="/data/curie4/nh444/project1/stars/",
                                 for_paper=False, labels=None, h_scale=0.679):
    """Plots stellar mass profiles output by stellar_mass.get_mstar_profiles()
    Args:
        projected: Use projected radial bins, otherwise 3D.
        BCG_only: Use stars bound only to the main halo i.e. BCG+ICL.
        stacked: Plot stacked profiles, otherwise individual profiles.
        fit_sersic: Fit a single Sersic model to the stacked profile.
        n_fixed: Fix sersic index at this value in fit.
        fit_radii: List giving min. and max. radius to use in the fit.
        M500min: Minimum M500 (Msun) to plot.
        M200min: Minimum M200 (Msun) to plot.
        M500max: Maximum M500 to plot (Msun). Default (None) is no limit.
        scaled: If True, x axis is r/r500, otherwise it is r (kpc)
        density: Plot mass per unit volume (or area if projected),
                 otherwise just mass.
        cumulative: Plot cumulative stellar mass, otherwise mass per bin.
        plot_label: Text string to add to figure at position 'plot_label_pos'.
        plot_label_pos: Position of plot_label string in axes coords. The
                        reference point is the middle right of the bounding box
        rsoft: Stellar softening length at given redshift in physical kpc/h.
        Stott: Plot Stott+2011 stacked stellar mass surface density profiles.
        kravtsov: Plot Kravtsov+2018 stellar mass surface density profiles.
        labels: List of labels, one for each simdir in simdirs.
    """
    import numpy as np
    import matplotlib.pyplot as plt
    import os
    if fit_sersic:
        from scipy.optimize import curve_fit

    if zoomdirs is None:
        zoomdirs = [""]*len(simdirs)
    if zooms is None:
        zooms = []
    if rmax is None:
        rmax = np.inf
    if fit_radii is None:
        fit_radii = [0.0, 100.0]

    basedir = os.path.normpath(basedir)+"/"

    fig, ax = plt.subplots(figsize=(7, 7))
    if scaled:
        ax.set_xlabel("r / r$_{500}$")
    else:
        ax.set_xlabel("r [pkpc]")
    if density:
        assert (not cumulative), "cumulative not compatible with density"
        if projected:
            ax.set_ylabel(r"$\rho_{\star}$ [M$_{\odot}$ pkpc$^{-2}$]")
        else:
            ax.set_ylabel(r"$\rho_{\star}$ [M$_{\odot}$ pkpc$^{-3}$]")
    else:
        if cumulative:
            ax.set_ylabel(r"M$_{\star}$($<$R) [M$_{\odot}$]")
        else:
            ax.set_ylabel(r"M$_{\star}$(R) [M$_{\odot}$]")
    ax.set_xscale('log')
    ax.set_yscale('log')
    if np.isfinite(rmax):
        ax.set_xlim(1, rmax)
    if normalise:
        ax.set_ylim(0.0, 1.0)
        ax.set_yscale('linear')
    elif density:
        ax.set_ylim(5e3, 5e10)
    if kravtsov:
        # Extrapolated Kravtsov+18 profiles drop to very low density.
        if projected and density:
            ax.set_ylim(3e4, 3e10)
        elif density:
            ax.set_ylim(70, 7e9)
    import matplotlib.ticker as ticker
    ax.xaxis.set_major_formatter(
            ticker.FuncFormatter(
                    lambda y, pos: (
                            '{{:.{:1d}f}}'.format(
                                    int(np.maximum(-np.log10(y), 0)))
                            ).format(y)))
    if not for_paper:
        ax.set_title("z = {:.2f}".format(redshift))

    col = ['#24439b', '#ED7600', '#50BFD7', '#B94400']
    if sum([len(zl) for zl in zooms]) <= len(col):
        from cycler import cycler
        ax.set_prop_cycle(cycler('color', col))

    if stacked:
        # Legend label for the individual profiles.
        label_ind = "individual profiles"
        # Legend label for the stacked profile.
        label_stack = "stacked profile"
        xstack = []
        ystack = []
        nstack = 0
    M500_list = []
    M200_list = []
    for simidx, simdir in enumerate(simdirs):
        simdir = os.path.normpath(simdir)+"/"
        profile = load_profile(basedir, simdir, redshift, nbins=nbins,
                               M500min=M500min, M200min=M200min,
                               M500max=M500max,
                               projected=projected, scaled=scaled,
                               density=density, BCG_only=BCG_only,
                               h_scale=h_scale)
        xall = profile['x']
        yall = profile['y']
        M500all = profile['M500']
        M200all = profile['M200']
        for x, y, M500, M200 in zip(xall, yall, M500all, M200all):
            mask = np.where(x <= rmax)[0]
            if cumulative:
                y = np.cumsum(y)
                if normalise:
                    y /= y[mask[-1]]
            if stacked:
                # Add profile to stack.
                if nstack > 0:
                    assert np.array_equal(xstack, x[mask]), "x is mismatched"
                    # ystack = ystack + y[mask]
                    ystack = np.vstack((ystack, y[mask]))
                else:
                    xstack = x[mask]
                    ystack = y[mask]
                nstack += 1
                # Plot individual profile.
                ax.plot(x[mask], y[mask], lw=3, c=col[0], alpha=0.3,
                        label=label_ind)
                label_ind = None  # Only want one of these labels.
            else:
                if not for_paper:
                    label = r"{:.1e}".format(M200) + r" M$_{\odot}$"
                else:
                    label = None
                ax.plot(x[mask], y[mask], lw=3,
                        c=col[0] if for_paper else None,
                        label=label)
            M500_list.append(M500)
            M200_list.append(M200)

        if zoomdirs[simidx] is not "":
            for zoom in zooms[simidx]:
                profile = load_profile(
                        basedir, zoomdirs[simidx]+zoom, redshift,
                        N=N, nbins=nbins, M500min=M500min, M200min=M200min,
                        M500max=M500max,
                        projected=projected, scaled=scaled,
                        density=density, BCG_only=BCG_only, h_scale=h_scale)
                xall = profile['x']
                yall = profile['y']
                M500all = profile['M500']
                M200all = profile['M200']
                for x, y, M500, M200 in zip(xall, yall, M500all, M200all):
                    mask = np.where(x <= rmax)[0]
                    if cumulative:
                        y = np.cumsum(y)
                        if normalise:
                            y /= y[mask[-1]]
                    if for_paper:
                        label = None
                    else:
                        label = (zoom.replace("_", r"\_") +
                                 r" ({:.1e}".format(M200) + r" M$_{\odot}$)")
                    M500_list.append(M500)
                    M200_list.append(M200)
                    if stacked:
                        # Add profile to stack.
                        if nstack > 0:
                            if not np.array_equal(xstack, x[mask]):
                                raise ValueError("Radial bins must be "
                                                 "the same for all profiles "
                                                 "if stacking.")
                            # ystack = ystack + y[mask]
                            ystack = np.vstack((ystack, y[mask]))
                        else:
                            xstack = x[mask]
                            ystack = y[mask]
                        nstack += 1
                        # Plot individual profile.
                        ax.plot(x[mask], y[mask], lw=3, c=col[0], alpha=0.3,
                                label=label_ind)
                        label_ind = None  # Only want one of these labels.
                    else:
                        ax.plot(x[mask], y[mask], lw=3,
                                c=col[0] if for_paper else None,
                                label=label)
    print "Median M500 =", np.median(M500_list) / 1e13, "10^13 Msun"
    print "Median M200 =", np.median(M200_list) / 1e13, "10^13 Msun"

    if stacked:
        # Plot the stacked profile.
        ymean = np.mean(ystack, axis=0)  # average of the stack at each radius.
        ax.plot(xstack, ymean, c=col[0], lw=4, label=label_stack)

        if fit_sersic and density and not cumulative:
            # Get standard deviation in log-space
            # Note I'm using np.mean so ddof=1 is assumed.
            logyerr = np.sqrt(np.mean((np.log10(ystack) -
                                       np.log10(ymean))**2, axis=0))
            ax.errorbar(xstack, ymean,
                        yerr=[ymean - 10**(np.log10(ymean) - logyerr),
                              10**(np.log10(ymean) + logyerr) - ymean],
                        ls='none', c='r')

            # parameters = re, ye, n
            ax.set_ylim(1e5, 5e10)
            ind = np.where((xstack > fit_radii[0]) &
                           (xstack < fit_radii[1]))[0]
            if n_fixed is not None:
                p, pcov = curve_fit(
                        lambda r, re, ye: sersic_model_log(r, re, ye, n_fixed),
                        xstack[ind], np.log10(ymean[ind]),
                        sigma=logyerr[ind],
                        p0=(50.0, 7.5), bounds=([10.0, 0.0], [np.inf, np.inf]))
                print "re = {:.1f} +/- {:.1f}".format(p[0], np.sqrt(pcov[0, 0]))
                print "ye = {:.1f} +/- {:.1f}".format(p[1], np.sqrt(pcov[1, 1]))
                ax.plot(xstack[ind],
                        10**sersic_model_log(xstack[ind], p[0], p[1], n_fixed),
                        'r--', lw=3,
                        label="Sersic fit (n = {:.1f})".format(n_fixed))
            else:
                p, pcov = curve_fit(sersic_model_log, xstack[ind],
                                    np.log10(ymean[ind]),
                                    p0=(50.0, 7.5, 5),
                                    bounds=([10.0, 0.0, 0.0],
                                            [np.inf, np.inf, 20.0]))
                print "re = {:.1f} +/- {:.1f}".format(p[0], np.sqrt(pcov[0, 0]))
                print "ye = {:.1f} +/- {:.1f}".format(p[1], np.sqrt(pcov[1, 1]))
                print "n = {:.1f} +/- {:.1f}".format(p[2], np.sqrt(pcov[2, 2]))
                ax.plot(xstack[ind], 10**sersic_model_log(xstack[ind], *p),
                        'r--', lw=3, label="Sersic fit (n free)")

        if cumulative:
            # Get the true half-mass radius
            ind = np.where((xstack > fit_radii[0]) &
                           (xstack < fit_radii[1]))[0]
            xstack = xstack[ind]
            ymean = ymean[ind]
            rhalf = xstack[np.argmin(np.abs(ymean - (ymean[-1] / 2.0)))]
            print "True half-mass rad =", rhalf

    if kravtsov and not scaled:
        # Load cluster names separately
        names = np.genfromtxt("/data/vault/nh444/ObsData/stellar_mass/"
                              "kravtsov_stellar_masses2.csv", usecols=[0],
                              delimiter=",", dtype='S7')
        # Load the rest of the data.
        krav = np.genfromtxt("/data/vault/nh444/ObsData/stellar_mass/"
                             "kravtsov_stellar_masses2.csv",
                             usecols=range(1, 29), delimiter=",")
        label = "Kravtsov+ 2018"
        for idx, name in enumerate(names):
            dat = np.genfromtxt("/data/vault/nh444/ObsData/stellar_mass/"
                                "BCG_profiles/Kravtsov18/"+str(name)+".dat")
            # Get log-centre of bins
            r = np.sqrt(dat[:, 0] * dat[:, 1])
            if projected:
                if density:
                    y = dat[:, 2]  # 2D surface density in Msun/kpc^2
                elif cumulative:
                    y = np.cumsum(dat[:, 2] * np.pi *
                                  (dat[:, 1]**2 - dat[:, 0]**2))  # Msun
                    if normalise:
                        y /= y[-1]
            else:
                if density:
                    y = dat[:, 3]  # 3D volume density in Msun/kpc^3
                elif cumulative:
                    y = np.cumsum(dat[:, 3] * 4.0 / 3.0 * np.pi *
                                  (dat[:, 1]**3 - dat[:, 0]**3))  # Msun
                    if normalise:
                        y /= y[-1]
            # Solid line for r > 1 kpc to r < Rout
            ind = np.where((dat[:, 0] > 0.0) & (dat[:, 0] < krav[idx, 6]))[0]
            p = ax.plot(r[ind], y[ind], lw=3, ls='--', c='#FF9819',
                        label=label)
            label = None  # Only add label once.
            # Dashed line for r > 1 kpc to r < Rout
            ind = np.where((dat[:, 0] > 0.0) & (dat[:, 0] >= krav[idx, 6]))[0]
            ax.plot(r[ind], y[ind], c=p[0].get_color(), lw=3, ls='dotted')

#    if density:
#        line_slope = -1.7
#        line_norm = 1e8
#        line_bins = np.logspace(np.log10(30), 3)
#        ax.plot(line_bins, line_norm * (line_bins / line_bins[0])**line_slope,
#                c='k', lw=3, ls='--')

    # Plot observed surface brightness profiles from Stott+11
    # observed profiles cover 1 kpc to ~200 kpc
#    rbins = np.logspace(0, np.log10(300.0), num=1000)
#    if density and not scaled:
#        if redshift >= 0.5:
#            norm = 3e9  # normalisation at rbins[0]
#            r_e_list = [16.8, 8.5, 62.6, 59.4, 80.3]
#            n_list = [2.9, 2.5, 6.7, 4.3, 6.8]
#            c = '0.7'
#            for r_e, n in zip(r_e_list, n_list):
#                s = get_sersic(rbins, r_e, n)
#                fac = norm / s[0]  # normalisation factor
#                ax.plot(rbins, s*fac, c=c, lw=3)
        # Stacked profiles:
# =============================================================================
#         if redshift < 0.4:
#             s = get_sersic(rbins, 57.9, 4.8)
#             ax.plot(rbins, s * norm / s[0],
#                     c='0.4', lw=4, ls='dashed', zorder=5, alpha=0.5,
#                     label="Stott+11 $z=0.23$")
#         if redshift > 0.7:
#             s = get_sersic(rbins, 47.6, 5.4)
#             ax.plot(rbins, s * norm / s[0],
#                     c='0.4', lw=4, ls='dashed', zorder=5, alpha=0.5,
#                     label="Stott+11 $z=1$")
# =============================================================================
#        # Add second y-axis for surface brightness.
#        ax_mag = ax.twinx()
#        if redshift < 0.4:
#            s = get_sersic(rbins, 57.9, 4.8, mu_e=25.1, mag=True)
#            ax_mag.plot(rbins, s/2.5,
#                        c='0.4', lw=4, ls='dashed', zorder=5,
#                        label="Stott+11 $z=0.23$")
#        if redshift > 0.7:
#            s = get_sersic(rbins, 47.6, 5.4, mu_e=26.4, mag=True)
#            ax_mag.plot(rbins, s/2.5,
#                        c='0.4', lw=4, ls='dashed', zorder=5,
#                        label="Stott+11 $z=1$")
#        # Flip y-axis.
#        ax_mag.set_ylim(ax_mag.get_ylim()[::-1])
    if Stott and not scaled and (density or cumulative):
        # Plot observed surface brightness profiles from Stott+11
        # observed profiles cover 1 kpc to ~200 kpc
        rbins = np.logspace(0, np.log10(1000.0), num=1000)
        if redshift < 0.5:
            label = "Stott+11 ($z=0.23$)"
            M_sun = 4.72  # F606W filter, AB magnitude
            k_e = 0.1226  # k+e correction (F606W filter)
            ML = 2.7078#3.3528  # restframe mass-to-light ratio (F606W filter)
            s = get_sersic(rbins, 57.9, 4.8, mu_e=25.1, z=0.23,
                           mag=False, M_sun=M_sun, k_e=k_e)
            Rout = 110.0
        elif redshift >= 0.5:
            label = "Stott+11 ($z=1$)"
            M_sun = 4.5  # F850LP filter, AB magnitude
            k_e = -0.1992  # k+e correction (F850LP filter)
            ML = 0.8830#1.0574  # restframe mass-to-light ratio (F850LP filter)
            # sersic index free
            s = get_sersic(rbins, 47.6, 5.4, mu_e=26.4, z=1.0,
                           mag=False, M_sun=M_sun, k_e=k_e)
            # sersic index n=4
#            s = get_sersic(rbins, 32.1, 4.0, mu_e=25.6, z=1.0,
#                           mag=False, M_sun=M_sun, k_e=k_e)
            Rout = 90.0
        if cumulative:
            area = np.pi * (rbins[1:]**2 - rbins[:-1]**2)
            x = rbins[1:]
            y = np.cumsum(s[1:] * ML * area)
        else:
            x = rbins
            y = s * ML
        ax.plot(x, y, c='0.4', lw=4, ls='dashed', zorder=5, label=label)
#        idx = np.argmin(np.abs(x - Rout))
#        ax.plot(x[:idx], y[:idx], c='0.4', lw=4, ls='dashed',
#                zorder=5, label=label)
#        ax.plot(x[idx:], y[idx:], c='0.6', lw=4, ls='dashed', zorder=5)

    # Plot the softening length if given.
    if rsoft is not None:
        ax.axvline(rsoft / h_scale, c=col[0], alpha=0.5,
                   ls='--', lw=2, zorder=0)
        xmin, xmax = ax.get_xlim()
        print(xmin, xmax)
        ax.text(np.log10(rsoft / h_scale) /
                (np.log10(xmax) - np.log10(xmin)) + 0.008,
                0.03, "softening", fontsize='x-small', color=col[0], alpha=0.5,
                rotation=90, ha='left', va='bottom', transform=ax.transAxes)

    if plot_label is not None:
        plt.text(plot_label_pos[0], plot_label_pos[1], plot_label,
                 ha='right', va='center', transform=ax.transAxes)

    # Add legend.
    ax.legend(loc='best', fontsize='small', borderaxespad=1)
    fig.savefig(outdir+outname, bbox_inches='tight')
    print "Plot saved to", outdir+outname


def load_profile(basedir, simdir, redshift,
                 N=0, M500min=0, M200min=0.0, M500max=None, nbins=0,
                 BCG_only=False, projected=False, scaled=True, density=False,
                 h_scale=0.679, verbose=False):
    """
    Open a hdf5 file produced by stellar_mass.get_mstar_profiles() and return
    a dictionary containing a list of arrays for the radius and
    stellar mass/density and a list for the halo mass (M500 and M200).
    Args:
        N: get N most massive (by M500) haloes in given mass range.
           Set to <= 0 to use all haloes in mass range.
        M500min: minimum M500 to plot (Msun)
        M200min: minimum M200 to plot (Msun)
        M500max: maximum M500 to plot (Msun). Set to None for max=infinity
        nbins: number of radial bins to try to re-bin to.
               Set to <=0 to use full number of bins stored.
        BCG_only: get profile of stars bound only to the main halo i.e. BCG+ICL
        projected: use projected radial bins, otherwise 3D
        scaled: if True, x axis is r/r500, otherwise it is r (kpc)
        density: calculate mass per unit volume (or area if projected),
                 otherwise just mass.
    """
    import numpy as np
    import h5py

    if M500max is None:
        M500max = np.inf

    xall = []
    yall = []
    M500all = []
    M200all = []
    suffix = "{:.2f}".format(redshift)
    with h5py.File(basedir + simdir + "/"+"stellar_mass_profiles_z" +
                   suffix.zfill(5) + ".hdf5", 'r') as f:
        if 'rminType' in f.attrs.keys():
            rminType = f.attrs['rminType']
        else:
            raise IOError("No 'rminType' attribute in file. "
                          "Re-run stellar_mass.get_mstar_profiles()")
        z = f.attrs['redshift']
        if rminType == "R500":
            binleft = np.asarray(f['r_r500'])
        elif rminType == "pkpc":
            binleft = np.asarray(f['r_pkpc'])
        else:
            raise ValueError("Unknown value of rminType = "+str(rminType))
        assert np.all(np.diff(binleft) >= 0), "bins are not in ascending order"
        if nbins > 0:
            # Re-bin loaded profile to smaller number of bins
            # number of original bins per new bin:
            lbin = int((len(binleft)-1) / nbins)
            if lbin <= 0:
                raise ValueError("Requested number of bins is larger than "
                                 "stored number of bins.")
            bin_ind = range(len(binleft)-1)[::lbin]  # bin indices
            # Now need to get bin edges of each bin
            # If bin edge of last bin is out of bounds:
            if max(bin_ind) + lbin >= len(binleft):
                binleft = binleft[bin_ind]
            # otherwise, add the bin edge of the last bin
            else:
                binleft = binleft[bin_ind + [max(bin_ind)+lbin]]
            if verbose:
                print "Requested", nbins, "bins: got", len(bin_ind)

        bincent = (binleft[1:] + binleft[:-1]) / 2.0
        grp_names = np.asarray(f.keys())

        if N > 0:
            # Get masses of all groups in file
            M500 = []
            for grp_name in grp_names:
                if not isinstance(f[grp_name], h5py.Group):  # skip datasets
                    continue
                M500.append(f[grp_name].attrs['M500'])
            # filter group names to N most massive groups
            grp_names = grp_names[np.argsort(M500)[-N:]]
        for grp_name in grp_names:
            h5grp = f[grp_name]
            if not isinstance(h5grp, h5py.Group):  # skip datasets
                continue
            M500 = h5grp.attrs['M500'] * 1e10 / h_scale
            M200 = h5grp.attrs['M200'] * 1e10 / h_scale
            if M500 > M500min and M200 > M200min and M500 < M500max:
                if rminType == "R500" and scaled:
                    x = bincent  # r/r500
                elif rminType == "R500":
                    x = bincent * h5grp.attrs['R500'] / h_scale / (1.0+z)
                elif rminType == "pkpc" and scaled:
                    x = bincent / (h5grp.attrs['R500'] / h_scale / (1.0+z))
                elif rminType == "pkpc":
                    x = bincent
                else:
                    raise ValueError("Unsure about radial units.")

                if density:
                    if projected:
                        # Msun per pkpc^2
                        fac = (1e10 / h_scale /
                               (np.pi * (binleft[1:]**2 - binleft[:-1]**2)))
                        if rminType == "R500":
                            fac = (fac / (h5grp.attrs['R500'] /
                                          h_scale / (1.0+z))**2.0)
                    else:
                        # Msun per pkpc^3
                        fac = (1e10 / h_scale /
                               (4.0/3.0 * np.pi *
                                (binleft[1:]**3 - binleft[:-1]**3)))
                        if rminType == "R500":
                            fac = (fac / (h5grp.attrs['R500'] /
                                          h_scale / (1.0+z))**3.0)
                else:
                    fac = 1e10 / h_scale

                if BCG_only:
                    if projected:
                        mstar = np.asarray(h5grp['mstar_proj_main_halo'])
                    else:
                        mstar = np.asarray(h5grp['mstar_main_halo'])
                else:
                    if projected:
                        mstar = np.asarray(h5grp['mstar_proj'])
                    else:
                        mstar = np.asarray(h5grp['mstar'])

                if nbins > 0:
                    if lbin > 1:
                        # If re-binning, we need to sum the stellar mass in
                        # adjacent bins.
                        # First pad with zeros to get a length that is
                        # divisible by lbin.
                        mstar = np.pad(mstar, (0, lbin-len(mstar) % lbin),
                                       mode='constant', constant_values=0)
                        # Now sum the stellar mass for every set of 'lbin' bins
                        # by reshaping the array and summing along the new axis
                        mstar = np.sum(mstar.reshape(-1, lbin), axis=1)
                        # As above, if we don't have bin edge for last bin
                        # then ignore it.
                        if max(bin_ind) + lbin >= len(binleft):
                            mstar = mstar[:-1]

                y = mstar * fac

                xall.append(x)
                yall.append(y)
                M500all.append(M500)
                M200all.append(M200)

    results = {'x': xall, 'y': yall, 'M500': M500all, 'M200': M200all}
    return results


def sersic_model(r, re, ye, n):
    import numpy as np

    # b is a coefficient chosen so that re is the half-light radius
    # defined as the radius encircling half the light from the galaxy.
    b = 2.0 * n - 0.327
    return ye * np.exp(-b * ((r / re)**(1.0/n) - 1.0))


def sersic_model_log(r, re, logye, n):
    """Sersic model for when the amplitude is in log base 10."""
    import numpy as np

    # b is a coefficient chosen so that re is the half-light radius
    # defined as the radius encircling half the light from the galaxy.
    b = 2.0 * n - 0.327
    return logye + (1.0 / np.log(10)) * (-b * ((r / re)**(1.0/n) - 1.0))


def get_sersic(r, r_e, n, mu_e=None, z=None, mag=False, M_sun=None, k_e=0.0):
    """Return Sersic profile with half-light radius r_e and Sersic index n.

    For each of radii r, return the surface brightness for a Sersic profile
    with half-light radius r_e and Sersic index n. Based on Stott+ 2011
    equations 1 and 2.
    Args:
        r: radii. Same units as r_e.
        r_e: effective (half-light) radius. Same units as r.
        n: Sersic index.
        mu_e: Surface brightness at r_e (apparent magnitude).
              If None, return intensity in arbitrary units.
        z: redshift. Only needed for flux units.
        mag: Return profile in units of apparent magnitude (Stott+ 2011 eq 2).
             Otherwise, return intensity in flux units (Stott+ 2011 eq 1).
        M_sun: Absolute magnitude of the Sun in this band.
        k_e: k-correction + evolutionary correction
    """
    import numpy as np
    from sim_python import cosmo
    h = 0.6774
    omega_m = 0.3089

    r = np.array(r)
    n = float(n)
    bn = 2.0*n - 0.327
    if mag:
        # Units of apparent magnitude.
        if mu_e is None:
            mu_e = 0.0  # arbitrary units.
        cn = 2.5 * bn / 2.302585093  # ln(10) ~ 2.302585093
        return mu_e + cn * ((r / r_e)**(1.0/n) - 1.0)
    else:
        # Units of flux.
        if mu_e is None:
            Ie = 1.0  # arbitrary units.
        else:
            # Convert mu_e (apparent magnitude per arcsec^2) to
            # luminosity surface density in Lsun per kpc^2
            print "mu_e =", mu_e
            assert z is not None, "Need redshift z."
            cm = cosmo.cosmology(h, omega_m)
            dl = cm.dl(z) * 1.0e6 / h  # Luminosity distance in pc
            print "dl =", dl / 1e6, "Mpc"
            # Convert apparent to absolute magnitude.
            # Estimate Galactic extinction as (from Schlafly & Finkbeiner 2011)
            # 1.243 * E(B-V) (ACS F850LP filter) or
            # 2.471 * E(B-V) (ACS F606W filter) for galactic reddening E(B-V)
            # at a given sky position (e.g. from NED or IRSA)
            # e.g. A = -0.08 for E(B-V)=0.065 and ACS F850LP filter
            A = 0.0  # Galactic extinction
            Mu_e = mu_e - 5.0*(np.log10(dl) - 1.0) + k_e + A
            # Convert to solar luminosity units.
            # Luminosity surface density in solar luminosities per arcsec^2.
            assert M_sun is not None, "Need M_sun."
            Ie = 10**(0.4*(M_sun - Mu_e))
            # Convert from arcsec^-2 to kpc^-2
            arcsec_to_phys = cm.arcsec_to_phys(1.0, z) * 1000.0 / h  # pkpc
            Ie /= arcsec_to_phys**2
            print "Ie =", Ie

#            # Convert apparent magnitude to flux (assuming no extinction)
#            flux = 10**(-(mu_e + 48.6)/2.5)
#            flux /= arcsec_to_phys
#            print "flux =", flux
#            # Convert flux to luminosity
#            dl = cm.dl(z) * cosmo.Mpc * 100.0 / h  # Luminosity distance in cm
#            print "dl =", dl
#            Ie = flux * 4.0*np.pi*dl**2
#            print "Ie =", Ie
#            # Luminosity of Sun in F850LP filter
#            Isun = 10**(-0.4*(4.5 + 48.6)) * 4.0*np.pi*(1.496e13)**2
#            print "Isun =", Isun
#            Ie /= Isun
#            print "Ie =", Ie, "\n"
        return Ie * np.exp(-1.0*bn*((r / r_e)**(1.0/n) - 1.0))


if __name__ == "__main__":
    main()
