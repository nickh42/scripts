#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 14 17:07:52 2017
Plot BCG star formation rates and stellar masses versus redshift or halo mass
@author: nh444
"""
import matplotlib.pyplot as plt
import numpy as np


def main():
    basedir = "/home/nh444/data/project1/zoom_ins/MDRInt/"
    subdirs = [
#            "c96_MDRInt", "c128_MDRInt", "c160_MDRInt", "c192_MDRInt",
#            "c224_MDRInt", "c256_MDRInt",
            #"c288_MDRInt",
            #"c320_MDRInt", "c352_MDRInt",
            "c384_MDRInt",
            "c448_MDRInt", "c480_MDRInt", "c512_MDRInt",
#            "c112_MDRInt", "c144_MDRInt", "c176_MDRInt", "c208_MDRInt",
#            "c240_MDRInt", "c272_MDRInt",
            #"c304_MDRInt", "c336_MDRInt",
            #"c368_MDRInt", "c400_MDRInt", "c410_MDRInt",
            "c432_MDRInt",
            "c464_MDRInt", "c496_MDRInt_SUBF",
          ]
    #snapnums = range(19, 25) * (len(subdirs)-2)
    #snapnums = [75, 79, 83, 87, 91, 95]
    snapnums = [range(16, 25)] * 6 + [range(63, 95, 4)] * 6
    snapnums = [range(16, 25)] * 4 + [range(63, 95, 4)] * 3
#    snapnums = [[25]] * 6 + [[99]]*6
    outdir = "/data/curie4/nh444/project1/stars/"
    mpc_units = True

#    plot_BCG_props(basedir, subdirs, snapnums, yaxis="sSFR", mpc_units=mpc_units, outdir=outdir)
#    plot_BCG_props(basedir, subdirs, snapnums, yaxis="sSFR", #aperture_rad=[15.0]*2,
#              mpc_units=mpc_units, outdir=outdir)
#    plot_BCG_props(basedir, subdirs, snapnums, yaxis="mass", mpc_units=mpc_units, outdir=outdir)
#    plot_BCG_props(basedir, subdirs, snapnums,#[[22]]*(len(subdirs)-4)+[[87]]*4,
#              xaxis="halomass", yaxis="mass", mpc_units=mpc_units, outdir=outdir)
#    plot_BCG_props(basedir, subdirs, snapnums,
#              xaxis="mass", yaxis="SFR", mpc_units=mpc_units, outdir=outdir)

    subdirs = [
            "c96_MDRInt", "c128_MDRInt",
            "c160_MDRInt", "c192_MDRInt",
            "c224_MDRInt", "c256_MDRInt",
            "c288_MDRInt",
            "c320_MDRInt", "c352_MDRInt",
            "c384_MDRInt",
            "c448_MDRInt", "c480_MDRInt", "c512_MDRInt",
            "c112_MDRInt", "c144_MDRInt", "c176_MDRInt", "c208_MDRInt",
            "c240_MDRInt", "c272_MDRInt",
            "c304_MDRInt", "c336_MDRInt",
            "c368_MDRInt", "c400_MDRInt", "c410_MDRInt",
            "c432_MDRInt",
            "c464_MDRInt", "c496_MDRInt_SUBF",
          ]
# =============================================================================
#     snapnums = [[24]]*13 + [[95]]*14
#     obs_dict = {'Liu12': True}
#     plot_BCG_props(basedir, subdirs, snapnums, xaxis="mass", yaxis="SFR",
#                    aperture_rad=[5.0, 5.0],# lookback_time=0.5,
#                    outsuffix="_5kpc",
#                    obs_dict=obs_dict, mpc_units=mpc_units, outdir=outdir)
# =============================================================================
    snapnums = [[24]]*13 + [[95]]*14
    obs_dict = {'Oliva': True, 'Liu12': True, 'Fraser-McKelvie': True}
    plot_BCG_props(basedir, subdirs, snapnums, xaxis="mass", yaxis="SFR",
                   M500lims=[1e14, 1e16],
                   aperture_rad=[30.0, 30.0], #lookback_time=0.5,
                   xlims=[3e10, 3e12], ylims=[0.005, 2000],
                   outsuffix="_30kpc",
                   obs_dict=obs_dict, mpc_units=mpc_units, outdir=outdir)


def plot_BCG_props(basedir, subdirs, snapnums, mpc_units=True,
                   M500lims=[1e13, 1e16], M200lims=[1e13, 1e16],
                   xaxis="z", yaxis="SFR", aperture_rad=None,
                   xlims=None, ylims=None,
                   plot_total_values=False,
                   lookback_time=None, obs_dict={},
                   outsuffix="",
                   outdir="/data/curie4/nh444/project1/stars/",
                   omega_m=0.3089, h_scale=0.6774):
    """
    Plot SFR/sSFR/stellar mass of BCGs versus redshift/halo mass/stellar mass.
    Args:
        snapnums: list of lists, each list containing the snapshot numbers for
                  each subdir.
        xaxis: what to plot on x-axis. "z" for redshift, "mass" for BCG mass or
               "halomass" for halo mass
        yaxis: what to plot on y-axis: "SFR", "sSFR" or "mass" (stellar mass)
        aperture_rad: (list length 2) If not None, calculate quantity within a
                      spherical radius equal to these values in physical kpc
                      for the x- and y-axis quantities, respectively.
        plot_total_values: Plot the total quantities (i.e. of all bound
                           stars/gas) as well as the aperture quantities.
        lookback_time: If not None, calculate the SFR from star particles
                       formed within this latest time period (in Gyr).
                       If None, the SFR is the instantaneous SFR of the gas.
        obs_dict: (dictionary) X-ray observations to compare to.
                  See defaults below.
        outsuffix: (string) Suffix appended to output file name.
    """
    import snap
    import matplotlib.ticker as ticker
    import copy
    from sim_python import cosmo
    cm = cosmo.cosmology(h_scale, omega_m)

    # default dictionary for obs_dict with all entries True
    obs_dict_def = {'McDonald16': False,
                    'Bonaventura17': False,
                    'Liu12': False,
                    'Hoffer12': False,
                    'Fraser-McKelvie': False,
                    'Oliva': False}
    # Make a copy of the dictionary so we don't modify the argument.
    obs_dict = copy.deepcopy(obs_dict)
    # fill in any missing keys
    for key in obs_dict_def:
        if key not in obs_dict:
            obs_dict[key] = obs_dict_def[key]

    if xaxis not in ["z", "mass", "halomass"]:
        raise ValueError("xaxis must be one of 'z', 'mass' or 'halomass'.")
    if yaxis not in ["SFR", "sSFR", "mass"]:
        raise ValueError("yaxis must be one of 'SFR', 'sSFR' or 'mass'.")

    fig, ax = plt.subplots(figsize=(7, 7))
    if xlims is not None:
        ax.set_xlim(xlims[0], xlims[1])
    if ylims is not None:
        ax.set_ylim(ylims[0], ylims[1])
    # Get axis names and units
    if xaxis == "z":
        xaxis_name = "z"
        xaxis_units = ""
        ax.set_xlim(0.1, 1.9)
    elif xaxis == "mass":
        xaxis_name = r"M$_{\star}$"
        xaxis_units = r"[M$_{\odot}$]"
        ax.set_xscale('log')
    elif xaxis == "halomass":
        xaxis_name = r"E(z) M$_{500}$"
        xaxis_units = r"[M$_{\odot}$]"
        ax.set_xscale('log')
        ax.set_xlim(1e14, 4e15)
    if yaxis == "sSFR":
        yaxis_name = "sSFR"
        yaxis_units = "[Gyr$^{-1}$]"
    elif yaxis == "SFR":
        yaxis_name = "SFR"
        yaxis_units = r"[M$_{\odot}$ yr$^{-1}$]"
    elif yaxis == "mass":
        yaxis_name = r"M$_{\star}$"
        yaxis_units = r"[M$_{\odot}$]"

    if aperture_rad is None:
        xlabel = " ".join((xaxis_name, xaxis_units))
    elif xaxis not in ("z", "halomass"):
        xlabel = " ".join((xaxis_name,
                           "($<$ {:d} pkpc)".format(int(aperture_rad[0])),
                           xaxis_units))
    ax.set_xlabel(xlabel)
    if aperture_rad is None:
        ylabel = " ".join((yaxis_name, yaxis_units))
    else:
        ylabel = " ".join((yaxis_name,
                           "($<$ {:d} pkpc)".format(int(aperture_rad[1])),
                           yaxis_units))
    ax.set_ylabel(ylabel)
    ax.set_yscale('log')
    if yaxis == "SFR":
        ax.yaxis.set_major_formatter(ticker.FuncFormatter(
                lambda y, pos: ('{{:.{:1d}f}}'.format(
                        int(np.maximum(-np.log10(y), 0)))).format(y)))

    if len(subdirs) < 8:
        col = ["#c7673e", "#78a44f", "#6b87c8", "#934fce",
               "#e6ab02", "#be558f", "#47af8d"]
    elif len(subdirs) <= 12:
        from palettable.colorbrewer.qualitative import Set3_12 as palette
        col = list(palette.mpl_colors)
    else:
        col = ['#24439b'] * len(subdirs)
    ms = 8
    mew = 1.5

    SalpShift = 10**-0.24  # conversion factor from Salpeter to Chabrier IMF

    if obs_dict['McDonald16']:
        # Individual measurements from McDonald 2016
        dat = np.genfromtxt("/data/vault/nh444/ObsData/McDonald_2016_BCGs.csv",
                            delimiter=",")
        # convert stellar masses and SFRs from Salpeter to Chabrier IMF
        dat[:, range(4, 16)] *= SalpShift
        if xaxis == "z":
            x = dat[:, 1]  # redshift
            xerr = None
        elif xaxis == "mass":
            x = dat[:, 4] * 1e12 * (0.7/h_scale)
            xerr = [np.abs(dat[:, 6]) * 1e12 * (0.7/h_scale),
                    dat[:, 5] * 1e12 * (0.7/h_scale)]
        elif xaxis == "halomass":
            # not sure if h=0.7 for vel. disp. masses
            x = dat[:, 20] * cm.E_z(dat[:, 1]) * 0.7/h_scale
            xerr = [dat[:, 21] * cm.E_z(dat[:, 1]) * 0.7/h_scale,
                    np.abs(dat[:, 22] * cm.E_z(dat[:, 1]) * 0.7/h_scale)]

    elinewidth = 2
    mew = 2
    alpha = 0.7
    kwargs = {'marker': 'o', 'capsize': 8, 'elinewidth': elinewidth,
              'mew': mew, 'ls': 'none',
              'c': '0.6', 'ms': 8, 'mec': 'none'}

    # Plot observations
    if yaxis == "mass":
        if obs_dict['McDonald16']:
            ax.errorbar(x, dat[:, 4] * 1e12 * (0.7/h_scale), xerr=xerr,
                        yerr=[np.abs(dat[:, 6]) * 1e12 * (0.7/h_scale),
                              dat[:, 5] * 1e12 * (0.7/h_scale)],
                        label="McDonald+16", **kwargs)
    elif yaxis == "SFR" or yaxis == "sSFR":
        if obs_dict['McDonald16']:
            UV = dat[:, 7]
            UV_err_high, UV_err_low = dat[:, 8], np.abs(dat[:, 9])
            OII = dat[:, 10]
            OII_err_high, OII_err_low = dat[:, 11], np.abs(dat[:, 12])
            IR = dat[:, 13]
            IR_err_high, IR_err_low = dat[:, 14], np.abs(dat[:, 15])
            # Upper limits have +ve errors equal to zero (or equivalently less
            # than 0.1 to avoid floating point errors)
            # Actual detections (not upper limits):
            UVind = np.where(UV_err_high > 0)[0]
            OIIind = np.where(OII_err_high > 0)[0]
            IRind = np.where(IR_err_high > 0)[0]
            ind = np.concatenate((UVind, OIIind, IRind))
            # Upper limits:
            UVuplims = np.where(UV_err_high < 0.1)[0]
            OIIuplims = np.where(OII_err_high < 0.1)[0]
            IRuplims = np.where(IR_err_high < 0.1)[0]
            # entries with at least one upper limit.
            uplims = np.concatenate((UVuplims, OIIuplims, IRuplims))
            # Take the average of the detections for each BCG
            # If no detections, take the lowest upper limit
            SFR = np.zeros_like(x)
            SFR_err_high = np.zeros_like(x)
            SFR_err_low = np.zeros_like(x)
            uplim_ind = np.zeros_like(x, dtype=bool)
            for i in range(len(SFR)):
                if i in ind:
                    cur_UVind = UVind[UVind == i]
                    cur_OIIind = OIIind[OIIind == i]
                    cur_IRind = IRind[IRind == i]
                    SFR[i] = np.mean(np.concatenate((
                            UV[cur_UVind], OII[cur_OIIind], IR[cur_IRind])))
                    err = np.concatenate((UV_err_high[cur_UVind],
                                          OII_err_high[cur_OIIind],
                                          IR_err_high[cur_IRind]))
                    # error in mean is error in sum divided by number
                    SFR_err_high[i] = (np.sqrt(np.sum((err)**2)) /
                                       float(len(err)))
                    err = np.concatenate((UV_err_low[cur_UVind],
                                          OII_err_low[cur_OIIind],
                                          IR_err_low[cur_IRind]))
                    SFR_err_low[i] = (np.sqrt(np.sum((err)**2)) /
                                      float(len(err)))
                elif i in uplims:
                    SFR[i] = np.min(np.concatenate((UV[UVuplims[UVuplims == i]], OII[OIIuplims[OIIuplims == i]], IR[IRuplims[IRuplims == i]])))
                    # set errors to constant fraction of the value
                    SFR_err_low[i] = SFR[i] * 0.5
                    uplim_ind[i] = True
                else:
                    raise IOError("Entry " + str(i) + " at z="+str(dat[i, 1]) +
                                  " has neither a detection or an upper limit")
            if yaxis == "sSFR":
                Mstar = dat[:, 4] * 1e12 * (0.7/h_scale)
                SFR /= Mstar / 1e9  # Gyr^-1
                SFR_err_high /= Mstar / 1e9
                SFR_err_low /= Mstar / 1e9
            # Plot detections:
            ax.errorbar(x[~uplim_ind], SFR[~uplim_ind],
                        xerr=None if xerr is None else [xerr[0][~uplim_ind], xerr[1][~uplim_ind]],
                        yerr=[SFR_err_low[~uplim_ind],
                              SFR_err_high[~uplim_ind]],
                        label="McDonald+16", alpha=alpha, **kwargs)
            # Plot upper limits:
            ax.errorbar(x[uplim_ind], SFR[uplim_ind],
                        xerr=None if xerr is None else [xerr[0][uplim_ind], xerr[1][uplim_ind]],
                        yerr=[SFR_err_low[uplim_ind], SFR_err_high[uplim_ind]],
                        uplims=True, capsize=0, elinewidth=elinewidth, mew=mew,
                        ls='none', alpha=alpha, c='0.7')
            if yaxis == "SFR":
                # approx lower limit of McDonald+16 sample
                ax.axhline(y=10, c='k', ls='dashed')

        if obs_dict['Hoffer12'] and xaxis == "mass" and yaxis == "SFR":
            # IR SFR measured within 14.3 kpc radius aperture.
            # WARNING: Some of these are upper limits.
            Hof = np.genfromtxt("/data/vault/nh444/ObsData/SFR/BCGs/" +
                                "BCG_SFR_Hoffer2012.csv", delimiter=",")
            ax.errorbar(Hof[:, 9]*1e10*(0.7/h_scale)**2,
                        Hof[:, 4]*(0.7/h_scale)**2, marker='o', ms=ms,
                        ls='none', mec='k', mfc='none',
                        label="Hoffer+ 12 (IR)")
        if obs_dict['Oliva'] and xaxis == "mass" and yaxis == "SFR":
            Oliva = np.genfromtxt("/data/vault/nh444/ObsData/SFR/BCGs/" +
                                  "Oliva-Altamirano2014_BCG_SFR_Mstar.csv",
                                  delimiter=",")
            ax.errorbar(Oliva[:, 0] * (0.7 / h_scale)**2,
                        Oliva[:, 1] * (0.7 / h_scale)**2,
                        yerr=[Oliva[:, 2] * (0.7 / h_scale)**2,
                              Oliva[:, 3] * (0.7 / h_scale)**2],
                        ls='none', lw=2, marker='o', ms=ms,
                        c='0.1', mec='0.1', mfc='0.1', zorder=4,
                        label="Oliva-Altamirano+ 14")
        if obs_dict['Fraser-McKelvie'] and xaxis == "mass" and yaxis == "SFR":
            Fras = np.genfromtxt("/data/vault/nh444/ObsData/SFR/BCGs/" +
                                 "Fraser-McKelvie2014.csv", delimiter=",")
            ax.errorbar(Fras[:, 3] * 1e10 * (0.7 / h_scale)**2,
                        Fras[:, 6] * (0.7 / h_scale)**2,
                        # xerr=[Fras[:, 5] * 1e10 * 0.7 / h_scale,
                        #       Fras[:, 4] * 1e10 * 0.7 / h_scale],
                        # yerr=[Fras[:, 8], Fras[:, 7]],
                        marker='s', ms=ms*0.8,
                        c='0.6', mec='0.6', mfc='none',
                        ls='none', label="Fraser-McKelvie+ 14")
            # SFRs below ~5 Msun/yr at z < 0.05 are likely underestimated so
            # plot these as lower limits.
            lolims = (Fras[:, 1] < 0.05) & (Fras[:, 6] < 5.0)
            ax.errorbar(Fras[lolims, 3] * 1e10 * (0.7 / h_scale)**2,
                        Fras[lolims, 6] * (0.7 / h_scale)**2,
                        yerr=Fras[lolims, 6]*0.5,
                        lolims=True,
                        marker='', ms=ms*0.8, c='0.6', ls='none')
            # ax.axhline(5.0, ls='--', lw=2, c='orange')
        if obs_dict['Liu12'] and xaxis == "mass" and yaxis == "SFR":
            # Liu 2012 - SFRs measured within 3'' DIAMETER (~10 kpc at z=0.2)
            Liu = np.genfromtxt("/data/vault/nh444/ObsData/SFR/BCGs/" +
                                "Liu2012_BCG_SFR.csv", delimiter=",")
            use_fibre = False
            if aperture_rad is not None:
                if aperture_rad[0] <= 10.0:
                    use_fibre = True
            if use_fibre:
                # Stellar mass measured within 3'' DIAMETER (~10 kpc) aperture.
                ax.errorbar(10**Liu[:, 5]*(0.7/h_scale)**2,
                            Liu[:, 12]*(0.7/h_scale)**2, ls='none',
                            marker='d', ms=ms, mec='0.7', mfc='none',
                            label=r"Liu+ 13 (M$_{\mathrm{\star, fibre}}$)")
            else:
                # Approximate Petrosian stellar masses.
                ax.errorbar(10**Liu[:, 6]*(0.7/h_scale)**2,
                            Liu[:, 12]*(0.7/h_scale)**2, ls='none',
                            marker='d', ms=ms, mec='0.7', mfc='none',
                            label=r"Liu+ 13 (M$_{\mathrm{\star, tot}}$)")

    if xaxis == "z":
        if obs_dict['McDonald16']:
            # Means in redshift bins with uncertainties from McDonald 2016
            dat = np.loadtxt("/data/vault/nh444/ObsData/SFR/BCGs/McDonald_2016_BCG_SFR.csv", delimiter=",")
            dat[:, [3,4,5]] *= SalpShift  # convert stellar masses and SFRs from Salpeter to Chabrier IMF
            if yaxis == "sSFR" or yaxis == "SFR":
                if yaxis == "sSFR":
                    ## logarithmic centre of specified range:
                    #y = (dat[:, 6]*dat[:, 7])**0.5
                    #yerr = [y-dat[:, 6], dat[:, 7]-y]
                    xys = np.column_stack((dat[:, 0], dat[:, 6])) ## lower-left corner of rectangle
                    widths = dat[:, 1] - dat[:, 0]
                    heights = dat[:, 7] - dat[:, 6]
                elif yaxis == "SFR":
                    #y = (dat[:, 4]*dat[:, 5])**0.5
                    #yerr = [y-dat[:, 4], dat[:, 5]-y]
                    xys = np.column_stack((dat[:, 0], dat[:, 4])) ## lower-left corner of rectangle
                    widths = dat[:, 1] - dat[:, 0]
                    heights = dat[:, 5] - dat[:, 4]
                #ax.errorbar(z, y, xerr=[z-dat[:, 0], dat[:, 1]-z], yerr=yerr, marker=',', ls='none', c='0.5', label="McDonald+2016")
                import matplotlib.patches as patches
                for xy, width, height in zip(xys, widths, heights):
                    rect = patches.Rectangle(xy, width, height, lw=2,
                                             fill=False, zorder=3)
                    ax.add_patch(rect)
        if obs_dict['Bonaventura17'] and yaxis == "SFR":
            Bon = np.genfromtxt("/data/vault/nh444/ObsData/SFR/BCGs/" +
                                "Bonaventura17_BCG_SFR.csv", delimiter=",")
            ax.errorbar(Bon[:, 0], Bon[:, 1], yerr=[Bon[:, 2], Bon[:, 3]],
                        marker='s', ms=8, c='k', lw=2, ls='none',
                        label="Bonaventura+ 17")

    for simidx, subdir in enumerate(subdirs):
        print "Starting subdir", subdir
        for j, snapnum in enumerate(snapnums[simidx]):
            sim = snap.snapshot(basedir+subdir, snapnum,
                                mpc_units=mpc_units, extra_sub_props=True)
            z = sim.header.redshift
            M500 = sim.cat.group_m_crit500 * 1e10 / h_scale
            M200 = sim.cat.group_m_crit200 * 1e10 / h_scale
            groupind = np.where((M500 >= M500lims[0]) & (M500 < M500lims[1]) &
                                (M200 >= M200lims[0]) & (M200 < M200lims[1]))[0]
            for grpidx, groupnum in enumerate(groupind):
                subnum = sim.cat.group_firstsub[groupnum]
                SFR_tot = sim.cat.sub_sfr[subnum]
                SFR_inrad = sim.cat.sub_sfrinrad[subnum]
                Mstar_tot = sim.cat.sub_masstab[subnum, 4] * 1e10 / h_scale
                Mstar_inrad = (sim.cat.sub_massinradtab[subnum, 4] *
                               1e10 / h_scale)
                if aperture_rad is not None:
                    for i, rad in enumerate(aperture_rad):
                        if rad is None:
                            continue
                        axis = (xaxis, yaxis)[i]
                        if axis == "mass":
                            Mstar_inrad = get_BCG_prop(
                                    sim, subnum, "mass", radius=rad,
                                    lookback_time=lookback_time,
                                    h_scale=h_scale)
                        elif axis in ["SFR", "sSFR"]:
                            SFR_inrad = get_BCG_prop(
                                    sim, subnum, "SFR", radius=rad, cm=cm,
                                    lookback_time=lookback_time,
                                    h_scale=h_scale)
                if yaxis == "sSFR":
                    y_tot = SFR_tot / Mstar_tot * 1e9
                    y_inrad = SFR_inrad / Mstar_inrad * 1e9
                elif yaxis == "SFR":
                    y_tot = SFR_tot
                    y_inrad = SFR_inrad
                elif yaxis == "mass":
                    y_tot = Mstar_tot
                    y_inrad = Mstar_inrad

                if xaxis == "z":
                    x1 = z
                    x2 = z
                    if plot_total_values:
                        x1 -= 0.01
                        x2 += 0.01
                elif xaxis == "mass":
                    x1 = Mstar_tot
                    x2 = Mstar_inrad
                elif xaxis == "halomass":
                    M = (sim.cat.group_m_crit500[groupnum] *
                         cm.E_z(sim.header.redshift) * 1e10 / h_scale)
                    x1 = M
                    x2 = M
                    if plot_total_values:
                        x1 *= 0.95
                        x2 *= 1.05

                if plot_total_values:
                    ax.plot(x1, y_tot, marker='o', ms=ms, mew=mew,
                            mec=col[simidx],
                            mfc=col[simidx], ls='none', zorder=4)
                    label = None
                elif simidx == 0 and j == 0 and grpidx == 0:
                    label = "FABLE"
                else:
                    label = None
                ax.errorbar(x2, y_inrad, marker='D', ms=ms, mew=mew,
                            mec=col[simidx], mfc='none', ls='none',
                            label=label, zorder=4)
                # Add down arrows along x-axis for y below the yaxis.
                y_inrad = 0.0
                if xlims is None or ylims is None:
                    print "Cannot plot down arrows without axes limits."
                else:
                    if y_inrad < ylims[0]:
                        # Arrow width and height in axes transform coords.
                        width = 0.007
                        height = 0.07
                        ax.arrow((np.log10(x2) - np.log10(xlims[0])) /
                                 np.log10(xlims[1] / xlims[0]),
                                 height, 0.0, -height,
                                 length_includes_head=True, width=width,
                                 head_width=3*width, head_length=3*width,
                                 fc=col[simidx], ec='none',
                                 transform=ax.transAxes)
                print "y_tot =", y_tot
                print "y_inrad =", y_inrad

    if plot_total_values:
        handles, labels = ax.get_legend_handles_labels()
        Artist_tot = plt.Line2D((0, 1), (0, 0),
                                mfc='k', mec='k', marker='o', ls='')
        Artist_inrad = plt.Line2D((0, 1), (0, 0),
                                  mfc='none', mec='k', marker='D', ls='')
        if aperture_rad is None:
            aplabel = r"$2 r_{\star, 1/2}$"
        else:
            aplabel = r"aperture"
        ax.legend([Artist_tot, Artist_inrad]+handles,
                  ["Total", aplabel]+labels, frameon=True, loc='best')
    else:
        ax.legend(frameon=False, fontsize='small', loc='best')
    plt.tight_layout()
    outname = "BCG_"+yaxis+"_vs_"+xaxis+outsuffix+".pdf"
    fig.savefig(outdir+outname, bbox_inches='tight')
    print "Saved to", outdir+outname


def get_BCG_prop(sim, subnum, prop, radius=30.0, lookback_time=None, cm=None,
                 h_scale=0.6774):
    """Calculate a galaxy property within the aperture 'radius' in pkpc. """

    z = sim.header.redshift
    if prop == "mass":
        # Get stars within the given aperture.
        sim.load_starpos()
        r = sim.nearest_dist_3D(sim.starpos,
                                sim.cat.sub_pos[subnum])
        rind = np.where(r / h_scale / (1+z) < radius)[0]
        sim.load_starmass()
        result = np.sum(sim.starmass[rind]) * 1e10 / h_scale
    elif prop == "SFR":
        if lookback_time is None:
            # use instantaneous SFR of the gas.
            sim.load_sfr()
            sim.load_gaspos()
            r = sim.nearest_dist_3D(sim.gaspos,
                                    sim.cat.sub_pos[subnum])
            ind = np.where(r / h_scale / (1+z) < radius)
            result = np.sum(sim.sfr[ind])
        else:
            if cm is None:
                raise ValueError("sim_python.cosmo.cosmology object is required.")
            # Calculate SFR from stars formed in the past X Gyr
            # where X is the value of 'lookback_time'.
            # Get stars within the given aperture.
            sim.load_starpos()
            r = sim.nearest_dist_3D(sim.starpos,
                                    sim.cat.sub_pos[subnum])
            rind = np.where(r / h_scale / (1+z) < radius)[0]
            sim.load_stellar_ages()  # formation time as scale factor.
            age_now = cm.age(sim.header.redshift)  # Gyr
            # Get max. redshift for star to be included.
            formation_z = cm.z_of_age(age_now - lookback_time)
            # Get stars between the current and max redshift.
            ageind = np.where(
                    (sim.age[rind] > 1.0/(1.0+formation_z)) &
                    (sim.age[rind] < 1.0/(1.0+sim.header.redshift)))[0]
            # Calculate SFR using the initial masses as star particles
            # can lose mass in the stellar evolution process.
            sim.load_star_initial_mass()
            result = (np.sum(sim.star_inimass[rind[ageind]]) *
                      1e10 / h_scale / (lookback_time * 1e9))
    else:
        raise ValueError("Property '" + str(prop) + "' not recognised.")
    return result


if __name__ == "__main__":
    main()
