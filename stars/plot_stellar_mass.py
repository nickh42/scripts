#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 16 17:36:50 2017

@author: nh444
"""

import os

salp2chab = 0.58  # Factor to multiply stellar masses to convert from
# studies using Salpeter IMF to a Chabrier 2003 IMF - factor from
# Madau 2014 "Cosmic SFH" (0.61) or Chiu 2016/Hilton 2013 (0.58)
lin2chab = 0.76  # Factor to convert Lin 2003 stellar masses to
# equivalent if using a Chabrier IMF (0.76 from Chiu 2015)
gonz2chab = 0.76  # As above for Gonzalez 2014 paper


def main():
    import numpy as np

    # General settings
    obs_dir = "/data/vault/nh444/ObsData/"
    N = 300
    delta = 500.0
    ref = "crit"
    use_fof = True  # Use halo file for M_delta and R_delta
    h_scale = 0.6774
    masstab = False

    basedir = "/data/curie4/nh444/project1/boxes/"
    zoombasedir = "/data/curie4/nh444/project1/zoom_ins/"
    simdirs = [  # "L40_512_LDRIntRadioEff_noBH/",
        # "L40_512_NoDutyRadioWeak/",
        # "L40_512_NoDutyRadioInt/",
        # "L40_512_DutyRadioWeak/",
        "L40_512_LDRIntRadioEffBHVel/",
        # "L40_512_LDRHighRadioEff/",
        "L40_512_MDRIntRadioEff/",
    ]
    labels = [  # "No BHs",
        #"Weak radio",
        #"Stronger radio",
        #"Quasar duty cycle",
        "$\delta t = 10$ Myr",  # "Combined",
        #"Longer radio duty",
        "$\delta t = 25$ Myr",
    ]
    zoomdirs = [  # "","","","",
        "LDRIntRadioEffBHVel/", "MDRInt/"]  # corresponding to each simdir
    zooms = [  # ["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c192_box_Arepo_new", "c256_box_Arepo_new", "c320_box_Arepo_new", "c384_box_Arepo_new", "c448_box_Arepo_new"],
        ["c96_box_Arepo_new", "c128_box_Arepo_new", "c192_box_Arepo_new",
         "c256_box_Arepo_new", "c384_box_Arepo_new"],
        ["c96_MDRInt", "c128_MDRInt", "c192_MDRInt", "c256_MDRInt", "c384_MDRInt"]]

    outfilename = "stellar_mass_fractions_models.pdf"

    basedir = "/data/curie4/nh444/project1/boxes/"
    simdirs = ["L40_512_DutyIntRadioWeak/",
               "L40_512_DutyIntRadioWeak_Thermal/",
               "L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff/",
               "L40_512_DutyInt1RadioWeak_Thermal_FixEta/",
               "L40_512_LDRIntRadioEffBHVel/",
               # "L40_512_LowDutyRadioWeak_HalfThermal_FixEta/",
               # "L40_512_NoDutyRadioWeak_HalfThermal_FixEta/",
               ]
    labels = [r"E$_{\mathrm{SNII},51}= 1$, $U_{therm} = 0$ (small $\epsilon$)",
              r"E$_{\mathrm{SNII},51}= 1$, $U_{therm} = E_k$ (small $\epsilon$)",
              # U$_{\mathrm{wind}} = 2.25$ $\frac{3}{2} \sigma_{DM,1D}^{2}$
              r"E$_{\mathrm{SNII},51}= 1.5$, $U_{therm} = 0.5 E_k$ (small $\epsilon$)",
              r"E$_{\mathrm{SNII},51}= 2$, $U_{therm} = E_k$ (small $\epsilon$)",
              r"E$_{\mathrm{SNII},51}= 1.5$, $U_{therm} = 0.5 E_k$ (larger $\epsilon$)",
              # r"LowDutyRadioWeak\_HalfThermal\_FixEta",
              # r"NoDutyRadioWeak\_HalfThermal\_FixEta",
              ]
    zoomdirs = [""]*len(simdirs)
    zooms = [[]]
    outfilename = "stellar_mass_fractions_winds_comparison_z0.pdf"

    basedir = "/data/curie4/nh444/project1/boxes/"
    simdirs = [  # "L40_512_LDRIntRadioEffBHVel/",
        "L40_512_MDRIntRadioEff/",
    ]
    labels = [None, "Combined"]

    zoombasedir = "/home/nh444/data/project1/zoom_ins/"
    zoomdirs = ["LDRIntRadioEffBHVel/"]  # corresponding to each simdir
    zooms = [["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new",
              "c256_box_Arepo_new", "c320_box_Arepo_new", "c384_box_Arepo_new", "c448_box_Arepo_new"]]
    zooms = [["c96_box_Arepo_new_LowSoft", "c128_box_Arepo_new_LowSoft", "c160_box_Arepo_new_LowSoft",
              "c256_box_Arepo_new_LowSoft", "c320_box_Arepo_new_LowSoft", "c384_box_Arepo_new_LowSoft"]]
    zoomdirs = ["MDRInt/"]
    zooms = [["c96_MDRInt", "c128_MDRInt", "c160_MDRInt", "c192_MDRInt",
              "c224_MDRInt", "c256_MDRInt", "c288_MDRInt", "c320_MDRInt",
              "c352_MDRInt", "c384_MDRInt", "c448_MDRInt", "c480_MDRInt",
              "c512_MDRInt",
              "c112_MDRInt", "c144_MDRInt", "c176_MDRInt", "c208_MDRInt",
              "c240_MDRInt", "c272_MDRInt", "c304_MDRInt", "c336_MDRInt",
              "c368_MDRInt", "c400_MDRInt", "c410_MDRInt", "c432_MDRInt",
              "c464_MDRInt", "c496_MDRInt_SUBF",
              ]]

    col = ["#c7673e", "#78a44f", "#6b87c8",
           "#934fce", "#e6ab02", "#be558f", "#47af8d"]
    # dark blue, light blue, orange, dark red
    col = ['#24439b', '#50BFD7', '#ED7600', '#B94400'] + col
#    from palettable.colorbrewer.sequential import YlGnBu_6_r as pallette
#    col = pallette.mpl_colors
    hlightcol = '#8292BF'
    mec = col
    mfc = ['none']*len(simdirs)
    ms = 8
    mew = 1.8
    errlw = 1.8
    ls = ['solid']*len(simdirs)

    datadir = "/data/curie4/nh444/project1/stars/"  # where stellar masses are saved
    snapnum, z = 25, 0.0
    snapnums = [snapnum]
    #snapnums = range(15, 26)
    #snapnums = range(59, 100)
    outdir = "/data/curie4/nh444/project1/stars/"

    from stars.stellar_mass import stellar_mass
#    for simdir in simdirs:
#        mstar = stellar_mass(basedir, simdir, datadir, snapnums, obs_dir, delta=delta, ref=ref, h_scale=h_scale, use_fof=use_fof, masstab=masstab, mpc_units=False)
#        #mstar.get_mass_fracs(M500min=1e12)
#        mstar.get_stellar_masses(M500min=10**11.6, verbose=True)
#        #mstar.plot_mstarfrac_v_mass(for_paper=True) #requires get_mass_fracs
#        #mstar.show_satellite_mass_distribution(range(0,1), cumulative=True, fractional=True)
#    for i, zoomdir in enumerate(zoomdirs):
#        if zoomdir is not "":
#            for zoom in zooms[i]:
#                mstar = stellar_mass(zoombasedir+zoomdir, zoom+"/",
#                                     outdir+zoomdir, snapnums, obs_dir, N=1,
#                                     delta=delta, ref=ref, h_scale=h_scale,
#                                     use_fof=use_fof, masstab=masstab,
#                                     mpc_units=True)
#                #mstar.get_mass_fracs()
#                mstar.get_stellar_masses(M500min=10**11.6, RminLowRes=5.0,
#                                         verbose=True)

    plot_mstar_v_mass(datadir, simdirs, zoomdirs, zooms, [0.0],#[0.0, 0.4, 0.8, 1.2],
                      #toPlot="total", outfilename="stellar_mass_vs_halo_mass_redshift.pdf",
                      #toPlot="sats", outfilename="satellite_mass_vs_halo_mass_redshift.pdf",
                      toPlot="ICL", outfilename="ICL_mass_vs_halo_mass_z0.pdf",
                      #toPlot="BCG", outfilename="BCG_mass_vs_halo_mass_redshift.pdf", #leg_kwargs={'fontsize': 'x-small', 'ncol': 2},
                      projected=False, incl_ICL=True, fractional=True,
                      BCG_radii=[30, 50, 100],
                      scat_idx=[0, 2], plot_med=True,
                      plot_obs=False, plot_sims=False, for_paper=True,
                      obs_dir=obs_dir, labels=[""], col=col, ls=ls,
                      ms=ms, mew=mew, hlightcol=hlightcol, outdir=outdir,
                      h_scale=h_scale, verbose=True)

#    plot_mBCG_v_mass(datadir, simdirs, zoomdirs, zooms, z=0,
#                     projected=True,
#                     # apsize=100, outfilename="BCG_mass_vs_halo_mass_100kpc.pdf",
#                     apsize=50, outfilename="BCG_mass_vs_halo_mass_50kpc.pdf",
#                     obs_dir=obs_dir, outdir=outdir,
#                     for_paper=True, scat_idx=[0], labels=labels,
#                     col=col, hlightcol=hlightcol, ls=ls,
#                     errlw=errlw, ms=ms, mew=mew,
#                     plot_med=False, h_scale=h_scale, verbose=True)

#    plot_msat_v_mass(datadir, simdirs, zoomdirs, zooms, z, projected=False,
#                     obs_dir=obs_dir, outdir=outdir, for_paper=True,
#                     scat_idx=[0], labels=labels,
#                     col=col, ls=ls, hlightcol=hlightcol,
#                     label_zooms=False, plot_med=False,
#                     h_scale=h_scale, verbose=True)

#    plot_mratio_v_mass(datadir, simdirs, zoomdirs, zooms, z=z,
#                       h5key1="Mstar_BCG_30kpc_proj", h5key2="Mstar_BCG_30kpc",
#                       labels=labels, for_paper=True,
#                       col=col, hlightcol=hlightcol, ls=ls,
#                       ms=ms, mew=mew, errlw=errlw,
#                       h_scale=h_scale, verbose=True)

#    plot_m10_vs_M100(datadir, simdirs, zoomdirs, zooms, z=0,
#                     for_paper=True, scat_idx=range(len(simdirs)),
#                     labels=None, col=col, hlightcol=hlightcol, ls=ls,
#                     ms=ms, mew=mew, errlw=errlw,
#                     h_scale=h_scale, verbose=True)

#    plot_ICL_v_mass(datadir, simdirs, zoomdirs, zooms, z, projected=False,
#                    fraction=False,
#                    obs_dir=obs_dir, outdir=outdir, for_paper=True,
#                    scat_idx=[0], labels=labels, col=col, ls=ls,
#                    plot_med=False, h_scale=h_scale, verbose=True)

#    plot_mass_components_v_mass(datadir, simdirs, zoomdirs, zooms, z, fractional=True,
#                                outdir=outdir, labels=labels, h_scale=h_scale, for_paper=True)

#    redshifts = np.arange(0.0, 1.6, 0.2)
##    plot_mstar_v_redshift(datadir, simdirs, zoomdirs, zooms, redshifts,
##                M500min=2e14, outdir=outdir, scat_idx=[0], plot_med=False, for_paper=True, obs_dir=obs_dir, labels=labels, col=col, ls=ls, h_scale=h_scale)
#    plot_msat_v_redshift(datadir, simdirs, zoomdirs, zooms, redshifts,
#                         M500min_list=[2e14], outdir=outdir, scat_idx=[0],
#                         for_paper=True, obs_dir=obs_dir,
#                         labels=labels, col=col, ls=ls, h_scale=h_scale)

#    redshifts = np.arange(0, 2.5, 0.05)
#    redshifts = np.arange(0, 1.2, 0.2)
#    plot_mBCG_v_redshift(datadir, simdirs, zoomdirs, zooms,
#                         redshifts, apsize=50, projected=False, M500min=3e14,
#                         obs_dir=obs_dir, join_points=False,
#                         outfilename="BCG_mass_vs_redshift_50kpc.pdf", outdir=outdir,
#                         for_paper=True,
#                         scat_idx=[0], labels=labels, col=col, ls=ls,
#                         h_scale=h_scale, verbose=False)

#    plot_mstarfrac_v_mass(datadir, simdirs, zoomdirs, zooms, snapnum, z, obs_dir=obs_dir,
#                                                incl_ICL=True, for_paper=True,
#                                                scat_idx=[], plot_med=False, labels=labels,
#                                                col=col, ms=ms, mec=mec, mfc=mfc, ls=ls,
#                                                outdir=outdir, outfilename=outfilename,
#                                                h_scale=0.679, verbose=False)
#    MBCG_outside_r500(datadir, simdirs, zoomdirs, zooms, snapnum, z, zooms2=zooms2, obs_dir=obs_dir,
#                      scat_idx=[0,1,2,3,4,5,6], labels=labels, col=col, ls=ls, outdir=outdir,
#                      plot_med=True, h_scale=0.7, verbose=False)


def plot_mstar_v_mass(basedir, simdirs, zoomdirs, zooms, redshifts,
                      toPlot="total",
                      fractional=False, projected=False,
                      incl_ICL=True, BCG_radii=[30],
                      figsize=(7, 7),
                      plot_obs=True, obs_dir="/data/vault/nh444/ObsData/",
                      plot_sims=True, fstar=None,
                      for_paper=False, scat_idx=None, plot_med=False,
                      M500lims=[7e12, 3e15], ylims=None,
                      ls=None, markers=None, ms=6.5, mew=1.2, errlw=1.2, lw=4,
                      col=None, hlightcol=None, plot_label=True,
                      plot_label_pos=(0.9, 0.1),
                      labels=None, leg_kwargs={}, leg_len=None,
                      do_fit=False, M500min_fit=1e14,
                      outfilename="stellar_mass_vs_halo_mass.pdf",
                      outdir="/data/curie4/nh444/project1/stars/",
                      h_scale=0.7, verbose=False):
    """Plot stellar mass against M500 at the given redshift(s).

    Args:
        redshifts: Iterable of redshifts.
        toPlot: (string) The stellar mass to plot on the y-axis. Options are:
            "total" - Total stellar mass within r500.
            "BCG"   - Stellar mass in the main halo *within*
                      the radii given in 'BCG_radii'.
            "ICL"   - Stellar mass in the main halo *outside*
                      the radii given in 'BCG_radii'.
            "sats"  - Stellar mass bound in satellites within r500.
        fractional: (bool) Plot stellar mass fraction. Otherwise just the mass.
        projected:  (bool) Projected or spherical stellar mass.
        fstar: (float) If not None, plot a line indicating a stellar fraction
               of this value.
        M500lims: (list/tuple length 2) Min. and max. M500 to plot.
        ylims: (list/tuple length 2) y-axis limits.
        hlightcol: If not None, this is the colour used to highlight
                   groups other than group 0 in zooms
        plot_label: (bool) Add text indicating the aperture.
        leg_kwargs: (dict) Dictionary of keyword arguments for plt.legend().
        leg_len: (int) Maximum number of entries in first legend before
                 overflowing into a second legend.
    """
    import numpy as np
    import matplotlib.pyplot as plt
    from scipy.stats import binned_statistic
    import h5py
    import os

    if not hasattr(redshifts, "__iter__"):
        # Convert scalar value to a list (length 1).
        redshifts = [redshifts]
    if len(redshifts) > 1:
        plot_medians_only = True
    else:
        plot_medians_only = False
    if toPlot in ["BCG", "ICL"]:
        for i in range(len(BCG_radii)):
            if int(BCG_radii[i]) not in [10, 30, 50, 100]:
                raise ValueError("BCG radius = " +
                                 str(BCG_radii[i]) + " not recognised.")
            BCG_radii[i] = int(BCG_radii[i])
        if len(BCG_radii) > 1:
            # If plotting stellar mass within multiple different radii,
            # plot only the median and not the scatter.
            plot_med = True
    if scat_idx is None:
        # Plot scatter for all simdirs by default.
        scat_idx = range(len(simdirs))

    fout_base = "stellar_masses_"
    basedir = os.path.normpath(basedir)+"/"

    # For median profiles
    xmin, xmax = M500lims
    dlogm = 0.3
    num = int((np.log10(xmax) - np.log10(xmin)) / dlogm) + 2
    bins = np.logspace(np.log10(xmin) - dlogm, np.log10(xmax), num=num)
    x = 10**((np.log10(bins[:-1]) + np.log10(bins[1:]))/2)

    if col is None:
        from palettable.colorbrewer.qualitative import Dark2_8 as pallette
        col = pallette.hex_colors
    if ls is None:
        ls = ['solid'] * (len(simdirs))
    if not for_paper:
        zcol = ["#c7673e", "#78a44f", "#6b87c8",
                "#934fce", "#E6AB02", "#be558f", "#47af8d"]
    if markers is None:
        markers = ["D", "s", "o", "^", ">", "v", "<"]
    c1 = '0.6'
    c2 = '0.3'
    simcol = '#F09C48'  # orange

    bias = 1.0  # mass bias factor, 1-b (M_X = (1-b)*M500)

    fig, ax = plt.subplots(figsize=figsize)  # width,height
    ax.set_xscale('log')
    ax.set_xlim(xmin, xmax)
    ax.set_xlabel(r"M$_{500}$ [M$_{\odot}$]")
    if toPlot == "total":
        fractional = False
        ytitle = r"$\mathrm{M}_{\mathrm{\star}, r500}$"
        if ylims is None:
            ax.set_ylim(2e11, 3.5e13)
    elif toPlot == "BCG":
        ytitle = r"$\mathrm{M}_{\mathrm{\star, cen}}$"
        if ylims is None:
            ax.set_ylim(8e10, 2e13)
    elif toPlot == "ICL":
        ytitle = r"$\mathrm{M}_{\mathrm{\star, ICL}}$"
        if ylims is None:
            ax.set_ylim(8e10, 2e13)
    elif toPlot == "sats":
        ytitle = r"$\mathrm{M}_{\mathrm{\star, sats}}$"
        if ylims is None:
            ax.set_ylim(8e10, 2e13)
    if fractional:
        ytitle += r"/ $\mathrm{M}_{\mathrm{\star}, r500}$"
        if ylims is None:
            ax.set_ylim(0.0, 1.0)
    else:
        ytitle += r" [$M_{\odot}$]"
        ax.set_yscale('log')
    ax.set_ylabel(ytitle)
    if ylims is not None:
        ax.set_ylim(ylims[0], ylims[1])

    if fstar is not None and not fractional:
        ax.plot([xmin, xmax], [xmin*fstar, xmax*fstar], lw=5,
                c='#8292BF', alpha=0.4,
                dashes=(0.2, 3), dash_capstyle='round',
                label=r"$f_{\star} ="+"{:.2f}".format(fstar)+"$")

    # Plot observational data and/or other simulation results.
    # Define keyword arguments for the plotting functions.
    kwargs_dat = {'ms': ms, 'mew': mew, 'errlw': errlw,
                  'fractional': fractional, 'projected': projected,
                  'incl_ICL': incl_ICL, 'BCG_radii': BCG_radii,
                  'obs_dir': obs_dir, 'h_scale': h_scale}
    if plot_sims:
        plot_sims_mstar_v_mass(ax, toPlot=toPlot, simcol=simcol, **kwargs_dat)
    if plot_obs:
        plot_obs_mstar_v_mass(ax, toPlot=toPlot, c1=c1, c2=c2, **kwargs_dat)

    # Define keyword args for get_stellar_mass_from_file()
    kwargs_Mstar = {'projected': projected,
                    'incl_ICL': incl_ICL, 'BCG_radii': BCG_radii,
                    'h_scale': h_scale}
    for simidx, simdir in enumerate(simdirs):
        for zidx, redshift in enumerate(redshifts):
            if verbose:
                print "Doing simdir", simdir
            simdir = os.path.normpath(simdir)+"/"
            if labels is None:
                label = simdir[8:-1]
            else:
                label = labels[simidx]
            suffix = "{:.2f}".format(redshift)
            filename = basedir+simdir+fout_base+"z"+suffix.zfill(5)+".hdf5"
            if not os.path.exists(filename):
                raise IOError("File does not exist: " + str(filename))
            with h5py.File(filename) as f:
                # datasets names are MstarTotal, Mstar_r500, Mstar_BCG,
                # Mstar_BCG_r500, Mstar_BCG_30kpc, Mstar_BCG_rcut,
                # Mstar_sats, Mstar_sats_r500
                M500 = np.asarray(f['M500'][:, 0]) / h_scale * bias
                Mstar = get_stellar_mass_from_file(f, toPlot, **kwargs_Mstar)
                if fractional:
                    MstarTot = get_stellar_mass_from_file(f, "total",
                                                          **kwargs_Mstar)
                for axis in range(np.shape(Mstar)[1]):
                    if fractional:
                        Y = Mstar[:, axis] / MstarTot[:, 0]
                    else:
                        Y = Mstar[:, axis]
                    if axis == 0:
                        Yall = np.reshape(Y, (Y.shape[0], 1))
                    else:
                        Yall = np.column_stack((Yall, Y))

                    if simidx in scat_idx and axis == 0 and zidx == 0:
                        if plot_med:
                            label = None  # add label to median below instead.
                        ax.plot(M500, Y, marker=markers[simidx],
                                c=col[simidx], mew=mew, mfc=col[simidx],
                                mec='none', ms=ms, ls='none',
                                label=label, zorder=4)

            if zoomdirs[simidx] is not "":
                if for_paper:
                    zcol = [col[simidx]]*len(zooms[simidx])
                for zoomidx, zoom in enumerate(zooms[simidx]):
                    if verbose:
                        print "Doing zoom", zoom
                    if for_paper:
                        label = None
                    else:
                        label = zoom.replace("_", r"\_")
                    filename = basedir+zoomdirs[simidx]+zoom+"/" + \
                        fout_base+"z"+suffix.zfill(5)+".hdf5"
                    if not os.path.exists(filename):
                        raise IOError("File does not exist: "+str(filename))
                    with h5py.File(filename) as f:
                        grpnum = f['GroupNum'].value[:, 0]
                        grp0 = (grpnum == 0)
                        M500z = f['M500'].value[:, 0] / h_scale * bias
                        M500 = np.concatenate((M500, M500z))
                        Mstar = get_stellar_mass_from_file(
                                f, toPlot, **kwargs_Mstar)
                        if fractional:
                            MstarTot = get_stellar_mass_from_file(
                                    f, "total", **kwargs_Mstar)
                            Yz = Mstar / MstarTot
                        else:
                            Yz = Mstar
                        Yall = np.concatenate((Yall, Yz), axis=0)
                        for axis in range(np.shape(Mstar)[1]):
                            if (simidx in scat_idx and
                                    axis == 0 and zidx == 0):
                                # plot group 0
                                ax.plot(M500z[grp0], Yz[grp0, axis],
                                        marker=markers[simidx],
                                        mec='none', mfc=zcol[zoomidx],
                                        mew=mew, ms=ms, ls='none', zorder=4,
                                        label=label)
                                # plot groups >0 (secondary FoF haloes)
                                if hlightcol is None:
                                    hlightcol = zcol[zoomidx]
                                ax.plot(M500z[~grp0], Yz[~grp0, axis],
                                        marker=markers[simidx],
                                        mec='none', mfc=hlightcol,
                                        mew=mew, ms=ms, ls='none', zorder=3,
                                        label=label)
            if plot_med:
                nlines = np.shape(Yall)[1]
                for line in range(nlines):
                    c = col[simidx+line+zidx]  # colour
                    label = labels[simidx]
                    if nlines > 1 and label is not None:
                        if projected:
                            label += r" r$_{\mathrm{2D}}$ "
                        else:
                            label += r" r$_{\mathrm{3D}}$ "
                    if len(redshifts) > 1 and label is not None:
                        label += " $z = " + "{:.1f}".format(redshift) + "$"
                    if len(BCG_radii) > 1 and label is not None:
                        if toPlot == "BCG":
                            label += " $<$ {:d} kpc".format(
                                    int(BCG_radii[line]))
                        if toPlot == "ICL":
                            label += " $>$ {:d} kpc".format(
                                    int(BCG_radii[line]))
                    med, _, _ = binned_statistic(M500 * bias, Yall[:, line],
                                                 statistic='median', bins=bins)
                    N, _, _ = binned_statistic(M500 * bias, Yall[:, line],
                                               statistic='count', bins=bins)
                    if plot_medians_only:
                        # Plot solid line for bins with more than 10 objects.
                        idx = np.min(np.where((N > 0) & (N < 10))[0])
                        ax.errorbar(x[:idx+1], med[:idx+1], c=c,
                                    lw=lw, ls='solid', dash_capstyle='round',
                                    label=label)
#                        # Plot dashed line for bins with more than 5 objects.
#                        ind2 = np.where((N > 0) & (N < 5))[0]
#                        if len(ind2) > 0:
#                            idx2 = np.min(ind2)
#                            ax.plot(x[idx:idx2+1], med[idx:idx2+1], c=c, lw=lw,
#                                    ls='dashed', dash_capstyle='round', zorder=4)
                        ax.plot(x[idx:], med[idx:], c=c, lw=lw,
                                ls='dashed', dash_capstyle='round', zorder=4)
                    else:
                        # Plot solid line for bins with more than 5 objects.
                        idx = np.min(np.where((N > 0) & (N < 5))[0])
                        ax.errorbar(x[:idx+1], med[:idx+1], c=c,
                                    lw=lw, ls='solid', dash_capstyle='round',
                                    label=label)
                        # Plot individual points for bins with fewer than 5 objects
                        mind = np.where(M500 * bias > x[idx])[0]
#                        # Plot individual points for X most massive.
#                        mind = np.argsort(M500)[-20:]
                        ax.plot(M500[mind] * bias, Yall[mind, line],
                                marker=markers[simidx], mec='none', mfc=c,
                                mew=mew, ms=ms, ls='none')
#                    if line == 0:
#                        # Plot 68% confidence interval.
#                        p = lambda a: np.percentile(a, 16.0)
#                        low, _, _ = binned_statistic(M500 * bias, Yall[:, line],
#                                                     statistic=p, bins=bins)
#                        p = lambda a: np.percentile(a, 84.0)
#                        high, _, _ = binned_statistic(M500 * bias, Yall[:, line],
#                                                      statistic=p, bins=bins)
#                        ax.fill_between(x[:idx+1], low[:idx+1], high[:idx+1],
#                                        facecolor=c, alpha=0.2)

            if do_fit:
                # Create the powerlaw function for a given pivot point X0
                from scaling.fit_relation import fit_powerlaw
                from scaling.fit_relation import make_powerlaw
                X0 = 4.8e14
                powerlaw = make_powerlaw(np.log10(X0))
                inislope = 0.8
                inilogC = 12.7
                ind = np.where(M500 >= M500min_fit)[0]
                if np.shape(Yall)[1] != 1:
                    raise NotImplementedError(
                            "Cannot fit for multiple BCG_radii.")
                (slope, slope_err, logC, logC_err,
                 scat, scat_err) = fit_powerlaw(
                    M500[ind], Yall[ind, 0], X0, inislope, inilogC,
                    bootstrap=True, verbose=verbose)
                if verbose:
                    print "slope = {:.3f} [{:.3f}, {:.3f}]".format(
                            slope, slope_err[0], slope_err[1])
                    print "logC = {:.3f} [{:.3f}, {:.3f}]".format(
                            logC, logC_err[0], logC_err[1])
                    print "scat = {:.3f} [{:.3f}, {:.3f}]".format(
                            scat, scat_err[0], scat_err[1])
                ax.plot([xmin, xmax],
                        [10**powerlaw(np.log10(xmin), slope, logC),
                         10**powerlaw(np.log10(xmax), slope, logC)],
                        c=col[simidx], lw=3)

    # Add legend(s).
    # define default ax.legend() kwargs.
    default_leg_kwargs = {'loc': 'best', 'borderaxespad': 1, 'frameon': False,
                          'fontsize': 'x-small'}
    # fill in any missing keys in leg_kwargs with the defaults.
    for key in default_leg_kwargs:
        if key not in leg_kwargs:
            leg_kwargs[key] = default_leg_kwargs[key]
    if leg_len is None:
        # Place all legend entries in one legend.
        leg = ax.legend(**leg_kwargs)
#        # Align legend text to the right-hand edge.
#        # get the width of your widest label, since every label will need
#        # to shift by this amount after we align to the right
#        shift = max([t.get_window_extent(renderer=fig.canvas.get_renderer()).width for t in leg.get_texts()])
#        for t in leg.get_texts():
#            t.set_ha('right')
#            t.set_position((shift, 0))
    else:
        # Two legends. One upper left and the other lower right.
        del leg_kwargs['loc']
        handles, labels = ax.get_legend_handles_labels()
        leg = ax.legend(handles[:leg_len], labels[:leg_len],
                        loc='upper left', **leg_kwargs)
        ax.add_artist(leg)
        ax.legend(handles[leg_len:], labels[leg_len:],
                  loc='lower right', **leg_kwargs)

    if plot_label:
        if projected:
            label = r"r$_{\mathrm{2D}}$ "
        else:
            label = r"r$_{\mathrm{3D}}$ "
        if toPlot == "total" or toPlot == "sats":
            label += r"$\leq$ r$_{500}$"
        elif toPlot == "BCG" and len(BCG_radii) < 2:
            label += r"$\leq$ {:d} pkpc".format(BCG_radii[0])
        elif toPlot == "ICL" and len(BCG_radii) < 2:
            label += r"$>$ {:d} pkpc".format(BCG_radii[0])
        ax.text(plot_label_pos[0], plot_label_pos[1], label,
                ha='right' if plot_label_pos[0] > 0.5 else 'left', va='center',
                transform=ax.transAxes)

    if not for_paper:
        fig.savefig(outdir+outfilename)
    else:
        fig.savefig(outdir+outfilename, bbox_inches='tight')
    print "Plot saved to", outdir+outfilename


def get_stellar_mass_from_file(f, toPlot, projected=True, incl_ICL=True,
                               BCG_radii=[30],
                               h_scale=0.6774):
    import numpy as np

    if toPlot == "total":
        if incl_ICL:
            if projected:
                Mstar = (f['Mstar_r500_proj'].value) / h_scale
            else:
                Mstar = (f['Mstar_r500'].value) / h_scale
        else:
            if projected:
                Mstar = (f['Mstar_BCG_30kpc'].value +
                         f['Mstar_sats_r500_proj'].value) / h_scale
            else:
                Mstar = (f['Mstar_BCG_30kpc'].value +
                         f['Mstar_sats_r500'].value) / h_scale
    elif toPlot in ["BCG", "ICL"]:
        # Set the HDF5 dataset name corresponding to the BCG mass
        # Possible datsets are:
        # Mstar_BCG_rcut, Mstar_BCG_10kpc, Mstar_BCG_30kpc,
        # Mstar_BCG_50kpc, Mstar_BCG_100kpc
        for i, BCG_rad in enumerate(BCG_radii):
            if BCG_rad == 10:
                BCG_mass_keyword = "Mstar_BCG_10kpc"
            elif BCG_rad == 30:
                BCG_mass_keyword = "Mstar_BCG_30kpc"
            elif BCG_rad == 50:
                BCG_mass_keyword = "Mstar_BCG_50kpc"
            elif BCG_rad == 100:
                BCG_mass_keyword = "Mstar_BCG_100kpc"
            if toPlot == "BCG":
                if projected:
                    Mstar_ = (f[BCG_mass_keyword+'_proj'].value) / h_scale
                else:
                    Mstar_ = (f[BCG_mass_keyword].value) / h_scale
            elif toPlot == "ICL":
                if projected:
                    Mstar_ = (f['Mstar_BCG_r500_proj'].value -
                              f[BCG_mass_keyword+'_proj'].value) / h_scale
                else:
                    Mstar_ = (f['Mstar_BCG_r500'].value -
                              f[BCG_mass_keyword].value) / h_scale
            if i == 0:
                Mstar = Mstar_
            else:
                Mstar = np.concatenate((Mstar, Mstar_), axis=1)
    elif toPlot == "sats":
        if projected:
            Mstar = f['Mstar_sats_r500_proj'].value / h_scale
        else:
            Mstar = f['Mstar_sats_r500'].value / h_scale
    else:
        raise ValueError("Unrecognised value for toPlot")

    return Mstar


def plot_obs_mstar_v_mass(ax, toPlot="total", c1='0.6', c2='0.3',
                          ms=6.5, mew=1.8, errlw=1.8, lw=2,
                          fractional=False, projected=False, incl_ICL=True,
                          BCG_radii=[30],
                          obs_dir="/data/vault/nh444/ObsData/",
                          h_scale=0.6774):
    import numpy as np

    # Stellar masses are scaled by (h_obs/h_scale)**hfac
    # Masses estimated from luminosity carry an h^-2 dependence (see e.g.
    # Croton2013 and DSouza2015)
    hfac = 2.0

    xmin, xmax = ax.get_xlim()

    krav_kwargs = {'c': c1, 'mfc': 'none', 'mec': c1, 'mew': mew,
                   'marker': 's', 'lw': errlw, 'ms': ms*1.4, 'ls': 'none'}

    if toPlot == "total" and not fractional and incl_ICL:
        # Budzynski+ 2013 best-fit
        mh_arr = np.logspace(13.7, 15.0, num=100)
        # Best-fit parameters without ICL
        a = 0.89
        b = 12.44
        # Best-fit parameters with ICL
        a = 1.05
        b = 12.6
        ax.errorbar(mh_arr,
                    (0.71/h_scale)**hfac *
                    10**(b + a * np.log10(mh_arr * h_scale/0.71/3e14)),
                    c=c1, lw=2, label="Budzynski+ 2013")

        # Gonzalez+ 2013 (BCG + sats + ICL) 0.05 < z < 0.12
        gonz = np.genfromtxt(obs_dir+"mass_fracs/" +
                             "Gonzalez_2013_stellar_gas_masses.csv",
                             delimiter=",")
        ax.errorbar(gonz[:11, 14]*0.702*1e14/h_scale,
                    gonz[:11, 23]*gonz2chab*1e13*(0.702/h_scale)**hfac,
                    xerr=[gonz[:11, 15]*0.702/h_scale*1e14,
                          gonz[:11, 15]*0.702/h_scale*1e14],
                    yerr=gonz[:11, 24]*gonz2chab*1e13*(0.702/h_scale)**hfac,
                    c=c1, mec=c1, marker='o', ls='none', ms=ms*1.2,
                    mew=mew, lw=errlw,
                    label="Gonzalez+ 2013")
        # Sanderson+ 2013 (BCG + sats + ICL) z=[0.0571, 0.0796, 0.0943]
        sand = np.genfromtxt(obs_dir + "mass_fracs/" +
                             "Sanderson_2013_stellar_baryon_fracs.csv",
                             delimiter=",")
        ax.errorbar(sand[:, 3]*0.7*1e14/h_scale,
                    sand[:, 7]*gonz2chab*sand[:, 3]*1e14*(0.7/h_scale)**hfac,
                    xerr=sand[:, 4]*0.7/h_scale*1e14,
                    yerr=sand[:, 7]*(gonz2chab*sand[:, 3] * 1e14 *
                                     (0.7/h_scale)**hfac *
                                     np.sqrt((sand[:, 8] / sand[:, 7])**2 +
                                             (sand[:, 4] / sand[:, 3])**2)),
                    c=c1, mec=c1, marker='+', ls='none', ms=ms*1.2,
                    mew=mew, lw=errlw,
                    label="Sanderson+ 2013")

        # Kravtsov+ 2018 (BCG + sats + ICL) z < 0.1
        # I use the values from their plot because their tabulated values
        # a) don't make sense, and b) don't match the plot
        krav = np.genfromtxt(obs_dir+"stellar_mass/" +
                             "kravtsov_stellar_masses.csv",
                             delimiter=",")
        ax.errorbar(krav[:, 3] * 1e14 * 0.7 / h_scale,
                    krav[:, 6] * 1e12 * (0.7 / h_scale)**hfac,
                    yerr=[krav[:, 7] * 1e12 * (0.7 / h_scale)**hfac,
                          krav[:, 8] * 1e12 * (0.7 / h_scale)**hfac],
                    label="Kravtsov+ 2018", **krav_kwargs)

#        def krav_fit(M): return 10**(0.59*np.log10(M/10**14.5) + 12.71)
#        ax.errorbar([xmin, xmax], [krav_fit(xmin), krav_fit(xmax)],
#                     c=c1, ls='-', label="Kravtsov+18 + Gonzalez+13")

#        def krav_fit(M): return 10**(0.69*np.log10(M/10**14.5) + 12.63)
#        ax.errorbar([xmin, xmax], [krav_fit(xmin), krav_fit(xmax)],
#                     c=c1, ls='--', label="Kravtsov+18")

        # Chiu+ 2018 high-z SZ-selected clusters
#        chiu = np.genfromtxt(obs_dir+"mass_fracs/"+"Chiu_2017.csv",
#                             delimiter=",")
#        ax.errorbar(chiu[:, 3]*1e14*0.68/h_scale,
#                     chiu[:, 7]*1e12*0.68/h_scale,
#                     xerr=chiu[:, 4]*1e14*0.68/h_scale,
#                     yerr=chiu[:, 8]*1e12*0.68/h_scale,
#                     marker='d', c=c1, mec=c1, mew=mew, lw=errlw,
#                     ls='none', label="Chiu+ 18")

        # Huang et al. 2018
        Huang = np.genfromtxt(obs_dir + "stellar_mass/Huang_18_Mstar_tot.csv",
                              delimiter=",")
        M200_to_M500 = 0.72  # same factor used in Huang+18
        ax.errorbar(10**Huang[:, 0] * (0.7 / h_scale) * M200_to_M500,
                    10**Huang[:, 1] * (0.7 / h_scale)**hfac,
                    c=c2, mfc='white', mec=c2, marker='o', mew=mew,
                    ls='none', ms=ms*1.4, label="Huang+ 2018")

    if toPlot == "BCG" and fractional:
        # Kravtsov+ 2018 (BCG + sats + ICL) z < 0.1
        krav = np.genfromtxt(obs_dir+"stellar_mass/" +
                             "kravtsov_stellar_masses.csv", delimiter=",")
        if 30 in BCG_radii:
            if projected:
                y = krav[:, 12] * 1e11 / (krav[:, 6] * 1e12)
            else:
                y = krav[:, 13] * 1e11 / (krav[:, 6] * 1e12)
            print "y =", y
            ax.errorbar(krav[:, 3] * 1e14 * 0.7 / h_scale, y,
                        yerr=[y*(krav[:, 7] / krav[:, 6]),
                              y*(krav[:, 8] / krav[:, 6])],
                        label="Kravtsov+ 2018 ($< 30$ kpc)", **krav_kwargs)

    if toPlot == "BCG" and not fractional:
        # Load observational data.
        DeM = np.genfromtxt(obs_dir+"stellar_mass/DeMaio_2018.csv",
                            delimiter=",")
        DeM_kwargs = {'c': c1, 'mec': c1, 'mew': mew, 'marker': 'd', 'ms': ms,
                      'lw': errlw, 'ls': 'none', 'label': "DeMaio+ 2018"}
        # Kravtsov+ 2018 (BCG + sats + ICL) z < 0.1
        krav = np.genfromtxt(obs_dir+"mass_fracs/" +
                             "kravtsov_stellar_masses2.csv", delimiter=",")

        # Plot observational data.
        # Conversion factor from M500 to M200 for concentration c=5
        M200toM500 = 0.72
        M500toM200 = 1.0 / M200toM500
        if 50 in BCG_radii:
            # Zhang+ 16 best-fit (individual measurements not tabulated)
            m = np.array([xmin, xmax]) * M500toM200 / 1.5e14
            ax.errorbar([xmin, xmax],
                        10**(11.6 + 0.3*np.log10(m * h_scale / 0.7)) *
                        (0.7 / h_scale)**hfac,
                        ls='--', lw=2, c=c2, label="Zhang+ 2016 best-fit")

            if projected:
                ax.errorbar(krav[:, 5]*1e14*0.7/h_scale,
                            krav[:, 32]*1e11*(0.7/h_scale)**hfac,
                            # xerr=krav[:, 6]*1e14*0.7/h_scale,
                            label="Kravtsov+ 2018", **krav_kwargs)
            else:
                ax.errorbar(krav[:, 5]*1e14*0.7/h_scale,
                            krav[:, 33]*1e11*(0.7/h_scale)**hfac,
                            # xerr=krav[:, 6]*1e14*0.7/h_scale,
                            label="Kravtsov+ 2018", **krav_kwargs)

            ax.errorbar(DeM[:, 5]*1e14*0.697/h_scale,
                        DeM[:, 18]*1e11*(0.697/h_scale)**hfac,
                        # xerr=DeM[:, 6]*1e14*0.697/h_scale,
                        yerr=DeM[:, 19]*1e11*(0.697/h_scale)**hfac,
                        **DeM_kwargs)

        if 10 in BCG_radii:
            ax.errorbar(DeM[:, 5]*1e14*0.697/h_scale,
                        DeM[:, 16]*1e11*(0.697/h_scale)**hfac,
                        xerr=DeM[:, 6]*1e14*0.697/h_scale,
                        yerr=DeM[:, 17]*1e11*(0.697/h_scale)**hfac,
                        **DeM_kwargs)
        if 30 in BCG_radii:
            # Lidman+ 2012
            Lid = np.genfromtxt(obs_dir + "stellar_mass/" +
                                "Lidman_2012.csv", delimiter=",")
            ind = np.where(Lid[:, 1] <= 0.2)[0]
            ax.errorbar(Lid[ind, 2]*1e15*M200toM500*(0.7/h_scale),
                        Lid[ind, 13] * 1e12 * (0.7/h_scale)**hfac,
                        # xerr=[np.abs(Lid[ind, 4])*1e15*M200toM500*(0.7/h_scale),
                        #      Lid[ind, 3]*1e15*M200toM500*(0.7/h_scale)],
                        marker='x', ms=ms, mew=mew, ls='none', lw=errlw,
                        c=c1, mec=c1, mfc='none',
                        label="Lidman+ 2012 ($z < 0.2$)")

            # Zhang+ 2016 (32 kpc radius aperture)
            Zhang = np.genfromtxt(obs_dir+"stellar_mass/" +
                                  "Zhang_2016.csv", delimiter=",")
            # Restrict to z<0.2
            ind = np.where(Zhang[:, 4] <= 0.2)[0]
            y = 10**Zhang[ind, 6]*(0.7/h_scale)**hfac
            yerr = np.array([y - (10**(Zhang[ind, 6]-Zhang[ind, 7]) *
                                  (0.7/h_scale)**hfac),
                             (10**(Zhang[ind, 6]+Zhang[ind, 7]) *
                              (0.7/h_scale)**hfac) - y])
            xerr = None
            ax.errorbar(10**Zhang[ind, 2]*M200toM500*0.7/h_scale, y,
                        xerr=xerr, yerr=yerr, c=c2, mec=c2, marker='D',
                        ls='none', lw=errlw, ms=ms*0.9,
                        label="Zhang+ 2016 ($z < 0.2$)")
            # Zhang+ 16 best-fit (individual measurements not tabulated)
            m = np.array([xmin, xmax]) * M500toM200 / 1.5e14
            ax.errorbar([xmin, xmax],
                        10**(11.52 + 0.24*np.log10(m * h_scale / 0.7)) *
                        (0.7 / h_scale)**hfac,
                        ls='--', lw=2, c=c2, label="Zhang+ 2016 best-fit")

            # Bellstedt+ 2016
            # Zhang+ 16 say that a 30kpc radius aperture is comparable to the
            # SExtractor aperture used by Bellstedt+16. This seems to be
            # consistent with the results of Lin+ 2017
            bell = np.genfromtxt(obs_dir+"stellar_mass/" +
                                 "Bellstedt_2016.csv", delimiter=",")
            zlim = 0.2  # Restrict to redshift < zlim
            ind = np.where((bell[:, 1] < zlim) | (bell[:, 2] < zlim))[0]
            ax.errorbar(bell[ind, 8]*M200toM500*1e15*0.7/h_scale,
                        bell[ind, 7]*1e12*(0.7/h_scale)**hfac,
                        # xerr=bell[ind, 9] * M200toM500*1e15*0.7/h_scale,
                        marker='o', c=c1, mec=c1, mfc='none', mew=mew, ms=ms,
                        lw=errlw, ls='none',
                        label="Bellstedt+ 2016 ($z < " +
                        "{:.1f}".format(zlim) + "$)")

            if projected:
                ax.errorbar(krav[:, 5]*1e14*0.7/h_scale,
                            krav[:, 30]*1e11*(0.7/h_scale)**hfac,
                            # xerr=krav[:, 6]*1e14*0.7/h_scale,
                            label="Kravtsov+ 2018", **krav_kwargs)
            else:
                ax.errorbar(krav[:, 5]*1e14*0.7/h_scale,
                            krav[:, 31]*1e11*(0.7/h_scale)**hfac,
                            # xerr=krav[:, 6]*1e14*0.7/h_scale,
                            label="Kravtsov+ 2018", **krav_kwargs)

        if 100 in BCG_radii:
            ax.errorbar(DeM[:, 5]*1e14*0.697/h_scale,
                        DeM[:, 20]*1e11*(0.697/h_scale)**hfac,
                        xerr=DeM[:, 6]*1e14*0.697/h_scale,
                        yerr=DeM[:, 21]*1e11*(0.697/h_scale)**hfac,
                        **DeM_kwargs)

    if toPlot == "ICL":
        if 30 in BCG_radii:
            krav = np.genfromtxt(obs_dir+"stellar_mass/" +
                                 "kravtsov_stellar_masses.csv", delimiter=",")
            # 'Total' BCG mass (from triple sersic fit integrated to infinity)
            # minus the BCG mass within 30 kpc.
            y = ((krav[:, 18] - krav[:, 26] / 10.0) *
                 1e12 * (0.7 / h_scale)**hfac)
            yerr = krav[:, 19] * 1e12 * (0.7 / h_scale)**hfac
            if fractional:  # divide by total stellar mass.
                y /= krav[:, 6] * 1e12 * (0.7 / h_scale)**hfac
                # Add errors in quadrature.
                yerr = [y * np.sqrt((krav[:, 19] /
                                     (krav[:, 18] - krav[:, 26] / 10.0))**2 +
                                    (krav[:, 8] / krav[:, 6])**2),
                        y * np.sqrt((krav[:, 19] /
                                     (krav[:, 18] - krav[:, 26] / 10.0))**2 +
                                    (krav[:, 7] / krav[:, 6])**2)]
            ax.errorbar(krav[:, 3] * 1e14 * 0.7 / h_scale, y, yerr=yerr,
                        label="Kravtsov+ 2018 ($> 30$ kpc)", **krav_kwargs)

    if toPlot == "sats":
        krav = np.genfromtxt(obs_dir+"stellar_mass/" +
                             "kravtsov_stellar_masses.csv", delimiter=",")
        y = krav[:, 9] * 1e12 * (0.7 / h_scale)**hfac
        yerr = [krav[:, 10] * 1e12 * (0.7 / h_scale)**hfac,
                krav[:, 11] * 1e12 * (0.7 / h_scale)**hfac]
        if fractional:  # divide by total stellar mass
            y /= krav[:, 6] * 1e12 * (0.7 / h_scale)**hfac
            # Add errors in quadrature. Use lower error bars in total mass.
            yerr = [y * np.sqrt((krav[:, 10] / krav[:, 9])**2 +
                                (krav[:, 7] / krav[:, 6])**2),
                    y * np.sqrt((krav[:, 11] / krav[:, 9])**2 +
                                (krav[:, 7] / krav[:, 6])**2)]
        ax.errorbar(krav[:, 3] * 1e14 * 0.7 / h_scale, y, yerr=yerr,
                    label="Kravtsov+ 2018", **krav_kwargs)
        if not fractional:
            def krav_fit(M):
                return (10**(0.87 * np.log10(M / 10**14.5 * h_scale / 0.7) +
                             12.42) * (0.7/h_scale)**hfac)
            ax.errorbar([xmin, xmax], [krav_fit(xmin), krav_fit(xmax)],
                        c=c1, ls='--', lw=lw, label="Kravtsov+ 2018 best-fit")

            def krav_fit(M):
                return (10**(0.75 * np.log10(M / 10**14.5 * h_scale / 0.7) +
                             12.52) * (0.7/h_scale)**hfac)
            ax.errorbar([xmin, xmax], [krav_fit(xmin), krav_fit(xmax)],
                        c=c1, ls='-', lw=lw,
                        label="Kravtsov+ 2018 +\n Gonzalez+ 2013")


def plot_sims_mstar_v_mass(ax, toPlot="total", simcol='#F09C48',
                           ms=6.5, mew=1.2, errlw=1.2, lw=2,
                           fractional=False, projected=False, incl_ICL=True,
                           BCG_radii=[30],
                           obs_dir="/data/vault/nh444/ObsData/",
                           h_scale=0.6774):
    import numpy as np

    TNG_Mmin, TNG_Mmax = 1e13, 1e15  # TNG fit "range of validity"

    if toPlot == "total" and not fractional and incl_ICL:
        # The halo masses and stellar masses tabulated in Barnes+ 17
        # match the plotted C-EAGLE relation in Bahe+ 17 much better
        # than the tabulated values in Bahe+ 17 so I'll use these.
        eagle = np.genfromtxt(obs_dir+"CEAGLE_all.csv", delimiter=",")
        ax.errorbar(10**eagle[:, 12]*0.6777/h_scale,
                    10**eagle[:, 20]*0.6777/h_scale,
                    marker='o', mfc=simcol, mec='none',
                    ls='none', ms=ms, mew=mew, label="C-EAGLE")

        def TNG(M): return 10**(0.84*np.log10(M/1e14) + 12.36)
        ax.errorbar([TNG_Mmin, TNG_Mmax], [TNG(TNG_Mmin), TNG(TNG_Mmax)],
                    c=simcol, ls='--', lw=lw,
                    label="IllustrisTNG")
    if toPlot == "BCG" and not fractional:
        if 30 in BCG_radii:
            def TNG(M): return 10**(0.49*np.log10(M/1e14) + 11.77)
            ax.errorbar([TNG_Mmin, TNG_Mmax], [TNG(TNG_Mmin), TNG(TNG_Mmax)],
                        c=simcol, ls='--', lw=lw,
                        label="IllustrisTNG ($> 30$ kpc)")
        if 50 in BCG_radii:
            eagle = np.genfromtxt(obs_dir+"stellar_mass/" +
                                  "CEAGLE_BCG_masses.csv", delimiter=",")
            ax.errorbar(10**eagle[:, 3]*0.6777/h_scale,
                        10**eagle[:, 1]*0.6777 / h_scale,
                        marker='o', c=simcol, mfc=simcol, mec='none',
                        mew=mew, ms=ms*0.8, ls='none',
                        label=r"C-EAGLE ($\mathrm{r_{2D}} < 50$ kpc)")
        if 100 in BCG_radii:
            def TNG(M): return 10**(0.59*np.log10(M/1e14) + 12.0)
            ax.errorbar([TNG_Mmin, TNG_Mmax], [TNG(TNG_Mmin), TNG(TNG_Mmax)],
                        c=simcol, ls='--', lw=lw,
                        label="IllustrisTNG ($> 100$ kpc)")
    if toPlot == "ICL" and not fractional:
        if 30 in BCG_radii:
            def TNG(M): return 10**(1.01*np.log10(M/1e14) + 12.01)
            ax.errorbar([TNG_Mmin, TNG_Mmax], [TNG(TNG_Mmin), TNG(TNG_Mmax)],
                        c=simcol, ls='--', lw=lw,
                        label="IllustrisTNG ($> 30$ kpc)")
        if 100 in BCG_radii:
            def TNG(M): return 10**(1.25*np.log10(M/1e14) + 11.74)
            ax.errorbar([TNG_Mmin, TNG_Mmax], [TNG(TNG_Mmin), TNG(TNG_Mmax)],
                        c=simcol, ls='--', lw=lw,
                        label="IllustrisTNG ($> 100$ kpc)")
    if toPlot == "sats" and not fractional:
        def TNG_bestfit(M):
            return 10**(1.14*np.log10(M/1e14) + 11.93)
        ax.errorbar([TNG_Mmin, TNG_Mmax],
                    [TNG_bestfit(TNG_Mmin), TNG_bestfit(TNG_Mmax)],
                    c=simcol, ls='--', lw=lw, label=r"IllustrisTNG")


def plot_mstar_v_redshift(basedir, simdirs, zoomdirs, zooms,
                          redshifts, M500lims_list=[[2.9e13, 1e16]],
                          mass_slope=None,
                          incl_ICL=True, do_fit=False,
                          obs_dir="/data/vault/nh444/ObsData/",
                          outfilename="stellar_mass_vs_redshift.pdf",
                          outdir="/data/curie4/nh444/project1/stars/",
                          scat_idx=None, labels=None, col=None, hlightcol=None,
                          ls=None, markers=None, ms=6.5, mew=1.2, errlw=1.2,
                          h_scale=0.7, for_paper=False, verbose=False):
    """Plot stellar mass within r500 versus redshift.

    The stellar mass is scaled by the slope of the stellar mass--total mass
    relation to take out the halo mass dependence.
    Arguments:
        M500min_list: List of minimum halo masses. Each sample is plotted
                      and/or fit separately.
        mass_slope: Slope of the Mstar,r500-M500 relation for the simulation.
                    If None, it is acquired from a simultaneous fit to the
                    mass and redshift dependence of Mstar,r500.
    """
    import numpy as np
    import matplotlib.pyplot as plt
    import h5py
    import os
    if do_fit:
        from linear_regression_two_predictors import linreg_two_predictors

    fout_base = "stellar_masses_"
    basedir = os.path.normpath(basedir)+"/"

    get_mass_slope = False
    if mass_slope is None:
        get_mass_slope = True
    if labels is None:
        labels = [None]*len(simdirs)
    if col is None:
        from palettable.colorbrewer.qualitative import Dark2_8 as pallette
        col = pallette.hex_colors
    if ls is None:
        ls = ['dashed', 'dotted', 'dashdot', 'solid'] + \
            ['solid']*(len(simdirs)-4)
    if markers is None:
        markers = ["D", "s", "o", "^", ">", "v", "<"]
    if scat_idx is None:
        # Plot scatter for everything by default.
        scat_idx = range(len(simdirs) * len(M500min_list))
    c = '0.6'

    Mpivot = 4.8e14
    zpivot = 1.6

    fig, ax = plt.subplots(figsize=(7, 7))  # width,height
    ax.set_ylabel(r"$\mathrm{M_{\star, r_{500}}(\frac{M_{500}}{10^{" +
                  r"{:.1f}".format(np.log10(Mpivot)) +
                  r"} M_{\odot}})^{-B}}$ [M$_{\odot}$]")
    ax.set_xlabel("$1 + z$")
    # Set limits to just below and just above min and max redshift.
    ax.set_xlim(min(redshifts + 1.0)-0.05, max(redshifts + 1.0)+0.05)
    # Set tick spacing to 0.2
    xmin, xmax = ax.get_xlim()
    ax.xaxis.set_ticks(np.arange(min(redshifts + 1.0),
                                 max(redshifts + 1.0), 0.2))
    ax.set_ylim(1e12, 3e13)
    ax.set_yscale('log')

    # Gonzalez+ 2013
    B_gonz = 0.52  # Slope of Mgas-M relation
    gonz = np.genfromtxt(obs_dir+"mass_fracs/" +
                         "Gonzalez_2013_stellar_gas_masses.csv",
                         delimiter=",")[:11, :]
    # Restrict to clusters in mass range.
    m_gonz = gonz[:, 14]*0.702*1e14/h_scale  # M500
    ind = np.where((m_gonz >= M500lims_list[0][0]) &
                   (m_gonz < M500lims_list[0][1]))[0]
    norm = (gonz[ind, 14]*0.702*1e14/h_scale / Mpivot)**-B_gonz
    ax.errorbar(gonz[ind, 1]+1.0,
                gonz[ind, 23]*gonz2chab*1e13*0.702/h_scale * norm,
                yerr=(gonz[ind, 23]*gonz2chab*1e13*0.702/h_scale * norm *
                      np.sqrt((gonz[ind, 24]/gonz[ind, 23])**2 +
                              (B_gonz*gonz[ind, 15]/gonz[ind, 14])**2)),
                c=c, mec=c, mfc='none', mew=mew, lw=errlw, marker='o', ms=ms,
                capsize=0, ls='none',
                label="Gonzalez+ 2013 ($B = "+"{:.2f}".format(B_gonz)+"$)")

#    chiu = np.genfromtxt(obs_dir+"mass_fracs/" +
#                         "Suhada2012_Chiu2016_XMM-BCS.csv", delimiter=",")
#    norm = (chiu[:, 21]*1e13*(0.7/h_scale)**2 / Mpivot)**B
#    ax.errorbar(chiu[:, 3]+1.0, chiu[:, 30]*1e12*0.7/h_scale * norm,
#                yerr=(chiu[:, 30]*1e12*(0.7/h_scale)**2 * norm *
#                      np.sqrt((chiu[:, 22]/chiu[:, 21])**2 +
#                      (B*chiu[:, 31]/chiu[:, 30])**2)),
#                marker='s', c=c, mec=c, mew=mew, lw=errlw,
#                capsize=0, ls='none', label="Chiu+16")

    # Chiu et al. 2018
    A_chiu = 4e12
    B_chiu = 0.8
    C_chiu = 0.05
    chiu = np.genfromtxt(obs_dir+"mass_fracs/"+"Chiu_2017.csv", delimiter=",")
    norm = (chiu[:, 3]*1e14*0.68/h_scale / Mpivot)**-B_chiu
    ax.errorbar(chiu[:, 1]+1.0, chiu[:, 7]*1e12*0.68/h_scale * norm,
                yerr=(chiu[:, 7]*1e12*0.68/h_scale * norm *
                      np.sqrt((chiu[:, 8]/chiu[:, 7])**2 +
                              (B_chiu*chiu[:, 4]/chiu[:, 3])**2)),
                marker='d', c=c, mec=c, ms=ms, mew=mew, lw=errlw,
                capsize=0, ls='none',
                label="Chiu+ 18 ($B = "+"{:.2f}".format(B_chiu)+"$)")
    x = np.array(ax.get_xlim())  # 1+z
    ax.errorbar(x, A_chiu * 0.68 / h_scale * ((1+x)/1.6)**C_chiu,
                c=c, ls='--', lw=3,
                label="Chiu+ 18")

#    # Decker 2019 MaDCoWS
#    Deck = np.genfromtxt(obs_dir+"stellar_mass/Decker18_MaDCoWS.csv",
#                         delimiter=",")
#    norm = (Deck[:, 14]*1e14*0.7/h_scale / Mpivot)**B
#    ax.errorbar(Deck[:, 13] + 1.0,
#                Deck[:, 17] * 1e12 * 0.7 / h_scale * norm,
#                yerr=(Deck[:, 17] * 1e12 * 0.7 / h_scale * norm *
#                      np.sqrt((Deck[:, 18] / Deck[:, 17])**2 +
#                              (B*Deck[:, 15] / Deck[:, 14])**2)),
#                marker='s', c=c, mec=c, mfc='none', ms=ms, mew=mew, lw=errlw,
#                capsize=0, ls='none', label='Decker+ 19')

    fit_labels = []
    fit_col = []
    for simidx, simdir in enumerate(simdirs):
        simdir = os.path.normpath(simdir)+"/"
        if verbose:
            print "Doing simdir", simdir
        for Midx, M500lims in enumerate(M500lims_list):
            idx = simidx * len(M500lims_list) + Midx
            for zidx, z in enumerate(redshifts):
                suffix = "{:.2f}".format(z)
                filename = basedir+simdir+fout_base+"z"+suffix.zfill(5)+".hdf5"
                if not os.path.exists(filename):
                    raise IOError("File does not exist: "+str(filename))
                with h5py.File(filename) as f:
                    # datasets names are MstarTotal, Mstar_r500, Mstar_BCG,
                    # Mstar_BCG_r500, Mstar_BCG_30kpc, Mstar_BCG_rcut,
                    # Mstar_sats, Mstar_sats_r500
                    M500 = f['M500'].value[:, 0] / h_scale
                    ind = np.where((M500 >= M500lims[0]) &
                                   (M500 < M500lims[1]))[0]
                    if incl_ICL:
                        Mstar = (f['Mstar_r500'][:, 0]) / h_scale
                    else:
                        Mstar = (f['Mstar_BCG_30kpc'][:, 0] +
                                 f['Mstar_sats_r500'][:, 0]) / h_scale
                    x = np.full_like(Mstar[ind], f.attrs['redshift']+1.0)
                    if zidx == 0:
                        # Initialise data arrays.
                        x_all = x  # 1+z
                        Mstar_all = Mstar[ind]
                        M500_all = M500[ind]
                        # Boolean mask - true if halo is secondary halo of zoom
                        secondary_halo = np.zeros_like(M500[ind], dtype=bool)
                    else:
                        x_all = np.concatenate((x_all, x))
                        Mstar_all = np.concatenate((Mstar_all, Mstar[ind]))
                        M500_all = np.concatenate((M500_all, M500[ind]))
                        secondary_halo = np.concatenate((
                                secondary_halo, np.zeros_like(M500[ind],
                                                              dtype=bool)))

                if zoomdirs[simidx] is not "":
                    for zoomidx, zoom in enumerate(zooms[simidx]):
                        filename = basedir + \
                            zoomdirs[simidx]+zoom+"/"+fout_base + \
                            "z"+suffix.zfill(5)+".hdf5"
                        if verbose:
                            print "Doing zoom", zoom
                        if not os.path.exists(filename):
                            print "Skipping zoom", zoom
                        else:
                            with h5py.File(filename) as f:
                                M500 = f['M500'].value[:, 0] / h_scale
                                ind = np.where((M500 >= M500lims[0]) &
                                               (M500 < M500lims[1]))[0]
                                grpnum = f['GroupNum'].value[ind, 0]
                                grp0 = np.array((grpnum == 0), dtype=bool)
                                if incl_ICL:
                                    Mstar = f['Mstar_r500'].value[:, 0] / h_scale
                                else:
                                    Mstar = (1.0 / h_scale *
                                             (f['Mstar_BCG_30kpc'].value[:, 0] +
                                              f['Mstar_sats_r500'].value[:, 0]))
                                if len(ind) > 0:
                                    x_all = np.concatenate(
                                            (x_all, np.full_like(
                                                    Mstar[ind],
                                                    f.attrs['redshift']+1.0)))
                                    Mstar_all = np.concatenate((Mstar_all, Mstar[ind]))
                                    M500_all = np.concatenate((M500_all, M500[ind]))
                                    secondary_halo = np.concatenate((secondary_halo, ~grp0))
            if do_fit or get_mass_slope:
                xlim = np.array([xmin, xmax])
                samples = linreg_two_predictors(
                            np.log10(M500_all / Mpivot),
                            np.log10(x_all / zpivot), np.log10(Mstar_all),
                            [12.7, 0.8, -0.1, 0.05], plot=False,
                            bounds=((11, 13), (0.1, 3.5), (-4, 4),
                                    (0.0, np.inf)),
                            )
                # Get the median and uncertainties from the 16th, 50th and 84th
                # percentiles of the samples in the marginalised distributions.
                logA, B, C, scat = map(
                        lambda v: (v[1], v[2]-v[1], v[1]-v[0]),
                        zip(*np.percentile(samples, [16, 50, 84], axis=0)))
                print "logA =", logA
                print "B =", B
                print "C =", C
                print "scat =", scat
                if get_mass_slope:
                    mass_slope = B[0]
            # Determine the legend label.
            if len(M500lims_list) > 1:
                label = (labels[simidx] + r" $M_{500} = " +
                         r"{:.1f}".format(M500lims_list[Midx][0] / 1e14) +
                         r"$--${:.1f}".format(M500lims_list[Midx][1] / 1e14) +
                         r" \times 10^{14} M_{\odot}$")
            else:
                label = labels[simidx]
            label += " ($B = "+"{:.2f}".format(mass_slope)+"$)"
            if idx in scat_idx:
                if do_fit:
                    # Add label to best-fit line below instead of the points.
                    scatter_label = None
                else:
                    scatter_label = label
                y = Mstar_all * (M500_all / Mpivot)**-mass_slope
                ax.plot(x_all[~secondary_halo], y[~secondary_halo],
                        marker=markers[idx], mew=mew, c=col[idx],
                        mfc=col[idx] if Midx == 0 else 'none',
                        mec=col[idx], ms=ms, ls='none',
                        zorder=4, label=scatter_label)
                if hlightcol is None:
                    c = col[idx]
                else:
                    c = col[idx]
                ax.plot(x_all[secondary_halo], y[secondary_halo],
                        marker=markers[idx], mew=mew, c=c,
                        mfc=c if Midx == 0 else 'none',
                        mec=c, ms=ms, ls='none',
                        zorder=4)

            if do_fit:
                # use a subset of the posterior samples to plot the
                # uncertainties on the best-fit line in data space.
                num = 10000  # size of subset to use.
                xbins = np.linspace(xlim[0], xlim[1], num=100)
                models = np.zeros((len(xbins), num))
                for j, i in enumerate(np.random.choice(np.arange(samples.shape[0]), num, replace=False)):
                    models[:, j] = 10**(samples[i, 0] + samples[i, 2]*np.log10(xbins / zpivot))
                # Plot the median and 68% confidence interval
                ax.fill_between(xbins, np.percentile(models, 16, axis=1),
                                np.percentile(models, 84, axis=1),
                                facecolor=col[idx], alpha=0.65,
                                zorder=4-Midx, label=label)
                # Make a custom legend where the label text has the fit colour.
                fit_labels.append(label)
                fit_col.append(col[idx])

# =============================================================================
#     if len(fit_labels) > 0:
#         from matplotlib.lines import Line2D
#         leg1 = ax.legend([Line2D([0], [0])] * len(fit_labels), fit_labels,
#                          markerscale=0, handlelength=0, handletextpad=0,
#                          loc='lower right', frameon=False,
#                          borderaxespad=1.0, numpoints=1, fontsize='small')
#         for i in range(len(fit_labels)):
#             leg1.get_texts()[i].set_color(fit_col[i])
#         ax.add_artist(leg1)
#     ax.legend(loc='upper left', frameon=False,
#               borderaxespad=1.0, numpoints=1, fontsize='small')
# =============================================================================
    ax.legend(loc='best', frameon=False,
              borderaxespad=1.0, numpoints=1, fontsize='x-small')
    plt.tight_layout()
    if not for_paper:
        plt.savefig(outdir+outfilename)
    else:
        plt.savefig(outdir+outfilename, bbox_inches='tight')
    print "Plot saved to", outdir+outfilename


def plot_msat_v_mass(basedir, simdirs, zoomdirs, zooms,
                     redshift, projected=False,
                     obs_dir="/data/vault/nh444/ObsData/",
                     outfilename="satellite_mass_vs_halo_mass.pdf",
                     outdir="/data/curie4/nh444/project1/stars/",
                     scat_idx=[], plot_med=True, labels=None,
                     col=None, hlightcol=None, ls=None, markers=None,
                     ms=8, mew=1.8, errlw=1.8,
                     label_zooms=False, for_paper=False,
                     h_scale=0.7, verbose=False):
    import numpy as np
    import matplotlib.pyplot as plt
    from scipy.stats import binned_statistic
    import h5py
    import os

    fout_base = "stellar_masses_"
    basedir = os.path.normpath(basedir)+"/"

    # For median profiles
    xmin, xmax = [1e13, 3e15]
    num = int((np.log10(xmax) - np.log10(xmin/10.0)) / 0.2)
    bins = np.logspace(np.log10(xmin/10.0), np.log10(xmax), num=num)
    x = 10**((np.log10(bins[:-1]) + np.log10(bins[1:]))/2)

    if col is None:
        from palettable.colorbrewer.qualitative import Dark2_8 as pallette
        col = pallette.hex_colors
    if ls is None:
        ls = ['dashed', 'dotted', 'dashdot', 'solid'] + \
            ['solid']*(len(simdirs)-4)
    if not for_paper:
        zcol = ["#c7673e", "#78a44f", "#6b87c8",
                "#934fce", "#E6AB02", "#be558f", "#47af8d"]
    if markers is None:
        markers = ["D", "s", "o", "^", ">", "v", "<"]
    lw = 2

    bias = 1.0  # mass bias factor, 1-b (M_X = (1-b)*M500)

    plt.figure(figsize=(7, 7))  # width,height
    plt.ylabel(r"$\mathrm{M}_{\mathrm{\star, sats}}$ [M$_{\odot}$]")
    plt.xlabel(r"M$_{500}$ [M$_{\odot}$]")
    plt.xlim(xmin, xmax)
    plt.ylim(8e10, 2e13)
    plt.xscale('log')
    plt.yscale('log')

    c = '0.6'

    krav = np.genfromtxt(obs_dir+"stellar_mass/" +
                         "kravtsov_sat_masses_from_plot.csv", delimiter=",")
    plt.errorbar(krav[:, 0]*0.7/h_scale, krav[:, 1]*(0.7/h_scale)**2,
                 yerr=[krav[:, 2]*(0.7/h_scale)**2,
                       krav[:, 3]*(0.7/h_scale)**2],
                 c=c, mfc='none', mec=c, marker='s', ls='none', ms=ms,
                 mew=mew, lw=errlw, label="Kravtsov+ 2018")

    def krav_fit(M): return 10**(0.87*np.log10(M/10**14.5) + 12.42)
    plt.errorbar([xmin, xmax], [krav_fit(xmin), krav_fit(xmax)],
                 c=c, ls='--', lw=lw, label="Kravtsov+ 2018 best-fit")

    def krav_fit(M): return 10**(0.75*np.log10(M/10**14.5) + 12.52)
    plt.errorbar([xmin, xmax], [krav_fit(xmin), krav_fit(xmax)],
                 c=c, ls='-', lw=lw, label="Kravtsov+ 2018 +\n Gonzalez+ 2013")
    xmin, xmax = plt.gca().get_xlim()

    def TNG(M): return 10**(1.14*np.log10(M/1e14) + 11.93)
    plt.errorbar([xmin, xmax], [TNG(xmin), TNG(xmax)], c='#F09C48',
                 ls='--', lw=lw, label=r"IllustrisTNG")  # ($> 10^8 M_{\odot}$)

    for j, simdir in enumerate(simdirs):
        if verbose:
            print "Doing simdir", simdir
        simdir = os.path.normpath(simdir)+"/"
        if labels is None:
            label = simdir[8:-1]
        else:
            label = labels[j]
        suffix = "{:.2f}".format(redshift)
        filename = basedir+simdir+fout_base+"z"+suffix.zfill(5)+".hdf5"
        if not os.path.exists(filename):
            raise IOError("Following file does not exist: "+str(filename))
        with h5py.File(filename) as f:
            # datasets names are MstarTotal, Mstar_r500, Mstar_BCG,
            # Mstar_BCG_r500, Mstar_BCG_30kpc, Mstar_BCG_rcut, Mstar_sats,
            # Mstar_sats_r500
            M500 = np.asarray(f['M500'][:, 0]) / h_scale
            if projected:
                Mstar = f['Mstar_sats_r500_proj'][:, 0] / h_scale
            else:
                Mstar = f['Mstar_sats_r500'][:, 0] / h_scale
            if plot_med:
                med, bin_edges, n = binned_statistic(
                    M500 * bias, Mstar, statistic='median', bins=bins)
                N, bin_edges, n = binned_statistic(
                    M500 * bias, Mstar, statistic='count', bins=bins)
                idx = np.min(np.where((N > 0) & (N < 10))[0])
                plt.plot(x[:idx+1], med[:idx+1], c=col[j], label=label,
                         lw=lw, ls='solid', dash_capstyle='round')
                plt.plot(x[idx:], med[idx:], c=col[j], lw=lw,
                         ls='dashed', dash_capstyle='round')

            if j in scat_idx or not plot_med:
                if plot_med:
                    label = None  # label should be added above instead
                plt.plot(M500 * bias, Mstar, marker=markers[j], mew=mew,
                         c=col[j], mfc='none', mec=col[j],
                         ms=ms, ls='none', label=label)

        if zoomdirs[j] is not "":
            if for_paper:
                zcol = [col[j]]*len(zooms[j])
            for zidx, zoom in enumerate(zooms[j]):
                if verbose:
                    print "Doing zoom", zoom
                if for_paper:
                    label = None
                else:
                    label = zoom.replace("_", r"\_")
                filename = basedir+zoomdirs[j]+zoom+"/" + \
                    fout_base+"z"+suffix.zfill(5)+".hdf5"
                if not os.path.exists(filename):
                    raise IOError("Following file does not exist: " +
                                  str(filename))
                with h5py.File(filename) as f:
                    grpnum = f['GroupNum'].value[:, 0]
                    grp0 = (grpnum == 0)
                    M500 = f['M500'].value[:, 0] / h_scale * bias
                    if projected:
                        Mstar = f['Mstar_sats_r500_proj'].value[:, 0] / h_scale
                    else:
                        Mstar = f['Mstar_sats_r500'].value[:, 0] / h_scale
                    # Plot group 0
                    plt.plot(M500[grp0], Mstar[grp0],
                             marker=markers[j], mec=zcol[zidx], mfc=zcol[zidx],
                             mew=mew, ms=ms, ls='none', label=label)
                    if label_zooms:
                        # label group 0
                        plt.annotate(zoom[:4].replace("_", ""),
                                     xy=(M500[grp0],
                                         Mstar[grp0]), xytext=(10, 0),
                                     textcoords='offset points',
                                     size='xx-small')
                    # plot groups >0 (secondary FoF haloes)
                    if hlightcol is None:
                        hlightcol = zcol[zidx]
                    plt.plot(M500[~grp0], Mstar[~grp0],
                             marker=markers[j], mec=hlightcol, mfc=hlightcol,
                             mew=mew, ms=ms, ls='none', label=label)
    plt.legend(loc='best', frameon=False, borderaxespad=1,
               numpoints=1, fontsize='small')
    if projected:
        plt.text(0.9, 0.1, r"r$_{\mathrm{2D}} \leq$ r$_{500}$",
                 ha='right', va='center', transform=plt.gca().transAxes)
    else:
        plt.text(0.9, 0.1, r"r$_{\mathrm{3D}} \leq$ r$_{500}$",
                 ha='right', va='center', transform=plt.gca().transAxes)
    if redshift > 0.0:
        plt.title("z = {:.1f}".format(redshift))

    if not for_paper:
        plt.savefig(outdir+outfilename)
    else:
        plt.savefig(outdir+outfilename, bbox_inches='tight')
    print "Plot saved to", outdir+outfilename


def plot_m10_vs_M100(basedir, simdirs, zoomdirs, zooms,
                     z=0, colour_by_mass=True, smooth=True,
                     outfilename="M10_vs_Mmax.pdf",
                     outdir="/data/curie4/nh444/project1/stars/",
                     for_paper=False, scat_idx=[], labels=None,
                     col=None, hlightcol=None, ls=None,
                     markers=None, ms=6.5, mew=1.2, errlw=1,
                     h_scale=0.7, verbose=False):
    """Plot stellar mass within 10kpc vs mass in 100 kpc.

    In analogy with Huang+ 2018, plot the stellar mass within a 2D aperture
    of radius 10 kpc against the stellar mass within 100 kpc for central
    galaxies (BCGs), colour coded by halo mass.
    Arguments:
        colour_by_mass: Colour the points according to their halo mass.
        smooth: For each point get a smoothed value for the halo mass using
                the LOESS method.
    """
    import matplotlib.pyplot as plt
    import numpy as np
    import h5py
    import os
    if smooth:
        from loess_2d import loess_2d

    fout_base = "stellar_masses_"
    basedir = os.path.normpath(basedir)+"/"

    if col is None:
        from palettable.colorbrewer.qualitative import Dark2_8 as pallette
        col = pallette.hex_colors
    if ls is None:
        ls = ['dashed', 'dotted', 'dashdot', 'solid'] + \
            ['solid']*(len(simdirs)-4)
    if not for_paper:
        zcol = ["#c7673e", "#78a44f", "#6b87c8",
                "#934fce", "#E6AB02", "#be558f", "#47af8d"]
    if markers is None:
        markers = ["D", "s", "o", "^", ">", "v", "<"]

    plt.figure(figsize=(7, 7))  # width,height
    plt.xlabel(r"$\mathrm{log \, M_{\star, \, 100 kpc}}$ [M$_{\odot}$]")
    plt.ylabel(r"$\mathrm{log \, M_{\star, \, 10 kpc}}$ [M$_{\odot}$]")
    plt.xlim(11.2, 12.6)
    plt.ylim(10.8, 12.2)

    if colour_by_mass:
        M500_all = []
        m10_all = []
        m100_all = []
    for j, simdir in enumerate(simdirs):
        if verbose:
            print "Doing simdir", simdir
        simdir = os.path.normpath(simdir)+"/"
        if labels is None:
            label = simdir[8:-1]
        else:
            label = labels[j]
        suffix = "{:.2f}".format(z)
        filename = basedir+simdir+fout_base+"z"+suffix.zfill(5)+".hdf5"
        if not os.path.exists(filename):
            raise IOError("File does not exist: "+str(filename))
        with h5py.File(filename) as f:
            # datasets names are MstarTotal, Mstar_r500, Mstar_BCG,
            # Mstar_BCG_r500, Mstar_BCG_30kpc, Mstar_BCG_rcut,
            # Mstar_sats, Mstar_sats_r500
            # Select only massive galaxies
            ind = np.where(
                    np.log10(f["Mstar_BCG"].value[:, 0] / h_scale) >= 11.4)[0]
            M500 = np.log10(f['M500'].value[ind, 0] / h_scale)
            m10 = np.log10(f["Mstar_BCG_10kpc_proj"].value[ind, 0] / h_scale)
            m100 = np.log10(f["Mstar_BCG_100kpc_proj"].value[ind, 0] / h_scale)

            if colour_by_mass:
                M500_all = np.concatenate((M500_all, M500))
                m10_all = np.concatenate((m10_all, m10))
                m100_all = np.concatenate((m100_all, m100))
            elif j in scat_idx:
                plt.plot(m100, m10, marker=markers[j], mew=mew,
                         c=col[j], mfc=col[j], mec=col[j], ms=ms,
                         ls='none', zorder=4, label=label)
        if zoomdirs[j] is not "":
            if for_paper:
                zcol = [col[j]]*len(zooms[j])
            for zidx, zoom in enumerate(zooms[j]):
                if verbose:
                    print "Doing zoom", zoom
                if for_paper:
                    label = None
                else:
                    label = zoom.replace("_", r"\_")
                filename = basedir+zoomdirs[j]+zoom+"/" + \
                    fout_base+"z"+suffix.zfill(5)+".hdf5"
                if not os.path.exists(filename):
                    raise IOError("File does not exist: "+str(filename))
                with h5py.File(filename) as f:
                    ind = np.where(np.log10(f["Mstar_BCG"].value[:, 0] /
                                            h_scale) >= 11.4)[0]
                    M500 = np.log10(f['M500'].value[ind, 0] / h_scale)
                    m10 = np.log10(f["Mstar_BCG_10kpc_proj"].value[ind, 0] /
                                   h_scale)
                    m100 = np.log10(f["Mstar_BCG_100kpc_proj"].value[ind, 0] /
                                    h_scale)
                    if hlightcol is None:
                        hlightcol = zcol[zidx]
                    if colour_by_mass:
                        M500_all = np.concatenate((M500_all, M500))
                        m10_all = np.concatenate((m10_all, m10))
                        m100_all = np.concatenate((m100_all, m100))
                    elif j in scat_idx:
                        grpnum = f['GroupNum'].value[ind, 0]
                        grp0 = (grpnum == 0)
                        # plot group 0
                        plt.plot(m100[grp0], m10[grp0], marker=markers[j],
                                 mec=zcol[zidx], mfc=zcol[zidx], mew=mew,
                                 ms=ms, ls='none', zorder=4, label=label)
                        # plot groups >0 (secondary FoF haloes)
                        plt.plot(m100[~grp0], m10[~grp0], marker=markers[j],
                                 mec=hlightcol, mfc=hlightcol, mew=mew,
                                 ms=ms, ls='none', zorder=3, label=label)

#    if density:
#        x = np.array(m100_all)
#        y = np.array(m10_all)
#        xmin = x.min()
#        xmax = x.max()
#        ymin = y.min()
#        ymax = y.max()
#        kernel = gaussian_kde(np.vstack([x, y]),
#                              bw_method='silverman')
#        X, Y = np.meshgrid(np.arange(xmin, xmax, 0.025),
#                           np.arange(ymin, ymax, 0.025))
#        positions = np.vstack([X.ravel(), Y.ravel()])
#
#        Z = np.reshape(kernel(positions).T, X.shape)
#
#        plt.gca().imshow(np.rot90(Z), cmap='Oranges',
#                         extent=[xmin, xmax, ymin, ymax])
#        plt.scatter(x, y, marker=markers[j],
#                    c=M500_all, cmap='Oranges')
    if colour_by_mass:
        if smooth:
            z, w = loess_2d(m100_all, m10_all, M500_all)
        else:
            z = M500_all
        plt.scatter(m100_all, m10_all, marker=markers[j],
                    c=z, cmap='Oranges')
        plt.colorbar()

    plt.legend(loc='upper left', frameon=False,
               borderaxespad=1, numpoints=1, fontsize='x-small')


def plot_mBCG_v_mass(basedir, simdirs, zoomdirs, zooms,
                     z=0, apsize=50, projected=False,
                     do_fit=False, M500min_fit=1e14,
                     obs_dir="/data/vault/nh444/ObsData/",
                     outfilename="BCG_mass_vs_halo_mass.pdf",
                     outdir="/data/curie4/nh444/project1/stars/",
                     for_paper=False, scat_idx=[], labels=None,
                     col=None, hlightcol=None, ls=None,
                     markers=None, ms=6.5, mew=1.2, errlw=1,
                     plot_med=True, h_scale=0.7, verbose=False):
    """Plot BCG stellar mass vs halo mass (M500).

    Args:
        apsize: radius of the aperture in pkpc e.g. 10, 30, 50, 100
    """
    import numpy as np
    import matplotlib.pyplot as plt
    from scipy.stats import binned_statistic
    import h5py
    import os

    assert int(apsize) in [10, 30, 50, 100], "apsize " + \
        str(apsize)+" not recognised."
    apsize = int(apsize)

    fout_base = "stellar_masses_"
    basedir = os.path.normpath(basedir)+"/"

    # For median profiles
    xmin, xmax = [1e13, 3e15]
    num = int((np.log10(xmax) - np.log10(xmin/10.0)) / 0.2)
    bins = np.logspace(np.log10(xmin/10.0), np.log10(xmax), num=num)
    x = 10**((np.log10(bins[:-1]) + np.log10(bins[1:]))/2)

    if col is None:
        from palettable.colorbrewer.qualitative import Dark2_8 as pallette
        col = pallette.hex_colors
    if ls is None:
        ls = ['dashed', 'dotted', 'dashdot', 'solid'] + \
            ['solid']*(len(simdirs)-4)
    if not for_paper:
        zcol = ["#c7673e", "#78a44f", "#6b87c8",
                "#934fce", "#E6AB02", "#be558f", "#47af8d"]
    if markers is None:
        markers = ["D", "s", "o", "^", ">", "v", "<"]
    lw = 3
    c1 = '0.6'
    c2 = '0.5'
    simcol = '#F09C48'  # orange

    bias = 1.0  # mass bias factor, 1-b (M_X = (1-b)*M500)

    fig, ax = plt.subplots(figsize=(7, 7))  # width,height
    plt.ylabel(r"$\mathrm{M}_{\mathrm{\star, BCG}}$ [M$_{\odot}$]")

    plt.xlabel(r"M$_{500}$ [M$_{\odot}$]")
    plt.xlim(xmin, xmax)
    plt.ylim(8e10, 1e13)
    plt.xscale('log')
    plt.yscale('log')

#    plot_obs_mstar_v_mass(ax, toPlot="BCG", c1=c1, c2=c2,
#                          ms=ms, mew=mew, errlw=errlw, lw=lw,
#                          fractional=False, projected=projected,
#                          BCG_radii=[apsize],
#                          obs_dir=obs_dir, h_scale=h_scale)
    # Load observational data.
    DeM = np.genfromtxt(obs_dir+"stellar_mass/DeMaio_2018.csv", delimiter=",")
    DeM_kwargs = {'c': c1, 'mec': c1, 'mew': mew, 'marker': 'd', 'ms': ms,
                  'lw': errlw, 'ls': 'none', 'label': "DeMaio+ 2018"}
    # Kravtsov+ 2018 (BCG + sats + ICL) z < 0.1
    krav = np.genfromtxt(obs_dir+"mass_fracs/" +
                         "kravtsov_stellar_masses2.csv", delimiter=",")
    krav_kwargs = {'c': c1, 'mfc': c1, 'mec': c1, 'mew': mew, 'marker': 's',
                   'lw': errlw, 'ms': ms, 'ls': 'none',
                   'label': "Kravtsov+ 2018"}
    TNG_Mmin, TNG_Mmax = 1e13, 1e15
    # fac_3D_to_2D = 1.2  # factor to scale 3D masses to approximate 2D masses

    # Plot observational data.
    # Conversion factor from M500 to M200 for concentration c=5
    M200toM500 = 0.72
    M500toM200 = 1.0 / M200toM500
    if apsize == 50:
        # Zhang+ 16 best-fit (individual measurements not tabulated)
        m = np.array([xmin, xmax]) * M500toM200 / 1.5e14
        plt.errorbar([xmin, xmax], 10**(11.6 + 0.3*np.log10(m)),
                     ls='--', lw=2, c=c2, label="Zhang+ 2016 best-fit")

        # Bellstedt+ 2016
        # C-EAGLE compare to Bellstedt+ 2016 with a 2D 50kpc radius aperture
        # based on Stott+ 2010, however it's not clear if Stott+10
        # meant 50kpc radius or diameter.
#        bell = np.genfromtxt(obs_dir+"stellar_mass/" +
#                             "Bellstedt_2016.csv", delimiter=",")
#        zlim = 0.2  # Restrict to redshift < zlim
#        ind = np.where((bell[:, 1] < zlim) | (bell[:, 2] < zlim))[0]
#        plt.errorbar(bell[ind, 8]*M200toM500*1e15*0.7/h_scale,
#                     bell[ind, 7]*1e12*(0.7/h_scale)**2,
#                     # xerr=bell[ind, 9] * M200toM500*1e15*0.7/h_scale,
#                     marker='o', c=c1, mec=c1, alpha=0.5, mew=mew, ms=ms,
#                     lw=errlw, ls='none',
#                     label="Bellstedt+ 2016 ($z < " +
#                           "{:.1f}".format(zlim)+"$)")

        if projected:
            plt.errorbar(krav[:, 5]*1e14*0.7/h_scale,
                         krav[:, 32]*1e11*(0.7/h_scale)**2,
                         # xerr=krav[:, 6]*1e14*0.7/h_scale,
                         **krav_kwargs)
        else:
            plt.errorbar(krav[:, 5]*1e14*0.7/h_scale,
                         krav[:, 33]*1e11*(0.7/h_scale)**2,
                         # xerr=krav[:, 6]*1e14*0.7/h_scale,
                         **krav_kwargs)

        plt.errorbar(DeM[:, 5]*1e14*0.697/h_scale,
                     DeM[:, 18]*1e11*(0.697/h_scale)**2,
                     # xerr=DeM[:, 6]*1e14*0.697/h_scale,
                     yerr=DeM[:, 19]*1e11*(0.697/h_scale)**2,
                     **DeM_kwargs)

        eagle = np.genfromtxt(obs_dir+"stellar_mass/" +
                              "CEAGLE_BCG_masses.csv", delimiter=",")
        plt.errorbar(10**eagle[:, 3]*0.6777/h_scale,
                     10**eagle[:, 1]*0.6777 / h_scale,
                     marker='o', c=simcol, mfc=simcol, mec='none',
                     mew=mew, ms=ms*0.8,
                     ls='none', label="C-EAGLE")

        # Gonzalez+ 2013 (BCG + sats + ICL) 0.05 < z < 0.12
#        gonz = np.genfromtxt(obs_dir + "mass_fracs/" +
#                             "Gonzalez_2013_stellar_gas_masses.csv",
#                             delimiter=",")
#        plt.errorbar(gonz[:11, 14]*0.702*1e14/h_scale,
#                     (gonz[:11, 23]*gonz2chab*1e13*(0.7/h_scale)**2 *
#                      gonz[:11, 5]/gonz[:11, 8]*0.4),
#                     xerr=[gonz[:11, 15]*0.702/h_scale*1e14,
#                           gonz[:11, 15]*0.702/h_scale*1e14],
#                     c=c, mec='none', marker='D', ls='none', lw=errlw,
#                     ms=ms*1.1, label="Gonzalez+ 2013")
    if apsize == 10:
        plt.errorbar(DeM[:, 5]*1e14*0.697/h_scale,
                     DeM[:, 16]*1e11*(0.697/h_scale)**2,
                     xerr=DeM[:, 6]*1e14*0.697/h_scale,
                     yerr=DeM[:, 17]*1e11*(0.697/h_scale)**2,
                     **DeM_kwargs)
    if apsize == 30:
        # Lidman+ 2012
        Lid = np.genfromtxt(obs_dir + "stellar_mass/" +
                            "Lidman_2012.csv", delimiter=",")
        ind = np.where(Lid[:, 1] <= 0.2)[0]
        plt.errorbar(Lid[ind, 2]*1e15*M200toM500*(0.7/h_scale),
                     Lid[ind, 13] * 1e12 * (0.7/h_scale)**2,
                     # xerr=[np.abs(Lid[ind, 4])*1e15*M200toM500*(0.7/h_scale),
                     #      Lid[ind, 3]*1e15*M200toM500*(0.7/h_scale)],
                     marker='x', ms=ms, mew=mew, ls='none', lw=errlw,
                     c=c1, mec=c1, mfc='none',
                     label="Lidman+ 2012 ($z < 0.2$)")

        # Zhang+ 2016 (32 kpc radius aperture)
        Zhang = np.genfromtxt(obs_dir+"stellar_mass/" +
                              "Zhang_2016.csv", delimiter=",")
        # Restrict to z<0.2
        ind = np.where(Zhang[:, 4] <= 0.2)[0]
        y = 10**Zhang[ind, 6]*(0.7/h_scale)**2
        yerr = np.array([y - (10**(Zhang[ind, 6]-Zhang[ind, 7]) *
                              (0.7/h_scale)**2),
                         (10**(Zhang[ind, 6]+Zhang[ind, 7]) *
                          (0.7/h_scale)**2) - y])
#        xerr = [((10**Zhang[ind, 2] - 10**(Zhang[ind, 2]-Zhang[ind, 3])) *
#                 M200toM500*0.7/h_scale),
#                (10**(Zhang[ind, 2]+Zhang[ind, 3]) -
#                 10**Zhang[ind, 2])*M200toM500*0.7/h_scale],
        xerr = None
        plt.errorbar(10**Zhang[ind, 2]*M200toM500*0.7/h_scale, y,
                     xerr=xerr, yerr=yerr,
                     c=c2, mec=c2, marker='D', ls='none', lw=errlw, ms=ms*0.9,
                     label="Zhang+ 2016 ($z < 0.2$)")
        # Zhang+ 16 best-fit (individual measurements not tabulated)
        m = np.array([xmin, xmax]) * M500toM200 / 1.5e14
        plt.errorbar([xmin, xmax], 10**(11.52 + 0.24*np.log10(m)),
                     ls='--', lw=2, c=c2, label="Zhang+ 2016 best-fit")

        # Bellstedt+ 2016
        # Zhang+ 16 say that a 30kpc radius aperture is comparable to the
        # SExtractor aperture used by Bellstedt+16. This seems to be
        # consistent with the results of Lin+ 2017
        bell = np.genfromtxt(obs_dir+"stellar_mass/" +
                             "Bellstedt_2016.csv", delimiter=",")
        zlim = 0.2  # Restrict to redshift < zlim
        ind = np.where((bell[:, 1] < zlim) | (bell[:, 2] < zlim))[0]
        plt.errorbar(bell[ind, 8]*M200toM500*1e15*0.7/h_scale,
                     bell[ind, 7]*1e12*(0.7/h_scale)**2,
                     # xerr=bell[ind, 9] * M200toM500*1e15*0.7/h_scale,
                     marker='o', c=c1, mec=c1, mfc='none', mew=mew, ms=ms,
                     lw=errlw, ls='none',
                     label="Bellstedt+ 2016 ($z < "+"{:.1f}".format(zlim)+"$)")

        if projected:
            plt.errorbar(krav[:, 5]*1e14*0.7/h_scale,
                         krav[:, 30]*1e11*(0.7/h_scale)**2,
                         # xerr=krav[:, 6]*1e14*0.7/h_scale,
                         **krav_kwargs)
        else:
            plt.errorbar(krav[:, 5]*1e14*0.7/h_scale,
                         krav[:, 31]*1e11*(0.7/h_scale)**2,
                         # xerr=krav[:, 6]*1e14*0.7/h_scale,
                         **krav_kwargs)

        # IllustrsTNG best-fit
        def TNG(M):
            y = 10**(0.49*np.log10(M/1e14) + 11.77)
            # if projected: y *= fac_3D_to_2D
            return y
        plt.errorbar(
                [TNG_Mmin, TNG_Mmax], [TNG(TNG_Mmin), TNG(TNG_Mmax)],
                c=simcol, ls='--', lw=2,
                label="IllustrisTNG (3D)" if projected else "IllustrisTNG")
        # label=r"IllustrisTNG ($\times${:.1f})".format(fac_3D_to_2D)

    if apsize == 100:
        plt.errorbar(DeM[:, 5]*1e14*0.697/h_scale,
                     DeM[:, 20]*1e11*(0.697/h_scale)**2,
                     xerr=DeM[:, 6]*1e14*0.697/h_scale,
                     yerr=DeM[:, 21]*1e11*(0.697/h_scale)**2,
                     **DeM_kwargs)

        def TNG(M): return 10**(0.59*np.log10(M/1e14) + 12.0)
        plt.errorbar([TNG_Mmin, TNG_Mmax], [TNG(TNG_Mmin), TNG(TNG_Mmax)],
                     c=simcol, ls='--', lw=2, label="IllustrisTNG $<100$ pkpc")

    for j, simdir in enumerate(simdirs):
        if verbose:
            print "Doing simdir", simdir
        simdir = os.path.normpath(simdir)+"/"
        if labels is None:
            label = simdir[8:-1]
        else:
            label = labels[j]
        suffix = "{:.2f}".format(z)
        filename = basedir+simdir+fout_base+"z"+suffix.zfill(5)+".hdf5"
        if not os.path.exists(filename):
            raise IOError("File does not exist: "+str(filename))
        if apsize == 10:
            key = "Mstar_BCG_10kpc"
        elif apsize == 30:
            key = "Mstar_BCG_30kpc"
        elif apsize == 50:
            key = "Mstar_BCG_50kpc"
        elif apsize == 100:
            key = "Mstar_BCG_100kpc"
        if projected:
            key += "_proj"
        with h5py.File(filename) as f:
            # datasets names are MstarTotal, Mstar_r500, Mstar_BCG,
            # Mstar_BCG_r500, Mstar_BCG_30kpc, Mstar_BCG_rcut,
            # Mstar_sats, Mstar_sats_r500
            M500 = f['M500'].value[:, 0] / h_scale * bias
            Mstar = f[key].value[:, 0] / h_scale
            if plot_med:
                med, bin_edges, n = binned_statistic(
                    M500, Mstar, statistic='median', bins=bins)
                N, bin_edges, n = binned_statistic(
                    M500, Mstar, statistic='count', bins=bins)
                idx = np.min(np.where((N > 0) & (N < 10))[0])
                plt.plot(x[:idx+1], med[:idx+1], c=col[j], label=label,
                         lw=lw, ls='solid', dash_capstyle='round')
                plt.plot(x[idx:], med[idx:], c=col[j], lw=lw,
                         ls='dashed', dash_capstyle='round')

            if j in scat_idx or not plot_med:
                if plot_med:
                    label = None  # label should be added above instead
                plt.plot(M500, Mstar, marker=markers[j], mew=mew,
                         c=col[j], mfc=col[j], mec=col[j], ms=ms,
                         ls='none', zorder=4, label=label)

        if zoomdirs[j] is not "":
            if for_paper:
                zcol = [col[j]]*len(zooms[j])
            for zidx, zoom in enumerate(zooms[j]):
                if verbose:
                    print "Doing zoom", zoom
                if for_paper:
                    label = None
                else:
                    label = zoom.replace("_", r"\_")
                filename = basedir+zoomdirs[j]+zoom+"/" + \
                    fout_base+"z"+suffix.zfill(5)+".hdf5"
                if not os.path.exists(filename):
                    raise IOError("File does not exist: "+str(filename))
                with h5py.File(filename) as f:
                    grpnum = f['GroupNum'].value[:, 0]
                    grp0 = (grpnum == 0)
                    M500z = f['M500'].value[:, 0] / h_scale * bias
                    M500 = np.concatenate((M500, M500z))
                    Mstarz = f[key].value[:, 0] / h_scale
                    Mstar = np.concatenate((Mstar, Mstarz))
                    # plot group 0
                    plt.plot(M500z[grp0], Mstarz[grp0], marker=markers[j],
                             mec=zcol[zidx], mfc=zcol[zidx], mew=mew,
                             ms=ms, ls='none', zorder=4, label=label)
                    # plot groups >0 (secondary FoF haloes)
                    if hlightcol is None:
                        hlightcol = zcol[zidx]
                    plt.plot(M500z[~grp0], Mstarz[~grp0], marker=markers[j],
                             mec=hlightcol, mfc=hlightcol, mew=mew,
                             ms=ms, ls='none', zorder=3, label=label)

        if do_fit:
            # Create the powerlaw function for a given pivot point X0
            from scaling.fit_relation import fit_powerlaw
            from scaling.fit_relation import make_powerlaw
            X0 = 3e14
            powerlaw = make_powerlaw(np.log10(X0))
            inislope = 0.3
            inilogC = 12.0
            ind = np.where(M500 >= M500min_fit)
            slope, slope_err, logC, logC_err, scat, scat_err = fit_powerlaw(
                M500[ind], Mstar[ind], X0, inislope, inilogC,
                bootstrap=True, verbose=verbose)
            if verbose:
                print "slope = {:.3f} [{:.3f}, {:.3f}]".format(slope, slope_err[0], slope_err[1])
                print "logC = {:.3f} [{:.3f}, {:.3f}]".format(logC, logC_err[0], logC_err[1])
                print "scat = {:.3f} [{:.3f}, {:.3f}]".format(scat, scat_err[0], scat_err[1])
            plt.plot([xmin, xmax],
                     [10**powerlaw(np.log10(xmin), slope, logC),
                      10**powerlaw(np.log10(xmax), slope, logC)],
                     c=col[j], lw=3)

    plt.legend(loc='upper left', frameon=False,
               borderaxespad=1, numpoints=1, fontsize='x-small')
    if projected:
        plt.text(0.9, 0.1,
                 r"r$_{\mathrm{2D}} \leq$"+" {:d} pkpc".format(apsize),
                 fontsize='small',
                 horizontalalignment='right', verticalalignment='center',
                 transform=plt.gca().transAxes)
    else:
        plt.text(0.9, 0.1,
                 r"r$_{\mathrm{3D}} \leq$"+" {:d} pkpc".format(apsize),
                 fontsize='small',
                 horizontalalignment='right', verticalalignment='center',
                 transform=plt.gca().transAxes)
    if z > 0.0:
        plt.title("z = {:.1f}".format(z))

    if not for_paper:
        plt.savefig(outdir+outfilename)
    else:
        plt.savefig(outdir+outfilename, bbox_inches='tight')
    print "Plot saved to", outdir+outfilename


def plot_mBCG_v_redshift(basedir, simdirs, zoomdirs, zooms,
                         redshifts, apsize=50, projected=False,
                         M500lim=[1e10, 1e18], M200lim=[1e10, 1e18],
                         scale_by_mass=False,
                         obs_dir="/data/vault/nh444/ObsData/",
                         outfilename="BCG_mass_vs_redshift.pdf",
                         outdir="/data/curie4/nh444/project1/stars/",
                         for_paper=False, join_points=True,
                         figsize=(7, 7), labels=None, ylims=(5e10, 1e13),
                         scat_idx=[],  med_idx=[], med_col=None,
                         col=None, hlightcol=None, ls=None,
                         markers=None, ms=6.5, mew=1.2, errlw=1.2,
                         h_scale=0.7, verbose=False):
    """ Plot BCG mass as a function of redshift.

    Arguments:
        apsize: radius of the aperture in pkpc e.g. 10, 30, 50, 100.
        scale_by_mass: Scale the BCG mass by the halo mass. The scaling
                       factor is chosen based on the aperture size.
    """
    import numpy as np
    import matplotlib.pyplot as plt
    import h5py
    import os

    if int(apsize) not in [10, 30, 50, 100]:
        raise ValueError("apsize " + str(apsize) + " not recognised.")
    apsize = int(apsize)
    redshifts = np.sort(redshifts)  # sort ascending

    fout_base = "stellar_masses_"
    basedir = os.path.normpath(basedir)+"/"

    if labels is None:
        labels = [None]*len(simdirs)
    if col is None:
        from palettable.colorbrewer.qualitative import Dark2_8 as pallette
        col = pallette.hex_colors
    if ls is None:
        ls = ['dashed', 'dotted', 'dashdot', 'solid'] + \
            ['solid']*(len(simdirs)-4)
    if not for_paper:
        zcol = ["#c7673e", "#78a44f", "#6b87c8",
                "#934fce", "#E6AB02", "#be558f", "#47af8d"]*3
    if markers is None:
        markers = ["D", "s", "o", "^", ">", "v", "<"]
    c1 = '0.6'  # X-ray only
    c2 = '0.5'  # other

    if scale_by_mass:
        # Get parameters for the halo mass scaling, which is
        # (M / Mpiv)**Mpower where M is either M500 or M200
        # for Mdelta = 500 or 200, respectively.
        if apsize == 30:
            Mpiv = 1.5e14
            Mdelta = 200
            Mpower = 0.24
        elif apsize == 100:
            Mpiv = 1e14
            Mdelta = 500
            Mpower = 0.37
        else:
            raise NotImplementedError("scale_by_mass is not yet implemented "
                                      "for aperture size: "+str(apsize))

    fig, ax = plt.subplots(figsize=figsize)
    if scale_by_mass:
        ax.set_ylabel(r"$\mathrm{M}_{\mathrm{\star, BCG}} (\frac{M_{" +
                      str(Mdelta) + r"}}{"+"{:.1f}".format(Mpiv/1e14) +
                      r"\times 10^{14} M_{\odot}})^{-"+str(Mpower) +
                      r"}$ [M$_{\odot}$]")
    else:
        ax.set_ylabel(r"$\mathrm{M}_{\mathrm{\star, BCG}}$ [M$_{\odot}$]")
    ax.set_xlabel("$1 + z$")
    ax.set_xlim(1.0+min(redshifts)-0.05, 1.0+max(redshifts)+0.05)
    ax.set_yscale('log')
    ax.set_ylim(ylims[0], ylims[1])

    # Define the marker styles for studies that appear multiple times.
    DeM_kwargs = {'c': c1, 'mec': c1, 'mew': mew, 'marker': 'd', 'ms': ms,
                  'lw': errlw, 'ls': 'none', 'label': "DeMaio+ 2018"}

    if apsize == 10:
        DeM = np.genfromtxt(obs_dir+"stellar_mass/" +
                            "DeMaio_2018.csv", delimiter=",")
        ax.errorbar(1.0 + DeM[:, 2], DeM[:, 16]*1e11*(0.697/h_scale)**2,
                    yerr=DeM[:, 17]*1e11*(0.697/h_scale)**2, **DeM_kwargs)
        # Lin+ 2013 (32kpc diameter aperture)
        Lin = np.genfromtxt(obs_dir+"stellar_mass/" +
                            "Lin2013_BCG_vs_z.csv", delimiter=",")
        ax.errorbar(1.0 + Lin[:, 0], Lin[:, 1]*1e11*(0.7/h_scale)**2,
                    yerr=[Lin[:, 2]*1e11*(0.7/h_scale)**2,
                          Lin[:, 3]*1e11*(0.7/h_scale)**2],
                    c=c2, mec=c2, marker='s', ls='none', lw=errlw, ms=ms,
                    label="Lin+ 2013")
    elif apsize == 30:
        # Lidman+ 2012
        Lid = np.genfromtxt(obs_dir + "stellar_mass/" +
                            "Lidman_2012.csv", delimiter=",")
        y = Lid[:, 13] * 1e12 * (0.7/h_scale)**2
        if scale_by_mass:
            assert Mdelta == 200
            y /= (Lid[:, 2]*1e15*(0.7/h_scale) / Mpiv)**Mpower
        ax.errorbar(1.0 + Lid[:, 1], y,
                    marker='x', ms=ms, mew=mew, ls='none', lw=errlw,
                    c=c1, mec=c1, mfc='none', label="Lidman+ 2012")

        # Zhang+ 2016 (32 kpc radius aperture)
        Zhang = np.genfromtxt(obs_dir+"stellar_mass/" +
                              "Zhang_2016.csv", delimiter=",")
        y = 10**Zhang[:, 6]*(0.7/h_scale)**2
        yerr = np.array([y - 10**(Zhang[:, 6]-Zhang[:, 7])*(0.7/h_scale)**2,
                         10**(Zhang[:, 6]+Zhang[:, 7])*(0.7/h_scale)**2 - y])
        if scale_by_mass:
            assert Mdelta == 200
            fac = (10**Zhang[:, 2]*(0.7/h_scale) / Mpiv)**Mpower
            y /= fac
            yerr /= fac
        ax.errorbar(1.0 + Zhang[:, 4], y, yerr=yerr,  # xerr=Zhang[:, 5],
                    c=c2, marker='D', ms=ms*0.8, mew=mew, ls='none', lw=errlw,
                    label="Zhang+ 2016")
        if scale_by_mass:
            # Zhang+ 16 best-fit
            xmin, xmax = ax.get_xlim()
            ax.errorbar([xmin, xmax], 10**(11.52-0.19*np.log10([xmin, xmax])),
                        c=c2, ls='--', lw=3, label="Zhang+ 2016 best-fit")

        # Bellstedt+ 16
        bell = np.genfromtxt(obs_dir+"stellar_mass/" +
                             "Bellstedt_2016.csv", delimiter=",")
        bell[np.isnan(bell[:, 1]), 1] = bell[np.isnan(bell[:, 1]), 2]
        y = bell[:, 7]*1e12*(0.7/h_scale)**2
        if scale_by_mass:
            assert Mdelta == 200
            y /= (bell[:, 8]*1e15*(0.7/h_scale) / Mpiv)**Mpower
        ax.errorbar(1.0 + bell[:, 1], y,
                    marker='o', ms=ms, mew=mew, ls='none', lw=errlw,
                    c=c1, mec=c1, mfc='none', label="Bellstedt+ 2016")

#        # Lin+ 2017
#        Lin = np.genfromtxt(obs_dir+"stellar_mass/Lin_2017.csv",
#                            delimiter=",")
#        y = Lin[:, 1]*1e11*(0.71/h_scale)**2
#        yerr_high = Lin[:, 2]*1e11*(0.71/h_scale)**2
#        yerr_low = Lin[:, 3]*1e11*(0.71/h_scale)**2
#        if scale_by_mass:
#            assert Mdelta == 200
#            fac = (Lin[:, 4]*1e14*(0.71/h_scale) / Mpiv)**Mpower
#            y /= fac
#            yerr_high /= fac
#            yerr_low /= fac
#        ax.errorbar(1.0 + Lin[:, 0], y, yerr=[yerr_low, yerr_high],
#                    marker='s', ms=ms*1.2, mew=mew, ls='none', lw=errlw,
#                    c=c2, mec=c2, mfc='none', label="Lin+ 2017")
    elif apsize == 50:
        DeM = np.genfromtxt(obs_dir+"stellar_mass/" +
                            "DeMaio_2018.csv", delimiter=",")
        ax.errorbar(1.0 + DeM[:, 2], DeM[:, 18]*1e11*(0.697/h_scale)**2,
                    yerr=DeM[:, 19]*1e11*(0.697/h_scale)**2, **DeM_kwargs)

        bell = np.genfromtxt(obs_dir+"stellar_mass/" +
                             "Bellstedt_2016.csv", delimiter=",")
        # Use photometric redshift where spectroscopic is not available.
        bell[np.isnan(bell[:, 1]), 1] = bell[np.isnan(bell[:, 1]), 2]
        ax.errorbar(1.0 + bell[:, 1], bell[:, 7]*1e12*(0.7/h_scale)**2,
                    marker='o', c=c1, mec=c1, ls='none', lw=errlw, ms=ms,
                    alpha=0.5, label="Bellstedt+2016")

    elif apsize == 100:
        DeM = np.genfromtxt(obs_dir+"stellar_mass/" +
                            "DeMaio_2018.csv", delimiter=",")
        ax.errorbar(1.0 + DeM[:, 2], DeM[:, 20]*1e11*(0.697/h_scale)**2,
                    yerr=DeM[:, 21]*1e11*(0.697/h_scale)**2, **DeM_kwargs)

    for simidx, simdir in enumerate(simdirs):
        if verbose:
            print "Doing simdir", simdir
        simdir = os.path.normpath(simdir)+"/"
        label = labels[simidx]
        if apsize == 10:
            key = "Mstar_BCG_10kpc"
        elif apsize == 30:
            key = "Mstar_BCG_30kpc"
        elif apsize == 50:
            key = "Mstar_BCG_50kpc"
        elif apsize == 100:
            key = "Mstar_BCG_100kpc"
        else:
            raise ValueError("HDF5 key unknown for apsize "+str(apsize))
        if projected:
            key += "_proj"

        if simidx in med_idx:
            # If we need to plot the median relation, save all of the
            # x and y values for the end. Each element is another list and
            # each element of that list corresponds to one redshift.
            x_all = []
            y_all = []
            M200_all = []

        for zidx, z in enumerate(redshifts):
            suffix = "{:.2f}".format(z)
            filename = basedir+simdir+fout_base+"z"+suffix.zfill(5)+".hdf5"
            if not os.path.exists(filename):
                raise IOError("File does not exist: " + str(filename))
            with h5py.File(filename) as f:
                # datasets names are MstarTotal, Mstar_r500, Mstar_BCG,
                # Mstar_BCG_r500, Mstar_BCG_30kpc, Mstar_BCG_rcut,
                # Mstar_sats, Mstar_sats_r500
                M500 = f['M500'].value[:, 0] / h_scale
                M200 = f['M200'].value[:, 0] / h_scale
                ind = np.where((M500 >= M500lim[0]) & (M500 <= M500lim[1]) &
                               (M200 >= M200lim[0]) & (M200 <= M200lim[1]))[0]
                Mstar = f[key][:, 0] / h_scale
                if scale_by_mass:
                    if Mdelta == 200:
                        y = Mstar[ind] / (M200[ind] / Mpiv)**Mpower
                    elif Mdelta == 500:
                        y = Mstar[ind] / (M500[ind] / Mpiv)**Mpower
                    else:
                        raise ValueError("Mdelta not recognised.")
                else:
                    y = Mstar[ind]

                if simidx in scat_idx and len(ind) > 0:
                    ax.plot(np.full_like(y, 1.0 + f.attrs['redshift']), y,
                            marker=markers[simidx], mew=mew, c=col[simidx],
                            mfc=col[simidx], mec=col[simidx], ms=ms, ls='none',
                            zorder=4, label=label)
                    label = None  # don't repeat for the other redshifts.

                if simidx in med_idx:
                    x_all.append(np.full_like(y, 1.0 + f.attrs['redshift']))
                    y_all.append(y)
                    M200_all.append(M200[ind])

        if zoomdirs[simidx] is not "":
            if for_paper:
                zcol = [col[simidx]]*len(zooms[simidx])
            for zoomidx, zoom in enumerate(zooms[simidx]):
                if for_paper:
                    label = None
                else:
                    label = zoom.replace("_", r"\_")
                if join_points:
                    BCG_subnum_next_snap = -1
                    prev_z = 0.0
                    prev_Mstar = 0.0
                for zidx, z in enumerate(redshifts):
                    suffix = "{:.2f}".format(z)
                    filename = basedir + \
                        zoomdirs[simidx]+zoom+"/"+fout_base + \
                        "z"+suffix.zfill(5)+".hdf5"
                    if verbose:
                        print "Doing zoom", zoom
                    if os.path.exists(filename):
                        with h5py.File(filename) as f:
                            grpnum = f['GroupNum'].value[:, 0]
                            M500 = f['M500'].value[:, 0] / h_scale
                            M200 = f['M200'].value[:, 0] / h_scale
                            Mstar = f[key].value[:, 0] / h_scale
                            if scale_by_mass:
                                if Mdelta == 200:
                                    y = Mstar / (M200 / Mpiv)**Mpower
                                elif Mdelta == 500:
                                    y = Mstar / (M500 / Mpiv)**Mpower
                                else:
                                    raise ValueError("Mdelta not recognised.")
                            else:
                                y = Mstar
                            ind = np.where((M500 >= M500lim[0]) &
                                           (M500 <= M500lim[1]) &
                                           (M200 >= M200lim[0]) &
                                           (M200 <= M200lim[1]))[0]
                            for i, idx in enumerate(ind):
                                if join_points and i == 0:
                                    BCG_subnum = f['BCG_subnum'][0, 0]
                                    if BCG_subnum == BCG_subnum_next_snap:
                                        # Current BCG is descendant of BCG
                                        # in previous snapshot.
                                        plt.plot([1.0 + prev_z,
                                                  1.0 + f.attrs['redshift']],
                                                 [prev_Mstar, y[idx]],
                                                 c=zcol[zoomidx], lw=2,
                                                 marker='o', mec=zcol[zoomidx],
                                                 mfc=zcol[zoomidx],
                                                 mew=mew, ms=5,
                                                 zorder=4, label=label)
                                        label = None
                                    else:
                                        ax.plot(
                                            1.0 + f.attrs['redshift'], y[idx],
                                            marker='o', mec=zcol[zoomidx],
                                            mfc=zcol[zoomidx], mew=mew, ms=5,
                                            ls='none')
                                    BCG_subnum_next_snap = f['BCG_subnum_next_snap'][0, 0]
                                    prev_z = f.attrs['redshift']
                                    prev_Mstar = y[idx]
                                elif int(grpnum[idx]) == 0:
                                    # Plot group 0
                                    ax.plot(1.0 + f.attrs['redshift'], y[idx],
                                            marker=markers[simidx],
                                            mec=zcol[zoomidx],
                                            mfc=zcol[zoomidx], mew=mew, ms=ms,
                                            ls='none', zorder=4, label=label)
                                else:
                                    # Use highlight colour for secondary haloes
                                    # if one is given.
                                    if hlightcol is None:
                                        hlightcol = zcol[zoomidx]
                                    ax.plot(1.0 + f.attrs['redshift'], y[idx],
                                            marker=markers[simidx],
                                            mec=hlightcol,
                                            mfc=hlightcol, mew=mew, ms=ms,
                                            ls='none', zorder=3, label=label)
                                if simidx in med_idx:
                                    x_all[zidx] = np.append(
                                        x_all[zidx], 1.0 + f.attrs['redshift'])
                                    y_all[zidx] = np.append(y_all[zidx],
                                                            y[idx])
                                    M200_all[zidx] = np.append(M200_all[zidx],
                                                               M200[idx])
        if simidx in med_idx:
            # Calculate median at each redshift and plot.
            x_med = []
            y_med = []
            M200_med = []
            for x, y, M in zip(x_all, y_all, M200_all):
                x_med.append(np.median(x))
                y_med.append(np.median(y))
                M200_med.append(np.median(M))
            ax.plot(x_med, y_med, marker='o', ms=ms*1.5, ls='none',
                    c=med_col, mfc='none', mew=mew*1.5, lw=2, zorder=5,
                    label="Median %s" % labels[simidx])
            # Fit a straight line to the median log(M) vs log(1+z)
            p = np.polyfit(np.log10(x_med), np.log10(y_med), 1)
            print "Slope =", p[0], "lognorm =", p[1]
            xmin, xmax = ax.get_xlim()
            ax.plot([xmin, xmax], 10**(p[1] + p[0]*np.log10([xmin, xmax])),
                    ls='--', lw=3, c=med_col, zorder=5)
            print "x =", x_med
            print "M200 =", M200_med

    ax.legend(loc='upper right', borderaxespad=1, ncol=3,
              numpoints=1, fontsize='small')
    if projected:
        ax.text(0.9, 0.1,
                r"r$_{\mathrm{2D}} \leq$"+" {:d} pkpc".format(apsize),
                fontsize='small', ha='right', va='center',
                transform=ax.transAxes)
    else:
        ax.text(0.9, 0.1,
                r"r$_{\mathrm{3D}} \leq$"+" {:d} pkpc".format(apsize),
                fontsize='small', ha='right', va='center',
                transform=ax.transAxes)
    plt.tight_layout()
    if not for_paper:
        fig.savefig(outdir+outfilename)
    else:
        fig.savefig(outdir+outfilename, bbox_inches='tight')
    print "Plot saved to", outdir+outfilename


def get_BCG_SFR(basedir, dirnames, apsize, **kwargs):
    """Return BCG SFRs for a halo mass limited sample at given redshift.

    Arguments:
        apsize: radius of the aperture in pkpc e.g. 10, 30, 50, 100.
        All other arguments are passed to get_BCG_props().
    """
    if int(apsize) not in [10, 30, 50, 100]:
        raise ValueError("apsize " + str(apsize) + " not recognised.")
    apsize = int(apsize)

    if apsize == 10:
        key = "SFR_BCG_10kpc"
    elif apsize == 30:
        key = "SFR_BCG_30kpc"
    elif apsize == 50:
        key = "SFR_BCG_50kpc"
    elif apsize == 100:
        key = "SFR_BCG_100kpc"
    else:
        raise ValueError("HDF5 key unknown for apsize " + str(apsize))

    return get_BCG_props(basedir, dirnames, key=key, **kwargs)


def get_BCG_props(basedir, dirnames, redshift=0.0, key="SFR_BCG_30kpc",
                  M500lim=[1e10, 1e18], M200lim=[1e10, 1e18],
                  fout_base="stellar_masses_", outsuffix="",
                  h_scale=0.6774, verbose=False):
    """Return BCG properties for a halo mass limited sample at given redshift.

    Arguments:
        basedir: where stellar mass hdf5 files are saved.
    """
    import numpy as np
    import h5py

    initialise = True
    for dirname in dirnames:
        # Get filename using the redshift rounded to two d.p.
        filename = get_filename(basedir, dirname, redshift,
                                fout_base=fout_base, outsuffix=outsuffix)
        if not filename:
            continue
        with h5py.File(filename) as f:
            M500 = f['M500'].value[:, 0] / h_scale
            M200 = f['M200'].value[:, 0] / h_scale
            ind = np.where((M500 >= M500lim[0]) & (M500 <= M500lim[1]) &
                           (M200 >= M200lim[0]) & (M200 <= M200lim[1]))[0]
            if len(ind) > 0:
                if initialise:
                    y_all = f[key].value[ind, 0]
                    M200_all = M200[ind]
                    M500_all = M500[ind]
                    initialise = False
                else:
                    y_all = np.concatenate((y_all, f[key].value[ind, 0]))
                    M200_all = np.concatenate((M200_all, M200[ind]))
                    M500_all = np.concatenate((M500_all, M500[ind]))
    if initialise:
        # Didn't get any haloes in specified mass range.
        return None, None, None
    else:
        return y_all, M200_all, M500_all


def get_filename(basedir, dirname, redshift, fout_base="stellar_masses_",
                 outsuffix=""):
    # Get filename using the redshift rounded to two d.p.
    basedir = os.path.normpath(basedir)+"/"
    dirname = os.path.normpath(dirname)+"/"
    for z in [redshift, redshift+0.01, redshift-0.01]:
        suffix = "{:.2f}".format(z)
        filename = (basedir + dirname + fout_base + "z" + suffix.zfill(5) +
                    outsuffix + ".hdf5")
        if os.path.exists(filename):
            break
        else:
            print "File does not exist:", str(filename)
    if not os.path.exists(filename):
        # raise IOError("Could not find file for redshift", redshift)
        return False

    return filename


def plot_ICL_v_mass(basedir, simdirs, zoomdirs, zooms,
                    redshift, fraction=False, projected=False,
                    obs_dir="/data/vault/nh444/ObsData/",
                    outfilename="ICL_mass_vs_halo_mass.pdf",
                    outdir="/data/curie4/nh444/project1/stars/",
                    for_paper=False,
                    scat_idx=[], labels=None, col=None, ls=None,
                    markers=None, ms=6.5,
                    plot_med=True, h_scale=0.7, verbose=False):
    """ Plot ICL mass or mass fraction as a function of halo mass.

    Arguments:
        fraction: If True, plot the fraction of mass in the ICL to the
                  total stellar mass, otherwise just plot the ICL mass.
    """
    import numpy as np
    import matplotlib.pyplot as plt
    from scipy.stats import binned_statistic
    import h5py
    import os

    fout_base = "stellar_masses_"
    basedir = os.path.normpath(basedir)+"/"
    # Set the HDF5 dataset name corresponding to the BCG mass
    # Possible datsets are:
    # Mstar_BCG_rcut, Mstar_BCG_10kpc, Mstar_BCG_30kpc,
    # Mstar_BCG_50kpc, Mstar_BCG_100kpc
    BCG_mass_keyword = 'Mstar_BCG_rcut'

    xmin, xmax = [1e13, 3e15]
    num = int((np.log10(xmax) - np.log10(xmin/10.0)) / 0.2)
    bins = np.logspace(np.log10(xmin/10.0), np.log10(xmax), num=num)
    x = 10**((np.log10(bins[:-1]) + np.log10(bins[1:]))/2)

    if col is None:
        from palettable.colorbrewer.qualitative import Dark2_8 as pallette
        col = pallette.hex_colors
    if ls is None:
        ls = ['dashed', 'dotted', 'dashdot', 'solid'] + \
            ['solid']*(len(simdirs)-4)
    if not for_paper:
        zcol = ["#c7673e", "#78a44f", "#6b87c8",
                "#934fce", "#E6AB02", "#be558f", "#47af8d"]
    if markers is None:
        markers = ["D", "s", "o", "^", ">", "v", "<"]
    mew = 1.2
    lw = 3

    bias = 1.0  # mass bias factor, 1-b (M_X = (1-b)*M500)

    plt.figure(figsize=(7, 7))  # width,height
    plt.ylabel(r"$\mathrm{M}_{\mathrm{\star, ICL}}$ [M$_{\odot}$]")

    plt.xlabel(r"M$_{500}$ [M$_{\odot}$]")
    plt.xlim(xmin, xmax)
    plt.xscale('log')
    if fraction:
        plt.ylim(0.0, 1.0)
    else:
        plt.ylim(8e10, 2e13)
        plt.yscale('log')

    xmin, xmax = plt.gca().get_xlim()

    if not fraction:
        def TNG(M): return 10**(1.01*np.log10(M/1e14) + 12.01)
        plt.errorbar([xmin, xmax], [TNG(xmin), TNG(xmax)], c='steelblue',
                     ls='dashdot', label="IllustrisTNG $>30$ pkpc")

    for j, simdir in enumerate(simdirs):
        if verbose:
            print "Doing simdir", simdir
        simdir = os.path.normpath(simdir)+"/"
        if labels is None:
            label = simdir[8:-1]
        else:
            label = labels[j]
        suffix = "{:.2f}".format(redshift)
        filename = basedir+simdir+fout_base+"z"+suffix.zfill(5)+".hdf5"
        if not os.path.exists(filename):
            raise IOError("File does not exist: "+str(filename))
        with h5py.File(filename) as f:
            # datasets names are MstarTotal, Mstar_r500, Mstar_BCG,
            # Mstar_BCG_r500, Mstar_BCG_30kpc, Mstar_BCG_rcut,
            # Mstar_sats, Mstar_sats_r500
            M500 = np.asarray(f['M500'][:, 0]) / h_scale
            if projected:
                M_ICL = (f['Mstar_BCG_r500_proj'][:, 0] -
                         f[BCG_mass_keyword+'_proj'][:, 0]) / h_scale
                Mstar_tot = f['Mstar_r500_proj'][:, 0] / h_scale
            else:
                M_ICL = (f['Mstar_BCG_r500'][:, 0] -
                         f[BCG_mass_keyword][:, 0]) / h_scale
                Mstar_tot = f['Mstar_r500'][:, 0] / h_scale
            if fraction:
                y = M_ICL / Mstar_tot
            else:
                y = M_ICL
            if plot_med:
                med, bin_edges, n = binned_statistic(
                    M500 * bias, y, statistic='median', bins=bins)
                N, bin_edges, n = binned_statistic(
                    M500 * bias, y, statistic='count', bins=bins)
                idx = np.min(np.where((N > 0) & (N < 10))[0])
                plt.plot(x[:idx+1], med[:idx+1], c=col[j], label=label,
                         lw=lw, ls='solid', dash_capstyle='round')
                plt.plot(x[idx:], med[idx:], c=col[j], lw=lw,
                         ls='dashed', dash_capstyle='round')

            if j in scat_idx or not plot_med:
                if plot_med:
                    label = None  # label should be added above instead
                plt.plot(M500 * bias, y,
                         marker=markers[j], mew=mew, c=col[j],
                         mfc='none', mec=col[j], ms=ms, ls='none', label=label)

        if zoomdirs[j] is not "":
            if for_paper:
                zcol = [col[j]]*len(zooms[j])
            for zidx, zoom in enumerate(zooms[j]):
                if verbose:
                    print "Doing zoom", zoom
                if for_paper:
                    label = None
                else:
                    label = zoom.replace("_", r"\_")
                filename = basedir+zoomdirs[j]+zoom+"/" + \
                    fout_base+"z"+suffix.zfill(5)+".hdf5"
                if not os.path.exists(filename):
                    raise IOError("File does not exist: "+str(filename))
                with h5py.File(filename) as f:
                    M500 = np.asarray(f['M500'][:, 0]) / h_scale
                    if projected:
                        M_ICL = (f['Mstar_BCG_r500_proj'][:, 0] -
                                 f[BCG_mass_keyword+'_proj'][:, 0]) / h_scale
                        Mstar_tot = f['Mstar_r500_proj'][:, 0] / h_scale
                    else:
                        M_ICL = (f['Mstar_BCG_r500'][:, 0] -
                                 f[BCG_mass_keyword][:, 0]) / h_scale
                        Mstar_tot = f['Mstar_r500'][:, 0] / h_scale
                    if fraction:
                        y = M_ICL / Mstar_tot
                    else:
                        y = M_ICL
                    plt.plot(M500 * bias, y,
                             marker=markers[j], mew=mew, ms=ms,
                             mec=zcol[zidx], mfc=zcol[zidx], ls='none',
                             label=label)

    plt.legend(loc='upper left', frameon=False,
               borderaxespad=1, numpoints=1, fontsize='small')
#    if projected:
#        plt.text(0.9, 0.1, r"r$_{\mathrm{2D}} >$ 30 pkpc", fontsize='small',
#                 horizontalalignment='right', verticalalignment='center',
#                 transform=plt.gca().transAxes)
#    else:
#        plt.text(0.9, 0.1, r"r$_{\mathrm{3D}} >$ 30 pkpc", fontsize='small',
#                 horizontalalignment='right', verticalalignment='center',
#                 transform=plt.gca().transAxes)
    if redshift > 0.0:
        plt.title("z = {:.1f}".format(redshift))

    if not for_paper:
        plt.savefig(outdir+outfilename)
    else:
        plt.savefig(outdir+outfilename, bbox_inches='tight')
    print "Plot saved to", outdir+outfilename


def plot_mratio_v_mass(basedir, simdirs, zoomdirs, zooms,
                       h5key1="Mstar_BCG_30kpc", h5key2="Mstar_BCG_30kpc_proj",
                       z=0, labels=None, for_paper=False,
                       outfilename="mass_ratio_vs_halo_mass.pdf",
                       outdir="/data/curie4/nh444/project1/stars/",
                       col=None, hlightcol=None, ls=None,
                       markers=None, ms=6.5, mew=1.2, errlw=1,
                       plot_med=True, h_scale=0.7, verbose=False):
    """Plot ratio between two stellar masses vs halo mass.

    Plot, as a function of halo mass, the ratio between the stellar masses
    corresponding to the HDF5 groups with the names h5key1 and h5key2.
    """
    import matplotlib.pyplot as plt
    import h5py
    import os
    if plot_med:
        import numpy as np
        from scipy.stats import binned_statistic

    fout_base = "stellar_masses_"
    basedir = os.path.normpath(basedir)+"/"

    if col is None:
        from palettable.colorbrewer.qualitative import Dark2_8 as pallette
        col = pallette.hex_colors
    if ls is None:
        ls = ['dashed', 'dotted', 'dashdot', 'solid'] + \
            ['solid']*(len(simdirs)-4)
    if not for_paper:
        zcol = ["#c7673e", "#78a44f", "#6b87c8",
                "#934fce", "#E6AB02", "#be558f", "#47af8d"]
    if markers is None:
        markers = ["D", "s", "o", "^", ">", "v", "<"]

    fig, ax = plt.subplots(figsize=(7, 7))  # width,height
    ax.set_ylabel(str(h5key1).replace("_", r"\_") + " / " +
                  str(h5key2).replace("_", r"\_"), fontsize='small')
    xmin, xmax = [1e13, 3e15]
    ax.set_xlabel(r"M$_{500}$ [M$_{\odot}$]")
    ax.set_xlim(xmin, xmax)
    ax.set_xscale('log')

    if plot_med:
        num = int((np.log10(xmax) - np.log10(xmin/10.0)) / 0.2)
        bins = np.logspace(np.log10(xmin/10.0), np.log10(xmax), num=num)
        x = 10**((np.log10(bins[:-1]) + np.log10(bins[1:]))/2)
        lw = 3
        M500_all = []
        ratio_all = []

    for j, simdir in enumerate(simdirs):
        if verbose:
            print "Doing simdir", simdir
        simdir = os.path.normpath(simdir)+"/"
        if labels is None:
            label = simdir[8:-1]
        else:
            label = labels[j]
        suffix = "{:.2f}".format(z)
        filename = basedir+simdir+fout_base+"z"+suffix.zfill(5)+".hdf5"
        if not os.path.exists(filename):
            raise IOError("File does not exist: "+str(filename))
        with h5py.File(filename) as f:
            # datasets names are MstarTotal, Mstar_r500, Mstar_BCG,
            # Mstar_BCG_r500, Mstar_BCG_30kpc, Mstar_BCG_rcut,
            # Mstar_sats, Mstar_sats_r500
            M500 = f['M500'].value[:, 0] / h_scale
            Mstar1 = f[h5key1].value[:, 0] / h_scale
            Mstar2 = f[h5key2].value[:, 0] / h_scale

            plt.plot(M500, Mstar1 / Mstar2, marker=markers[j], mew=mew,
                     c=col[j], mfc=col[j], mec=col[j], ms=ms,
                     ls='none', zorder=4, label=label)
            if plot_med:
                M500_all = np.concatenate((M500_all, M500))
                ratio_all = np.concatenate((ratio_all, Mstar1 / Mstar2))
        if zoomdirs[j] is not "":
            if for_paper:
                zcol = [col[j]]*len(zooms[j])
            for zidx, zoom in enumerate(zooms[j]):
                if verbose:
                    print "Doing zoom", zoom
                if for_paper:
                    label = None
                else:
                    label = zoom.replace("_", r"\_")
                filename = basedir+zoomdirs[j]+zoom+"/" + \
                    fout_base+"z"+suffix.zfill(5)+".hdf5"
                if not os.path.exists(filename):
                    raise IOError("File does not exist: "+str(filename))
                with h5py.File(filename) as f:
                    grpnum = f['GroupNum'].value[:, 0]
                    grp0 = (grpnum == 0)
                    M500 = f['M500'].value[:, 0] / h_scale
                    Mstar1 = f[h5key1].value[:, 0] / h_scale
                    Mstar2 = f[h5key2].value[:, 0] / h_scale
                    # plot group 0
                    plt.plot(M500[grp0], Mstar1[grp0] / Mstar2[grp0],
                             marker=markers[j],
                             mec=zcol[zidx], mfc=zcol[zidx], mew=mew,
                             ms=ms, ls='none', zorder=4, label=label)
                    # plot groups >0 (secondary FoF haloes)
                    if hlightcol is None:
                        hlightcol = zcol[zidx]
                    plt.plot(M500[~grp0], Mstar1[~grp0] / Mstar2[~grp0],
                             marker=markers[j],
                             mec=hlightcol, mfc=hlightcol, mew=mew,
                             ms=ms, ls='none', zorder=3, label=label)
                    if plot_med:
                        M500_all = np.concatenate((M500_all, M500))
                        ratio_all = np.concatenate((ratio_all,
                                                    Mstar1 / Mstar2))

    if plot_med:
        med, bin_edges, n = binned_statistic(
            M500_all, ratio_all, statistic='median', bins=bins)
        N, bin_edges, n = binned_statistic(
            M500_all, ratio_all, statistic='count', bins=bins)
        idx = np.min(np.where((N > 0) & (N < 10))[0])
        plt.plot(x[:idx+1], med[:idx+1], c=col[j], label=label,
                 lw=lw, ls='solid', dash_capstyle='round')
        plt.plot(x[idx:], med[idx:], c=col[j], lw=lw,
                 ls='dashed', dash_capstyle='round')

    plt.legend(loc='upper left', frameon=False,
               borderaxespad=1, numpoints=1, fontsize='x-small')


def plot_msat_v_redshift(basedir, simdirs, zoomdirs, zooms,
                         redshifts, M500min_list=[2.9e13],
                         obs_dir="/data/vault/nh444/ObsData/",
                         outfilename="satellite_mass_vs_redshift.pdf",
                         outdir="/data/curie4/nh444/project1/stars/",
                         scat_idx=[], labels=None, col=None, hlightcol=None,
                         ls=None, markers=None, ms=6.5, mew=1.2, errlw=1.2,
                         h_scale=0.6774, for_paper=False, verbose=False):
    """Plot satellite stellar mass within r500 versus redshift.
    """
    import numpy as np
    import matplotlib.pyplot as plt
    import h5py
    import os

    fout_base = "stellar_masses_"
    basedir = os.path.normpath(basedir)+"/"

    if labels is None:
        labels = [None]*len(simdirs)
    if col is None:
        from palettable.colorbrewer.qualitative import Dark2_8 as pallette
        col = pallette.hex_colors
    if ls is None:
        ls = ['dashed', 'dotted', 'dashdot', 'solid'] + \
            ['solid']*(len(simdirs)-4)
    if not for_paper:
        zcol = ["#c7673e", "#78a44f", "#6b87c8",
                "#934fce", "#E6AB02", "#be558f", "#47af8d"]
    if markers is None:
        markers = ["D", "s", "o", "^", ">", "v", "<"]
    M500min_list = list(M500min_list)

    fig, ax = plt.subplots(figsize=(7, 7))  # width,height
    ax.set_ylabel(r"$\mathrm{M_{\star, sat, r_{500}}}$ [10$^{12}$ M$_{\odot}$]")
    ax.set_xlabel("$z$")
    # Set limits to just below and just above min and max redshift.
    ax.set_xlim(min(redshifts)-0.05, max(redshifts)+0.05)
    # Set tick spacing to 0.2
    xmin, xmax = ax.get_xlim()
    ax.xaxis.set_ticks(np.arange(min(redshifts),
                                 max(redshifts), 0.2))
    #ax.set_ylim(1e12, 3e13)
    ax.set_yscale('log')

    for simidx, simdir in enumerate(simdirs):
        for Midx, M500min in enumerate(M500min_list):
            if verbose:
                print "Doing simdir", simdir
            simdir = os.path.normpath(simdir)+"/"
            idx = simidx * len(M500min_list) + Midx
            if len(M500min_list) > 1:
                label = (labels[simidx] + r" $M_{500} > " +
                         r"{:.1f}".format(M500min_list[Midx] / 1e14) +
                         r" \times 10^{14} M_{\odot}$")
            else:
                label = labels[simidx]
            for z in redshifts:
                suffix = "{:.2f}".format(z)
                filename = basedir+simdir+fout_base+"z"+suffix.zfill(5)+".hdf5"
                if not os.path.exists(filename):
                    raise IOError("File does not exist: "+str(filename))
                with h5py.File(filename) as f:
                    # datasets names are MstarTotal, Mstar_r500, Mstar_BCG,
                    # Mstar_BCG_r500, Mstar_BCG_30kpc, Mstar_BCG_rcut,
                    # Mstar_sats, Mstar_sats_r500
                    M500 = f['M500'].value[:, 0] / h_scale
                    ind = np.where(M500 >= M500min)[0]
                    Mstar = f['Mstar_sats_r500'][:, 0] / h_scale
                    y = Mstar[ind]
                    if simidx in scat_idx:
                        ax.plot(np.full_like(y, f.attrs['redshift']), y,
                                marker=markers[idx], mew=mew, c=col[idx],
                                mfc=col[idx] if Midx == 0 else 'none',
                                mec=col[idx], ms=ms, ls='none',
                                zorder=4, label=label)

                if zoomdirs[simidx] is not "":
                    if for_paper:
                        zcol = [col[idx]]*len(zooms[simidx])
                    for zidx, zoom in enumerate(zooms[simidx]):
                        filename = basedir + \
                            zoomdirs[simidx]+zoom+"/"+fout_base + \
                            "z"+suffix.zfill(5)+".hdf5"
                        if verbose:
                            print "Doing zoom", zoom
                        if for_paper:
                            label = None
                        else:
                            label = zoom.replace("_", r"\_")
                        if not os.path.exists(filename):
                            print "Skipping zoom", zoom
                        else:
                            with h5py.File(filename) as f:
                                M500 = f['M500'].value[:, 0] / h_scale
                                ind = np.where(M500 >= M500min)[0]
                                grpnum = f['GroupNum'].value[ind, 0]
                                grp0 = (grpnum == 0)
                                Mstar = f['Mstar_sats_r500'][:, 0] / h_scale
                                y = Mstar[ind]
                                # plot group 0
                                ax.plot(np.full_like(y[grp0],
                                                     f.attrs['redshift']),
                                        y[grp0], marker=markers[idx], mec=zcol[zidx],
                                        mfc=zcol[zidx] if Midx == 0 else 'none',
                                        mew=mew, ms=ms, ls='none',
                                        zorder=4, label=label)
                                # plot groups >0 (secondary FoF haloes)
                                if hlightcol is None:
                                    hlightcol = zcol[zidx]
                                ax.plot(np.full_like(y[~grp0],
                                                     f.attrs['redshift']),
                                        y[~grp0], marker=markers[idx],
                                        mec=zcol[zidx], mew=mew,
                                        mfc=zcol[zidx] if Midx == 0 else 'none',
                                        ms=ms, ls='none', zorder=3, label=label)

    ax.legend(loc='best', frameon=False, borderaxespad=1.0,
              numpoints=1, fontsize='small')
    plt.tight_layout()
    if not for_paper:
        plt.savefig(outdir+outfilename)
    else:
        plt.savefig(outdir+outfilename, bbox_inches='tight')
    print "Plot saved to", outdir+outfilename


def plot_mstarfrac_v_mass(basedir, simdirs, zoomdirs, zooms, snapnum, redshift,
                          obs_dir="/data/vault/nh444/ObsData/",
                          incl_ICL=True, outfilename="default",
                          outdir="/data/curie4/nh444/project1/stars/",
                          for_paper=False, scat_idx=[], labels=None,
                          col=None, mec=None, mfc=None, markers=None,
                          ms=7, ls=None, plot_med=True,
                          h_scale=0.7, verbose=False):
    """Plot stellar mass fraction vs halo mass."""
    import numpy as np
    import matplotlib.pyplot as plt
    from scipy.stats import binned_statistic
    import h5py

    z = redshift
    fout_base = "stellar_masses_"
    if outfilename == "default":
        if for_paper:
            outfilename = "stellar_mass_fractions_paper.pdf"
        else:
            outfilename = "stellar_mass_fractions.pdf"
    basedir = os.path.normpath(basedir)+"/"

    if incl_ICL:
        gonz = np.genfromtxt(
            obs_dir+"mass_fracs/"+"Gonzalez_2013_stellar_gas_masses.csv",
            delimiter=",")
        krav = np.genfromtxt(obs_dir+"mass_fracs/" +
                             "kravtsov_stellar_masses2.csv", delimiter=",")
        sand = np.genfromtxt(
            obs_dir+"mass_fracs/"+"Sanderson_2013_stellar_baryon_fracs.csv",
            delimiter=",")
        Bud = np.genfromtxt(obs_dir+"mass_fracs/" +
                            "Budzynski_stellar_masses.csv", delimiter=",")
        Ill = np.genfromtxt(obs_dir+"mass_fracs/" +
                            "Illustris_stellar_fracs.csv", delimiter=",")
    else:
        chiu = np.genfromtxt(obs_dir+"mass_fracs/" +
                             "Suhada2012_Chiu2016_XMM-BCS.csv", delimiter=",")
        chiu2 = np.genfromtxt(obs_dir+"mass_fracs/" +
                              "Chiu_2016_baryon_fracs.csv", delimiter=",")
        gio = np.genfromtxt(obs_dir+"mass_fracs/"+"giodini2009_star_fracs.txt")
        lin = np.genfromtxt(obs_dir+"mass_fracs/" +
                            "lin_mohr_table1.txt", usecols=range(0, 19))
        van = np.genfromtxt(obs_dir+"mass_fracs/" +
                            "van_der_burg_stellar_masses.csv", delimiter=",")

    plt.figure(figsize=(7, 7))  # width,height
    # plt.ylabel(r"$\mathrm{M}_{\star}/\mathrm{M}_{500}$")
    plt.ylabel(r"$\mathrm{M}_{\mathrm{star,}500}/\mathrm{M}_{500}$")
    plt.xlabel(r"M$_{500}$ [M$_{\odot}$]")
    plt.xlim(1e12, 2e15)
    plt.ylim(0.0, 0.08)
    for label in plt.gca().yaxis.get_ticklabels()[1::2]:
        label.set_visible(False)  # skip every other tick label
    plt.xscale('log')
    c = '0.7'
    ms = 7
    if z <= 0.15:
        if incl_ICL:
            # (BCG + sats + ICL) # 0.05 < z < 0.12
            plt.errorbar(gonz[:11, 14]*0.702*1e14/h_scale,
                         gonz[:11, 29]*gonz2chab,
                         xerr=[gonz[:11, 15]*0.702/h_scale*1e14,
                               gonz[:11, 15]*0.702/h_scale*1e14],
                         yerr=[gonz[:11, 30] * gonz2chab,
                               abs(gonz[:11, 31]*gonz2chab)],
                         c=c, mec='none', marker='o', ls='none',
                         ms=ms*1.1, label="Gonzalez+ 2013")
            # (BCG + sats + ICL) # z=0.0571, 0.0796, 0.0943
            plt.errorbar(sand[:, 3]*0.7*1e14/h_scale, sand[:, 7]*gonz2chab,
                         xerr=sand[:, 4]*0.7/h_scale*1e14,
                         yerr=sand[:, 8]*gonz2chab,
                         c=c, mec='none', marker='d', ls='none', ms=ms*1.2,
                         label="Sanderson+ 2013")
            # (BCG + sats + ICL) #z < 0.1 # '#00cc99'
            plt.errorbar(krav[:, 5]*1e14*0.7/h_scale,
                         (krav[:, 11]+krav[:, 14])/krav[:, 5]/100,
                         xerr=krav[:, 6]*1e14*0.7/h_scale,
                         yerr=(krav[:, 11]+krav[:, 14])/krav[:, 5]/100*(
                ((krav[:, 12]**2 + krav[:, 15]**2)**0.5 /
                 (krav[:, 11]+krav[:, 14]))**2)**0.5,
                    c=c, mec='none', marker='s', ls='none',
                    ms=ms, label="Kravtsov+ 2014")
            # (BCG + sats + ICL) # z=0.15 - 0.4
            plt.errorbar(Bud[:, 0]*0.71/h_scale, Bud[:, 9],
                         xerr=[Bud[:, 1]*0.71/h_scale, Bud[:, 2]*0.71/h_scale],
                         yerr=[Bud[:, 10], Bud[:, 11]], c='k',
                         mec='none', marker='o', ls='none', ms=ms,
                         label="Budzynski+ 2013")
            plt.errorbar(10**Ill[:, 0]*0.704/h_scale, Ill[:, 1],
                         dash_capstyle='round', dashes=(4, 6), lw=2.5,
                         c='0.5', label="Illustris")
        else:
            mask = (chiu[:, 3] < 0.15)  # redshift mask
            plt.errorbar(chiu[mask, 21]*1e13*0.7/h_scale,
                         chiu[mask, 30]/chiu[mask, 21]/10,
                         xerr=chiu[mask, 22]*1e13*0.7/h_scale,
                         yerr=(chiu[mask, 30] / chiu[mask, 21] / 10 *
                               ((chiu[mask, 31]/chiu[mask, 30])**2 +
                                (chiu[mask, 22]/chiu[mask, 21])**2)**0.5),
                         c=c, mec='none', marker='D', ls='none', ms=ms,
                         label="Chiu, Saro, Mohr+ 2016")
            # 0.016 < z < 0.09 # '#ff9cff'
            plt.errorbar(2.55e13*(lin[:, 2]**1.58)*0.7/h_scale,
                         lin[:, 16]*0.01*lin2chab,
                         yerr=[(lin[:, 18])*0.01,
                               (lin[:, 17])*0.01 * lin2chab],
                         c=c, mec='none', marker='D', ls='none', ms=ms,
                         label="Lin+ 2003 (BCG + sats)")
    if z >= 0.0 and z < 1.0:  # z = 0.1 - 1.0
        if not incl_ICL:
            # (BCG + sats) $0.1 < z < 1.0$ # '#6ea3ff' # errors are the st dev
            plt.errorbar(gio[:, 0]*0.72/h_scale, gio[:, 3]*salp2chab,
                         xerr=[(gio[:, 0]-gio[:, 1])*0.72/h_scale,
                               (gio[:, 2]-gio[:, 0])*0.72],
                         yerr=[(gio[:, 3]-gio[:, 4])*salp2chab,
                               (gio[:, 5]-gio[:, 3])*salp2chab],
                         c=c, mec='none', marker='^', ls='none', ms=ms,
                         label=r"Giodini+ 2009")
    if z >= 0.15 and z <= 0.4:
        if incl_ICL:
            plt.errorbar(Bud[:, 0]*0.71/h_scale, Bud[:, 9],
                         xerr=[Bud[:, 1]*0.71/h_scale,
                               Bud[:, 2]*0.71/h_scale],
                         yerr=[Bud[:, 10], Bud[:, 11]],
                         c='k', mec='none', marker='o', ls='none',
                         ms=ms, label="Budzynski+ 2013 (BCG + sats + ICL)")
    if z >= 0.1 and z <= 0.5:
        if not incl_ICL:
            mask = (chiu[:, 3] < 0.5)
            plt.errorbar(chiu[mask, 21]*1e13*0.7/h_scale,
                         chiu[mask, 30]/chiu[mask, 21]/10,
                         xerr=chiu[mask, 22]*1e13*0.7/h_scale,
                         yerr=(chiu[mask, 30] / chiu[mask, 21] / 10 *
                               ((chiu[mask, 31]/chiu[mask, 30])**2 +
                                (chiu[mask, 22]/chiu[mask, 21])**2)**0.5),
                         c=c, mec='none', marker='D', ls='none', ms=ms,
                         label="Chiu, Saro, Mohr+ 2016")
    if z >= 0.5 and z <= 0.8:
        if not incl_ICL:
            # (BCG + sats) 0.5 < z < 0.8
            mask = (chiu[:, 3] > 0.5) & (chiu[:, 3] < 0.8)
            plt.errorbar(chiu[mask, 21]*1e13*0.7/h_scale,
                         chiu[mask, 30]/chiu[mask, 21]/10,
                         xerr=chiu[mask, 22]*1e13*0.7/h_scale,
                         yerr=(chiu[mask, 30] / chiu[mask, 21] / 10 *
                               ((chiu[mask, 31]/chiu[mask, 30])**2 +
                                (chiu[mask, 22]/chiu[mask, 21])**2)**0.5),
                         c=c, mec='none', marker='D', ls='none', ms=ms,
                         label="Chiu, Saro, Mohr+ 2016")
    if z >= 0.8 and z <= 1.1:
        if not incl_ICL:
            # (BCG + sats) 0.8 < z < 1.0
            mask = (chiu[:, 3] > 0.8)
            plt.errorbar(chiu[mask, 21]*1e13*0.7/h_scale,
                         chiu[mask, 30]/chiu[mask, 21]/10,
                         xerr=chiu[mask, 22]*1e13*0.7/h_scale,
                         yerr=(chiu[mask, 30] / chiu[mask, 21] / 10 *
                               ((chiu[mask, 31]/chiu[mask, 30])**2 +
                                (chiu[mask, 22]/chiu[mask, 21])**2)**0.5),
                         c=c, mec='none', marker='D', ls='none', ms=ms,
                         label="Chiu, Saro, Mohr+ 2016")
    if z >= 0.6 and z <= 1.4:
        if not incl_ICL:
            # (BCG + sats) 0.6 < z < 1.4
            mask = (chiu2[:, 1] > 0.5)
            plt.errorbar(chiu2[mask, 2]*1e14*0.683/h_scale,
                         chiu2[mask, 10]/chiu2[mask, 2]/100,
                         xerr=chiu2[mask, 3]*1e14*0.683/h_scale,
                         yerr=(chiu2[mask, 10] / chiu2[mask, 2] / 100 *
                               ((chiu2[mask, 11]/chiu2[mask, 2])**2 +
                                (chiu2[mask, 3]/chiu2[mask, 2])**2)**0.5),
                         c=c, mec='none', marker='D', ls='none', ms=ms,
                         label="Chiu, Mohr, McDonald+ 2016")
    if z > 0.8 and z < 1.4:
        if not incl_ICL:
            # (BCG + sats) 0.86 < z 1.34
            # Convert M200 to M500 by factor 0.631 as in paper
            plt.errorbar(van[:, 7]*1e14*0.7/h_scale*0.631,
                         van[:, 17]/(van[:, 7]*0.631)/100,
                         xerr=[van[:, 8]*1e14*0.7/h_scale*0.631,
                               abs(van[:, 9])*1e14*0.7*0.631],
                         yerr=(van[:, 17] / (van[:, 7]*0.631) / 100 *
                               ((van[:, 18]/van[:, 17])**2 +
                                (van[:, 8]/van[:, 7])**2)**0.5),
                         c=c, mec='none', marker='D', ms=ms,
                         ls='none', label="van der Burg+ 2014")

    # For median profiles
    xmin, xmax = plt.gca().get_xlim()
    num = int((np.log10(xmax) - np.log10(xmin/10.0)) / 0.2)
    bins = np.logspace(np.log10(xmin/10.0), np.log10(xmax), num=num)
    x = 10**((np.log10(bins[:-1]) + np.log10(bins[1:]))/2)

    if col is None:
        from palettable.colorbrewer.qualitative import Dark2_8 as pallette
        col = pallette.hex_colors
    if mec is None:
        mec = col
    if mfc is None:
        mfc = ['none']*len(simdirs)
    if ls is None:
        ls = ['dashed', 'dotted', 'dashdot', 'solid'] + \
            ['solid']*(len(simdirs)-4)
    if markers is None:
        markers = ["D", "s", "o", "^", ">", "v", "<"]
    mew = 1.8
    lw = 3

    bias = 1.0  # mass bias factor, 1-b (M_X = (1-b)*M500)

    for j, simdir in enumerate(simdirs):
        if verbose:
            print "Doing simdir", simdir
        simdir = os.path.normpath(simdir)+"/"
        if labels is None:
            label = simdir[8:-1]
        else:
            label = labels[j]
        with h5py.File(basedir + simdir + fout_base +
                       str(snapnum).zfill(3) + ".hdf5") as f:
            # datasets names are MstarTotal, Mstar_r500, Mstar_BCG,
            # Mstar_BCG_r500, Mstar_BCG_30kpc, Mstar_BCG_rcut,
            # Mstar_sats, Mstar_sats_r500
            if incl_ICL:
                # total mass of stellar particles within r500
                MstarFrac = f['Mstar_r500'][:, 0] / f['M500'][:, 0] / bias
            else:
                # stellar mass of BCG within 30kpc plus satellites within r500
                MstarFrac = ((f['Mstar_BCG_30kpc'][:, 0] +
                             f['Mstar_sats_r500'][:, 0]) /
                             f['M500'][:, 0] / bias)
            if plot_med:
                med, bin_edges, n = binned_statistic(
                        f['M500'][:, 0] * bias, MstarFrac,
                        statistic='median', bins=bins)
                plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[j],
                         label=label, lw=lw, ls=ls[j], dash_capstyle='round')

            if j in scat_idx or not plot_med:
                if plot_med:
                    label = None  # label should be added above instead
                plt.plot(f['M500'][:, 0] / h_scale * bias, MstarFrac,
                         marker=markers[j], ms=ms, mew=mew,
                         mfc=mfc[j], mec=mec[j], ls='none', label=label)

        if zoomdirs[j] is not "":
            for zidx, zoom in enumerate(zooms[j]):
                if verbose:
                    print "Doing zoom", zoom
                if for_paper:
                    label = None
                    marker = markers[j]
                else:
                    label = zoom.replace("_", r"\_")
                    marker = markers[j]
                with h5py.File(basedir + zoomdirs[j] + zoom + "/" +
                               fout_base + str(snapnum).zfill(3)+".hdf5") as f:
                    if incl_ICL:
                        # total stellar mass of BCG within r500 plus
                        # satellites within r500.
                        MstarFrac = (1.0 / f['M500'][:, 0] / bias *
                                     (f['Mstar_BCG_r500'][:, 0] +
                                      f['Mstar_sats_r500'][:, 0]))
                    else:
                        # stellar mass of BCG within 30kpc plus
                        # satellites within r500.
                        MstarFrac = (1.0 / f['M500'][:, 0] / bias *
                                     (f['Mstar_BCG_30kpc'][:, 0] +
                                      f['Mstar_sats_r500'][:, 0]))
                    plt.plot(f['M500'][0] / h_scale * bias, MstarFrac[0],
                             marker=marker, mec=mec[j], mfc=mfc[j],
                             mew=mew, ms=ms, ls='none', label=label)

    if for_paper:
        plt.legend(loc='upper right', frameon=False, borderaxespad=1,
                   numpoints=1, ncol=1, fontsize='x-small')
    else:
        plt.legend(loc='upper left', frameon=False, numpoints=1, ncol=2)
    if z > 0.0:
        plt.title("z = {:.1f}".format(z))

    if not for_paper:
        plt.savefig(outdir+outfilename)
    else:
        plt.savefig(outdir+outfilename, bbox_inches='tight')
    print "Plot saved to", outdir+outfilename


def plot_mass_components_v_mass(basedir, simdirs, zoomdirs, zooms, redshift,
                                M500min=0.0, fractional=True,
                                obs_dir="/data/vault/nh444/ObsData/",
                                outdir="/data/curie4/nh444/project1/stars/",
                                outfilename="default",
                                markers=None, ms=6.5, labels=None,
                                h_scale=0.7, for_paper=False):
    """
    Plots the mass fraction of different components (BCG, ICL, satellites)
    using the output from stellar_mass.get_stellar_masses()
    """
    import numpy as np
    import matplotlib.pyplot as plt
    import h5py

    fout_base = "stellar_masses_"
    if outfilename == "default":
        outfilename = "stellar_mass_components.pdf"
    basedir = os.path.normpath(basedir)+"/"

    col = ["#c7673e", "#78a44f", "#6b87c8", "#e6ab02", "#be558f", "#47af8d"]
    if markers is None:
        markers = ["D", "s", "o", "^", ">", "v", "<"]

    fig, axes = plt.subplots(1, 2, figsize=(16, 7))
    for ax in axes:
        ax.set_xlabel(r"M$_{500}$ [M$_{\odot}$]")
        ax.set_xscale('log')
        if not fractional:
            ax.set_yscale('log')
    if fractional:
        axes[0].set_ylabel(r"BCG+ICL mass fraction for $< r_{500}$")
        axes[1].set_ylabel(r"Satellite mass fraction for $< r_{500}$")
        axes[0].set_ylim(0.0, 1.0)
        axes[1].set_ylim(0.0, 1.0)
    else:
        axes[0].set_ylabel(r"M$_{\star,\mathrm{BCG+ICL}}$ ($< r_{500}$)")
        axes[1].set_ylabel(r"M$_{\star,\mathrm{sat}}$ ($< r_{500}$)")

    c = '0.5'
    # if fractional:
    # Gonzalez 2013 L_BCG_ICL / L_Total - I'm not certain if this is the same as the mass ratio, since it doesn't seem to quite match Kravtsov+2014 fig 9 who also plot the mass ratio of Gonzalez+ 2013
    #gonz = np.genfromtxt(obs_dir+"mass_fracs/"+"Gonzalez_2013_stellar_gas_masses_Planck_cosmology.csv", delimiter=",")
    # axes[0].errorbar(gonz[:,10]*1e14*0.702/h_scale, gonz[:,4] / gonz[:,6], xerr=gonz[:,11]*0.702/h_scale*1e14, yerr=((gonz[:,5]/gonz[:,4])**2 + (gonz[:,7]/gonz[:,6])**2)**0.5 * gonz[:,4] / gonz[:,6], c=c, mec='none', marker='s', ls='none', ms=5, label="Gonzalez+ 2013")# 0.05 < z < 0.12
    KravAM = np.genfromtxt(obs_dir+"mass_fracs/" +
                           "Kravtsov2014_AM.csv", delimiter=",")
    KravAM_BCG = np.genfromtxt(
        obs_dir+"mass_fracs/"+"Kravtsov2014_AM_BCG-M500.csv", delimiter=",")
    krav_sat = np.genfromtxt(obs_dir+"stellar_mass/" +
                             "kravtsov_sat_masses_from_plot.csv", delimiter=",")
    krav_tot = np.genfromtxt(obs_dir+"stellar_mass/" +
                             "kravtsov_tot_masses_from_plot.csv", delimiter=",")
    if fractional:
        axes[0].errorbar(10**KravAM[:, 0]*0.7/h_scale, 10**KravAM[:, 1] /
                         10**KravAM[:, 3], c=c, lw=2, label="Kravtsov+ 2014 AM w/o scat")
        axes[1].errorbar(10**KravAM[:, 0]*0.7/h_scale, 10**KravAM[:, 2] /
                         10**KravAM[:, 3], c=c, lw=2, label="Kravtsov+ 2014 AM w/o scat")
        axes[1].errorbar(krav_sat[:, 0]*0.7/h_scale, krav_sat[:, 1]/krav_tot[:, 1], yerr=[krav_sat[:, 1]/krav_tot[:, 1]*np.sqrt((krav_sat[:, 2]/krav_sat[:, 1])**2+(krav_tot[:, 2]/krav_tot[:, 1])**2),
                                                                                          krav_sat[:, 1]/krav_tot[:, 1]*np.sqrt((krav_sat[:, 3]/krav_sat[:, 1])**2+(krav_tot[:, 3]/krav_tot[:, 1])**2)], c=c, mfc='none', mec=c, marker='s', ls='none', ms=ms, label="Kravtsov+ 2018")
        axes[0].errorbar(krav_sat[:, 0]*0.7/h_scale, 1.0-(krav_sat[:, 1]/krav_tot[:, 1]), yerr=[(1.0-(krav_sat[:, 1]/krav_tot[:, 1]))*np.sqrt((krav_sat[:, 2]/krav_sat[:, 1])**2+(krav_tot[:, 2]/krav_tot[:, 1])**2),
                                                                                                (1.0-(krav_sat[:, 1]/krav_tot[:, 1]))*np.sqrt((krav_sat[:, 3]/krav_sat[:, 1])**2+(krav_tot[:, 3]/krav_tot[:, 1])**2)], c=c, mfc='none', mec=c, marker='s', ls='none', ms=ms, label="Kravtsov+ 2018")
        puch = np.genfromtxt(obs_dir+"stellar_mass/" +
                             "Puchwein_2010_BCG_ICL.csv", delimiter=",")
        axes[0].errorbar(puch[:, 0]/h_scale, puch[:, 1],
                         marker='+', c=c, ls='none', label="Puchwein+ 2010")
        axes[1].errorbar(puch[:, 0]/h_scale, 1.0-puch[:, 1],
                         marker='+', c=c, ls='none', label="Puchwein+ 2010")

        gonz = np.genfromtxt(obs_dir+"stellar_mass/" +
                             "Gonzalez_2007.csv", delimiter=",")
        axes[0].errorbar(10**gonz[:, 6]*0.7/h_scale, gonz[:, 11], yerr=gonz[:, 12],
                         marker='o', c=c, mec='none', ls='none', label="Gonzalez 2007")
        axes[1].errorbar(10**gonz[:, 6]*0.7/h_scale, 1.0-gonz[:, 11], yerr=gonz[:, 12],
                         marker='o', c=c, mec='none', ls='none', label="Gonzalez 2007")

        Ill = np.genfromtxt(obs_dir+"mass_fracs/" +
                            "IllustrisTNG_BCG-ICL_fracs.csv", delimiter=",")
        # Halo masses are M200 so I divide by ~1.5
        axes[0].errorbar(Ill[:, 0]/1.5, Ill[:, 1],
                         c='darkblue', lw=3, label="TNG300")
        axes[1].errorbar(Ill[:, 0]/1.5, 1.0-Ill[:, 1],
                         c='darkblue', lw=3, label="TNG300")
    else:
        axes[0].errorbar(KravAM_BCG[:, 0]*0.7/h_scale, KravAM_BCG[:, 1] *
                         (0.7/h_scale)**2, c=c, lw=2, label="Kravtsov+ 2014 AM with scat")
        axes[1].errorbar(10**KravAM[:, 0]*0.7/h_scale, 10**KravAM[:, 2],
                         c=c, lw=2, label="Kravtsov+ 2014 AM w/o scat")
        axes[1].errorbar(krav_sat[:, 0]*0.7/h_scale, krav_sat[:, 1]*(0.7/h_scale)**2, yerr=[krav_sat[:, 2]*(0.7/h_scale) **
                                                                                            2, krav_sat[:, 3]*(0.7/h_scale)**2], c=c, mfc='none', mec=c, marker='s', ls='none', ms=ms, label="Kravtsov+ 2018")

    for j, simdir in enumerate(simdirs):
        simdir = os.path.normpath(simdir)+"/"
        if labels is None:
            label = simdir[8:-1]
        else:
            label = labels[j]
        suffix = "{:.2f}".format(redshift)
        filename = basedir+simdir+fout_base+"z"+suffix.zfill(5)+".hdf5"
        assert os.path.exists(
            filename), "Following file does not exist: "+filename
        with h5py.File(filename) as f:
            print "filename =", filename
            # datasets names are MstarTotal, Mstar_r500, Mstar_BCG, Mstar_BCG_r500, Mstar_BCG_30kpc, Mstar_BCG_rcut, Mstar_sats, Mstar_sats_r500
            ind = np.where(f['M500'][:, 0] / h_scale > M500min)[0]
            if fractional:
                #total_mass_r500 = f['Mstar_BCG_r500'][ind,0] + f['Mstar_sats_r500'][ind,0]
                total_mass_r500 = f['Mstar_r500'][:, 0]
                BCGICL = f['Mstar_BCG_r500'][ind, 0] / total_mass_r500
                Sat = f['Mstar_sats_r500'][ind, 0] / total_mass_r500
            else:
                BCGICL = f['Mstar_BCG_r500'][ind, 0] / h_scale
                Sat = f['Mstar_sats_r500'][ind, 0] / h_scale

            axes[0].plot(f['M500'][ind, 0] / h_scale, BCGICL, marker=markers[j], mew=1.3,
                         c=col[j], mfc='none', mec=col[j], ms=ms, ls='none', label=label)
            axes[1].plot(f['M500'][ind, 0] / h_scale, Sat, marker=markers[j], mew=1.3,
                         c=col[j], mfc='none', mec=col[j], ms=ms, ls='none', label=label)

        for zidx, zoom in enumerate(zooms[j]):
            if zoomdirs[j] is not "":
                filename = basedir+zoomdirs[j]+zoom+"/" + \
                    fout_base+"z"+suffix.zfill(5)+".hdf5"
                assert os.path.exists(
                    filename), "Following file does not exist: "+filename
                with h5py.File(filename) as f:
                    if fractional:
                        #total_mass_r500 = f['Mstar_BCG_r500'][:,0] + f['Mstar_sats_r500'][:,0]
                        total_mass_r500 = f['Mstar_r500'][:, 0]
                        BCGICL = f['Mstar_BCG_r500'][:, 0] / total_mass_r500
                        Sat = f['Mstar_sats_r500'][:, 0] / total_mass_r500
                    else:
                        BCGICL = f['Mstar_BCG_r500'][:, 0] / h_scale
                        Sat = f['Mstar_sats_r500'][:, 0] / h_scale
                    if for_paper:
                        axes[0].plot(f['M500'][0] / h_scale, BCGICL[0],
                                     marker=markers[j], mec=col[j], mfc=col[j], ms=ms, ls='none')
                        axes[1].plot(f['M500'][0] / h_scale, Sat[0], marker=markers[j],
                                     mec=col[j], mfc=col[j], ms=ms, ls='none')
                    else:
                        axes[0].plot(f['M500'][0] / h_scale, BCGICL[0], marker=markers[j],
                                     mec='none', ms=ms, ls='none', label=zoom.replace("_", "\_"))
                        axes[1].plot(f['M500'][0] / h_scale, Sat[0], marker=markers[j],
                                     mec='none', ms=ms, ls='none', label=zoom.replace("_", "\_"))

    #ax.legend(bbox_to_anchor=(1.04,1), loc='upper left', frameon=True, numpoints=1, fontsize=10, ncol=1)
    # plt.tight_layout(rect=[0,0,0.85,1])
    for ax in axes:
        ax.legend(loc='best', frameon=False, numpoints=1, fontsize='small')
    # plt.subplots_adjust(right=0.7)
    fig.savefig(outdir+outfilename, bbox_inches='tight')
    print "Plot saved to", outdir+outfilename


def plot_ICLfrac_v_mass(basedir, simdirs, zoomdirs, zooms, snapnum, obs_dir="/data/vault/nh444/ObsData/", outdir="/data/curie4/nh444/project1/stars/", zooms2=[], outfilename="default", scat_idx=[], labels=None, h_scale=0.7):
    """
    Plots ICL mass fraction using the output of stellar_mass.get_stellar_masses()
    """
    import numpy as np
    import matplotlib.pyplot as plt
    #from scipy.stats import binned_statistic
    import h5py

    fout_base = "stellar_masses_"
    if outfilename == "default":
        outfilename = "ICL_fractions.pdf"
    basedir = os.path.normpath(basedir)+"/"

    # For median profiles
    #xmin, xmax = plt.gca().get_xlim()
    #xmin, xmax = 1e12, 1e15
    #num = int((np.log10(xmax) - np.log10(xmin/10.0)) / 0.2)
    #bins = np.logspace(np.log10(xmin/10.0), np.log10(xmax), num=num)
    #x = 10**((np.log10(bins[:-1]) + np.log10(bins[1:]))/2)

    #from palettable.colorbrewer.qualitative import Dark2_8 as pallette
    # col=pallette.hex_colors
    col = ["#c7673e", "#78a44f", "#6b87c8", "#e6ab02", "#be558f", "#47af8d"]
    ms = 6.5

    fig, axes = plt.subplots(1, 2, figsize=(14, 7))  # width,height
    for ax in axes:
        # "M$_{\mathrm{ICL}}$/M$_{\star, 500}$"
        ax.set_ylabel(r"ICL mass fraction for $< r_{500}$")
        ax.set_xlabel(r"M$_{500}$ [M$_{\odot}$]")
        ax.set_xlim(1e13, 1e15)
        ax.set_ylim(0.0, 1.0)
        ax.set_xscale('log')
    axes[0].set_title("BCG: $r < 30$ kpc")
    axes[1].set_title("BCG: $r < r_{cut} \propto M_{200}^{0.29}$")

    krav = np.genfromtxt(obs_dir+"mass_fracs/" +
                         "Kravtsov2014.csv", delimiter=",")
    axes[0].errorbar(krav[:, 4]*1e14*0.7/h_scale, (krav[:, 8]*1e12 - krav[:, 19]*1e11)/(krav[:, 12]*1e12),
                     # approximate error including 0.2 dex uncertainty in M/L ratio
                     yerr=(10**0.2 * 2**0.5 + ((krav[:, 9]/krav[:, 8])**2 + (krav[:, 13]/krav[:, 12])**2)**0.5) * (
                         krav[:, 8]*1e12 - krav[:, 19]*1e11)/(krav[:, 4]*1e14),
                     c='0.5', marker='s', mec='none', ms=7, ls='none', label="Kravtsov+ 2014")

    for j, simdir in enumerate(simdirs):
        simdir = os.path.normpath(simdir)+"/"
        if labels is None:
            label = simdir[8:-1]
        else:
            label = labels[j]
        with h5py.File(basedir+simdir+fout_base+str(snapnum).zfill(3)+".hdf5") as f:
            # datasets names are MstarTotal, Mstar_r500, Mstar_BCG, Mstar_BCG_r500, Mstar_BCG_30kpc, Mstar_BCG_rcut, Mstar_sats, Mstar_sats_r500
            ICLFrac_30kpc = (f['Mstar_BCG_r500'][:, 0] -
                             f['Mstar_BCG_30kpc'][:, 0]) / f['Mstar_r500'][:, 0]
            ICLFrac_rcut = (f['Mstar_BCG_r500'][:, 0] -
                            f['Mstar_BCG_rcut'][:, 0]) / f['Mstar_r500'][:, 0]
            #med, bin_edges, n = binned_statistic(f['M500'][:,0], ICLFrac, statistic='median', bins=bins)
            #plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[j], label=label, lw=2, ls=ls[j], dash_capstyle='round')

            if j in scat_idx:
                axes[0].plot(f['M500'][:, 0] / h_scale, ICLFrac_30kpc, marker='o',
                             mew=1.3, c=col[j], mfc='none', mec=col[j], ms=ms, ls='none')
                axes[1].plot(f['M500'][:, 0] / h_scale, ICLFrac_rcut, marker='o',
                             mew=1.3, c=col[j], mfc='none', mec=col[j], ms=ms, ls='none')

        for zidx, zoom in enumerate(zooms):
            if zoomdirs[j] is not "":
                with h5py.File(basedir+zoomdirs[j]+zoom+"/"+fout_base+str(snapnum).zfill(3)+".hdf5") as f:
                    ICLFrac_30kpc = (
                        f['Mstar_BCG_r500'][:, 0] - f['Mstar_BCG_30kpc'][:, 0]) / f['Mstar_r500'][:, 0]
                    ICLFrac_rcut = (
                        f['Mstar_BCG_r500'][:, 0] - f['Mstar_BCG_rcut'][:, 0]) / f['Mstar_r500'][:, 0]
                    p0 = axes[0].plot(f['M500'][0] / h_scale, ICLFrac_30kpc[0], marker='D',
                                      mec='none', ms=9, ls='none', label=zoom.replace("_", "\_"))
                    p1 = axes[1].plot(f['M500'][0] / h_scale, ICLFrac_rcut[0], marker='D',
                                      mec='none', ms=9, ls='none', label=zoom.replace("_", "\_"))
                if zidx < len(zooms2):
                    with h5py.File(basedir+zoomdirs[j]+zooms2[zidx]+"/"+fout_base+str(snapnum).zfill(3)+".hdf5") as f:
                        ICLFrac_30kpc = (
                            f['Mstar_BCG_r500'][:, 0] - f['Mstar_BCG_30kpc'][:, 0]) / f['Mstar_r500'][:, 0]
                        ICLFrac_rcut = (
                            f['Mstar_BCG_r500'][:, 0] - f['Mstar_BCG_rcut'][:, 0]) / f['Mstar_r500'][:, 0]
                        axes[0].plot(f['M500'][0] / h_scale, ICLFrac_30kpc[0], marker='D', mew=1.5, mec=p0[0].get_color(
                        ), mfc='none', ms=8, ls='none', label=zooms2[zidx].replace("_", "\_"))
                        axes[1].plot(f['M500'][0] / h_scale, ICLFrac_rcut[0], marker='D', mew=1.5, mec=p1[0].get_color(
                        ), mfc='none', ms=8, ls='none', label=zooms2[zidx].replace("_", "\_"))

    axes[0].legend(loc='upper left', frameon=False,
                   numpoints=1, fontsize=10, ncol=2)
    fig.savefig(outdir+outfilename, bbox_inches='tight')
    print "Plot saved to", outdir+outfilename


def MBCG_outside_r500(basedir, simdirs, zoomdirs, zooms, snapnum, redshift, zooms2=[], obs_dir="/data/vault/nh444/ObsData/", outfilename="default", outdir="/data/curie4/nh444/project1/stars/", scat_idx=[], labels=None, col=None, ls=None, plot_med=True, h_scale=0.7, verbose=False):
    """
    Plots ratio of BCG stellar mass within r500 to total stellar mass using the output of stellar_mass.get_stellar_masses()
    zooms2 is another list of zooms which are plotted with the same colours as zooms but with open rather than solid symbols
    """
    import numpy as np
    import matplotlib.pyplot as plt
    from scipy.stats import binned_statistic
    import h5py

    z = redshift
    fout_base = "stellar_masses_"
    if outfilename == "default":
        outfilename = "BCG_mass_outside_r500.pdf"
    basedir = os.path.normpath(basedir)+"/"

    plt.figure(figsize=(7, 7))  # width,height
    plt.ylabel(r"$\mathrm{M}_{\star}/\mathrm{M}_{500}$")
    plt.xlabel(r"M$_{500}$ [M$_{\odot}$]")
    plt.xlim(1e12, 2e15)
    plt.xscale('log')

    # For median profiles
    xmin, xmax = plt.gca().get_xlim()
    num = int((np.log10(xmax) - np.log10(xmin/10.0)) / 0.2)
    bins = np.logspace(np.log10(xmin/10.0), np.log10(xmax), num=num)
    x = 10**((np.log10(bins[:-1]) + np.log10(bins[1:]))/2)

    if col is None:
        from palettable.colorbrewer.qualitative import Dark2_8 as pallette
        col = pallette.hex_colors
    if ls is None:
        ls = ['dashed', 'dotted', 'dashdot', 'solid'] + \
            ['solid']*(len(simdirs)-4)
    zcol = ["#c7673e", "#78a44f", "#6b87c8",
            "#934fce", "#E6AB02", "#be558f", "#47af8d"]
    ms = 6.5
    mew = 1.3
    lw = 3

    bias = 1.0  # mass bias factor, 1-b (M_X = (1-b)*M500)

    for j, simdir in enumerate(simdirs):
        if verbose:
            print "Doing simdir", simdir
        simdir = os.path.normpath(simdir)+"/"
        if labels is None:
            label = simdir[8:-1]
        else:
            label = labels[j]
        with h5py.File(basedir+simdir+fout_base+str(snapnum).zfill(3)+".hdf5") as f:
            # datasets names are MstarTotal, Mstar_r500, Mstar_BCG, Mstar_BCG_r500, Mstar_BCG_30kpc, Mstar_BCG_rcut, Mstar_sats, Mstar_sats_r500
            MstarFrac = f['Mstar_BCG_r500'][:, 0] / f['Mstar_BCG'][:, 0]

            if plot_med:
                med, bin_edges, n = binned_statistic(
                    f['M500'][:, 0] * bias, MstarFrac, statistic='median', bins=bins)
                plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[j],
                         label=label, lw=lw, ls=ls[j], dash_capstyle='round')

            if j in scat_idx or not plot_med:
                if plot_med:
                    label = None  # label should be added above instead
                plt.plot(f['M500'][:, 0] / h_scale * bias, MstarFrac, marker='o', mew=mew,
                         c=col[j], mfc='none', mec=col[j], ms=ms, ls='none', label=label)
                print MstarFrac

        Nzooms = len(zooms)
        if zoomdirs[j] is not "":
            for zidx, zoom in enumerate(np.concatenate((zooms, zooms2))):
                if verbose:
                    print "Doing zoom", zoom
                label = zoom.replace("_", "\_")
                marker = 'D'
                with h5py.File(basedir+zoomdirs[j]+zoom+"/"+fout_base+str(snapnum).zfill(3)+".hdf5") as f:
                    MstarFrac = f['Mstar_BCG_r500'][:, 0] / \
                        f['Mstar_BCG'][:, 0]
                    if zidx < Nzooms:
                        plt.plot(f['M500'][0] / h_scale * bias, MstarFrac[0], marker=marker,
                                 mec=zcol[zidx], mfc='none', mew=mew, ms=ms, ls='none', label=label)
                    else:  # from zooms2 list
                        plt.plot(f['M500'][0] / h_scale * bias, MstarFrac[0], marker=marker,
                                 mec='none', mfc=zcol[zidx - Nzooms], mew=0.5, ms=ms, ls='none', label=label)

    plt.legend(loc='upper right', frameon=False, borderaxespad=1,
               numpoints=1, ncol=1, fontsize='x-small')
    if z > 0.0:
        plt.title("z = {:.1f}".format(z))

    # plt.savefig(outdir+outfilename)
    #print "Plot saved to", outdir+outfilename


def list_stellar_masses(basedir, simdirs, snapnum, h_scale=0.7):
    import h5py

    fout_base = "stellar_masses_"
    basedir = os.path.normpath(basedir)+"/"

    for j, simdir in enumerate(simdirs):
        simdir = os.path.normpath(simdir)+"/"
        assert os.path.exists(basedir+simdir+fout_base +
                              str(snapnum).zfill(3)+".hdf5")
        with h5py.File(basedir+simdir+fout_base+str(snapnum).zfill(3)+".hdf5") as f:
            M200 = f['M200'][0][0] / h_scale
            M500 = f['M500'][0][0] / h_scale
            Mstar_BCG = f['Mstar_BCG'][:, 0][0] / h_scale
            Mstar_BCG_rcut = f['Mstar_BCG_rcut'][:, 0][0] / h_scale
            Mstar_ICL = Mstar_BCG - Mstar_BCG_rcut

        #print M200, M500, Mstar_BCG, Mstar_BCG_rcut, Mstar_ICL
        print ','.join('{:.5}'.format(k)
                       for k in [M200, M500, Mstar_BCG, Mstar_BCG_rcut, Mstar_ICL])


if __name__ == "__main__":
    main()
