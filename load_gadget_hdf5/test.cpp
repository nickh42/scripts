/* This is a test C++ script for running load_gadget_hdf5.c from C++ */

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include "table.h"
#include "load_gadget_hdf5.h"

  int load_mass ();
  int load_pos ();
  int load_z ();

  //header data
  int ntot,npart[6],nall[6]; 
  int cooling,sfr,feedback,filenum;
  double masstab[6],redshift,Time;
  double boxsize,omega_m,omega_l,hubble;

  struct my_vector
  {
    float x,y,z;
  };

  float *mass, *z;
  struct my_vector *pos;
  const char *fname = "/data/emergence3/deboras/Elliptical_ZoomIns/L40_512_NoDutyRadioIntLow_HalfThermal_FixEta_BHChangeVel/snap_025.hdf5";
  bool mpc_lengths = false;

int main () {
  read_gadget_head_full_hdf5(npart,masstab,&Time,&redshift,&sfr,&feedback,nall,
                        &cooling,&filenum,&boxsize,&omega_m,&omega_l,&hubble, fname);
  ntot = 0;
  for(int i=0; i<6; i++) ntot += nall[i];

  mass = 0;
  load_mass();
  cout << "Mass[0] = " << mass[0] << endl;

  pos = 0;
  load_pos();
  for (int i = 0; i<10; i++) {
    cout << "pos[i].x = " << pos[i].x << "\tpos[i].y = " << pos[i].y << "\tpos[i].z = " << pos[i].z << "\n";
  }

  z=0;
  load_z();
  for (int i = 0; i<10; i++) {
    cout << "z[i] = " << z[i] << "\n";
  }

  return 0;
}

int load_pos ()
{
  int coordinates_read;
  coordinates_read = read_gadget_hdf5(&pos,"POS ", fname);
  if (mpc_lengths) for (int i=0; i<ntot; ++i) {pos[i].x *= 1000.0; pos[i].y *= 1000.0; pos[i].z *= 1000.0;}
  return coordinates_read;
}

int load_z ()
{
  return read_gadget_hdf5(&z,"Z   ", fname);
}

int load_mass ()
{
  mass = new float[ntot];
  float *temp_mass = 0;
  int result = 0;  

  bool need_massblock = false;
  for(int k=0; k<6; k++) if (nall[k]!=0 && masstab[k]==0) need_massblock = true;
  if (need_massblock) result = read_gadget_hdf5(&temp_mass,"MASS",fname);

  double mass_in_species; 

  int i = 0, m = 0;
  for(int k=0; k<6; k++)
  {
    mass_in_species = 0;
    
    for(int j=0;j<nall[k];j++)
    {
      if (masstab[k]!=0) mass[i] = masstab[k];
      else {mass[i] = temp_mass[m]; ++m;}
      mass_in_species += mass[i];

      if(j==0 || j==npart[k]-1) cout << "mass = " << mass[i] << endl; 

      ++i;
    }

    cout << "mass of all particles of species " << k << " = " << mass_in_species << endl;  
  }   
   
  free(temp_mass);
  
  return result;
}

