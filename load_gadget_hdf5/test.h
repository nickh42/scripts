#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <hdf5.h>

struct my_vector
{
  float x,y,z;
};

extern "C" void read_header_struct_hdf5(const char *fname);
extern "C" void read_gadget_head_full_hdf5(int *npart,double *massarr,double *time,double *redshift,
                          int *sfr, int *feedback, int *nall, int *cooling, int *filenum,   
                          double *boxsize, double *omega_m, double *omega_l, double *hubble, const char *fname);
extern "C" void get_dataset_name(const char *label, char *buf);
extern "C" int read_gadget_hdf5_float(float **data, const char *label, const char *fname);

#ifdef __cplusplus
template <typename datatype>
int read_gadget_hdf5 (datatype **data_inout, const char *label, const char *fname)
{
	float **data = (float**) data_inout;
	int floats_read=0;
	floats_read = read_gadget_hdf5_float(data, label, fname);

	return floats_read;
}
#endif
