#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <hdf5.h>
#include "load_gadget.h"

struct SubfStruct {
  //halo information from subfind
  int subf_Len, subf_Offset, subf_ContaminationCount, subf_Nsubs, subf_FirstSub;
  float subf_Mass, subf_M_Mean200, subf_R_Mean200, subf_M_Crit200, subf_R_Crit200, subf_M_Crit500, subf_R_Crit500;
  float subf_M_TopHat200, subf_R_TopHat200, subf_ContaminationMass;
  struct my_vector subf_Pos;
  int subhalo_num, *subhalo_sorted_ids, ids_num;
  bool have_subf_halo_info;
  int subf_info_group;
};

extern "C" void read_header_struct_hdf5(const char *fname);
extern "C" void read_gadget_head_full_hdf5(int *npart,double *massarr,double *time,double *redshift,
                          int *sfr, int *feedback, int *nall, int *cooling, int *filenum,   
                          double *boxsize, double *omega_m, double *omega_l, double *hubble, const char *fname);
extern "C" void get_dataset_name(const char *label, char *buf);
extern "C" int read_gadget_hdf5_float(float **data, const char *label, const char *fname);
extern "C" void get_halo_information_subf_hdf5 (struct SubfStruct *subf, int group, bool get_center, const char *fname, int subhalo, int *offset, int *length);
extern "C" void allocate_Int2D(int*** arr, int nrows, int ncols);
extern "C" void deallocate_Int2D(int*** arr, int nrows);

/* In C++ the following function will take any datatype as */
#ifdef __cplusplus
template <typename datatype>
int read_gadget_hdf5 (datatype **data_inout, const char *label, const char *fname)
{
	//float **data = (float**) data_inout;
	int floats_read=0;
	floats_read = read_gadget_hdf5_float((float**) data_inout, label, fname);

	//data_inout = (datatype*) data; //necessary?

	return floats_read;
}
#endif
