#include "load_gadget_hdf5.h"

#ifdef __cplusplus
extern "C" {
#else
//// define bool if not using C++ compiler ///
typedef int bool;
#define true 1
#define false 0
#endif

const int NTYPES = 6;
int have_header = 0;

/// Header struct for the standard file format ///
struct io_header
{
  int npart[6];			/**< number of particles of each type in this file */
  double mass[6];		/**< mass of particles of each type. If 0, then the masses are explicitly
				   stored in the mass-block of the snapshot file, otherwise they are omitted */
  double time;			/**< time of snapshot file */
  double redshift;		/**< redshift of snapshot file */
  int flag_sfr;			/**< flags whether the simulation was including star formation */
  int flag_feedback;		/**< flags whether feedback was included (obsolete) */
  unsigned int npartTotal[6];	/**< total number of particles of each type in this snapshot. This can be
				   different from npart if one is dealing with a multi-file snapshot. */
  int flag_cooling;		/**< flags whether cooling was included  */
  int num_files;		/**< number of files in multi-file snapshot */
  double BoxSize;		/**< box-size of simulation in case periodic boundaries were used */
  double Omega0;		/**< matter density in units of critical density */
  double OmegaLambda;		/**< cosmological constant parameter */
  double HubbleParam;		/**< Hubble parameter in units of 100 km/sec/Mpc */
  int flag_stellarage;		/**< flags whether the file contains formation times of star particles */
  int flag_metals;		/**< flags whether the file contains metallicity values for gas and star
				   particles */
  unsigned int npartTotalHighWord[6];	/**< High word of the total number of particles of each type */
  int flag_entropy_instead_u;	/**< flags that IC-file contains entropy instead of u */
  int flag_doubleprecision;	/**< flags that snapshot contains double-precision instead of single precision */

  int flag_lpt_ics;		/**< flag to signal that IC file contains 2lpt initial conditions */
  float lpt_scalingfactor;	/**< scaling factor for 2lpt initial conditions */

  int flag_tracer_field;	/**< flags presence of a tracer field */

  int composition_vector_length;	/**< specifies the length of the composition vector (0 if not present)  */

//#if (NTYPES==6)
  char fill[40];		/**< fills to 256 Bytes */
//#elif (NTYPES==7)
  //char fill[8];                 /**< fills to 256 Bytes */
//#endif
}
header;				/**< holds header for snapshot files */

#ifdef __cplusplus
}
#endif

/*
/// Test ///
int main ()
{
	// Snapshot filename
	const char *fname = "/data/emergence3/deboras/Elliptical_ZoomIns/L40_512_NoDutyRadioIntLow_HalfThermal_FixEta_BHChangeVel/snap_025.hdf5";
	const char *label = "MASS";
	float *data;
	int i;
	int floats_read = 0;

	int ntot,npart[6],nall[6]; 
	int cooling,sfr,feedback,filenum;
	double masstab[6],redshift,time;
	double boxsize,omega_m,omega_l,hubble;
	read_gadget_head_full_hdf5(npart,masstab,&time,&redshift,&sfr,&feedback,nall,
                        &cooling,&filenum,&boxsize,&omega_m,&omega_l,&hubble,fname);
	printf("npart[0] = %d\nnall[0] = %d\nmasstab[0] = %f\n", npart[0], nall[0], masstab[0]);
	printf("time = %f\nredshift = %f\nboxsize = %f\n", time, redshift, boxsize);


	/// Read header into header struct ///
	read_header_struct_hdf5(fname);
	floats_read = read_gadget_hdf5_float(&data, label, fname);
	printf("Read %d floats.\n", floats_read);
	for (i=0; i<10; i++) printf("data[%d] = %.10f\n", i, data[i]);
	int n = (266591122*3) - 10;
	//for (i=n; i<n+10; i++) printf("data[%d] = %.10f\n", i, data[i]);
	free(data);
	return 0;
}
*/

int read_gadget_hdf5_float(float **data, const char *label, const char *fname)
{
	int rank, i=0;
	hid_t hdf5_file = 0, hdf5_grp[NTYPES], hdf5_dataspace_in_file, hdf5_dataspace_in_memory;
	hid_t hdf5_datatype = 0, hdf5_dataset, dataspace, hdf5_class;
	hsize_t dims[2];
	size_t datasize;
	int type = 0;
	int N=0;//floats read
	char buf[500];
	char dataset_name[50];
	int curgroup_pos=0;
	unsigned int values_per_particle = 1;

	/// If header not yet read, read it ///
	if (have_header == 0) {read_header_struct_hdf5(fname); have_header=1;}

  	/// Decide if label exists for each particle type ///
  	bool data_for_type[6];
  	for (i=0; i<6; ++i) data_for_type[i] = false;
	
	/// Save current error handler ///
	//herr_t (*old_func)(void*);
	//void *old_client_data;
	//H5Eget_auto1(&old_func, &old_client_data);
	/// Turn off error handling ///
	H5Eset_auto1(NULL,NULL);
	/// Restore previous error handler (remember to save original as above) ///
	//H5Eset_auto1(old_func, old_client_data);

	/// Check file extension ///
	//if (strcmp(strrchr(fname, '.'), ".hdf5") != 0) {
		//printf("OOPS wrong extension ext = %s\n", strrchr(fname, '.'));
	//}

	/// Open HDF5 file ///
	hdf5_file = H5Fopen(fname, H5F_ACC_RDONLY, H5P_DEFAULT); /// return file ID (<0 if failed) ///
	if(hdf5_file < 0)
	{
		printf("ERROR: Could not read file %s\n", fname);
		exit(1);
	}

	/// Get name of dataset in HDF5 file from label ///
	get_dataset_name(label, dataset_name);
	printf("Loading %s...\n", dataset_name);

	/// Get dataset lengths, data types, data type size before reading data ///
	for (type=0; type<NTYPES; type++)
	{
		/// Get group location IDs for each particle type ///
		sprintf(buf, "/PartType%d", type);
		hdf5_grp[type] = H5Gopen(hdf5_file, buf, H5P_DEFAULT);
		hdf5_dataset = H5Dopen(hdf5_grp[type], dataset_name, H5P_DEFAULT);
		/// Test if dataset was present ///
		if(hdf5_dataset > 0)
		{
			printf("Found %s data for particle type %d\n", label, type);
			if (strcmp(label, "MASS") == 0 && header.mass[type]!=0) {data_for_type[type] = false; continue; printf("Not loading masses for type %d\n", type);}
			else {data_for_type[type] = true;}
			dims[0] = header.npart[type];
			dims[1] = 0; //1D array
			hid_t dataspace = H5Dget_space(hdf5_dataset);
			/// Get rank and dimensions of dataspace ///
			rank = H5Sget_simple_extent_ndims(dataspace);
			H5Sget_simple_extent_dims(dataspace, dims, NULL);
			//printf("rank %d, dimensions %d x %d \n", rank, (int)dims[0], (int)dims[1]);
			N += dims[0];
			if (dims[1] >= values_per_particle) {
				if (dims[1] == 0) values_per_particle = 1;
				else values_per_particle = dims[1];
			}
			//printf("Values per particle = %d\n", values_per_particle);

			hdf5_datatype = H5Dget_type(hdf5_dataset);
			datasize = H5Tget_size(hdf5_datatype);
			//printf("Data has size %d bytes.\n", (int)datasize);

			/// Check that data is a float or double ///
			hdf5_class = H5Tget_class(hdf5_datatype);
			if (hdf5_class != H5T_FLOAT) {
				printf("WARNING: Dataset %s for particle type %d is not a float or double.\n", dataset_name, type);
				//exit(1);
			}
			H5Tclose(hdf5_datatype);
			H5Sclose(dataspace);
		}
		H5Dclose(hdf5_dataset);
	}
	if (N == 0) {
		printf("ERROR: Dataset %s not found for any particle type.\n", dataset_name);
		exit(1);
	}

	//*data = (float *) malloc(datasize * N * values_per_particle);
	*data = (float *) calloc(N * values_per_particle, datasize);
	if (data == NULL) {
		printf("ERROR: Memory not allocated.\n");
		exit(1);
	}

	for (type=0; type<NTYPES; type++)
	{
		if (data_for_type[type] == true) {
			hdf5_dataset = H5Dopen(hdf5_grp[type], dataset_name, H5P_DEFAULT);
			/// Make sure dataset was opened properly ///
			if(hdf5_dataset > 0)
			{
				/// Initialisation of dimensions ///
				dims[0] = header.npart[type];
				dims[1] = 0;
				/// Get rank and dimensions of dataspace automatically ///
				dataspace = H5Dget_space(hdf5_dataset);
				rank = H5Sget_simple_extent_ndims(dataspace);
				H5Sget_simple_extent_dims(dataspace, dims, NULL);
				//printf("rank %d, dimensions %d x %d \n", rank, (int)dims[0], (int)dims[1]);
				hdf5_dataspace_in_file = H5Screate_simple(rank, dims, NULL);
				hdf5_dataspace_in_memory = H5Screate_simple(rank, dims, NULL);

				hdf5_datatype = H5Dget_type(hdf5_dataset);

				/// Copy dataset to *data ///
				H5Dread(hdf5_dataset, hdf5_datatype, hdf5_dataspace_in_memory, hdf5_dataspace_in_file, H5P_DEFAULT, *data + (size_t)curgroup_pos);
				curgroup_pos += dims[0] * values_per_particle;

				H5Tclose(hdf5_datatype);
				H5Sclose(hdf5_dataspace_in_memory);
				H5Sclose(hdf5_dataspace_in_file);
				H5Sclose(dataspace);
			}
			else {
				printf("Could not read dataset %s for type %d.\n", dataset_name, type);
				exit(1);
			}
			H5Dclose(hdf5_dataset);
		}
	}
	/// Close leftover hdf5 stuff ///
	for(type = NTYPES-1; type >= 0; type--) {
	        H5Gclose(hdf5_grp[type]);
	}
	H5Fclose(hdf5_file);

	return N;
}

void get_halo_information_subf_hdf5 (struct SubfStruct *subf, int group, bool get_center, const char *fname, int subhalo, int *offset, int *length)
{
  printf("Getting center from hdf5 file: %s\n", fname);

  int Ngroups, TotNgroups, Nids, Nsubgroups, TotNsubgroups;
  //int NTask;
  long long TotNids;
  hid_t hdf5_file, hdf5_grp, hdf5_headergrp, hdf5_attribute, hdf5_dataset;

  float *locMass = 0;
  struct my_vector *locPos = 0;
  float *loc_M_Mean200 = 0;
  float *loc_R_Mean200 = 0;
  float *loc_M_Crit200 = 0;
  float *loc_R_Crit200 = 0;
  float *loc_M_Crit500 = 0;
  float *loc_R_Crit500 = 0;
  float *loc_M_TopHat200 = 0;
  float *loc_R_TopHat200 = 0;
  //int *locContaminationCount = 0;
  //float *locContaminationMass = 0;
  int *locNsubs = 0;
  int *locFirstSub = 0;

  struct lentab
  {
    int part0, part1, part2, part3, part4, part5;
  };
  int *locLen = 0;
  int *GroupOffset = 0;
  struct lentab *GroupLenTab = 0;
  struct lentab *GroupOffsetTab = 0;
  //int** GroupLenTab;
  //allocate_Int2D(&GroupLenTab, Ngroups, 6); // this needs to go after reading Ngroups!
  //int** GroupOffsetTab;
  //allocate_Int2D(&GroupOffsetTab, Ngroups, 6);
  printf("Allocated variables.\n");

  hdf5_file = H5Fopen(fname, H5F_ACC_RDONLY, H5P_DEFAULT); /// return file ID (<0 if failed) ///
  if(hdf5_file < 0)
  {
    printf("ERROR: Could not read subf file %s\n", fname);
    exit(1);
  }

  hdf5_headergrp = H5Gopen(hdf5_file, "/Header", H5P_DEFAULT);
  if(hdf5_headergrp < 0)
  {
    printf("ERROR: Header not found in subf file %s\n", fname);
    exit(1);
  }

  hdf5_attribute = H5Aopen_name(hdf5_headergrp, "Ngroups_ThisFile");
  H5Aread(hdf5_attribute, H5T_NATIVE_INT, &Ngroups);
  H5Aclose(hdf5_attribute);

  hdf5_attribute = H5Aopen_name(hdf5_headergrp, "Ngroups_Total");
  H5Aread(hdf5_attribute, H5T_NATIVE_INT, &TotNgroups);
  H5Aclose(hdf5_attribute);

  hdf5_attribute = H5Aopen_name(hdf5_headergrp, "Nids_ThisFile");
  H5Aread(hdf5_attribute, H5T_NATIVE_INT, &Nids);
  H5Aclose(hdf5_attribute);

  hdf5_attribute = H5Aopen_name(hdf5_headergrp, "Nids_Total");
  H5Aread(hdf5_attribute, H5T_NATIVE_INT, &TotNids);
  H5Aclose(hdf5_attribute);

  hdf5_attribute = H5Aopen_name(hdf5_headergrp, "Nsubgroups_ThisFile");
  H5Aread(hdf5_attribute, H5T_NATIVE_INT, &Nsubgroups);
  H5Aclose(hdf5_attribute);

  hdf5_attribute = H5Aopen_name(hdf5_headergrp, "Nsubgroups_Total");
  H5Aread(hdf5_attribute, H5T_NATIVE_INT, &TotNsubgroups);
  H5Aclose(hdf5_attribute);

  hdf5_grp = H5Gopen(hdf5_file, "/Group", H5P_DEFAULT);

  hdf5_dataset = H5Dopen(hdf5_grp, "GroupLen", H5P_DEFAULT);
  locLen = (int *) calloc(Ngroups, sizeof(int));
  H5Dread(hdf5_dataset, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, locLen);
  H5Dclose(hdf5_dataset);

    hdf5_dataset = H5Dopen(hdf5_grp, "GroupLenType", H5P_DEFAULT);
    GroupLenTab = (struct lentab *) calloc(Ngroups*6, sizeof(int));
    H5Dread(hdf5_dataset, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, GroupLenTab);
    H5Dclose(hdf5_dataset);
    printf("Read in GroupLenTab:\n");
    printf("GroupLenTab[0].part0 = %d\n", GroupLenTab[0].part0);

  hdf5_dataset = H5Dopen(hdf5_grp, "GroupMass", H5P_DEFAULT);
  locMass = (float *) calloc(Ngroups, sizeof(float));
  H5Dread(hdf5_dataset, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, H5P_DEFAULT, locMass);
  H5Dclose(hdf5_dataset);

  hdf5_dataset = H5Dopen(hdf5_grp, "GroupPos", H5P_DEFAULT);
  locPos = (struct my_vector *) calloc(Ngroups*3, sizeof(float));
  H5Dread(hdf5_dataset, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, H5P_DEFAULT, locPos);
  H5Dclose(hdf5_dataset);

  hdf5_dataset = H5Dopen(hdf5_grp, "Group_M_Mean200", H5P_DEFAULT);
  loc_M_Mean200 = (float *) calloc(Ngroups, sizeof(float));
  H5Dread(hdf5_dataset, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, H5P_DEFAULT, loc_M_Mean200);
  H5Dclose(hdf5_dataset);

  hdf5_dataset = H5Dopen(hdf5_grp, "Group_R_Mean200", H5P_DEFAULT);
  loc_R_Mean200 = (float *) calloc(Ngroups, sizeof(float));
  H5Dread(hdf5_dataset, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, H5P_DEFAULT, loc_R_Mean200);
  H5Dclose(hdf5_dataset);

  hdf5_dataset = H5Dopen(hdf5_grp, "Group_M_Crit200", H5P_DEFAULT);
  loc_M_Crit200 = (float *) calloc(Ngroups, sizeof(float));
  H5Dread(hdf5_dataset, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, H5P_DEFAULT, loc_M_Crit200);
  H5Dclose(hdf5_dataset);

  hdf5_dataset = H5Dopen(hdf5_grp, "Group_R_Crit200", H5P_DEFAULT);
  loc_R_Crit200 = (float *) calloc(Ngroups, sizeof(float));
  H5Dread(hdf5_dataset, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, H5P_DEFAULT, loc_R_Crit200);
  H5Dclose(hdf5_dataset);

  hdf5_dataset = H5Dopen(hdf5_grp, "Group_M_Crit500", H5P_DEFAULT);
  loc_M_Crit500 = (float *) calloc(Ngroups, sizeof(float));
  H5Dread(hdf5_dataset, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, H5P_DEFAULT, loc_M_Crit500);
  H5Dclose(hdf5_dataset);

  hdf5_dataset = H5Dopen(hdf5_grp, "Group_R_Crit500", H5P_DEFAULT);
  loc_R_Crit500 = (float *) calloc(Ngroups, sizeof(float));
  H5Dread(hdf5_dataset, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, H5P_DEFAULT, loc_R_Crit500);
  H5Dclose(hdf5_dataset);

  hdf5_dataset = H5Dopen(hdf5_grp, "Group_M_TopHat200", H5P_DEFAULT);
  loc_M_TopHat200 = (float *) calloc(Ngroups, sizeof(float));
  H5Dread(hdf5_dataset, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, H5P_DEFAULT, loc_M_TopHat200);
  H5Dclose(hdf5_dataset);

  hdf5_dataset = H5Dopen(hdf5_grp, "Group_R_TopHat200", H5P_DEFAULT);
  loc_R_TopHat200 = (float *) calloc(Ngroups, sizeof(float));
  H5Dread(hdf5_dataset, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, H5P_DEFAULT, loc_R_TopHat200);
  H5Dclose(hdf5_dataset);

  hdf5_dataset = H5Dopen(hdf5_grp, "GroupNsubs", H5P_DEFAULT);
  locNsubs = (int *) calloc(Ngroups, sizeof(int));
  H5Dread(hdf5_dataset, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, locNsubs);
  H5Dclose(hdf5_dataset);

  hdf5_dataset = H5Dopen(hdf5_grp, "GroupFirstSub", H5P_DEFAULT);
  locFirstSub = (int *) calloc(Ngroups, sizeof(int));
  H5Dread(hdf5_dataset, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, locFirstSub);
  H5Dclose(hdf5_dataset);

  (*subf).subf_Len = locLen[group];
  (*subf).subf_Mass = locMass[group];
  (*subf).subf_Pos = locPos[group];
  (*subf).subf_M_Mean200 = loc_M_Mean200[group];
  (*subf).subf_R_Mean200 = loc_R_Mean200[group];
  (*subf).subf_M_Crit200 = loc_M_Crit200[group];
  (*subf).subf_R_Crit200 = loc_R_Crit200[group];
  (*subf).subf_M_Crit500 = loc_M_Crit500[group];
  (*subf).subf_R_Crit500 = loc_R_Crit500[group];
  (*subf).subf_M_TopHat200 = loc_M_TopHat200[group];
  (*subf).subf_R_TopHat200 = loc_R_TopHat200[group];
  (*subf).subf_Nsubs = locNsubs[group];
  (*subf).subf_FirstSub = locFirstSub[group];
  int FirstSub = locFirstSub[group];
  int Nsubs = locNsubs[group];

  // Create GroupOffset (counts all particle types)
  GroupOffset = (int *) calloc(Ngroups, sizeof(int));
  int cumsum = 0, i;
  for (i=0; i < Ngroups; i++) {
    GroupOffset[i] = cumsum;
    cumsum += locLen[i];
  }

  // Create GroupOffsetTab
  GroupOffsetTab = (struct lentab *) calloc(Ngroups*6, sizeof(int));
  int *cumsumtab;
  cumsumtab = (int*) calloc(6, sizeof(int)); //zero array of length 6
  int j;
  for (i=0; i<Ngroups; i++) {
    GroupOffsetTab[i].part0 = cumsumtab[0];
    cumsumtab[0] += GroupLenTab[i].part0;
    GroupOffsetTab[i].part1 = cumsumtab[1];
    cumsumtab[1] += GroupLenTab[i].part1;
    GroupOffsetTab[i].part2 = cumsumtab[2];
    cumsumtab[2] += GroupLenTab[i].part2;
    GroupOffsetTab[i].part3 = cumsumtab[3];
    cumsumtab[3] += GroupLenTab[i].part3;
    GroupOffsetTab[i].part4 = cumsumtab[4];
    cumsumtab[4] += GroupLenTab[i].part4;
    GroupOffsetTab[i].part5 = cumsumtab[5];
    cumsumtab[5] += GroupLenTab[i].part5;
    // I seriously need to make this a 2D array instead...
    //for (j=0; j<6; j++) {
      //GroupOffsetTab[i][j] = cumsumtab[j];
      //cumsumtab[j] += GroupLenTab[i][j];
    //}
  }
  printf("Created GroupOffsetTab.\n");
  free(cumsumtab);

  free(locLen);
  free(locMass);
  free(locPos);
  free(loc_M_Mean200);
  free(loc_R_Mean200);
  free(loc_M_Crit200);
  free(loc_R_Crit200);
  free(loc_M_Crit500);
  free(loc_R_Crit500);
  free(loc_M_TopHat200);
  free(loc_R_TopHat200);
  free(locNsubs);
  free(locFirstSub);

  printf("Group data obtained.\n");

  if (subhalo>=0)
  {
    //int** SubLenTab;
    //allocate_Int2D(&SubLenTab, Nsubgroups, 6); // 2D array
    //int** SubOffsetTab;
    //allocate_Int2D(&SubOffsetTab, Nsubgroups, 6);
    int *SubLen = 0;
    int *SubOffset = 0;
    struct lentab *SubLenTab = 0;
    //int *locOffset = 0;
    //struct lentab *SubOffsetTab = 0;
    //struct lentab *cumsum = 0;

    hdf5_grp = H5Gopen(hdf5_file, "/Subhalo", H5P_DEFAULT);

    hdf5_dataset = H5Dopen(hdf5_grp, "SubhaloLen", H5P_DEFAULT);
    SubLen = (int *) calloc(Nsubgroups, sizeof(int));
    H5Dread(hdf5_dataset, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, SubLen);
    H5Dclose(hdf5_dataset);
    printf("SubLen[0] = %d\n", SubLen[0]);

    hdf5_dataset = H5Dopen(hdf5_grp, "SubhaloLenType", H5P_DEFAULT);
    SubLenTab = (struct lentab *) calloc(Nsubgroups*6, sizeof(int));
    H5Dread(hdf5_dataset, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, SubLenTab);
    H5Dclose(hdf5_dataset);
    printf("SubLenTab[0].part0 = %d\n", SubLenTab[0].part0);

    // Create SubOffset for given group for type 0
    SubOffset = (int *) calloc(Nsubs, sizeof(int));
    cumsum = 0;
    for (i=FirstSub; i < FirstSub+Nsubs; i++) {
      SubOffset[i] = cumsum;
      cumsum += SubLenTab[i].part0;
    }
    printf("Created SubOffset.\n");

    *offset = GroupOffsetTab[group].part0 + SubOffset[subhalo]; //just part type 0 for now since particles are grouped by type before being grouped by fof group ot subhalo 
    //*length = SubLen[(*subf).subf_FirstSub + subhalo];
    *length = SubLenTab[FirstSub + subhalo].part0;

    /*
    // Create SubOffsetTab
    int *cumsum;
    cumsum = (int*) calloc(6, sizeof(int)); //zero array of length 6
    int i, j;
    for (i=0; i<Nsubgroups; i++) {
      for (j=0; j<6; j++) {
        SubOffsetTab[i][j] = cumsum[j];
        cumsum[j] += SubLenTab[i][j];
      }
    }
    for (i=0; i<Ngroups; i++) {
      if ((*subf).subf_Nsubs[i] >= 1) {
        
      }
    }*/

    /*
    SubOffsetTab = (struct lentab *) calloc(Nsubgroups*6, sizeof(int));
    cumsum = (struct lentab *) calloc(Nsubgroups*6, sizeof(int));

    for (j=0; j<Nsubgroups; j++) {
      SubOffsetTab[j].part0 = cumsum[j].part0;
      SubOffsetTab[j].part1 = cumsum[j].part1;
      SubOffsetTab[j].part2 = cumsum[j].part2;
      SubOffsetTab[j].part3 = cumsum[j].part3;
      SubOffsetTab[j].part4 = cumsum[j].part4;
      SubOffsetTab[j].part5 = cumsum[j].part5;

      if (j+1 < Nsubgroups) {
        cumsum[j+1].part0 = cumsum[j].part0 + SubLenTab[j].part0;
        cumsum[j+1].part1 = cumsum[j].part1 + SubLenTab[j].part1;
        cumsum[j+1].part2 = cumsum[j].part2 + SubLenTab[j].part2;
        cumsum[j+1].part3 = cumsum[j].part3 + SubLenTab[j].part3;
        cumsum[j+1].part4 = cumsum[j].part4 + SubLenTab[j].part4;
        cumsum[j+1].part5 = cumsum[j].part5 + SubLenTab[j].part5;
      }
    }*/

    //printf("Sorry. Subhalo>0 currently not supported for hdf5\n");
    //exit(1);

    //deallocate_Int2D(&SubLenTab, Nsubgroups);
    //deallocate_Int2D(&SubOffsetTab, Nsubgroups);
    //free(cumsum);

    free(SubLen);
    free(SubLenTab);
    free(SubOffset);
  }
  //deallocate_Int2D(&GroupLenTab, Ngroups);
  //deallocate_Int2D(&GroupOffsetTab, Ngroups);
  free(GroupOffsetTab);
  free(GroupLenTab);
  free(GroupOffset);

  H5Gclose(hdf5_grp);
  H5Fclose(hdf5_file);
}

void allocate_Int2D(int*** arr, int nrows, int ncols)
{
  *arr = (int**) malloc(nrows * sizeof(int*));
  int i;
  for (i=0; i < nrows; i++) {
    (*arr)[i] = (int*) malloc(ncols * sizeof(int));
  }
}

void deallocate_Int2D(int*** arr, int nrows)
{
  int i;
  for (i=0; i < nrows; i++) {
    free((*arr)[i]);
  }
  free(*arr);
}

/// Read header attributes into given variables ///
void read_gadget_head_full_hdf5(int *npart,double *massarr,double *time,double *redshift,
                          int *sfr, int *feedback, int *nall, int *cooling, int *filenum,   
                          double *boxsize, double *omega_m, double *omega_l, double *hubble, const char *fname)
{
  hid_t hdf5_file, hdf5_headergrp, hdf5_attribute;
  hdf5_file = H5Fopen(fname, H5F_ACC_RDONLY, H5P_DEFAULT);
  hdf5_headergrp = H5Gopen(hdf5_file, "/Header", H5P_DEFAULT);

  hdf5_attribute = H5Aopen_name(hdf5_headergrp, "NumPart_ThisFile");
  H5Aread(hdf5_attribute, H5T_NATIVE_INT, npart);
  H5Aclose(hdf5_attribute);

  hdf5_attribute = H5Aopen_name(hdf5_headergrp, "NumPart_Total");
  H5Aread(hdf5_attribute, H5T_NATIVE_UINT, nall);
  H5Aclose(hdf5_attribute);

  hdf5_attribute = H5Aopen_name(hdf5_headergrp, "MassTable");
  H5Aread(hdf5_attribute, H5T_NATIVE_DOUBLE, massarr);
  H5Aclose(hdf5_attribute);

  hdf5_attribute = H5Aopen_name(hdf5_headergrp, "Time");
  H5Aread(hdf5_attribute, H5T_NATIVE_DOUBLE, time);
  H5Aclose(hdf5_attribute);

  hdf5_attribute = H5Aopen_name(hdf5_headergrp, "Redshift");
  H5Aread(hdf5_attribute, H5T_NATIVE_DOUBLE, redshift);
  H5Aclose(hdf5_attribute);

  hdf5_attribute = H5Aopen_name(hdf5_headergrp, "BoxSize");
  H5Aread(hdf5_attribute, H5T_NATIVE_DOUBLE, boxsize);
  H5Aclose(hdf5_attribute);

  hdf5_attribute = H5Aopen_name(hdf5_headergrp, "NumFilesPerSnapshot");
  H5Aread(hdf5_attribute, H5T_NATIVE_INT, filenum);
  H5Aclose(hdf5_attribute);

  hdf5_attribute = H5Aopen_name(hdf5_headergrp, "Omega0");
  H5Aread(hdf5_attribute, H5T_NATIVE_DOUBLE, omega_m);
  H5Aclose(hdf5_attribute);

  hdf5_attribute = H5Aopen_name(hdf5_headergrp, "OmegaLambda");
  H5Aread(hdf5_attribute, H5T_NATIVE_DOUBLE, omega_l);
  H5Aclose(hdf5_attribute);

  hdf5_attribute = H5Aopen_name(hdf5_headergrp, "HubbleParam");
  H5Aread(hdf5_attribute, H5T_NATIVE_DOUBLE, hubble);
  H5Aclose(hdf5_attribute);

  hdf5_attribute = H5Aopen_name(hdf5_headergrp, "Flag_Sfr");
  H5Aread(hdf5_attribute, H5T_NATIVE_INT, sfr);
  H5Aclose(hdf5_attribute);

  hdf5_attribute = H5Aopen_name(hdf5_headergrp, "Flag_Cooling");
  H5Aread(hdf5_attribute, H5T_NATIVE_INT, cooling);
  H5Aclose(hdf5_attribute);

  hdf5_attribute = H5Aopen_name(hdf5_headergrp, "Flag_Feedback");
  H5Aread(hdf5_attribute, H5T_NATIVE_INT, feedback);
  H5Aclose(hdf5_attribute);

  H5Gclose(hdf5_headergrp);
  H5Fclose(hdf5_file);

}

/// Read header into header struct ///
void read_header_struct_hdf5(const char *fname)
{
  hid_t hdf5_file, hdf5_headergrp, hdf5_attribute;

  hdf5_file = H5Fopen(fname, H5F_ACC_RDONLY, H5P_DEFAULT);
  hdf5_headergrp = H5Gopen(hdf5_file, "/Header", H5P_DEFAULT);

  hdf5_attribute = H5Aopen_name(hdf5_headergrp, "NumPart_ThisFile");
  H5Aread(hdf5_attribute, H5T_NATIVE_INT, header.npart);
  H5Aclose(hdf5_attribute);

  hdf5_attribute = H5Aopen_name(hdf5_headergrp, "NumPart_Total");
  H5Aread(hdf5_attribute, H5T_NATIVE_UINT, header.npartTotal);
  H5Aclose(hdf5_attribute);

  hdf5_attribute = H5Aopen_name(hdf5_headergrp, "NumPart_Total_HighWord");
  H5Aread(hdf5_attribute, H5T_NATIVE_UINT, header.npartTotalHighWord);
  H5Aclose(hdf5_attribute);

  hdf5_attribute = H5Aopen_name(hdf5_headergrp, "MassTable");
  H5Aread(hdf5_attribute, H5T_NATIVE_DOUBLE, header.mass);
  H5Aclose(hdf5_attribute);

  hdf5_attribute = H5Aopen_name(hdf5_headergrp, "Time");
  H5Aread(hdf5_attribute, H5T_NATIVE_DOUBLE, &header.time);
  H5Aclose(hdf5_attribute);

  hdf5_attribute = H5Aopen_name(hdf5_headergrp, "Redshift");
  H5Aread(hdf5_attribute, H5T_NATIVE_DOUBLE, &header.redshift);
  H5Aclose(hdf5_attribute);

  hdf5_attribute = H5Aopen_name(hdf5_headergrp, "BoxSize");
  H5Aread(hdf5_attribute, H5T_NATIVE_DOUBLE, &header.BoxSize);
  H5Aclose(hdf5_attribute);

  hdf5_attribute = H5Aopen_name(hdf5_headergrp, "NumFilesPerSnapshot");
  H5Aread(hdf5_attribute, H5T_NATIVE_INT, &header.num_files);
  H5Aclose(hdf5_attribute);

  hdf5_attribute = H5Aopen_name(hdf5_headergrp, "Omega0");
  H5Aread(hdf5_attribute, H5T_NATIVE_DOUBLE, &header.Omega0);
  H5Aclose(hdf5_attribute);

  hdf5_attribute = H5Aopen_name(hdf5_headergrp, "OmegaLambda");
  H5Aread(hdf5_attribute, H5T_NATIVE_DOUBLE, &header.OmegaLambda);
  H5Aclose(hdf5_attribute);

  hdf5_attribute = H5Aopen_name(hdf5_headergrp, "HubbleParam");
  H5Aread(hdf5_attribute, H5T_NATIVE_DOUBLE, &header.HubbleParam);
  H5Aclose(hdf5_attribute);

  hdf5_attribute = H5Aopen_name(hdf5_headergrp, "Flag_Sfr");
  H5Aread(hdf5_attribute, H5T_NATIVE_INT, &header.flag_sfr);
  H5Aclose(hdf5_attribute);

  hdf5_attribute = H5Aopen_name(hdf5_headergrp, "Flag_Cooling");
  H5Aread(hdf5_attribute, H5T_NATIVE_INT, &header.flag_cooling);
  H5Aclose(hdf5_attribute);

  hdf5_attribute = H5Aopen_name(hdf5_headergrp, "Flag_StellarAge");
  H5Aread(hdf5_attribute, H5T_NATIVE_INT, &header.flag_stellarage);
  H5Aclose(hdf5_attribute);

  hdf5_attribute = H5Aopen_name(hdf5_headergrp, "Flag_Metals");
  H5Aread(hdf5_attribute, H5T_NATIVE_INT, &header.flag_metals);
  H5Aclose(hdf5_attribute);

  hdf5_attribute = H5Aopen_name(hdf5_headergrp, "Flag_Feedback");
  H5Aread(hdf5_attribute, H5T_NATIVE_INT, &header.flag_feedback);
  H5Aclose(hdf5_attribute);

  hdf5_attribute = H5Aopen_name(hdf5_headergrp, "Flag_DoublePrecision");
  H5Aread(hdf5_attribute, H5T_NATIVE_INT, &header.flag_doubleprecision);
  H5Aclose(hdf5_attribute);

  H5Gclose(hdf5_headergrp);
  H5Fclose(hdf5_file);
}

/// Get name of dataset in HDF5 file corresponding to given label ///
void get_dataset_name(const char *label, char *buf)
{
    strcpy(buf, "default");

    if (strcmp(label, "POS ") == 0 ) {
      strcpy(buf, "Coordinates");
      }
    else if (strcmp(label, "VEL ") == 0 ) {
      strcpy(buf, "Velocities");
      }
    else if (strcmp(label, "ID  ") == 0 ) {
      strcpy(buf, "ParticleIDs");
      }
    else if (strcmp(label, "MASS") == 0 ) {
      strcpy(buf, "Masses");
      }
    else if (strcmp(label, "U   ") == 0 ) {
      strcpy(buf, "InternalEnergy");
      }
    else if (strcmp(label, "RHO ") == 0 ) {
      strcpy(buf, "Density");
      }
    else if (strcmp(label, "VOL ") == 0 ) {
      strcpy(buf, "Volume");
      }
    else if (strcmp(label, "PRESSURE") == 0 ) {
      strcpy(buf, "Pressure");
      }
    else if (strcmp(label, "NE  ") == 0 ) {
      strcpy(buf, "ElectronAbundance");
      }
    else if (strcmp(label, "NH  ") == 0 ) {
      strcpy(buf, "NeutralHydrogenAbundance");
      }
    else if (strcmp(label, "ELECT") == 0 ) {
      strcpy(buf, "elect");
      }
    else if (strcmp(label, "HI  ") == 0 ) {
      strcpy(buf, "HI");
      }
    else if (strcmp(label, "HII ") == 0 ) {
      strcpy(buf, "HII");
      }
    else if (strcmp(label, "HeI ") == 0 ) {
      strcpy(buf, "HeI");
      }
    else if (strcmp(label, "HeII") == 0 ) {
      strcpy(buf, "HeII");
      }
    else if (strcmp(label, "HeIII") == 0 ) {
      strcpy(buf, "HeIII");
      }
    else if (strcmp(label, "H2I") == 0 ) {
      strcpy(buf, "H2I");
      }
    else if (strcmp(label, "H2II") == 0 ) {
      strcpy(buf, "H2II");
      }
    else if (strcmp(label, "HM  ") == 0 ) {
      strcpy(buf, "HM");
      }
    else if (strcmp(label, "HSML") == 0 ) {
      strcpy(buf, "SmoothingLength");
      }
    else if (strcmp(label, "SFR ") == 0 ) {
      strcpy(buf, "StarFormationRate");
      }
    else if (strcmp(label, "AGE ") == 0 ) {
      strcpy(buf, "StellarFormationTime");
      }
    else if (strcmp(label, "Z   ") == 0 ) {
      strcpy(buf, "GFM_Metallicity");
      }
    else if (strcmp(label, "POT ") == 0 ) {
      strcpy(buf, "Potential");
      }
    else if (strcmp(label, "ACCEL") == 0 ) {
      strcpy(buf, "Acceleration");
      }
    else if (strcmp(label, "BFLD") == 0 ) {
      strcpy(buf, "MagneticField");
      }
    else {
	printf("ERROR: Label %s unknown.\n", label);
	exit(1);
      }
}
