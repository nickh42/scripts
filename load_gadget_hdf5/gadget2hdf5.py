## This script converts snapshot and halo files from Gadget format to Arepo HDF5 format

import readsnap as rs
import readsubf
import h5py
import os, sys

convert_snapshots=False
convert_fof=True

calc_m500 = False ##DOESN'T WORK - Calculates M500 and R500 for halo file
mpc_units = True
convert_mpc = False # If true and units are Mpc, will convert to kpc units

simdir = "/data/deboras/ewald/"

simnames = ["m_585_h_486_2210_z3/csf", "m_585_h_486_2210_z3/csfbh",
	    "m_717_h_497_3136_z3/csf", "m_717_h_497_3136_z3/csfbh",
	    "m_878_h_466_1998_z3/csf", "m_878_h_466_1998_z3/csfbh",
	    "m_1075_h_442_550_z3/csf", "m_1075_h_442_550_z3/csfbh",
	    "m_1317_h_379_2174_z3/csf", "m_1317_h_379_2174_z3/csfbh",
	    "m_1613_h_390_2864_z3/csf", "m_1613_h_390_2864_z3/csfbh",
	    "m_1975_h_506_1096_z3/csf", "m_1975_h_506_1096_z3/csfbh",
	    "m_2186_h_56_1789_z3/csf", "m_2186_h_56_1789_z3/csfbh",
	    "m_2419_h_436_2408_z3/csf", "m_2419_h_436_2408_z3/csfbh",
	    "m_2963_h_73_1513_z3/csf", "m_2963_h_73_1513_z3/csfbh",
	    "m_3279_h_103_2600_z3/csf", "m_3279_h_103_2600_z3/csfbh",
	    "m_3629_h_259_610_z3/csf", "m_3629_h_259_610_z3/csfbh",
	    "m_4445_h_183_1161_z3/csf", "m_4445_h_183_1161_z3/csfbh",
	    "m_5443_h_352_482_z3/csf", "m_5443_h_352_482_z3/csfbh",
	    "m_6664_h_48_194_z3/csf", "m_6664_h_48_194_z3/csfbh",
	    "m_10002_h_94_501_z3/csf", "m_10002_h_94_501_z3/csfbh",
	    "m_14999_h_355_1278_z3/csf",
	    "m_22498_h_197_0_z2/csf", "m_22498_h_197_0_z2/csfbh",
	    "m_33617_h_146_0_z2/csf", "m_33617_h_146_0_z2/csfbh"]

#simnames = ["m_3629_h_259_610_z3/csf", "m_3629_h_259_610_z3/csfbh"]
#simnames = ["m_585_h_486_2210_z3/csf"]
snaplist = [62]
snapnamebase = "snap_"
outdir = "/home/nh444/data/project1/Puchwein2008sims/"

g2h5dict = {'POS ':'Coordinates', 'VEL ':'Velocities', 'ID  ':'ParticleIDs',
            'MASS':'Masses', 'U   ':'InternalEnergy', 'RHO ':'Density',
            'VOL ':'Volume', 'NE  ':'ElectronAbundance',
            'NH  ':'NeutralHydrogenAbundance', 'HSML':'SmoothingLength',
            'SFR ':'StarFormationRate', 'AGE ':'StellarFormationTime',
            'Z   ':'GFM_Metallicity', 'BHMA':'BH_Mass', 'BHMD':'BH_Mdot',
            ## Note this uses 'GFM_Metallicity' for Z - you may want 'Metallicity' instead
            'BHMQ':'BH_CumMassGrowth_QM', 'BHMI':'BH_Mass_ini',
            'BHMB':'BH_Mass_Bubbles', 'SUBH':'SubfindHsml',
            'HRGM':'HighResGasMass', 'ALRE':'AllowRefinement',
            'BFLD':'MagneticField', 'GZ  ':'GFM_Metallicity',
            'GAGE':'GFM_StellarFormationTime', 'GMET':'GFM_Metallicity'};

if convert_snapshots:
    for simname in simnames:
        for snapnum in snaplist:
            print "Converting snapshot", snapnum, "from", simname
            infile = os.path.normpath(simdir)+"/"+simname+"/"+snapnamebase+str(snapnum).zfill(3)
            header = rs.snapshot_header(infile)

            simoutdir = os.path.normpath(outdir)+"/"+simname
            if not os.path.exists(simoutdir):
                os.makedirs(simoutdir)
            outfile = simoutdir+"/"+snapnamebase+str(snapnum).zfill(3)+".hdf5"
            if os.path.exists(outfile):
                os.remove(outfile)
            f = h5py.File(outfile)

            ### Write header ###
            grp = f.create_group("Header")
            grp.attrs['BoxSize'] = header.boxsize
            grp.attrs['NumPart_Total'] = header.nall
            grp.attrs['Redshift'] = header.redshift
            grp.attrs['Flag_Cooling'] = header.cooling
            grp.attrs['NumPart_ThisFile'] = header.npart
            grp.attrs['Flag_Sfr'] = header.sfr
            grp.attrs['Flag_Feedback'] = header.feedback
            grp.attrs['HubbleParam'] = header.hubble
            grp.attrs['OmegaLambda'] = header.omega_l
            grp.attrs['MassTable'] = header.massarr
            grp.attrs['Omega0'] = header.omega_m
            grp.attrs['Time'] = header.time
            grp.attrs['NumFilesPerSnapshot'] = header.filenum

            ### Write datasets ###
            for parttype in range(0,6):
                if (header.npart[parttype] > 0):
                    print "Converting particle type", parttype
                    grp = f.create_group("PartType"+str(parttype))

                    for gname in g2h5dict:
                        #print gname, ">>>", g2h5dict[gname]
                        try:
                            partdata = rs.read_block(infile, gname, parttype=parttype, err_msgs = False)
                            if convert_mpc:
                                if (gname=="POS "):
                                    partdata *= 1000.0
                                if (gname=="RHO "):
                                    partdata *= 1.0e9
                                if (gname=="HSML"):
                                    partdata *= 1000.0
                            dset = grp.create_dataset(g2h5dict[gname], data=partdata)
                        except:
                            pass
            f.close()
                    
if convert_fof:
    groupdict = {'group_len':'GroupLen',
            'group_lentab':'GroupLenType',
            'group_mass':'GroupMass',
            'group_masstab':'GroupMassType',
            'group_pos':'GroupPos',
            'group_vel':'GroupVel',               
            'group_m_mean200':'Group_M_Mean200',
            'group_r_mean200':'Group_R_Mean200',
            'group_m_crit200':'Group_M_Crit200',
            'group_r_crit200':'Group_R_Crit200',
            'group_m_tophat200':'Group_M_TopHat200',
            'group_r_tophat200':'Group_R_TopHat200',
            'group_nsubs':'GroupNsubs',
            'group_firstsub':'GroupFirstSub'};
    subhalodict = {'sub_len':'SubhaloLen',
            'sub_lentab':'SubhaloLenType',
            'sub_parent':'SubhaloParent',
            'sub_mass':'SubhaloMass',
            'sub_masstab':'SubhaloMassType',
            'sub_pos':'SubhaloPos',
            'sub_vel':'SubhaloVel',
            'sub_cm':'SubhaloCM',
            'sub_spin':'SubhaloSpin',
            'sub_veldisp':'SubhaloVelDisp',
            'sub_vmax':'SubhaloVmax',
            'sub_vmaxrad':'SubhaloVmaxRad',
            'sub_halfmassrad':'SubhaloHalfmassRad',
            'sub_id_mostbound':'SubhaloIDMostbound',
            'sub_grnr':'SubhaloGrNr'};
    
    for simname in simnames:
        for snapnum in snaplist:
            print "Converting halo file", snapnum, "from", simname
            basedir = os.path.normpath(simdir)+"/"+simname
            simoutdir = os.path.normpath(outdir)+"/"+simname
            if not os.path.exists(simoutdir):
                os.makedirs(simoutdir)
            outfile = simoutdir+"/subhalo_tab_"+str(snapnum).zfill(3)+".hdf5"
            if os.path.exists(outfile):
                os.remove(outfile)
            f = h5py.File(outfile)

            cat = readsubf.subfind_catalog(basedir, snapnum, masstab=True)

            grp = f.create_group("Header")
            try:
                grp.attrs['BoxSize'] = cat.boxsize
                grp.attrs['HubbleParam'] = cat.hubble
                grp.attrs['OmegaLambda'] = cat.omega_l
                grp.attrs['Omega0'] = cat.omega_m
                grp.attrs['Redshift'] = cat.redshift
                grp.attrs['Time'] = cat.time
            except:
                pass

            try:
                grp.attrs['Ngroups_Total'] = cat.ngroups
                grp.attrs['Ngroups_ThisFile'] = cat.ngroups
                grp.attrs['Nids_Total'] = cat.nids
                grp.attrs['Nids_ThisFile'] = cat.nids
                grp.attrs['Nsubgroups_Total'] = cat.nsubs
                grp.attrs['Nsubgroups_ThisFile'] = cat.nsubs
                grp.attrs['NumFiles'] = 1 ## Combines all binary files into one hdf5 file
            except:
                continue
            
            grp = f.create_group("Group")
            for rsubfname in groupdict:
                try:
                    dset = grp.create_dataset(groupdict[rsubfname], data=getattr(cat, rsubfname))
                except:
                    pass

            if calc_m500:
                ## Calculate the remaining group properties
                import snap
                M500c = []
                R500c = []

                snapdata = snap.snapshot(basedir, snapnum, masstab=True, mpc_units=mpc_units)
                for groupnum in range(0,cat.ngroups):
                    m500c,r500c = snapdata.get_so_mass_rad(groupnum, 500, ref="crit")
                    M500c.append(m500c)
                    R500c.append(r500c)
                dset = grp.create_dataset('Group_M_Crit500', data=M500c)
                dset = grp.create_dataset('Group_R_Crit500', data=R500c)


            grp = f.create_group("Subhalo")
            for rsubfname in subhalodict:
                try:
                    dset = grp.create_dataset(subhalodict[rsubfname], data=getattr(cat, rsubfname))
                except:
                    pass

            f.close()
