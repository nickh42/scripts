
def main():
    basedir = "/data/curie4/nh444/project1/zoom_ins/MDRInt/"   
    subdirs = ["c96_MDRInt", "c128_MDRInt", "c192_MDRInt", "c224_MDRInt"]
    #subdirs = ["c432_MDRInt", "c448_MDRInt", "c464_MDRInt", "c480_MDRInt", "c496_MDRInt", "c512_MDRInt"]
    #basedir = "/data/curie4/nh444/project1/zoom_ins/"   
    #subdirs = ["LDRIntRadioEffBHVel/c512_box_Arepo_new", "MDRInt/c512_MDRInt", "MDRInt/c585_MDRInt"]
#    basedir = "/data/curie4/nh444/project1/zoom_ins/"   
#    subdirs = ["MDRInt/c448_MDRInt", "MDRInt/c448_MDRInt_noprescrit", "Illustris_model/c448_Ill"]
#    basedir = "/home/nh444/project1/boxes/"
#    subdirs = ["L40_512_LDRIntRadioEff_SoftSmaller", "L40_512_LDRIntRadioEff_SoftSmall", "L40_512_LDRIntRadioEffBHVel"]

    subdirs = ["c96_MDRInt", "c128_MDRInt", "c160_MDRInt", "c192_MDRInt",
               "c224_MDRInt", "c256_MDRInt", "c288_MDRInt", "c320_MDRInt",
               "c352_MDRInt", "c384_MDRInt", "c448_MDRInt", "c480_MDRInt",
               "c512_MDRInt",
               "c112_MDRInt", "c144_MDRInt", "c176_MDRInt", "c208_MDRInt",
               "c240_MDRInt", "c272_MDRInt", "c304_MDRInt",
               "c336_MDRInt",
               "c368_MDRInt", "c400_MDRInt", "c410_MDRInt", "c432_MDRInt",
               "c464_MDRInt",
               "c496_MDRInt",
               ]
#    basedir = "/data/curie4/nh444/project1/boxes/"
#    subdirs = ["L40_512_DutyRadioWeak",
#               "L40_512_LDRHighRadioEff",
#               "L40_512_LDRIntRadioEff",
#               "L40_512_noBH",
#               "L40_512_LDRIntRadioEff_SoftSmall",
#               "L40_512_LDRIntRadioEff_SoftSmaller",
#               "L40_512_MDRIntRadioEff",
#               "L40_512_NoDutyRadioInt",
#               "L40_512_NoDutyRadioWeak",
#               ]


    labels = [subdir.replace("_", "\_") for subdir in subdirs]

#    tcpu_vs_redshift(basedir, subdirs, labels, norm=False)
    sum_cpu_time(basedir, subdirs, verbose=True)


def sum_cpu_time(basedir, subdirs, verbose=False):
    """Sum the total cpu time of the given simulations."""
    tCPU = 0.0
    for i, subdir in enumerate(subdirs):
        if verbose:
            print "subdir", subdir
        with open(basedir + subdir + "/" + "cpu.txt", 'r') as f:
            # Search file in reverse order.
            for line in reversed(f.readlines()):
                # Find the last output to cpu.txt
                if line[:5] == "total":
                    tCPU_per_CPU = float(line.split()[2])
                    if verbose:
                        print "tCPU_per_CPU =", tCPU_per_CPU / 3600.0
                if line[:4] == "Step":
                    # Get number of cores
                    nCPU = float(line.split()[5].replace(",", ""))
                    tCPU += tCPU_per_CPU * nCPU
                    if verbose:
                        print "nCPU =", int(nCPU)
                        print "tCPU =", tCPU_per_CPU * nCPU / 3600.0
                    break

    print "Total CPU time", tCPU / 3600.0, "hours."


def tcpu_vs_redshift(basedir, subdirs, labels, norm=True, xlims=None):
    import matplotlib.pyplot as plt
    import numpy as np
    from scipy.stats import binned_statistic

    col = ["#c7673e", "#78a44f", "#6b87c8","#934fce","#E6AB02","#be558f","#47af8d"]
    bins = np.logspace(-1, 3, num=100)
    plt.figure(figsize=(14,10))
    
    
# Format of cpu.txt is:
#Step 0, Time: 0.0078125, CPUs: 128, MultiDomains: 8, HighestActiveTimeBin: 0
#                          diff          cumulative
#total                    89.70       89.70  100.0%
#  treegrav               71.90       71.90   80.2%
    
    for i, subdir in enumerate(subdirs):
        tstep = [] # timestep
        nCPU = [] # number of cores
        tCPU_tot = [] # average wall clock time per core in seconds
        
        with open(basedir+subdir+"/"+"cpu.txt", 'r') as f:
            for line in f:
                if line[:4]=="Step":
                    arr = line.split() ## split by whitespace
                    tstep.append(float(arr[3].replace(",","")))
                    nCPU.append(int(arr[5].replace(",","")))
                if line[:5]=="total":
                    arr = line.split() ## split by whitespace
                    tCPU_tot.append(float(arr[2]))
                    
        tstep = np.asarray(tstep)
        nCPU = np.asarray(nCPU)
        assert np.all((nCPU == nCPU[0])), "No. of cores not the same at all timesteps"
        tCPU_tot = np.asarray(tCPU_tot)
        z = (1.0 / tstep) - 1.0
        
        if norm: ## normalise by the total cpu time
            y = tCPU_tot / tCPU_tot[-1]
            plt.ylabel("Normalised wall clock time on all cores")
        else:
             y = tCPU_tot / 3600.0 * nCPU ## convert to total wall clock time in hours for all cores
             plt.ylabel("Total wall clock time on all cores [CPUh]")
        #plt.plot(z+1.0, y, c=col[i], alpha=0.5, label=labels[i]) ## this is a lot of points to plot so can be slow
        hist, bin_edges, n = binned_statistic(z+1.0, y, statistic='median', bins=bins)
        x = np.sqrt(bin_edges[1:]*bin_edges[:-1])
        plt.plot(x, hist, c=col[i], lw=2, label=labels[i]) ## warning: this plots the median at the centre of each bin so is not exact
    
    plt.legend(loc='best')
    plt.xscale('log')
    plt.yscale('log')
    plt.gca().invert_xaxis()
    plt.xlabel("1 + z")
    

if __name__ == "__main__":
    main()