#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 10 14:21:11 2017
Calculates and plots properties like gas, stellar and baryon mass
@author: nh444
"""
from __future__ import print_function


def main():
    import numpy as np

    basedir = "/data/curie4/nh444/project1/boxes/"
    simdirs = [
#            "L40_512_LDRIntRadioEff_noBH/",
        # "L40_512_NoDutyRadioWeak/",
        # "L40_512_NoDutyRadioInt/",
        # "L40_512_DutyRadioWeak/",
        "L40_512_MDRIntRadioEff",
        # "",
    ]
    labels = ["FABLE", "No BHs", "Weak radio", "Stronger radio",
              "Quasar duty cycle", "Fiducial"]
    suffix = "_no_BHs"  # for plot filename, needs underscore
    usehighres = False
    
    zoombasedir = "/home/nh444/data/project1/zoom_ins/"
    zoomdirs = ["MDRInt/"]  # corresponding to each simdir
    zooms = [[
            "c96_MDRInt", "c128_MDRInt", "c160_MDRInt", "c192_MDRInt",
            "c224_MDRInt", "c256_MDRInt",
            "c288_MDRInt",
            "c320_MDRInt",
            "c352_MDRInt", "c384_MDRInt",
            "c448_MDRInt",
            "c480_MDRInt",
            "c512_MDRInt",
            "c112_MDRInt", "c144_MDRInt", "c176_MDRInt", "c208_MDRInt",
            "c240_MDRInt", "c272_MDRInt", "c304_MDRInt",
            "c336_MDRInt", "c368_MDRInt",
            "c400_MDRInt",
            "c410_MDRInt", "c432_MDRInt",
            "c464_MDRInt",
            "c496_MDRInt_SUBF",
            ]]

    outdir = "/data/curie4/nh444/project1/ICM/"
    snapnums = range(15, 26)
#    snapnums = range(4, 26)
#    snapnums = range(59, 100, 4)

    M500min = 1e12#10**11.6
    nthreads = 2
    h_scale = 0.6774
    calc_energies = False
    calc_temp = True
    fout_suffix = "_temp_only_02-10keV"

    # col = ["#c7673e","#78a44f","#6b87c8","#e6ab02","#be558f","#47af8d"]
    col = ["#c7673e", "#78a44f", "#6b87c8",
           "#934fce", "#e6ab02", "#be558f", "#47af8d"]
    col = ['#24439b', '#ED7600']  # blue orange
    ms = 8
    mew = 1.8
    errlw = 1.8
    hlightcol = '#8292BF'

#    save_ICM_masses(basedir, simdirs, snapnums, M500min=M500min,
#                    mpc_units=False, outdir=outdir, usehighres=False,
#                    calc_energies=calc_energies, calc_temp=calc_temp,
#                    fout_suffix=fout_suffix,
#                    nthreads=nthreads, h_scale=h_scale)
#    for i, zoomdir in enumerate(zoomdirs):
#        if zoomdir is not "":
#            save_ICM_masses(zoombasedir+zoomdir, zooms[i], snapnums,
#                            M500min=M500min, RminLowRes=5.0, mpc_units=True,
#                            outdir=outdir+zoomdir, usehighres=usehighres,
#                            calc_energies=calc_energies, calc_temp=calc_temp,
#                            fout_suffix=fout_suffix,
#                            show_temp=False,
#                            h_scale=h_scale, nthreads=nthreads)
    redshifts = np.arange(0.0, 2.1, 0.2)
#    plot_baryon_fracs(outdir, simdirs, zoomdirs, zooms, redshifts, scat_idx=[0], M500lims=[1e12, 1e15], plot_obs=True, outdir=outdir)
#    plot_Mgas_vs_redshift(outdir, simdirs, zoomdirs, zooms, redshifts,
#                          M500lims=[2.9e14, 1e16], labels=labels,
#                          col=col, outdir=outdir,
#                          ms=ms, mew=mew, errlw=errlw, hlightcol=hlightcol,
#                          h_scale=h_scale)
#    plot_Mgas_vs_mass(outdir, simdirs, zoomdirs, zooms, 0.0, labels=labels,
#                      M500lims=[5e12, 3e15], fractional=False,
#                      # ylim=[3e11, 3e14],
#                      col=col, outdir=outdir,
#                      ms=ms, mew=mew, errlw=0, hlightcol=hlightcol,
#                      h_scale=h_scale)
    plot_Mass_vs_redshift(outdir, simdirs, zoomdirs, zooms, redshifts,
                          M500lims=[2.9e14, 1e16], MainHalo=False,
                          outdir=outdir, h_scale=h_scale)
#    plot_mass_function(outdir, simdirs, zoomdirs, zooms,
#                       #redshifts, plot=False, dlog10m=0.01,
#                       [0.0, 1.0, 1.8], boxsize=65.0,
#                       #[0.0, 1.0, 1.8], compare_to_hmf=False,
#                       M500lims=[1e12, 3e15],
#                       h_scale=h_scale)
#    plot_energy_vs_redshift(outdir, simdirs, zoomdirs, zooms, redshifts,
#                            M500lims=[10**14, 10**14.5], #N=1,
#                            add_labels=True,
#                            plot_median=True,
#                            outdir=outdir, h_scale=h_scale)
#    for z in [0.0]:
#        compare_temp(outdir, simdirs, zoomdirs, zooms, z, labels=labels,
#                     fout_suffix=fout_suffix,
#                     col=col, outdir=outdir, plot_ratio=False,
#                     ms=ms, mew=mew, errlw=0, hlightcol=hlightcol)
#    for z in [0.0, 1.0]:
#        plot_relationship(outdir, simdirs, zoomdirs, zooms, z,
#                          "M500", "Tkin_r500",
#                          xlabel=r"$\mathrm{M_{500}}$",
#                          ylabel=r"$\mathrm{T_{kin, \,r_{500}}}$",
#                          M500lims=[2e13, 1e16],
#                          labels=labels, fout_suffix=fout_suffix,
#                          col=col, outdir=outdir,
#                          ms=ms, mew=mew, errlw=0, hlightcol=hlightcol)


def save_ICM_masses(basedir, simdirs, snapnums, M500min=0, Ngrps=1,
                    usehighres=True, RminLowRes=0.0, mpc_units=False,
                    calc_energies=True, calc_temp=True, show_temp=False,
                    fluxdir1=None, fluxdir2=None,
                    fout_base="ICM_", fout_suffix="",
                    outdir="/data/curie4/nh444/project1/ICM/",
                    nthreads=8, h_scale=0.6774):
    """
    Calculate various properties such as the masses of gas and stars and
    save them to a HDF5 file.
    Args:
        RminLowRes: Exclude FoF groups with Type 2 particles inside this
                    fraction of R500.
        calc_energies: Calculate the total kinetic and thermal energy.
        calc_temp: Calculate the global temperature: mass-weighted,
                   spectroscopic-like and emission-weighted.
        fluxdir1, fluxdir2: Location of the X-ray flux tables for
                            calculating the emission-weighted temperature.
                            Only used if calc_temp.
        show_temp: plot the temperature distribution of the gas weighted
                   by mass, flux etc.
        fout_base: The output file name will start with this string.
        fout_suffix: The output file name will end with this string.
    """
    import snap
    import numpy as np
    from scipy.spatial import cKDTree
    from timeit import default_timer as timer
    import readsnap as rs
    import os
    import h5py
    from scipy.constants import eV
    keV = eV * 1000.0
    if calc_temp:
        from scaling.xray_python import xray
        # Anders & Grevesse solar mass fractions
        ag_hydrogen = 0.7068
        ag_helium = 0.27431
        ag_metals = 0.01886
        from scipy.constants import m_p
    if show_temp:
        import matplotlib.pyplot as plt

    if fluxdir1 is None:
        fluxdir1 = ("/data/vault/nh444/ObsData/flux_tables/"
                    "flux_tables_apec_02-12keV/")
    if fluxdir2 is None:
        fluxdir2 = ("/data/vault/nh444/ObsData/flux_tables/"
                    "flux_tables_apec_02-12keV_redshifted/")

    for idx, simdir in enumerate(simdirs):
        if simdir[-1] != "/":
            simdir += "/"
        if not os.path.exists(outdir+simdir):
            os.makedirs(outdir+simdir)
        for snapidx, snapnum in enumerate(snapnums):
            sim = snap.snapshot(basedir+simdir, snapnum, mpc_units=mpc_units)
            z = sim.header.redshift
            h = sim.header.hubble
            t = sim.header.time
            print("Starting simdir", simdir, "at z = {:.2f}".format(z))
            print("Loading positions...")
            sim.load_gaspos()
            sim.load_starpos()
            print("Loading masses...")
            sim.load_gasmass()
            sim.load_starmass()
            print("Loading gas velocities...")
            sim.load_gasvel()
            if calc_energies:
                print("Loading stellar velocities...")
                sim.load_starvel()
                print("Loading internal energy of gas...")
                sim.load_u()
            if calc_temp:
                print("Calculating temperature...")
                sim.get_temp(del_u=False, del_ne=False)
                print("Loading metallicities...")
                sim.load_Zgas()
                print("Loading gas densities...")
                sim.load_gasrho()

            if usehighres:
                sim.load_highresgasmass()
                hrind = np.where(sim.hrgm > 0.5*sim.gasmass)[0]
                gaspos = sim.gaspos[hrind]
                gasmass = sim.gasmass[hrind]
                gasvel = sim.gasvel[hrind]
                if calc_energies:
                    u = sim.u[hrind]
                if calc_temp:
                    temp = sim.temp[hrind]
                    ne = sim.ne[hrind]
                    abund = (sim.Zgas[hrind] / (1.0-sim.Zgas[hrind]) *
                             (ag_hydrogen+ag_helium)/ag_metals)
                    gasrho = sim.gasrho[hrind]
            else:
                gaspos = sim.gaspos
                gasmass = sim.gasmass
                gasvel = sim.gasvel
                if calc_energies:
                    u = sim.u
                if calc_temp:
                    temp = sim.temp
                    ne = sim.ne
                    abund = (sim.Zgas / (1.0-sim.Zgas) *
                             (ag_hydrogen+ag_helium)/ag_metals)
                    gasrho = sim.gasrho

            # GFM_StellarFormationTime
            age = rs.read_block(sim.snapname, "GAGE",
                                cosmic_ray_species=sim.num_cr_species)
            # birth time <=0 is a wind particle (=0 if just recoupled)
            starind = np.where(age > 0.0)[0]
            windind = np.where(age <= 0.0)[0]

            print("Constructing k-d trees (may take a while)...")
            starttime = timer()
            # leafsize is approx no of particles at which kd-tree
            # switches to brute-force.
            kdtree_gas = cKDTree(gaspos, leafsize=20,
                                 boxsize=sim.box_sidelength,
                                 balanced_tree=False, compact_nodes=False)
            # setting balanced_tree and compact_nodes to False makes
            # the construction much faster (~5 times) and doesn't
            # affect the final plot.
            print("Done. k-d tree for gas took",
                  timer() - starttime, "seconds.")
            starttime = timer()
            kdtree_star = cKDTree(sim.starpos[starind], leafsize=5,
                                  boxsize=sim.box_sidelength,
                                  balanced_tree=True, compact_nodes=True)
            print("Done. k-d tree for stars took",
                  timer() - starttime, "seconds.")
            starttime = timer()
            kdtree_wind = cKDTree(sim.starpos[windind], leafsize=5,
                                  boxsize=sim.box_sidelength,
                                  balanced_tree=True, compact_nodes=True)
            print("Done. k-d tree for wind took",
                  timer() - starttime, "seconds.")

            print("Calculating properties...")
            Mgas_r500 = []
            Mstar_r500 = []
            Mwind_r500 = []
            if calc_energies:
                KEgas_r500 = []
                KEstars_r500 = []
                Ugas_r500 = []
            if calc_temp:
                Tmw_r500_all = []  # mass-weighted temperature (all gas)
                Tmw_r500_warm = []  # mass-weighted temperature (warm gas)
                Tmw_r500_hot = []  # mass-weighted temperature (hot gas)
                Temw_r500 = []  # emission measure-weighted temperature
                Tsl_r500 = []  # spectroscopic-like temperature
                Tew_r500 = []  # emission-weighted temperature for fluxdir1
                # emission-weighted temperature assuming 0.3 solar metallicity
                Tew_r500_Zfixed = []
                Tew_r500_2 = []  # emission-weighted temperature for fluxdir2
                Tkin_r500 = []  # effective temperature of non-thermal pressure
            if M500min > 0:
                grpind = np.where(sim.cat.group_m_crit500 *
                                  1e10/h_scale > M500min)[0]
            else:
                grpind = range(Ngrps)

            # Ignore groups with low res contamination
            if RminLowRes > 0:
                # Calculate distance of Type 2 particles
                sim.load_posarr(parttypes=[2])
                new_grpind = []
                for group in grpind:
                    # get distance of Type 2 particles relative to group
                    r = sim.nearest_dist_3D(sim.posarr['type2'],
                                            sim.cat.group_pos[group])
                    # keep group if nearest Type 2 is further
                    # than RminLowRes*R500 away
                    if np.min(r) > RminLowRes * sim.cat.group_r_crit500[group]:
                        new_grpind.append(group)
                # Make sure group 0 is in the list regardless of contamination
                if 0 not in new_grpind:
                    new_grpind.append(0)
                grpind = new_grpind

            # I'm going to do this one group at a time to save memory,
            # else I could use cKDTree.query_ball_tree to return a list
            # of indices for every group in one go.
            for group in grpind:
                # Get indices for gas within r500 of this group.
                kdind = kdtree_gas.query_ball_point(
                        sim.cat.group_pos[group],
                        sim.cat.group_r_crit500[group], n_jobs=nthreads)
                # Convert to a numpy array so that we can index it with a list.
                kdind = np.asarray(kdind)
                Mgas_r500.append(np.sum(gasmass[kdind]))

                if calc_energies:
                    # Total kinetic energy in cluster restframe:
                    KEgas_r500.append(0.5 * np.sum(gasmass[kdind] * np.sum(
                        (gasvel[kdind] - sim.cat.group_vel[group] / t)**2,
                        axis=1)))
                    # Total thermal energy:
                    Ugas_r500.append(np.sum(gasmass[kdind] * u[kdind]))

                if calc_temp:
                    # Mass-weighted temperature for all gas.
                    Tmw_r500_all.append(np.sum(temp[kdind] * gasmass[kdind]) /
                                        np.sum(gasmass[kdind]))
                    # Exclude cold and multiphase gas.
                    Tind1 = kdind[np.where(
                            (temp[kdind] >= 3e4*8.617342e-8) &
                            (gasrho[kdind] < 0.000880623 / (1+z)**3))[0]]
                    # Mass-weighted temperature for warm gas.
                    Tmw_r500_warm.append(np.sum(temp[Tind1] * gasmass[Tind1]) /
                                         np.sum(gasmass[Tind1]))

                    # Kinetic temperature for warm gas.
                    # Get mean mass-weighted velocity in each coordinate (km/s)
                    vmean = np.sum(gasvel[Tind1] * gasmass[Tind1, None],
                                   axis=0) / np.sum(gasmass[Tind1])
                    # Get 3D gas velocities relative to the mean.
                    dv = gasvel[Tind1] - vmean
                    # Add the difference in Hubble velocity to each coordinate.
                    dv += (sim.cm.hubble_z(sim.header.redshift) *
                           sim.header.time *
                           sim.rel_pos(gaspos[Tind1],
                                       sim.cat.group_pos[group]))
                    # Get mass-weighted average of the 3D velocity dispersion
                    # as proxy for the 1-D dispersion (as in SUBFIND).
                    veldisp = np.sqrt(np.sum(gasmass[Tind1] *
                                             np.sum(dv**2.0, axis=1)) /
                                      (3.0 * np.sum(gasmass[Tind1])))
                    # Calculate effective kinetic temperature in keV
                    Tkin_r500.append(veldisp**2 * 1e6 * 0.59 * m_p / keV)
                    print("Tmw, Tkin =", Tmw_r500_warm[-1], Tkin_r500[-1])

                    # Emission measure-weighted temperature.
                    # (Need to exclude cold dense gas otherwise it dominates).
                    Temw_r500.append(np.sum(temp[Tind1] * gasmass[Tind1] *
                                            gasrho[Tind1] * ne[Tind1]) /
                                     np.sum(gasmass[Tind1] * gasrho[Tind1] *
                                            ne[Tind1]))
                    # Emission-weighted temperature.
                    # Get X-ray flux from tables.
                    src = xray.xray_source(z, h, sim.header.omega_m,
                                           fluxdir=fluxdir1, verbose=True)
                    # For Tew_r500 use the abundance of the gas.
                    flux = src.get_flux_from_table(
                            temp[Tind1], abund[Tind1], ne[Tind1],
                            gasmass[Tind1], gasrho[Tind1],
                            modelname="apec")
                    Tew_r500.append(np.sum(temp[Tind1] * flux) /
                                    np.sum(flux))
                    # For Tew_r500_Zfixed use constant 0.3 solar metallicity.
                    flux = src.get_flux_from_table(
                            temp[Tind1], np.full_like(abund[Tind1], 0.3),
                            ne[Tind1], gasmass[Tind1], gasrho[Tind1],
                            modelname="apec")
                    Tew_r500_Zfixed.append(np.sum(temp[Tind1] * flux) /
                                           np.sum(flux))
                    # Similarly for the second set of flux tables.
                    src = xray.xray_source(z, h, sim.header.omega_m,
                                           fluxdir=fluxdir2, verbose=True)
                    flux = src.get_flux_from_table(
                            temp[Tind1], abund[Tind1], ne[Tind1],
                            gasmass[Tind1], gasrho[Tind1],
                            modelname="apec")
                    Tew_r500_2.append(np.sum(temp[Tind1] * flux) /
                                      np.sum(flux))
                    # Spectroscopic-like temperature
                    # Ignore T < 0.3 keV as in Truong+18
                    if len(np.where(temp[kdind] > 0.3)[0]) > 0:
                        Tind2 = kdind[np.where(temp[kdind] > 0.3)[0]]
                        Tsl_r500.append(np.sum(gasrho[Tind2] * gasmass[Tind2] *
                                               temp[Tind2]**0.25) /
                                        np.sum(gasrho[Tind2] * gasmass[Tind2] *
                                               temp[Tind2]**-0.75))
                        # Mass-weighted temperature for hot gas.
                        Tmw_r500_hot.append(np.sum(temp[Tind2] *
                                                   gasmass[Tind2]) /
                                            np.sum(gasmass[Tind2]))
                    else:
                        Tsl_r500.append(0.0)
                        Tmw_r500_hot.append(0.0)
                    if show_temp:
                        fig, ax = plt.subplots(figsize=(7, 7))
                        ax.set_xlabel("Temperature [keV]")
                        temp_bins = np.linspace(0.0, 12.0, 241, endpoint=True)
                        ax.hist(temp[kdind], bins=temp_bins, normed=True,
                                fc='none', ec='red', label="N")
                        ax.hist(temp[kdind], bins=temp_bins,
                                weights=gasmass[kdind], normed=True,
                                fc='none', ec='green', label="mass")
                        ax.axvline(Tmw_r500_all[-1], c='green', ls='--')
                        ax.hist(temp[Tind1], bins=temp_bins,
                                weights=(gasmass[Tind1] * gasrho[Tind1] *
                                         ne[Tind1]),
                                normed=True, fc='none', ec='blue', label="EM")
                        ax.axvline(Temw_r500[-1], c='blue', ls='--')
                        ax.hist(temp[Tind1], bins=temp_bins,
                                weights=flux, normed=True,
                                fc='none', ec='orange', label="flux")
                        ax.axvline(Tew_r500[-1], c='orange', ls='--')
                        ax.axvline(Tew_r500_Zfixed[-1], c='orange',
                                   ls='dashdot')
                        ax.axvline(Tew_r500_2[-1], c='orange', ls='dotted')
                        ax.legend()

                # Get indices for stars within r500 of this group.
                kdind = kdtree_star.query_ball_point(
                    sim.cat.group_pos[group], sim.cat.group_r_crit500[group],
                    n_jobs=nthreads)
                Mstar_r500.append(np.sum(sim.starmass[starind[kdind]]))
                if calc_energies:
                    KEstars_r500.append(
                        0.5 * np.sum(sim.starmass[starind[kdind]] *
                                     np.sum((sim.starvel[starind[kdind]] -
                                             sim.cat.group_vel[group] / t)**2,
                                            axis=1)))
                kdind = kdtree_wind.query_ball_point(
                    sim.cat.group_pos[group], sim.cat.group_r_crit500[group],
                    n_jobs=nthreads)
                Mwind_r500.append(np.sum(sim.starmass[windind[kdind]]))
            del kdtree_gas
            del kdtree_star
            del kdtree_wind
            # Convert lists to numpy arrays with desired units.
            Mgas_r500 = np.asarray(Mgas_r500) * 1e10  # Msun/h
            Mstar_r500 = np.asarray(Mstar_r500) * 1e10  # Msun/h
            Mwind_r500 = np.asarray(Mwind_r500) * 1e10  # Msun/h
            if calc_energies:
                KEgas_r500 = np.asarray(KEgas_r500) * 1e10
                KEstars_r500 = np.asarray(KEstars_r500) * 1e10
                Ugas_r500 = np.asarray(Ugas_r500) * 1e10
            if calc_temp:
                # Convert lists to numpy arrays.
                Tmw_r500_all = np.asarray(Tmw_r500_all)  # keV
                Tmw_r500_warm = np.asarray(Tmw_r500_warm)  # keV
                Tmw_r500_hot = np.asarray(Tmw_r500_hot)  # keV
                Temw_r500 = np.asarray(Temw_r500)  # keV
                Tsl_r500 = np.asarray(Tsl_r500)  # keV
                Tew_r500 = np.asarray(Tew_r500)  # keV
                Tew_r500_Zfixed = np.asarray(Tew_r500_Zfixed)  # keV
                Tew_r500_2 = np.asarray(Tew_r500_2)  # keV
                Tkin_r500 = np.asarray(Tkin_r500)  # keV

            # Calculate total kinetic energy of DM particles.
            if calc_energies:
                # First free up some memory be deleting the references to the
                # already loaded numpy arrays. Garbage collection should then
                # free up the associated memory.
                del gaspos
                del sim.gaspos
                del gasmass
                del sim.gasmass
                if calc_energies:
                    del gasvel
                    del sim.gasvel
                    del u
                    del sim.u
                if calc_temp:
                    del temp
                    del sim.temp
                    del gasrho
                    del sim.gasrho
                    del ne
                    del sim.ne
                    del abund
                    del sim.Zgas

                DMpos = rs.read_block(
                    sim.snapname, "POS ", parttype=1,
                    cosmic_ray_species=sim.num_cr_species)
                if mpc_units:
                    DMpos *= 1000.0
                DMmass = rs.read_block(
                    sim.snapname, "MASS", parttype=1,
                    cosmic_ray_species=sim.num_cr_species)
                print("Constructing k-d tree for dark matter...")
                starttime = timer()
                # leafsize is approx no of particles at which kd-tree
                # switches to brute-force.
                kdtree_DM = cKDTree(DMpos, leafsize=20,
                                    boxsize=sim.box_sidelength,
                                    balanced_tree=False, compact_nodes=False)
                print("Done. k-d tree for DM took",
                      timer() - starttime, "seconds.")
                del DMpos
                DMvel = rs.read_block(sim.snapname, "VEL ",
                                      physical_velocities=True,
                                      parttype=1,
                                      cosmic_ray_species=sim.num_cr_species)
                M_DM_r500 = []
                KEdm_r500 = []
                for group in grpind:
                    kdind = kdtree_DM.query_ball_point(
                        sim.cat.group_pos[group],
                        sim.cat.group_r_crit500[group], n_jobs=nthreads)
                    M_DM_r500.append(np.sum(DMmass[kdind]))
                    KEdm_r500.append(
                        0.5 * np.sum(DMmass[kdind] *
                                     np.sum((DMvel[kdind] -
                                             sim.cat.group_vel[group] / t)**2,
                                            axis=1)))
                M_DM_r500 = np.asarray(M_DM_r500) * 1e10
                KEdm_r500 = np.asarray(KEdm_r500) * 1e10

            print("Saving...")
            suffix = "{:.2f}".format(z)
            with h5py.File(outdir + simdir + fout_base + "z" +
                           suffix.zfill(5) + fout_suffix + ".hdf5", "w") as f:
                f.attrs['redshift'] = z
                f.attrs['h'] = h
                f.attrs['MassUnits'] = "Msun/h"
                f.attrs['DistUnit'] = "kpc/h"
                f.attrs['RminLowRes'] = RminLowRes

                f.create_dataset("GroupNum", data=np.c_[grpind])
                f.create_dataset("M500", data=np.c_[
                                 sim.cat.group_m_crit500[grpind] * 1e10])
                f.create_dataset("R500", data=np.c_[
                                 sim.cat.group_r_crit500[grpind]])
                f.create_dataset("M200", data=np.c_[
                                 sim.cat.group_m_crit200[grpind] * 1e10])
                f.create_dataset("R200", data=np.c_[
                                 sim.cat.group_r_crit200[grpind]])
                f.create_dataset("Mgas_r500", data=np.c_[Mgas_r500])
                f.create_dataset("Mwind_r500", data=np.c_[Mwind_r500])
                f.create_dataset("Mstar_r500", data=np.c_[Mstar_r500])
                if calc_energies:
                    f.create_dataset("M_DM_r500", data=np.c_[M_DM_r500])
                    f.create_dataset("KEgas_r500", data=np.c_[KEgas_r500])
                    f.create_dataset("KEstars_r500", data=np.c_[KEstars_r500])
                    f.create_dataset("KEdm_r500", data=np.c_[KEdm_r500])
                    f.create_dataset("Ugas_r500", data=np.c_[Ugas_r500])
                if calc_temp:
                    f.create_dataset("Tmw_r500", data=np.c_[Tmw_r500_all])
                    f.create_dataset("Tmw_r500_warm",
                                     data=np.c_[Tmw_r500_warm])
                    f.create_dataset("Tmw_r500_hot", data=np.c_[Tmw_r500_hot])
                    f.create_dataset("Temw_r500", data=np.c_[Temw_r500])
                    f.create_dataset("Tsl_r500", data=np.c_[Tsl_r500])
                    f.create_dataset("Tew_r500", data=np.c_[Tew_r500])
                    f.create_dataset("Tew_r500_Zfixed",
                                     data=np.c_[Tew_r500_Zfixed])
                    f.create_dataset("Tew_r500_2", data=np.c_[Tew_r500_2])
                    f.create_dataset("Tkin_r500", data=np.c_[Tkin_r500])

            print("Saved data to", outdir+simdir +
                  fout_base+"z"+suffix.zfill(5)+fout_suffix+".hdf5")


def plot_Mgas_vs_mass(indir, simdirs, zoomdirs, zooms, z, labels=None,
                      fout_base="ICM_", M500lims=[5e12, 3e15],
                      fractional=False, fgas=None, ylim=None,
                      col=None, hlightcol=None, ms=6.5, mew=1.2, errlw=1.2,
                      plot_obs=True, obs_dir="/data/vault/nh444/ObsData/",
                      plot_sims=True, plot_label=True,
                      do_fit=False, M500min_fit=1e14,
                      leg_kwargs={}, leg_len=None,
                      outdir="/data/curie4/nh444/project1/ICM/",
                      outfilename="gas_mass_vs_mass.pdf",
                      h_scale=0.6774):
    """Plot gas mass within r500 against total mass M500.

    Arguments:
        fractional: Plot gas mass fraction. Otherwise total gas mass.
        fgas: If not None, plot a line indicating a gas fraction
              of this value.
        plot_label: Add text indicating the aperture.
        leg_kwargs: dict of keyword arguments for plt.legend().
        leg_len: Maximum number of entries in first legend before
                 overflowing into a second legend.
    """
    import numpy as np
    import matplotlib.pyplot as plt
    import h5py
    import os

    if col is None:
        col = ["#c7673e", "#78a44f", "#6b87c8",
               "#934fce", "#e6ab02", "#be558f", "#47af8d"]

    fig, ax = plt.subplots(figsize=(7, 7))
    ax.set_xlabel(r"M$_{500}$ [M$_{\odot}$]")
    ax.set_xscale('log')
    if fractional:
        ax.set_ylabel(r"$\mathrm{M_{\mathrmrm{gas, r_{500}}}}$ / $\mathrm{M_{500}}$")
    else:
        ax.set_ylabel(r"$\mathrm{M_{\mathrm{gas, r_{500}}}}$ [M$_{\odot}$]")
        ax.set_yscale('log')
    ax.set_xlim(M500lims[0], M500lims[1])
    if ylim is not None:
        ax.set_ylim(ylim[0], ylim[1])

    if fgas is not None:
        ax.plot([M500lims[0], M500lims[1]],
                [M500lims[0]*fgas, M500lims[1]*fgas], lw=5,
                c='#8292BF', alpha=0.4,
                dashes=(0.2, 3), dash_capstyle='round',
                label=r"$f_{\mathrm{gas}} ="+"{:.3f}".format(fgas)+"$")

    if plot_sims and not fractional:
        simcol = '#F09C48'  # orange

        eagle = np.genfromtxt(obs_dir+"CEAGLE_all.csv", delimiter=",")
        plt.errorbar(10**eagle[:, 12]*0.6777/h_scale,  # true masses
                     10**eagle[:, 19]*0.6777/h_scale,
                     marker='o', mfc=simcol, mec='none',
                     ls='none', ms=ms, mew=mew, label="C-EAGLE")

    if plot_obs and not fractional:
        c1 = '0.6'  # X-ray hydrostatic halo masses
        c2 = '0.3'  # Weak lensing halo masses

        # Maughan+ 2008
        Mau = np.genfromtxt(obs_dir+"Maughan2008.csv", delimiter=",")
        ax.errorbar(Mau[:, 3]*1e14*0.7/h_scale,
                    Mau[:, 13]*1e13*0.7/h_scale,
                    yerr=[np.absolute(Mau[:, 15])*1e13*0.7/h_scale,
                          Mau[:, 14]*1e13*0.7/h_scale],
                    c=c1, mec=c1, marker='v', ms=ms, mew=mew,
                    ls='none', lw=errlw,
                    label="Maughan+ 2008")

        # Sun+ 2009 (0.012 < z < 0.12)
        Sun = np.genfromtxt(obs_dir+"mass_fracs/" +
                            "sun_group_x-ray_properties.txt",
                            usecols=np.arange(0, 24))
        ax.errorbar(Sun[:, 13]*1e13*0.73/h_scale,
                    Sun[:, 16]*Sun[:, 13]*1e13*0.73/h_scale,
                    xerr=[Sun[:, 15]*1e13*0.73/h_scale,
                          Sun[:, 14]*1e13*0.73/h_scale],
                    yerr=[Sun[:, 18]*Sun[:, 13]*1e13*0.73/h_scale,
                          Sun[:, 17]*Sun[:, 13]*1e13*0.73/h_scale],
                    c=c1, mec=c1, marker='^', ms=ms, mew=mew,
                    ls='none', lw=errlw, label="Sun+ 2009")

        # Gonzalez+ 2013
        gonz = np.genfromtxt(obs_dir+"mass_fracs/" +
                             "Gonzalez_2013_stellar_gas_masses.csv",
                             delimiter=",")
        ax.errorbar(gonz[:, 14]*0.702*1e14/h_scale,
                    gonz[:, 17]*0.702*1e13/h_scale,
                    xerr=gonz[:, 15]*0.702*1e14/h_scale,
                    yerr=gonz[:, 18]*0.702*1e13/h_scale,
                    c=c1, mec=c1, marker='o', ms=ms*1.2, mew=mew,
                    ls='none', lw=errlw, label="Gonzalez+ 2013")

        # Sanderson+ 2013 z=[0.0571, 0.0796, 0.0943]
        sand = np.genfromtxt(obs_dir + "mass_fracs/" +
                             "Sanderson_2013_gas_fracs.csv", delimiter=",")
        plt.errorbar(sand[:, 3]*0.7*1e14/h_scale,
                     sand[:, 11]*sand[:, 3] * 0.7 * 1e14 / h_scale,
                     xerr=sand[:, 4]*0.7/h_scale*1e14,
                     yerr=(sand[:, 11]*sand[:, 3] * 0.7 * 1e14 / h_scale *
                           np.sqrt((sand[:, 12] / sand[:, 11])**2 +
                                   (sand[:, 4] / sand[:, 3])**2)),
                     c=c1, mec=c1, marker='+', ms=ms*1.2, mew=mew,
                     ls='none', lw=errlw, label="Sanderson+ 2013")

        # Lovisari+ 2015 X-ray hydrostatic masses
        Lov = np.genfromtxt(obs_dir+"Lovisari2015.csv", delimiter=",")
        ax.errorbar(Lov[:, 7]*1e13*0.7/h_scale, Lov[:, 9]*1e12*0.7/h_scale,
                    xerr=Lov[:, 8]*1e13*0.7/h_scale,
                    yerr=Lov[:, 10]*1e12*0.7/h_scale,
                    c=c1, mec=c1, marker='*', ms=ms*1.6, mew=mew,
                    ls='none', lw=errlw, label="Lovisari+ 2015")

        # Eckert+ 2016 XXL - weak-lensing calibrated halo masses
        XXL = np.genfromtxt(obs_dir+"XXL.csv", delimiter=",")
#        from sim_python import cosmo
#        cm = cosmo.cosmology(0.7, 0.28)
#        arcmin_to_kpc = cm.arcsec_to_phys(60.0, XXL[:, 1]) * 1000.0 / 0.7
#        R = XXL[:, 5] / arcmin_to_kpc
#        np.savetxt(obs_dir+"XXL_r.csv", np.c_[XXL[:, 5], R], delimiter=",")
        # Eckert+ 16 only use objects with R500 < 8 arcmin
        ind = np.where(XXL[:, 26] <= 8.0)[0]
        ax.errorbar(XXL[ind, 8]*1e14*0.7/h_scale, XXL[ind, 11]*0.7/h_scale,
                    xerr=[XXL[ind, 10]*1e14*0.7/h_scale,
                          XXL[ind, 9]*1e14*0.7/h_scale],
                    yerr=[np.abs(XXL[ind, 13])*0.7/h_scale,
                          XXL[ind, 12]*0.7/h_scale],
                    c=c2, mec=c2, mfc='none', marker='s', ms=ms*0.8, mew=mew,
                    ls='none',  lw=errlw, label="Eckert+ 2016")

        # Chiu+ 2018 - These cover a wide redshift range so should
        # probably be rescaled with redshift. Although the same
        # applies to Maughan+ 2008
#        chiu = np.genfromtxt(obs_dir+"mass_fracs/"+"Chiu_2017.csv",
#                             delimiter=",")
#        plt.errorbar(chiu[:, 3]*1e14*0.68/h_scale,
#                     chiu[:, 5]*1e13*0.68/h_scale,
#                     xerr=chiu[:, 4]*1e14*0.68/h_scale,
#                     yerr=chiu[:, 6]*1e13*0.68/h_scale,
#                     marker='d', c=c1, mec=c1, ms=ms, mew=mew, ls='none',
#                     lw=errlw, label="Chiu+ 18")

    suffix = "{:.2f}".format(z)
    for simidx, simdir in enumerate(simdirs):
        if simdir[-1] != "/":
            simdir += "/"
        if labels is not None:
            label = labels[simidx]
        else:
            label = None
        filename = indir+simdir+fout_base+"z"+suffix.zfill(5)+".hdf5"
        if os.path.exists(filename):
            with h5py.File(filename, "r") as f:
                M500 = f['M500'].value[:, 0] / h_scale
                Mgas = f['Mgas_r500'].value[:, 0] / h_scale
                Mwind = f['Mwind_r500'].value[:, 0] / h_scale
                # Get haloes in mass range.
                ind = np.where((M500 > M500lims[0]) & (M500 < M500lims[1]))[0]
                # Filter out haloes with zero gas mass.
                ind = ind[np.where(Mgas[ind] > 0.0)[0]]
                M500 = M500[ind]
                # Total gas mass is Mgas plus mass of wind particles.
                if fractional:
                    Y = (Mgas[ind] + Mwind[ind]) / M500[ind]
                else:
                    Y = Mgas[ind] + Mwind[ind]
                ax.plot(M500, Y, ls='none',
                        marker='D', ms=ms, mew=mew,
                        mec=col[simidx], mfc=col[simidx],
                        label=label, zorder=4)
        else:
            print("File does not exist:", filename)

        if zoomdirs[simidx] is not "":
            for zidx, zoom in enumerate(zooms[simidx]):
                filename = indir+zoomdirs[simidx]+zoom + \
                    "/"+fout_base+"z"+suffix.zfill(5)+".hdf5"
                if os.path.exists(filename):
                    with h5py.File(filename) as f:
                        grpnum = f['GroupNum'].value[:, 0]
                        M500z = f['M500'].value[:, 0] / h_scale
                        Mgas = f['Mgas_r500'].value[:, 0] / h_scale
                        Mwind = f['Mwind_r500'].value[:, 0] / h_scale
                        # Get haloes in mass range.
                        ind = np.where((M500z >= M500lims[0]) &
                                       (M500z <= M500lims[1]))[0]
                        # Filter out haloes with zero gas mass.
                        ind = ind[np.where(Mgas[ind] > 0.0)[0]]
                        if fractional:
                            Yz = (Mgas[ind] + Mwind[ind]) / M500z[ind]
                        else:
                            Yz = Mgas[ind] + Mwind[ind]
                        M500 = np.concatenate((M500, M500z[ind]))
                        Y = np.concatenate((Y, Yz))
                        # Plot group 0
                        grp0 = (grpnum[ind] == 0)
                        ax.plot(M500z[ind[grp0]], Yz[grp0],
                                marker='D', ms=ms, mew=mew, ls='none',
                                mec=col[simidx], mfc=col[simidx],
                                zorder=4)
                        # plot groups >0 (secondary FoF haloes)
                        if hlightcol is None:
                            hlightcol = col[simidx]
                        ax.plot(M500z[ind[~grp0]], Yz[~grp0],
                                marker='D', ms=ms, mew=mew, ls='none',
                                mec=hlightcol, mfc=hlightcol,
                                zorder=3)
                else:
                    print("File does not exist:", filename)
        if do_fit:
            # Create the powerlaw function for a given pivot point X0
            from scaling.fit_relation import fit_powerlaw
            from scaling.fit_relation import make_powerlaw
            X0 = 4.8e14
            powerlaw = make_powerlaw(np.log10(X0))
            inislope = 1.3
            inilogC = 14.0
            ind = np.where(M500 >= M500min_fit)
            slope, slope_err, logC, logC_err, scat, scat_err = fit_powerlaw(
                M500[ind], Y[ind], X0, inislope, inilogC,
                bootstrap=True)
            print("slope = {:.3f} [{:.3f}, {:.3f}]".format(
                    slope, slope_err[0], slope_err[1]))
            print("logC = {:.3f} [{:.3f}, {:.3f}]".format(
                    logC, logC_err[0], logC_err[1]))
            print("scat = {:.3f} [{:.3f}, {:.3f}]".format(
                    scat, scat_err[0], scat_err[1]))
            ax.plot([M500lims[0], M500lims[1]],
                    [10**powerlaw(np.log10(M500lims[0]), slope, logC),
                     10**powerlaw(np.log10(M500lims[1]), slope, logC)],
                    c=col[simidx], lw=3)

    if plot_label:
        plt.text(0.9, 0.1, r"r$_{\mathrm{3D}} \leq$ r$_{500}$",
                 horizontalalignment='right', verticalalignment='center',
                 transform=plt.gca().transAxes)

    # Add legend(s).
    # define default ax.legend() kwargs.
    default_leg_kwargs = {'loc': 'upper left', 'borderaxespad': 1,
                          'fontsize': 'small'}
    # fill in any missing keys in leg_kwargs with the defaults.
    for key in default_leg_kwargs:
        if key not in leg_kwargs:
            leg_kwargs[key] = default_leg_kwargs[key]
    if leg_len is None:
        # Place all legend entries in one legend.
        ax.legend(**leg_kwargs)
    else:
        # Two legends. One upper left and the other lower right.
        del leg_kwargs['loc']
        handles, labels = ax.get_legend_handles_labels()
        leg = ax.legend(handles[:leg_len], labels[:leg_len],
                        loc='upper left', **leg_kwargs)
        ax.add_artist(leg)
        ax.legend(handles[leg_len:], labels[leg_len:],
                  loc='lower right', **leg_kwargs)
    fig.savefig(outdir+outfilename, bbox_inches='tight')
    print("Saved to", outdir+outfilename)


def plot_Mgas_vs_redshift(indir, simdirs, zoomdirs, zooms, redshifts,
                          M500lims_list=[[1e14, 1e16]], mass_slope=None,
                          fout_base="ICM_", do_fit=False, labels=None,
                          scat_idx=None, markers=None,
                          col=None, hlightcol=None, ms=6.5, mew=1.2, errlw=1.2,
                          plot_obs=True, obs_dir="/data/vault/nh444/ObsData/",
                          outdir="/data/curie4/nh444/project1/ICM/",
                          outfilename="gas_mass_vs_redshift.pdf",
                          h_scale=0.6774):
    """Plot gas mass within r500 versus redshift.

    The gas mass is scaled by the slope of the gas mass--total mass relation
    to take out the halo mass dependence.
    Arguments:
        M500lims_list: Iterable where each element is a list of the form
                      [M500min, M500max] where M500min and M500max are the
                      minimum and maximum halo masses of each sample. Each
                      sample is plotted and/or fit separately.
        mass_slope: Slope of the Mgas,r500-M500 relation for the simulation.
                    If None, it is acquired from a simultaneous fit to the
                    mass and redshift dependence of Mgas,r500.
    """
    import numpy as np
    import matplotlib.pyplot as plt
    import h5py
    import os
    if do_fit:
        from linear_regression_two_predictors import linreg_two_predictors

    get_mass_slope = False
    if mass_slope is None:
        get_mass_slope = True
    fig, ax = plt.subplots(figsize=(7, 7))
    if col is None:
        col = ["#c7673e", "#78a44f", "#6b87c8",
               "#934fce", "#e6ab02", "#be558f", "#47af8d"]
    if markers is None:
        markers = ["D", "s", "o", "^", ">", "v", "<"]
    if scat_idx is None:
        # Plot scatter for everything by default.
        scat_idx = range(len(simdirs) * len(M500lims_list))

    Mpivot = 4.8e14
    zpivot = 1.6

    ax.set_yscale('log')
    ax.set_xlabel("$1 + z$")
    ax.set_ylabel(r"$\mathrm{M_{\mathrm{gas, r_{500}}}(\frac{M_{500}}{10^{" +
                  r"{:.1f}".format(np.log10(Mpivot)) +
                  r"} M_{\odot}})^{-B}}$ [M$_{\odot}$]")
    ax.set_ylim(2.1e13, 2.1e14)
    # Set limits to just below and just above min and max redshift.
    ax.set_xlim(min(redshifts + 1.0)-0.05, max(redshifts + 1.0)+0.05)
    # Set tick spacing to 0.2
    xmin, xmax = ax.get_xlim()
    ax.xaxis.set_ticks(np.arange(min(redshifts + 1.0),
                                 max(redshifts + 1.0), 0.2))

    if plot_obs:
        # Plot observational data
        c1 = '0.6'
        # c2 = '0.3'

        # Maughan+ 2008 - need to calculate errors properly (See below)
#        Mau = np.genfromtxt(obs_dir+"Maughan2008.csv", delimiter=",")
#        norm = (Mau[:, 3]*1e14*0.7/h_scale / Mpivot)**B_maughan
#        ax.errorbar(Mau[:, 1]+1.0,
#                    Mau[:, 13]*1e13*0.7/h_scale * norm,
#                    yerr=[np.absolute(Mau[:, 15])*1e13*0.7/h_scale * norm,
#                          Mau[:, 14]*1e13*0.7/h_scale * norm],
#                    c=c1, mec=c1, marker='v', ms=ms, mew=mew,
#                    ls='none', lw=errlw, label="Maughan+ 2008")

        # Gonzalez+ 2013
        B_gonz = 1.26  # Slope of Mgas-M relation
        gonz = np.genfromtxt(obs_dir+"mass_fracs/" +
                             "Gonzalez_2013_stellar_gas_masses.csv",
                             delimiter=",")
        # Restrict to clusters in mass range.
        m_gonz = gonz[:, 14]*0.702*1e14/h_scale  # M500
        ind = np.where((m_gonz >= M500lims_list[0][0]) &
                       (m_gonz < M500lims_list[0][1]))
        norm = (gonz[ind, 14]*0.702*1e14/h_scale / Mpivot)**-B_gonz
        ax.errorbar(gonz[ind, 1]+1.0,
                    gonz[ind, 17]*0.702*1e13/h_scale * norm,
                    # xerr=gonz[ind, 15]*0.702*1e14/h_scale,
                    yerr=(gonz[ind, 17]*0.702*1e13/h_scale * norm *
                          np.sqrt((gonz[ind, 18]/gonz[ind, 17])**2 +
                                  (B_gonz*gonz[ind, 15]/gonz[ind, 14])**2)),
                    c=c1, mec=c1, mfc='none', marker='o', ms=ms, mew=mew,
                    ls='none', lw=errlw,
                    label="Gonzalez+ 2013 ($B = "+"{:.2f}".format(B_gonz)+"$)")

        # Chiu+ 2018
        B_chiu = 1.33
        A_chiu = 5.69e13
        C_chiu = -0.15
        chiu = np.genfromtxt(obs_dir+"mass_fracs/"+"Chiu_2017.csv",
                             delimiter=",")
        norm = (chiu[:, 3]*1e14*0.68/h_scale / Mpivot)**-B_chiu
        ax.errorbar(chiu[:, 1]+1.0, chiu[:, 5]*1e13*0.68/h_scale * norm,
                    yerr=(chiu[:, 5]*1e13*0.68/h_scale * norm *
                          np.sqrt((chiu[:, 6]/chiu[:, 5])**2 +
                                  (B_chiu*chiu[:, 4]/chiu[:, 3])**2)),
                    marker='d', c=c1, mec=c1, ms=ms, mew=mew, lw=errlw,
                    capsize=0, ls='none',
                    label="Chiu+ 18 ($B = "+"{:.2f}".format(B_chiu)+"$)")
        # Best-fit
        x = np.array(ax.get_xlim())  # 1+z
        ax.errorbar(x, A_chiu * 0.68 / h_scale * ((1+x)/1.6)**C_chiu,
                    c=c1, ls='--', lw=3, label="Chiu+ 18 best-fit")

    for simidx, simdir in enumerate(simdirs):
        simdir = os.path.normpath(simdir)+"/"
        for Midx, M500lims in enumerate(M500lims_list):
            idx = simidx * len(M500lims_list) + Midx
            for zidx, z in enumerate(redshifts):
                suffix = "{:.2f}".format(z)
                filename = indir+simdir+fout_base+"z"+suffix.zfill(5)+".hdf5"
                if not os.path.exists(filename):
                    raise IOError("File does not exist: "+str(filename))
                with h5py.File(filename, "r") as f:
                    M500 = f['M500'].value[:, 0] / h_scale
                    Mgas = f['Mgas_r500'].value[:, 0] / h_scale
                    Mwind = f['Mwind_r500'].value[:, 0] / h_scale
                    groupind = np.where(
                        (M500 > M500lims[0]) & (M500 < M500lims[1]))[0]
                    ind = np.intersect1d(np.where(Mgas > 0.0)[0], groupind,
                                         assume_unique=True)
                    x = np.full_like(M500[ind], f.attrs['redshift']+1.0)
                    if zidx == 0:
                        # Initialise data arrays.
                        x_all = x  # 1+z
                        Mgas_all = Mgas[ind] + Mwind[ind]
                        M500_all = M500[ind]
                        # Boolean mask - true if halo is secondary halo of zoom
                        secondary_halo = np.zeros_like(M500[ind], dtype=bool)
                    else:
                        x_all = np.concatenate((x_all, x))
                        Mgas_all = np.concatenate(
                                (Mgas_all, Mgas[ind] + Mwind[ind]))
                        M500_all = np.concatenate((M500_all, M500[ind]))
                        secondary_halo = np.concatenate((
                                secondary_halo, np.zeros_like(M500[ind],
                                                              dtype=bool)))

                if zoomdirs[simidx] is not "":
                    for zoomidx, zoom in enumerate(zooms[simidx]):
                        filename = indir+zoomdirs[simidx]+zoom + \
                            "/"+fout_base+"z"+suffix.zfill(5)+".hdf5"
                        if not os.path.exists(filename):
                            print("Skipping zoom", zoom)
                        else:
                            with h5py.File(filename) as f:
                                grpnum = f['GroupNum'].value[:, 0]
                                M500 = f['M500'].value[:, 0] / h_scale
                                Mgas = f['Mgas_r500'].value[:, 0] / h_scale
                                Mwind = f['Mwind_r500'].value[:, 0] / h_scale
                                ind = np.where((M500 >= M500lims[0]) &
                                               (M500 <= M500lims[1]))[0]
                                x = np.full_like(M500[ind],
                                                 f.attrs['redshift']+1.0)
                                grp0 = np.array((grpnum[ind] == 0), dtype=bool)
                                if len(ind) > 0:
                                    x_all = np.concatenate((x_all, x))
                                    Mgas_all = np.concatenate(
                                            (Mgas_all, Mgas[ind] + Mwind[ind]))
                                    M500_all = np.concatenate(
                                            (M500_all, M500[ind]))
                                    secondary_halo = np.concatenate((
                                            secondary_halo, ~grp0))
# =============================================================================
#             y = (M500_all / Mpivot)**-mass_slope * Mgas_all
#             ax.plot(x_all, y,
#                     marker='D', ms=ms, mew=mew, ls='none',
#                     mec=col[idx], mfc='none',
#                     zorder=4)
# =============================================================================
            if do_fit or get_mass_slope:
                xlim = np.array([xmin, xmax])
                samples = linreg_two_predictors(
                            np.log10(M500_all / Mpivot),
                            np.log10(x_all / zpivot), np.log10(Mgas_all),
                            [13.8, 1.0, 0.1, 0.05], plot=False,
                            bounds=((12, 14), (0.1, 3.5), (-4, 4), (0.0, np.inf)))
                # Get the median and uncertainties from the 16th, 50th and 84th
                # percentiles of the samples in the marginalised distributions.
                logA, B, C, scat = map(
                        lambda v: (v[1], v[2]-v[1], v[1]-v[0]),
                        zip(*np.percentile(samples, [16, 50, 84], axis=0)))
                print("logA =", logA)
                print("B =", B)
                print("C =", C)
                print("scat =", scat)
                if get_mass_slope:
                    mass_slope = B[0]
            # Determine the legend label.
            if len(M500lims_list) > 1:
                label = (labels[simidx] + r" $M_{500} = " +
                         r"{:.1f}".format(M500lims_list[Midx][0] / 1e14) +
                         r"$--${:.1f}".format(M500lims_list[Midx][1] / 1e14) +
                         r" \times 10^{14} M_{\odot}$")
            else:
                label = labels[simidx]
            label += " ($B = "+"{:.2f}".format(mass_slope)+"$)"
            if idx in scat_idx:
                if do_fit:
                    # Add label to best-fit line below instead of the points.
                    scatter_label = None
                else:
                    scatter_label = label
                y = Mgas_all * (M500_all / Mpivot)**-mass_slope
                ax.plot(x_all[~secondary_halo], y[~secondary_halo],
                        marker=markers[idx], mew=mew, c=col[idx],
                        mfc=col[idx] if Midx == 0 else 'none',
                        mec=col[idx], ms=ms, ls='none',
                        zorder=4, label=scatter_label)
                ax.plot(x_all[secondary_halo], y[secondary_halo],
                        marker=markers[idx], mew=mew, c=hlightcol,
                        mfc=hlightcol if Midx == 0 else 'none',
                        mec=hlightcol, ms=ms, ls='none',
                        zorder=4)
            if do_fit:
                # use a subset of the posterior samples to plot the
                # uncertainties on the best-fit line in data space.
                num = 10000  # size of subset to use.
                xbins = np.linspace(xlim[0], xlim[1], num=100)
                models = np.zeros((len(xbins), num))
                for j, i in enumerate(np.random.choice(np.arange(samples.shape[0]), num, replace=False)):
                    models[:, j] = 10**(samples[i, 0] + samples[i, 2]*np.log10(xbins / zpivot))
                # Plot the median and 68% confidence interval
                ax.fill_between(xbins, np.percentile(models, 16, axis=1),
                                np.percentile(models, 84, axis=1),
                                facecolor=col[idx], alpha=0.65, label=label,
                                zorder=4-Midx)
    ax.legend(loc='best', fontsize='x-small', borderaxespad=1,
              frameon=False, borderpad=0.6)
    plt.tight_layout()
    fig.savefig(outdir+outfilename, bbox_inches='tight')
    print("Saved to", outdir+outfilename)


def plot_energy_vs_redshift(indir, simdirs, zoomdirs, zooms, redshifts,
                            fout_base="ICM_",
                            gas_only=True, plot_median=False,
                            ms=6.5, M500lims=[1e8, 1e16], N=0,
                            add_labels=False,
                            obs_dir="/data/vault/nh444/ObsData/",
                            outdir="/data/curie4/nh444/project1/ICM/",
                            outfilename="energy_ratio_vs_redshift.pdf",
                            h_scale=0.6774):
    """
    Plot ratio of total thermal and total kinetic energy (in group rest frame)
    as a function of redshift
    Args:
        indir: directory containing ICM mass data saved by save_ICM_masses().
        gas_only: only consider kinetic energy of the gas
        plot_median: plot the median of all points,
                     otherwise plot individual haloes.
        M500lims: min and max M500 (Msun) to plot (at each redshift)
        N: N most massive haloes (by M500) to plot (applies after M500 limits)
        add_labels: label each halo on the plot
    """
    import numpy as np
    import matplotlib.pyplot as plt
    import h5py
    import os

    fig, ax = plt.subplots(figsize=(7, 7))
    col = ["#c7673e", "#78a44f", "#6b87c8",
           "#934fce", "#e6ab02", "#be558f", "#47af8d"]

    ax.set_xlabel("$z$")
    if gas_only:
        ax.set_ylabel(r"KE$_{\mathrm{500, gas}}$ / U$_{500}$")
    else:
        ax.set_ylabel(r"U$_{500}$ / KE$_{\mathrm{500, tot}}$")
    ax.set_ylim(ymin=0)

    if plot_median:
        med = []
        z_list = []
    for simidx, simdir in enumerate(simdirs):
        if simdir is not "":
            if simdir[-1] != "/":
                simdir += "/"
        for zidx, z in enumerate(redshifts):
            if plot_median:
                y_list = []
            suffix = "{:.2f}".format(z)
            filename = indir+simdir+fout_base+"z"+suffix.zfill(5)+".hdf5"
            if os.path.exists(filename):
                with h5py.File(filename, "r") as f:
                    M500 = f['M500'].value[:, 0] / h_scale
                    groupind = np.where(
                        (M500 > M500lims[0]) & (M500 < M500lims[1]))[0]
                    if N > 0:
                        groupind = groupind[np.argsort(M500[groupind])[-N:]]
                    U = f['Ugas_r500'].value[:, 0]
                    if gas_only:
                        KE = f['KEgas_r500'].value[:, 0]
                        y = KE / U
                    else:
                        KE = f['KEdm_r500'].value[:, 0] + \
                            f['KEgas_r500'].value[:, 0] + \
                            f['KEstars_r500'].value[:, 0]
                        y = U / KE
                    if plot_median:
                        y_list = y[groupind]
                    zarr = np.full_like(y[groupind], f.attrs['redshift'])
                    ax.plot(zarr, y[groupind], marker='D',
                            ls='none', mec=col[simidx], mfc='none', ms=ms)
                    if add_labels:
                        groupnums = f['GroupNum'].value[groupind, 0]
                        for i, groupnum in enumerate(groupnums):
                            ax.annotate(str(groupnum),
                                        xy=(zarr[i], y[groupind[i]]),
                                        xytext=(6, 0),
                                        textcoords='offset points',
                                        color=col[simidx], fontsize=10)

            if zoomdirs[simidx] is not "":
                for zidx, zoom in enumerate(zooms[simidx]):
                    filename = indir+zoomdirs[simidx]+zoom + \
                        "/"+fout_base+"z"+suffix.zfill(5)+".hdf5"
                    if os.path.exists(filename):
                        with h5py.File(filename) as f:
                            M500 = f['M500'].value[:, 0] / h_scale
                            groupind = np.where(
                                (M500 > M500lims[0]) & (M500 < M500lims[1]))[0]
                            if N > 0:
                                groupind = groupind[np.argsort(
                                    M500[groupind])[-N:]]
                            U = f['Ugas_r500'].value[:, 0]
                            if gas_only:
                                KE = f['KEgas_r500'].value[:, 0]
                                y = KE / U
                            else:
                                KE = f['KEdm_r500'].value[:, 0] + \
                                    f['KEgas_r500'].value[:, 0] + \
                                    f['KEstars_r500'].value[:, 0]
                                y = U / KE
                            if plot_median:
                                y_list = np.concatenate((y_list, y[groupind]))
                            zarr = np.full_like(
                                y[groupind], f.attrs['redshift'])
                            ax.plot(zarr, y[groupind], marker='D', ls='none',
                                    mec=col[simidx], mfc=col[simidx], ms=ms)
                            if add_labels:
                                groupnums = f['GroupNum'].value[groupind, 0]
                                for i, groupnum in enumerate(groupnums):
                                    ax.annotate(zoom.split("_")[0],
                                                xy=(zarr[i], y[groupind[i]]),
                                                xytext=(6, 0),
                                                textcoords='offset points',
                                                color=col[simidx], fontsize=10)
            if plot_median:
                if len(y_list) > 0:
                    print("z =", z, y_list)
                    med.append(np.median(y_list))
                    z_list.append(z)
        if plot_median:
            ax.plot(z_list, med, c=col[simidx], lw=3)

    ax.legend(loc='best', fontsize='small', frameon=True, borderpad=0.5)
    plt.tight_layout()
    fig.savefig(outdir+outfilename, bbox_inches='tight')
    print("Saved to", outdir+outfilename)


def plot_Mass_vs_redshift(indir, simdirs, zoomdirs, zooms, redshifts,
                          fout_base="ICM_", M500lims=[1e8, 1e16],
                          MainHalo=False, add_labels=True, ms=6.5,
                          obs_dir="/data/vault/nh444/ObsData/",
                          outdir="/data/curie4/nh444/project1/ICM/",
                          outfilename="mass_vs_redshift.pdf",
                          h_scale=0.6774):
    """
    Plot halo mass as a function of redshift
    Args:
        indir: directory containing ICM mass data saved by save_ICM_masses().
        M500lims: min and max M500 (Msun) to plot (at each redshift)
        MainHalo: plot only the main halo in each zoom
        add_labels: label each halo on the plot
    """
    import numpy as np
    import matplotlib.pyplot as plt
    import h5py
    import os

    fig, ax = plt.subplots(figsize=(7, 7))
    col = ["#c7673e", "#78a44f", "#6b87c8",
           "#934fce", "#e6ab02", "#be558f", "#47af8d"]

    ax.set_yscale('log')
    ax.set_xlabel("$z$")
    ax.set_ylabel(r"M$_{500}$ [M$_{\odot}$]")

    # Plot observational data
    c = '0.7'
    chiu = np.genfromtxt(obs_dir+"mass_fracs/"+"Chiu_2017.csv", delimiter=",")
    plt.errorbar(chiu[:, 1], chiu[:, 3]*1e14*0.68/h_scale,
                 yerr=chiu[:, 4]*1e14*0.68/h_scale,
                 marker='d', c=c, mec=c, capsize=0, ls='none', label="Chiu+17")

    stott = np.genfromtxt(
        obs_dir+"stellar_mass/Stott_2011_profiles.csv", delimiter=",")
    M200toM500 = 1.0/1.5
    ind = np.where(stott[:, 1] > 0.8)[0]
    plt.errorbar(stott[ind, 1], stott[ind, 9]*1e14*M200toM500*0.7/h_scale,
                 yerr=stott[ind, 10]*1e14*M200toM500*0.7/h_scale,
                 marker='o', c='k', mec='k', ls='none', label="Stott+11")

    for zidx, z in enumerate(redshifts):
        suffix = "{:.2f}".format(z)

        for simidx, simdir in enumerate(simdirs):
            if simdir is not "":
                if simdir[-1] != "/":
                    simdir += "/"
            filename = indir+simdir+fout_base+"z"+suffix.zfill(5)+".hdf5"
            if os.path.exists(filename):
                with h5py.File(filename, "r") as f:
                    M500 = f['M500'].value[:, 0] / h_scale
                    groupind = np.where(
                        (M500 > M500lims[0]) & (M500 < M500lims[1]))[0]
                    y = M500[groupind]
                    zarr = np.full_like(y, f.attrs['redshift'])
                    ax.plot(zarr, y, marker='D', ls='none',
                            mec=col[simidx], mfc='none', ms=ms)
                    if add_labels:
                        groupnums = f['GroupNum'].value[groupind, 0]
                        for i, groupnum in enumerate(groupnums):
                            ax.annotate(str(groupnum),
                                        xy=(zarr[i], y[i]),
                                        xytext=(6, 0),
                                        textcoords='offset points',
                                        color=col[simidx], fontsize=10)

            if zoomdirs[simidx] is not "":
                for zidx, zoom in enumerate(zooms[simidx]):
                    filename = indir+zoomdirs[simidx]+zoom + \
                        "/"+fout_base+"z"+suffix.zfill(5)+".hdf5"
                    if os.path.exists(filename):
                        with h5py.File(filename) as f:
                            M500 = f['M500'].value[:, 0] / h_scale
                            if MainHalo:
                                groupind = np.where(
                                    f['GroupNum'].value[:, 0] == 0)[0]
                            else:
                                groupind = np.where((M500 > M500lims[0]) &
                                                    (M500 < M500lims[1]))[0]
                            y = M500[groupind]
                            zarr = np.full_like(y, f.attrs['redshift'])
                            ax.plot(zarr, y, marker='D', ls='none',
                                    mec=col[simidx], mfc=col[simidx], ms=ms)
                            if add_labels:
                                groupnums = f['GroupNum'].value[groupind, 0]
                                for i, groupnum in enumerate(groupnums):
                                    ax.annotate(zoom.split("_")[0],
                                                xy=(zarr[i], y[i]),
                                                xytext=(6, 0),
                                                textcoords='offset points',
                                                color=col[simidx], fontsize=10)

        handles, leglabels = ax.axes.get_legend_handles_labels()
        ax.legend(loc='best', fontsize='small', frameon=True, borderpad=0.5)
    plt.tight_layout()
    fig.savefig(outdir+outfilename, bbox_inches='tight')
    print("Saved to", outdir+outfilename)


def plot_mass_function(indir, simdirs, zoomdirs, zooms, redshifts, plot=True,
                       fout_base="ICM_", M500lims=[1e8, 1e16], dlog10m=0.2,
                       compare_to_hmf=True, boxsize=3000.0,
                       outdir="/data/curie4/nh444/project1/ICM/",
                       outfilename=None,
                       h_scale=0.6774):
    """
    Plot number of haloes as a function of M500 at different redshifts.
    Args:
        indir: directory containing ICM mass data saved by save_ICM_masses().
        M500lims: min and max M500 (Msun) to plot (at each redshift)
        compare_to_hmf: compare the counts to the expectation from the
                        halo mass function.
        boxsize: box sidelength in comoving Mpc/h.
    """
    import numpy as np
    import matplotlib.pyplot as plt
    import h5py
    import os
    if plot:
        import palettable
    if compare_to_hmf:
        import hmf

    if plot:
        fig, ax = plt.subplots(figsize=(7, 7))
        ax.set_xscale('log')
        ax.set_yscale('log')
        ax.set_xlabel(r"M$_{500}$ [M$_{\odot}$]")
        ax.set_ylabel("Number per mass bin")
        col = ["#c7673e", "#78a44f", "#6b87c8",
               "#934fce", "#e6ab02", "#be558f", "#47af8d"]
        #col = palettable.cmocean.sequential.Deep_7.hex_colors[2:]
        #col = palettable.matplotlib.Viridis_7.hex_colors[:-1]

    bins = np.logspace(np.log10(M500lims[0]), np.log10(M500lims[1]),
                       num=int((np.log10(M500lims[1]) -
                                np.log10(M500lims[0])) / dlog10m)+1)

    if compare_to_hmf:
        # Get the astropy.cosmology object
        my_cosmo = hmf.cosmo.Cosmology()
        # Use the default Planck cosmology but update omega_m to
        # the correct value as in table 4 of Planck XIII 2015.
        my_cosmo.update(cosmo_params={'Om0': 0.3089})
        mf = hmf.MassFunction(z=redshifts[0], Mmin=np.log10(M500lims[0]),
                              Mmax=np.log10(M500lims[1]),
                              dlog10m=dlog10m,
                              delta_h=500.0, delta_wrt='crit',
                              hmf_model=hmf.fitting_functions.Watson)
        # colorbrewer.qualitative.Paired_12
        col = ['#A6CEE3', '#1F78B4', '#B2DF8A', '#33A02C', '#FB9A99', '#E31A1C',
               '#FDBF6F', '#FF7F00', '#CAB2D6', '#6A3D9A', '#FFFF99', '#B15928']

    col_idx = 0
    for zidx, z in enumerate(redshifts):
        if compare_to_hmf:
            mf.update(z=z)
            if plot:
                ax.step(mf.m[1:], (mf.ngtm[:-1] - mf.ngtm[1:]) * boxsize**3,
                        where='pre', c=col[col_idx], lw=3, linestyle='--',
                        label="z = {:.1f}".format(z) +
                        " ({:d} Mpc/h)".format(int(boxsize)))
                col_idx += 1

        suffix = "{:.2f}".format(z)
        for simidx, simdir in enumerate(simdirs):
            if simdir is not "":
                if simdir[-1] != "/":
                    simdir += "/"
            filename = indir+simdir+fout_base+"z"+suffix.zfill(5)+".hdf5"
            with h5py.File(filename, "r") as f:
                    M500 = f['M500'].value[:, 0] / h_scale

            if zoomdirs[simidx] is not "":
                for zoom in zooms[simidx]:
                    filename = indir+zoomdirs[simidx]+zoom + \
                        "/"+fout_base+"z"+suffix.zfill(5)+".hdf5"
                    if os.path.exists(filename):
                        with h5py.File(filename) as f:
                            M500 = np.concatenate((
                                    M500, f['M500'].value[:, 0] / h_scale))
                    else:
                        print("File not found:", filename)
        if plot:
            ax.hist(M500, bins=bins, histtype='step', lw=3, color=col[col_idx],
                    label="z = {:.1f} sample".format(z))
            col_idx += 1

        if compare_to_hmf:
            # Count number of haloes above certain mass threshold.
            M_thresh = 1e14
            N = sum(M500 > M_thresh)
            N_err = float(N)**0.5  # Poission error
            idx = np.argmin(np.abs(np.log10(mf.m) - np.log10(M_thresh)))
            N_pred = mf.ngtm[idx] * boxsize**3
            N_pred_err = float(N_pred)**0.5
            if zidx == 0:
                N0 = N
                N_err0 = N_err
                N_pred0 = N_pred
                N_pred_err0 = N_pred_err
            print("\nz = {:.1f}".format(z))
            print("Number of haloes above 10^14:", N)
            print("Predicted number above 10^14:", int(N_pred))
            print("Ratio: {:.2f} +/- {:.2f}".format(
                    float(N) / N0,
                    float(N) / N0 *
                    ((N_err / N)**2 + (N_err0 / N0)**2)**0.5))
            print("Predicted Ratio: {:.2f} +/- {:.2f}".format(
                    float(N_pred) / N_pred0,
                    float(N_pred) / N_pred0 *
                    ((N_pred_err / N_pred)**2 + (N_pred_err0 / N_pred0)**2)**0.5))
            print("Expected number: {:d}".format(int(float(N_pred) / N_pred0 * N0)))

    if plot:
        ax.legend(loc='best', fontsize='small', frameon=True, borderpad=0.5)
        plt.tight_layout()
        ax.set_ylim(ymin=1.0)
        if outfilename is not None:
            fig.savefig(outdir+outfilename, bbox_inches='tight')
            print("Saved to", outdir+outfilename)


def plot_baryon_fracs(indir, simdirs, zoomdirs, zooms, redshifts,
                      fout_base="ICM_", scat_idx=[], ms=6.5,
                      M500lims=[1e8, 1e16], plot_obs=True,
                      outdir="/data/curie4/nh444/project1/ICM/",
                      outsuffix="", h_scale=0.6744):
    import numpy as np
    import matplotlib.pyplot as plt
    from scipy.stats import binned_statistic
    import h5py

    N = len(redshifts)
    if N > 1:
        fig, axes = plt.subplots(1, N, figsize=(5*N, 7), sharey=True)
    else:
        fig, axes = plt.subplots(figsize=(9, 8))

    bins = np.logspace(8, 15, num=30)  # width 0.2 dex
    x = (bins[1:]*bins[:-1])**0.5
    for zidx, z in enumerate(redshifts):
        suffix = "{:.2f}".format(z)
        if N > 1:
            ax = axes[zidx]
        else:
            ax = axes
        ax.set_xscale('log')
        ax.set_xlabel(r"$M_{500}^{\mathrm{crit}}$ [M$_{\odot}$]")
        ax.set_xlim(M500lims[0], M500lims[1])
        ax.set_ylabel("Mass Fraction within r$_{500}$")
        ax.set_title("z={:.1f}".format(z))

        # baryon fraction in boxes
        ax.axhline(0.04836 / 0.3065, c='0.5', ls='dashed')

        if plot_obs:
            obs_dir = "/data/vault/nh444/ObsData/mass_fracs/"
            ms = 7
            cgas = '0.3'
            Gio = np.genfromtxt(obs_dir+"Giodini09_baryon_fracs.txt")
            ax.errorbar(Gio[:, 0]*0.72/h_scale, Gio[:, 5],
                        xerr=[Gio[:, 1], Gio[:, 2]], yerr=Gio[:, 6],
                        c=cgas, mec='none', marker='o', ls='none',
                        ms=ms, label="Giodini+ 2009")
#            Gonz = np.genfromtxt(obs_dir+"Gonzalez_2013_gas_fracs.txt")
#            ax.errorbar(Gonz[:, 2]*1e14*0.702/h_scale, Gonz[:, 4],
#                        xerr=Gonz[:, 3]*1e14*0.702/h_scale,
#                        yerr=Gonz[:, 5], c=cgas, mec='none',
#                        marker='s', ls='none', ms=ms,
#                        label="Gonzalez+ 2013")

            cstar = '0.3'
            salp2chab = 0.58
            ax.errorbar(Gio[:, 0]*0.72/h_scale, Gio[:, 3]*salp2chab,
                        xerr=[Gio[:, 1], Gio[:, 2]],
                        yerr=Gio[:, 4] * salp2chab,
                        c=cstar, mec='none', marker='o', ls='none', ms=ms,
                        label="Giodini+ 2009")
#            gonz2chab = 0.76
#            gonz = np.genfromtxt(
#                    obs_dir + "Gonzalez_2013_stellar_gas_masses.csv",
#                    delimiter=",")
#            plt.errorbar(gonz[:11, 14]*0.702*1e14/h_scale,
#                         gonz[:11, 29]*gonz2chab,
#                         xerr=[gonz[:11, 15]*0.702/h_scale*1e14,
#                               gonz[:11, 15]*0.702/h_scale*1e14],
#                         yerr=[gonz[:11, 30]*gonz2chab,
#                               abs(gonz[:11, 31]*gonz2chab)],
#                         c=cstar, mec='none', marker='s', ls='none',
#                         ms=ms, label="Gonzalez+ 2013")

            cbary = '0.3'
            ax.errorbar(Gio[:, 0]*0.72/h_scale, Gio[:, 7],
                        xerr=[Gio[:, 1], Gio[:, 2]], yerr=Gio[:, 8],
                        c=cbary, mec='none', marker='o', ls='none',
                        ms=ms, label="Giodini+ 2009")
        for simidx, simdir in enumerate(simdirs):
            if simdir[-1] != "/":
                simdir += "/"
            with h5py.File(indir + simdir + fout_base +
                           "z"+suffix.zfill(5)+".hdf5", "r") as f:
                M500 = f['M500'].value[:, 0] / h_scale
                Mgas = f['Mgas_r500'].value[:, 0] / h_scale
                Mstar = f['Mstar_r500'].value[:, 0] / h_scale
                Mwind = f['Mwind_r500'].value[:, 0] / h_scale
                groupind = np.where(
                    (M500 > M500lims[0]) & (M500 < M500lims[1]))[0]
                groupGasind = np.intersect1d(
                    np.where(Mgas > 0.0)[0], groupind, assume_unique=True)
                groupStarind = np.intersect1d(
                    np.where(Mstar > 0.0)[0], groupind, assume_unique=True)
                groupBaryonind = np.intersect1d(
                    groupGasind, groupStarind, assume_unique=True)
                if simidx in scat_idx:
                    ax.plot(M500[groupGasind],
                            (Mgas[groupGasind] + Mwind[groupGasind]) /
                            M500[groupGasind], marker='o', ls='none',
                            mec='none', mfc='0.7', ms=ms)
                    ax.plot(M500[groupStarind],
                            Mstar[groupStarind] / M500[groupStarind],
                            marker='^', ls='none',
                            mec='none', mfc="#934fce", ms=ms)

                med, bin_edges, n = binned_statistic(
                    M500[groupGasind],
                    (Mgas[groupGasind]+Mwind[groupGasind]) / M500[groupGasind],
                    statistic='mean', bins=bins)
                ax.plot(x, med, c=col[simidx], lw=2.5, label=labels[simidx])
                med, bin_edges, n = binned_statistic(
                    M500[groupStarind],
                    Mstar[groupStarind] / M500[groupStarind],
                    statistic='mean', bins=bins)
                ax.plot(x, med, c=col[simidx], lw=2.5,
                        ls='dashed', label=labels[simidx])
                med, bin_edges, n = binned_statistic(
                        M500[groupBaryonind],
                        (Mgas[groupBaryonind] + Mwind[groupBaryonind] +
                         Mstar[groupBaryonind]) / M500[groupBaryonind],
                        statistic='mean', bins=bins)
                ax.plot(x, med, c=col[simidx], lw=2.5, ls='dashdot',
                        dash_capstyle='round', label=labels[simidx])

            if zoomdirs[simidx] is not "":
                for zidx, zoom in enumerate(zooms[simidx]):
                    with h5py.File(indir + zoomdirs[simidx] + zoom + "/" +
                                   fout_base+"z"+suffix.zfill(5)+".hdf5") as f:
                        M500 = f['M500'].value[:, 0] / h_scale
                        Mgas = f['Mgas_r500'].value[:, 0] / h_scale
                        Mstar = f['Mstar_r500'].value[:, 0] / h_scale
                        Mwind = f['Mwind_r500'].value[:, 0] / h_scale
                        for idx in range(len(M500)):
                            if M500[idx] >= M500lims[0] and M500[idx] <= M500lims[1]:
                                ax.plot(M500[idx], (Mgas[idx] + Mwind[idx]) / M500[idx],
                                        marker='o', ls='none', mec='none', mfc='0.7', ms=ms)
                                ax.plot(M500[idx], Mstar[idx] / M500[idx], marker='^',
                                        ls='none',  mec='none', mfc="#934fce", ms=ms)
        handles, leglabels = ax.axes.get_legend_handles_labels()
        leg1 = ax.legend(handles[::3], leglabels[::3], loc='best',
                         fontsize='x-small',
                         frameon=True, borderpad=0.5)  # just the labels for gas
        ax.add_artist(leg1)
        if N > 1:
            leg2 = ax.legend(handles[:3], ['gas', 'stars', 'baryons'], bbox_to_anchor=(
                0.5, 0.05), loc=3, frameon=False, fontsize='small')  # bbox_to_anchor=(0.5, 0.2)
        else:
            leg2 = ax.legend(handles[:3], [
                             'gas', 'stars', 'baryons'], loc='lower left', frameon=True, fontsize='small')
        ax.add_artist(leg2)
    plt.tight_layout()
    if len(scat_idx) > 0:
        outfilepath = outdir+"baryon_fracs"+outsuffix+".png"
        fig.savefig(outfilepath, bbox_inches='tight', dpi=300)
    else:
        outfilepath = outdir+"baryon_fracs"+outsuffix+".pdf"
        fig.savefig(outfilepath, bbox_inches='tight')
    print("Saved to", outfilepath)


def compare_temp(indir, simdirs, zoomdirs, zooms, z, plot_ratio=False,
                 labels=None, fout_base="ICM_", fout_suffix="",
                 M500lims=[0.0, 1e16],
                 col=None, hlightcol=None, ms=6.5, mew=1.2, errlw=1.2,
                 outdir="/data/curie4/nh444/project1/ICM/",
                 outfilename="temp_comparison.pdf", h_scale=0.6774):
    """Compare different temperature weightings. """
    import numpy as np
    import matplotlib.pyplot as plt
    import h5py
    import os

    if col is None:
        # Use default colours.
        col = plt.rcParams['axes.prop_cycle'].by_key()['color']

    fig, ax = plt.subplots(figsize=(7, 7))
    ax.set_xscale('log')
    if plot_ratio:
        ax.set_xlim(0.3, 20.0)
        ax.set_ylim(-0.5, 1.5)
        ax.axhline(0.0, c='0.5', ls='--', lw=2)
    else:
        ax.set_xlim(0.1, 10.0)
        ax.set_yscale('log')
        ax.set_ylim(0.1, 10.0)

        # Plot line of equality
        xmin, xmax = ax.get_xlim()
        ax.plot([xmin, xmax], [xmin, xmax], c='0.5', ls='--', lw=2)

    # Define hdf5 group names for the x- and y-axes
    yname = 'Tew_r500'
    ax.set_ylabel(r"$\mathrm{T_{ew, \,r_{500}}}$")
#    yname = 'Tew_r500_2'
#    ax.set_ylabel(r"$\mathrm{T_{ew, \,r_{500}, \,2}}$")
#    xname = 'Tew_r500_Zfixed'
#    ax.set_xlabel(r"$\mathrm{T_{ew, \,r_{500}, \,Zfixed}}$")
#    xname = 'Tsl_r500'
#    ax.set_xlabel(r"$\mathrm{T_{sl, \,r_{500}}}$")
#    xname = 'Temw_r500'
#    ax.set_xlabel(r"$\mathrm{T_{emw, \,r_{500}}}$")
#    xname = 'Tkin_r500'
#    ax.set_xlabel(r"$\mathrm{T_{kin, \,r_{500}}}$")
    xname = 'Tmw_r500'
    xname = 'Tmw_r500_warm'
#    yname = 'Tmw_r500_hot'
    ax.set_xlabel(r"$\mathrm{T_{mw, \,r_{500}}}$")
#    yname = 'Tsl_r500'
#    ax.set_ylabel(r"$\mathrm{T_{sl, \,r_{500}}}$")

    suffix = "{:.2f}".format(z)
    for simidx, simdir in enumerate(simdirs):
        if simdir[-1] != "/":
            simdir += "/"
        if labels is not None:
            label = labels[simidx]
        else:
            label = None
        filename = (indir + simdir + fout_base + "z" + suffix.zfill(5) +
                    fout_suffix + ".hdf5")
        if os.path.exists(filename):
            with h5py.File(filename, "r") as f:
                M500 = f['M500'].value[:, 0] / h_scale
                x = f[xname].value[:, 0]
                if plot_ratio:
                    y = (f[yname].value[:, 0] - x) / x
                else:
                    y = f[yname].value[:, 0]
                # Get haloes in mass range.
                ind = np.where((M500 > M500lims[0]) & (M500 < M500lims[1]))[0]
                # Filter out haloes with zero temperature.
                ind = ind[np.where((x[ind] > 0.0))[0]]
                ax.plot(x[ind], y[ind], ls='none',
                        marker='D', ms=ms, mew=mew,
                        mec=col[simidx], mfc=col[simidx],
                        label=label, zorder=4)
        else:
            print("File does not exist:", filename)

        if zoomdirs[simidx] is not "":
            for zidx, zoom in enumerate(zooms[simidx]):
                filename = (indir + zoomdirs[simidx] + zoom + "/" + fout_base +
                            "z" + suffix.zfill(5) + fout_suffix + ".hdf5")
                if os.path.exists(filename):
                    with h5py.File(filename) as f:
                        grpnum = f['GroupNum'].value[:, 0]
                        M500 = f['M500'].value[:, 0] / h_scale
                        if xname not in f:
                            raise KeyError("key " + str(xname) +
                                           " not found for zoom " + str(zoom))
                        x = f[xname].value[:, 0]
                        if plot_ratio:
                            y = ((f[yname].value[:, 0] -
                                  f[xname].value[:, 0]) /
                                 f[xname].value[:, 0])
                        else:
                            y = f[yname].value[:, 0]
                        # Get haloes in mass range.
                        ind = np.where((M500 > M500lims[0]) &
                                       (M500 < M500lims[1]))[0]
                        # Filter out haloes with zero temperature.
                        ind = ind[np.where((x[ind] > 0.0))[0]]
                        # Plot group 0
                        grp0 = (grpnum[ind] == 0)
                        ax.plot(x[ind[grp0]], y[ind[grp0]],
                                marker='D', ms=ms, mew=mew, ls='none',
                                mec=col[simidx], mfc=col[simidx],
                                zorder=4)
                        # plot groups >0 (secondary FoF haloes)
                        if hlightcol is None:
                            hlightcol = col[simidx]
                        ax.plot(x[ind[~grp0]], y[ind[~grp0]],
                                marker='D', ms=ms, mew=mew, ls='none',
                                mec=hlightcol, mfc=hlightcol,
                                zorder=3)
                else:
                    print("File does not exist:", filename)

    ax.text(0.9, 0.1, r"r$_{\mathrm{3D}} \leq$ r$_{500}$",
            horizontalalignment='right', verticalalignment='center',
            transform=plt.gca().transAxes)
    plotlabel = "$z="+"{:.1f}".format(z)+"$"
    ax.text(0.9, 0.3, plotlabel, horizontalalignment='right',
            verticalalignment='center', transform=ax.transAxes)
    ax.legend(loc='upper left', fontsize='small',
              frameon=False, borderaxespad=1)
    plt.tight_layout()
    fig.savefig(outdir+outfilename, bbox_inches='tight')
    print("Saved to", outdir+outfilename)


def plot_relationship(indir, simdirs, zoomdirs, zooms, z,
                      xname, yname, xlabel=None, ylabel=None,
                      labels=None, fout_base="ICM_", fout_suffix="",
                      M500lims=[0.0, 1e16],
                      col=None, hlightcol=None, ms=6.5, mew=1.2, errlw=1.2,
                      outdir="/data/curie4/nh444/project1/ICM/",
                      outfilename="comparison.pdf", h_scale=0.6774):
    """Compare different properties. """
    import numpy as np
    import matplotlib.pyplot as plt
    import h5py
    import os

    if col is None:
        # Use default colours.
        col = plt.rcParams['axes.prop_cycle'].by_key()['color']

    fig, ax = plt.subplots(figsize=(7, 7))
    ax.set_xscale('log')
    ax.set_yscale('log')

    if xlabel is not None:
        ax.set_xlabel(xlabel)
    if ylabel is not None:
        ax.set_ylabel(ylabel)

    suffix = "{:.2f}".format(z)
    for simidx, simdir in enumerate(simdirs):
        if simdir[-1] != "/":
            simdir += "/"
        if labels is not None:
            label = labels[simidx]
        else:
            label = None
        filename = (indir + simdir + fout_base + "z" + suffix.zfill(5) +
                    fout_suffix + ".hdf5")
        if os.path.exists(filename):
            with h5py.File(filename, "r") as f:
                M500 = f['M500'].value[:, 0] / h_scale
                x = f[xname].value[:, 0]
                if yname == "Ttot":
                    y = (f["Tmw_r500_warm"].value[:, 0] +
                         f["Tkin_r500"].value[:, 0])
                else:
                    y = f[yname].value[:, 0]
                # Get haloes in mass range.
                ind = np.where((M500 > M500lims[0]) & (M500 < M500lims[1]))[0]
                # Filter out haloes with zero temperature.
                ind = ind[np.where((x[ind] > 0.0))[0]]
                ax.plot(x[ind], y[ind], ls='none',
                        marker='D', ms=ms, mew=mew,
                        mec=col[simidx], mfc=col[simidx],
                        label=label, zorder=4)
        else:
            print("File does not exist:", filename)

        if zoomdirs[simidx] is not "":
            for zidx, zoom in enumerate(zooms[simidx]):
                filename = (indir + zoomdirs[simidx] + zoom + "/" + fout_base +
                            "z" + suffix.zfill(5) + fout_suffix + ".hdf5")
                if os.path.exists(filename):
                    with h5py.File(filename) as f:
                        grpnum = f['GroupNum'].value[:, 0]
                        M500 = f['M500'].value[:, 0] / h_scale
                        if xname not in f:
                            raise KeyError("key " + str(xname) +
                                           " not found for zoom " + str(zoom))
                        x = f[xname].value[:, 0]
                        if yname == "Ttot":
                            y = (f["Tmw_r500_warm"].value[:, 0] +
                                 f["Tkin_r500"].value[:, 0])
                        else:
                            y = f[yname].value[:, 0]
                        # Get haloes in mass range.
                        ind = np.where((M500 > M500lims[0]) &
                                       (M500 < M500lims[1]))[0]
                        # Filter out haloes with zero temperature.
                        ind = ind[np.where((x[ind] > 0.0))[0]]
                        # Plot group 0
                        grp0 = (grpnum[ind] == 0)
                        ax.plot(x[ind[grp0]], y[ind[grp0]],
                                marker='D', ms=ms, mew=mew, ls='none',
                                mec=col[simidx], mfc=col[simidx],
                                zorder=4)
                        # plot groups >0 (secondary FoF haloes)
                        if hlightcol is None:
                            hlightcol = col[simidx]
                        ax.plot(x[ind[~grp0]], y[ind[~grp0]],
                                marker='D', ms=ms, mew=mew, ls='none',
                                mec=hlightcol, mfc=hlightcol,
                                zorder=3)
                else:
                    print("File does not exist:", filename)

    plotlabel = "$z="+"{:.1f}".format(z)+"$"
    ax.text(0.9, 0.3, plotlabel, horizontalalignment='right',
            verticalalignment='center', transform=ax.transAxes)
    ax.legend(loc='upper left', fontsize='small',
              frameon=False, borderaxespad=1)
    plt.tight_layout()
    fig.savefig(outdir+outfilename, bbox_inches='tight')
    print("Saved to", outdir+outfilename)


def show_wind_frac(scatter=False):
    import snap
    import numpy as np
    from scipy.spatial import cKDTree
    from timeit import default_timer as timer
    import readsnap as rs
    import matplotlib.pyplot as plt
    from scipy.stats import binned_statistic

    N = len(snapnums)
    fig, axes = plt.subplots(1, N, figsize=(5*N, 7), sharey=True)

    bins = np.logspace(8.5, 15, num=30)
    x = (bins[1:]*bins[:-1])**0.5

    for snapidx, snapnum in enumerate(snapnums):
        if N > 1:
            ax = axes[snapidx]
        else:
            ax = axes
        ax.set_xscale('log')
        ax.set_xlabel("$M_{500}^{\mathrm{crit}}$ [M$_{\odot}$]")
        ax.set_ylim(0, 1.05)
        if snapidx == 0:
            ax.set_ylabel("Wind mass fraction within r$_{500}$")

        z = snap.snapshot(basedir+simdirs[0], snapnum, mpc_units=mpc_units,
                          read_params=False, header_only=True).header.redshift
        ax.set_title("z={:.1f}".format(z))

        for idx, simdir in enumerate(simdirs):
            sim = snap.snapshot(basedir+simdir, snapnum, mpc_units=mpc_units)
            print("Loading positions...")
            sim.load_starpos()
            print("Loading masses...")
            sim.load_starmass()

            # GFM_StellarFormationTime
            age = rs.read_block(sim.snapname, "GAGE",
                                cosmic_ray_species=sim.num_cr_species)
            # birth time <=0 is a wind particle (=0 if just recoupled)
            starind = np.where(age > 0.0)[0]
            windind = np.where(age <= 0.0)[0]

            print("Constructing k-d trees...")
            starttime = timer()
            kdtree_star = cKDTree(
                sim.starpos[starind], leafsize=5, boxsize=sim.box_sidelength)
            kdtree_wind = cKDTree(
                sim.starpos[windind], leafsize=5, boxsize=sim.box_sidelength)
            print("Done. k-d trees took",
                  timer() - starttime, "seconds.")

            Mstar = []
            Mwind = []
            groupind = np.where(sim.cat.group_r_crit500 > 0.0)[0]
            for group in groupind:  # I'm going to do this one group at a time to save memory, else I could use cKDTree.query_ball_tree to return a list of indices for every group in one go
                kdind = kdtree_star.query_ball_point(
                    sim.cat.group_pos[group], sim.cat.group_r_crit500[group], n_jobs=nthreads)
                Mstar.append(np.sum(sim.starmass[starind[kdind]]))
                kdind = kdtree_wind.query_ball_point(
                    sim.cat.group_pos[group], sim.cat.group_r_crit500[group], n_jobs=nthreads)
                Mwind.append(np.sum(sim.starmass[windind[kdind]]))
            del kdind
            del kdtree_star
            del kdtree_wind
            Mstar = np.asarray(Mstar)
            Mwind = np.asarray(Mwind)
            M500 = sim.cat.group_m_crit500[groupind]

            ind = np.where((Mstar + Mwind > 0.0))[0]
            if scatter and idx == 0:
                ax.plot(M500[ind] * 1e10 / h_scale, Mwind[ind] / (Mwind[ind] + Mstar[ind]),
                        marker='o', ls='none', mec='none', mfc='0.7', ms=2, alpha=0.3)

            mean, bin_edges, n = binned_statistic(
                M500[ind] * 1e10 / h_scale, Mwind[ind] / (Mwind[ind] + Mstar[ind]), statistic='mean', bins=bins)
            ax.plot(x, mean, c=col[idx], lw=2.5, label=labels[idx])
            med, bin_edges, n = binned_statistic(M500[ind] * 1e10 / h_scale, Mwind[ind] / (
                Mwind[ind] + Mstar[ind]), statistic='median', bins=bins)
            ax.plot(x, med, c=col[idx], lw=2.5, ls='dashed')

        ax.legend(loc='best')
    plt.tight_layout()
    if scatter:
        outfilepath = outdir+"wind_fracs"+suffix+".png"
        fig.savefig(outfilepath, bbox_inches='tight', dpi=300)
    else:
        outfilepath = outdir+"wind_fracs"+suffix+".pdf"
        fig.savefig(outfilepath, bbox_inches='tight')


if __name__ == "__main__":
    main()
