#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 11 10:21:03 2017

@author: nh444
"""

def main():
    delta = 500.0
    ref = "crit"
    use_fof = True ## Use halo file for M_delta and R_delta
    h_scale = 0.7 ## used in plotting
    masstab = False
    basedir = "/data/emergence3/deboras/Elliptical_ZoomIns/"
    simdirs = ["L40_512_NoDutyRadioWeak_HalfThermal_FixEta/",
               "L40_512_NoDutyRadioInt_HalfThermal_FixEta/",
               "L40_512_LowDutyRadioWeak_HalfThermal_FixEta/",
               "L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff/",
               ]
    labels = ["Ref",
              "Stronger radio",
              "Quasar duty cycle",
              "Best"]     
    outdir = "/data/curie4/nh444/project1/ICM/" ## where stellar masses are saved
    snapnums = [4, 5, 6, 8, 10, 15, 20, 25]
    snapnums = [24,25]
    snapnums = range(1,26)
    times = [0.090909,0.100000,0.111111,0.125000,0.142857,0.166667,0.181818,
             0.200000,0.222222,0.250000,0.263158,0.277778,0.294118,0.312500,0.333333,
             0.357143,0.384615,0.416667,0.454545,0.500000,0.555556,0.625000,0.714286,
             0.833333,1.000000]
    mark_snap = None

    
    zoombasedir = "/home/nh444/data/project1/zoom_ins/"
    zoomdirs = ["","","",
                "LDRIntRadioEffBHVel/"] ## corresponding to each simdir
    zooms = ["c96", "c128_fixed", "c160", "c192", "c256_fixed", "c320", "c384_fixed", "c448"]
    zooms = ["c96_box_Arepo", "c96_box_Arepo_2", "c96_box_Arepo_new",
             "c96_box_Arepo_new_PM1024", #"c96", "c96_FeedbackFix", "c96_FeedbackFix_OldGrav", "c96_FeedbackFix_OldGrav_3", "c96_FeedbackFix_OldGrav_2",
             #"c128_box_Arepo", "c128_new", "c128_new_FeedbackFix",
             ]
    zooms = ["c96_box_Arepo",
             "c96_box_Arepo_RKLSF",
             #"c96_box_Arepo_new",
             #"c96_box_Arepo_new_PM1024",
             #"c96_box_Arepo_new_Ngb",
             "c96_FeedbackFix",
             "c96_FeedbackFix_OldGrav",
             #"c96_FeedbackFix_OldGrav_3",
             "c96_FeedbackFix_OldGrav_2",
             "c96_FeedbackFix_OldGrav_2_MHGG",
            ]
    zooms = [#"c96_box_Arepo",
             #"c96_box_Arepo_RKLSF",
             "c96_box_Arepo_new",
             #"c96_box_Arepo_new_LowSoft",
             #"c96_box_Arepo_new_BHVel",
             "c96_box_Arepo_new_ASMTH",
             "c96_box_Arepo_new_PM1024",
             #"c96_box_Arepo_new_Ngb",
            ]
    outfilename="mass_vs_time_PMGRID_c96.pdf"
    mark_snap = None ## None to switch off
    zoomlabels = [#"old arepo",
                  #"old arepo RK + LSF",
                  "old arepo modified",#"c96 High Soft"
                  #"c96 Low Soft",#"old arepo modified with BHVel",
                  "old arepo modified ASMTH 1.5 RCUT 6.0",
                  "old arepo modified PMGRID 1024",
                  "old arepo modified Ngb 256",
                  "new arepo",
                  "+ old gravity solver",
                  #"+ IND\_GRAV\_SOFT",
                  "+ RCUT + IND\_GRAV\_SOFT",
                  "+ MH + GG",
                  ]
    
    zooms = [#"c96_box_Arepo_new",
             #"c96_box_Arepo_new_BHVel",
             #"c96_box_Arepo_new_DRAINGAS_2",
             #"c96_box_Arepo_new_Ngb",
             #"c96_box_Arepo_new_PM1024",
             "c96_box_Arepo_new_LowSoft",
             "c96_box_Arepo_new_LowSoft_BHVel",
             "c96_box_Arepo_new_LowSoft_BHVel_Ngb",
             #"c96_box_Arepo_new_LowSoft_BHVel_Ngb_PM1024_DRAINGAS2",
             ]
    outfilename="mass_vs_time_LowSoft_BHVel_Ngb.pdf"
    mark_snap = None ## None to switch off
    zoomlabels = ["c96 High Soft", "c96 High Soft with BHVel", #"c9 DRAINGAS 2", "c96 256 Ngb", "c96 PMGRID 1024",
                  "c96 Low Soft", "c96 Low Soft BHVel", "c96 Low Soft BHVel Ngb 256", "c96 Low Soft BHVel 256 Ngb DRAINGAS=2 PM=1024"]

                  
#==============================================================================
#     zooms = ["c128_box_Arepo_new",
#              "c128_box_Arepo_new_LowSoft",
#              #"c128_box_Arepo",
#              #"c128_box_Arepo_RKLSF",
#              #"c128_new_FeedbackFix",
#              #"c128_new_FeedbackFix_OldGrav",
#              #"c128_FeedbackFix_OldGrav_2_openmpi_2",
#              #"c128_FeedbackFix_OldGrav_2_MHGG",
#              ]
#     outfilename="mass_vs_time_Low_Softening_c128.pdf"#old_new_arepo_c128
#     mark_snap = None #16
#     zoomlabels = ["c128 High Soft",
#                   "c128 Low Soft",
#                   "old arepo",
#                   "old arepo RK + LSF",
#                   "new arepo",
#                   "+ old gravity solver",
#                   "+ RCUT + IND\_GRAV\_SOFT",
#                   "+ MH + GG",
#                   ]
#==============================================================================

#==============================================================================
#     zooms = ["c128_new", "c128_new_noBHVel", 
#              "c128_new_ThermFeedbackFix",
#              "c128_new_FeedbackFix", 
#              "c128_new_FeedbackFix_OldGrav",
#              ]
#     outfilename="mass_vs_time_FeedbackFix_c128.pdf"
#     #mark_snap = 16
#     zoomlabels = ["Ref",
#                   "without BHVel",
#                   "fixed double feedback",
#                   "+ fixed wrong feedback mode",
#                   "+ old gravity solver",
#                   ]
#==============================================================================
                  
#==============================================================================
#     zoomdirs = ["","","LDRIntRadioEffBHVel_fix_metal_cooling/",
#                 "LDRIntRadioEffBHVel/"]
#     zooms = ["c256"]
#     outfilename="mass_vs_time_metal_cooling_fix_c256.pdf"
#     zoomlabels = ["c256"]
#==============================================================================

    basedir = "/data/emergence3/deboras/Elliptical_ZoomIns/"
    simdirs = ["L40_512_NoDutyRadioWeak_HalfThermal_FixEta/",
                "L40_512_NoDutyRadioWeak_HalfThermal_FixEta_BHChangeVel/",
                "L40_512_LowDutyRadioWeak_HalfThermal_FixEta/",
                "L40_512_LowDutyRadioWeak_HalfThermal_FixEta_BHChangeVel/",
                "L40_512_NoDutyRadioIntLow_HalfThermal_FixEta/",
                "L40_512_NoDutyRadioIntLow_HalfThermal_FixEta_BHChangeVel/",
               ]
    labels = ["NoDutyRadioWeak",
                "NoDutyRadioWeak\_BHChangeVel",
                "LowDutyRadioWeak",
                "LowDutyRadioWeak\_BHChangeVel",
                "NoDutyRadioIntLow",
                "NoDutyRadioIntLow\_BHChangeVel",
              ]
    snapnums = [25]
    N = 10
    zoomdirs = [""]*len(simdirs)

    #from ICM import ICM
#==============================================================================
#     for simdir in simdirs:
#         mgas = ICM(basedir, simdir, outdir, snapnums, N=N, delta=delta, ref=ref, h_scale=h_scale, use_fof=use_fof, masstab=masstab, mpc_units=False)
#         mgas.get_gasmass_fracs()
#         #mgas.plot_mgasfrac_v_mass(for_paper=True) #requires get_gasmass_fracs
#==============================================================================
#==============================================================================
#     for zoomdir in zoomdirs:
#         if zoomdir is not "":
#             for zoom in zooms:
#                 mgas = ICM(zoombasedir+zoomdir, zoom+"/", outdir+zoomdir, snapnums, N=1, delta=delta, ref=ref, h_scale=h_scale, use_fof=use_fof, masstab=masstab, mpc_units=True)
#                 mgas.get_gasmass_fracs()
#==============================================================================
        
    #plot_mgasfrac_v_mass_comparison(outdir, simdirs, zoomdirs, zooms, 25, outfilename = "gas_mass_fractions_BHVel.pdf", z=0, delta=delta, for_paper=True, scat_idx=range(len(simdirs)), labels=labels, h_scale=h_scale, outdir=outdir)
    plot_massfrac_v_time_zooms(outdir, zoomdirs, zooms, snapnums, times, mark_snap=mark_snap, fracs=False, outfilename=outfilename, for_paper=False, labels=zoomlabels, h_scale=h_scale, outdir=outdir)

def plot_massfrac_v_time_zooms(indir, zoomdirs, zooms, snapnums, times, fracs=True, stars=True,
                               redshift=True, obs_dir="/data/vault/nh444/ObsData/mass_fracs/",
                               outdir="", outfilename="mass_fractions_vs_time.pdf", delta=500,
                               fout_base="default", for_paper=False, labels=None, h_scale=0.7, mark_snap=None):
    ## Plot gas mass frac vs redshift - Loads files created by ICM.get_gasmass_fracs
    import matplotlib.pyplot as plt
    import numpy as np
    import os
    
    if fout_base == "default":
        fout_base = "gas_mass_"
    indir = os.path.normpath(indir)+"/"
    if outdir is not "":
        outdir = os.path.normpath(outdir)+"/"
    
    if stars:
        fig = plt.figure(figsize=(24,9))#width,height
        ax2 = fig.add_subplot(132)
        ax1 = fig.add_subplot(131)
        ax3 = fig.add_subplot(133)
        if fracs:
            ax3.set_ylabel(r"M$_{\mathrm{*}}$/M$_{"+str(int(delta))+"}$")
        else:
            ax3.set_ylabel(r"M$_{\mathrm{*}}$/M$_{\mathrm{*},\mathrm{"+labels[0]+"}}$")
        ax3_twin = ax3.twinx()
        ax3_twin.set_ylabel(r"M$_{\mathrm{*}}$", rotation=270)## [$h^{-1}$ M$_{\odot}$]
        if redshift:
            ax3.set_xlabel(r"$1+z$")
            ax3.set_xscale('log')
        else: ax3.set_xlabel(r"time")
    else:
        fig = plt.figure(figsize=(16,9))#width,height
        ax2 = fig.add_subplot(111)
        ax1 = fig.add_subplot(122)
    
    if fracs:
        ax2.set_ylabel(r"M$_{\mathrm{gas}}$/M$_{"+str(int(delta))+"}$")
    else:
        ax2.set_ylabel(r"M$_{\mathrm{gas}}$/M$_{\mathrm{gas},\mathrm{"+labels[0]+"}}$")
    ax2.set_xlabel(r"time")
    ax2_twin = ax2.twinx()
    ax2_twin.set_ylabel(r"M$_{\mathrm{gas}}$", rotation=270)#, labelpad=-15)## [$h^{-1}$ M$_{\odot}$]
    ax1.set_ylabel(r"M$_{"+str(int(delta))+"}$/M$_{"+str(int(delta))+",\mathrm{"+labels[0]+"}}$")
    ax1.set_xlabel(r"time")
    ax1_twin = ax1.twinx()
    ax1_twin.set_ylabel(r"M$_{"+str(int(delta))+"}$", rotation=270)#, labelpad=-15)
    plt.tight_layout()
    plt.subplots_adjust(wspace=0.24)
    
    Gastal = np.genfromtxt(obs_dir+"gastaldello_M500.txt")
    ind = [2,4] # NGC 2563, NGC 533 z=0.0149, 0.0185
    Gz = np.array([0.0149, 0.0185])
    Gt = 1.0 / (1.0 + Gz)
    
    if redshift:
        times = 1.0 / np.array(times)
        for ax in (ax2, ax1):
            ax.set_xlabel(r"$1+z$")
            ax.set_xscale('log')
        if fracs:
            ax2.errorbar(Gz+1.0, Gastal[ind,6], yerr=[Gastal[ind,7],Gastal[ind,8]], c='0.5', mec='none', marker='D', ls='none', ms=5, label="Gastaldello et al. 2007")# z< 0.08 #cc99ff
    elif fracs:
        ax2.errorbar(Gt, Gastal[ind,6], yerr=[Gastal[ind,7],Gastal[ind,8]], c='0.5', mec='none', marker='D', ls='none', ms=5, label="Gastaldello et al. 2007")# z< 0.08 #cc99ff
        
    if mark_snap is not None:
        mark_time = times[np.isclose(snapnums, mark_snap)]
        print "Adding marker at", mark_time
        for ax in fig.axes:
            ax.axvline(x=mark_time, c='k', ls='dashed', alpha=0.2)

    mass1 = None
    gasmass1 = None
    starmass1 = None
    for zoomdir in zoomdirs:
        if zoomdir is not "":
            zoomdir = os.path.normpath(zoomdir)+"/"
            for i, zoom in enumerate(zooms):
                if labels is None:
                    label = zoom.replace("_","\_")
                else:
                    label = labels[i]
                gasfracs = []
                gasmass = []
                mass = []
                starfracs = []
                starmass = []
                t = []
                for j, snapnum in enumerate(snapnums):
                    if os.path.exists(indir+zoomdir+zoom+"/"+fout_base+str(snapnum).zfill(3)+".txt"):
                        data = np.loadtxt(indir+zoomdir+zoom+"/"+fout_base+str(snapnum).zfill(3)+".txt")
                        gasfracs.append(data[3]/data[1])
                        gasmass.append(data[3]*1e10)
                        mass.append(data[1]*1e10)
                        starfracs.append(data[4]/data[1])
                        starmass.append(data[4]*1e10)
                        t.append(times[j])
                if mass1 is None:
                    mass1 = np.array(mass)
                    gasmass1 = np.array(gasmass)
                    starmass1 = np.array(starmass)
                if fracs:
                    ax2.plot(t, gasfracs, lw=2, label=label)
                else:
                    ax2.plot(t, gasmass/gasmass1[:len(gasmass)], lw=2, label=label)
                ax2_twin.loglog(t, gasmass, lw=2, ls='dashed', alpha=0.6)
                ax1.plot(t, np.array(mass)/mass1[:len(mass)], lw=2, label=label)
                ax1_twin.loglog(t, mass, lw=2, ls='dashed', alpha=0.6)
                if stars:
                    if fracs:
                        ax3.plot(t, starfracs, lw=2, label=label)
                    else:
                        ax3.plot(t, starmass/starmass1[:len(starmass)], lw=2, label=label)
                    ax3_twin.loglog(t, starmass, lw=2, ls='dashed', alpha=0.6)
    ax1.legend(loc='best')
    for ax in (ax1, ax2):
        ymin, ymax = ax.get_ylim()
        ax.set_ylim(0.0, ymax)
    if stars:
        ymin, ymax = ax3.get_ylim()
        ax3.set_ylim(0.0, ymax)
    if redshift:
        import matplotlib.ticker as ticker
        xmin, xmax = ax1.get_xlim()
        for ax in fig.axes:
            ax.set_xlim(10, xmin)
            ax.xaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))
    plt.savefig(outdir+outfilename, bbox_inches='tight')
    print "Plot saved to", outdir+outfilename
    
## This function will combine the data output by ICM.get_gasmass_fracs() for multiple simdirs into one plot
def plot_mgasfrac_v_mass_comparison(indir, simdirs, zoomdirs, zooms, snapnum, obs_dir="/data/vault/nh444/ObsData/mass_fracs/", outdir="", outfilename="gas_mass_fractions.pdf", z=0, delta=500, fout_base="default", for_paper=False, scat_idx=[], labels=None, h_scale=0.7):
    import numpy as np
    import matplotlib.pyplot as plt
    import os
    import scipy.stats as stats

    if fout_base == "default":
        fout_base = "gas_mass_"
    indir = os.path.normpath(indir)+"/"
    if outdir is not "":
        outdir = os.path.normpath(outdir)+"/"

    plt.figure(figsize=(12,11))#width,height
    plt.ylabel(r"Gas mass fraction within r$_{"+str(int(delta))+"}$", fontsize=16)
    plt.xlabel(r"M$_{"+str(int(delta))+"}$ [$h^{-1}$ M$_{\odot}$]", fontsize=16)
    plt.xlim(3e12,1e15)
    plt.xscale('log')

    Vikh = np.genfromtxt(obs_dir+"gas_frac_M-T_viklinin.txt")
    Sun = np.genfromtxt(obs_dir+"sun_group_x-ray_properties.txt", usecols=np.arange(0,24))
    Gastal = np.genfromtxt(obs_dir+"gastaldello_M500.txt")

    if z <= 0.23:
        plt.errorbar(Sun[:,13]*1e13*0.73, Sun[:,16], xerr=[Sun[:,3],Sun[:,2]], yerr=[Sun[:,18],Sun[:,17]], c='#90AC28', mec='none', marker='D', linestyle='none', ms=3, label="Sun et al. 2008")# 0.012 <z< 0.12
        plt.errorbar(Vikh[:,11]*1e14*0.72, Vikh[:,15], xerr=Vikh[:,4], yerr=Vikh[:,16], c='#ff471a', mec='none', marker='D', linestyle='none', ms=3, label="Vikhlinin et al. 2006")#z < 0.23
        plt.errorbar(Gastal[:,0]*0.7, Gastal[:,6], xerr=[Gastal[:,1],Gastal[:,2]], yerr=[Gastal[:,7],Gastal[:,8]], c='#cc99ff', mec='none', marker='D', linestyle='none', ms=3, label="Gastaldello et al. 2007")# z< 0.08

    ## For median profiles
    xmin, xmax = plt.gca().get_xlim()
    num = int((np.log10(xmax) - np.log10(xmin/10.0)) / 0.2)
    bins = np.logspace(np.log10(xmin/10.0), np.log10(xmax), num=num)
    x = 10**((np.log10(bins[:-1]) + np.log10(bins[1:]))/2)

    col_idx = np.linspace(0, 1, num=len(simdirs))

    for simnum, simdir in enumerate(simdirs):
        simdir = os.path.normpath(simdir)+"/"
        if labels is None: label = simdir[8:-1]
        else: label = labels[simnum]
        data = np.genfromtxt(indir+simdir+fout_base+str(snapnum).zfill(3)+".txt")
        M_delta = data[:,1]
        #R_delta = data[:,2]
        M_delta_gas = data[:,3]

        med, bin_edges, n = stats.binned_statistic(M_delta*1e10, M_delta_gas/M_delta, statistic='median', bins=bins)
        mask = np.isfinite(med)
        plt.plot(x[mask], med[mask], c=plt.cm.rainbow_r(col_idx[simnum]), label=label, lw=1.5)
        
        if simnum in scat_idx:
            plt.plot(M_delta*1e10, M_delta_gas/M_delta, marker='s', mew=1.4, mfc='none', mec=plt.cm.rainbow_r(col_idx[simnum]), linestyle='none')
        if zoomdirs[simnum] is not "":
            for zoom in zooms:
                data = np.loadtxt(indir+zoomdirs[simnum]+zoom+"/"+fout_base+str(snapnum).zfill(3)+".txt")
                plt.plot(data[1]*1e10/h_scale, data[3]/data[1], marker='D', mew=0.5, mec='k', ms=8, ls='none', label=zoom.replace("_","\_"))
        
    plt.legend(loc='upper left', frameon=False, borderaxespad=1, numpoints=1, fontsize=14, ncol=2)
    
    if not for_paper:
        plt.savefig(outdir+outfilename)
    else:
        plt.savefig(outdir+outfilename, bbox_inches='tight')
    print "Plot saved to", outdir+outfilename
    return outdir+outfilename
    
if __name__ == "__main__":
    main()