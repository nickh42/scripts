#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 12 13:18:50 2017

@author: nh444
"""
import sys
import h5py

filename = sys.argv[1]

f = h5py.File(filename, "r")

header = f['Header']
for attrname in header.attrs.keys():
    print attrname.ljust(25)+" "+str(header.attrs[attrname])

f.close()
