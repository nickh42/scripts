import numpy as np
import snap
from astropy.cosmology import FlatLambdaCDM
from scipy.constants import m_p as mproton ## in kg
from scipy.constants import m_e as melectron ## in kg
from scipy.constants import parsec as pc # in m
from scipy.constants import pi
mproton_in_g = mproton * 1000.0
melectron_in_g = melectron * 1000.0
Eelectron_in_kev = 510.99891 ## rest mass energy of electron in keV
thomson = 6.6524587158e-29 ## thomson cross-section m^2
UnitMass_in_g = 10.0**10 * 1.9884430e33
Xh = 0.76 ## hydrogen mass fraction

basedir = "/home/nh444/data/project1/zoom_ins/LDRIntRadioEffBHVel/"
sims = ["c96", "c128_fixed", "c160", "c192", "c256_fixed", "c320", "c384_fixed", "c448"]
sims = ["c384_box_Arepo_new"]

snapnum = 25
obs_dir = "/home/nh444/vault/ObsData/SZ/"
outdir="/data/curie4/nh444/project1/SZ/"
group = 0

cylinder=True
for sim in sims:
    s = snap.snapshot(basedir+sim+"/", snapnum)
    h = s.header.hubble
    s.load_gasmass()
    s.load_gaspos()
    s.get_temp(del_ne=False)
    #print "Getting hostsubhalos..."
    #s.get_hostsubhalos()
    cm = FlatLambdaCDM(H0 = s.header.hubble*100.0, Om0=s.header.omega_m)
    #hostsubhalos = s.hostsubhalos[s.species_start[0]:s.species_end[0]]
    
    if cylinder:
        #rad = np.sqrt(((s.rel_pos(s.gaspos, s.cat.group_pos[group]))**2).sum(axis=1))
        rad = np.sqrt((s.rel_pos(s.gaspos[:,0],s.cat.group_pos[group,0]))**2 + (s.rel_pos(s.gaspos[:,1],s.cat.group_pos[group,1]))**2)
        ind = np.where(rad < 5.0 * s.cat.group_r_crit500[group]) ##cylinder along z with radius 5*R500
    else:
        ## Get particles within R500
        rad = s.nearest_dist_3D(s.gaspos, s.cat.group_pos[group])
        ind = np.where(rad < s.cat.group_r_crit500[group])##This is not guaranteed to be the same R500 as that of the main subhalo but probably close
    ## Get gas particles in main subhalo of halo
    #ind = np.where(hostsubhalos == subnum)
    m = s.gasmass[ind]
    pos = s.gaspos[ind]
    #rho = rho[ind]
    NE = s.ne[ind] #electron abundance
    temp = s.temp[ind] #gas kT in kev
    electrons = m * UnitMass_in_g / (mproton_in_g + melectron_in_g) * Xh * NE / h#s.header.hubble ## number of electrons per particle
    #ne = rho * UnitMass_in_g / (mproton_in_g + melectron_in_g) * Xh * NE * s.header.hubble / (1.0e3*pc*100.0)**3.0
    PV = electrons * temp #temp=kT, assume electron temp same as gas temp (equilibrium)
    ## Mass-weighted sum
    Y = np.sum(PV) * thomson / Eelectron_in_kev ## now in m^2
    print sim, Y / (pc * 1e6)**2#, "[Mpc^2]"
    ## Scale to z=0 and DA=500 Mpc/arcmin
    Y *= cm.efunc(s.header.redshift)**(-1.0) / (500.0*1.0e6*pc/(180.0/pi*60.0))**2.0
    if cylinder: Y /= 1.796 ##conversion factor from cylinder as used in Planck paper (see also Le Brun 2015)
    #print "Y =", Y
    #print "log M500 =", np.log10(s.cat.group_m_crit500[group]*1e10/h)