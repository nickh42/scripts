#include <iostream>
#include <cmath>
#include <algorithm>

#include "read_write_fits.h"

namespace pg{

double spline (double u1, double u2, double u3)
{
  double u = sqrt(u1*u1+u2*u2+u3*u3);
  if (u <= 0.5) return(1-6*pow(u,2)+6*pow(u,3));
  else if (u > 0.5 && u < 1) return(2*pow(1-u,3));
  else return(0);
}

double kernel (std::string kernel_type, double u1, double u2, double u3)
{
  if (kernel_type == "spline") return(spline(u1, u2, u3));
  else
  {
    std::cout << "ERROR: wrong kernel type" << std::endl;
    exit(1);
  }
}

double *table (std::string kernel_type, int n)
{
  double *array = new double[(2*n+1)*(2*n+1)];
  double fi,fj,fh;
  for (int i=0;i<2*n+1;i++)
  { 
    std::cout << "progress = " << i << "/" << 2*n+1 << std::endl;
    for (int j=0;j<2*n+1;j++)
      {
        array[j*(2*n+1)+i] = 0;
        fi = double(i);
        fj = double(j);
        for (int h=0; h<2*n+1; h++)
          {
            fh = double(h);
            array[j*(2*n+1)+i] += kernel(kernel_type,fi/n-1,fj/n-1,fh/n-1);
          }
      }
  }
  std::cout << "progress = " << 2*n+1 << "/" << 2*n+1 << std::endl;
  double sum = 0;
  for (int i=0;i<2*n+1;i++)
    for (int j=0;j<2*n+1;j++) sum +=array[j*(2*n+1)+i];
  for (int i=0;i<2*n+1;i++)
    for (int j=0;j<2*n+1;j++) array[j*(2*n+1)+i] /= sum;
  return(array);
}

double *integral_table (double *source_array, int width, int height)
{
  double *target_array = new double[(width+1)*(height+1)];
  
  for (int i=0; i<width+1; i++)
    for (int j=0; j<height+1; j++)
    {
      if (i == 0 || j == 0) target_array[j*width+i] = 0;
      else target_array[j*(width+1)+i] = target_array[j*(width+1)+i-1]
            +target_array[(j-1)*(width+1)+i]
            -target_array[(j-1)*(width+1)+i-1]
            +source_array[(j-1)*width+i-1];
    }
    
  return(target_array);
}

void save_kernel_tables (std::string kernel_type, int n, std::string table_filename, std::string integral_table_filename)
{
  double *kernel_table = table(kernel_type, n);
  double *kernel_integral_table = integral_table(kernel_table, 2*n+1, 2*n+1);
  write_fits<double,double>(table_filename, 2*n+1, 2*n+1, kernel_table);
  write_fits<double,double>(integral_table_filename, 2*n+2, 2*n+2, kernel_integral_table);  
}

};




