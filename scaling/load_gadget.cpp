#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>

#include "load_gadget.h"

#define int4bytes int

/*--------- comment/uncomment to remove/enable DEBUG outputs ------------------*/
//#define MY_DEBUG

/*-----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------*/
/*---------------------------- Low Level Routines -----------------------------*/
/*-----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------*/
int4bytes blksize, swapstate=0;
#define SKIP {my_fread(&blksize,sizeof(int),1,fd); swap_Nbyte((char*)&blksize,1,4);}

/*---------------------- Basic routine to read data from a file ---------------*/
size_t my_fread(void *ptr, size_t size, size_t nmemb, FILE * stream)
{
  size_t nread;

  if((nread = fread(ptr, size, nmemb, stream)) != nmemb)
    {
      printf("I/O error (fread) ! (nread = %lu)\n", (unsigned long) nread);
      exit(3);
    }
  return nread;
}

/*-----------------------------------------------------------------------------*/
/*---------------------- Routine to swap ENDIAN -------------------------------*/
/*-------- char *data:    Pointer to the data ---------------------------------*/
/*-------- int n:         Number of elements to swap --------------------------*/
/*-------- int m:         Size of single element to swap ----------------------*/
/*--------                int,float = 4 ---------------------------------------*/
/*--------                double    = 8 ---------------------------------------*/
/*-----------------------------------------------------------------------------*/
void swap_Nbyte(char *data,int n,int m)
{
  int i,j;
  char old_data[16];

  if(swapstate>0)
    {
      for(j=0;j<n;j++)
        {
          memcpy(&old_data[0],&data[j*m],m);
          for(i=0;i<m;i++)
            {
              data[j*m+i]=old_data[m-i-1];
            }
        }
    }
}

/*-----------------------------------------------------------------------------*/
/*---------------------- Routine find a block in a snapfile -------------------*/
/*-------- FILE *fd:      File handle -----------------------------------------*/
/*-------- char *label:   4 byte identifyer for block -------------------------*/
/*-------- returns length of block found, -------------------------------------*/
/*-------- the file fd points to starting point of block ----------------------*/
/*-----------------------------------------------------------------------------*/
int find_block_format1(FILE *fd,char *label)
{
#ifdef MY_DEBUG
  printf("Searching Block in Format 1 file\n");
#endif

  int blocknum = 0, cur_blksize;

  if (strcmp(label,"HEAD")==0) blocknum = 1;
  else if (strcmp(label,"POS ")==0) blocknum = 2;
  else if (strcmp(label,"VEL ")==0) blocknum = 3;
  else if (strcmp(label,"ID  ")==0) blocknum = 4;
  else if (strcmp(label,"MASS")==0) blocknum = 5;
  else if (strcmp(label,"U   ")==0) blocknum = 6;
  else if (strcmp(label,"RHO ")==0) blocknum = 7;
  else if (strcmp(label,"NE  ")==0) blocknum = 8;
  else if (strcmp(label,"NH  ")==0) blocknum = 9;
  else if (strcmp(label,"HSML")==0) blocknum = 10;
  else if (strcmp(label,"SFR ")==0) blocknum = 11;
  else if (strcmp(label,"AGE ")==0) blocknum = 12;
  else if (strcmp(label,"Z   ")==0) blocknum = 13;
  else if (strcmp(label,"MBH ")==0) blocknum = 14;
  else if (strcmp(label,"MDBH")==0) blocknum = 15;   
  else
  {
    printf("block <%s> not supported",label);
    exit(1);
  } 

  rewind(fd);
  SKIP;
  if(blksize != 256) swapstate = 1-swapstate;        
  rewind(fd);
  SKIP;
  if(blksize != 256)
  {
    printf("incorrect format (blksize=%d)!\n",blksize);
    exit(1);
  }
  rewind(fd);

  int blockpos = 1;
  while(!feof(fd) && blockpos<blocknum)
  {
    SKIP;
    cur_blksize = blksize;
#ifdef MY_DEBUG
    printf("Found Block with %d bytes\n",blksize);
#endif
    fseek(fd,blksize,1);
    SKIP;
    if (blksize != cur_blksize)
    {
      printf("missed block end");
      exit(1);
    }
    ++blockpos;        
  }

  SKIP;
  fseek(fd,-4,1);
  return(blksize);
}

int find_block(FILE *fd,char *label)
{
  int4bytes blocksize=0;
  char blocklabel[5]={"    "};

  rewind(fd);

  SKIP;
  if(blksize!=134217728 && blksize!=8)
  {
    blocksize = find_block_format1(fd, label) + 8;
  }
  else
  {
#ifdef MY_DEBUG
    printf("Searching Block in Format 2 file\n");
#endif    
    rewind(fd);

    while(!feof(fd) && blocksize == 0)
    {
      SKIP;
      if(blksize == 134217728)
      {
#ifdef MY_DEBUG
        printf("Enable ENDIAN swapping !\n");
#endif
        swapstate=1-swapstate;
        swap_Nbyte((char*)&blksize,1,4);
      }
      if(blksize != 8)
      {
        printf("incorrect format (blksize=%d)!\n",blksize);
        exit(1);
      }
      else
      {
        my_fread(blocklabel, 4*sizeof(char), 1, fd);
        my_fread(&blocksize, sizeof(int4bytes), 1, fd);
        swap_Nbyte((char*)&blocksize,1,4);
#ifdef MY_DEBUG
        printf("Found Block <%s> with %d bytes\n",blocklabel,blocksize);
#endif
        SKIP;
	if(strcmp(label,blocklabel)!=0)
	{ 
          fseek(fd,blocksize,1);
          blocksize=0;
	}
      }
    }
  }
  return(blocksize-8);
}

/*-----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------*/
/*---------------------------- High Level Routines ----------------------------*/
/*-----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------*/


/*---------------------- Routine to read the header information ---------------*/
/*-------- int *npart:    List of Particle numbers for spezies [0..5] ---------*/
/*-------- int *massarr:  List of masses for spezies [0..5] -------------------*/
/*-------- int *time:     Time of snapshot ------------------------------------*/
/*-------- int *massarr:  Redshift of snapshot --------------------------------*/
/*-------- FILE *fd:      File handle -----------------------------------------*/
/*-------- returns number of read bytes ---------------------------------------*/
/*-----------------------------------------------------------------------------*/
int read_gadget_head(int *npart,double *massarr,double *time,double *redshift,FILE *fd)
{
  ::swapstate=0;
  int blocksize,dummysize;

  blocksize = find_block(fd,(char*)"HEAD");
  if(blocksize <= 0)
    {
      printf("Block <%s> not fond !\n","HEAD");
      exit(5);
    }
  else
    {
       dummysize=blocksize - 6 * sizeof(int) - 8 * sizeof(double);
       SKIP;
       my_fread(npart,6*sizeof(int), 1, fd);        swap_Nbyte((char*)npart,6,4);
       my_fread(massarr,6*sizeof(double), 1, fd);   swap_Nbyte((char*)massarr,6,8);
       my_fread(time,sizeof(double), 1, fd);        swap_Nbyte((char*)time,1,8);
       my_fread(redshift,sizeof(double), 1, fd);    swap_Nbyte((char*)redshift,1,8);
       fseek(fd,dummysize,1);
       SKIP;
    }
  return(blocksize);
}

int read_gadget_head_full(int *npart,double *massarr,double *time,double *redshift,
                          int *sfr, int *feedback, int *nall, int *cooling, int *filenum,   
                          double *boxsize, double *omega_m, double *omega_l, double *hubble,FILE *fd)
{
  ::swapstate=0;
  int blocksize,dummysize;

  blocksize = find_block(fd,(char*)"HEAD");
  if(blocksize <= 0)
    {
      printf("Block <%s> not fond !\n","HEAD");
      exit(5);
    }
  else
    {
       dummysize=blocksize - 16 * sizeof(int) - 12 * sizeof(double);
       SKIP;
       my_fread(npart,6*sizeof(int), 1, fd);        swap_Nbyte((char*)npart,6,4);
       my_fread(massarr,6*sizeof(double), 1, fd);   swap_Nbyte((char*)massarr,6,8);
       my_fread(time,sizeof(double), 1, fd);        swap_Nbyte((char*)time,1,8);
       my_fread(redshift,sizeof(double), 1, fd);    swap_Nbyte((char*)redshift,1,8);
       my_fread(sfr,sizeof(int), 1, fd);            swap_Nbyte((char*)sfr,1,4);
       my_fread(feedback,sizeof(int), 1, fd);       swap_Nbyte((char*)feedback,1,4);
       my_fread(nall,6*sizeof(int), 1, fd);         swap_Nbyte((char*)nall,6,4);
       //fseek(fd,6*sizeof(int),1); //skip Nall(6)
       my_fread(cooling,sizeof(int), 1, fd);        swap_Nbyte((char*)cooling,1,4);
       my_fread(filenum,sizeof(int), 1, fd);        swap_Nbyte((char*)filenum,1,4);
       //fseek(fd,sizeof(int),1); //skip NumFiles
       my_fread(boxsize,sizeof(double), 1, fd);     swap_Nbyte((char*)boxsize,1,8);
       my_fread(omega_m,sizeof(double), 1, fd);     swap_Nbyte((char*)omega_m,1,8);
       my_fread(omega_l,sizeof(double), 1, fd);     swap_Nbyte((char*)omega_l,1,8);
       //fseek(fd,2*sizeof(double),1); //skip Omega0, OmegaLambda
       my_fread(hubble,sizeof(double), 1, fd);     swap_Nbyte((char*)hubble,1,8);              
       fseek(fd,dummysize,1);
       SKIP;
    }
  return(blocksize);
}

/*-----------------------------------------------------------------------------*/
/*---------------------- Routine to read a 1D float array ---------------------*/
/*-------- int *data:     Pointer where the data are stored to ----------------*/
/*-------- char *label:   Identifyer for the datafield to read ----------------*/
/*-------- FILE *fd:      File handle -----------------------------------------*/
/*-------- returns length of dataarray ----------------------------------------*/
/*-----------------------------------------------------------------------------*/
int read_gadget_float(float *data,char *label,FILE *fd)
{
  ::swapstate=0;
  int blocksize;

  blocksize = find_block(fd,label);
  if(blocksize <= 0)
    {
      printf("Block <%s> not fond !\n",label);
      exit(5);
    }
  else
    {
//#ifdef MY_DEBUG
       printf("Reading %d bytes of data from <%s>...\n",blocksize,label);
//#endif
       SKIP;
       my_fread(data,blocksize, 1, fd);
       swap_Nbyte((char*)data,blocksize/sizeof(float),4);
       SKIP;
    }
  return(blocksize/sizeof(float));
}

/*-----------------------------------------------------------------------------*/
/*---------------------- Routine to read a 3D float array ---------------------*/
/*-------- int *data:     Pointer where the data are stored to ----------------*/
/*-------- char *label:   Identifyer for the datafield to read ----------------*/
/*-------- FILE *fd:      File handle -----------------------------------------*/
/*-------- returns length of dataarray ----------------------------------------*/
/*-----------------------------------------------------------------------------*/
int read_gadget_float3(float *data,char *label,FILE *fd)
{
  ::swapstate=0;
  int blocksize;

  blocksize = find_block(fd,label);
  if(blocksize <= 0)
    {
      printf("Block <%s> not found !\n",label);
      exit(5);
    }
  else
    {
//#ifdef MY_DEBUG
       printf("Reading %d bytes of data from <%s>...\n",blocksize,label);
//#endif
       SKIP;
       my_fread(data,blocksize, 1, fd);
       swap_Nbyte((char*)data,blocksize/sizeof(float),4);
       SKIP;
    }
  return(blocksize/sizeof(float)/3);
}

/*-----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------*/
/*---------------------------- Small Example HowToUse -------------------------*/
/*-----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------*/

/*int main(int argc, char **argv)
{
  FILE *fd = 0;
  char filename[265];
  int n,ntot=0,npart[6];
  //int cooling,sfr,feedback;
  double masstab[6],redshift,time;
  //double boxsize,hubble;
  //float *rho, *mass, *uint, *temp, *nh;
  float *Z; 
  struct vector *pos;//,*b;

  if(argc >= 2)
    {
      strcpy(filename,argv[1]);
      if(!(fd = fopen(filename,"rb")))
        {
           printf("Cant open file <%s> !\n",filename);
           exit(2);
        }  
      else
        {
         //----------- READ HEADER TO GET GLOBAL PROPERTIES -----------
          n = read_gadget_head(npart,masstab,&time,&redshift,fd);
          //n = read_gadget_head_full(npart,masstab,&time,&redshift,&sfr,&feedback,
          //                          &cooling,&boxsize,&hubble,fd);
          for(int i=0; i<6; i++) ntot += npart[i];   

         //---------- ALLOCATE MEMORY ---------------------------------
          //rho = (float*) malloc(npart[0]*sizeof(float));
          //b = (struct vector*) malloc(3*npart[0]*sizeof(float));
          pos = (struct vector*) malloc(3*ntot*sizeof(float));
          //mass = (float*) malloc(ntot*sizeof(float));
          //uint = (float*) malloc(npart[0]*sizeof(float));
          //temp = (float*) malloc(npart[0]*sizeof(float));
          //nh = (float*) malloc(npart[0]*sizeof(float));
          //temp = (float*) malloc(npart[0]*sizeof(float));
          Z = (float*) malloc((npart[0]+npart[4])*sizeof(float));

         //---------- READ DATA BLOCKS --------------------------------
          //n = read_gadget_float(rho,"RHO ",fd);
          n = read_gadget_float3((float*)pos,"POS ",fd);
          //n = read_gadget_float(mass,"MASS",fd);
          //n = read_gadget_float(uint,"U   ",fd);
          //n = read_gadget_float(temp,"TEMP",fd);
          //n = read_gadget_float(nh,"NH  ",fd);
          n = read_gadget_float(Z,"Z   ",fd);
          //n = read_gadget_float3((float*)b,"BFLD",fd);

         //---------- SOME TEST OUTPUT --------------------------------

         printf("first particle x = %f \n",pos[0].x);
         printf("last particle x = %f \n",pos[ntot-1].x);
         printf("first particle Z = %f \n",Z[0]);
         printf("last particle Z = %f \n",Z[npart[0]-1]);
         printf("redshift = %f \n",redshift);

         //---------- CLOSE FILE AND FREE DATA ------------------------
          fclose(fd);
          //free(rho);
          free(pos);
          //free(mass);
          //free(uint);
          //free(temp);
          //free(nh);
          //free(b);
          free(Z);
        }
    }
  else
    {
      printf("Please give a filename ...\n");
      exit(4);
    }
  exit(0);
}*/
