import pylab as pyl

import snap

simdir = "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c320_box_Arepo_new/"
simdir = "/data/curie4/nh444/project1/zoom_ins/MDRInt/c128_MDRInt/"
simdir = "/data/curie4/nh444/project1/zoom_ins/MDRInt/c384_MDRInt/"

sim = snap.snapshot(simdir, 25, mpc_units = True, group_veldisp = True, masstab = True)

r500c = sim.cat.group_r_crit500[0]
m500c = sim.cat.group_m_crit500[0]
pos = sim.cat.group_pos[0]

sim.load_gaspos()

#rproj = pyl.sqrt((sim.gaspos[:,0]-pos[0])**2 + (sim.gaspos[:,1]-pos[1])**2)
#ind = pyl.where(rproj < 5.0*r500c)[0]

r = sim.nearest_dist_3D(sim.gaspos, pos)
ind = pyl.where(r < 5.0*r500c)[0]


sim.get_SZ_yA()

yA = sim.yA[ind].sum(dtype=pyl.float64)/snap.Mpc**2 ## convert m^2 to Mpc^2, h has been included

print yA

## Below is the same calculation as in SZ_scaling.py
from scipy.constants import m_p as mproton ## in kg
from scipy.constants import m_e as melectron ## in kg
from scipy.constants import parsec as pc # in m
from scipy.constants import pi
mproton_in_g = mproton * 1000.0
melectron_in_g = melectron * 1000.0
Eelectron_in_kev = 510.99891 ## rest mass energy of electron in keV
thomson = 6.6524587158e-29 #thomson cross-section m^2
UnitMass_in_g = 10.0**10 * 1.9884430e33
Xh = 0.76 # hydrogen mass fraction
    
electrons = sim.gasmass[ind] * UnitMass_in_g / (mproton_in_g + melectron_in_g) * Xh * sim.ne[ind] ## number of electrons per particle
#ne = rho * UnitMass_in_g / (mproton_in_g + melectron_in_g) * Xh * NE * s.header.hubble / (1.0e3*pc*100.0)**3.0
PV = electrons * sim.temp[ind] #temp=kT, assume electron temp same as gas temp (equilibrium)
## Mass-weighted sum
Y = pyl.sum(PV) * thomson / Eelectron_in_kev / sim.header.hubble ## now in m^2
Y /= snap.Mpc**2 ## convert m^2 to Mpc^2

print Y
print yA / Y
## ratio is 1.00054454549 so there's some rounding error or something somewhere

from astropy.cosmology import FlatLambdaCDM
cm = FlatLambdaCDM(H0 = sim.header.hubble*100.0, Om0=sim.header.omega_m)
print cm.efunc(sim.header.redshift)**(-1.0)
Y *= cm.efunc(sim.header.redshift)**(-1.0) / (500.0*1.0e6*pc/(180.0/pi*60.0))**2.0
print Y