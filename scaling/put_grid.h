namespace pg{

double spline (double u1, double u2, double u3);

double kernel (std::string kernel_type, double u1, double u2, double u3);

double *table (std::string kernel_type, int n);

double *integral_table (double *source_array, int width, int height);

double save_kernel_tables (std::string kernel_type, int n, std::string table_filename, std::string integral_table_filename);

template <class datatype, class int_tab_type, class pix_tab_type> 
int put_function(int_tab_type *int_tab, int tabsize, datatype *array, int y_grid_size, int z_grid_size, double value, double ypos, double zpos, double h_kernel, pix_tab_type *pix_tab)
//----- PUTS A PARTICLE ON A GRID WITH A GIVEN DISTIRIBUTION FUNCTION -----
//int_tab ... table of the integral of the kernel function
//tabsize ... size of int_tab, has to be even
//array ... grid on which to put the particle
//y_grid_size ... width of array
//z_grid_size ... height of array
//value ... value to put on the grid for the particle
//ypos ... particle horizontal position in pixels
//zpos ... particle vertical poaition in pixels
//h_kernel ... smoothing length in pixels (radius of the kernel function)
//pix_tab ... array for use during calculation has to be of size
//            (grid_size+1)*(grid_size+1) with value 0 everywhere
//            it will also be returned with value 0 everywhere
{
  int ii, jj, l_table=(tabsize-2)/2;
  int iimin, iimax, jjmin, jjmax, inttaby, inttabz;
  double ymin,zmin,tabnumy,tabnumz,ay,az,A1,A2,A3,A4;
  // here ii - horizontal coordinate, jj - vertical coordinate

  //--------- PUT QUANTITY ON GRID WITH DISTRIBUTION FUNCTIONS ---------------
  if (ypos <= y_grid_size-1+0.5+(h_kernel+h_kernel/(2*l_table)) && ypos>=-0.5-(h_kernel+h_kernel/(2*l_table)) &&
      zpos <= z_grid_size-1+0.5+(h_kernel+h_kernel/(2*l_table)) && zpos>=-0.5-(h_kernel+h_kernel/(2*l_table)))
      {
      //calculate values for pixels
      ymin=ypos-h_kernel;
      zmin=zpos-h_kernel;
      iimin=int(ceil(ymin-h_kernel/(2.0*l_table)-0.5));
      jjmin=int(ceil(zmin-h_kernel/(2.0*l_table)-0.5));
      iimax=int(ceil(ypos+h_kernel+h_kernel/(2.0*l_table)-0.5));
      jjmax=int(ceil(zpos+h_kernel+h_kernel/(2.0*l_table)-0.5));
      
      for (jj = std::max(jjmin,-1); jj < std::min(jjmax,z_grid_size); jj++)
      for (ii = std::max(iimin,-1); ii < std::min(iimax,y_grid_size); ii++)
        {
        tabnumy=((ii+0.5)-ymin)*l_table/h_kernel+0.5;
        tabnumz=((jj+0.5)-zmin)*l_table/h_kernel+0.5;
        inttaby=int(floor(tabnumy));
        inttabz=int(floor(tabnumz));
        //warning: when the side of the integral table is exactly (within machine precision)
        //         at the side of a pixel inttaby+1 or inttabz+1 may be larger than the 
        //         dimension of inttab, resulting in reading the wrong value, if reading after
        //         the end of inttab it may result in a crash 
        ay=tabnumy-inttaby;
        az=tabnumz-inttabz;
        A1=int_tab[inttabz*tabsize+inttaby];
        //A1 integral value left below upper right corner of pixel ii, jj
        A2=int_tab[inttabz*tabsize+inttaby+1];
        //A2 integral value right below upper right corner of pixel ii, jj
        A3=int_tab[(inttabz+1)*tabsize+inttaby];
        //A3 integral value left above upper right corner of pixel ii, jj
        A4=int_tab[(inttabz+1)*tabsize+inttaby+1];
        //A4 integral value right above upper right corner of pixel ii, jj
        pix_tab[(jj+1)*(y_grid_size+1)+ii+1]=A1+(A2-A1)*ay+(A3-A1)*az+(A4-A3-A2+A1)*ay*az;
        /* pix_tab[(jj+1)*(y_grid_size+1)+ii+1)] is the value of the integral
        of the approximated cubic spline function at the upper, right corner of pixel
        ii, jj */
        }
        
      if (iimax<y_grid_size)
      {
        ii = iimax;
        for (jj = std::max(jjmin,-1); jj < std::min(jjmax,z_grid_size); jj++)
        {
          inttaby=tabsize-1;
          tabnumz=((jj+0.5)-zmin)*l_table/h_kernel+0.5;
          inttabz=int(floor(tabnumz));
          az=tabnumz-inttabz;
          A1=int_tab[inttabz*tabsize+inttaby];
          A3=int_tab[(inttabz+1)*tabsize+inttaby];
          pix_tab[(jj+1)*(y_grid_size+1)+ii+1]=A1+(A3-A1)*az;
        }
      }
      
      if (jjmax<z_grid_size)
      {
        jj = jjmax;
        for (ii = std::max(iimin,-1); ii < std::min(iimax,y_grid_size); ii++)
        {
          inttabz=tabsize-1;
          tabnumy=((ii+0.5)-ymin)*l_table/h_kernel+0.5;
          inttaby=int(tabnumy);
          ay=tabnumy-inttaby;
          A1=int_tab[inttabz*tabsize+inttaby];
          A2=int_tab[inttabz*tabsize+inttaby+1];
          pix_tab[(jj+1)*(y_grid_size+1)+ii+1]=A1+(A2-A1)*ay;
        }
      }
      
      if (iimax<y_grid_size && jjmax<z_grid_size) pix_tab[(jjmax+1)*(y_grid_size+1)+iimax+1]=int_tab[tabsize*tabsize-1];

      //write values for pixels
      if (iimin >= 0 && jjmin >= 0)
        array[jjmin*y_grid_size+iimin]+=pix_tab[(jjmin+1)*(y_grid_size+1)+iimin+1]*value;

      if (jjmin >=0)
      {
      jj=jjmin;
      for (ii = std::max(iimin+1,0); ii <= std::min(iimax,y_grid_size-1); ii++)
        array[jj*y_grid_size+ii]+=(pix_tab[(jj+1)*(y_grid_size+1)+ii+1]
                                -pix_tab[(jj+1)*(y_grid_size+1)+ii])*value;
      }

      if (iimin >=0)
      {
      ii = iimin;
      for (jj = std::max(jjmin+1,0); jj <= std::min(jjmax,z_grid_size-1); jj++)
        array[jj*y_grid_size+ii]+=(pix_tab[(jj+1)*(y_grid_size+1)+ii+1]
                                -pix_tab[jj*(y_grid_size+1)+ii+1])*value;
      }

      for (jj = std::max(jjmin+1,0); jj <= std::min(jjmax,z_grid_size-1); jj++)
        for (ii = std::max(iimin+1,0); ii <= std::min(iimax,y_grid_size-1); ii++)
          array[jj*y_grid_size+ii]+=(pix_tab[(jj+1)*(y_grid_size+1)+ii+1]
                                  -pix_tab[(jj+1)*(y_grid_size+1)+ii]
                                  -pix_tab[jj*(y_grid_size+1)+ii+1]
                                  +pix_tab[jj*(y_grid_size+1)+ii])*value;

      //clear pix_tab;
      for (jj = std::max(jjmin,-1); jj <= std::min(jjmax,z_grid_size-1); jj++)
        for (ii = std::max(iimin,-1); ii <= std::min(iimax,y_grid_size-1); ii++)
          pix_tab[(jj+1)*(y_grid_size+1)+ii+1]=0;

      return(1);
      }
  else return(0);         
}

template <class datatype>
int put_cloud(datatype *array, int grid_size, double value, double ypos, double zpos)
{
  int inty = int(floor(ypos)), intz = int(floor(zpos));
  double dy = ypos-inty, dz = zpos-intz;
  int status=0;
  
  if (inty >= 0 && inty <= grid_size-1 && intz >=0 && intz <= grid_size-1)
    {array[intz*grid_size+inty] += value*(1-dy)*(1-dz); status=1;}
  if (inty+1 >= 0 && inty+1 <= grid_size-1 && intz >=0 && intz <= grid_size-1)
    {array[intz*grid_size+inty+1] += value*dy*(1-dz); status=1;}
  if (inty >= 0 && inty <= grid_size-1 && intz+1 >=0 && intz+1 <= grid_size-1)
    {array[(intz+1)*grid_size+inty] += value*(1-dy)*dz; status=1;}
  if (inty+1 >= 0 && inty+1 <= grid_size-1 && intz+1 >=0 && intz+1 <= grid_size-1)
    {array[(intz+1)*grid_size+inty+1] += value*dy*dz; status=1;}
  return(status);
}

};

