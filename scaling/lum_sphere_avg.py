"""
Calculate bremsstrahlung luminosity in spherical bins centred on a halo
"""

import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np
import os
import scipy.stats as stats
import sys
from time import sleep

outname = "L_avg-M500_relation.pdf"
outdir = "/home/nh444/data/project1/L-T_relations/"
obs_dir = "/home/nh444/vault/ObsData/L-M/"

snapnum = 25
h = 0.679
simdir = "/data/emergence4/deboras/Elliptical_ZoomIns/"
simnames = ["L40_512_NoDutyRadioWeak_HalfThermal_FixEta",
            "L40_512_NoDutyRadioWeak_HalfThermal_FixEtaRadEff",
            "L40_512_NoDutyRadioWeak_HalfThermal_FixEta_BHChangeVel",
            "L40_512_NoDutyRadioWeak_HalfThermal_FixEta_BHChange",
            "L40_512_LowDutyRadioWeak_HalfThermal_FixEta",
            "L40_512_LowDutyRadioWeak_HalfThermal_FixEta_BHChangeVel",
            "L40_512_TempDutyRadioWeak_HalfThermal_FixEta_BHChangeVel",
            "L40_512_NoDutyRadioIntLow_HalfThermal_FixEta",
            "L40_512_NoDutyRadioIntLow_HalfThermal_FixEta_BHChangeVel",
            "L40_512_NoDutyRadioInt_HalfThermal_FixEta",
            "L40_512_NoDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_Prex",
            "L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff"]
labels = [r"NDRW",
          r"NDRWRadEff",
          r"NDRWBHVel",
          r"NDRWBHAcc",
          r"LDRW",
          r"LDRWBHVel",
          r"TDRWBHVel",
          r"NDRIntLow",
          r"NDRIntLowBHVel",
          r"NDRInt",
          r"NDRIntRadioEffBHVelPre",
          r"LDRIntRadioEffBHVel"]

simnames = ["L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff"]
labels = [r"LDRIntRadioEffBHVel"]

simdir = "/data/curie4/nh444/project1/boxes/"
simnames = ["L40_512_MDRIntRadioEff"]
labels = ["fiducial"]

median = [True, True, True, True, True, True, True, True, True, True, True, True]

Tlim = 3e4 ## lower temp limit (K)
logM500max = 15.2 ## currently for plotting only
logM500min = 11.5 ## log10 of minimum M500 (Msun) to compute lum for
M500min = 10**logM500min
h_scale = 0.678 #value of h to scale output values and observation values to

def main():
    #save_L_M500(num=100, use_T500=False, outfile_suffix="main_subhalo")
    #plot_L_M500_multiple()
    plot_L_M500_single(outfile_suffix="main_subhalo")
    #plot_L_M500_comparison(outfile_suffixes=["main_subhalo", "all_subhalos"])

def save_L_M500(num=100, use_T500=False, outfile_suffix=""):
    import snap
    from scipy.stats import binned_statistic
    import numpy as np

    metallicity = 0.3 #metal abundance, solar units
    ag_hydrogen = 0.70683 # Anders & Grevesse solar mass fractions 
    ag_helium = 0.27431
    ag_metals = 0.01886
    nelec = 1.0 + 0.5*ag_helium/ag_hydrogen + 0.5*metallicity*ag_metals/ag_hydrogen # n_e / n_H
    #nH = ne / nelec
    f = ag_hydrogen/(ag_hydrogen+ag_helium+metallicity*ag_metals) ## hydrogen mass fraction ~ 0.71
    
    UnitLength_in_cm = 3.0856775807e21

    ## Formula for calculting luminosity from temp, mass, density
    calc_lum = lambda T, m, rho: 1.17e-23 * (1.98844e30 * 1e10)**2 / UnitLength_in_cm**3 * (1+f) / 4.0 / (1.67262171e-27)**2 * h * (1+z)**3 * T**0.5 * m * rho
    # Formula is 1.17e-23 / (1+f) * (kT)**0.5 * ne**2 for temp kT in keV and
    # electron number density ne in cm^-3 for fully ionised H+He plasma
    # (see eq 1.34 in Puchwein 2007 thesis or notebook)
    # 1.17e-23 is the factor calculated in ~/scripts/scaling/bremsstrahlung.nb
    # including constants and average Gaunt factor
    # (1e10 * 1.9e30)**2 is conversion of mass code units to kg
    # / UnitLength_in_cm**3 * is conversion to cm^-3
    # (1+f) / 4.0 / (1.67262171e-27)**2 is conversion of mass and mass-density
    # to electron number density (i.e. ne = (1+f)/2/m_p for fully ionised
    # H+He plasma) - one factor (1+f) is cancelled out in the formula
    # hubble and redshift scalings are taken from scaling_new.cpp

    groups = []
    lum_sum = []
    lum_avg = []
    M500 = []
    for simname in simnames:
        s = snap.snapshot(simdir+simname+"/", snapnum)
        h = s.header.hubble
        z = s.header.redshift
        
        print "Loading masses..."
        s.load_gasmass()
        print "Loading densities..."
        s.load_gasrho()
        print "Loading positions..."
        s.load_gaspos()
        print "Calculating temperatures..."
        s.get_temp(del_ne=False)
        print "Getting hostsubhalos..."
        s.get_hostsubhalos()
        print "Getting particle types..."
        s.get_parttype()
        ## Get gas particles
        ind = np.where(s.parttype == 0)
        mass = s.gasmass
        densities = s.gasrho
        positions = s.gaspos
        hostsubhalos = s.hostsubhalos[ind]

        print "Computing..."
        numgroups = np.sum((s.cat.group_m_crit500[:]*1.0e10 / s.header.hubble > M500min))
        numgroupsDone = 0
        for group in range(0, s.cat.ngroups):
            if int(numgroupsDone / numgroups * 100) % 5 == 0:
                sys.stdout.write("\r"+str(numgroupsDone)+"/"+str(numgroups)+"("+str(int(100.0 * numgroupsDone / numgroups))+"%)")
                sys.stdout.flush()
            if s.cat.group_m_crit500[group]*1.0e10 / s.header.hubble > M500min:
                #print "Doing group", group
                numgroupsDone += 1
                ## Use gas particles in main subhalo only
                ind = np.where(hostsubhalos == s.cat.group_firstsub[group])
                m = mass[ind]
                rho = densities[ind]
                pos = positions[ind]
                temp = s.temp[ind]
                ## Use all gas particles within r<R
                #m = mass
                #rho = densities
                #pos = positions
                #temp = s.temp

                rad = s.nearest_dist_3D(pos, s.cat.sub_pos[s.cat.group_firstsub[group]])
                rel_pos = s.rel_pos(pos, s.cat.sub_pos[s.cat.group_firstsub[group]])
                rad2D = ((rel_pos[:,0])**2 + (rel_pos[:,1])**2)**0.5
                R = s.cat.group_r_crit500[group]##This is not guaranteed to be the same R500 as that of the main subhalo but probably close

                if use_T500:
                    ind = np.where((rad2D <= R) & (temp > Tlim*1.38064852e-23/1.60217662e-19/1000.0)) ## look only within projected r500
                    r2D = rad2D[ind] / R
                else:
                    ind = np.where((rad < R) & (temp > Tlim*1.38064852e-23/1.60217662e-19/1000.0))
                r = rad[ind] / R
                m = m[ind]
                rho = rho[ind]
                temp = temp[ind]

                bins = np.logspace(-3, 0, num=num)
                mean_mass, bin_edges, bin_idx = binned_statistic(r, m, bins=bins, statistic='mean')
                mean_rho, bin_edges, bin_idx = binned_statistic(r, rho, bins=bins, statistic='mean')
                if use_T500:
                    sum_masstemp, bin_edges, bin_idx = binned_statistic(r2D, m*temp, bins=1, range=(0.3, 1.0), statistic='sum')
                    sum_mass, bin_edges, bin_idx = binned_statistic(r2D, m, bins=1, range=(0.15, 1.0), statistic='sum')
                    T500 = sum_masstemp[0] / sum_mass[0]
                    mean_temp = np.full_like(mean_mass, T500)
                    print str(group)+": T500 =", T500
                else:
                    #mean_temp, bin_edges, bin_idx = binned_statistic(r, temp, bins=bins, statistic='mean')
                    sum_masstemp, bin_edges, bin_idx = binned_statistic(r, m*temp, bins=bins, statistic='sum')
                    sum_mass, bin_edges, bin_idx = binned_statistic(r, m, bins=bins, statistic='sum')
                    mean_temp = sum_masstemp / sum_mass
                counts, bin_edges, bin_idx = binned_statistic(r, m, bins=bins, statistic='count')
                lum_means = calc_lum(mean_temp, mean_mass, mean_rho) * counts
                #1.17e-23 * 1e20 / 4.0 * (1.98844e30/1.67262171e-27)**2 / UnitLength_in_cm**3 * (1+f) * h * (1+z)**3 * (mean_temp)**0.5 * mean_mass * mean_rho * counts

                if use_T500: lum = calc_lum(T500, m, rho)
                else: lum = calc_lum(temp, m, rho)
                
                groups.append(group)
                lum_sum.append(np.sum(lum))
                lum_avg.append(np.sum(lum_means[np.isfinite(lum_means)]))
                #lum_med.append(np.sum(med_sum[np.isfinite(med_sum)]))
                M500.append(s.cat.group_m_crit500[group]*1.0e10 / s.header.hubble)

        if outfile_suffix is not "":
            if outfile_suffix[0] is not "_": outfile_suffix = "_"+outfile_suffix
        np.savetxt(outdir+simname+"/lum_avg_M500_snap_"+str(snapnum).zfill(3)+outfile_suffix+".txt", np.c_[groups, M500, lum_sum, lum_avg], header="group M500(Msun) lum_sum_r500 lum_avg_r500")

## For plotting just lum_avg for multiple simulations
def plot_L_M500_multiple():
    import os
    h_scale = 0.679
    
    fig = plt.figure(figsize=(12,10))

    anderson = np.genfromtxt(obs_dir+"Anderson2014_lum.csv", delimiter=",")
    plt.errorbar(anderson[:,2], anderson[:,11]+np.log10(0.704*0.704/(h*h)), yerr=anderson[:,12]+np.log10(0.704*0.704/(h*h)), c='#d34044', mec='none', marker='^', linestyle='none', ms=6, label="ROSAT LBGs (Anderson et al. 2014)")

    ## Median profiles
    bins = np.linspace(11, 15, num=40)
    x = (bins[:-1] + bins[1:])/2

    #colours = ['red', 'lightblue', 'green', 'purple', '0.5']
    colours = ["#d05352",
               "#be53be",
               "#65b97f",
               "#7979cf",
               "#9fd550",
               "#c0933d",
               "#44c7e2",
               "#d9d458",
               "#ce568b",
               "#5c8532"]
    col_idx = np.linspace(0, 1, num=len(simnames))
    markers = ['o', 's', r'$\bigodot$', 's', r'$\boxdot$', '^']
    mew = [0.8, 0.8, 0.6, 1.5, 0.6, 1.5]


    j = 0
    for i, simname in enumerate(simnames):
        if os.path.exists(outdir+simname+"/lum_avg_M500_snap_"+str(snapnum).zfill(3)+".txt"):
            data = np.loadtxt(outdir+simname+"/lum_avg_M500_snap_"+str(snapnum).zfill(3)+".txt")
            M500 = np.log10(data[:,1])
            med, bin_edges, n = stats.binned_statistic(M500, data[:,3], statistic='median', bins=bins)
            mask = np.isfinite(med)
            plt.plot(x[mask], np.log10(med[mask]), c=plt.cm.rainbow_r(col_idx[i]), label=labels[i], lw=1.5)
            if not median[i]:
                plt.plot(M500, np.log10(data[:,3]), marker=markers[j], mfc='none', mec=plt.cm.rainbow_r(col_idx[i]), ms=4, linestyle='none', mew=mew[j])
                j += 1


    #plt.xlim(11.5, 14.5)
    #plt.ylim(38,44)
    plt.ylabel("log$_{10}$($L_{500}$ [erg s$^{-1}$])", fontsize=16)
    plt.xlabel("log$_{10}$(M$_{500}$ [$M_{\odot}$])", fontsize=16)
    plt.legend(loc='upper left', frameon=False, borderaxespad=1, numpoints=1, fontsize=12, ncol=1)


    plt.savefig(outname, bbox_inches='tight')
    print "Saved to", outname

## For plotting direct sum luminosity and luminosity from mock observation code
## as well as the spherically averaged luminosity. Ideally just plot one sim
def plot_L_M500_single(outfile_suffix=""):
    import os
    h_scale = 0.679
    
    fig = plt.figure(figsize=(12,10))

    anderson = np.genfromtxt(obs_dir+"Anderson2014_lum.csv", delimiter=",")
    plt.errorbar(anderson[:,2], anderson[:,11]+np.log10(0.704*0.704/(h*h)), yerr=anderson[:,12]+np.log10(0.704*0.704/(h*h)), c='#d34044', mec='none', marker='^', linestyle='none', ms=6, label="ROSAT LBGs (Anderson et al. 2014)")

    ## Median profiles
    bins = np.linspace(11, 15, num=40)
    x = (bins[:-1] + bins[1:])/2

    #colours = ['red', 'lightblue', 'green', 'purple', '0.5']
    colours = ["#d05352",
               "#be53be",
               "#65b97f",
               "#7979cf",
               "#9fd550",
               "#c0933d",
               "#44c7e2",
               "#d9d458",
               "#ce568b",
               "#5c8532"]
    col_idx = np.linspace(0, 1, num=len(simnames))
    markers = ['o', 's', r'$\bigodot$', 's', r'$\boxdot$', '^']
    mew = [0.8, 0.8, 0.6, 1.5, 0.6, 1.5]


    if outfile_suffix is not "":
        if outfile_suffix[0] is not "_": outfile_suffix = "_"+outfile_suffix

    j = 0
    for i, simname in enumerate(simnames):
        if os.path.exists(outdir+simname+"/lum_avg_M500_snap_"+str(snapnum).zfill(3)+outfile_suffix+".txt"):
            if os.path.exists(os.path.normpath(outdir)+"/"+simname+"_"+str(snapnum).zfill(3)+"/prop_file.txt"):
                data = np.loadtxt((os.path.normpath(outdir)+"/"+simname+"_"+str(snapnum).zfill(3)+"/prop_file.txt"))
                med, bin_edges, n = stats.binned_statistic(np.log10(data[:,21]*1e10), data[:,18], statistic='median', bins=bins)
                mask = np.isfinite(med)
                plt.plot(x[mask], np.log10(med[mask]), c='orange', label=labels[i]+" (mock observation)", lw=1.5, linestyle='dashed')
            else:
                print "Could not find", os.path.normpath(outdir)+"/"+simname+"_"+str(snapnum).zfill(3)+"/prop_file.txt"
                
            data = np.loadtxt(outdir+simname+"/lum_avg_M500_snap_"+str(snapnum).zfill(3)+outfile_suffix+".txt")
            M500 = np.log10(data[:,1])
            med, bin_edges, n = stats.binned_statistic(M500, data[:,2], statistic='median', bins=bins)
            mask = np.isfinite(med)
            plt.plot(x[mask], np.log10(med[mask]), c='red', label=labels[i]+" (direct sum)", lw=1.5)
            med, bin_edges, n = stats.binned_statistic(M500, data[:,3], statistic='median', bins=bins)
            mask = np.isfinite(med)
            plt.plot(x[mask], np.log10(med[mask]), c='blue', label=labels[i]+" (spherically averaged)", lw=1.5, linestyle='dashed')

            plt.plot(M500, np.log10(data[:,3]), marker=markers[j], mec='none', mfc='blue', ms=2, linestyle='none', mew=mew[j])
            j += 1
        else:
            raise IOError("Could not find path" +
                          outdir + simname+"/lum_avg_M500_snap_"+str(snapnum).zfill(3)+outfile_suffix+".txt")

    plt.xlim(11.5, 14.5)
    #plt.ylim(38,44)
    plt.ylabel("log$_{10}$($L_{500}$ [erg s$^{-1}$])", fontsize=16)
    plt.xlabel("log$_{10}$(M$_{500}$ [$M_{\odot}$])", fontsize=16)
    plt.legend(loc='upper left', frameon=False, borderaxespad=1, numpoints=1, fontsize=12, ncol=1)


    plt.savefig(outname, bbox_inches='tight')
    print "Saved to", outname

def plot_L_M500_comparison(outfile_suffixes=[""]):
    import os
    h_scale = 0.679
    filename = "prop_file.txt"
    filename = "prop_file_Z_0.3_0-82.txt"
    
    fig = plt.figure(figsize=(12,10))

    anderson = np.genfromtxt(obs_dir+"Anderson2014_lum.csv", delimiter=",")
    plt.errorbar(anderson[:,2], anderson[:,11]+np.log10(0.704*0.704/(h*h)), yerr=anderson[:,12]+np.log10(0.704*0.704/(h*h)), c='green', mec='none', marker='^', linestyle='none', ms=7, label="ROSAT LBGs (Anderson et al. 2014)", zorder=3)

    ## Median profiles
    bins = np.linspace(11, 15, num=40)
    x = (bins[:-1] + bins[1:])/2

    col_idx = np.linspace(0.2, 1, num=len(outfile_suffixes))
    markers = ['o', 's', r'$\bigodot$', 's', r'$\boxdot$', '^']
    mew = [0.8, 0.8, 0.6, 1.5, 0.6, 1.5]

    for j, outfile_suffix in enumerate(outfile_suffixes):
        if outfile_suffix is not "":
            if outfile_suffix[0] is not "_": outfile_suffixes[j] = "_"+outfile_suffix

    j = 0
    for i, simname in enumerate(simnames):
        if os.path.exists(os.path.normpath(outdir)+"/"+simname+"_"+str(snapnum).zfill(3)+"/"+filename):
            data = np.loadtxt((os.path.normpath(outdir)+"/"+simname+"_"+str(snapnum).zfill(3)+"/"+filename))
            med, bin_edges, n = stats.binned_statistic(np.log10(data[:,21]*1e10), data[:,18], statistic='median', bins=bins)
            mask = np.isfinite(med)
            plt.plot(x[mask], np.log10(med[mask]), c='orange', label=labels[i]+" (mock observation)", lw=1.5, linestyle='dashed')
        else:
            print "Could not find", os.path.normpath(outdir)+"/"+simname+"_"+str(snapnum).zfill(3)+"/"+filename
        for j, outfile_suffix in enumerate(outfile_suffixes):
            if os.path.exists(outdir+simname+"/lum_avg_M500_snap_"+str(snapnum).zfill(3)+outfile_suffix+".txt"):
                data = np.loadtxt(outdir+simname+"/lum_avg_M500_snap_"+str(snapnum).zfill(3)+outfile_suffix+".txt")
                M500 = np.log10(data[:,1])
                med, bin_edges, n = stats.binned_statistic(M500, data[:,2], statistic='median', bins=bins)
                mask = np.isfinite(med)
                plt.plot(M500, np.log10(data[:,2]), marker=markers[j], mfc='none', mec=plt.cm.rainbow(col_idx[j]), ms=4, linestyle='none', mew=mew[j])
                plt.plot(x[mask], np.log10(med[mask]), c=plt.cm.rainbow(col_idx[j]), label=labels[i]+outfile_suffix+" (bremsstrahlung, direct sum)", lw=1.5)
                med, bin_edges, n = stats.binned_statistic(M500, data[:,3], statistic='median', bins=bins)
                mask = np.isfinite(med)
                #plt.plot(x[mask], np.log10(med[mask]), c=plt.cm.rainbow(col_idx[j]), label=labels[i]+outfile_suffix+" (spherically averaged)", lw=1.5, linestyle='dashed')
                #plt.plot(M500, np.log10(data[:,3]), marker=markers[j], mec='none', mfc=plt.cm.rainbow(col_idx[j]), ms=2, linestyle='none', mew=mew[j])
                j += 1
            ## Arrows
            data1 = np.loadtxt(outdir+simname+"/lum_avg_M500_snap_"+str(snapnum).zfill(3)+outfile_suffixes[0]+".txt")#
            data2 = np.loadtxt(outdir+simname+"/lum_avg_M500_snap_"+str(snapnum).zfill(3)+outfile_suffixes[1]+".txt")
            print M500[:20], data1[:20,3], data2[:20,3], data1[:20,3]/data2[:20,3]
            for k in range(0,len(data1[:,2])):
                if np.log10(data1[k,2]) > 41 or np.log10(data2[k,2]) > 41:
                    plt.gca().annotate("",
                                    xy=(np.log10(data1[k,1]), np.log10(data1[k,2])),
                                    xycoords='data', xytext=(np.log10(data2[k,1]),
                                                            np.log10(data2[k,2])),
                                    textcoords='data', arrowprops=dict(fc='0.7', ec='none', shrink=0.03, connectionstyle="arc3", width=1, headlength=5, headwidth=5),
                    )


    plt.xlim(11.5, 14.5)
    #plt.ylim(38,44)
    plt.ylabel("log$_{10}$($L_{500}$ [erg s$^{-1}$])", fontsize=16)
    plt.xlabel("log$_{10}$(M$_{500}$ [$M_{\odot}$])", fontsize=16)
    plt.legend(loc='upper left', frameon=False, borderaxespad=1, numpoints=1, fontsize=12, ncol=1)


    plt.savefig(outname, bbox_inches='tight')
    print "Saved to", outname

if __name__ == "__main__":
    main()
