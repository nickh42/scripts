import os

nbasedir = "./"
#basedir = "/home/nh444/data/project1/L-T_relations/L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_025/"

filenames = ["xspec_model.txt", "xspec_model_proj.txt", "fitted_model.txt", "fitted_model_proj.txt", "fitted_flux_lum.txt"]

for group in range(0, 90):
    for filename in filenames:
        os.system('cp '+basedir+'group_'+str(group)+'/'+filename+' '+basedir+'group_'+str(group)+'/'+os.path.splitext(filename)[0]+'_Z_0.3'+os.path.splitext(filename)[1])
