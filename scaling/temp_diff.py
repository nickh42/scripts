#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 24 16:10:58 2017
Plot difference between spectroscopic and mass-weighted global temperature
as a function of mass 
@author: nh444
"""
import numpy as np
import matplotlib.pyplot as plt
import os
import matplotlib.ticker as ticker


outdir = "/data/curie4/nh444/project1/L-T_relations/"
outname = "temp_diff.pdf"

indir = "/data/curie4/nh444/project1/L-T_relations/"
#simname = "L40_512_LDRIntRadioEffBHVel"
#zoomdir=indir+"LDRIntRadioEffBHVel/"
#zooms = ["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c256_box_Arepo_new", "c320_box_Arepo_new", "c384_box_Arepo_new",
#        "c448_box_Arepo_new", "c512_box_Arepo_new",
#        ]
simname = "L40_512_MDRIntRadioEff"
zoomdir=indir+"MDRInt/"
zooms = ["c96_MDRInt", "c128_MDRInt", "c192_MDRInt", "c256_MDRInt", "c320_MDRInt", "c384_MDRInt", "c448_MDRInt"]
snapnums = [25]
h = 0.679

markers = ['D', 's', 'o', '^', '>', 'v', '<']
ms = 7.5
mew=1.6
col = ['#E7298A']
col = ['#24439b','#225EA8', 'orange', 'green']

filenames = ["L-T_data_apec_0.5-10_default_Tvir_thresh_4.0.txt"]
labels = [None]*len(filenames)

#==============================================================================
# filenames = ["L-T_data_apec_0.5-10_default_Tvir_thresh_4.0.txt",
#              #"L-T_data_apec_0.03-10_default_Tvir_thresh_4.0.txt",
#              #"L-T_data_apec_0.5-10_default_Tvir_thresh_4.0_c07.txt", # ACIS-I
#              "L-T_data_apec_0.5-10_default_Tvir_thresh_4.0_c07_ACIS-S.txt",
#              #"L-T_data_apec_0.03-10_default_Tvir_thresh_4.0_c07.txt",
#              "L-T_data_apec_0.5-10_default_Tvir_thresh_4.0_c20_ACIS-S.txt",
#              #"L-T_data_apec_0.5-10_default_Tvir_thresh_4.0_c20_ACIS-I.txt",
#              #"L-T_data_apec_0.03-10_default_Tvir_thresh_4.0_c20_ACIS-S.txt",
#              #"L-T_data_apec_0.5-10_Rasia2012_Tvir_thresh_4.0.txt",
#              ]
# labels = [None]*len(filenames)
# labels = ["default [0.5-10 keV fit]",
#           #"default [0.03-10 keV fit]",
#           #"ACIS-I C07 [0.5-10 keV fit]",
#           "ACIS-S C07 [0.5-10 keV fit]",
#           #"ACIS-I C07 [0.03-10 keV fit]",
#           "ACIS-S C20 [0.5-10 keV fit]",
#           #"ACIS-I C20 [0.5-10 keV fit]",
#           #"ACIS-S C20 [0.03-10 keV fit]",
#           ]
# col = ["#c7673e","#78a44f","#6b87c8","#934fce","#e6ab02","#be558f","#47af8d"]
# 
#==============================================================================
#==============================================================================
# filenames = [#"L-T_data_apec_0.5-10_default_Tvir_thresh_4.0.txt",
#              "L-T_data_apec_default_Tvir_thresh_4.0_dummyresp.txt",
#              #"L-T_data_apec_default_Tvir_thresh_4.0_dummyresp_ignore_lt0.01.txt",
#              ]
# labels = [#"Chandra response",
#           "dummy response",
#           "dummy response ignoring $<0.01$ keV"]
#==============================================================================


fig = plt.figure(figsize=(7,7))
M500min = 0.0#3e12
highlight = []#[5, 44, 100, 200] ## group numbers to highlight

# temp_mw, temp_emw, temp_bw, temp_ew, temp_mw_3D, temp_emw_3D, temp_bw_3D, temp_ew_3D, temp_3D
# 14,      15,       16,      17,      18,         19,          20,         21,         22,
spec_idx = 22 ## 5 for projected, 22 for 3D 
mw_idx = 18 ## 14 for projected mw, 18 for 3D mw

for isnap, snapnum in enumerate(snapnums):
    for i, filename in enumerate(filenames):
        data = np.loadtxt(indir+simname+"_"+str(snapnum).zfill(3)+"/"+filename)
        Tspec = data[:,spec_idx] 
        Tmw = data[:, mw_idx]
        M500 = data[:,12]*1e10/h # Msun
        #ind = np.where(M500 >= M500min)[0]
        print np.where(Tspec >= 1.0)[0], Tspec[np.where(Tspec >= 1.0)[0]]
        ind = np.where(Tspec >= 0.2)[0]
        #y = (Tspec-Tmw)/Tmw
        y = Tmw
        #x = M500
        x = Tspec
        plt.plot(x[ind], y[ind], ls='none', marker=markers[i], mfc='none', mec=col[i+isnap*len(filenames)], mew=mew, ms=ms,
                 label=labels[i])
        highind = np.in1d(data[:,0], highlight)
        plt.plot(x[highind], y[highind], ls='none', marker=markers[i], mfc='none', mec='r', mew=mew, ms=ms)
        
        for zoom in zooms:
            if os.path.exists(zoomdir+zoom+"_"+str(snapnum).zfill(3)+"/"+filename):
                data = np.loadtxt(zoomdir+zoom+"_"+str(snapnum).zfill(3)+"/"+filename)
                Tspec = data[spec_idx]
                Tmw_zoom = data[mw_idx]
                M500 = data[12]*1e10/h # Msun
                print zoom, Tspec, Tmw_zoom, M500
                #y = (Tspec-Tmw_zoom)/Tmw_zoom
                y = Tmw_zoom
                #x = M500
                x = Tspec
                plt.plot(x, y, ls='none', marker=markers[i], ms=ms, mfc='none', mec=col[i+isnap*len(filenames)], mew=mew)
                
#plt.axhline(y=0.0, ls='dashed', c='black')
plt.xlim(0.1, 10)
plt.ylim(0.1, 10)
plt.plot([0.1,10],[0.1,10], c='k')
plt.xscale('log')
plt.yscale('log')
#plt.xlim(xmin=M500min)
#plt.xlabel(r"M$_{500}$ (M$_{\odot}$)")
#plt.ylabel(r"$(\mathrm{T}_{\mathrm{spec}}-\mathrm{T}_{\mathrm{mw}})/\mathrm{T}_{\mathrm{mw}}$")
plt.xlabel(r"T$_{500}^{\mathrm{spec}}$ [keV]")
plt.ylabel(r"T$_{500}^{\mathrm{mw}}$ [keV]")
#plt.ylim(-0.5, 0.55)
#plt.xlim(xmin=8e12)
plt.gca().xaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))
plt.gca().yaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))
plt.legend(frameon=False, numpoints=1, loc='best')
plt.tight_layout()
plt.savefig(outdir+outname, bbox_inches='tight')
print "Output to", outdir+outname
