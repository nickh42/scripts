import sys
import numpy as np
import matplotlib.pyplot as plt
import snap
from matplotlib.backends.backend_pdf import PdfPages

basedir = "/data/emergence4/deboras/Elliptical_ZoomIns/"
simdir = "L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff/"
snapnum = 25
N = 4 ## N most massive halos to show
Nstart = 6 ## skip this many most massive halos
Zbins = np.linspace(0.0, 1.0, num=20)
pdf = PdfPages("metals_distribution.pdf")

ag_hydrogen = 0.70683 ## Anders, Grevesse mass fractions
ag_helium = 0.27431
ag_metals = 0.01886

snapshot = snap.snapshot(basedir+simdir, snapnum, mpc_units=False, masstab=False)

if hasattr(snapshot.cat, 'group_r_crit500'):
    R_500 = snapshot.cat.group_r_crit500[Nstart:Nstart+N]
    M_500 = snapshot.cat.group_m_crit500[Nstart:Nstart+N]
else:
    R_500 = np.zeros(N)
    M_500 = np.zeros(N)
    for i in range(0,N):
        M_500[i], R_500[i] = snapshot.get_so_mass_rad(i+Nstart, 500, ref='crit', only_sel=False)

fig = plt.figure(figsize=(10,8))
for i in range(0,N):
    centre = snapshot.cat.group_pos[Nstart+i]
    ind = snapshot.get_particles_sphere(centre, R_500[i], parttype=0)
    if not hasattr(snapshot, 'mass'):
        snapshot.load_mass()
    if not hasattr(snapshot, 'Zgas'):
        snapshot.load_Zgas()
    if not hasattr(snapshot, 'rho'):
        snapshot.load_rho()

    plt.hist(snapshot.Zgas[ind]/(1-snapshot.Zgas[ind])*(ag_hydrogen+ag_helium)/ag_metals, bins=Zbins, weights=snapshot.mass[ind]/10, histtype='step', label="group_"+str(i+Nstart)+": M$_{500}$ = "+'{:.1f}'.format(M_500[i]/1e3)+" x 10$^{13}$ M$_{\odot}$")
plt.xlabel("Metallicity (Z$_{\odot}$)")
plt.ylabel("Gas Mass within R$_{500}$ (10$^{11}$ M$_{\odot}$)")
plt.legend(loc='upper right', frameon=False, borderaxespad=1, numpoints=1, fontsize=14)
pdf.savefig(fig)

fig = plt.figure(figsize=(10,8))
for i in range(0,N):
    centre = snapshot.cat.group_pos[Nstart+i]
    ind = snapshot.get_particles_sphere(centre, R_500[i], parttype=0)
    if not hasattr(snapshot, 'mass'):
        snapshot.load_mass()
    if not hasattr(snapshot, 'Zgas'):
        snapshot.load_Zgas()
    if not hasattr(snapshot, 'rho'):
        snapshot.load_rho()
    plt.hist(snapshot.Zgas[ind]/(1-snapshot.Zgas[ind])*(ag_hydrogen+ag_helium)/ag_metals, bins=Zbins, weights=snapshot.mass[ind]*snapshot.rho[ind], histtype='step', label="group_"+str(i+Nstart)+": M$_{500}$ = "+'{:.1f}'.format(M_500[i]/1e3)+" x 10$^{13}$ M$_{\odot}$", normed=True)
plt.xlabel("Metallicity (Z$_{\odot}$)")
plt.ylabel("Emission measure probability density within R$_{500}$ (10$^{11}$ M$_{\odot}$)")
plt.legend(loc='upper left', frameon=False, borderaxespad=1, numpoints=1, fontsize=14)
pdf.savefig(fig)

fig = plt.figure(figsize=(10,8))
for i in range(0,N):
    centre = snapshot.cat.group_pos[Nstart+i]
    ind = snapshot.get_particles_sphere(centre, R_500[i], parttype=0)
    if not hasattr(snapshot, 'mass'):
        snapshot.load_mass()
    if not hasattr(snapshot, 'Zgas'):
        snapshot.load_Zgas()
    if not hasattr(snapshot, 'rho'):
        snapshot.load_rho()
    plt.hist(snapshot.Zgas[ind]/(1-snapshot.Zgas[ind])*(ag_hydrogen+ag_helium)/ag_metals, bins=Zbins, weights=snapshot.rho[ind], histtype='step', label="group_"+str(i+Nstart)+": M$_{500}$ = "+'{:.1f}'.format(M_500[i]/1e3)+" x 10$^{13}$ M$_{\odot}$")
plt.xlabel("Metallicity (Z$_{\odot}$)")
plt.ylabel("Total Density within R$_{500}$ (10$^{11}$ M$_{\odot}$)")
plt.legend(loc='upper left', frameon=False, borderaxespad=1, numpoints=1, fontsize=14)
pdf.savefig(fig)

pdf.close()

