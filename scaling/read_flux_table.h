class channel_data{

protected:
  int channel, tempnum, abundnum;
  double redshift, mintemp, maxtemp, minabund, maxabund, nHwabs;
  double chmin, chmax;
  double *data;

public:
  channel_data (const char *filename, int num);
  double getflux (double abund, double temp, bool &inside_range);
  void show();
};
