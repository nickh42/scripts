"""
Compute, save and plot SZ signal within R500 as a function of halo mass
"""

def main():
    import numpy as np
    
    compare={'Planck15': False, 'PlanckLBG':False, 'Wang':False, 'SPT': False}
    outdir="/data/curie4/nh444/project1/SZ/"
    
    basedir = "/data/emergence4/deboras/Elliptical_ZoomIns/"
    simdirs = ["L40_512_NoDutyRadioWeak_HalfThermal_FixEta",
            "L40_512_NoDutyRadioInt_HalfThermal_FixEta",
            "L40_512_LowDutyRadioWeak_HalfThermal_FixEta",
            "L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff",
            ]
    labels = ["Ref",
              "Stronger radio",
              "Quasar duty cycle",
              "Combined"]
    basedir = "/data/curie4/nh444/project1/boxes/"
    simdirs = [#"L40_512_NoDutyRadioWeak",
               #"L40_512_NoDutyRadioInt",
               #"L40_512_DutyRadioWeak",
               "L40_512_LDRIntRadioEffBHVel",
               ]
    labels = [#"Weak radio",
              #"Stronger radio",
              #"Quasar duty cycle",
              "Combined"]
    
    simdirs = [#"L40_512_NoDutyRadioWeak",
               #"L40_512_NoDutyRadioInt",
               #"L40_512_DutyRadioWeak",
               #"L40_512_LDRIntRadioEffBHVel",
               "L40_512_LDRIntRadioEff_noBH",
               #"L40_512_MDRIntRadioEff",
               ]
    labels = [#"Weak radio",
              #"Stronger radio",
              #"Quasar duty cycle",
              #"$\delta t = 10$ Myr",
              None,#"fiducial",#"$\delta t = 25$ Myr",
              ]

    zoombasedir = "/home/nh444/data/project1/zoom_ins/"
    zoomdirs = [#"","","",
                #"LDRIntRadioEffBHVel/",
                "MDRInt/",
                ] ## one for each in simdirs
    zooms = [#["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c192_box_Arepo_new", "c256_box_Arepo_new", "c320_box_Arepo_new", "c384_box_Arepo_new", "c448_box_Arepo_new"],
             #["c96_box_Arepo_new", "c128_box_Arepo_new", "c192_box_Arepo_new", "c256_box_Arepo_new", "c384_box_Arepo_new"],
             ["c96_MDRInt", "c128_MDRInt", "c192_MDRInt", "c224_MDRInt", "c256_MDRInt",
              "c288_MDRInt", "c320_MDRInt", "c352_MDRInt", "c384_MDRInt",
              #"c400_MDRInt", "c410_MDRInt",
              "c448_MDRInt","c512_MDRInt",]
              ]
    zooms = [["c96_MDRInt", "c128_MDRInt", "c160_MDRInt",
              "c192_MDRInt", "c224_MDRInt", "c256_MDRInt",
              "c288_MDRInt", "c320_MDRInt", "c352_MDRInt",
              "c384_MDRInt", #"c410_MDRInt",
              "c448_MDRInt", "c480_MDRInt", "c512_MDRInt"]]
    snapnums = range(20,26)[::-1]
    #zooms = [["c400_MDRInt", "c410_MDRInt"]]
    #snapnums = range(1, 100)
    #zooms = [["c224_MDRInt", "c288_MDRInt", "c352_MDRInt"]]
    #snapnums = range(20, 26)
    
    Rlims=[0.0, 5.0]
    Rlimtype="R500"
    suffix = "_5r500"  # appends to output SZ-M500 files (needs underscore)
    compare['Planck15'] = True
    compare['PlanckLBG'] = True
    ylims = [5e-8, 1e-3]
    z, tol = np.arange(0.0, 2.1, 0.2), 0.02
    outsuffix="_Planck"

#    Rlims=[0.0, 0.75]
#    Rlimtype="arcmin"
#    suffix = "_SPT"#"_SPT_fullbox_lowres" ## appends to output SZ-M500 files (needs underscore)
#    compare['SPT'] = True
#    ylims = [1e-8, 1e-4]
#    z, tol = np.arange(0.2, 2.1, 0.2), 0.02
#    outsuffix="_SPT"

#    Rlims = [0.0, 1.0]
#    Rlimtype = "R500"
#    suffix = "_r500"
    
    h_scale = 0.679 #0.7#value of h to scale output values and observation values to
    omega_m = 0.3065 #0.2793 #value of omega matter to scale output values and observation values to
    projected = False
    zthick = 0 ## set to 0 for whole box, otherwise units of R500 e.g. 10
    ## Note that for 0.75' aperture the results at z=0.2 are indistinguishable between whole box and 10*r500
    temp_thresh = -1
    filter_type = "none"
    M500min = 1e12
    
    col = ['#24439b', '#ED7600'] ## blue orange
    #col = ['#ACF0F2'] ## light blue
    mew=1.8
    ms=8
    
    for snapnum in snapnums:
        save_SZ_M500(basedir, simdirs, snapnum, M500min=M500min, Rlims=Rlims, Rlimtype=Rlimtype, projected=projected, zthick=zthick, temp_thresh=temp_thresh, filter_type=filter_type, suffix=suffix)
#        for i, zoomdir in enumerate(zoomdirs):
#            if zoomdir is not "":
#                save_SZ_M500(zoombasedir+zoomdir, zooms[i], snapnum, highres_only=True, M500min=M500min, Rlims=Rlims, Rlimtype=Rlimtype, projected=projected, zthick=zthick, outdir="/data/curie4/nh444/project1/SZ/"+zoomdir, suffix=suffix, temp_thresh=temp_thresh, filter_type=filter_type, mpc_units=True)
#    z, tol = [1.8], 0.02
#    for zi in z:
#        plot_SZ_M500(simdirs, zi, tol=tol, for_paper=True, title="", scat_idx=range(len(simdirs)), plot_5r500=True, med_idx=[], 
#                     M500min=1e13, ylims=ylims, compare=compare, zoomdirs=zoomdirs, zooms=zooms, projected=projected, outsuffix=outsuffix, insuffix=suffix, labels=labels, col=col, mew=mew, ms=ms, h_scale=h_scale, omega_m=omega_m, outdir=outdir)
    
#    plot_SZ_M500_multi(simdirs, np.arange(0.0, 0.9, 0.2), tol=tol, outsuffix=outsuffix, insuffix=suffix, M500min=1e13, ylims=ylims,
#                       do_fit=True, compare=compare, zoomdirs=zoomdirs, zooms=zooms, projected=projected,
#                       labels=labels, col=col, mew=mew, ms=ms, h_scale=h_scale, omega_m=omega_m, outdir=outdir)
    
#    plot_SZ_M500_redshift(simdirs, outsuffix=outsuffix, insuffix=suffix, M500min=1e13, ylims=ylims,
#                       zoomdirs=zoomdirs, zooms=zooms, projected=projected,
#                       compare=compare, h_scale=h_scale, omega_m=omega_m, outdir=outdir)

def plot_SZ_M500_redshift(simdirs, minz=0.2, maxz=2.1, zstep=0.2,
                          insuffix="", outsuffix="", param_kwargs={},
                          compare={'Planck15': False, 'PlanckLBG':False, 'SPT': False},
                          ylims=[None, None], M500min=1e13,
                          outdir="/data/curie4/nh444/project1/SZ/",
                          zoomdirs=None, zooms=[],
                          projected=False,
                          h_scale=0.679, omega_m=0.3065,
                          blkwht=False):
    """
    Plot the best-fitting relation for various redshifts (z) on the same figure
    as well as the best-fit parameters as a function of redshift
    Args:
        param_kwargs: keyword args to function plot_norm_slope_vs_z
    """
    
    import matplotlib.pyplot as plt
    import numpy as np
    
    assert len(simdirs)==1, "simdirs must have just one entry"
    
    z = np.arange(minz, maxz, zstep)
      
    fig, ax = plt.subplots(figsize=(7,7))
    if blkwht:
        fig.patch.set_facecolor('0.2')
        
    #from palettable.colorbrewer.sequential import YlGnBu_9_r as pallette
    #cmax = 0.6 ## increase to include lighter colours
    #colour_idx = np.linspace(cmax, 0, len(z))
    from palettable.colorbrewer.diverging import RdBu_11_r as pallette
    colour_idx = np.concatenate((np.linspace(0.3, 0.0, len(z)/2+1), np.linspace(1.0, 0.7, len(z)/2)))
    cmap=pallette.mpl_colormap
    col = cmap(colour_idx)
        
    slope = np.zeros_like(z)
    logC = np.zeros_like(z)
    scat = np.zeros_like(z)
    slope_err, logC_err, scat_err = [], [], []
    Nfit = np.zeros(len(z), dtype=int)
    for i, zi in enumerate(z):
        labels = ["$"+"{:.1f}".format(zi)+"$"]
        (slope[i], cur_slope_err, logC[i], cur_logC_err,
         scat[i], cur_scat_err, Nfit[i]) = plot_SZ_M500(simdirs, zi, axis=ax,
             title="", scat_idx=[], med_idx=[], plot_5r500=True,
             M500min=M500min, blkwht=blkwht, ylims=ylims,
             zoomdirs=zoomdirs, zooms=zooms, projected=projected,
             insuffix=insuffix, labels=labels, col=[col[i]], compare=compare,
             h_scale=h_scale, omega_m=omega_m, do_fit=True, plot_obs=False,
             fit_lw=3, fit_alpha=0.7)
        slope_err.append(cur_slope_err)
        logC_err.append(cur_logC_err)
        scat_err.append(cur_scat_err)
        
    ## Convert list of arrays to 2D arrays
    slope_err = np.array(slope_err).T
    logC_err = np.array(logC_err).T
    scat_err = np.array(scat_err).T
    
    ax.legend(loc='lower right', fontsize='small', frameon=False, borderaxespad=1, title="Redshift", ncol=2)
    if blkwht:
        fig.savefig(outdir+"SZ-M500_fits_vs_z"+outsuffix+".png", transparent=True, bbox_inches='tight')
        print "Saved to", outdir+"SZ-M500_fits_vs_z"+outsuffix+".png"
    else:
        fig.savefig(outdir+"SZ-M500_fits_vs_z"+outsuffix+".pdf", bbox_inches='tight')
        print "Saved to", outdir+"SZ-M500_fits_vs_z"+outsuffix+".pdf"
        
        
    ## save and plot best-fit parameters
    outfile = outdir+"fit_params_mass_SZ"+outsuffix+".txt"
    ## Concatenate arrays into 2D array for saving to file
    dat = np.c_[z, slope, slope_err[0,:], slope_err[1,:], logC, logC_err[0,:], logC_err[1,:], scat, scat_err[0,:], scat_err[1,:], Nfit]
    ## Add any extra error bars
    if np.shape(slope_err)[0] > 2:
        nextra = (np.shape(slope_err)[0] / 2)-1
        print "Got", nextra, "extra errors"
        for j in range(1,nextra+1):
            dat = np.column_stack((dat, slope_err[2*j,:], slope_err[2*j+1,:], logC_err[2*j,:], logC_err[2*j+1,:], scat_err[2*j,:], scat_err[2*j+1,:]))
    np.savetxt(outfile, dat, header="z, slope, slope_err_low, slope_err_high, logC, logC_err_low, logC_err_high, scat, scat_err_low, scat_err_high, num_fit")
    print "Parameters output to", outfile
    from scaling.plotting.plot_params import plot_slope_norm_vs_z
    plot_slope_norm_vs_z(outdir, ["mass", "SZ"], "Y-M"+outsuffix, insuffixes=[outsuffix], outdir=outdir, **param_kwargs)
    
#    ## Plot best-fit parameters as a function of redshift
#    col = '#24439b'
#    kwargs = {'c':col, 'marker':'o', 'mec':'none', 'ms':10, 'lw':3, 'capsize':0}
#    
#    ## Slope
#    fig, ax = plt.subplots(figsize=(7,7))
#    ax.errorbar(z, slope, yerr=slope_err, **kwargs)
#    ax.set_xlabel("Redshift")
#    ax.set_xlim(min(z)-0.2, max(z)+0.2)
#    ax.axhline(5.0/3.0, ls='dashed', c='0.4')
#    ax.set_ylabel(r"$\beta_{\mathrm{Y-M}}$")
#    fig.savefig(outdir+"slope_vs_z_Y-M"+outsuffix+".pdf", bbox_inches='tight')
#    print "Saved to", outdir+"slope_vs_z_Y-M"+outsuffix+".pdf"
#    
#    ## Normalisation
#    fig, ax = plt.subplots(figsize=(7,7))
#    ax.errorbar(z, logC, yerr=logC_err, **kwargs)
#    ax.set_xlabel("Redshift")
#    ax.set_xlim(min(z)-0.2, max(z)+0.2)
#    ax.set_ylabel(r"$\mathrm{log}_{10}(\mathrm{C}_{\mathrm{"+"Y-M"+"}}$)")
#    fig.savefig(outdir+"norm_vs_z_Y-M"+outsuffix+".pdf", bbox_inches='tight')
#    print "Saved to", outdir+"norm_vs_z_Y-M"+outsuffix+".pdf"

def plot_SZ_M500_multi(simdirs, z, tol=0.02, insuffix="", outsuffix="",
                 compare={'Planck15': False, 'PlanckLBG':False, 'SPT': False},
                 ylims=[None, None],
                 M500min=1e13,
                 outdir="/data/curie4/nh444/project1/SZ/",
                 zoomdirs=None, zooms=[],
                 projected=False,
                 h_scale=0.679, omega_m=0.3065,
                 labels=None,
                 col=None, ls=None, mew=1.2, ms=6.5,
                 do_fit=False,
                 blkwht=False):
    import matplotlib.pyplot as plt
    import matplotlib.gridspec as gridspec
    ncol = 2
   
    nrow = int(0.5 + float(len(z)) / ncol)
    print "ncol =", ncol, "nrow =", nrow
    fig = plt.figure(figsize=(ncol*6, nrow*6))
    if blkwht:
        fig.patch.set_facecolor('0.2')
    gs = gridspec.GridSpec(nrow, ncol, wspace=0.0, hspace=0.0)
    for i, zi in enumerate(z):
        ax = plt.subplot(gs[i])
        plot_SZ_M500(simdirs, zi, plotlabel="z = {:.1f}".format(zi), axis=ax, tol=tol, title="", outsuffix="_dyn", scat_idx=range(len(simdirs)), plot_5r500=True, med_idx=[],
                     M500min=M500min, blkwht=blkwht, ylims=ylims, compare=compare, zoomdirs=zoomdirs, zooms=zooms, projected=projected, insuffix=insuffix, labels=labels, col=col, mew=mew, ms=ms, h_scale=h_scale, omega_m=omega_m, do_fit=do_fit)
        ## Remove overlapping axis and tick labels:
        if i % ncol != 0: ## not in first column
            ax.set_yticklabels([]) ## remove all y tick labels
            ax.yaxis.label.set_visible(False) ## hide axes label
        elif i>=ncol: ## in first column but not in first row
            ax.get_yticklabels()[-3].set_visible(False) ## remove last tick label
        #elif i < (nrow-1)*ncol: ## in first column but not in the last row
            #ax.get_yticklabels()[1].set_visible(False)
        if i < (nrow-1)*ncol: ## not the last row
            ax.set_xticklabels([])
            ax.xaxis.label.set_visible(False) ## hide axes label
            
    if blkwht:
        fig.savefig(outdir+"SZ-M500_vs_z"+outsuffix+".png", transparent=True, bbox_inches='tight')
        print "Saved to", outdir+"SZ-M500_vs_z"+outsuffix+".png"
    else:
        fig.savefig(outdir+"SZ-M500_vs_z"+outsuffix+".pdf", bbox_inches='tight')
        print "Saved to", outdir+"SZ-M500_vs_z"+outsuffix+".pdf"
            

def plot_SZ_M500(simdirs, z, axis=None, tol=0.02,
                 indir="/data/curie4/nh444/project1/SZ/", insuffix="",
                 outsuffix="", hdf5=True,
                 zoomdirs=None, zooms=[],
                 compare={'Planck15': False, 'PlanckLBG':False, 'Wang':True, 'SPT': False},
                 ylims=[None, None], plotlabel=None,                
                 scat_idx=[], med_idx=[],
                 projected=False, plot_5r500=True,
                 M500min=1e13, M500max=3e15, logmass=False,
                 h_scale=0.679, omega_m=0.3065, omega_l=0.6935,
                 labels=None, title="", figsize=(7,7),
                 col=None, ls=None, mew=1.2, ms=6.5,
                 plot_obs=True, do_fit=False, fit_method="bces_orthog",
                 bootstrap=True, Ntrials=10000, fit_lw=4, fit_alpha=0.4,
                 outdir="/data/curie4/nh444/project1/SZ/", outfilename=None,
                 blkwht=False, for_paper=False):
    """
    Plot SZ flux versus M500. This can also be done with plot_relation.py
    Args:
        axis: pylot axes instance, if None we create a new figure
        plotlabel: text to add to axis
        compare: dictionary specifying which observational data to compare against
        plot_5r500: If comparing to Planck data, compare to observed flux within 5r500, otherwise r500
    """
    import numpy as np
    import matplotlib.pyplot as plt
    from scipy.stats import binned_statistic
    if do_fit:
        from scaling.fit_relation import fit_powerlaw
        from scaling.fit_relation import make_powerlaw
       
    if axis is None:
        fig, ax = plt.subplots(figsize=figsize)
        if not for_paper:
            ax.set_title(title+" z = {:.2f}".format(z))
    else:
        ax = axis
    
    if col is None:
        from palettable.colorbrewer.qualitative import Dark2_8 as pallette
        col=pallette.hex_colors
    if ls is None:
        ls = ['solid']*len(simdirs)
    if zoomdirs is None:
        zoomdirs = [""]*len(simdirs)
    lw=2.5
    
    deltalogM500 = 0.2
    if labels is None: labels=[""]*len(simdirs)
    
    ax.set_yscale('log')
    if ylims[0] is not None:
        ax.set_ylim(ymin=ylims[0])
    if ylims[1] is not None:
        ax.set_ylim(ymax=ylims[1])
    if logmass:
        ax.set_xlim(np.log10(M500min), np.log10(M500max))
        ax.set_xlabel(r"log$_{10}$(M$_{500}$ [M$_{\odot}$])")
    else:
        ax.set_xscale('log')
        ax.set_xlim(M500min, M500max)
        #ax.set_ylim(1e-8, 2e-3)
        ax.set_xlabel(r"M$_{500}$ [M$_{\odot}$]")
        
    if compare['PlanckLBG'] or compare['Planck15']:
        if plot_5r500:
            ax.set_ylabel(r"$\mathrm{E(z)^{-2/3} Y_{5r_{500}} D_A^2}$ [Mpc$^2$]")
        else:
            ax.set_ylabel(r"$\mathrm{E(z)^{-2/3} Y_{r_{500}} D_A^2}$ [Mpc$^2$]")
    elif compare['SPT']:
        ax.set_ylabel(r"$\mathrm{E(z)^{-2/3} Y^{0'.75} D_A^2}$ [Mpc$^2$]")
    Ezpow = -2.0/3.0
    
    ## Observations
    if plot_obs:
        plot_SZ_obs(ax, z, tol=tol, mew=mew, ms=ms, logmass=logmass,
                compare=compare, blkwht=blkwht, plot_5r500=plot_5r500,
                Ezpow=Ezpow, h_scale=h_scale, omega_m=omega_m)

    ## determine input filename
    if projected:
        insuffix += "_proj"
    infile = "SZ-M500_z"+"{:.2f}".format(z)+insuffix
    if hdf5:
        infile += ".hdf5"
    else:
        infile += ".txt"
    i = 0 ## counter for colours
    for simidx, sim in enumerate(simdirs):
        if hdf5:
            X, Y, M500, grps = load_SZ_hdf5(indir+sim+"/"+infile, z, Ezpow=Ezpow, omega_m=omega_m, omega_l=omega_l, h_scale=h_scale)
        else:
            X, Y, M500, grps = load_SZ_txt(indir+sim+"/"+infile, z, Ezpow=Ezpow, omega_m=omega_m, omega_l=omega_l, h_scale=h_scale)
        ind = np.where((M500 >= M500min) & (M500 < M500max))
        
        if simidx in scat_idx:
            if logmass:
                ax.errorbar(np.log10(M500[ind]), Y[ind], marker='D', mec=col[i], mfc='none', mew=mew, ms=ms, ls='none')
            else:
                ax.errorbar(M500[ind], Y[ind], marker='D', mec=col[i], mfc='none', mew=mew, ms=ms, ls='none')
               
        if zoomdirs[simidx] is not "":
            for zoom in zooms[simidx]:
                if hdf5:
                    Xz, Yz, M500z, grpsz = load_SZ_hdf5(indir+zoomdirs[simidx]+zoom+"/"+infile, z, Ezpow=Ezpow, omega_m=omega_m, omega_l=omega_l, h_scale=h_scale)
                else:
                    Xz, Yz, M500z, grpsz = load_SZ_txt(indir+zoomdirs[simidx]+zoom+"/"+infile, z, Ezpow=Ezpow, omega_m=omega_m, omega_l=omega_l, h_scale=h_scale)
                ind = np.where((M500z >= M500min) & (M500z < M500max))
                if simidx in scat_idx:
                    if logmass:
                        ax.errorbar(np.log10(M500z[ind]), Yz[ind], marker='D', mec=col[i], mfc=col[i], mew=mew, ms=ms, ls='none')
                    else:
                        ax.errorbar(M500z[ind], Yz[ind], marker='D', mec=col[i], mfc=col[i], mew=mew, ms=ms, ls='none')
                M500 = np.append(M500, M500z[ind])
                Y = np.append(Y, Yz[ind])
                    

        if simidx in med_idx:
            if logmass:
                bins = np.linspace(np.log10(M500min)-deltalogM500, np.log10(M500max), num=np.ceil((np.log10(M500max) - np.log10(M500min))/deltalogM500))
                x = (bins[1:]+bins[:-1])/2.0
                means, bin_edges, n = binned_statistic(np.log10(M500), Y, bins=bins, statistic='mean')
                num, bin_edges, n = binned_statistic(np.log10(M500), Y, bins=bins, statistic='count')
            else:
                bins = np.logspace(np.log10(M500min)-deltalogM500, np.log10(M500max), num=np.ceil((np.log10(M500max) - np.log10(M500min))/deltalogM500))
                x = 10**((np.log10(bins[:-1]) + np.log10(bins[1:]))/2.0)
                means, bin_edges, n = binned_statistic(M500, Y, bins=bins, statistic='mean')
                num, bin_edges, n = binned_statistic(M500, Y, bins=bins, statistic='count')
        
            mask = (num > 2)
            ax.plot(x[mask], means[mask], '-', c=col[i], lw=lw, label=labels[simidx], ls=ls[simidx], dash_capstyle='round')
            if len(simdirs) == 1 and not simidx in scat_idx:## 1-sigma contours
                if logmass:
                    binlabels = np.digitize(np.log10(M500), bins, right=False)
                else:
                    binlabels = np.digitize(M500, bins, right=False)
                err1 = np.asarray([np.percentile(Y[binlabels == k], 16.0) if len(Y[binlabels==k])>0 else np.nan for k in range(1, len(bins))])
                err2 = np.asarray([np.percentile(Y[binlabels == k], 84.0) if len(Y[binlabels==k])>0 else np.nan for k in range(1, len(bins))])
                ax.plot(x[mask], err1[mask], '--', c=col[i], lw=2)
                ax.plot(x[mask], err2[mask], '--', c=col[i], lw=2)

        if do_fit:
            ymin, ymax = ax.get_ylim()
            ind = np.where((M500 >= M500min) & (M500 <= M500max) & (Y > ymin) & (Y < ymax))[0]
            num = len(ind)
            X0 = 1e14
            inislope = 5.0/3.0
            inilogC = -5.5
            slope, slope_err, logC, logC_err, scat, scat_err = fit_powerlaw(M500[ind], Y[ind], X0, inislope, inilogC, bootstrap=bootstrap, method=fit_method, Ntrials=Ntrials)
            print "slope =", slope
            print "logC =", logC
            powerlaw = make_powerlaw(np.log10(X0))
            plt.plot([M500min, M500max], [10**powerlaw(np.log10(M500min), slope, logC), 10**powerlaw(np.log10(M500max), slope, logC)], c=col[i], lw=fit_lw, alpha=fit_alpha, label=labels[simidx])
                        
        i += 1

    handles, labels = ax.axes.get_legend_handles_labels()
    n = len(med_idx) ## currently labels are only added for median relations
    leg1 = ax.legend(handles[:n], labels[:n], loc='lower right', frameon=False,
                     borderaxespad=1)
    ax.add_artist(leg1)
    leg2 = ax.legend(handles[n:], labels[n:], loc='lower right',
                     frameon=False, borderaxespad=1, fontsize='small')
    if plotlabel is not None:
        plt.text(0.1, 0.9, plotlabel, horizontalalignment='left',
                 verticalalignment='top', transform=ax.transAxes,
                 fontsize='medium', color='white' if blkwht else 'k')

    if outfilename is None:
        outfilename = "SZ-M500_z"+"{:.2f}".format(z)+outsuffix+".pdf"

    if blkwht:
        ax.set_axis_bgcolor('0.2')
        ax.xaxis.label.set_color('white')
        ax.tick_params(axis='x', which='both', colors='white')
        ax.yaxis.label.set_color('white')
        ax.tick_params(axis='y', which='both', colors='white')
        for text in leg1.get_texts():
            text.set_color('white')
        for text in leg2.get_texts():
            text.set_color('white')
        import matplotlib.spines
        for child in ax.get_children():
            if isinstance(child, matplotlib.spines.Spine):
                child.set_color('white')
        if axis is None:
            fig.patch.set_facecolor('0.2')
            plt.savefig(outdir+outfilename.replace(".pdf", ".png"),
                        bbox_inches='tight', transparent=True)
    elif axis is None:
        plt.savefig(outdir+outfilename, bbox_inches='tight')
        print "Plot saved to", indir+outfilename

    if do_fit:
        return slope, slope_err, logC, logC_err, scat, scat_err, num


def load_SZ_hdf5(infile, h_scale=0.679):
    """Load the SZ data in hdf5 format output by save_SZ_M500."""
    assert infile[-5:] == ".hdf5", "file must be .hdf5"
    import h5py
    from copy import deepcopy

    with h5py.File(infile) as f:
        if 'M500' not in f.keys():
            raise IOError(str(infile) + " is empty")
        X = M500 = f['M500'].value[:, 0] / h_scale
        grps = f['groups'].value[:, 0]
        Y = f['Y'].value[:, 0] / h_scale
        metadata = deepcopy(dict(f.attrs))

    return X, Y, M500, grps, metadata


def load_SZ_txt(infile, h_scale=0.679):
    import numpy as np

    data = np.loadtxt(infile)
    try:
        X = data[:, 1] / h_scale  # M500
        Y = data[:, 2] / h_scale
        M500 = data[:, 1] / h_scale
        grps = data[:, 0]
    except IndexError:
        X = data[1] / h_scale  # M500
        Y = data[2] / h_scale
        M500 = data[1] / h_scale
        grps = data[0]

    return X, Y, M500, grps


def plot_SZ_obs(ax, z, tol=0.02, mew=1.2, ms=8, lw=3, logmass=False,
                compare={}, bestfit=False,
                blkwht=False, plot_5r500=True,
                h_scale=0.6774, omega_m=0.3089):
    """Plot SZ flux-M500 observational data onto axes 'ax'.

    Plot Y(r500) (or Y(5r500) if plot_5r500) scaled by E(z)^-2/3 and
    the angular diameter distance versus M500 for the observational
    data given in 'compare' for clusters at redshift 'z' +/- tol.
    Args:
        ax: pyplot.axes object to plot to.
        z: redshift used to select data.
        tol: tolerance about redshift 'z'.
        mew: marker edge width.
        ms: marker size in pts.
        lw: thickness of best-fit lines.
        logmass: use log10(M500) rather than M500.
                 This is not yet implemented for some datasets.
        compare: dictionary of booleans defining which datasets to plot.
                 See compare_default below for options.
        bestfit: (bool) Plot best-fitting relation to the data if available.
        blkwht: use colours that work better for a dark background.
        plot_5r500: plot SZ flux within 5r500 rather than r500.
        h_scale: value of little h to scale data to.
        omega_m: value of omega_m used in calculating E(z).
    """
    import numpy as np
    from scipy.stats import binned_statistic
    from sim_python import cosmo
    cm = cosmo.cosmology(h_scale, omega_m)

    obs_dir = "/home/nh444/vault/ObsData/SZ/"
    # define power of (h_obs / h_scale) to multiply YSZ value by
    hpow = 2.0  # YSZ has no h-dependence but DA^2 has h^-2 dependence

    # power of E(z) to multiply the SZ flux. Currently values
    # other than the default (-3/2) are not currently supported as some
    # datasets have already scaled Y500 by E(z)^(-2/3), which
    # needs to be corrected for before applying E(z)**Epow.
    Ezpow = -2.0/3.0  # do not change.

    if blkwht:
        c1 = '0.5'
        c2 = '0.7'
    else:
        c1 = '0.6'
        c2 = '0.3'
    errlw = 2.0  # thickness of errorbars
    mew = 2.0  # marker edge width
    cap = ms*0.5  # cap size of error bars

    # default values for comparison dictionary
    compare_default = {'SPT': False,
                       'Planck15': False,
                       'Andersson11': False,
                       'Marrone12': False,
                       'Planck14': False,
                       'PlanckLBG': False,
                       'Wang': False,
                       'Lim18': False,
                       'Nagarajan18': False}
    # first make a copy of the given dictionary
    # so that we don't modify the original
    import copy
    my_compare = copy.deepcopy(compare)
    # fill any missing keys in comparison dictionary with the defaults
    for key in compare_default:
        if key not in my_compare:
            my_compare[key] = compare_default[key]

    # define initial redshift limits for obs, which will then be iterated over
    # until a sample with the desired redshift is found
    minz = max(0.0, z-0.2)
    maxz = z+0.2
    if z < 0.05:
        # Increase tolerance on median for very low redshift
        tol = 0.2
    # need to set zorder for ax.errorbar so that error bars overlap properly
    zorder = 3
    if my_compare['SPT']:
        # ----- Updated Bocquet et al. (2018) catalogue ----- #
        SPT = np.genfromtxt(obs_dir+"SPT/SPT-SZ_WL.csv", delimiter=",")
        # Mass estimates marginalised over cosmology.
        # The cosmology has h=0.645 but the masses are quoted for h=0.7
        M = SPT[:, 14]*1e14*(0.7/h_scale)
        # ----- Original Bleem et al. (2015) catalogue ----- #
#        SPT = np.genfromtxt(obs_dir+"SPT/SPT-SZ.csv", delimiter=",")
#        # fiducial cosmology (h=0.7 omega_m=0.3)
#        M = SPT[:, 12]*(0.7/h_scale)
#        # Planck cosmology (h=0.67 omega_m=0.32)
#        # M = SPT[:, 14]*(0.67/h_scale)
#        # dynamical masses for cosmology with h=0.734 omega_m=?
#        # M = SPT[:, 16]*(0.734/h_scale)
        SPT_z = SPT[:, 8]
        SPT_Y = SPT[:, 6]
        # Choose redshift sample and those with measured masses:
        ind = np.where((SPT_z >= minz) & (SPT_z < maxz)
                       & (M > 0.0))[0]
        ind2 = get_median_sample(SPT_z[ind], z, tol=tol, verbose=False)
        ind = ind[ind2]
        print len(ind), "in SPT sample at z={:.1f}".format(z)
        if len(ind) > 0:
            label = (r"SPT-SZ $z_{\mathrm{med}} =" +
                     "{:.2f}".format(np.median(SPT_z[ind])) +
                     "$\n($"+"{:.2f}".format(np.min(SPT_z[ind])) +
                     "< z <"+"{:.2f}".format(np.max(SPT_z[ind]))+"$)")
            # Convert arcmin^2 to Ez^-2/3 Mpc^2 using fiducial cosmology
#            fac = (cm.E_z(SPT_z[ind])**Ezpow * (cm.da(SPT_z[ind]) / h_scale /
#                   60.0 * (np.pi / 180.0))**2.0)
            fac = (cm.E_z(SPT_z[ind])**Ezpow *
                   (cm.arcsec_to_phys(60.0, SPT_z[ind]) / h_scale)**2)
            # Plot median relation for bins with more than 10 counts:
            Mmin, Mmax = 13.85, 15.25
            bins = np.linspace(Mmin, Mmax, num=int((Mmax-Mmin)/0.2)+1)
            x = 10**((bins[1:]+bins[:-1])/2)
            count, bin_edges, n = binned_statistic(
                    np.log10(M[ind]), [], bins=bins, statistic='count')
            med, bin_edges, n = binned_statistic(
                    np.log10(M[ind]), SPT_Y[ind]*fac, bins=bins,
                    statistic='median')
            err_low, bin_edges, n = binned_statistic(
                    np.log10(M[ind]), SPT_Y[ind]*fac, bins=bins,
                    statistic=lambda x: np.median(x) - np.percentile(x, 16.0))
            err_high, bin_edges, n = binned_statistic(
                    np.log10(M[ind]), SPT_Y[ind]*fac, bins=bins,
                    statistic=lambda x: np.percentile(x, 84.0) - np.median(x))
            X = 10
            mask = np.where(np.isfinite(med) & (count >= X))[0]
            if len(mask) > 0:
                ax.errorbar(x[mask], med[mask],
                            yerr=[err_low[mask], err_high[mask]],
                            marker='o', c=c2, mec=c2, mfc='none',
                            ms=ms*1.4, mew=mew, lw=errlw, ls='none',
                            zorder=zorder, label=label)
                label = None
            # Plot points for bins with fewer than 10 counts.
            kwargs = {'marker': 'o', 'mec': c2, 'mfc': 'none', 'ms': 6,
                      'ls': 'none'}
            for i, curbin in enumerate(bin_edges[:-1]):
                if count[i] < X:
                    # Get indices of clusters in this bin and plot them.
                    ind2 = np.where((np.log10(M[ind]) >= bin_edges[i]) &
                                    (np.log10(M[ind]) < bin_edges[i+1]))[0]
                    if len(ind2) > 0:
                        ax.errorbar(M[ind[ind2]], SPT_Y[ind[ind2]]*fac[ind2],
                                    label=label, zorder=zorder, **kwargs)
                        label = None
            # Plot clusters not in any bin.
            ind2 = np.where((np.log10(M[ind]) < bin_edges[0]) |
                            (np.log10(M[ind]) > bin_edges[-1]))[0]
            if len(ind2) > 0:
                ax.errorbar(M[ind[ind2]], SPT_Y[ind[ind2]]*fac[ind2],
                            zorder=zorder, **kwargs)
        zorder += 1
    if my_compare['Planck15']:
        # Load Planck catalogue
        from astropy.io import fits
        hdulist = fits.open("/data/vault/nh444/ObsData/SZ/Planck/HFI_PCCS_SZ-union_R2.08.fits")
        t = hdulist[1].data
        bias = 1.0 ## bias in the Planck masses, which are derived from the
        ## baseline Y500-M500 relation calculated in Planck XX (2014) (eq 7 and Appendix A)
        M = t['MSZ'] / bias * 1e14 * 0.7/h_scale
        ## Filter to given redshift range and those not heavily IR contaminated (IR_FLAG=0),
        ## with an M500 estimate (MSZ>0) and recommended neural network quality (Q_NEURAL>0.4)
        #minz = 0.0
        #maxz = 0.25
        ind = np.where((t['REDSHIFT'] >= minz) & (t['REDSHIFT'] <= maxz) & #(t['SNR'] > 6) &
                        (t['IR_FLAG']==0) & (t['MSZ'] > 0.0) & (t['Q_NEURAL'] > 0.4))[0]
        ind2 = get_median_sample(t['REDSHIFT'][ind], z, tol=tol, verbose=False)
        ind = ind[ind2]
        print len(ind), "in Planck sample at z={:.1f}".format(z)
        if len(ind) > 0:
            label = "Planck 2015 $z_{\mathrm{med}} ="+"{:.2f}".format(np.median(t['REDSHIFT'][ind]))+"$\n($"+"{:.2f}".format(np.min(t['REDSHIFT'][ind]))+"< z <"+"{:.2f}".format(np.max(t['REDSHIFT'][ind]))+"$)"
            ## factor to convert arcmin^2 to Ez^-2/3 Mpc^2 using fiducial cosmology
            fac = cm.E_z(t['REDSHIFT'][ind])**Ezpow*(cm.da(t['REDSHIFT'][ind]) / h_scale / 60.0 * (np.pi / 180.0))**2.0
            #ax.errorbar(M[ind], t['Y5R500'][ind] * 1e-3 * fac, marker='d', alpha=0.5, c=c1, mec='none', ms=7, ls='none', label="Planck")
            ### Plot median relation:
            Mmin, Mmax = 13.85, 15.25
            #Mmin, Mmax = 13.9, 15.9
            bins = np.linspace(Mmin, Mmax, num=int((Mmax-Mmin)/0.2)+1)
            x = 10**((bins[1:]+bins[:-1])/2)
            count, bin_edges, buf = binned_statistic(np.log10(M[ind]), [], bins=bins, statistic='count')
            med, bin_edges, buf = binned_statistic(np.log10(M[ind]), t['Y5R500'][ind] * 1e-3 * fac, bins=bins, statistic='median')
            err_low, bin_edges, buf = binned_statistic(np.log10(M[ind]), t['Y5R500'][ind] * 1e-3 * fac, bins=bins, statistic=lambda x: np.median(x) - np.percentile(x, 16.0))
            err_high, bin_edges, buf = binned_statistic(np.log10(M[ind]), t['Y5R500'][ind] * 1e-3 * fac, bins=bins, statistic=lambda x: np.percentile(x, 84.0) - np.median(x))
            X = 10
            mask = np.isfinite(med) & (count >= X)
            if sum(mask) > 0:
                ax.errorbar(x[mask], med[mask],
                            yerr=[err_low[mask], err_high[mask]],
                            marker='o', c=c2, mec=c2, mfc='none',
                            ms=ms*1.4, mew=mew, lw=errlw, ls='none',
                            zorder=zorder, label=label)
                label = None
            ## Plot points for bins with fewer than X counts
            kwargs = {'marker':'o', 'mec':c2, 'mfc':'none', 'ms':6, 'ls':'none'}
            for i, curbin in enumerate(bin_edges[:-1]):
                if count[i] < X:
                    ## Get indices of clusters in this bin and plot them
                    ind2 = np.where((np.log10(M[ind]) >= bin_edges[i]) & (np.log10(M[ind]) < bin_edges[i+1]))[0]
                    if len(ind2) > 0:
                        ax.errorbar(M[ind[ind2]],
                                    t['Y5R500'][ind[ind2]] * 1e-3 * fac[ind2],
                                    label=label, zorder=zorder, **kwargs)
                        label = None
            ## Plot clusters not in any bin
            ind2 = np.where((np.log10(M[ind]) < bin_edges[0]) | (np.log10(M[ind]) > bin_edges[-1]))[0]
            if len(ind2) > 0:
                ax.errorbar(M[ind[ind2]],
                            t['Y5R500'][ind[ind2]] * 1e-3 * fac[ind2],
                            label=label, zorder=zorder, **kwargs)
        zorder += 1
#==============================================================================
#         if z<0.1:
#             dat = np.genfromtxt(obs_dir+"Planck_2015_from_Barnes16_z0.csv", delimiter=",") ## MACSIS paper
#             ax.errorbar(dat[:,0]*(0.6777/h_scale), dat[:,1]*(0.6777/h_scale), marker='D', c=c1, mec='none', ms=9, ls='none', label="Planck+15 (MACSIS)")
#             #ax.errorbar(dat[:,0]*(0.6777/h_scale) / 0.7*(0.6777/h_scale), dat[:,1], marker='o', mfc='none', mec=c1, ms=9, ls='none', label="Planck+ 15 (1-b)=0.7")
#             #ax.quiver(dat[:,0]*(0.6777/h_scale), dat[:,1]*(0.6777/h_scale), (dat[:,0] / 0.7) - dat[:,0], np.zeros_like(dat[:,1]), units='width')
#             
#             dat = np.genfromtxt(obs_dir+"Planck_2015_from_Barnes17.csv", delimiter=",") ## C-EAGLE paper
#             ax.errorbar(dat[:,0]*(0.6777/h_scale), dat[:,1]*(0.6777/h_scale), marker='s', c=c1, mec='none', ms=9, ls='none', label="Planck+15 (C-EAGLE)")
#             print np.log10(dat[:,0]*(0.6777/h_scale))
#             
#             dat = np.genfromtxt(obs_dir+"Planck_2015_from_McCarthy17.csv", delimiter=",")
#             ax.errorbar(10**dat[:,0]*(0.7/h_scale), 10**dat[:,1]*(0.7/h_scale)**2, marker='o', c=c2, mec='none', ms=9, ls='none', label="Planck+15 (BAHAMAS)")
#         elif z>= 0.8:
#             dat = np.genfromtxt(obs_dir+"Planck_2015_from_Barnes16_z1.csv", delimiter=",")
#             ax.errorbar(dat[:,0]*(0.6777/h_scale), dat[:,1]*(0.6777/h_scale), marker='o', mec=c1, mfc='none', ms=9, ls='none', label="Planck+ 15")
#==============================================================================
    if my_compare['Andersson11'] and not logmass:
        from scipy.constants import m_p  # in kg
        from scipy.constants import m_e  # in kg
        from scipy.constants import c
        from scipy.constants import e
        from scipy.constants import parsec as pc  # in m
        thomson = 6.6524587158e-29  # thomson cross-section m^2
        mu_e = 1.165  # mean molecular weight per free electron.
        Msun = 1.9884430e30  # solar mass in kg
        # get factor to convert Msun keV to Mpc^2
        fac = Msun * 1.0e3 * e / (mu_e * m_p * m_e * c**2 / thomson) / (1.0e6 * pc)**2

        And = np.genfromtxt(obs_dir+"Andersson_2011.csv", delimiter=",")
        ax.errorbar(And[:, 3] * 1e14 * 0.702 / h_scale,
                    And[:, 9] * 1e14 * (0.702 / h_scale)**hpow * fac * cm.E_z(And[:, 1])**Ezpow,
                    xerr=And[:, 4] * 1e14 * 0.702 / h_scale,
                    yerr=And[:, 10] * 1e14 * (0.702 / h_scale)**hpow * fac * cm.E_z(And[:, 1])**Ezpow,
                    marker='o', c=c1, mfc=c1, mec='none', ms=ms*1.2,
                    mew=mew, lw=errlw, ls='none',
                    zorder=zorder, label="Andersson+ 2011")

        if bestfit:
            def Anders_bestfit(M):
                # return Y500 E(z)^-2/3 DA^2 in Mpc^2
                A = 14.06
                B = 1.67
                return fac * 10**A * (0.702 / h_scale)**hpow * (np.asarray(M) * h_scale / 0.702 / 3e14)**B
            xmin, xmax = ax.get_xlim()
            ax.errorbar([xmin, xmax], Anders_bestfit([xmin, xmax]), c=c1, lw=lw,
                        dashes=(9, 7), zorder=zorder, label="Andersson+ 2011")

        zorder += 1

    if my_compare['Marrone12'] and not logmass:
        Marr = np.genfromtxt(obs_dir+"Marrone_12.csv", delimiter=",")
        ax.errorbar(Marr[:, 19] * 1e14 * (0.73 / h_scale),
                    Marr[:, 22] * 1e-5 * (0.73 / h_scale)**hpow * cm.E_z(Marr[:, 1])**Ezpow,
                    xerr=[np.abs(Marr[:, 21] * 1e14 * (0.73 / h_scale)),
                          Marr[:, 20] * 1e14 * (0.73 / h_scale)],
                    yerr=[np.abs(Marr[:, 24] * 1e-5 * (0.73 / h_scale)**hpow) * cm.E_z(Marr[:,1])**Ezpow,
                          Marr[:, 23] * 1e-5 * (0.73 / h_scale)**hpow * cm.E_z(Marr[:,1])**Ezpow],
                    marker='o', c=c2, mec=c2, mfc='none', ms=ms*1.4,
                    mew=mew, lw=errlw, ls='none', zorder=zorder,
                    label="Marrone+ 2012")
        if bestfit:
            def Marrone_bestfit(Y):
                # return M500 given Y = Y500 * DA^2 * E(z)^-2/3 in Mpc^2

                A = 0.367
                B = 0.44
                return 1e14 * (0.73 / h_scale)**hpow * 10**A * (np.asarray(Y) * (h_scale / 0.73) / 1e-5)**B
            ymin, ymax = ax.get_ylim()
            ax.errorbar(Marrone_bestfit([ymin, ymax]), [ymin, ymax], c=c2, lw=lw,
                        dashes=(1, 4), dash_capstyle='round', ls='dashed',
                        zorder=zorder, label="Marrone+ 2012")

        zorder += 1

    if my_compare['Planck14'] and not logmass:
        def Planck_bestfit(M):
            # given M500 in Msun return Y500 * E(z)^-2/3 * DA^2 in Mpc^2
            b = 0.0
            return (1e-4 * (0.7 / h_scale)**hpow * 10**-0.19 *
                    (np.asarray(M) * (h_scale / 0.7) * (1.0 - b) / 6e14)**1.79)
        xmin, xmax = ax.get_xlim()
        ax.errorbar([xmin, xmax], Planck_bestfit([xmin, xmax]),
                    c=c1, lw=lw, # dashes=(1, 4), dash_capstyle='round',
                    ls='dashed', zorder=zorder, label="Planck 2014")

        zorder += 1

    if my_compare['PlanckLBG'] and z<0.2:
        if plot_5r500:
            fac= 1.796 ## conversion factor from tabulated fluxes to flux within 5r500
        else:
            fac = 1.0
        # factor to convert arcmin^2 at DA=500 Mpc to Mpc^2
        fac *= (500.0/(180.0/np.pi*60.0))**2.0
        LBGs = np.genfromtxt(obs_dir+"planck_SZ_M500_LBGs.txt")
        if logmass:
            ax.errorbar(LBGs[:,0]+np.log10(0.704/h_scale),
                        LBGs[:,3]*fac*1e-6*(0.704/h_scale)**hpow,
                        xerr=[LBGs[:,1]+np.log10(0.704/h_scale), LBGs[:,2]+np.log10(0.704/h_scale)],
                        yerr=LBGs[:,5]*fac*1e-6*(0.704/h_scale)**hpow, ## bootstrap errors
                        c=c1, mfc=c1, mec='none', marker='o', ls='none',
                        ms=ms*1.4, mew=mew, lw=errlw, zorder=zorder,
                        label="Planck XI 2013 (LBGs)")
        else:
            ### statistical errors (just error bars, no symbols)
            #ax.errorbar(10**LBGs[:,6]*(0.704/h_scale), LBGs[:,3]*fac*1e-6*(0.704/h_scale)**hpow,
                        #yerr=LBGs[:,4]*fac*1e-6*(0.704/h_scale)**hpow, capsize=cap, c=c1, ls='none')
            ## bootstrap errors
            ax.errorbar(10**LBGs[:,6]*(0.704/h_scale),
                        LBGs[:,3]*fac*1e-6*(0.704/h_scale)**hpow,
                        yerr=LBGs[:,5]*fac*1e-6*(0.704/h_scale)**hpow, ## bootstrap errors
                        capsize=cap, c=c1, mfc=c1, mec=c1, marker='o', ls='none',
                        ms=ms*1.4, mew=mew, lw=errlw, zorder=zorder,
                        label="Planck XI 2013 (LBGs)")
            ## Planck 2015 from CEAGLE - these are X-RAY MASSES so mass bias
            ## I expect the units are Mpc^2 so I scale to 500 Mpc^2 for units in arcmin^2
            #dat = np.loadtxt("/data/vault/nh444/ObsData/SZ/Planck_2015_from_Barnes17.csv", delimiter=",")
            #ax.errorbar(dat[:,0], dat[:,1] / (500.0/(180.0/np.pi*60.0))**2.0, marker='D', mfc='k', ms=6, ls='none')
        
        #MCXC = np.genfromtxt(obs_dir+"planck_SZ_M500_MCXC.txt")
        #ax.errorbar(MCXC[:,0]+np.log10(0.704/h_scale),
                     #MCXC[:,1]+np.log10(fac*(0.704/h_scale)**hpow), yerr=[MCXC[:,2]+np.log10(fac*(0.704/h_scale)**hpow), MCXC[:,3]+np.log10(fac*(0.704/h_scale)**hpow)], c='g', mec='g', mfc='none', marker='D', ls='none', ms=5, mew=mew, lw=1.5, label="Planck 2013 MCXC X-ray subsample")

        if bestfit:
            # Best-fit for planck data
            planck_bestfit = lambda x: 0.73e-3 * (x / (3.0e14 * 0.704 / h_scale))**(5.0/3.0)
            xmin, xmax = ax.get_xlim()
            if xmin < 10:  # if zero
                xmin = 1e13
            if logmass:
                ax.errorbar([xmin, xmax],
                            [planck_bestfit(10**xmin)*fac*(0.704/h_scale)**hpow,
                             planck_bestfit(10**xmax)*fac*(h_scale/0.704)],
                            c=c1, lw=lw, dashes=(9, 7), dash_capstyle='round',
                            ls='dashed', zorder=zorder)
            else:
                ax.errorbar([xmin, xmax],
                            [planck_bestfit(xmin)*fac*(0.704/h_scale)**hpow,
                             planck_bestfit(xmax)*fac*(h_scale/0.704)],
                            c=c1, lw=lw, dashes=(9, 7), dash_capstyle='round',
                            ls='dashed', zorder=zorder)

        zorder += 1

    if my_compare['Wang'] and z < 0.2 and not logmass:
        # Wang+2016 (weak lensing calibrated masses)

        LBGs = np.genfromtxt(obs_dir+"planck_SZ_M500_LBGs.txt")
        # conversion factor from tabulated fluxes to flux within 5r500
        if plot_5r500:
            fac = 1.796
        else:
            fac = 1.0
        # factor to convert arcmin^2 at DA=500 Mpc to Mpc^2
        fac *= (500.0/(180.0/np.pi*60.0))**2.0
        # Note that I assume h=0.704 for the Wang data even though they
        # use h=0.673 because (according to email from Wenting Wang) they
        # simply apply a correction factor to the halo masses derived by
        # Planck XI 2013 and so the halo masses are in the same units
        # as Planck XI, which used h=0.704
        # bootstrap errors
        ax.errorbar(10**LBGs[:, 0]*(0.704/h_scale),
                    LBGs[:, 3]*fac*1e-6*(0.704/h_scale)**hpow,
                    xerr=[(10**(LBGs[:, 0])-10**(LBGs[:, 0]-LBGs[:, 1])) *
                          (0.704/h_scale),
                          (10**(LBGs[:, 0]+LBGs[:, 2])-10**(LBGs[:, 0])) *
                          (0.704/h_scale)],
                    yerr=LBGs[:, 5]*fac*1e-6*(0.704/h_scale)**hpow,
                    capsize=cap, c=c2, mec='none', mfc=c2, marker='s',
                    ms=ms, mew=mew, lw=errlw, ls='none', zorder=zorder,
                    label="Wang+ 2016")
        if bestfit:
            def Wang_bestfit(x):
                return 2.31e-5 * (x / (10**13.5 * 0.704/h_scale))**1.61
            xmin, xmax = ax.get_xlim()
            ax.errorbar([xmin, xmax],
                        [Wang_bestfit(xmin)*fac*(0.704/h_scale)**hpow,
                         Wang_bestfit(xmax)*fac*(0.704/h_scale)**hpow],
                        c=c2, lw=lw, dashes=(1, 4), dash_capstyle='round',
                        ls='dashed', zorder=zorder)  # label="Wang+ 2016")

        zorder += 1

    if my_compare['Lim18'] and not logmass:
        Lim = np.genfromtxt(obs_dir+"Lim_2018.csv", delimiter=",")
        # SZ flux is Y500 * E(z)^(-2/3) * (DA / 500 Mpc)^2
        ax.errorbar(10**Lim[:, 0] * (0.704 / h_scale),
                    Lim[:, 1] * 1e-6 * (500.0/(180.0/np.pi*60.0))**2.0,
                    yerr=Lim[:, 2] * 1e-6 * (500.0/(180.0/np.pi*60.0))**2.0,
                    marker='^', c=c1, mec=c1, mfc='none', ms=ms*1.4,
                    mew=mew, lw=errlw, ls='none',
                    zorder=zorder, label="Lim+ 2018")

        zorder += 1

    if my_compare['Nagarajan18'] and not logmass:
        Nag = np.genfromtxt(obs_dir+"Nagarajan_18.csv", delimiter=",")
        uplims = (Nag[:, 11] > 0.5)
        # upper limits are plotted at y + 2-sigma
        Nag[uplims, 2] += 2.0 * Nag[uplims, 3]
        # set uplim arrows to same size
        Nag[uplims, 3] = 0.3 * Nag[uplims, 2]
        # upper limits in mass
        # (where mass errorbar is larger than the mass)
        xuplims = ((Nag[:, 5] + Nag[:, 7]) < 0)
        Nag[xuplims, 7] = 0.3 * Nag[xuplims, 5]
        ax.errorbar(Nag[:, 5] * 1e14 * 0.7 / h_scale,
                    Nag[:, 2] * 1e-5 * (0.7 / h_scale)**hpow *
                    cm.E_z(Nag[:, 1])**Ezpow,
                    xerr=[np.abs(Nag[:, 7] * 1e14 * 0.7 / h_scale),
                          Nag[:, 6] * 1e14 * 0.7 / h_scale],
                    yerr=Nag[:, 3] * 1e-5 * (0.7 / h_scale)**hpow *
                    cm.E_z(Nag[:, 1])**Ezpow,
                    marker='D', c=c2, mec=c2, mfc='none', ms=ms,
                    mew=mew, lw=errlw, ls='none',
                    uplims=uplims, xuplims=xuplims,
                    zorder=zorder, label="Nagarajan+ 2018")

        if bestfit:
            def Naga_bestfit(M):
                # return Y500 E(z)^-2/3 DA^2 in Mpc^2
                A = 0.97
                B = 1.67
                return (A * 7.93e-5 * (0.7 / h_scale)**hpow *
                        (np.asarray(M) * h_scale / 0.7 / 7.084e14)**B)
            xmin, xmax = ax.get_xlim()
            ax.errorbar([xmin, xmax], Naga_bestfit([xmin, xmax]), c=c2, lw=lw,
                        dashes=(9, 7), zorder=zorder, label="Nagarajan+ 2018")

        zorder += 1


def get_median_sample(redshifts, target_z, tol=0.02,
                      maxwidth=0.2, verbose=False):
    """
    Given a list of redshifts, return the indices for a sample with
    a median redshift in the range 'target_z' +/- 'tol'
    Arguments:
        redshift: Sequence of redshifts.
        target_z: Desired median redshift.
        tol: +/- tolerance on the median redshift.
        maxwidth: If not None, restrict the width of the redshift
                  distribution to be less than or equal to this value.
    """
    import numpy as np

    if maxwidth is None:
        maxwidth = np.inf
    z = np.sort(redshifts)  # sorted ascending
    ind = range(len(z))
    for i in range(1000):  # iterate
        med = np.median(z[ind])
        if verbose:
            print "med =", med, len(ind)
        if med > target_z+tol:  # remove largest redshift
            new_ind = ind[:-1]
        elif med < target_z-tol:  # remove smallest redshift
            new_ind = ind[1:]
        elif (np.max(z[ind]) - np.min(z[ind])) > maxwidth:
            # The redshift range is too large.
            # Remove the smallest and largest redshift
            new_ind = ind[1:-1]
        else:  # we are close enough to the target redshift
            return np.argsort(redshifts)[ind]
        # If the next iteration would bring us further from the target, stop.
        if abs(np.median(z[new_ind]) - target_z) > abs(med - target_z):
            return np.argsort(redshifts)[ind]
        else:
            ind = new_ind

    print "Ran out of iterations"
    return np.argsort(redshifts)[ind]


def save_SZ_M500(basedir, simdirs, snapnum, Rlims=[0.0, 5.0], Rlimtype="R500",
                 projected=False, zthick=10.0,
                 highres_only=False, main_halo_only=False,
                 temp_thresh=-1, filter_type="Rasia",
                 outdir="/data/curie4/nh444/project1/SZ/", suffix="",
                 hdf5=True,
                 M500min=1e12, RminLowRes=5.0,
                 mpc_units=False):
    """
    Calculate and save SZ flux within a given aperture
    Args:
        Rlims: inner and outer radius of the aperture in R500
        projected: calculate SZ flux within a cylinder of length zthick (in r500), otherwise a sphere
        hdf5: save in hdf5 format, othewise txt
        M500min: minimum M500 (Msun/h) to compute Y500 for
        RminLowRes: minimum contamination radius by low-res particles in R500

    """
    import numpy as np
    import snap
    import os
    from sim_python import cosmo
    if hdf5:
        import h5py

    from scipy.constants import m_p as mproton  # in kg
    from scipy.constants import m_e as melectron  # in kg
    from scipy.constants import parsec as pc  # in m
    mproton_in_g = mproton * 1000.0
    melectron_in_g = melectron * 1000.0
    Eelectron_in_kev = 510.99891  # rest mass energy of electron in keV
    thomson = 6.6524587158e-29  # thomson cross-section m^2
    UnitMass_in_g = 10.0**10 * 1.9884430e33
    Xh = 0.76  # hydrogen mass fraction

    Rlims = np.array(Rlims)

    if projected:
        suffix += "_proj"

    if hdf5:
        # definite attributes for output hdf5 file
        attrs = {'Rlims': Rlims,
                 'Rlimtype': Rlimtype,
                 'projected': projected,
                 'zthick': zthick,
                 'highres_only': highres_only,
                 'main_halo_only': main_halo_only,
                 'temp_thresh': temp_thresh,
                 'filter_type': filter_type,
                 'M500min': M500min,
                 'mpc_units': mpc_units,
                 }

    for simdir in simdirs:
        print "\nStarting simdir", simdir, "snapshot", snapnum
        s = snap.snapshot(basedir+simdir+"/", snapnum, mpc_units=mpc_units)
        z = s.header.redshift
        attrs['hubble'] = s.header.hubble
        attrs['redshift'] = s.header.redshift
        attrs['omega_m'] = s.header.omega_m
        attrs['omega_l'] = s.header.omega_l

        # Get groups above lower mass limit
        groups = np.where(s.cat.group_m_crit500*1.0e10 > M500min)[0]
        print len(groups), "halos found above M500", M500min, "Msun/h"
        # ignore groups with low res contamination
        if RminLowRes > 0 and s.header.nall[2] > 0:
            # Calculate distance of Type 2 particles
            s.load_posarr(parttypes=[2])
            new_groups = []
            for group in groups:
                # get distance of Type 2 particles relative to group
                r = s.nearest_dist_3D(s.posarr['type2'],
                                      s.cat.group_pos[group])
                # add group if nearest Type 2 is more than RminLowRes*R500 away
                if np.min(r) > RminLowRes * s.cat.group_r_crit500[group]:
                    new_groups.append(group)
            # Make sure group 0 is in the list regardless of contamination
            if 0 not in new_groups:
                new_groups.append(0)
            groups = new_groups
        Ntot = len(groups)
        print Ntot, "of these are uncontaminated within {:.1f} R500".format(RminLowRes)
        if Ntot == 0:
            print "Skipping."
            continue

        print "Loading masses..."
        s.load_gasmass()
        print "Loading positions..."
        s.load_gaspos()
        print "Calculating temperatures..."
        s.get_temp(del_ne=False)
        if main_halo_only:
            print "Getting hostsubhalos..."
            s.get_hostsubhalos()
            hostsubhalos = s.hostsubhalos[s.species_start[0]:s.species_end[0]]
            assert len(hostsubhalos) == len(s.gasmass)

        if Rlimtype == "arcmin":
            cm = cosmo.cosmology(s.header.hubble, s.header.omega_m)

        if not os.path.exists(outdir+simdir+"/"):
            os.mkdir(outdir+simdir+"/")
        header = ("main_halo_only = " + str(main_halo_only) + " temp_thresh = "
                  + str(temp_thresh) + " Rlims = " + str(Rlims[0]) + "-" +
                  str(Rlims[1])+" R500")
        outfile = "SZ-M500_z"+"{:.2f}".format(z)+suffix
        if hdf5:
            outfile += ".hdf5"
        else:
            outfile += ".txt"
        cur_groups = []
        M500_list = []
        Y_list = []
        j = 0
        for group in groups:
            M500 = s.cat.group_m_crit500[group]
            R500 = s.cat.group_r_crit500[group]
            if Rlimtype == "R500":
                cur_Rlims = Rlims * R500
            elif Rlimtype == "arcmin":
                # convert arcmins to ckpc/h
                cur_Rlims = cm.arcsec_to_comov(Rlims*60.0, z)*1000.0
                print "cur_Rlims =", cur_Rlims
            else:
                raise ValueError('Rlimtype is unrecognised.')
            if projected:
                if zthick == 0:
                    cur_zthick = s.box_sidelength
                else:
                    cur_zthick = zthick * R500
                rad = np.sqrt((s.rel_pos(s.gaspos[:, 0],
                                         s.cat.group_pos[group, 0]))**2 +
                              (s.rel_pos(s.gaspos[:, 1],
                                         s.cat.group_pos[group, 1]))**2)
                ind = np.where((rad >= cur_Rlims[0]) &
                               (rad < cur_Rlims[1]) &
                               (np.abs(s.rel_pos(s.gaspos[:, 2],
                                                 s.cat.group_pos[group, 2])) < cur_zthick))[0]
            else:  # Get particles within spherical R500
                rad = s.nearest_dist_3D(s.gaspos, s.cat.group_pos[group])
                ind = np.where((rad >= cur_Rlims[0]) & (rad < cur_Rlims[1]))[0]
            print "in radius =     ", len(ind)
            if highres_only:
                s.load_highresgasmass()
                ind = np.intersect1d(ind, np.where(s.hrgm > 0.5*s.gasmass)[0],
                                     assume_unique=True)
            # Get gas particles bound to the main subhalo only
            if main_halo_only:
                ind = np.intersect1d(ind, np.where(hostsubhalos == s.cat.group_firstsub[group])[0])
                print "in main halo =  ", len(ind)
            if temp_thresh > 0:
                ind = np.intersect1d(ind, np.where(s.temp < temp_thresh*s.cat.group_T_crit200[group])[0], assume_unique=True)
                print "in temp thresh =", len(ind)
            if filter_type is not "none":
                import scaling.xray_python.xray as xray
                if not hasattr(s, "gasrho"):
                    s.load_gasrho()
                ind = np.intersect1d(ind, xray.xray_source(s.header.redshift, s.header.hubble, s.header.omega_m).filter_phase_space(s.temp, s.gasrho, filter_type=filter_type, M500=M500, R500=R500/(1+s.header.redshift)), assume_unique=True)
                print "in filter =     ", len(ind)
            if len(ind) == 0:
                print "No cells left. Skipping."
                continue
            electrons = s.gasmass[ind] * UnitMass_in_g / (mproton_in_g + melectron_in_g) * Xh * s.ne[ind] ## h^-1 number of electrons per particle
            PV = electrons * s.temp[ind] #temp=kT, assume electron temp same as gas temp (equilibrium)
            # Mass-weighted sum
            Y = np.sum(PV) * thomson / Eelectron_in_kev ## now in h^-1 m^2
            Y /= (pc * 1e6)**2 ## now in h^-1 Mpc^2
            print "Y =", Y, "h^-1 Mpc^2", j+1, "of", Ntot
            # you would divide now by the angular diameter distance squared to get in units of arcmin^2 as measured by Planck
            # Scale to z=0 and DA=500 Mpc/arcmin
            #Y *= cm.efunc(s.header.redshift)**(-1.0) / (500.0/(180.0/pi*60.0))**2.0
            cur_groups.append(group)
            M500_list.append(M500*1.0e10)
            Y_list.append(Y)
            j += 1

            # ----- write to file ----- #
            # Re-write file after each group
            if hdf5:
                with h5py.File(outdir+simdir+"/"+outfile, "w") as fout:
                    for key, value in attrs.items():
                        fout.attrs[key] = value

                    fout.create_dataset('groups', data=np.c_[cur_groups])
                    m500 = fout.create_dataset('M500', data=np.c_[M500_list])
                    m500.attrs['units'] = "Msun/h"
                    y = fout.create_dataset('Y', data=np.c_[Y_list])
                    y.attrs['units'] = "h^-1 Mpc^2"
            else:
                np.savetxt(outdir+simdir+"/"+outfile,
                           np.c_[cur_groups, M500_list, Y_list],
                           header=(header + "\nGroup M500 [h^-1 Msun] " +
                                   "Y [h^-1 Mpc^2]"))

            print "Saved to", outdir+simdir+"/"+outfile


if __name__ == "__main__":
    main()
