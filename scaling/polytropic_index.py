#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 17 11:47:48 2017
Estimates the polytropic index from the density and temperature profiles of a group
@author: nh444
"""
import numpy as np
import matplotlib.pyplot as plt

fig = plt.figure()

#==============================================================================
# ## For now I'll just load the pre-tabulated profiles output by scripts/scaling code
# R500 = 457.3069
# T_data = np.loadtxt("/data/curie4/nh444/project1/L-T_relations/LDRIntRadioEffBHVel/c256_025/group_0/temperature_entropy_profiles.txt")
# r = T_data[:,2]
# logT = np.log10(T_data[:,4])                     
# rho_data = np.loadtxt("/data/curie4/nh444/project1/L-T_relations/LDRIntRadioEffBHVel/c256_025/group_0/seperate_profiles.txt")
# logrho = np.log10(rho_data[:,6])
#==============================================================================
import h5py
indir = "/data/curie4/nh444/project1/profiles/L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff/"
filename = "snap_025_profiles_mass_weighted.hdf5"
f = h5py.File(indir+filename, 'r')
grp_list = np.asarray(f.keys())
for i in range(len(grp_list)):
    print "Plotting", grp_list[i]
    group = f[grp_list[i]]
    r = group['bins']
    logrho = np.log10(group['density'])
    logT = np.log10(group['temperature'])

    grad = (logT[1:] - logT[:-1]) / (logrho[1:] - logrho[:-1])


    plt.plot(r[1:]/group.attrs['R500'], 1+grad)
#plt.xlim(0.2, 2.0)
plt.xscale('log')
plt.ylim(0.7,1.7)
plt.show()


