#include "read_key.h"

bool find_pos (std::ifstream &file, std::string keyword, bool findend)
{
  bool stopread = false;
  char buffer[512];
  char *find;
  std::streampos pos;
  
  file.seekg(0,std::ios::beg);
  while (!file.eof() && !stopread)
  {
    pos = file.tellg();
    file.getline (buffer,512);
    find = strstr(buffer,keyword.c_str());
    if (find!=0)
    {
      file.seekg(pos);
      file.seekg(find-buffer,std::ios::cur);
      if (findend) file.seekg(keyword.length(),std::ios::cur);
      //file.getline (buffer,512); std::cout << buffer << std::endl; file.seekg(pos); file.seekg(find-buffer,std::ios::cur);
      stopread = true;
    }
  }
  
  if (!stopread)
  {
    std::cout << "ERROR: could not find position of keyword \"" << keyword << "\"" << std::endl;
    return(false);
  }
  
  return(true);  
}

void find_linestart (std::ifstream &file)
{
  char ch;
  bool found = false;
  std::streampos pos;
  pos = file.tellg();
  while (pos>0 && !found)
  {
    pos = int(pos) - 1;
    file.seekg(pos);
    file.get(ch);
    if (ch=='\n') found = true;
    else file.seekg(pos);
  }
}
