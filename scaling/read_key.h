#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <fstream>

template <class keytype>
bool read_key (std::ifstream &file, std::string keyword, keytype &key, bool error_messages = true)
{
  bool stopread = false;
  char buffer[512];
  char *find;
  std::streampos pos;
  
  file.seekg(0,std::ios::beg);
  while (!file.eof() && !stopread)
  {
    pos = file.tellg();
    file.getline (buffer,512);
    find = strstr(buffer,keyword.c_str());
    if (find!=0)
    {
      find = strstr(buffer,"=")+1;
      file.seekg(pos);
      file.seekg(find-buffer,std::ios::cur);
      //file.getline (buffer,512); cout << buffer << endl; file.seekg(pos); file.seekg(find-buffer,ios::cur);
      file >> key;
      stopread = true;
    }
  }
  if (!stopread) file.clear();
  file.seekg(0,std::ios::beg);

  if (!stopread)
  {
    if (error_messages) std::cout << "ERROR: could not read key \"" << keyword << "\"" << std::endl;
    return(false);
  }
  
  return(true); 
}

bool find_pos (std::ifstream &file, std::string keyword, bool findend = false);

void find_linestart (std::ifstream &file);
