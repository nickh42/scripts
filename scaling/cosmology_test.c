#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <cmath>
#include <cassert>
#include <vector>
#include "cosmology.h"

double omega_m = 0.3;
double omega_l = 0.7;
double hubble = 0.7;
double redshift = 0.2;
const double hor = 2997924.58; // 2997924.58 kpc * h^-1 = c/H

cosmology cm(omega_m,omega_l,hubble,-1.0);   
DA = cm.angularDist(0.0,redshift)*hor; // in h^-1 kpc
cout << "DA = " << DA << endl;
