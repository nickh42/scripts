#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 10 13:21:24 2018
Functions for performing fitting of scaling relations
@author: nh444
    import numpy as np
"""
import numpy as np

def fit_powerlaw(X, Y, X0, inislope, inilogC, method="bces_orthog",
                 bootstrap=False, Ntrials=10000, verbose=False):
    """
    Fit a power law relation to the given (X,Y) data in log-space
    The power law has the form:
        log10(Y) = logC + slope*(log(X/X0))
    Args:
        x: independent variable
        y: dependent variable
        X0: pivot point for x
        inislope: initial guess for slope
        inilogC: initial guess for normalisation
        method: fitting method 
            "lm" for Levenberg-Marquardt algorithm using scipy.curve_fit
            "bces" for BCES(Y|X) (Akritas & Bershady 1996)
            "bcesxy" for BCES(X|Y)
            "bces_orthog" for orthogonal BCES
            "linmix" for Kelly 2007 Bayesian linear regression method
        bootstrap: estimate errors on slope and norm by bootstrap resampling with replacement
        Ntrials: number of trials in bootstrap estimate
    """
    
    assert np.shape(X) == np.shape(Y), "X and Y arrays must have same shape."
    logX0 = np.log10(X0)
    powerlaw = make_powerlaw(logX0)
    ## curve_fit in particular needs float64:
    X = X.astype(np.float64)
    Y = Y.astype(np.float64)
    
    ## Get best-fitting parameters using all the data
    method = method.lower() ## convert to lowercase
    if method=="lm":
        from scipy.optimize import curve_fit
        popt, pcov = curve_fit(powerlaw, np.log10(X), np.log10(Y), p0=(inislope, inilogC))
        err = np.sqrt(np.diag(pcov))
        slope = popt[0]
        slope_err = [err[0], err[0]]
        logC = popt[1]
        logC_err = [err[1], err[1]]
        ## estimate intrinsic scatter using method in Maughan 2007:
        scat = estimate_scatter(np.log10(X)-logX0, np.log10(Y), slope, logC)
        scat_err = [0.0, 0.0] ## will be estimated below if bootstrap==True
        if verbose:
            print "slope_err =", slope_err
            print "logC_err =", logC_err
            print "cov =", pcov[0,1], pcov[1,0]
            print "scat =", scat
    elif method=="bces" or method=="bcesxy" or method=="bces_orthog":
        import bces
        ## Give zero errors and zero covariance
        zeros = np.zeros_like(X)
        if bootstrap:
            ## Use parallel version of BCES with bootstrapping
            slope, logC, slope_err, logC_err, cov, scat, scat_err = bces.bcesp(np.log10(X)-logX0, zeros, np.log10(Y), zeros, zeros, nsim=Ntrials)
        else:
            slope, logC, slope_err, logC_err, cov, scat = bces.bces(np.log10(X)-logX0, zeros, np.log10(Y), zeros, zeros)
        if method=="bces":
            idx = 0 ## 0 for Y|X, 1 for X|Y, 2 for bisector, 3 for orthogonal least-squares
        elif method=="bcesxy":
            idx = 1
        elif method=="bces_orthog":
            idx = 3
        slope = slope[idx]
        logC = logC[idx]
        cov = cov[idx]
        if bootstrap:
            slope_err = slope_err[:,idx]
            logC_err = logC_err[:,idx]
            scat = scat[idx]
            scat_err = scat_err[:,idx]
        else:
            slope_err = [slope_err[idx], slope_err[idx]]
            logC_err = [logC_err[idx], logC_err[idx]]
            ## Estimate intrinsic scatter:
            scat = estimate_scatter(np.log10(X)-logX0, np.log10(Y), slope, logC)
            scat_err = [0.0, 0.0] ## can estimate from bootstrapping
        if verbose:
            print "slope_err =", slope_err
            print "logC_err =", logC_err
            print "cov =", cov
            print "scat =", scat
    elif method == "linmix":
        import linmix
        # Create LinMix object with K=2
        # (2 Gaussians should be sufficient to describe the distribution of
        # the true independent variable. I find little difference with K=3)
        # Give zero uncertainties.
        zeros = np.zeros_like(X)
        lm = linmix.LinMix(np.log10(X)-logX0, np.log10(Y),
                           xsig=zeros, ysig=zeros, xycov=zeros, K=2)
        ## Run MCMC to make random draws from the posterior:
        lm.run_mcmc(silent=not verbose)
        ## lm.chain contains the second half of each chain 
        ## I take the mean and standard deviation of all chains
        slope = lm.chain['beta'].mean()
        slope_err = lm.chain['beta'].std()
        slope_err = [slope_err, slope_err]
        logC = lm.chain['alpha'].mean()
        logC_err = lm.chain['alpha'].std()
        logC_err = [logC_err, logC_err]
        ## intrinsic scatter:
        sig = np.sqrt(lm.chain['sigsqr'])
        scat = np.mean(sig)
        scat_err = np.std(sig)
        scat_err = [scat_err, scat_err]
        if verbose:
            print "slope_err =", slope_err
            print "logC_err =", logC_err
            print "scat =", scat
            print "scat_err =", scat_err
    else:
        raise ValueError("Unknown fitting method:", str(method))
        
    if bootstrap and method == "lm":
        if verbose: print "Performing bootstrap..."
        if method == "lm":
            N = len(X)
            ## For each trails of Ntrials, randomly draw a sample of N data points 
            ## from the N data points in (X,Y) with replacement i.e. some get dropped
            ## while others get doubled, tripled etc.
            slope_diff_sq = 0.0 ## cumulative difference in resampled slope and best-fit slope
            logC_diff_sq = 0.0
            cov_diff_sq = 0.0
            scat_diff_sq = 0.0
            for i in range(Ntrials):
                ## Generate indices of a sample from 0 to len(X)-1
                sample_ind = np.random.randint(N, size=N) ## list of N random np.ints from 0 to N-1
                popt, pcov = curve_fit(powerlaw, np.log10(X[sample_ind]), np.log10(Y[sample_ind]), p0=(inislope, inilogC))
                ## Sum the difference between this best-fit parameter and the overall best-fitting parameter
                slope_diff_sq += (popt[0] - slope)**2.0
                logC_diff_sq += (popt[1] - logC)**2.0
                cov_diff_sq += (popt[0] - slope) * (popt[1] - logC)
                cur_scat = estimate_scatter(np.log10(X[sample_ind])-logX0, np.log10(Y[sample_ind]), slope, logC)
                scat_diff_sq += (cur_scat - scat)**2.0
            ## divide sum by Ntrials to get the variance, sigma-squared
            slope_err = [np.sqrt(slope_diff_sq / Ntrials), np.sqrt(slope_diff_sq / Ntrials)]
            logC_err = [np.sqrt(logC_diff_sq / Ntrials), np.sqrt(logC_diff_sq / Ntrials)]
            cov = cov_diff_sq / Ntrials
            scat_err = [np.sqrt(scat_diff_sq / Ntrials), np.sqrt(scat_diff_sq / Ntrials)]
            if verbose:
                print "slope_err =", slope_err[0]
                print "logC_err =", logC_err[0]
                print "cov =", cov
                print "scat_err =", scat_err[0]

    return slope, slope_err, logC, logC_err, scat, scat_err


def make_powerlaw(logX0):
    def powerlaw(logX, slope, logC):
        return logC + slope*(logX - logX0)
    return powerlaw


def make_broken_powerlaw(logX0):
    def broken_powerlaw(logX, slope1, slope2, logC):
        slopes = np.zeros_like(logX)
        slopes[logX < logX0] = slope1
        slopes[logX > logX0] = slope2
        return logC + slopes*(logX - logX0)
    return broken_powerlaw


def estimate_scatter(x, y, slope=None, logC=None):
    """
    Estimate intrinsic scatter using method in Maughan 2007 and Tremaine 2002 assuming zero measurement errors
    i.e. the correct value of the intrinsic scatter in y is the value that gives a reduced chi-squared of 1
    Note that slope and logC are keywords so that this function is compatible
    with bootstrap.py, which requires (positional) arguments that are all
    arrays of the same shape
    """
    
    if slope is None:
        raise ValueError("slope of relation not specified.")
    if logC is None:
        raise ValueError("normalisation logC of relation not specified.")

    ## with no errors, chi-squared equals the sum of the squared deviations of
    ## the points from the best-fit line:
    chisq = np.sum((y - logC - slope*(x))**2)
    n = len(y)
    if n>2:
        red_chisq = chisq / float(n-2) ## reduced chi-squared (divide by n-2 as in Kelly 2007)
        scat = red_chisq**0.5
#        if red_chisq < 1.0:
#            scat = 0.0 ## assume zero scatter as in Kelly 2007 (following Tremaine 2002)
#        else:
#            scat = red_chisq**0.5 ## scatter gives a reduced chi-squared of one
    else:
        scat = 0.0

    return scat
    