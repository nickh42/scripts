from pylab import *
import xspec

xspec.Xset.cosmo="67.9 0.0 0.6935" 

xspec.AllData.clear()
xspec.AllModels.clear()

group = 54
spec = xspec.Spectrum("/data/curie4/nh444/project1/L-T_relations/L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_025/group_"+str(group)+"/spectrum.fak")
#spec = xspec.Spectrum("/data/curie4/nh444/project1/L-T_relations/Helens_clusters/A478_01/group_0/spectrum.fak")

model = xspec.Model("mekal") # e.g. mekal or apec

#in_temp = 6.6 ## A478
in_temp = 0.984071 #group 0: t_m = 0.984071 (t_spec = 1.67365)
#in_temp = 0.133075 #group 88: t_m = 0.133075 (t_spec = 0.20364)
in_nh = 1.0
in_abundance = 0.3
in_redshift = 0.001
#in_redshift = 0.0881 ## A478

model.setPars(in_temp,in_nh,in_abundance,in_redshift,0,1) #mekal
#model.setPars(in_temp,in_abundance,in_redshift,1) #apec
model.mekal.Abundanc.frozen = False
#model.apec.Abundanc.frozen = False

model.show()

## Extend response energies
xspec.AllModels.setEnergies("extend", "low,0.001,30 lin")
xspec.AllModels.setEnergies("extend", "high,111.0,1000 lin")

spec.ignore("1-20 686-1024") # see below + warf.209.fits.gz for channels -> energy
#spec.ignore("1-40 670-1024") # 0.7 - 7.0 keV
## 1-20 = 0.3-0.5 keV
## 686-1024 = 7.16-10.54 keV (max channel 1070 = 11.0 keV)

print "DOING FIT..."
print

xspec.Fit.statMethod = "cstat"
xspec.Fit.nIterations = 1000
xspec.Fit.perform()

Emin_restframe = 0.01
Emax_restframe = 100.0
'''
xspec.AllData.clear()
# If there is no data Xspec will automatically generate a dummy response, default 0.1-50 keV, 50 bins, log scale e.g. see below
xspec.AllModels.calcFlux(str(Emin_restframe/(1.0+in_redshift))+" "+str(Emax_restframe/(1.0+in_redshift)))
flux = model.flux[0]
xspec.AllModels.calcLumin(str(Emin_restframe)+" "+str(Emax_restframe)+" "+str(in_redshift))
luminosity = model.lumin[0]*1.0e44
'''
#xspec.AllData.dummyrsp(0.003, 105.0, 1000, "log") # lowE, highE, nBins, scaleType
#xspec.AllData.dummyrsp(0.1, 50.0, 50, "log") # lowE, highE, nBins, scaleType
xspec.AllModels.calcFlux(str(Emin_restframe/(1.0+in_redshift))+" "+str(Emax_restframe/(1.0+in_redshift)))
flux = spec.flux[0]
xspec.AllModels.calcLumin(str(Emin_restframe)+" "+str(Emax_restframe)+" "+str(in_redshift))
luminosity = spec.lumin[0]*1.0e44

print "flux =", flux, "ergs / cm^2 / s"
print "luminosity =", luminosity, "erg / s"

