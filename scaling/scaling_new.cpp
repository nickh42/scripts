#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <cmath>
#include <cassert>
#include <vector>

/// Header for load_gadget_hdf5 ///
#include "load_gadget_hdf5.h" //includes load_gadget.h
//#include "load_gadget.h" //has to be above table.h (namespace std is used there)

#include "table.h"
#include "cosmology.h"

//#define max(a,b) (((a)>(b))?(a):(b))
//#define min(a,b) (((a)<(b))?(a):(b))

#include "read_write_fits.h"
#include "put_grid.h"
#include "read_flux_table.h"

#define pi 3.1415926535897932384626

#include <algorithm>
#include <cstring>


//--------- ROTATES COORDINATES WITH EULER ANGLES a_z, a_x, a_z2 ------
//(rotates coordinate system not particle)

template <class flpn>
int rotate(flpn& x, flpn& y, flpn& z, double a_z, double a_x, double a_z2)
{
  double x_temp=x, y_temp=y, z_temp=z;
  x = (cos(a_z2)*cos(a_z)-cos(a_x)*sin(a_z)*sin(a_z2))*x_temp
      +(cos(a_z2)*sin(a_z)+cos(a_x)*cos(a_z)*sin(a_z2))*y_temp
      +sin(a_z2)*sin(a_x)*z_temp;
  y = (-sin(a_z2)*cos(a_z)-cos(a_x)*sin(a_z)*cos(a_z2))*x_temp
      +(-sin(a_z2)*sin(a_z)+cos(a_x)*cos(a_z)*cos(a_z2))*y_temp
      +sin(a_x)*cos(a_z2)*z_temp;
  z = sin(a_x)*sin(a_z)*x_temp
      -sin(a_x)*cos(a_z)*y_temp
      +cos(a_x)*z_temp;
  return 0;
}

//class halo declaration

class halo{

protected:
  
  //simulation file
  FILE *fd;
  string filename;
  bool file_open;
  int snapnum;
  bool mpc_lengths;
  bool is_hdf5;

  //output directory
  string output_base;

  //header data
  int ntot,npart[6],nall[6]; 
  int cooling,sfr,feedback,filenum;
  double masstab[6],redshift,time;
  double boxsize,omega_m,omega_l,hubble;

  //mass fraction, approximate mean particle mass
  double f, approx_mmean, ag_hydrogen, ag_helium, ag_metals;

  //particle data
  float *mass, *rho, *hsml, *u, *ne, *z;  
  struct my_vector *pos;
  int *id;

  //additional particle data
  float *temp, *xray_flux, *hot_phase_temp, *mhi;
  int *xray_flux_status;
  bool cooling_branch_excluded;

  //Euler angles by which coordinate system is rotated
  double a_z, a_x, a_z2;

  //angular diameter distance, luminosity distance, critcal_density, mean_density
  double DA, D_lum, rho_crit_today, rho_crit, rho_mean;

  //center of halo
  double centerx, centery, centerz;
  
  //halo information from subfind
  int subf_Len, subf_Offset, subf_ContaminationCount, subf_Nsubs, subf_FirstSub;
  float subf_Mass, subf_M_Mean200, subf_R_Mean200, subf_M_Crit200, subf_R_Crit200, subf_M_Crit500, subf_R_Crit500;
  float subf_M_TopHat200, subf_R_TopHat200, subf_ContaminationMass;
  struct my_vector subf_Pos;
  int subhalo_num, *subhalo_sorted_ids, ids_num;
  bool have_subf_halo_info;
  int subf_info_group;

  bool *member_particle;

  //halo information from FOF special
  struct my_vector fofs_Pos;
  float fofs_temp_ew_200;
  bool have_fofs_halo_info;  
  int fofs_info_group;

  //halo information from denhf
  bool have_denhf_halo_info;

  //profiles
  double *matter_mass_profile, *matter_cum_mass_profile, *matter_density_profile;
  double matter_profile_radius;
  int matter_profile_binnum;
  double *luminosity_profile, *cum_luminosity_profile, *luminosity_density_profile;
  double xray_profile_radius;
  int xray_profile_binnum;
  double *seperate_mass_profiles, *seperate_cum_mass_profiles, *seperate_density_profiles;
  double seperate_profiles_radius;
  int seperate_profiles_binnum;
  double *temperature_profile, *entropy_profile;
  double temperature_entropy_profile_radius;
  int temperature_entropy_profile_binnum;
  double *metallicity_profile;
  double metallicity_profile_radius;
  int metallicity_profile_binnum;
  double mean_metallicity;
  double median_metallicity;
  string metals_method;
  double *hsml_profile;
  double hsml_profile_radius;
  int hsml_profile_binnum;

public:

  halo (string filename, string metals_method, bool mpc_lengths, bool is_hdf5);
  ~halo ();

  inline double get_time_of_snap () {return time;};

  void set_output_base (string dir, int snapnumber);

  void show_header ();

  //load particle data
  int load_mass ();
  int load_pos ();
  int load_id ();
  int load_rho ();
  int load_hsml ();
  int load_u ();
  int load_ne ();
  int load_z ();
  int load_hot_phase_temp ();
  int load_mhi ();
  
  void calculate_temp ();
  void calculate_xray_flux (string flux_table, int channel, bool exclude_cooling_branch, bool use_multiphase,
                            bool use_sim_metallicities); 

  void rotate_coordinates (double a_z, double a_x, double a_z2);
  void set_center (double centerx, double centery, double centerz);
  void get_halo_information_subf (int group, bool get_center, int subhalo, string filename);
  void get_halo_information_fof_special (int group, bool get_center, string filename);
  void get_center_denhf (string filename);

  void mark_subhalo_members ();
  void mark_region_members (char firstcoor, char secondcoor, string region_file);

  void get_matter_profile (int binnum, double radius);
  void save_matter_profile ();
  double get_radius_for_contrast (double contrast, string ref);

  void get_xray_profile (int binnum, double radius);
  void save_xray_profile ();
  double get_luminosity_for_radius (double inner_radius, double outer_radius);
  double get_temperature_for_radius (string type, double inner_radius, double outer_radius,
                                     bool use_projected_radii, bool members_only);
  double get_mean_temperature ();

  void get_seperate_profiles (int binnum, double radius);
  void save_seperate_profiles (string filename, double ref_radius);
  double get_matter_type_mass (int type, double inner_radius, double outer_radius);

  void get_temperature_entropy_profiles (int binnum, double radius);
  void save_temperature_entropy_profiles (string filename, double ref_radius);

  void get_metallicity_profile (int binnum, double radius);
  void save_metallicity_profile ();
 
  void get_hsml_profile (int binnum, double radius);
  void save_hsml_profile ();

  void show_properties (bool to_file);

  int make_mass_map (int gridsize, double sidelength, string filename);
  int make_xray_maps (int gridsize, double sidelength, bool members_only, int save_densities_and_temperatures);

  void write_xspec_model_file (double inner_radius, double outer_radius,
                               bool use_projected_radii, bool members_only,
                               bool exclude_cooling_branch, bool use_multiphase, bool use_sim_metallicities,
                               bool fit, bool use_apec, bool ignore_outside_range, double &t_spec, double &abund_spec, double &norm_spec, double &lum_spec,
                               string outfile_suffix);

  void get_observables_for_halos (int num_groups, string group_info, bool make_maps,
                                  bool use_projected_radii, bool only_main_halo_particles, bool use_sim_metallicities, int group_start, bool use_halo_r500, bool use_apec, bool ignore_outside_range);
};

//class halo implementation

halo::halo (string filename, string metals_method, bool mpc_lengths = false, bool is_hdf5 = false)
{
  this->filename = filename;
  this->mpc_lengths = mpc_lengths;
  this->is_hdf5 = is_hdf5;
  this->metals_method = metals_method;

  if (is_hdf5 == 0) {
    if(!(fd = fopen(filename.c_str(),"rb")))
    {
      if(!(fd = fopen((filename+".0").c_str(),"rb")))
      {
        std::cout << "ERROR: Cant open file " << filename << " or file "<< filename+".0" << "!" << std::endl;
        exit(1);
      }
    }
  }
  file_open = true;
  cout << "File opened: " << filename << endl;

  if (is_hdf5) { read_gadget_head_full_hdf5(npart,masstab,&time,&redshift,&sfr,&feedback,nall,
                        &cooling,&filenum,&boxsize,&omega_m,&omega_l,&hubble,filename.c_str());}
  else { read_gadget_head_full(npart,masstab,&time,&redshift,&sfr,&feedback,nall,
                        &cooling,&filenum,&boxsize,&omega_m,&omega_l,&hubble,fd);}
  if (redshift < 0.001) redshift = 0.001; ///This is here because very low redshift can cause the MEKAL norm to go beyond the XSPEC hard limit
  ntot = 0;
  for(int i=0; i<6; i++) ntot += nall[i];

  mass = 0; rho = 0; hsml = 0; u = 0; ne = 0; z = 0; 
  pos = 0; id = 0;
  temp = 0; xray_flux = 0; hot_phase_temp = 0; mhi = 0; xray_flux_status = 0;

  f = 0.76; approx_mmean = (1.0+(1.0-f)/f)/(2.0+3.0*(1.0-f)/(4.0*f));
  ag_hydrogen = 0.70683, ag_helium = 0.27431, ag_metals = 0.01886; // Anders, Grevesse mass fractions
  
  a_z = 0; a_x = 0; a_z2 = 0;

  matter_mass_profile = 0; 
  matter_cum_mass_profile = 0; 
  matter_density_profile = 0;
  matter_profile_radius = 0;
  matter_profile_binnum = 0;

  luminosity_profile = 0; 
  cum_luminosity_profile = 0; 
  luminosity_density_profile = 0;
  xray_profile_radius = 0;
  xray_profile_binnum = 0;

  seperate_mass_profiles = 0;
  seperate_cum_mass_profiles = 0;
  seperate_density_profiles = 0;
  seperate_profiles_radius = 0;
  seperate_profiles_binnum = 0;

  temperature_profile = 0;
  entropy_profile = 0;
  temperature_entropy_profile_radius = 0;
  temperature_entropy_profile_binnum = 0;
  
  metallicity_profile = 0;
  metallicity_profile_radius = 0;
  metallicity_profile_binnum = 0; 
  mean_metallicity = 0;
  median_metallicity = 0;

  hsml_profile = 0;
  hsml_profile_radius = 0;
  hsml_profile_radius = 0;

  const double hor = 2997924.58; // 2997924.58 kpc * h^-1 = c/H
  cosmology cm(omega_m,omega_l,hubble,-1.0);   
  DA = cm.angularDist(0.0,redshift)*hor; // in h^-1 pkpc
  D_lum = DA*(1+redshift)*(1+redshift)*3.0856775807e21/hubble; // in cm e.g. ~1.36e25 cm at z=0.001
  rho_crit_today = 2.77536627e-8/pow(1+redshift,3); 
    // comoving value at z = redshift of the physical critical density today
  rho_crit = 2.77536627e-8*pow(cm.hubble(redshift),2)/(pow(hubble,2)*pow(1+redshift,3));
  rho_mean = 2.77536627e-8*omega_m;
    // 2.77536627e-8 critical density today in Gadget Units, value is independent of h, 
    // as the second power of h also appears in the Gadget density units

  stringstream snapnum_sstr;
  snapnum_sstr << filename.substr(filename.length()-3,3);
  if (!(is_hdf5)) snapnum_sstr >> snapnum;

  have_subf_halo_info = false;
  subf_info_group = -1;
  have_fofs_halo_info = false;
  fofs_info_group = -1;
  have_denhf_halo_info = false;

  subhalo_num = -1;
  member_particle = 0;
  subhalo_sorted_ids = 0;
  ids_num = 0;

  cooling_branch_excluded = true;
}

halo::~halo ()
{
 
  if (is_hdf5) {
    free(mass); 
    free(rho); 
    free(hsml); 
    free(u); 
    free(ne);  
    free(z);
    free(pos);
    free(id);
  }
  else {
    if (fd != 0) fclose(fd);
    delete[] mass; 
    delete[] rho; 
    delete[] hsml; 
    delete[] u; 
    delete[] ne;  
    delete[] z;
    delete[] pos;
    delete[] id;
  }
 
  delete[] member_particle;
  delete[] subhalo_sorted_ids;

  delete[] temp;
  delete[] xray_flux;
  delete[] hot_phase_temp;
  delete[] mhi;
  delete[] xray_flux_status;

  delete[] matter_mass_profile; 
  delete[] matter_cum_mass_profile; 
  delete[] matter_density_profile;

  delete[] luminosity_profile; 
  delete[] cum_luminosity_profile; 
  delete[] luminosity_density_profile;

  delete[] seperate_mass_profiles;
  delete[] seperate_cum_mass_profiles;
  delete[] seperate_density_profiles;

  delete[] temperature_profile;
  delete[] entropy_profile;

  delete[] metallicity_profile;

  delete[] hsml_profile;
}

void halo::set_output_base (string dir, int snapnumber) {output_base = dir; snapnum = snapnumber;}

void halo::show_header ()
{
   cout << "-------------------------------------" << endl;
   cout << "HEADER of " << filename << endl;
   cout << "snapnum = " << snapnum << endl << endl;

   cout << "particles:" << endl;    
   for(int k=0; k<6; ++k)
   {
     cout << "species " << k << ", number = " << nall[k] << ", masstab = " 
          << masstab[k] << endl;
   }
   cout << "total particle number = " << ntot << endl << endl;
   
   cout << "time of snapshot = " << time << ", redshift = " << redshift << endl;
   cout << "star formation = " << sfr << endl;
   cout << "Feedback = " << feedback << endl;
   cout << "cooling = " << cooling << endl;
   cout << "boxsize = " << boxsize << endl;
   cout << "reduced Hubble parameter = " << hubble << endl;
   cout << "omega_m = " << omega_m << endl;
   cout << "omeba_l = " << omega_l << endl; 

   cout << "-------------------------------------" << endl;
}

int halo::load_mass ()
{
  mass = new float[ntot];
  float *temp_mass = 0;
  int result = 0;  

  bool need_massblock = false;
  for(int k=0; k<6; k++) if (nall[k]!=0 && masstab[k]==0) need_massblock = true;
  if (need_massblock) {
    if (is_hdf5) read_gadget_hdf5(&temp_mass,"MASS",filename.c_str());
    else {result = read_gadget_multi(temp_mass,"MASS",filename);}
  }

  double mass_in_species; 

  int i = 0, m = 0;
  for(int k=0; k<6; k++)
  {
    mass_in_species = 0;
    
    for(int j=0;j<nall[k];j++)
    {
      if (masstab[k]!=0) mass[i] = masstab[k];
      else {mass[i] = temp_mass[m]; ++m;}
      mass_in_species += mass[i];

      if(j==0 || j==npart[k]-1) cout << "mass = " << mass[i] << endl; 

      ++i;
    }

    cout << "mass of all particles of species " << k << " = " << mass_in_species << endl;  
  }   
   
  if (is_hdf5) free(temp_mass);
  else {delete[] temp_mass;}
  
  return result;
}

int halo::load_pos ()
{
  int coordinates_read;
  if (is_hdf5) coordinates_read = read_gadget_hdf5(&pos,"POS ", filename.c_str());
  else {coordinates_read = read_gadget_multi(pos,"POS ", filename);}
  if (mpc_lengths) for (int i=0; i<ntot; ++i) {pos[i].x *= 1000.0; pos[i].y *= 1000.0; pos[i].z *= 1000.0;}
  return coordinates_read;
}

int halo::load_id ()
{
  if (is_hdf5) return read_gadget_hdf5(&id,"ID  ", filename.c_str());
  else {return read_gadget_multi(id,"ID  ", filename);}
}

int halo::load_rho ()
{
  int floats_read;
  if (is_hdf5) floats_read = read_gadget_hdf5(&rho,"RHO ",filename.c_str());
  else {floats_read = read_gadget_multi(rho,"RHO ",filename);}
  if (mpc_lengths) for (int i=0; i<nall[0]; ++i) rho[i] /= 1e9;
  return floats_read;
}

int halo::load_hsml ()
{
  int floats_read; 
  if (is_hdf5) floats_read = read_gadget_hdf5(&hsml,"HSML",filename.c_str());
  else {floats_read = read_gadget_multi(hsml,"HSML",filename);}
  if (mpc_lengths) for (int i=0; i<nall[0]; ++i) hsml[i] *= 1000.0;
  return floats_read;
}

int halo::load_u ()
{
  if (is_hdf5) return read_gadget_hdf5(&u,"U   ",filename.c_str());
  else {return read_gadget_multi(u,"U   ",filename);}
}

int halo::load_ne ()
{
  if (is_hdf5) return read_gadget_hdf5(&ne,"NE  ",filename.c_str());
  else {return read_gadget_multi(ne,"NE  ",filename);}
}

int halo::load_z ()
{
  if (is_hdf5) return read_gadget_hdf5(&z,"Z   ",filename.c_str());
  else {return read_gadget_multi(z,"Z   ",filename);}
}

int halo::load_hot_phase_temp () 
{
  int floats_read;
  if (is_hdf5) floats_read = read_gadget_hdf5(&hot_phase_temp,"TEMP",filename.c_str());
  else {floats_read = read_gadget_multi(hot_phase_temp,"TEMP",filename);}
  for (int i = 0; i < nall[0]; ++i) hot_phase_temp[i] *= 1.3806503e-23/1.602176462e-16;
  return floats_read;
}

int halo::load_mhi () //load mass in neutral hydrogen
{
  if (is_hdf5) return read_gadget_hdf5(&mhi,"MHI ",filename.c_str());
  else {return read_gadget_multi(mhi,"MHI ",filename);}
}

void halo::calculate_temp ()
{
  assert(u!=0);
  if (ne==0) cout << "using approx_mmean in calculate_temp" << endl;
  delete[] temp;
  temp = new float[nall[0]];  

  double mmean_exact;
  for (int i=0; i < nall[0]; ++i)
  {
    if (ne!=0) mmean_exact = 4.0 / (1.0 + 3.0*f + 4.0*f*ne[i]);
    else mmean_exact = approx_mmean;

    //temp[i] = 2.0/3.0*u[i]*1.0e6*mmean*1.67262158e-27/1.3806503e-23; //in Kelvin
    //temp[i] = 2.0/3.0*u[i]*1.0e6*mmean*1.67262158e-27/1.602176462e-16; //in keV
        
    temp[i] = 2.0/3.0*u[i]*1.0e6*mmean_exact*1.67262158e-27/1.602176462e-16; //in keV   
  }  
}

/* Estimate the x-ray flux contribution of each particle */
void halo::calculate_xray_flux (string flux_table, int channel , bool exclude_cooling_branch, bool use_multiphase,
                                bool use_sim_metallicities)
{
  assert(u!=0 || temp !=0);
  if (ne==0) cout << "using approximate free electron density in calculate_xray_flux" << endl;
  if (temp==0) calculate_temp();
  
  if (use_multiphase) assert(mhi!=0 && hot_phase_temp!=0);
  if (use_sim_metallicities) assert(z!=0);
  
  channel_data *fluxptr = 0;
  if (flux_table!="simple")
  {
    fluxptr = new channel_data(flux_table.c_str(),channel);
    cout << "loaded flux table: " << flux_table << endl;
    fluxptr[0].show();
  }

  /*double test_flux = 0, curtemp;
  bool test_bool;
  for (int curtempnum = 0; curtempnum < 1024; ++curtempnum)
  {
    curtemp = 0.1 + (24.0-0.1)/(1024.0-1.0)*curtempnum;
    test_flux += fluxptr[0].getflux(0.3,curtemp,test_bool);
  }
  cout << "test_flux = " << test_flux << endl;*/

  xray_flux = new float[nall[0]];
  xray_flux_status = new int[nall[0]];

  bool inside_range;
  double hydrogen_mass_fraction, xspec_metallicity, nelec, temperature, hot_fraction;
  for (int i=0; i<nall[0]; ++i)
  {
    if (!sfr || ((temp[i]>=3e4*8.617342e-8 || rho[i]<=500*0.044*2.77536627e-8 || !exclude_cooling_branch)
                 && (rho[i] < 0.00085483/((1+redshift)*(1+redshift)*(1+redshift)) || use_multiphase))) 
    //is the rho threshold 500*0.044*2.77536627e-8 right ???
    //temp threshold ???
    //exclude_cooling_branch ???
    {      
//       // old parameters
//       if (z!=0) hydrogen_mass_fraction = (1-z[i])*f;
//       else hydrogen_mass_fraction = f;
//       if (ne!=0) nelec = ne[i];
//       else nelec = 1.0 + (1.0-f)/(2.0*f);
//       xspec_metallicity = 0.3;

      // more accurate parameters
      if (use_sim_metallicities) xspec_metallicity = z[i]/(1-z[i])*(ag_hydrogen+ag_helium)/ag_metals;
      else if (strcmp(metals_method.c_str(), "median") == 0) xspec_metallicity = median_metallicity/(1-median_metallicity)*(ag_hydrogen+ag_helium)/ag_metals;
      else if (strcmp(metals_method.c_str(), "mean") == 0) xspec_metallicity = mean_metallicity/(1-mean_metallicity)*(ag_hydrogen+ag_helium)/ag_metals;
      else if (strcmp(metals_method.c_str(), "fixed") == 0) xspec_metallicity = 0.3;
      else {
        cout << "ERROR: metals_method not recognised. Using 0.3 solar.";
        xspec_metallicity = 0.3;
      }
      hydrogen_mass_fraction = ag_hydrogen/(ag_hydrogen+ag_helium+xspec_metallicity*ag_metals); //0.716286 for xspec_metallicity = 0.3
      nelec = 1.0 + 0.5*ag_helium/ag_hydrogen + 0.5*xspec_metallicity*ag_metals/ag_hydrogen;
      if (ne!=0) nelec *= ne[i]/(1.0+(1.0-f)/(2.0*f));

      if (use_multiphase)
      {
        hot_fraction = 1.0 - mhi[i]/(f*mass[i]);
        //if (hot_fraction < 0.9) hot_fraction = 0.0;
        temperature = hot_phase_temp[i];
      }
      else
      {
        temperature = temp[i];
        hot_fraction = 1.0;
      }

      if (flux_table!="simple") 
      {
        xray_flux[i] = 5.05216e12*mass[i]*rho[i]*hubble*hubble*hubble //see mekal_norm.nb for constant
                       *hydrogen_mass_fraction*hydrogen_mass_fraction*hot_fraction*hot_fraction*nelec*(1+redshift)/(4.0*pi*DA*DA)
                       *fluxptr[0].getflux(xspec_metallicity,temperature,inside_range);
        if (inside_range) xray_flux_status[i] = 0;
        else xray_flux_status[i] = 1;
      }    
      else //if flux_table = "simple"
      {
        xray_flux[i] = 1.17e-23*1e20/4.0*pow(1.98844e30/1.67262171e-27,2)*pow(3.0856775807e21,-3)
                       *(1+f)*hubble*pow(1+redshift,3)*sqrt(temperature)*mass[i]*rho[i]*hot_fraction*hot_fraction/(4*pi*D_lum*D_lum);
        xray_flux_status[i] = 0;
      }            
    }
    else {xray_flux[i] = 0; xray_flux_status[i] = 2;}
  }

  cooling_branch_excluded = exclude_cooling_branch;

  delete[] fluxptr;
}

void halo::rotate_coordinates (double a_z, double a_x, double a_z2)
{
  cout << "rotating coordinate system";
  cout.flush();
  
  assert(this->a_z==0 && this->a_x==0 && this->a_z2==0);

  double ang_z = a_z*pi; 
  double ang_x = a_x*pi; 
  double ang_z2 = a_z2*pi;

  if (ang_z != 0 || ang_x != 0 || ang_z2 !=0)
  {
    for (int i=0; i<ntot; i++) rotate(pos[i].x, pos[i].y, pos[i].z, ang_z, ang_x, ang_z2);
    rotate(centerx,centery,centerz,ang_z,ang_x,ang_z2);
  }
  
  this->a_z = a_z;
  this->a_x = a_x;
  this->a_z2 = a_z2;
  
  cout << " FINISHED" << endl;
}

void halo::set_center (double centerx, double centery, double centerz)
{
  cout << "center set to x = " << centerx << ", y = " << centery << ", z = " << centerz << endl;  
  
  if (a_z != 0 || a_x != 0 || a_z2 !=0)
    rotate(centerx,centery,centerz,a_z*pi,a_x*pi,a_z2*pi);
  
  this->centerx = centerx;
  this->centery = centery; 
  this->centerz = centerz;
}

void halo::get_halo_information_subf (int group, bool get_center, int subhalo = -1, string file = "none")
{
  if (file == "none")
  {
    string snapnumstr = filename.substr(filename.length()-3,3);
    size_t foundsnapdir = filename.find("snapdir");
    if (foundsnapdir!=string::npos) file = filename.substr(0,foundsnapdir);
    else file = filename.substr(0,filename.find_last_of("/")+1);
    file += "groups_" + snapnumstr + "/subhalo_tab_" + snapnumstr + ".0";
  }
  cout << "getting center from file: " << file << endl;

  ifstream subhalo_tab(file.c_str());

  int Ngroups, TotNgroups, Nids, NTask, Nsubgroups, TotNsubgroups;
  long long TotNids;

  // Read in header
  assert(sizeof(int)==4 && sizeof(long long)==8);
  subhalo_tab.read((char*) &Ngroups, 4);
  subhalo_tab.read((char*) &TotNgroups, 4);
  subhalo_tab.read((char*) &Nids, 4);
  subhalo_tab.read((char*) &TotNids, 8);
  subhalo_tab.read((char*) &NTask, 4);
  subhalo_tab.read((char*) &Nsubgroups, 4);
  subhalo_tab.read((char*) &TotNsubgroups, 4);

  int *locLen = new int[Ngroups];
  int *locOffset = new int[Ngroups];
  float *locMass = new float[Ngroups];
  struct my_vector *locPos = new my_vector[Ngroups];
  float *loc_M_Mean200 = new float[Ngroups];
  float *loc_R_Mean200 = new float[Ngroups];
  float *loc_M_Crit200 = new float[Ngroups];
  float *loc_R_Crit200 = new float[Ngroups];
  float *loc_M_TopHat200 = new float[Ngroups];
  float *loc_R_TopHat200 = new float[Ngroups];
  int *locContaminationCount = new int[Ngroups];
  float *locContaminationMass = new float[Ngroups];
  int *locNsubs = new int[Ngroups];
  int *locFirstSub = new int[Ngroups];
 
  // Read in group data
  assert(sizeof(float)==4);
  subhalo_tab.read((char*) locLen, 4*Ngroups);
  subhalo_tab.read((char*) locOffset, 4*Ngroups);
  subhalo_tab.read((char*) locMass, 4*Ngroups);
  subhalo_tab.read((char*) locPos, 3*4*Ngroups);
  subhalo_tab.read((char*) loc_M_Mean200, 4*Ngroups);
  subhalo_tab.read((char*) loc_R_Mean200, 4*Ngroups);
  subhalo_tab.read((char*) loc_M_Crit200, 4*Ngroups);
  subhalo_tab.read((char*) loc_R_Crit200, 4*Ngroups);
  subhalo_tab.read((char*) loc_M_TopHat200, 4*Ngroups);
  subhalo_tab.read((char*) loc_R_TopHat200, 4*Ngroups); 
  subhalo_tab.read((char*) locContaminationCount, 4*Ngroups);
  subhalo_tab.read((char*) locContaminationMass, 4*Ngroups);
  subhalo_tab.read((char*) locNsubs, 4*Ngroups);
  subhalo_tab.read((char*) locFirstSub, 4*Ngroups);

  // Store data for given group
  subf_Len = locLen[group];
  subf_Offset = locOffset[group];
  subf_Mass = locMass[group]; 
  subf_Pos = locPos[group]; 
  subf_M_Mean200 = loc_M_Mean200[group]; 
  subf_R_Mean200 = loc_R_Mean200[group]; 
  subf_M_Crit200 = loc_M_Crit200[group]; 
  subf_R_Crit200 = loc_R_Crit200[group]; 
  subf_M_TopHat200 = loc_M_TopHat200[group]; 
  subf_R_TopHat200 = loc_R_TopHat200[group]; 
  subf_ContaminationCount = locContaminationCount[group]; 
  subf_ContaminationMass = locContaminationMass[group];
  subf_Nsubs = locNsubs[group];
  subf_FirstSub = locFirstSub[group];

  delete[] locLen;
  delete[] locOffset;
  delete[] locMass;
  delete[] locPos;
  delete[] loc_M_Mean200;
  delete[] loc_R_Mean200;
  delete[] loc_M_Crit200;
  delete[] loc_R_Crit200;
  delete[] loc_M_TopHat200;
  delete[] loc_R_TopHat200;
  delete[] locContaminationCount;
  delete[] locContaminationMass;
  delete[] locNsubs;
  delete[] locFirstSub;

  have_subf_halo_info = true;
  subf_info_group = group;
  
  if (get_center)
  {
    if (mpc_lengths) set_center(subf_Pos.x*1000.0, subf_Pos.y*1000.0, subf_Pos.z*1000.0);
    else set_center(subf_Pos.x, subf_Pos.y, subf_Pos.z);
  }
  
  // Read in subhalo data (follows group data)
  locLen = new int[Nsubgroups];
  locOffset = new int[Nsubgroups];
  locMass = new float[Nsubgroups];
  locPos = new my_vector[Nsubgroups];
  int *locParent = new int[Nsubgroups];
  int *locGrNr = new int[Nsubgroups];
 
  subhalo_tab.read((char*) locLen, 4*Nsubgroups);
  subhalo_tab.read((char*) locOffset, 4*Nsubgroups);
  subhalo_tab.read((char*) locParent, 4*Nsubgroups);
  subhalo_tab.read((char*) locMass, 4*Nsubgroups);
  subhalo_tab.read((char*) locPos, 3*4*Nsubgroups);
  subhalo_tab.seekg(14*4*Nsubgroups, ios::cur);
  subhalo_tab.read((char*) locGrNr, 4*Nsubgroups);  

  subhalo_tab.close();

  // Create subhalo_sorted_ids
  if (subhalo>=0)
  {
    assert(subf_FirstSub+subhalo < Nsubgroups);
    int length = locLen[subf_FirstSub+subhalo];
    int offset = locOffset[subf_FirstSub+subhalo];

    size_t stringpos = file.rfind("tab");
    assert(stringpos!=string::npos);
    file.replace(stringpos,3,"ids");
    stringpos = file.rfind(".");
    assert(stringpos!=string::npos);
    file.erase(stringpos+1,string::npos); // 'file' should now be of the form "subhalo_ids_000." (group 0)
    
    string cur_file;
    int *subf_ids = new int[TotNids], cur_Nids, cur_offset=0, file_offset;
    stringstream strs;
    char str[256];

    for (int fnr=0; fnr<NTask; ++fnr) // loops over multiple files e.g. subhalo_ids_000.0, subhalo_ids_000.1 etc.
    {
      strs << file << fnr << endl;
      strs.getline(str,256);
      ifstream subhalo_ids(str);

      subhalo_ids.seekg(2*4,ios::cur);      
      subhalo_ids.read((char*) &cur_Nids, 4);
      subhalo_ids.seekg(3*4,ios::cur);
      subhalo_ids.read((char*) &file_offset, 4);
      assert(file_offset==cur_offset);
      
      subhalo_ids.read((char*) (subf_ids+cur_offset), 4*cur_Nids); //concatenate ids from multiple files into 'subf_ids'
      cur_offset += cur_Nids;

      subhalo_ids.close();    
    }
    assert(cur_offset==TotNids);

    assert(offset+length<=TotNids);
    subhalo_sorted_ids = new int[length];
    ids_num = length;
    for (int i=0; i<length; ++i) subhalo_sorted_ids[i] = subf_ids[i+offset]; //store ids of particles in the given subhalo
    delete[] subf_ids;    
    std::sort(subhalo_sorted_ids, subhalo_sorted_ids+length);  
   
    subhalo_num = subhalo;
  }

  delete[] locLen;
  delete[] locOffset;
  delete[] locParent;
  delete[] locMass;
  delete[] locPos;
  delete[] locGrNr;
}

void halo::get_halo_information_fof_special (int group, bool get_center, string file = "none")
{
  if (file == "none")
  {
    string snapnumstr = filename.substr(filename.length()-3,3);
    file = filename.substr(0,filename.find_last_of("/")+1) +
           "groups_properties/fof_special_properties_" + snapnumstr;
  }
  cout << "getting center from file: " << file << endl;

  ifstream fof_special_properties(file.c_str());

  int Ngroups;

  assert(sizeof(int)==4 && sizeof(long long)==8);
  fof_special_properties.read((char*) &Ngroups, 4);

  struct my_vector *locPos = new my_vector[Ngroups];
  fof_special_properties.read((char*) locPos, 3*4*Ngroups);
  fof_special_properties.seekg(19*4*Ngroups, ios::cur);
  float *temp_ew_200 = new float [Ngroups];
  fof_special_properties.read((char*) temp_ew_200, 4*Ngroups);

  fof_special_properties.close();

  fofs_Pos = locPos[group];
  fofs_temp_ew_200 = temp_ew_200[group];

  delete[] locPos;
  delete[] temp_ew_200;

  have_fofs_halo_info = true;
  fofs_info_group = group;  

  if (get_center)
  {
    if (mpc_lengths) set_center(fofs_Pos.x*1000.0, fofs_Pos.y*1000.0, fofs_Pos.z*1000.0);
    else set_center(fofs_Pos.x, fofs_Pos.y, fofs_Pos.z);
  }
}

void halo::get_center_denhf (string file)
{
  int error=1;
  char* buffer = (char*) malloc(256*sizeof(char));
  char* start;
  char* stop;

  if (file == "none")
  {
    string snapnumstr = filename.substr(filename.length()-3,3);
    file = filename.substr(0,filename.find_last_of("/")+1) + "Post/main_prog.a_dm.gv";
  }
  cout << "getting center from file: " << filename << endl;

  ifstream denhf_file (file.c_str());

  if (denhf_file.is_open())
  while (!denhf_file.eof() && error == 1)
  {
    denhf_file.getline(buffer,256);
    start=buffer;
    if (strtol(start,&stop,10)==snapnum)
    {
      start=stop;
      strtol(start,&stop,10);
      start=stop;
      strtol(start,&stop,10);
      start=stop;
      strtol(start,&stop,10);
      start=stop;
      strtod(start,&stop);
      start = stop;
      strtod(start,&stop);
      start = stop;
      strtod(start,&stop);
      start = stop;
      strtod(start,&stop);
      start = stop;
      strtod(start,&stop);
      start = stop;
      centerx = 1000*strtod(start,&stop);
      start = stop;
      centery = 1000*strtod(start,&stop);
      start = stop;
      centerz = 1000*strtod(start,&stop);      
      error=0;
    }
  }
  denhf_file.close();
  
  cout << "centerx = " << centerx << ", centery = " << centery << ",centerz = " << centerz << endl;  

  assert(error==0);

  have_denhf_halo_info = true;
}

struct particle_index_tracer{
  int id, index; 
  bool member; 
}; 

bool index_less_than (particle_index_tracer a, particle_index_tracer b)
{
  return (a.index<b.index); 
}

bool id_less_than (particle_index_tracer a, particle_index_tracer b)
{
  return (a.id<b.id); 
}

void halo::mark_subhalo_members ()
{
  assert(id!=0);

  particle_index_tracer *tracer = new particle_index_tracer[ntot];
  for (int i=0; i<ntot; ++i)
  {
    tracer[i].id = id[i];
    tracer[i].index = i;
    tracer[i].member = false;
  }

  std::sort(tracer,tracer+ntot,id_less_than);

  int ids_offset = 0, i = 0;
  while (ids_offset<ids_num)//ids_num is number of particles in chosen subhalo
  {
    assert(i<ntot);
   
    if (tracer[i].id == subhalo_sorted_ids[ids_offset])
    {
      tracer[i].member = true;
      ++i;
      ++ids_offset;
    }
    else ++i;
  } 

  std::sort(tracer,tracer+ntot,index_less_than);

  if (member_particle == 0)
  {
    member_particle = new bool[ntot];
    for (int i=0; i<ntot; ++i) member_particle[i] = true;
  }

  for (int i=0; i<ntot; ++i) if (tracer[i].member == false) member_particle[i] = false;

  int marked_gas = 0, marked_dm = 0, marked_stars = 0, marked_boundaries = 0;
  for (int i=0; i<nall[0]; ++i) if (member_particle[i]) ++marked_gas;
  for (int i=nall[0]; i<nall[0]+nall[1]; ++i) if (member_particle[i]) ++marked_dm;
  for (int i=nall[0]+nall[1]; i<nall[0]+nall[1]+nall[2]+nall[3]; ++i) if (member_particle[i]) ++marked_boundaries;
  for (int i=nall[0]+nall[1]+nall[2]+nall[3]; i<nall[0]+nall[1]+nall[2]+nall[3]+nall[4]; ++i) 
    if (member_particle[i]) ++marked_stars;

  cout << "MARKING MEMBERS OF SUBHALO " << subhalo_num << " OF GROUP " << subf_info_group << ":" << endl;
  cout << marked_gas << " of " << nall[0] << " gas particles marked as member" << endl;
  cout << marked_dm << " of " << nall[1] << " DM particles marked as member" << endl;
  cout << marked_boundaries << " of " << nall[2]+nall[3] << " DM boundary particles marked as member" << endl;
  cout << marked_stars << " of " << nall[4] << " star particles marked as member" << endl;
  if (is_hdf5) { cout << "WARNING: Only gas particles marked as members for hdf5 snapshots.\n" << endl;}

  delete[] tracer;
}

struct region{
  double imx, imy, par1;
  string type;
};

void halo::mark_region_members (char firstcoor, char secondcoor, string region_file)
{ 
  ifstream regfile(region_file.c_str());
  char line[256];
  stringstream strs;  

  vector<region> regions;
  region cur_reg;
  char buff;
  bool found_region;

  while (!regfile.eof())
  {
    regfile.getline(line,256);
    //cout << line << endl;

    found_region = false;
    
    if (std::strncmp(line,"-circle",7) == 0)
    {
      strs << line+7;
      strs >> buff>> cur_reg.imx >> buff >> cur_reg.imy >> buff >> cur_reg.par1 >> buff;
      cur_reg.type = "-circle";
      found_region = true;
    }
    
    if (found_region)
    {
      //cout << "-circle found: " << cur_reg.imx << " " << cur_reg.imy << " " << cur_reg.par1 << endl;
      regions.push_back(cur_reg);
    }
  }
  
  int regions_num = regions.size();
  cout << "read " << regions_num << " region from " << region_file << endl;

  if (member_particle == 0)
  {
    member_particle = new bool[ntot];
    for (int i=0; i<ntot; ++i) member_particle[i] = true;
  }
 
  double part_imx, part_imy;

  for (int reg=0; reg<regions_num; ++reg)
  {
    if (regions.at(reg).type == "-circle")
    {
      for (int i=0; i<ntot; ++i)
      {
        if (firstcoor == 'y' && secondcoor == 'z') {part_imx = pos[i].y; part_imy = pos[i].z;}
        else {cout << "ERROR in mark_region_members" << endl; exit(1);}

        if (pow(part_imx-regions.at(reg).imx, 2)+pow(part_imy-regions.at(reg).imy, 2) < pow(regions.at(reg).par1,2))
          member_particle[i] = false;
      }
    }
  }
}

void halo::get_matter_profile (int binnum, double radius)
{
  double r, binwidth = radius/binnum;
  int binindex;

  delete[] matter_mass_profile;
  delete[] matter_cum_mass_profile;
  delete[] matter_density_profile;

  matter_mass_profile = new double[binnum];
  matter_cum_mass_profile = new double[binnum];
  matter_density_profile = new double[binnum];
  for (int i=0; i<binnum; ++i)
  {
    matter_mass_profile[i] = 0;
    matter_cum_mass_profile[i] = 0;
    matter_density_profile[i] = 0;
  }

  int i=0;
  for(int k=0; k<6; k++)
  for(int j=0;j<nall[k];j++)
  {
    r = sqrt((pos[i].x-centerx)*(pos[i].x-centerx)+
             (pos[i].y-centery)*(pos[i].y-centery)+
             (pos[i].z-centerz)*(pos[i].z-centerz));

    binindex = (int) floor(r/binwidth);
    assert(binindex>=0);
    if (binindex<binnum) matter_mass_profile[binindex] += mass[i];
 
    ++i;
  }
  
  double cum_mass = 0;
  for (int i=0; i<binnum; ++i)
  {
    cum_mass += matter_mass_profile[i];
    matter_cum_mass_profile[i] = cum_mass;
    matter_density_profile[i] = matter_mass_profile[i]
         /(4.0*pi/3.0*((i+1)*(i+1)*(i+1)-i*i*i)*binwidth*binwidth*binwidth);
  }

  matter_profile_binnum = binnum;
  matter_profile_radius = radius;
}

void halo::save_matter_profile ()
{
  ofstream f((output_base+"matter_profile.txt").c_str());
  
  double binwidth = matter_profile_radius/matter_profile_binnum;
  
  assert(matter_mass_profile!=0 && matter_cum_mass_profile!=0 && matter_density_profile!=0);
  for (int i=0; i<matter_profile_binnum; ++i)
  {
    f << i << "   " << i*binwidth << "   " << (i+0.5)*binwidth << "   " << (i+1)*binwidth << "   "
      << matter_mass_profile[i] << "   " << matter_cum_mass_profile[i] << "   " 
      << matter_density_profile[i] << endl;
  }
  f.close();
}

double halo::get_radius_for_contrast (double contrast, string ref) // find radius for specific overdensity
{
  assert(matter_cum_mass_profile!=0 && matter_profile_binnum>0);
  
  double *mean_density = new double[matter_profile_binnum];
  double binwidth = matter_profile_radius/matter_profile_binnum; 
 
  for (int i=0; i<matter_profile_binnum; ++i)
  {
    mean_density[i] = matter_cum_mass_profile[i]/(4.0*pi/3.0*pow((i+1)*binwidth,3));
    //cout << "matter_cum_mass_profile[i] = " << matter_cum_mass_profile[i] << endl;
    //cout << "matter_mass_profile[i] = " << matter_mass_profile[i] << endl;
  }
  
  double rho_thres;
  if (ref=="mean") rho_thres = rho_mean*contrast;
  else if (ref=="crit") rho_thres = rho_crit*contrast;
  else if (ref=="crit_today") rho_thres = rho_crit_today*contrast;
  else {cout << "ERROR: undefined reference density" << endl; exit(1);}
  cout << "rho_thres = " << rho_thres << endl;
  
  double radius;
  int radii_found=0;
  for (int i=1; i<matter_profile_binnum; ++i)
    if (mean_density[i-1]>rho_thres && mean_density[i]<=rho_thres)
    {
      radius = ((mean_density[i-1]-rho_thres)/(mean_density[i-1]-mean_density[i])+i)*binwidth;
      ++radii_found;
    }
  if (radii_found==1) {cout << "r_" << contrast << "_" << ref << " = " << radius << endl; return radius;}
  else if (radii_found==0) {cout << "ERROR: no radius for given density contrast found" << endl; return 0;}
  else {cout << "ERROR: multiple radii for given density contrast found" << endl; return 0;}
}

void halo::get_xray_profile (int binnum, double radius)
{
  double r, binwidth = radius/binnum;
  int binindex;

  delete[] luminosity_profile;
  delete[] cum_luminosity_profile;
  delete[] luminosity_density_profile;

  luminosity_profile = new double[binnum];
  cum_luminosity_profile = new double[binnum];
  luminosity_density_profile = new double[binnum];
  for (int i=0; i<binnum; ++i)
  {
    luminosity_profile[i] = 0;
    cum_luminosity_profile[i] = 0;
    luminosity_density_profile[i] = 0;
  }

  for(int j=0;j<nall[0];j++)
  {
    r = sqrt((pos[j].x-centerx)*(pos[j].x-centerx)+
             (pos[j].y-centery)*(pos[j].y-centery)+
             (pos[j].z-centerz)*(pos[j].z-centerz));

    binindex = (int) floor(r/binwidth);
    assert(binindex>=0);
    if (binindex<binnum) luminosity_profile[binindex] += xray_flux[j]*4*pi*D_lum*D_lum;   
  }
  
  double cum_luminosity = 0;
  for (int i=0; i<binnum; ++i)
  {
    cum_luminosity += luminosity_profile[i];
    cum_luminosity_profile[i] = cum_luminosity;
    luminosity_density_profile[i] = luminosity_profile[i]
         /(4.0*pi/3.0*((i+1)*(i+1)*(i+1)-i*i*i)*binwidth*binwidth*binwidth);
  }

  xray_profile_binnum = binnum;
  xray_profile_radius = radius;
}

void halo::save_xray_profile ()
{
  ofstream f((output_base+"xray_profile.txt").c_str());
  
  double binwidth = xray_profile_radius/xray_profile_binnum;
  
  assert(luminosity_profile!=0 && cum_luminosity_profile!=0 && luminosity_density_profile!=0);
  for (int i=0; i<xray_profile_binnum; ++i)
  {
    f << i << "   " << i*binwidth << "   " << (i+0.5)*binwidth << "   " << (i+1)*binwidth << "   "
      << luminosity_profile[i] << "   " << cum_luminosity_profile[i] << "   " 
      << luminosity_density_profile[i] << endl;
  }
  f.close();
}

double halo::get_luminosity_for_radius (double inner_radius, double outer_radius)
{
  assert(xray_profile_binnum!=0);
  assert(xray_profile_radius!=0);
  assert(inner_radius<outer_radius);

  double binwidth = xray_profile_radius/xray_profile_binnum;
  double L_inner, L_outer;
  int i_inner = (int) floor(inner_radius/binwidth);
  int i_outer = (int) floor(outer_radius/binwidth);
  assert(i_inner>=0 && i_outer<xray_profile_binnum);

  double smaller_cum_luminosity;
  
  if (i_inner>0) smaller_cum_luminosity = cum_luminosity_profile[i_inner-1];
  else smaller_cum_luminosity = 0;
  L_inner = (i_inner + 1 - inner_radius/binwidth)*smaller_cum_luminosity +
            (inner_radius/binwidth - i_inner)*cum_luminosity_profile[i_inner];

  if (i_outer>0) smaller_cum_luminosity = cum_luminosity_profile[i_outer-1];
  else smaller_cum_luminosity = 0;
  L_outer = (i_outer + 1 - outer_radius/binwidth)*smaller_cum_luminosity +
            (outer_radius/binwidth - i_outer)*cum_luminosity_profile[i_outer];
  
  double lum = L_outer - L_inner;

  cout << "luminosity between radii " << inner_radius << " (L = " << L_inner << ") and " 
                                      << outer_radius << " (L = " << L_outer << ") = " << lum << endl;

  return lum;  
}

double halo::get_temperature_for_radius (string type, double inner_radius, double outer_radius,
                                         bool use_projected_radii, bool members_only)
{
  assert(type=="m" || type=="ew" || type=="sl" || type=="ew_simple" || type=="ew_simple_0.5_10");
  double weight = 0, weighted_temp = 0, r;
  double alpha_sl = 0.75;
  bool member;

  for(int j=0;j<nall[0];j++)
  {
    if (members_only && member_particle!=0) member = member_particle[j];
    else member = true;
    
    if (!use_projected_radii) r = sqrt((pos[j].x-centerx)*(pos[j].x-centerx)+
                                       (pos[j].y-centery)*(pos[j].y-centery)+
                                       (pos[j].z-centerz)*(pos[j].z-centerz));
    else r = sqrt((pos[j].y-centery)*(pos[j].y-centery)+
                  (pos[j].z-centerz)*(pos[j].z-centerz));
    
    if (r>=inner_radius && r<=outer_radius && xray_flux_status[j]<2 && member)
    {
      if (type == "m")
      {
        weight += mass[j];
        weighted_temp += mass[j]*temp[j];
      }
      else if (type == "ew")
      {
        weight += xray_flux[j];
        weighted_temp += xray_flux[j]*temp[j];
      }
      else if (type == "ew_simple")
      {
        weight += mass[j]*rho[j]*sqrt(temp[j]);
        weighted_temp += mass[j]*rho[j]*sqrt(temp[j])*temp[j];
      }
      else if (type == "ew_simple_0.5_10")
      {
        weight += mass[j]*rho[j]*sqrt(temp[j])*(exp(-0.5/temp[j])-exp(-10.0/temp[j]));
        weighted_temp += mass[j]*rho[j]*sqrt(temp[j])*temp[j]*(exp(-0.5/temp[j])-exp(-10.0/temp[j]));
      }
      else if (type == "sl")
      {
        weight += mass[j]*rho[j]*pow(double(temp[j]),alpha_sl-1.5);
        weighted_temp += mass[j]*rho[j]*pow(double(temp[j]),alpha_sl-0.5);
      }  
    }
  }
 
  if (weight==0)
  {
    cout << "ERROR: could not get averaged temperature" << endl;
    return 0;
  }
 
  if (type=="sl" && !cooling_branch_excluded) 
    cout << "WARNING: very cold particles included in t_sl computation" << endl; 
 
  weighted_temp /= weight;
  cout << "t_" << type << " in " << inner_radius << " < r < " << outer_radius << " = " << weighted_temp << endl;
  return weighted_temp;
}

double halo::get_mean_temperature ()
{
  double weight = 0, weighted_temp = 0;

  for(int j=0;j<nall[0];j++)
  {
    weight += mass[j];
    weighted_temp += mass[j]*temp[j];
  }
 
  if (weight==0)
  {
    cout << "ERROR: could not get mean temperature" << endl;
    return 0;
  }
 
  weighted_temp /= weight;
  cout << "mean gas mass weighted temperature in box is " << weighted_temp << " keV or " 
       << weighted_temp/1.3806503e-23*1.602176462e-16 << " Kelvin" << endl;
  return weighted_temp;
}
 
void halo::get_seperate_profiles(int binnum, double radius)
{
  double r, binwidth = radius/binnum;
  int binindex;

  delete[] seperate_mass_profiles;
  delete[] seperate_cum_mass_profiles;
  delete[] seperate_density_profiles;

  seperate_mass_profiles = new double[binnum*6];
  seperate_cum_mass_profiles = new double[binnum*6];
  seperate_density_profiles = new double[binnum*6];
  for (int k=0; k<6; ++k) // k is particle species
  for (int i=0; i<binnum; ++i)
  {
    seperate_mass_profiles[binnum*k+i] = 0;
    seperate_cum_mass_profiles[binnum*k+i] = 0;
    seperate_density_profiles[binnum*k+i] = 0;
  }

  int i=0;
  for(int k=0; k<6; ++k)
  for(int j=0; j<nall[k]; ++j)
  {
    r = sqrt((pos[i].x-centerx)*(pos[i].x-centerx)+
             (pos[i].y-centery)*(pos[i].y-centery)+
             (pos[i].z-centerz)*(pos[i].z-centerz));

    binindex = (int) floor(r/binwidth);
    assert(binindex>=0);
    if (binindex<binnum) seperate_mass_profiles[binnum*k+binindex] += mass[i];   
 
    ++i;
  }
  
  for(int k=0; k<6; ++k)
  {
    double cum_mass = 0;
    for (int i=0; i<binnum; ++i)
    {
      cum_mass += seperate_mass_profiles[binnum*k+i];
      seperate_cum_mass_profiles[binnum*k+i] = cum_mass;
      seperate_density_profiles[binnum*k+i] = seperate_mass_profiles[binnum*k+i]
           /(4.0*pi/3.0*((i+1)*(i+1)*(i+1)-i*i*i)*binwidth*binwidth*binwidth);
    }
  }

  seperate_profiles_binnum = binnum;
  seperate_profiles_radius = radius;
}

void halo::save_seperate_profiles (string filename = "none", double ref_radius = 0)
{
  ofstream f;
  if (filename=="none") f.open((output_base+"seperate_profiles.txt").c_str());
  else f.open(filename.c_str());
  
  double binwidth = seperate_profiles_radius/seperate_profiles_binnum;
  
  assert(seperate_mass_profiles!=0 && seperate_cum_mass_profiles!=0 && seperate_density_profiles!=0);
  for (int i=0; i<seperate_profiles_binnum; ++i)
  {
    f << i << "   " << i*binwidth << "   " << (i+0.5)*binwidth << "   " << (i+1)*binwidth;
    if (ref_radius>0) f << "   " << (i+0.5)*binwidth/ref_radius; 
    for (int k=0; k<6; ++k)
      f << "   " << seperate_mass_profiles[seperate_profiles_binnum*k+i] 
        << "   " << seperate_cum_mass_profiles[seperate_profiles_binnum*k+i] 
        << "   " << seperate_density_profiles[seperate_profiles_binnum*k+i];
    f << endl;
  }
  f.close();
}

double halo::get_matter_type_mass (int type, double inner_radius, double outer_radius)
{
  //assert(seperate_profiles_binnum!=0 && seperate_profiles_radius!=0 && inner_radius<outer_radius && type>=0 && type<6);
  assert(seperate_profiles_binnum!=0);
  assert(seperate_profiles_radius!=0);
  assert(inner_radius<=outer_radius);
  assert(type>=0 && type<6);

  double binwidth = seperate_profiles_radius/seperate_profiles_binnum;
  double m_inner, m_outer;
  int i_inner = (int) floor(inner_radius/binwidth);
  int i_outer = (int) floor(outer_radius/binwidth);
  assert(i_inner>=0 && i_outer<seperate_profiles_binnum);

  double smaller_cum_mass;
  
  if (i_inner>0) smaller_cum_mass = seperate_cum_mass_profiles[seperate_profiles_binnum*type+i_inner-1];
  else smaller_cum_mass = 0;
  m_inner = (i_inner + 1 - inner_radius/binwidth)*smaller_cum_mass +
            (inner_radius/binwidth - i_inner)*seperate_cum_mass_profiles[seperate_profiles_binnum*type+i_inner];

  if (i_outer>0) smaller_cum_mass = seperate_cum_mass_profiles[seperate_profiles_binnum*type+i_outer-1];
  else smaller_cum_mass = 0;
  m_outer = (i_outer + 1 - outer_radius/binwidth)*smaller_cum_mass +
            (outer_radius/binwidth - i_outer)*seperate_cum_mass_profiles[seperate_profiles_binnum*type+i_outer];
  
  double mass = m_outer - m_inner;

  cout << "mass of matter type " << type << " between radii " << inner_radius << " (m = " << m_inner << ") and " 
                                      << outer_radius << " (m = " << m_outer << ") = " << mass << endl;

  return mass;  
}

void halo::get_temperature_entropy_profiles (int binnum, double radius)
{
  double r, binwidth = radius/binnum;
  int binindex;

  delete[] temperature_profile;
  delete[] entropy_profile;

  temperature_profile = new double[binnum];
  entropy_profile = new double[binnum];
  double *temp_mass_profile = new double[binnum];

  for (int i=0; i<binnum; ++i)
  {
    temperature_profile[i] = 0;
    entropy_profile[i] = 0;
    temp_mass_profile[i] = 0;
  }

  int i=0;
  for(int k=0; k<1; k++)
  for(int j=0; j<nall[k]; j++)
  {
    r = sqrt((pos[i].x-centerx)*(pos[i].x-centerx)+
             (pos[i].y-centery)*(pos[i].y-centery)+
             (pos[i].z-centerz)*(pos[i].z-centerz));

    binindex = (int) floor(r/binwidth);
    assert(binindex>=0);
    if (binindex<binnum)
    { 
      temperature_profile[binindex] += temp[i]*mass[i];
      entropy_profile[binindex] += temp[i]/(pow(double(rho[i]*ne[i]),2.0/3.0))*mass[i];
      temp_mass_profile[binindex] += mass[i];
    }   

    ++i;
  }

  for (int i=0; i<binnum; ++i)
    if (temp_mass_profile[i]>0) 
    {
      temperature_profile[i] /= temp_mass_profile[i];
      entropy_profile[i] /= temp_mass_profile[i];
    }

  delete[] temp_mass_profile;

  temperature_entropy_profile_binnum = binnum;
  temperature_entropy_profile_radius = radius;
}

void halo::save_temperature_entropy_profiles (string filename = "none", double ref_radius = 0)
{
  ofstream f;
  if (filename=="none") f.open((output_base+"temperature_entropy_profiles.txt").c_str());
  else f.open(filename.c_str());  

  double binwidth = temperature_entropy_profile_radius/temperature_entropy_profile_binnum;
  
  assert(temperature_profile!=0 && entropy_profile!=0);
  for (int i=0; i<temperature_entropy_profile_binnum; ++i)
  {
    f << i << "   " << i*binwidth << "   " << (i+0.5)*binwidth << "   " << (i+1)*binwidth << "   ";
    if (ref_radius>0) f << (i+0.5)*binwidth/ref_radius << "   "; 
    f << temperature_profile[i] << "   " << entropy_profile[i] << endl;
  }
  f.close();
}

void halo::get_metallicity_profile (int binnum, double radius)
{
  assert(z!=0);

  double r, binwidth = radius/binnum;
  int binindex;

  delete[] metallicity_profile;

  metallicity_profile = new double[binnum];
  double *temp_mass_profile = new double[binnum];
  double mass_total = 0;
  int N = 0;
  int *inside = new int[nall[0]];

  for (int i=0; i<binnum; ++i)
  {
    metallicity_profile[i] = 0;
    temp_mass_profile[i] = 0;
  }

  int i=0;
  for(int k=0; k<1; k++)
  for(int j=0;j<nall[k];j++)
  {
    inside[j] = 0;
    r = sqrt((pos[i].x-centerx)*(pos[i].x-centerx)+
             (pos[i].y-centery)*(pos[i].y-centery)+
             (pos[i].z-centerz)*(pos[i].z-centerz));

    binindex = (int) floor(r/binwidth);
    assert(binindex>=0);
    if (binindex<binnum)
    { 
      metallicity_profile[binindex] += z[i]*mass[i];
      temp_mass_profile[binindex] += mass[i];

      mean_metallicity += z[i]*mass[i];
      mass_total += mass[i];
      inside[j] = 1;
      N++;
    }
 
    ++i;
  }
  mean_metallicity /= mass_total;
  cout << "Mean abundance is " << mean_metallicity << endl;

  /// Median metallicity
  vector<double> z_temp(N);
  int x = 0;
  for(int j=0;j<nall[0];j++)
  {
    if(inside[j] == 1) {
      z_temp[x] = z[j];
      x++;
      //cout << "z_temp[" << x << "] = " << z_temp[x] << endl;
      //cout << "z[" << j << "] = " << z[j] << endl;
     }
  }
  size_t z_ind = z_temp.size() / 2;
  nth_element(z_temp.begin(), z_temp.begin()+z_ind, z_temp.end());
  median_metallicity = z_temp[z_ind];
  cout << "Median abundance is " << median_metallicity << endl;
  
  for (int i=0; i<binnum; ++i) if (temp_mass_profile[i]>0) metallicity_profile[i] /= temp_mass_profile[i];

  delete[] temp_mass_profile;

  metallicity_profile_binnum = binnum;
  metallicity_profile_radius = radius;
}

void halo::save_metallicity_profile ()
{
  ofstream f((output_base+"metallicity_profile.txt").c_str());
  
  double binwidth = metallicity_profile_radius/metallicity_profile_binnum;
  
  assert(metallicity_profile!=0);
  for (int i=0; i<metallicity_profile_binnum; ++i)
  {
    f << i << "   " << i*binwidth << "   " << (i+0.5)*binwidth << "   " << (i+1)*binwidth << "   " 
      << metallicity_profile[i] << "   " // mass fraction in simulation as in Z data block
      << metallicity_profile[i]/(1-metallicity_profile[i])*(ag_hydrogen+ag_helium)/ag_metals << endl; // xspec metal abundance
  }
  f.close();
}
void halo::get_hsml_profile (int binnum, double radius)
{
  assert(z!=0);

  double r, binwidth = radius/binnum;
  int binindex;

  delete[] hsml_profile;

  hsml_profile = new double[binnum];
  double *temp_mass_profile = new double[binnum];

  for (int i=0; i<binnum; ++i)
  {
    hsml_profile[i] = 0;
    temp_mass_profile[i] = 0;
  }

  int i=0;
  for(int k=0; k<1; k++)
  for(int j=0;j<nall[k];j++)
  {
    r = sqrt((pos[i].x-centerx)*(pos[i].x-centerx)+
             (pos[i].y-centery)*(pos[i].y-centery)+
             (pos[i].z-centerz)*(pos[i].z-centerz));

    binindex = (int) floor(r/binwidth);
    assert(binindex>=0);
    if (binindex<binnum)
    { 
      hsml_profile[binindex] += hsml[i]*mass[i];
      temp_mass_profile[binindex] += mass[i];
    }   
 
    ++i;
  }
  
  for (int i=0; i<binnum; ++i) if (temp_mass_profile[i]>0) hsml_profile[i] /= temp_mass_profile[i];

  delete[] temp_mass_profile;

  hsml_profile_binnum = binnum;
  hsml_profile_radius = radius;
}

void halo::save_hsml_profile ()
{
  ofstream f((output_base+"hsml_profile.txt").c_str());
  
  double binwidth = hsml_profile_radius/hsml_profile_binnum;
  
  assert(hsml_profile!=0);
  for (int i=0; i<hsml_profile_binnum; ++i)
  {
    f << i << "   " << i*binwidth << "   " << (i+0.5)*binwidth << "   " << (i+1)*binwidth << "   " 
      << hsml_profile[i] << endl;
  }
  f.close();
}

void halo::show_properties (bool to_file)
{
  ostream *out;
  ofstream file;
  if (to_file) {file.open((output_base+"properties.txt").c_str()); out = &file;}
  else out = &cout;
  
  if (!to_file) *out << "-------------------------------------" << endl;
  *out << "HALO PROPERTIES" << endl << endl;

  cout << "redshift = " << redshift << endl;
  cout << "angular diameter distance [h^-1 kpc] = " << DA << endl;
  cout << "luminosity distance [cm] = " << D_lum << endl;

  if (have_subf_halo_info)
  {
    *out << endl << "SUBFIND output for group " << subf_info_group << ": " << endl << endl;
    *out << "particles in group = " << subf_Len << endl;
    *out << "FOF mass = " << subf_Mass << endl << endl; 
    *out << "group x = " << subf_Pos.x << endl;
    *out << "group y = " << subf_Pos.y << endl; 
    *out << "group z = " << subf_Pos.z << endl << endl; 
    *out << "m_mean_200 = " << subf_M_Mean200 << endl;
    *out << "r_mean_200 = " << subf_R_Mean200 << endl; 
    *out << "m_crit_200 = " << subf_M_Crit200 << endl;
    *out << "r_crit_200 = " << subf_R_Crit200 << endl; 
    *out << "m_tophat_200 = " << subf_M_TopHat200 << endl; 
    *out << "r_tophat_200 = " << subf_R_TopHat200 << endl << endl; 
    *out << "contamining particles = " << subf_ContaminationCount << endl; 
    *out << "contamining mass = " << subf_ContaminationMass << endl;
    *out << "number of sub groups = " << subf_Nsubs << endl;
    *out << "first subhalo = " << subf_FirstSub << endl;
  }
  if (ids_num!=0) *out << "particles in subhalo " << subhalo_num << ": " << ids_num << endl;

  if (have_fofs_halo_info)
  {
    *out << endl << "FoF special output for group " << fofs_info_group << ": " << endl << endl;
    *out << "group x = " << fofs_Pos.x << endl;
    *out << "group y = " << fofs_Pos.y << endl; 
    *out << "group z = " << fofs_Pos.z << endl;
    *out << "t_ew_200 = " << fofs_temp_ew_200 << endl;
    *out << "t_ew_200 [keV] = " << fofs_temp_ew_200*1.3806503e-23/1.602176462e-16 << endl;
  }
  if (!to_file) *out << "-------------------------------------" << endl;

  if (to_file) file.close(); 
}

int halo::make_mass_map(int gridsize, double sidelength, string filename = "none")
{
  assert(pos!=0);
  assert(mass!=0);

  double gridconst = sidelength/gridsize;
  double ypos, zpos; 
  int count = 0;

  double *massinpix = new double[gridsize*gridsize];
  for (int i = 0; i<gridsize*gridsize; ++i) massinpix[i] = 0;
 
  int part = 0;
  for(int k=0; k<6; ++k)
  for(int j=0; j<nall[k]; ++j)
  {   
    if (k!=2 && k!=3)
    if (pos[j].y+gridconst>centery-sidelength/2 && pos[j].y-gridconst<centery+sidelength/2 &&
        pos[j].z+gridconst>centerz-sidelength/2 && pos[j].z-gridconst<centerz+sidelength/2)
    {
      ypos = (gridsize-1)*(pos[j].y-(centery-sidelength/2))/sidelength;
      zpos = (gridsize-1)*(pos[j].z-(centerz-sidelength/2))/sidelength;
  
      count += pg::put_cloud(massinpix, gridsize, mass[part], ypos, zpos);
    }
    ++part;
  }

  if (filename=="none") filename = output_base+"mass_map.fit";
  write_fits<double,float>(filename, gridsize, gridsize, massinpix,
    centery-sidelength/2, centerz-sidelength/2, sidelength/(gridsize-1), sidelength/(gridsize-1));
 
  delete[] massinpix;

  return count;
}

//make_xray_maps(512,3000,true,2);
int halo::make_xray_maps(int gridsize, double sidelength, bool members_only, int save_densities_and_temperatures = 0)
{
  assert(pos!=0);
  assert(xray_flux!=0 && xray_flux_status!=0);

  double *flux = new double[gridsize*gridsize];
  double *lum = new double[gridsize*gridsize];
  for (int i=0; i<gridsize*gridsize; ++i) {flux[i]=0; lum[i]=0;}

  double *int_tab;
  read_fits("d_spline_integral_table.fit", int_tab);
  double *pix_tab = new double[(gridsize+1)*(gridsize+1)];
  for (int i=0; i<(gridsize+1)*(gridsize+1); ++i) pix_tab[i]=0;

  double ypos, zpos, progress = 0;
  int xray_count = 0, out_of_range = 0;

  ofstream rhotemp, rhotemp_excluded, rhotemp_all;
  if (save_densities_and_temperatures > 0)
  {
    rhotemp.open((output_base+"rhotemp.txt").c_str());
    rhotemp_excluded.open((output_base+"rhotemp_excluded.txt").c_str());
  }
  if (save_densities_and_temperatures > 1)
    rhotemp_all.open((output_base+"rhotemp_all.txt").c_str());

  bool member;

  for(int j=0; j<nall[0]; ++j)
  {
    if (members_only && member_particle!=0) member = member_particle[j];
    else member = true;
    
    if (pos[j].y+hsml[j]>centery-sidelength/2 && pos[j].y-hsml[j]<centery+sidelength/2 &&
        pos[j].z+hsml[j]>centerz-sidelength/2 && pos[j].z-hsml[j]<centerz+sidelength/2 &&
        member)
    {
      if (xray_flux_status[j]<2)
      {
        ypos = (gridsize-1)*(pos[j].y-(centery-sidelength/2))/sidelength;
        zpos = (gridsize-1)*(pos[j].z-(centerz-sidelength/2))/sidelength;
  
        xray_count += pg::put_function(int_tab, 1024, flux, gridsize, gridsize, xray_flux[j], ypos, zpos,
                                    hsml[j]/sidelength*(gridsize-1), pix_tab);
    
        if (xray_flux_status[j]==1) ++out_of_range;

        if (save_densities_and_temperatures > 0) rhotemp << rho[j] << "   " << temp[j] << endl;
      }
      else if (save_densities_and_temperatures > 0) rhotemp_excluded << rho[j] << "   " << temp[j] << endl;
    }
    if (save_densities_and_temperatures > 1) rhotemp_all << rho[j] << "   " << temp[j] << endl;
   
    if ((j-progress*nall[0])/double(nall[0]) > 0.01)
    {  
      progress += 0.01; 
      cout << int(progress*100) << " % of gas particles put on grid" << endl;
    }
  }
  cout << out_of_range << " of " << xray_count << " gas particles out of T,Z range" << endl;
  
  delete[] int_tab;
  delete[] pix_tab;

  if (save_densities_and_temperatures > 0)
  {
    rhotemp.close();
    rhotemp_excluded.close();
  }
  if (save_densities_and_temperatures > 1)
    rhotemp_all.close();

  write_fits<double,float>(output_base+"xray_flux.fit", gridsize, gridsize, flux,
    centery-sidelength/2, centerz-sidelength/2, sidelength/(gridsize-1), sidelength/(gridsize-1));

  for (int i=0; i<gridsize*gridsize; ++i) lum[i] = flux[i]*4*pi*D_lum*D_lum/1e40; //in 10^40 ergs/s
  write_fits<double,float>(output_base+"xray_lum.fit", gridsize, gridsize, lum,
    centery-sidelength/2, centerz-sidelength/2, sidelength/(gridsize-1), sidelength/(gridsize-1));
 
  delete[] flux;
  delete[] lum;

  return xray_count;
}

void halo::write_xspec_model_file (double inner_radius, double outer_radius, 
                                   bool use_projected_radii, bool members_only,
                                   bool exclude_cooling_branch, bool use_multiphase, bool use_sim_metallicities, 
                                   bool fit, bool use_apec, bool ignore_outside_range, double &t_spec, double &abund_spec, double &norm_spec, double &lum_spec,
                                   string outfile_suffix = "")
{
  int outside_range = 0, inside_radial_range = 0;
  bool binned;
  
  double mintemp = 0.1, maxtemp = 24.0; 
  if (use_apec) {mintemp = 0.01;} // apec has min. kT of 0.008 keV but mekal has min. 0.08
  int numtemp = 1024;
  double minabund = 0.3, maxabund = 0.35;
  int numabund = 2; 
  double curtemp, curabund, curnorm;
  int parttemp, partabund;
  double dparttemp, dpartabund;

  double hydrogen_mass_fraction, xspec_metallicity, nelec, temperature, hot_fraction, r;

  if (use_multiphase) assert(mhi!=0 && hot_phase_temp!=0);
  if (use_sim_metallicities) {
    assert(z!=0);
    minabund = 0.0;
    maxabund = 1.0;
    numabund = 21;
  }
  else if (strcmp(metals_method.c_str(), "median") == 0) {
    minabund = median_metallicity/(1-median_metallicity)*(ag_hydrogen+ag_helium)/ag_metals;
    maxabund = median_metallicity/(1-median_metallicity)*(ag_hydrogen+ag_helium)/ag_metals + 0.05;//doesn't matter what this is because xspec_metallicity is set to constant
  }
  else if (strcmp(metals_method.c_str(), "mean") == 0) {
    minabund = mean_metallicity/(1-mean_metallicity)*(ag_hydrogen+ag_helium)/ag_metals;
    maxabund = mean_metallicity/(1-mean_metallicity)*(ag_hydrogen+ag_helium)/ag_metals + 0.05;
  }

  double norm[numtemp][numabund]; //MEKAL (and APEC) norm
  for (int i=0; i<numtemp; ++i)
    for (int j=0; j<numabund; ++j) norm[i][j] = 0;

  bool member;

  for (int i=0; i<nall[0]; ++i)
  {
    if (members_only && member_particle!=0) member = member_particle[i];
    else member = true;
    
    if (!sfr || ((temp[i]>=3e4*8.617342e-8 || rho[i]<=500*0.044*2.77536627e-8 || !exclude_cooling_branch)
                 && (rho[i] < 0.00085483/((1+redshift)*(1+redshift)*(1+redshift)) || use_multiphase))) 
    //is the rho threshold 500*0.044*2.77536627e-8 right ???
    //temp threshold ???
    // 3e4*8.617342e-8 = 3 keV
    //exclude_cooling_branch ???
    //use multiphase particles ???
    {      
      if (!use_projected_radii) r = sqrt((pos[i].x-centerx)*(pos[i].x-centerx)+
                                         (pos[i].y-centery)*(pos[i].y-centery)+
                                         (pos[i].z-centerz)*(pos[i].z-centerz));
      else r = sqrt((pos[i].y-centery)*(pos[i].y-centery)+
                    (pos[i].z-centerz)*(pos[i].z-centerz));
      
      if (r>=inner_radius && r<=outer_radius && member)
      {    
//         // old parameters
//         if (z!=0) hydrogen_mass_fraction = (1-z[i])*f;
//         else hydrogen_mass_fraction = f;
//         if (ne!=0) nelec = ne[i];
//         else nelec = 1.0 + (1.0-f)/(2.0*f);
//         xspec_metallicity = 0.3;

        // more accurate parameters
        if (use_sim_metallicities) {
          xspec_metallicity = z[i]/(1-z[i])*(ag_hydrogen+ag_helium)/ag_metals;
        }
        else if (strcmp(metals_method.c_str(), "median") == 0) {
	  xspec_metallicity = median_metallicity/(1-median_metallicity)*(ag_hydrogen+ag_helium)/ag_metals;
          //NB: Setting the metallicity to constant puts all particles in the same abundance bin...
	}
        else if (strcmp(metals_method.c_str(), "mean") == 0) {
          xspec_metallicity = mean_metallicity/(1-mean_metallicity)*(ag_hydrogen+ag_helium)/ag_metals;
        }
        else xspec_metallicity = 0.3;//NB: Setting the metallicity to constant puts all particles in the same abundance bin
        hydrogen_mass_fraction = ag_hydrogen/(ag_hydrogen+ag_helium+xspec_metallicity*ag_metals);
        nelec = 1.0 + 0.5*ag_helium/ag_hydrogen + 0.5*xspec_metallicity*ag_metals/ag_hydrogen;//1.198 for 0.3 solar metallicity, 1.197 for 0.2
        if (ne!=0) nelec *= ne[i]/(1.0+(1.0-f)/(2.0*f));
        // (1.0+(1.0-f)/(2.0*f)) is rough approximation of ne2nh assuming full ionization and neglecting metallicity

        if (use_multiphase)
        {
          hot_fraction = 1.0 - mhi[i]/(f*mass[i]);
          //if (hot_fraction < 0.9) hot_fraction = 0.0;
          temperature = hot_phase_temp[i];
        }
        else 
        {
          temperature = temp[i];
          hot_fraction = 1.0;
        }

        parttemp = (int) floor((temperature-mintemp)/(maxtemp-mintemp)*(numtemp-1));//which temp bin
        dparttemp = (temperature-mintemp)/(maxtemp-mintemp)*(numtemp-1) - parttemp;//distance into temp bin
        partabund = (int) floor((xspec_metallicity-minabund)/(maxabund-minabund)*(numabund-1)); //constant metallicity assumed
        dpartabund = (xspec_metallicity-minabund)/(maxabund-minabund)*(numabund-1) - partabund; 

/*
        if (parttemp<0) {parttemp=0; dparttemp=0; ++outside_range;}//temp is below mintemp
        if (partabund<0) {partabund=0; dpartabund=0; if (parttemp>=0) ++outside_range;}//abund below minabund
        if (parttemp>=numtemp-1) {parttemp=numtemp-1; dparttemp=0; ++outside_range;}//temp is above max
        if (partabund>=numabund-1) {partabund=numabund-1; dpartabund=0; if (parttemp<numtemp-1) ++outside_range;}
*/
        binned = true;
        if (parttemp<0) {parttemp=0; dparttemp=0; ++outside_range; binned=false;}//temp is below mintemp
        if (partabund<0) {partabund=0; dpartabund=0; if (parttemp>=0) ++outside_range; binned=false;}//abund below minabund
        if (parttemp>=numtemp-1) {parttemp=numtemp-2; dparttemp=1; ++outside_range; binned=false;}//temp is above max
        if (partabund>=numabund-1) {partabund=numabund-2; dpartabund=1; if (parttemp<numtemp-1) ++outside_range; binned=false;}

        /// Calculate MEKAL norm for individual particle - sum over volume of product of electron and hydrogen number densities - see https://heasarc.gsfc.nasa.gov/xanadu/xspec/manual/XSmodelMekal.html
        curnorm = 5.05216e12*mass[i]*rho[i]*hubble*hubble*hubble //see mekal_norm.nb for constant - it is 10^-14*10^20*(1.98844*10^30)^2/((1.67262171*10^-27)^2*(3.0856775807*10^21)^5)
                  // Note: hubble*hubble*hubble because DA is in units of h^-1 kpc
                  *hydrogen_mass_fraction*hydrogen_mass_fraction*hot_fraction*hot_fraction*nelec*(1+redshift)/(4.0*pi*DA*DA);
        /// number density of Hydrogen is approx hydrogen_mass_fraction*rho/proton_mass
        /// electron number density is the hydrogen number density times nelec
      

        /// Project onto grid of points
        if (!ignore_outside_range) {binned = true;}
        if (binned) {
          norm[parttemp][partabund] += curnorm*(1-dparttemp)*(1-dpartabund);
          norm[parttemp+1][partabund] += curnorm*dparttemp*(1-dpartabund);
          norm[parttemp][partabund+1] += curnorm*(1-dparttemp)*dpartabund;
          norm[parttemp+1][partabund+1] += curnorm*dparttemp*dpartabund;
        }

	/*
        if (dparttemp <= 0.5 && dpartabund <= 0.5) {
          norm[parttemp][partabund] += curnorm*(0.5+dparttemp)*(0.5+dpartabund);
          if (parttemp>0) norm[parttemp-1][partabund] += curnorm*(0.5-dparttemp)*(0.5+dpartabund);
          if (partabund>0) norm[parttemp][partabund-1] += curnorm*(0.5+dparttemp)*(0.5-dpartabund);
          if (parttemp>0 && partabund>0) norm[parttemp-1][partabund-1] += curnorm*(0.5-dparttemp)*(0.5-dpartabund);
        }
        if (dparttemp > 0.5 && dpartabund > 0.5) {
          norm[parttemp][partabund] += curnorm*(1.5-dparttemp)*(1.5-dpartabund);
          norm[parttemp+1][partabund] += curnorm*(dparttemp-0.5)*(1.5-dpartabund);
          norm[parttemp][partabund+1] += curnorm*(1.5-dparttemp)*(dpartabund-0.5);
          inorm[parttemp+1][partabund+1] += curnorm*(dparttemp-0.5)*(dpartabund-0.5);
        }
        if (dparttemp <= 0.5 && dpartabund > 0.5) {
          norm[parttemp][partabund] += curnorm*(0.5+dparttemp)*(1.5-dpartabund);
          if (parttemp>0) norm[parttemp-1][partabund] += curnorm*(0.5-dparttemp)*(1.5-dpartabund);
          norm[parttemp][partabund+1] += curnorm*(0.5+dparttemp)*(dpartabund-0.5);
          if (parttemp>0) norm[parttemp-1][partabund+1] += curnorm*(0.5-dparttemp)*(dpartabund-0.5);
        }
        if (dparttemp > 0.5 && dpartabund <= 0.5) {
          norm[parttemp][partabund] += curnorm*(1.5-dparttemp)*(0.5+dpartabund);
          norm[parttemp+1][partabund] += curnorm*(dparttemp-0.5)*(0.5+dpartabund);
          if (partabund>0) norm[parttemp][partabund-1] += curnorm*(1.5-dparttemp)*(0.5-dpartabund);
          if (partabund>0) norm[parttemp-1][partabund-1] += curnorm*(dparttemp-0.5)*(0.5-dpartabund);
        }
	*/

        ++inside_radial_range;
      }
    }
  }   

  if (use_projected_radii) outfile_suffix += "_proj";
  ofstream model_file((output_base+"xspec_model"+outfile_suffix+".txt").c_str());
  model_file << "model "; 
  for (double curtempnum = 0; curtempnum < numtemp; ++curtempnum)
  for (double curabundnum = 0; curabundnum < numabund; ++curabundnum)
  {
    if (use_apec) {model_file << "apec";}
    else {model_file << "mekal";}
    if (!(curtempnum==numtemp-1 && curabundnum==numabund-1)) model_file << " + "; 
  }
  model_file << endl;
  for (int curtempnum = 0; curtempnum < numtemp; ++curtempnum)
  for (int curabundnum = 0; curabundnum < numabund; ++curabundnum)
  {
    curtemp = mintemp + (maxtemp-mintemp)/(numtemp-1.0)*curtempnum;
    curabund = minabund + (maxabund-minabund)/(numabund-1.0)*curabundnum;
    
    model_file << curtemp << endl; 
    if (!use_apec) {model_file << 1 << endl;} //nH
    model_file << curabund << endl; 
    model_file << redshift << endl; 
    if (!use_apec) {model_file << 0 << endl;} //switch
    //if (curabundnum==0) model_file << 1 << endl; //norm
    //else model_file << 0 << endl;
    model_file << norm[curtempnum][curabundnum] << endl;
  }
  model_file << endl;
  model_file.close();

  cout << "XSPEC model file written, " << outside_range << " of " << inside_radial_range 
       << " particles outside temperature or metallicity range" << endl;

  /// Remove any existing fitted_model.txt
  if (FILE *fit_file_test = fopen((output_base+"fitted_model"+outfile_suffix+".txt").c_str(), "r")) {
    system(("rm "+output_base + "fitted_model"+outfile_suffix+".txt").c_str());
    fclose(fit_file_test);
  }
  ofstream fakeit_file((output_base+"fake_spec"+outfile_suffix+".txt").c_str());
  fakeit_file << "fakeit none & rmf_pi.209.fits.gz & warf.209.fits.gz & y & & "
              << output_base << "spectrum"+outfile_suffix+".fak & 1000000" << endl;
  fakeit_file << "exit" << endl;
  fakeit_file.close();

  ofstream fit_file ((output_base+"fit_spec"+outfile_suffix+".txt").c_str());
  fit_file << "cosmo " << hubble*100 << " 0 " << omega_l << endl;
  fit_file << "query yes" << endl;
  fit_file << "data " << output_base << "spectrum"+outfile_suffix+".fak" << endl;
  //use mass-weighted temperature and Z=0.3 solar as initial guess
  if (use_apec) { fit_file << "model apec & " << get_temperature_for_radius("m",inner_radius,outer_radius,false,false)
           << " & " << 0.3 << " & " << redshift << " & 1" << endl;}
  else {fit_file << "model mekal & " << get_temperature_for_radius("m",inner_radius,outer_radius,false,false)
           << " & " << 1 << " & " << 0.3 << " & " << redshift << " & 0 & 1" << endl;}
  if (use_apec) {fit_file << "thaw 2" << endl;}
  else {fit_file << "thaw 3" << endl;}
  fit_file << "statistic cstat" << endl;
  fit_file << "ignore 1-20" << endl;//ignore detector data channels
  fit_file << "ignore 686-1024" << endl;
  fit_file << "fit 1000" << endl;
  fit_file << "save model \"" << output_base << "fitted_model"+outfile_suffix+".txt\"" << endl;
  fit_file << "dummyrsp 0.003 105.0 1000 log 0 0" << endl;
  fit_file << "flux " << 0.01/(1+redshift) << " " << 100.0/(1+redshift) << endl;
  fit_file << "tclout flux" << endl;
  fit_file << "echo $xspec_tclout > " << output_base << "fitted_flux_lum"+outfile_suffix+".txt" << endl;
  fit_file << "lumin 0.01 100 " << redshift << endl; //calculate the luminosity for rest frame energy range 0.01 to 100 keV at given redshift
  fit_file << "tclout lumin" << endl; 
  fit_file << "echo $xspec_tclout >> " << output_base << "fitted_flux_lum"+outfile_suffix+".txt" << endl;
  fit_file << "exit" << endl;

  if (fit)
  { 
    system(("xspec - " + output_base + "xspec_model"+outfile_suffix+".txt " + output_base +
            "fake_spec"+outfile_suffix+".txt >> " + 
            output_base + "xspec_fake_log"+outfile_suffix+".txt").c_str());
	cout << "fake spectrum produced" << endl;		
    system(("xspec - " + output_base + "fit_spec"+outfile_suffix+".txt >> "
           + output_base + "xspec_fit_log"+outfile_suffix+".txt").c_str());
    cout << "spectrum fitted" << endl;

    char str[1024];
    double flux, lum;

    ifstream model_file((output_base + "fitted_model"+outfile_suffix+".txt").c_str());
    for (int i=1; i<=8; ++i) model_file.getline(str,1024);
    model_file >> t_spec; // >> reads up until the first whitespace
    model_file.getline(str,1024);
    if (!(use_apec)) {model_file.getline(str,1024);}
    model_file >> abund_spec;
    model_file.getline(str,1024);//skip rest of abundance line
    model_file.getline(str,1024);//skip redshift
    if (!(use_apec)) {model_file.getline(str,1024);}//skip switch
    model_file >> norm_spec;
    model_file.close();

    cout << "t_spec = " << t_spec << endl;
    cout << "abund_spec = " << abund_spec << endl;
	  cout << "norm_spec = " << norm_spec << endl;

    ifstream lum_file((output_base + "fitted_flux_lum"+outfile_suffix+".txt").c_str());
    lum_file >> flux;
    lum_file.getline(str,1024);
    lum_file >> lum;
    lum_spec = flux*4*pi*D_lum*D_lum;

    cout << "flux_spec = " << flux << endl;
    cout << "lum_spec = " << lum_spec << " (directly from XSPEC " << lum*1e44 << ")" << endl; 
  }
  else {t_spec = 0; abund_spec = 0; lum_spec = 0;}
}

// ------- standard verison of get_observables_for_halos ----------
void halo::get_observables_for_halos (int num_groups, string group_info, bool make_maps, 
                                      bool use_projected_radii, bool only_main_halo_particles, bool use_sim_metallicities, int group_start = 0, bool use_halo_r500 = false, bool use_apec = false, bool ignore_outside_range = false)
{
  double r_200_m, r_200_c, r_500_m, r_500_c, r_500_c_today, r_2500_m, r_2500_c, r_2500_c_today, lum,
         t_m, t_ew, t_ew_simple, t_ew_simple_band, t_sl, inner_radius, outer_radius,
         m_gas_500_c_today, m_stars_500_c_today, m_500_c_today, m_gas_2500_c_today, m_stars_2500_c_today, m_2500_c_today; 
  double t_spec_simple, t_spec, abund_spec, lum_spec;  
  string base_dir = output_base;
  
  /// Backup any existing prop_file.txt
  if (FILE *prop_file_test = fopen((output_base+"prop_file.txt").c_str(), "r")) {
    system(("cp "+output_base+"prop_file.txt "+output_base+"prop_file_bkup.txt").c_str());
    fclose(prop_file_test);
  }
  ofstream prop_file((base_dir+"prop_file.txt").c_str());
  stringstream strs;
  char str[256];

  if (only_main_halo_particles) assert((group_info == "subfind") || (group_info == "fof_subfind"));

  for (int group=group_start; group<group_start+num_groups; ++group)
  {
    cout << "\nStarting group " << group << endl;
    strs << "mkdir " << base_dir << "group_" << group << endl;
    strs.getline(str,256);
    system(str);
    strs << base_dir << "group_" << group << "/" << endl;
    strs.getline(str,256);
    set_output_base(str, snapnum);

    /* In order to make the C function for reading hdf5 snapshots work with the
	halo class I read the hdf5 subf data into a custom struct which is then
	copied into the class variables */
    if (is_hdf5) {
      bool get_center = true;
      struct SubfStruct *subf = new SubfStruct();
      stringstream sstm;
      sstm << "0" << snapnum;
      string snapnumstr;
      snapnumstr = sstm.str();
      //sprintf(snapnumstr.c_str(), "0%d", snapnum);
      string subf_file = filename.substr(0,filename.find_last_of("/")+1);
      //subf_file += "fof_subhalo_tab_" + snapnumstr + ".hdf5";
      int offset=0, length=0;
      if (only_main_halo_particles) {
        subhalo_num = 0;
        subf_file += "fof_subhalo_tab_" + snapnumstr + ".hdf5"; get_halo_information_subf_hdf5(subf, group, get_center, subf_file.c_str(), 0, &offset, &length);
        assert(length > 0);
        subhalo_sorted_ids = new int[length];
        ids_num = length;
        for (int i=0; i<length; ++i) subhalo_sorted_ids[i] = id[i+offset]; //store ids of particles in the given subhalo
        /* WARNING: The offset and length correspond only to type 0 particles since the 
           particle IDs are ordered as (all type 0)+(all type 1)+(... 
           - thus subhalo_sorted_ids only contains type 0 IDs*/
        cout << "subhalo_sorted_ids[0] = " << subhalo_sorted_ids[0] << endl;
        cout << "id[1951284] = " << id[1951284] << endl;
        std::sort(subhalo_sorted_ids, subhalo_sorted_ids+length); // Sort into ascending order for matching
        cout << "\nUsing main halo particles only. "<< endl;
        cout << "OFFSET = " << offset << endl;
        cout << "LENGTH = " << length << endl;
        cout << "subhalo_sorted_ids[0] = " << subhalo_sorted_ids[0] << "\n" << endl;
      }
      else if (group_info == "subfind") {subf_file += "subhalo_tab_" + snapnumstr + ".hdf5"; get_halo_information_subf_hdf5(subf, group, get_center, subf_file.c_str(), -1, &offset, &length);}
      else if (group_info == "fof_subfind") {subf_file += "fof_subhalo_tab_" + snapnumstr + ".hdf5"; get_halo_information_subf_hdf5(subf, group, get_center, subf_file.c_str(), -1, &offset, &length);}
      else {cout << "\nERROR: incorrect group info type " << group_info << endl; exit(1);}
      subf_Len = (*subf).subf_Len;
      subf_Offset = 0;
      subf_Mass = (*subf).subf_Mass; 
      subf_Pos = (*subf).subf_Pos; 
      subf_M_Mean200 = (*subf).subf_M_Mean200; 
      subf_R_Mean200 = (*subf).subf_R_Mean200; 
      subf_M_Crit200 = (*subf).subf_M_Crit200; 
      subf_R_Crit200 = (*subf).subf_R_Crit200;
      subf_M_Crit500 = (*subf).subf_M_Crit500; 
      subf_R_Crit500 = (*subf).subf_R_Crit500; 
      subf_M_TopHat200 = (*subf).subf_M_TopHat200; 
      subf_R_TopHat200 = (*subf).subf_R_TopHat200; 
      subf_ContaminationCount = 0; 
      subf_ContaminationMass = 0;
      subf_Nsubs = (*subf).subf_Nsubs;
      subf_FirstSub = (*subf).subf_FirstSub;

      cout << "subf_Mass[" << group << "] = " << subf_Mass << endl;
      cout << "subf.subf_Mass[" << group << "] = " << (*subf).subf_Mass << endl;
      cout << "subf_Pos[" << group << "].x = " << subf_Pos.x << endl;
      cout << "subf.subf_Pos[" << group << "].x = " << (*subf).subf_Pos.x << endl;

      have_subf_halo_info = true;
      subf_info_group = group;

      if (get_center)
      {
        if (mpc_lengths) set_center(subf_Pos.x*1000.0, subf_Pos.y*1000.0, subf_Pos.z*1000.0);
        else set_center(subf_Pos.x, subf_Pos.y, subf_Pos.z);
      }
    }
    else {
      if (only_main_halo_particles) get_halo_information_subf(group,true,0);
      else if (group_info == "subfind") get_halo_information_subf(group,true);
      else if (group_info == "fof_special") get_halo_information_fof_special(group,true);
      else if (group_info == "denhf" && group == 0) 
        get_center_denhf("none");
      else {cout << "ERROR: incorrect group info type " << group_info << endl; exit(1);}
    }

    if (only_main_halo_particles) mark_subhalo_members();

    show_properties(true);

    get_matter_profile(1024,3000);
    r_200_m = get_radius_for_contrast(200,"mean");
    r_200_c = get_radius_for_contrast(200,"crit");
    r_500_m = get_radius_for_contrast(500,"mean");
    r_500_c = get_radius_for_contrast(500,"crit");
    r_500_c_today = get_radius_for_contrast(500,"crit_today");
    r_2500_m = get_radius_for_contrast(2500,"mean");
    r_2500_c = get_radius_for_contrast(2500,"crit");
    r_2500_c_today = get_radius_for_contrast(2500,"crit_today");
    save_matter_profile();
    
    //if (r_500_c_today == 0 && subf_R_Crit500 > 0) {
      //cout << "WARNING: Could not compute R_Crit500. Using R_Crit500 = " << subf_R_Crit500 << " from halo file." << endl;
      //r_500_c_today = subf_R_Crit500;
    //}
    inner_radius = 0.0;
    //inner_radius = 0.15*r_500_c_today;
    if (use_halo_r500) {
      if (mpc_lengths) {outer_radius = subf_R_Crit500 * 1000.0;}
      else {outer_radius = subf_R_Crit500;}
    }
    else {outer_radius = r_500_c_today;}

    t_m = get_temperature_for_radius("m", inner_radius , outer_radius, false, true);
    t_ew = get_temperature_for_radius("ew", inner_radius , outer_radius, use_projected_radii, true);
    t_ew_simple = get_temperature_for_radius("ew_simple", inner_radius , outer_radius, use_projected_radii, true);
    t_ew_simple_band = 
      get_temperature_for_radius("ew_simple_0.5_10", inner_radius , outer_radius, use_projected_radii, true);
    t_sl = get_temperature_for_radius("sl", inner_radius , outer_radius, use_projected_radii, true);

    get_xray_profile(1024,3000);
    save_xray_profile();
    lum = get_luminosity_for_radius(0, outer_radius); //use r_500 scaling relation ???
  
    get_seperate_profiles(512,2000);
    save_seperate_profiles();

    m_gas_500_c_today = get_matter_type_mass(0,0,r_500_c_today);
    m_stars_500_c_today = get_matter_type_mass(4,0,r_500_c_today);
    m_500_c_today = 0;
    for (int type=0; type<6; ++type) m_500_c_today += get_matter_type_mass(type,0,r_500_c_today);
    cout << m_500_c_today << " " << 4.0/3.0*pi*pow(r_500_c_today,3)*rho_crit_today*500.0 << endl; 

    m_gas_2500_c_today = get_matter_type_mass(0,0,r_2500_c_today);
    m_stars_2500_c_today = get_matter_type_mass(4,0,r_2500_c_today);
    m_2500_c_today = 0;
    for (int type=0; type<6; ++type) m_2500_c_today += get_matter_type_mass(type,0,r_2500_c_today);
    cout << m_2500_c_today << " " << 4.0/3.0*pi*pow(r_2500_c,3)*rho_crit_today*2500.0 << endl; 

    get_temperature_entropy_profiles(512,2000);
    save_temperature_entropy_profiles();

    get_metallicity_profile(512,2000);
    save_metallicity_profile();

    //get_hsml_profile(512,2000);
    //save_hsml_profile();

    double abund_buff, lum_buff, norm_buff;
    bool members_only = true; //only effective if members are marked before
    bool exclude_cooling_branch = true; 
    bool use_multiphase = false; 
    //bool use_sim_metallicities = true; 
    bool fit = true;
    /// Calculate luminosity 'lum_spec' and 't_spec_simple' inside three-dimensional radius 'outer_radius'
    write_xspec_model_file(0, outer_radius, false, members_only, exclude_cooling_branch, use_multiphase, 
                           use_sim_metallicities, fit, use_apec, ignore_outside_range, t_spec_simple, abund_buff, norm_buff, lum_spec);
    /// Calculate temperature 't_spec' and adundance inside projected radii
    write_xspec_model_file(inner_radius, outer_radius, use_projected_radii, members_only, exclude_cooling_branch,
                           use_multiphase, use_sim_metallicities, fit, use_apec, ignore_outside_range, t_spec, abund_spec, norm_buff, lum_buff);
    cout << "Written XPSEC model files" << endl;

    prop_file << group << "   " << 1/(1+redshift) << "   "
              << r_200_m << "   " << r_200_c << "   "
              << r_500_m << "   " << r_500_c << "   " << r_500_c_today << "   "
              << r_2500_m << "   " << r_2500_c << "   " << r_2500_c_today << "   "
              << t_m << "   " << t_ew << "   " << t_ew_simple << "   " << t_ew_simple_band << "   " 
              << t_sl << "   " << t_spec << "   " << t_spec_simple << "   "
              << lum << "   " << lum_spec << "   " 
              << m_gas_500_c_today << "   " << m_stars_500_c_today<< "   " << m_500_c_today << "   "
              << m_gas_2500_c_today << "   " << m_stars_2500_c_today<< "   " << m_2500_c_today <<endl;

    if (make_maps) make_xray_maps(512,3000,true,2);
  }

  prop_file.close();
  output_base = base_dir;
//  set_output_base(base_dir, snapnum);
}

// ------- verison for checking entropy profile errors ------------
// void halo::get_observables_for_halos (int num_groups, string group_info, bool make_maps, 
//                                       bool use_projected_radii, bool only_main_halo_particles)
// {
//   double r_200_m, r_200_c, r_500_m, r_500_c, r_500_c_today, r_2500_m, r_2500_c, lum,
//          t_m, t_ew, t_ew_simple, t_ew_simple_band, t_sl, inner_radius, outer_radius,
//          m_gas_500_c_today, m_500_c_today; 
//   double t_spec_simple, t_spec, abund_spec, lum_spec;  
//   string base_dir = output_base;
//   ofstream prop_file((base_dir+"prop_file.txt").c_str());
//   stringstream strs;
//   char str[256];
// 
//   if (only_main_halo_particles) assert(group_info == "subfind"); 
// 
//   for (int group=0; group<num_groups; ++group)
//   {
//     strs << "mkdir " << base_dir << "group_" << group << endl;
//     strs.getline(str,256);
//     system(str);
//     strs << base_dir << "group_" << group << "/" << endl;
//     strs.getline(str,256);
//     set_output_base(str);
// 
//     if (only_main_halo_particles) get_halo_information_subf(group,true,0);
//     else if (group_info == "subfind") get_halo_information_subf(group,true);
//     else if (group_info == "fof_special") get_halo_information_fof_special(group,true);
//     else if (group_info == "denhf" && group == 0) 
//       get_center_denhf("none");
//     else {cout << "ERROR: incorrect group info type" << endl; exit(1);}
// 
//     if (only_main_halo_particles) mark_subhalo_members();
// 
//     show_properties(true);
// 
//     get_matter_profile(1024,3000);
//     r_200_m = get_radius_for_contrast(200,"mean");
//     r_200_c = get_radius_for_contrast(200,"crit");
//     r_500_m = get_radius_for_contrast(500,"mean");
//     r_500_c = get_radius_for_contrast(500,"crit");
//     r_500_c_today = get_radius_for_contrast(500,"crit_today");
//     r_2500_m = get_radius_for_contrast(2500,"mean");
//     r_2500_c = get_radius_for_contrast(2500,"crit");
//     save_matter_profile();
//     
//     inner_radius = 0.8*r_200_c;
//     outer_radius = 1.2*r_200_c;
// 
//     t_m = get_temperature_for_radius("m", inner_radius , outer_radius, false, true);
//     t_ew = get_temperature_for_radius("ew", inner_radius , outer_radius, use_projected_radii, true);
//     t_ew_simple = get_temperature_for_radius("ew_simple", inner_radius , outer_radius, use_projected_radii, true);
//     t_ew_simple_band = 
//       get_temperature_for_radius("ew_simple_0.5_10", inner_radius , outer_radius, use_projected_radii, true);
//     t_sl = get_temperature_for_radius("sl", inner_radius , outer_radius, use_projected_radii, true);
// 
//     get_xray_profile(1024,3000);
//     save_xray_profile();
//     lum = get_luminosity_for_radius(0, outer_radius); //use r_500 scaling relation ???
//   
//     get_seperate_profiles(512,2000);
//     save_seperate_profiles();
//     m_gas_500_c_today = get_matter_type_mass(0,0,r_500_c_today);
//     m_500_c_today = 0;
//     for (int type=0; type<6; ++type) m_500_c_today += get_matter_type_mass(type,0,r_500_c_today);
//     cout << m_500_c_today << " " << 4.0/3.0*pi*pow(r_500_c_today,3)*rho_crit_today*500.0 << endl; 
// 
//     get_temperature_entropy_profiles(512,2000);
//     save_temperature_entropy_profiles();
// 
//     get_metallicity_profile(512,2000);
//     save_metallicity_profile();
// 
//     get_hsml_profile(512,2000);
//     save_hsml_profile();
// 
//     double abund_buff, lum_buff;
//     bool members_only = true; //only effective if members are marked before
//     bool exclude_cooling_branch = true; 
//     bool use_multiphase = false; 
//     bool use_sim_metallicities = false; 
//     bool fit = true;
// 	
// 	double t_spec_spher, abund_spher, norm_spher, lum_spher, t_spec_cyl, abund_cyl, norm_cyl, lum_cyl;
// 	
// 	for (int bin = 0; bin<6; ++bin)
// 	{
//       strs << bin << endl;
//       strs.getline(str,256);
// 
// 	  t_m = get_temperature_for_radius("m", 0.25*bin*r_200_c, 0.25*(bin+1)*r_200_c, false, true);
//       write_xspec_model_file(0.25*bin*r_200_c, 0.25*(bin+1)*r_200_c, false, members_only, exclude_cooling_branch, use_multiphase, 
//                              use_sim_metallicities, fit, t_spec_spher, abund_spher, norm_spher, lum_spher, str);
//       write_xspec_model_file(0.25*bin*r_200_c, 0.25*(bin+1)*r_200_c, true, members_only, exclude_cooling_branch,
//                              use_multiphase, use_sim_metallicities, fit, t_spec_cyl, abund_cyl, norm_cyl, lum_cyl, str);
// 
//       prop_file << bin << "  " << 0.25*bin*r_200_c << "   " << 0.25*(bin+0.5)*r_200_c << "   " << (bin+1)*r_200_c << "   "
//                 << t_m << "   " 
// 				<< t_spec_spher << "   " << abund_spher << "   " << norm_spher << "  " 
// 			    << sqrt(norm_spher/(pow(0.25*(bin+1)*r_200_c,3)-pow(0.25*bin*r_200_c,3))) << "  " << lum_spher << "   "
//                 << t_spec_cyl << "   " << abund_cyl << "   " << norm_cyl << "   " 
// 			    << sqrt(norm_cyl/pow(pow(0.25*(bin+1)*r_200_c,2)-pow(0.25*bin*r_200_c,2),1.5)) << "  " << lum_cyl << "   " << endl;
//     }
// 
//     if (make_maps) make_xray_maps(512,3000,true,2);
//   }
// 
//   prop_file.close();
// 
//   set_output_base(base_dir);
// }

// -------- main program ----------

int main ()
{
/*
 /// For testing hdf5 conversions of Millennium zooms 
  int numsnaps = 1;
  int snaplist[] = {62}; //NB: XSPEC does not like ~zero redshift
  int numsims = 26;
  string simnames[] = {"m_585_h_486_2210_z3/csf", "m_585_h_486_2210_z3/csfbh",
		"m_717_h_497_3136_z3/csf", "m_717_h_497_3136_z3/csfbh",
		"m_878_h_466_1998_z3/csf", "m_878_h_466_1998_z3/csfbh",
		"m_1075_h_442_550_z3/csf", "m_1075_h_442_550_z3/csfbh",
		"m_1317_h_379_2174_z3/csf", "m_1317_h_379_2174_z3/csfbh",
		"m_1613_h_390_2864_z3/csf", "m_1613_h_390_2864_z3/csfbh",
		"m_1975_h_506_1096_z3/csf", "m_1975_h_506_1096_z3/csfbh",
		"m_2186_h_56_1789_z3/csf", "m_2186_h_56_1789_z3/csfbh",
		"m_2419_h_436_2408_z3/csf", "m_2419_h_436_2408_z3/csfbh",
		"m_2963_h_73_1513_z3/csf", "m_2963_h_73_1513_z3/csfbh",
		"m_3279_h_103_2600_z3/csf", "m_3279_h_103_2600_z3/csfbh",
		"m_3629_h_259_610_z3/csf", "m_3629_h_259_610_z3/csfbh",
		"m_4445_h_183_1161_z3/csf", "m_4445_h_183_1161_z3/csfbh"};
		//"m_5443_h_352_482_z3/csf", "m_5443_h_352_482_z3/csfbh",
		//"m_6664_h_48_194_z3/csf", "m_6664_h_48_194_z3/csfbh",
		//"m_10002_h_94_501_z3/csf", "m_10002_h_94_501_z3/csfbh",
		//"m_14999_h_355_1278_z3/csf",
		//"m_22498_h_197_0_z2/csf", "m_22498_h_197_0_z2/csfbh",
		//"m_33617_h_146_0_z2/csf", "m_33617_h_146_0_z2/csfbh"};
  int groupnum[] = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}; //N most massive halos to analyse
  int group_start = 0; // Group number to start with i.e. analyze groups from group_start to group_start+groupnum
  string snapnamebase = "snap";
  bool multiple_file_snapshot = false;
  string snapdirbase = "snapdir";
  bool mpc_lengths = true;
  bool is_hdf5 = true; //hdf5 snapshots
  string hdf5_ext = ".hdf5";
  assert(!(is_hdf5 && multiple_file_snapshot));
  string metals_method = "fixed";
  string group_info = "subfind";
  string fluxtablebase = "/home/nh444/scripts/scaling/flux_tables_mekal_63/fluxTable_mekal_0";
  string simdirname = "/home/nh444/data/project1/Puchwein2008sims/";
  string outputdirname = "/home/nh444/data/project1/Puchwein2008sims";
  bool use_halo_r500 = false;
  bool only_main_halo_particles = false;
  bool use_apec = false;
  bool ignore_outside_range = false;
*/
/*
  // For testing Gadget zoom-ins
  int numsnaps = 1;
  int snaplist[] = {62}; //NB: XSPEC does not like ~zero redshift so there is a min. redshift limit (~0.001) in the halo class initialisation function
  int numsims = 26;
//  string simnames[] = {"m_585_h_486_2210_z3/csf", "m_585_h_486_2210_z3/csfbh",
//		"m_717_h_497_3136_z3/csf", "m_717_h_497_3136_z3/csfbh",
//		"m_878_h_466_1998_z3/csf", "m_878_h_466_1998_z3/csfbh",
//		"m_1075_h_442_550_z3/csf", "m_1075_h_442_550_z3/csfbh",
//		"m_1317_h_379_2174_z3/csf", "m_1317_h_379_2174_z3/csfbh",
//		"m_1613_h_390_2864_z3/csf", "m_1613_h_390_2864_z3/csfbh",
//		"m_1975_h_506_1096_z3/csf", "m_1975_h_506_1096_z3/csfbh",
//		"m_2186_h_56_1789_z3/csf", "m_2186_h_56_1789_z3/csfbh",
//		"m_2419_h_436_2408_z3/csf", "m_2419_h_436_2408_z3/csfbh",
//		"m_2963_h_73_1513_z3/csf", "m_2963_h_73_1513_z3/csfbh",
//		"m_3279_h_103_2600_z3/csf", "m_3279_h_103_2600_z3/csfbh",
//		"m_3629_h_259_610_z3/csf", "m_3629_h_259_610_z3/csfbh",
//		"m_4445_h_183_1161_z3/csf", "m_4445_h_183_1161_z3/csfbh"};
//  string simnames[] = {"m_5443_h_352_482_z3/csfbh",
//		"m_6664_h_48_194_z3/csf", "m_6664_h_48_194_z3/csfbh",
//		"m_10002_h_94_501_z3/csf", "m_10002_h_94_501_z3/csfbh",
//		"m_14999_h_355_1278_z3/csf",
//		"m_22498_h_197_0_z2/csf", "m_22498_h_197_0_z2/csfbh",
//		"m_33617_h_146_0_z2/csf", "m_33617_h_146_0_z2/csfbh"};
//  string simnames[] = {"m_14999_h_355_1278_z3/csfbh"};
  string simnames[] = {"m_6664_h_48_194_z3/csf", "m_6664_h_48_194_z3/csfbh"};
  int groupnum[] = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}; //N most massive halos to analyse
  int group_start = 0; // Group number to start with i.e. analyze groups from group_start to group_start+groupnum
  string snapnamebase = "snap";
  bool multiple_file_snapshot = false;
  string snapdirbase = "snapdir";
  bool mpc_lengths = true;
  bool is_hdf5 = false; //hdf5 snapshots
  string hdf5_ext = ".hdf5";
  assert(!(is_hdf5 && multiple_file_snapshot));
  string metals_method = "fixed";
  string group_info = "subfind";
  string fluxtablebase = "/home/nh444/scripts/scaling/flux_tables_mekal_63/fluxTable_mekal_0";
  /// If flux table is not found then "simple" prescription will be used in xray flux estimates (only affects xray flux maps) ///
  string simdirname = "/data/deboras/ewald/";
  string outputdirname = "/home/nh444/data/project1/Puchwein2008sims";
  bool use_halo_r500 = false;
  bool only_main_halo_particles = false;
  bool use_apec = false;
  bool ignore_outside_range = false;
*/

  int numsnaps = 1;
  int snaplist[] = {25}; //NB: XSPEC does not like ~zero redshift so there is a min. redshift limit (~0.001) in the halo class initialisation function
  int numsims = 1;
  //string simnames[] = {"L40_512_NoDutyRadioWeak_HalfThermal_FixEta_BHChangeVel",
			//"L40_512_NoDutyRadioIntLow_HalfThermal_FixEta_BHChangeVel"};
  //string simnames[] = {"L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff"};
  string simnames[] = {"L40_512_NoDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_Prex"};
  int groupnum[] = {80,20,20,20,20,20,20,20,20,20}; //N most massive halos to analyse
  int group_start = 0; // Group number to start with i.e. analyze groups from group_start to group_start+groupnum
  string snapnamebase = "snap";
  bool multiple_file_snapshot = false;
  string snapdirbase = "snapdir";
  bool mpc_lengths = false;
  bool is_hdf5 = true; //hdf5 snapshots
  string hdf5_ext = ".hdf5";
  assert(!(is_hdf5 && multiple_file_snapshot));
  string metals_method = "fixed";
  string group_info = "fof_subfind";
  string fluxtablebase = "/home/nh444/scripts/scaling/flux_tables_mekal_63/fluxTable_mekal_0";
  /// If flux table is not found then "simple" prescription will be used in xray flux estimates (only affects xray flux maps) ///
  string simdirname = "/data/emergence3/deboras/Elliptical_ZoomIns/";
  string outputdirname = "/data/curie4/nh444/project1/L-T_relations";
  bool use_halo_r500 = false;
  bool only_main_halo_particles = false;
  bool use_apec = true;
  bool ignore_outside_range = false;

/*
  /// Zoom-ins - LDRIntRadioEffBHVel
  int numsnaps = 1;
  int snaplist[] = {25}; //NB: XSPEC does not like ~zero redshift so there is a min. redshift limit (~0.001) in the halo class initialisation function
  int numsims = 2;
  //string simnames[] = {"c256", "c384"};
  //string simnames[] = {"c384"};
  string simnames[] = {"c256"};
  int groupnum[] = {1,1,1,1,1,1,1,1,1,1}; //N most massive halos to analyse
  int group_start = 0; // Group number to start with i.e. analyze groups from group_start to group_start+groupnum
  string snapnamebase = "snap";
  bool multiple_file_snapshot = false;
  string snapdirbase = "snapdir";
  bool mpc_lengths = true;
  bool is_hdf5 = true; //hdf5 snapshots
  string hdf5_ext = ".hdf5";
  assert(!(is_hdf5 && multiple_file_snapshot));
  string metals_method = "fixed";
  string group_info = "fof_subfind";
  string fluxtablebase = "/home/nh444/scripts/scaling/flux_tables_mekal_63/fluxTable_mekal_0";
  /// If flux table is not found then "simple" prescription will be used in xray flux estimates (only affects xray flux maps) ///
  string simdirname = "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/";
  string outputdirname = "/data/curie4/nh444/project1/L-T_relations/LDRIntRadioEffBHVel";
  bool use_halo_r500 = false;
  bool only_main_halo_particles = false;
  bool use_apec = true;
  bool ignore_outside_range = true;
*/
/*
  /// For fake snapshots
  int numsnaps = 1;
  int snaplist[] = {1}; //NB: XSPEC does not like ~zero redshift so there is a min. redshift limit (~0.001) in the halo class initialisation function
  int numsims = 1;
  string simnames[] = {"A478"};
  int groupnum[] = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}; //N most massive halos to analyse
  int group_start = 0; // Group number to start with i.e. analyze groups from group_start to group_start+groupnum
  string snapnamebase = "snap";
  bool multiple_file_snapshot = false;
  string snapdirbase = "snapdir";
  bool mpc_lengths = false;
  bool is_hdf5 = true; //hdf5 snapshots
  string hdf5_ext = ".hdf5";
  assert(!(is_hdf5 && multiple_file_snapshot));
  string metals_method = "fixed";
  string group_info = "fof_subfind";
  string fluxtablebase = "";
  /// If flux table is not found then "simple" prescription will be used in xray flux estimates (only affects xray flux maps) ///
  string simdirname = "/data/curie4/nh444/project1/L-T_relations/Helens_clusters/";
  string outputdirname = "/data/curie4/nh444/project1/L-T_relations/Helens_clusters";
  bool use_halo_r500 = true;
  bool only_main_halo_particles = false;
  bool use_apec = false;
  bool ignore_outside_range = false;
*/
/*
  /// For fake snapshots produced from ACCEPT cluster profiles
  int numsnaps = 1;
  int snaplist[] = {1}; //NB: XSPEC does not like ~zero redshift so there is a min. redshift limit (~0.001) in the halo class initialisation function
  int numsims = 17;
  string simnames[] = {"ABELL_1060", "HCG_42",  "NGC_5846", "NGC_5813",
         "MKW_04", "HCG_0062", "ESO_5520200",  "MS_J1157.3+5531",
         "NGC_5044", "ESO_3060170", "NGC_0507", "ABELL_1991",
         "ABELL_0576", "3C_388", "2PIGG_3041","2PIGG_2850", "ABELL_0222"};
  int groupnum[] = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}; //N most massive halos to analyse
  int group_start = 0; // Group number to start with i.e. analyze groups from group_start to group_start+groupnum
  string snapnamebase = "snap";
  bool multiple_file_snapshot = false;
  string snapdirbase = "snapdir";
  bool mpc_lengths = false;
  bool is_hdf5 = true; //hdf5 snapshots
  string hdf5_ext = ".hdf5";
  assert(!(is_hdf5 && multiple_file_snapshot));
  string metals_method = "fixed";
  string group_info = "fof_subfind";
  string fluxtablebase = "";
  /// If flux table is not found then "simple" prescription will be used in xray flux estimates (only affects xray flux maps) ///
  string simdirname = "/data/curie4/nh444/project1/L-T_relations/ACCEPT_clusters/";
  string outputdirname = "/data/curie4/nh444/project1/L-T_relations/ACCEPT_clusters";
  bool use_halo_r500 = true;
  bool only_main_halo_particles = false;
*/
/*
  /// For fake snapshots produced from Viklinin+2006 profiles
  int numsnaps = 1;
  int snaplist[] = {1}; //NB: XSPEC does not like ~zero redshift so there is a min. redshift limit (~0.001) in the halo class initialisation function
  int numsims = 1;
  string simnames[] = {"MKW4", "A262", "A133", "A1795", "A478", "A2029", "USGC_S152"};
  int groupnum[] = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}; //N most massive halos to analyse
  int group_start = 0; // Group number to start with i.e. analyze groups from group_start to group_start+groupnum
  string snapnamebase = "snap";
  bool multiple_file_snapshot = false;
  string snapdirbase = "snapdir";
  bool mpc_lengths = false;
  bool is_hdf5 = true; //hdf5 snapshots
  string hdf5_ext = ".hdf5";
  assert(!(is_hdf5 && multiple_file_snapshot));
  string metals_method = "fixed";
  string group_info = "fof_subfind";
  string fluxtablebase = "";
  /// If flux table is not found then "simple" prescription will be used in xray flux estimates (only affects xray flux maps) ///
  string simdirname = "/data/curie4/nh444/project1/L-T_relations/Vikh06_clusters/";
  string outputdirname = "/data/curie4/nh444/project1/L-T_relations/Vikh06_clusters";
  bool use_halo_r500 = true;
  bool only_main_halo_particles = false;
*/

//   int numsnaps = 2;
//   int snaplist[] = {55,62};
//   int numsims = 4;
//   string simnames[] = {"m_585_h_486_2210_z3_csf","m_585_h_486_2210_z3_csfbh",/*
//                        "m_717_h_497_3136_z3_csf","m_717_h_497_3136_z3_csfbh",
//                        "m_878_h_466_1998_z3_csf","m_878_h_466_1998_z3_csfbh","m_878_h_466_1998_z4_csfbh",
//                        "m_1075_h_442_550_z3_csf","m_1075_h_442_550_z3_csfbh","m_1075_h_442_550_z4_csfbh", 
//                        "m_1317_h_379_2174_z3_csf","m_1317_h_379_2174_z3_csfbh",
//                        "m_1613_h_390_2864_z3_csf","m_1613_h_390_2864_z3_csfbh"
//                        "m_1975_h_506_1096_z3_csf","m_1975_h_506_1096_z3_csfbh",
//                        "m_2186_h_56_1789_z3_csf","m_2186_h_56_1789_z3_csfbh",
//                        "m_2419_h_436_2408_z3_csf","m_2419_h_436_2408_z3_csfbh",
//                        "m_2963_h_73_1513_z3_csf","m_2963_h_73_1513_z3_csfbh",
//                        "m_3279_h_103_2600_z3_csf","m_3279_h_103_2600_z3_csfbh",
//                        "m_3629_h_259_610_z3_csf","m_3629_h_259_610_z3_csfbh"
//                        "m_4445_h_183_1161_z3_csf","m_4445_h_183_1161_z3_csfbh",
//                        "m_5443_h_352_482_z3_csfbh"
//                        */"m_6664_h_48_194_z3_csf","m_6664_h_48_194_z3_csfbh"/*,
//                        "m_14999_h_355_1278_z3_csfbh"*/};
//   int groupnum[] = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
//   string snapnamebase = "snap";
//   bool multiple_file_snapshot = false;
//   string snapdirbase = "snapdir";
//   bool mpc_lengths = true;
//   string group_info = "subfind";
//   string fluxtablebase = "./flux_tables_mekal_63/fluxTable_mekal_0";
//   string simdirname = "/scratch/simdat/";
//   string outputdirname = "output_paper_test"; 
//   bool only_main_halo_particles = false;





  stringstream strs;
  string outputsimname; 
  size_t slashpos; 
  char str[256];

  for (int simindex=0; simindex<numsims; ++ simindex)
  for (int snapindex=0; snapindex<numsnaps; ++snapindex) 
  {
    strs << simdirname << simnames[simindex] << "/";
    if (multiple_file_snapshot) strs << snapdirbase << "_0" << snaplist[snapindex] << "/";
    if (is_hdf5) strs << snapnamebase << "_0" << snaplist[snapindex] << hdf5_ext << endl;
    else {strs << snapnamebase << "_0" << snaplist[snapindex] << endl;}
    strs.getline(str,256);
    halo clus(str, metals_method, mpc_lengths, is_hdf5);
    outputsimname = simnames[simindex];
    while((slashpos=outputsimname.find("/")) != string::npos) outputsimname.replace(slashpos, 1, "_");
    strs << "mkdir " << outputdirname << "/" << outputsimname << "_0" << snaplist[snapindex] << endl;
    strs.getline(str,256);
    system(str);
    strs << outputdirname << "/" << outputsimname << "_0" << snaplist[snapindex] << "/" << endl;
    strs.getline(str,256);
    clus.set_output_base(str, snaplist[snapindex]);

    clus.show_header();
  
    clus.load_mass();
    clus.load_pos();
    clus.load_id();
    clus.load_rho();
    //clus.load_hsml();
    clus.load_u();
    clus.load_ne();
    clus.load_z();
    cout << "Calculating temperatures..." << endl;
    clus.calculate_temp();
    cout << "Loaded all particle data." << endl;

    //clus.load_hot_phase_temp();
    //clus.load_mhi();

    bool exclude_cooling_branch = true; 
    bool use_multiphase = false;
    bool use_sim_metallicities = false;  
    strs << fluxtablebase << snaplist[snapindex] << ".d" << endl;
    strs.getline(str,256);
    cout << "Calculating X-ray flux..." << endl;
    /// Estimate the x-ray flux contribution of each particle which is used to estimate the luminosity profile in get_xray_profile --> value of 'lum' (not lum_spec) in prop file.
    if (FILE *flux_table_test = fopen(str, "r")) {
      clus.calculate_xray_flux(str, 0, exclude_cooling_branch, use_multiphase, use_sim_metallicities);
      fclose(flux_table_test);
    }
    else {
      cout << "WARNING: Could not find flux table " << str << " Using 'simple' prescription for xray flux estimate" << endl;
      clus.calculate_xray_flux("simple", 0, exclude_cooling_branch, use_multiphase, use_sim_metallicities);
    }
    //clus.calculate_xray_flux("/home/nh444/scripts/scaling/flux_tables_mekal_63/fluxTable_mekal_063.d", 0, exclude_cooling_branch, use_multiphase, use_sim_metallicities);

  
    //clus.mark_region_members('y','z',
    //  "/afs/mpa/home/puchwein/data/c++files/scaling/output/m_4445_h_183_1161_csf_062/group_0/pow.reg");

    bool make_maps = true; 
    bool use_projected_radii = true;

    make_maps = false;

    cout << "Running get_observables_for_halos..." << endl;
    clus.get_observables_for_halos (groupnum[simindex], group_info, make_maps, use_projected_radii, only_main_halo_particles, use_sim_metallicities, group_start = group_start, use_halo_r500 = use_halo_r500, use_apec = use_apec, ignore_outside_range=ignore_outside_range);
  }

  cout << "done!" << endl;
}

/*int main () // get times of millenium run 
{
  ofstream times("/afs/mpa/home/puchwein/times.txt");
  stringstream strs;
  char filename[256];

  for (int i=0; i<=63; ++i)
  {
    strs << "/afs/mpa/project/sim/Millennium/snapdir_0";
    if (i<10) strs << "0";
    strs << i << "/snap_millennium_0";
    if (i<10) strs << "0";
    strs << i << ".0" << endl;
    strs.getline(filename,256);
    halo clus(filename);
    times << clus.get_time_of_snap() << endl; 
  }

  times.close();

  return 0;
}*/

/*int main (int argc, char *argv[]) // code for showsnap 
{
  if (argc<3)
  {
    cout << "ERROR: too few or wrong parameters given" << endl;
    cout << "usage: showsnap filename command <command parameters>" << endl;
    cout << "commands and command parameters: " << endl; 
    cout << "- header" << endl;
    cout << "- group <groupnumber>" << endl;
    cout << "- masses" << endl;
    cout << "- massmap <length unit (kpc or mpc)> {<groupnumber> or <x-center> <y-center> <z-center>} "
         << "<projection (x, y or z)>  <grid dimension> <sidelength> <output file>" << endl;
    cout << "- meantemp" << endl;
    cout << "- massprofiles <length unit (kpc or mpc)> {<groupnumber> or <x-center> <y-center> <z-center>} <bins> <radius> <output file>" << endl;
    cout << "- tempprofiles <length unit (kpc or mpc)> {<groupnumber> or <x-center> <y-center> <z-center>} <bins> <radius> <output file>" << endl;
    exit(0);
  }  

  if (strcmp(argv[2],"header")==0 && argc==3) 
  {
    halo clus(argv[1]);
    clus.show_header();
  }
  else if (strcmp(argv[2],"group")==0 && argc==4)
  {
    halo clus(argv[1]);
    
    stringstream strs;
    strs << argv[3] << endl;
    int group;
    strs >> group;
  
    clus.get_halo_information_subf(group,true);
    clus.show_properties(false);
  }
  else if (strcmp(argv[2],"masses")==0 && argc==3)
  {
    halo clus(argv[1]);
    clus.load_mass();
  }
  else if (strcmp(argv[2],"massmap")==0 && (argc==9 || argc==11))
  {
    bool mpc_lengths;
    if (strcmp(argv[3],"mpc")==0) mpc_lengths = true;
    else if (strcmp(argv[3],"kpc")==0) mpc_lengths = false;
    else {cout << "give kpc or mpc as length unit" << endl; exit(0);}

    halo clus(argv[1], mpc_lengths);

    stringstream strs;
    
    int gridsize, curindex;
    double sidelength;
    string filename;
    if (argc==9)
    {
      strs << argv[4] << endl;
      int group;
      strs >> group;
      clus.get_halo_information_subf(group,true);
      curindex = 5;
    }
    else
    {
      double xc, yc, zc;
      strs << argv[4] << endl;
      strs >> xc;
      strs << argv[5] << endl;
      strs >> yc;
      strs << argv[6] << endl;
      strs >> zc;
      if (mpc_lengths) {xc *= 1000.0; yc *= 1000.0; zc *= 1000.0;}
      clus.set_center(xc,yc,zc);
      curindex = 7;
    }

    string proj;
    if (strcmp(argv[curindex],"x")==0) proj = "x";
    else if (strcmp(argv[curindex],"y")==0) proj = "y";
    else if (strcmp(argv[curindex],"z")==0) proj = "z";
    else {cout << "give x, y or z as projection" << endl; exit(0);}

    strs << argv[curindex+1] << endl;
    strs >> gridsize;
    if (gridsize > 4096) {cout << "maximum gridsize is 4096" << endl; exit(0);}
    strs << argv[curindex+2] << endl;
    strs >> sidelength;
    if (mpc_lengths) sidelength *= 1000.0;
    strs << argv[curindex+3] << endl;
    strs >> filename; 

    clus.load_mass();
    clus.load_pos();

    if (proj=="y") clus.rotate_coordinates(1.5,0,0);
    if (proj=="z") clus.rotate_coordinates(0,1.5,1.5);

    cout << "creating mass map for " << proj << "-projection" << endl;  

    clus.make_mass_map(gridsize,sidelength,filename);
  }
  else if (strcmp(argv[2],"meantemp")==0 && argc==3)
  {
    halo clus(argv[1]);
    clus.load_mass();
    clus.load_u();
    clus.calculate_temp();
    clus.get_mean_temperature();
  }
  else if (strcmp(argv[2],"massprofiles")==0 && (argc==8 || argc==10))
  {    
    bool mpc_lengths;
    if (strcmp(argv[3],"mpc")==0) mpc_lengths = true;
    else if (strcmp(argv[3],"kpc")==0) mpc_lengths = false;
    else {cout << "give kpc or mpc as length unit" << endl; exit(0);}

    halo clus(argv[1], mpc_lengths);
    
    stringstream strs;
    
    int binnum;
    double radius;
    string filename;
    if (argc==8)
    {
      strs << argv[4] << endl;
      int group;
      strs >> group;
      clus.get_halo_information_subf(group,true);
      strs << argv[5] << endl;
      strs >> binnum;
      strs << argv[6] << endl;
      strs >> radius;
      if (mpc_lengths) radius *= 1000.0;
      strs << argv[7] << endl;
      strs >> filename;
    }
    else
    {
      double xc, yc, zc;
      strs << argv[4] << endl;
      strs >> xc;
      strs << argv[5] << endl;
      strs >> yc;
      strs << argv[6] << endl;
      strs >> zc;
      strs << argv[7] << endl;
      strs >> binnum;
      strs << argv[8] << endl;
      strs >> radius;
      if (mpc_lengths) {xc *= 1000.0; yc *= 1000.0; zc *= 1000.0; radius *= 1000.0;}
      clus.set_center(xc,yc,zc);
      strs << argv[9] << endl;
      strs >> filename;
    }

    clus.load_mass();
    clus.load_pos();

    clus.get_seperate_profiles(binnum,radius);
    
    clus.get_matter_profile(1365,4000);
    double ref_radius = clus.get_radius_for_contrast(200,"crit");

    clus.save_seperate_profiles(filename, ref_radius);
  }
  else if (strcmp(argv[2],"tempprofiles")==0 && (argc==8 || argc==10))
  {    
    bool mpc_lengths;
    if (strcmp(argv[3],"mpc")==0) mpc_lengths = true;
    else if (strcmp(argv[3],"kpc")==0) mpc_lengths = false;
    else {cout << "give kpc or mpc as length unit" << endl; exit(0);}

    halo clus(argv[1], mpc_lengths);
    
    stringstream strs;
    
    int binnum;
    double radius;
    string filename;
    if (argc==8)
    {
      strs << argv[4] << endl;
      int group;
      strs >> group;
      clus.get_halo_information_subf(group,true);
      strs << argv[5] << endl;
      strs >> binnum;
      strs << argv[6] << endl;
      strs >> radius;
      if (mpc_lengths) radius *= 1000.0;
      strs << argv[7] << endl;
      strs >> filename;
    }
    else
    {
      double xc, yc, zc;
      strs << argv[4] << endl;
      strs >> xc;
      strs << argv[5] << endl;
      strs >> yc;
      strs << argv[6] << endl;
      strs >> zc;
      strs << argv[7] << endl;
      strs >> binnum;
      strs << argv[8] << endl;
      strs >> radius;
      if (mpc_lengths) {xc *= 1000.0; yc *= 1000.0; zc *= 1000.0; radius *= 1000.0;}
      clus.set_center(xc,yc,zc);
      strs << argv[9] << endl;
      strs >> filename;
    }

    clus.load_mass();
    clus.load_pos();
    clus.load_rho();
    clus.load_u();
    clus.load_ne();
    clus.calculate_temp();

    clus.get_temperature_entropy_profiles(binnum,radius);

    clus.get_matter_profile(1365,4000);
    double ref_radius = clus.get_radius_for_contrast(200,"crit");

    clus.save_temperature_entropy_profiles(filename, ref_radius);
  }
  
  return 0;
}*/

/*int main ()
{
  halo clus("/scratch/simdat/m_14999_h_355_1278_z3_csfbh/snapdir_062/snap_062", true);
  clus.load_pos();
  clus.load_mass();
}*/
