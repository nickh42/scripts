import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np
import scipy.stats as stats
from os import chdir
chdir('/home/nh444/data/project1/L-T_relations')

core_excised = False

if core_excised:
    outname = "L-T_relation_paper_cex.pdf"
    filename = "L-T_data_apec_0.5-10_Rasia_1e7_core_ex_L.txt"
else:
    filename = "L-T_data_apec_0.5-10_Rasia_1e7_Tvir_thresh_3.0.txt"
    outname = "L-T_relation_paper_cin.pdf"

#filename = "L-T_data_apec_0.5-2_Rasia_c4.txt"
#filename = "L-T_data_apec_0.5-10_Rasia_c4_Tvir_thresh_2.5.txt"

snapnum = 25
H = 67.9
h = H /100.0

#fig = plt.figure(figsize=(7,6.5))
fig = plt.figure(figsize=(7,7))

bins = np.logspace(np.log10(0.16), np.log10(1.7), num=10)
x = 10**((np.log10(bins[:-1]) + np.log10(bins[1:]))/2)

labels = ["Ref",
          "Stronger radio",
          "Quasar duty cycle",
          "Combined"]

col = ["#f95c57",
       "#6ca3d9",
       "#95c967",
       "#ce6ac4"]
col = ["#525cc8",
       "#d45b46",
       "#a95ac7"]
from palettable.colorbrewer.qualitative import Dark2_8 as pallette
col=pallette.hex_colors
markers = ["D", "s", "o"]
ls=['dashdot', 'dotted', 'dashed', 'solid']
ms = 6.5
lw=2
temp_idx = 5 ## temperature index - 5 for spectral, 15 for emission-weighted, 14 for mass-weighted

best = np.loadtxt("L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_"+str(snapnum).zfill(3)+"/"+filename)
plt.plot(best[:,temp_idx], best[:,1], marker=markers[2], mfc='none', mec=col[3], ms=ms, ls='none', mew=1.2, zorder=4)
## median is plotted below
#==============================================================================
# for zoom in ["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c256_box_Arepo_new", "c320_box_Arepo_new", "c384_box_Arepo_new"]:
#     data = np.loadtxt("/home/nh444/data/project1/L-T_relations/LDRIntRadioEffBHVel/"+zoom+"_"+str(snapnum).zfill(3)+"/"+filename)
#     best = np.vstack((best, data))
#     plt.plot(data[temp_idx], data[1]*67.74**2/H**2, marker='o', mfc='none', mec=col[3], mew=1.2, ms=ms, ls='none', zorder=4)
#==============================================================================

data = np.loadtxt("L40_512_NoDutyRadioWeak_HalfThermal_FixEta_"+str(snapnum).zfill(3)+"/"+filename)
#plt.plot(data[:,temp_idx], data[:,1], marker=markers[0], mec='none', mfc=col[0], ms=ms, ls='none', mew=1.0, zorder=4)
med, bin_edges, n = stats.binned_statistic(data[:,temp_idx], data[:,1], statistic='median', bins=bins)
plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[0], ls=ls[2], lw=lw, dash_capstyle='round', label=labels[0], zorder=4)

data = np.loadtxt("L40_512_NoDutyRadioInt_HalfThermal_FixEta_"+str(snapnum).zfill(3)+"/"+filename)
#plt.plot(data[:,temp_idx], data[:,1], marker=markers[0], mec='none', mfc=col[1], ms=ms, ls='none', mew=1.0, zorder=4)
med, bin_edges, n = stats.binned_statistic(data[:,temp_idx], data[:,1], statistic='median', bins=bins)
plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[1], ls=ls[1], lw=lw, dash_capstyle='round', label=labels[1], zorder=4)

data = np.loadtxt("L40_512_LowDutyRadioWeak_HalfThermal_FixEta_"+str(snapnum).zfill(3)+"/"+filename)
#plt.plot(data[:,temp_idx], data[:,1], marker=markers[1], mec='none', mfc=col[2], ms=ms, ls='none', mew=1.0, zorder=4)
med, bin_edges, n = stats.binned_statistic(data[:,temp_idx], data[:,1], statistic='median', bins=bins)
plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[2], ls=ls[0], lw=lw, dash_capstyle='round', label=labels[2], zorder=4)

med, bin_edges, n = stats.binned_statistic(best[:,temp_idx], best[:,1], statistic='median', bins=bins)
plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[3], ls=ls[3], lw=lw, dash_capstyle='round', label=labels[3], zorder=4)

## Phase space cut comparison
'''
### 0.5-2 keV fit range for box and zooms LDRIntRadioEffBHVel with original phase space cut
colour = '#a73ed7'#purple
marker='s'
filename="L-T_data_apec_0.5-2.txt"
data = np.loadtxt("L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_"+str(snapnum).zfill(3)+"/"+filename)## 0.5-2 kev fit range
plt.plot(data[:,temp_idx], np.log10(data[:,1]), marker=marker, mec=colour, mfc='none', ms=5, ls='none', mew=1.2)
#med, bin_edges, n = stats.binned_statistic(data[:,temp_idx], data[:,1], statistic='median', bins=bins)
#plt.plot(x[np.isfinite(med)], np.log10(med[np.isfinite(med)]), c='purple', lw=lw, label=r"Best box (default cut)")#(APEC $T_{min}=0.01$ keV)
zooms = ["c96", "c128_fixed", "c160", "c192", "c256_fixed", "c320", "c384_fixed", "c448"]
for zoom in zooms:
    data = np.loadtxt("/home/nh444/data/project1/L-T_relations/LDRIntRadioEffBHVel/"+zoom+"_"+str(snapnum).zfill(3)+"/"+filename)
    plt.plot(data[temp_idx], np.log10(data[1])+np.log10(67.74*67.74/(H*H)), marker=marker, mec=colour, mfc='none', mew=1.2, ms=5, ls='none', zorder=3)#(APEC $T_{min}=0.01$ keV)

### As above but with Rasia2012 phase space cut (0.5-2 keV fit range for box and zooms LDRIntRadioEffBHVel)
colour = '#ef3a24'#red
marker='o'
data = np.loadtxt("L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_0.5-2_Rasia_c4.txt")## 0.5-2 kev fit range
plt.plot(data[:,temp_idx], np.log10(data[:,1]), marker=marker, mec=colour, mfc='none', ms=5, ls='none', mew=1.2, label="Rasia+ 2012 cut")
#med, bin_edges, n = stats.binned_statistic(data[:,temp_idx], data[:,1], statistic='median', bins=bins)
#plt.plot(x[np.isfinite(med)], np.log10(med[np.isfinite(med)]), c=colour, lw=lw, label=r"Best box (Rasia+ 2012 cut)")
for zoom in zooms:
    data = np.loadtxt("/home/nh444/data/project1/L-T_relations/LDRIntRadioEffBHVel/"+zoom+"_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_0.5-2_Rasia_c4.txt")
    plt.plot(data[temp_idx], np.log10(data[1])+np.log10(67.74*67.74/(H*H)), marker=marker, mec=colour, mfc='none', mew=1.2, ms=5, ls='none', zorder=3)#(APEC $T_{min}=0.01$ keV)
'''

c = '0.7'
from scaling.plotting.plotObsDat import L_T
L_T(plt.gca(), core_excised=core_excised, c=c, h=h)


### This is just to show where Sun 2009 and Vikhlinin 2006 lie on L-T and uses L-T values form Horner 2001 for the same groups
#Sun = np.genfromtxt("/data/vault/nh444/ObsData/L-T/Sun2009.csv", delimiter=",")
#mask = np.isfinite(Sun[:,6])
## This uses the L-T directly from Horner 2001, not from the data table
##### divide lum by Ez!! #####
#plt.errorbar(Sun[mask,9], 10**Sun[mask,8]*(50.0*50.0/(H*H)), xerr=Sun[mask,10], c='darkblue', mec='none', marker='D', ls='none', ms=4, label=r"Sun+ 2009 sample \& Horner 2001 L-T", zorder=3)##e60000
## This uses the L-T directly from the paper -
##### divide lum by Ez!! #####
#plt.errorbar(Sun[mask,9], 10**Sun[mask,6]*(Sun[mask,7])*(50.0*50.0/(H*H)), xerr=Sun[mask,10], c='red', mec='none', marker='D', ls='none', ms=4, label="Sun+ 2009 sample & Horner 2001 L-T", zorder=3)
#plt.errorbar(Sun[:,1], Sun[:,4]*44.0*(72.0*72.0/(H*H)), xerr=[abs(Sun[:,3]), Sun[:,2]], c='#ff00bf', mec='none', marker='D', ls='none', ms=4, label="Sun+ 2009 sample & Zhang+ 2011 luminosities", zorder=3)
#Vikh = np.genfromtxt("/data/vault/nh444/ObsData/profiles/Vikhlinin_profiles/Vikhlinin2006_maintable.csv", delimiter=",")
#mask = np.isfinite(Vikh[:,19])
##### divide lum by Ez!! #####
#plt.errorbar(Vikh[mask,21], 10**Vikh[mask,23]*(50.0*50.0/(H*H)), xerr=Vikh[mask,22], c=c, mec='none', marker='D', ls='none', ms=4, label=r"Vikhlinin+ 2006 sample \& Horner 2001 L-T", zorder=3)
## This uses the L-T directly from Horner 2001, not from the data table
##### divide lum by Ez!! #####
#plt.errorbar(Vikh[mask,21], 10**Vikh[mask,19]*(Vikh[mask,20])*(50.0*50.0/(H*H)), xerr=Vikh[mask,22], c='green', mec='none', marker='D', ls='none', ms=4, label="Vikhlinin+ 2006 sample & Horner 2001 L-T", zorder=3)
#plt.errorbar(Vikh[:,3], (Vikh[:,13]*1e44*(72.0*72.0/(H*H)), xerr=Vikh[:,4], yerr=Vikh[:,14], c='#006080', mec='none', marker='D', ls='none', ms=4, label="Vikhlinin+ 2006 sample & Zhang+ 2011 luminosities", zorder=3)


plt.xlim(0.2, 20)
plt.ylim(1e40,1e46)
plt.xscale('log')
plt.yscale('log')
if core_excised:
    plt.ylabel("L$_{500, \mathrm{ce}}$ E(z)$^{-1}$ [erg s$^{-1}$]", fontsize=22)#^{\mathrm{bol}}
    plt.xlabel(r"T$_{500, \mathrm{ce}}$ [keV]", fontsize=22)
else:
    plt.ylabel("L$_{500}$ E(z)$^{-1}$ [erg s$^{-1}$]", fontsize=22)#^{\mathrm{bol}}
    plt.xlabel(r"T$_{500}$ [keV]", fontsize=22)

handles, labels = plt.gca().axes.get_legend_handles_labels()
n = 4
leg1 = plt.legend(handles[:n], labels[:n], loc='lower right', frameon=False, numpoints=1, ncol=1, fontsize='small')
ax = plt.gca().add_artist(leg1)
plt.legend(handles[n:], labels[n:], loc='upper left', frameon=False, borderaxespad=1, numpoints=1, ncol=1, fontsize='small')
#plt.legend(loc='upper left', frameon=False, borderaxespad=1, numpoints=1, fontsize=12, ncol=1)

plt.gca().xaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))

plt.tight_layout(pad=0.45)
#plt.tight_layout()
plt.savefig(outname)#, bbox_inches='tight')
print "Saved to", outname
