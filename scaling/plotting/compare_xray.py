def main():
    outdir = "/home/nh444/Documents/paper/"
    basedir="/home/nh444/data/project1/L-T_relations/"
    simdirs = ["L40_512_LDRIntRadioEffBHVel",
               ]
    labels = [None]
    filenames = ["L-T_data_apec_0.5-10_T300kpc_default_Tvir_thresh_4.0.txt",
                 "L-T_data_apec_0.5-10_default_Tvir_thresh_4.0_setE.txt"]
    axislabels = ["T$_{300 \mathrm{kpc}}$ [keV]", "T$_{500}$ [keV]"]
    outname = "xray_comparison_T300kpc_T500.pdf"
    snapnum = 25
    h = 0.679
    zoomdirs = ["/data/curie4/nh444/project1/L-T_relations/LDRIntRadioEffBHVel/"]
    zooms = ["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c256_box_Arepo_new", "c320_box_Arepo_new", "c384_box_Arepo_new", "c448_box_Arepo_new"]
    
    
    figsize=(7,7)
    from palettable.colorbrewer.qualitative import Dark2_8 as pallette
    col=pallette.hex_colors
    markers = ["D", "s", "^", "o"]
    ls=['dashdot', 'dotted', 'dashed', 'solid']
    ms=6.5
    lw=3
    mew=1.2
    binwidth = 0.2 ## dex for mass in Msun
    idxScat = [0]
    idxMed = []
    binwidth = 0.1
    
    plot_comp(simdirs, snapnum, filenames, labels=labels, axislabels=axislabels, zoomdirs=zoomdirs, zooms=zooms, h=h, basedir=basedir, outname=outname, outdir=outdir, figsize=figsize, idxScat=idxScat, idxMed=idxMed, col=col, markers=markers, ls=ls, ms=ms, lw=lw, mew=mew, binwidth=binwidth)

def plot_comp(simdirs, snapnum, filenames, comp="temp",
             labels=None, axislabels=None, zoomdirs=None, zooms=[],
             h=0.679, basedir="/home/nh444/data/project1/L-T_relations/",
             outname="xray_comparison.pdf",
             outdir="/home/nh444/data/project1/L-T_relations/",
             figsize=(7,7), idxScat=[], idxMed=[], col=None,
             markers=None, ls=None, ms=6.5, lw=2, mew=1.2, binwidth=0.1):
    import matplotlib.pyplot as plt
    import matplotlib.ticker as ticker
    import numpy as np
    import scipy.stats as stats
    
    assert len(simdirs) > 0, "simdirs is an empty list"
    if zoomdirs is None:
        zoomdirs = [""]*len(simdirs)
    if axislabels is None:
        axislabels = filenames      
    
    if col is None:
        from palettable.colorbrewer.qualitative import Dark2_8 as pallette
        col=pallette.hex_colors
    if markers is None:
        markers = ["D", "s", "o"]
    if ls is None:
        ls=['dashdot', 'dotted', 'dashed', 'solid']
        
    fig = plt.figure(figsize=figsize)
    
    if comp=="temp":
        # temp_mw, temp_emw, temp_bw, temp_ew, temp_mw_3D, temp_emw_3D, temp_bw_3D, temp_ew_3D, temp_3D
        # 14,      15,       16,      17,      18,         19,          20,         21,         22,
        idx = 5 ##  5 for spectral projected, 22 for spectral 3D, 15 for emission-weighted, 14 for mass-weighted
        xmin, xmax = 0.3, 10.0
        plt.plot([xmin, xmax], [xmin, xmax], c='0.5', ls='--')
    else:
        assert False, "comp ="+comp+"not recognised."  
       
    ## Median profiles
    num = int((np.log10(xmax) - np.log10(xmin)) / binwidth)
    bins = np.logspace(np.log10(xmin), np.log10(xmax), num=num)
    xbin = 10**((np.log10(bins[:-1]) + np.log10(bins[1:]))/2)
    
    for simidx, simdir in enumerate(simdirs):
        xdat = np.loadtxt(basedir+simdirs[simidx]+"_"+str(snapnum).zfill(3)+"/"+filenames[0])
        ydat = np.loadtxt(basedir+simdirs[simidx]+"_"+str(snapnum).zfill(3)+"/"+filenames[1])
        label = labels[simidx]
        
        x = xdat[:,idx]
        y = ydat[:,idx]

        if zoomdirs[simidx] != "" and len(zooms)>0:
            for zoom in zooms:
                xdat = np.loadtxt(zoomdirs[simidx]+zoom+"_"+str(snapnum).zfill(3)+"/"+filenames[0])
                ydat = np.loadtxt(zoomdirs[simidx]+zoom+"_"+str(snapnum).zfill(3)+"/"+filenames[1])
                x = np.append(x, xdat[idx])
                y = np.append(y, ydat[idx])
                
        if simidx in idxMed:
            med, bin_edges, n = stats.binned_statistic(x, y, statistic='median', bins=bins)
            plt.plot(xbin[np.isfinite(med)], med[np.isfinite(med)], c=col[simidx], ls=ls[simidx], lw=lw, dash_capstyle='round', label=label)
            label = None ## don't want two legend entries
        if simidx in idxScat:
            plt.plot(x, y, marker=markers[simidx], mfc='none', mec=col[simidx], ms=ms, ls='none', mew=mew, label=label)

    plt.xscale('log')
    plt.xlim(xmin, xmax)
    plt.ylim(xmin, xmax)
    plt.yscale('log')
    plt.xlabel(axislabels[0])
    plt.ylabel(axislabels[1])

    plt.legend()
    #leg = plt.legend(loc='upper left', frameon=False, borderaxespad=1, numpoints=1, fontsize=12, ncol=1)
    #if comp=="temp":
        #plt.gca().xaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))
        #plt.gca().yaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))
    
    plt.tight_layout(pad=0.45)
    fig.savefig(outdir+outname)#, bbox_inches='tight', transparent=True)
    print "Saved to", outdir+outname

if __name__ == "__main__":
    main()