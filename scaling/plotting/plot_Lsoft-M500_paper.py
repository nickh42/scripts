import matplotlib.pyplot as plt
#import matplotlib.ticker as ticker
import numpy as np
import scipy.stats as stats
from os import chdir
chdir('/home/nh444/data/project1/L-T_relations')

outname = "Lsoft-M500_relation_paper.pdf"

snapnum = 25
h = 0.679

filename = "L-T_data_apec_0.5-7_Rasia_c4_Tvir_thresh_2.5_L0.1-2.4.txt"

fig = plt.figure(figsize=(7,7))
Mmin, Mmax = 10**12.5, 10**15

labels = ["Ref",
          "Stronger radio",
          "Quasar duty cycle",
          "Combined"]

from palettable.colorbrewer.qualitative import Dark2_8 as pallette
col=pallette.hex_colors
markers = ["D", "s", "o"]
ls=['dashdot', 'dotted', 'dashed', 'solid']
ms = 6.5
lw=2.5
mew=1.2

## Median profiles
bins = np.logspace(11, 13.5, num=25)
x = 10**((np.log10(bins[:-1]) + np.log10(bins[1:]))/2.0)

data1 = np.loadtxt("L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_"+str(snapnum).zfill(3)+"/"+filename)
plt.plot(data1[:,12] * 1e10 / h, data1[:,1], marker=markers[2], mfc='none', mec=col[3], ms=ms, ls='none', mew=mew, zorder=4)
## median is plotted below
#==============================================================================
# for zoom in ["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c256_box_Arepo_new", "c384_box_Arepo_new"]:
#     data = np.loadtxt("/home/nh444/data/project1/L-T_relations/LDRIntRadioEffBHVel/"+zoom+"_"+str(snapnum).zfill(3)+"/"+filename)
#     data1 = np.vstack((data1, data))
#     plt.plot(data[12] * 1e10 / h, data[1], marker=markers[2], mfc='none', mec=col[3], mew=mew, ms=ms, ls='none', zorder=4)
#==============================================================================

data = np.loadtxt("L40_512_NoDutyRadioWeak_HalfThermal_FixEta_"+str(snapnum).zfill(3)+"/"+filename)
#plt.plot(data[:,12] * 1e10 / h, data[:,1], marker=markers[0], mec='none', mfc=col[0], ms=ms, ls='none', mew=mew, zorder=4)
med, bin_edges, n = stats.binned_statistic(data[:,12] * 1e10 / h, data[:,1], statistic='median', bins=bins)
plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[0], ls=ls[2], lw=lw, dash_capstyle='round', label=labels[0], zorder=4)

data = np.loadtxt("L40_512_NoDutyRadioInt_HalfThermal_FixEta_"+str(snapnum).zfill(3)+"/"+filename)
#plt.plot(data[:,12] * 1e10 / h, data[:,1], marker=markers[0], mec='none', mfc=col[1], ms=ms, ls='none', mew=mew, zorder=4)
med, bin_edges, n = stats.binned_statistic(data[:,12] * 1e10 / h, data[:,1], statistic='median', bins=bins)
plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[1], ls=ls[1], lw=lw, dash_capstyle='round', label=labels[1], zorder=4)

data = np.loadtxt("L40_512_LowDutyRadioWeak_HalfThermal_FixEta_"+str(snapnum).zfill(3)+"/"+filename)
#plt.plot(data[:,12] * 1e10 / h, data[:,1], marker=markers[1], mec='none', mfc=col[2], ms=ms, ls='none', mew=mew, zorder=4)
med, bin_edges, n = stats.binned_statistic(data[:,12] * 1e10 / h, data[:,1], statistic='median', bins=bins)
plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[2], ls=ls[0], lw=lw, dash_capstyle='round', label=labels[2], zorder=4)

med, bin_edges, n = stats.binned_statistic(data1[:,12] * 1e10 / h, data1[:,1], statistic='median', bins=bins)
plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[3], ls=ls[3], lw=lw, dash_capstyle='round', label=labels[3], zorder=4)
    

c = '0.7'
c2 = '0.4'
from scaling.plotting.plotObsDat import Lsoft_M500
Lsoft_M500(plt.gca(), c=c, c2=c2, h=h)

plt.xlim(Mmin, Mmax)
plt.ylim(1e40,1e46)
plt.xscale('log')
plt.yscale('log')
plt.ylabel("L$_{500}^{0.1-2.4 \mathrm{keV}}$ E(z)$^{-1}$ [erg s$^{-1}$]", fontsize=22)
plt.xlabel("M$_{500}$ E(z) [$M_{\odot}$]", fontsize=22)#, labelpad=10)#, fontsize=16)
plt.gca().tick_params(axis='x', which='major', pad=7)

handles, labels = plt.gca().axes.get_legend_handles_labels()
n = 4
leg1 = plt.legend(handles[:n], labels[:n], loc='lower right', frameon=False, numpoints=1, ncol=1)
ax = plt.gca().add_artist(leg1)
plt.legend(handles[n:], labels[n:], loc='upper left', frameon=False, numpoints=1, ncol=1)
#plt.legend(loc='lower right', frameon=False, numpoints=1, ncol=1)#, borderaxespad=1

plt.tight_layout(pad=0.45)
plt.savefig(outname)#, bbox_inches='tight')
print "Saved to", outname
