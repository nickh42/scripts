#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  2 12:28:38 2016

@author: nh444
"""
def main():
    import numpy as np
    import matplotlib.pyplot as plt
    from os import chdir
    workdir = '/home/nh444/data/project1/L-T_relations/'
    chdir(workdir)
    
    simdirs = [#"L40_512_LDRIntRadioEff_noBH",
               #"L40_512_NoDutyRadioWeak",
               #"L40_512_NoDutyRadioInt",
               #"L40_512_DutyRadioWeak",
               "L40_512_LDRIntRadioEffBHVel",
               #"L40_512_LDRHighRadioEff",
               "L40_512_MDRIntRadioEff",
               ]
    zoomdirs = [#"","","","",
                "LDRIntRadioEffBHVel/",
                "MDRInt/"] ## empty string if no zooms
    zooms = [#["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c192_box_Arepo_new", "c256_box_Arepo_new", "c320_box_Arepo_new", "c384_box_Arepo_new", "c448_box_Arepo_new"],
             ["c96_box_Arepo_new", "c128_box_Arepo_new", "c192_box_Arepo_new", "c256_box_Arepo_new", "c384_box_Arepo_new"],
             ["c96_MDRInt", "c128_MDRInt", "c192_MDRInt", "c256_MDRInt", "c384_MDRInt"]]
    labels = [#"No BHs",
              #"Weak radio",
              #"Stronger radio",
              #"Quasar duty cycle",
              "$\delta t = 10$ Myr",#"Combined",
              #"Longer radio duty",
              "$\delta t = 25$ Myr",
              ]
    snapnum = 25
    scat_ind = range(len(simdirs))
    med_ind = []#range(len(simdirs))
    
    outfilename="gas_mass_fractions_models.pdf"
    filename = "L-T_data_apec_0.5-10_Rasia_1e7.txt"
    filename = "L-T_data_apec_0.5-10_default_Tvir_thresh_4.0.txt"
    h = 0.679
    
    fig = plt.figure(figsize=(7,7))#width,height
    
    ## For median profiles
    xmin, xmax = [3e12,1e15]
    num = int((np.log10(xmax) - np.log10(xmin/10.0)) / 0.2)
    bins = np.logspace(np.log10(xmin/10.0), np.log10(xmax), num=num)
    x = 10**((np.log10(bins[:-1]) + np.log10(bins[1:]))/2)
    
    #from palettable.colorbrewer.qualitative import Dark2_8 as pallette
    #col=pallette.hex_colors
    #col = ["#c7673e","#78a44f","#6b87c8","#934fce","#e6ab02","#be558f","#47af8d"]
    #col = ['#FF9819', '#B94400', '#6395EC', '#6154BE'] ## Hydrangea orange-blue colour scheme
    #col = ["#F2BE40", "#D9501D", '#6395EC', '#4D4396'] ## orange-blue - combination of Viget and Hydrangea, slightly altered
    #col = ["#c7673e","#78a44f","#6b87c8","#934fce","#e6ab02","#be558f","#47af8d"]
    col = ['#24439b', '#ED7600']
    mec = col
    mfc = ['none']*len(simdirs)
    markers = ['D', 's', 'o', '^', '>', 'v', '<']
    #ls=['dashdot', 'dotted', 'dashed', 'solid']
    #ls = ['solid']*4
    ms=8
    lw=3
    mew=1.8
    
    ### OBSERVATIONS ###
    from scaling.plotting.plotObsDat import gas_frac
    gas_frac(plt.gca(), for_paper=False, h=h)
    
    midx = 0
    for isim, simdir in enumerate(simdirs):
        data = np.loadtxt(simdirs[isim]+"_"+str(snapnum).zfill(3)+"/"+filename)
    
        if zoomdirs[isim] is not "":
            for zoom in zooms[isim]:
                zdata = np.loadtxt(zoomdirs[isim]+zoom+"_"+str(snapnum).zfill(3)+"/"+filename)
                data = np.vstack((data, zdata))
                #plt.plot(zdata[12] * 1e10 / h, zdata[13]/zdata[12], marker=markers[midx], mfc=mfc[isim], mec=mec[isim], mew=mew, ms=ms, ls='none')
                
        if isim in scat_ind:
            plt.plot(data[:,12] * 1e10 / h, data[:,13]/data[:,12], marker=markers[midx], mfc=mfc[isim], mec=mec[isim], ms=ms, ls='none', mew=mew, label=labels[isim])
            midx += 1    
            
        if isim in med_ind:
            med, idx = get_median(data[:,12] * 1e10 / h, data[:,13]/data[:,12], bins)
            plt.plot(x[:idx+1], med[:idx+1], c=col[isim], lw=lw, ls='solid', label=labels[isim])
            plt.plot(x[idx:], med[idx:], c=col[isim], lw=lw, ls='dashed', dash_capstyle='round')   
    
    
    plt.ylabel(r"M$_{\mathrm{gas}}$/M$_{500}$")#, fontsize=16)
    plt.xlabel(r"M$_{500}$ [M$_{\odot}$]")#, fontsize=16)
    plt.xlim(xmin, 3e15)##2e15
    plt.ylim(0.0, 0.2)
    plt.xscale('log')
    
    from scaling.plotting.plotObsDat import add_legends
    add_legends(plt.gca(), simdirs, labels, col=col, nfiles=1, sims_only=True, frameon=True, verbose=True)
        
    ## Set axis labels to decimal format not scientific when using log scale
    #plt.gca().yaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))
    #plt.setp(plt.gca().get_xticklabels(), fontsize=16)
    #plt.setp(plt.gca().get_yticklabels(), fontsize=16)
    
    plt.tight_layout(pad=0.45)
    fig.savefig(workdir+outfilename)#, bbox_inches='tight')
    print "Plot saved to", workdir+outfilename
    
def get_median(x, values, bins, NforIdx=10):
    """ returns median and index of first bin with fewer than NforIdx values"""
    from scipy.stats import binned_statistic
    import numpy as np
    
    med = binned_statistic(x, values, statistic='median', bins=bins)[0]
    N = binned_statistic(x, values, statistic='count', bins=bins)[0]
    idx = np.min(np.where((N > 0) & (N < NforIdx))[0])
    return med, idx
    
if __name__ == "__main__":
    main()