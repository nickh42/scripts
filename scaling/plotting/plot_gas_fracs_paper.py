#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  2 12:28:38 2016

@author: nh444
"""
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as stats
from os import chdir
chdir('/home/nh444/data/project1/L-T_relations')

simdirs = ["L40_512_NoDutyRadioWeak_HalfThermal_FixEta/",
           "L40_512_NoDutyRadioInt_HalfThermal_FixEta/",
           #"L40_512_LowDutyRadioWeak_HalfThermal_FixEta_BHChangeVel/",
           "L40_512_LowDutyRadioWeak_HalfThermal_FixEta/",
           "L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff/",
           #"L40_512_NoDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_Prex/",
           ]
labels = ["Ref",
          "Stronger radio",
          "Quasar duty cycle",
          "Combined"]
snapnum = 25

outfilename="gas_mass_fractions_models.pdf"
#filename = "L-T_data_apec_0.5-2_Rasia_c4.txt"
filename = "L-T_data_apec_0.5-10_Rasia_c4_Tvir_thresh_2.5.txt"
#filename = "L-T_data_apec_0.5-10_none_Tvir_thresh_2.5.txt"
filename = "L-T_data_apec_0.5-10_Rasia_1e7_Tvir_thresh_3.0.txt"
h = 0.679

fig = plt.figure(figsize=(7,7))#width,height

## For median profiles
xmin, xmax = [3e12,7e13]
num = int((np.log10(xmax) - np.log10(xmin/10.0)) / 0.2)
bins = np.logspace(np.log10(xmin/10.0), np.log10(xmax), num=num)
x = 10**((np.log10(bins[:-1]) + np.log10(bins[1:]))/2)

from palettable.colorbrewer.qualitative import Dark2_8 as pallette
col=pallette.hex_colors
#col = ["#c7673e","#78a44f","#6b87c8","#934fce","#e6ab02","#be558f","#47af8d"]
markers = ["D", "s", "o"]
ls=['dashdot', 'dotted', 'dashed', 'solid']
#ls = ['solid']*4
ms = 6.5
lw=3
mew=1.2

data1 = np.loadtxt("L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_"+str(snapnum).zfill(3)+"/"+filename)
plt.plot(data1[:,12] * 1e10 / h, data1[:,13]/data1[:,12], marker=markers[2], mfc='none', mec=col[3], ms=ms, ls='none', mew=mew, zorder=4)
#median is plotted below
#==============================================================================
# for zoom in ["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c256_box_Arepo_new", "c320_box_Arepo_new", "c384_box_Arepo_new"]:
#     zdata = np.loadtxt("/home/nh444/data/project1/L-T_relations/LDRIntRadioEffBHVel/"+zoom+"_"+str(snapnum).zfill(3)+"/"+filename)
#     data1 = np.vstack((data1, zdata))
#     plt.plot(zdata[12] * 1e10 / h, zdata[13]/zdata[12], marker='o', mfc='none', mec=col[3], mew=mew, ms=ms, ls='none', zorder=4)
#==============================================================================
#==============================================================================
# for zoom in ["c96_box_Arepo_new_LowSoft", "c128_box_Arepo_new_LowSoft", "c160_box_Arepo_new_LowSoft", "c256_box_Arepo_new_LowSoft", "c320_box_Arepo_new_LowSoft", "c384_box_Arepo_new_LowSoft"]:
#     zdata = np.loadtxt("/home/nh444/data/project1/L-T_relations/LDRIntRadioEffBHVel/"+zoom+"_"+str(snapnum).zfill(3)+"/"+filename)
#     data1 = np.vstack((data1, zdata))
#     plt.plot(zdata[12] * 1e10 / h, zdata[13]/zdata[12], marker='D', mfc='none', mec=col[3], mew=mew, ms=ms, ls='none', zorder=4)
# 
#==============================================================================
data = np.loadtxt("L40_512_NoDutyRadioWeak_HalfThermal_FixEta_"+str(snapnum).zfill(3)+"/"+filename)
M500 = data[:,12] * 1e10 / h
#plt.plot(data[:,12] * 1e10 / h, data[:,13]/data[:,12], marker=markers[0], mec='none', mfc=col[0], ms=ms, ls='none', mew=mew, zorder=4)
med, bin_edges, n = stats.binned_statistic(M500, data[:,13]/data[:,12], statistic='median', bins=bins)
plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[0], ls=ls[2], lw=lw, dash_capstyle='round', label=labels[0], zorder=4)
#plt.plot(M500[M500>xmax], data[M500>xmax,13]/data[M500>xmax,12], marker=markers[0], mfc='none', mec=col[0], ms=ms, ls='none', mew=mew, zorder=4)

data = np.loadtxt("L40_512_NoDutyRadioInt_HalfThermal_FixEta_"+str(snapnum).zfill(3)+"/"+filename)
M500 = data[:,12] * 1e10 / h
#plt.plot(M500, data[:,13]/data[:,12], marker=markers[0], mec='none', mfc=col[1], ms=ms, ls='none', mew=mew, zorder=4)
med, bin_edges, n = stats.binned_statistic(M500, data[:,13]/data[:,12], statistic='median', bins=bins)
plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[1], ls=ls[1], lw=lw, dash_capstyle='round', label=labels[1], zorder=4)
#plt.plot(M500[M500>xmax], data[M500>xmax,13]/data[M500>xmax,12], marker=markers[1], mfc='none', mec=col[1], ms=ms, ls='none', mew=mew, zorder=4)

data = np.loadtxt("L40_512_LowDutyRadioWeak_HalfThermal_FixEta_"+str(snapnum).zfill(3)+"/"+filename)
M500 = data[:,12] * 1e10 / h
#plt.plot(M500, data[:,13]/data[:,12], marker=markers[1], mec='none', mfc=col[2], ms=ms, ls='none', mew=mew, zorder=4)
med, bin_edges, n = stats.binned_statistic(M500, data[:,13]/data[:,12], statistic='median', bins=bins)
plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[2], ls=ls[0], lw=lw, dash_capstyle='round', label=labels[2], zorder=4)
#plt.plot(M500[M500>xmax], data[M500>xmax,13]/data[M500>xmax,12], marker=markers[2], mfc='none', mec=col[2], ms=ms, ls='none', mew=mew, zorder=4)

med, bin_edges, n = stats.binned_statistic(data1[:,12] * 1e10 / h, data1[:,13]/data1[:,12], statistic='median', bins=bins)
plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[3], ls=ls[3], lw=lw, dash_capstyle='round', label=labels[3], zorder=4)#(APEC $T_{min}=0.01$ keV)

#==============================================================================
# ## without cuts or T thresh
# data = np.loadtxt("L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_0.5-10_none.txt")
# med, bin_edges, n = stats.binned_statistic(data[:,12] * 1e10 / h, data[:,13]/data[:,12], statistic='median', bins=bins)
# plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c='k', lw=lw, ls='dashed', dash_capstyle='round', label=labels[3]+" (no cuts, no T thresh)", zorder=4)#(APEC $T_{min}=0.01$ keV)
# for zoom in ["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c256_box_Arepo_new", "c384_box_Arepo_new"]:
#     data = np.loadtxt("/home/nh444/data/project1/L-T_relations/LDRIntRadioEffBHVel/"+zoom+"_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_0.5-10_none.txt")
#     plt.plot(data[12] * 1e10 / h, data[13]/data[12], marker='o', mfc='k', mec='k', mew=mew, ms=ms, ls='none', zorder=4)
#==============================================================================
    
## without cuts with T thresh
data = np.loadtxt("L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_0.5-10_none.txt")
med, bin_edges, n = stats.binned_statistic(data[:,12] * 1e10 / h, data[:,13]/data[:,12], statistic='median', bins=bins)
plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c='0.3', lw=lw, ls='dashed', dash_capstyle='round', label=labels[3]+" (all gas)", zorder=4)#(APEC $T_{min}=0.01$ keV)
#for zoom in ["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c256_box_Arepo_new", "c384_box_Arepo_new"]:
    #data = np.loadtxt("/home/nh444/data/project1/L-T_relations/LDRIntRadioEffBHVel/"+zoom+"_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_0.5-10_none_Tvir_thresh_2.5.txt")
    #plt.plot(data[12] * 1e10 / h, data[13]/data[12], marker='o', mfc='none', mec='0.3', mew=mew, ms=ms, ls='none', zorder=4)

### OBSERVATIONS ###
from scaling.plotting.plotObsDat import gas_frac
gas_frac(plt.gca(), h=h)

plt.ylabel(r"M$_{\mathrm{gas}}$/M$_{500}$")#, fontsize=16)
plt.xlabel(r"M$_{500}$ [M$_{\odot}$]")#, fontsize=16)
plt.xlim(xmin, 1e15)##2e15
plt.ylim(0.0, 0.2)
plt.xscale('log')
#plt.yscale('log')

handles, labels = plt.gca().axes.get_legend_handles_labels()
n = 5
leg1 = plt.legend(handles[:n], labels[:n], loc='lower right', frameon=False, numpoints=1, fontsize='x-small', ncol=1)
ax = plt.gca().add_artist(leg1)
plt.legend(handles[n:], labels[n:], loc='upper left', frameon=False, borderaxespad=1, numpoints=1, fontsize='x-small', ncol=2)
#plt.legend(loc='upper left', frameon=False, borderaxespad=1, numpoints=1, ncol=2)
## Set axis labels to decimal format not scientific when using log scale
#plt.gca().yaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))
#plt.setp(plt.gca().get_xticklabels(), fontsize=16)
#plt.setp(plt.gca().get_yticklabels(), fontsize=16)

plt.tight_layout(pad=0.45)
plt.savefig(outfilename)#, bbox_inches='tight')
print "Plot saved to", outfilename
