def main():
    outdir = "/data/curie4/nh444/project1/L-T_relations/"
    basedir= "/data/curie4/nh444/project1/L-T_relations/"
    simdirs = ["L40_512_NoDutyRadioWeak_HalfThermal_FixEta",
               "L40_512_NoDutyRadioInt_HalfThermal_FixEta",
               "L40_512_LowDutyRadioWeak_HalfThermal_FixEta",
               "L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff",
               ]
    labels = ["Ref",
              "Stronger radio",
              "Quasar duty cycle",
              "Combined"]
    
    basedir = "/data/curie4/nh444/project1/L-T_relations/"
    simdirs = [#"L40_512_LDRIntRadioEff_noBH",
               #"L40_512_NoDutyRadioWeak",
               #"L40_512_NoDutyRadioInt",
               #"L40_512_DutyRadioWeak",
               "L40_512_LDRIntRadioEffBHVel",
               #"L40_512_LDRHighRadioEff",
               "L40_512_MDRIntRadioEff",
               ]
    labels = [#"No BHs",
              #"Weak radio",
              #"Stronger radio",
              #"Quasar duty cycle",
              "$\delta t = 10$ Myr",#"Combined",
              #"Longer radio duty",
              "$\delta t = 25$ Myr",
              ]
    zoomdirs = ["/data/curie4/nh444/project1/L-T_relations/LDRIntRadioEffBHVel/",
                "/data/curie4/nh444/project1/L-T_relations/MDRInt/"]
    zooms = [#["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c192_box_Arepo_new", "c256_box_Arepo_new", "c320_box_Arepo_new", "c384_box_Arepo_new", "c448_box_Arepo_new"],
             ["c96_box_Arepo_new", "c128_box_Arepo_new", "c192_box_Arepo_new", "c256_box_Arepo_new", "c384_box_Arepo_new", "c448_box_Arepo_new"],
             ["c96_MDRInt", "c128_MDRInt", "c192_MDRInt", "c256_MDRInt", "c384_MDRInt", "c448_MDRInt", "c512_MDRInt"]]   
       
    snapnum = 25
    h = 0.679
    
    figsize=(7,7)
    #col = ["#c7673e","#78a44f","#6b87c8","#934fce","#e6ab02","#be558f","#47af8d"]
    col = ['#24439b', '#ED7600'] ## blue orange
    markers = ['D', 's', 'o', '^', '>', 'v', '<']
    ms=8
    mew=1.8
    
    filenames = ["L-T_data_apec_0.5-10_T300kpc_default_Tvir_thresh_4.0.txt", "L-T_data_apec_0.5-10_default_Tvir_thresh_4.0.txt"]
    filelabels = ["T$_{300 \mathrm{kpc}}$ [keV]", "T$_{500}$ [keV]"]
    outname = "temp_comparison_T300kpc_T500.pdf"

    plot_temp_comp(simdirs, snapnum, filenames, log=True, projected=True, filelabels=filelabels, labels=labels, zoomdirs=zoomdirs, zooms=zooms, h=h, basedir=basedir, outname=outname, outdir=outdir, col=col, markers=markers, ms=ms, mew=mew, figsize=figsize)

def plot_temp_comp(simdirs, snapnum, filenames, projected=True, filelabels=None, labels=None, zoomdirs=None, zooms=[], log=False, h=0.679, basedir="/home/nh444/data/project1/L-T_relations/", outname="temp_comparison.pdf", outdir="/home/nh444/data/project1/L-T_relations/", figsize=(7,7), col=None, markers=None, ms=6.5, mew=1.2):
    import matplotlib.pyplot as plt
    import numpy as np
    
    assert len(simdirs) > 0, "simdirs is an empty list"
    if not isinstance(filenames, (list, tuple)):## if not a list, convert to list
        filenames = [filenames]
    
    # temp_mw, temp_emw, temp_bw, temp_ew, temp_mw_3D, temp_emw_3D, temp_bw_3D, temp_ew_3D, temp_3D
    # 14,      15,       16,      17,      18,         19,          20,         21,         22,
    if projected:
        temp_idx = 5 ## temperature index - 5 for spectral projected, 22 for spectral 3D, 15 for emission-weighted, 14 for mass-weighted
    else:
        temp_idx = 22
    
    if col is None:
        from palettable.colorbrewer.qualitative import Dark2_8 as pallette
        col=pallette.hex_colors
    if markers is None:
        markers = ["D", "s", "o"]
    if filelabels is None:
        filelabels = [label.replace("_", "\_") for label in filenames]
    if labels is None:
        labels = [label.replace("_", "\_") for label in simdirs]
    
    fig = plt.figure(figsize=figsize)
    Tmin, Tmax = 0.4, 10.0 ## keV
    if log:
        Tmin, Tmax = np.log10(Tmin), np.log10(Tmax)
    else:
        plt.xscale('log')
        plt.yscale('log')
      
    for simidx, simdir in enumerate(simdirs):
        idx = simidx
        data1 = np.loadtxt(basedir+simdirs[simidx]+"_"+str(snapnum).zfill(3)+"/"+filenames[0])
        data2 = np.loadtxt(basedir+simdirs[simidx]+"_"+str(snapnum).zfill(3)+"/"+filenames[1])
        
        if zoomdirs[simidx] is not "":
            for zoom in zooms[simidx]:
                zdata1 = np.loadtxt(zoomdirs[simidx]+zoom+"_"+str(snapnum).zfill(3)+"/"+filenames[0])
                data1 = np.vstack((data1, zdata1))
                zdata2 = np.loadtxt(zoomdirs[simidx]+zoom+"_"+str(snapnum).zfill(3)+"/"+filenames[1])
                data2 = np.vstack((data2, zdata2))
        
        if log:
            x = np.log10(data2[:,temp_idx])
            y = np.log10(data1[:,temp_idx])
        else:
            x = data2[:,temp_idx]
            y = data1[:,temp_idx]
        
        plt.plot(x, y, marker=markers[idx], mfc='none', mec=col[idx], ms=ms, ls='none', mew=mew, label=labels[simidx])
   
    plt.xlim(Tmin, Tmax)
    plt.ylim(Tmin, Tmax)
    plt.ylabel(filelabels[0])
    plt.xlabel(filelabels[1])
    
    plt.plot([Tmin, Tmax], [Tmin, Tmax], c='0.5', label="y=x")
    if log:
        plt.plot([Tmin-0.05, Tmax-0.05], [Tmin, Tmax], c='0.5', ls='dashed', label="y=x+0.05 dex")
        plt.plot([Tmin-0.1, Tmax-0.1], [Tmin, Tmax], c='0.5', ls='dashdot', label="y=x+0.1 dex")
    
    plt.legend(loc='best')
   
    #import matplotlib.ticker as ticker
    #plt.gca().xaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))
    #plt.gca().yaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))
    
    plt.tight_layout(pad=0.45)
    fig.savefig(outdir+outname)#, bbox_inches='tight', transparent=True)
    print "Saved to", outdir+outname

if __name__ == "__main__":
    main()