import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np
import scipy.stats as stats
from os import chdir
chdir('/home/nh444/data/project1/L-T_relations')

outname = "L-T_relation.pdf"
#outname = "L-T_relation_Tvir_thresh.pdf"
obs_dir = "/home/nh444/vault/ObsData/L-T/"
filename = "L-T_data_apec_0.5-2_Rasia_c4.txt"
filename = "L-T_data_apec_0.5-10_Rasia_c4_Tvir_thresh_2.5.txt"
filename = "L-T_data_apec_0.5-10_Rasia_1e7_Tvir_thresh_3.0.txt"
#filename = "L-T_data_apec_0.5-10_Rasia_1e7_core_ex.txt"

snapnum = 25
H = 67.9

#fig = plt.figure(figsize=(7,6.5))
fig = plt.figure(figsize=(12,10))

bins = np.logspace(np.log10(0.16), np.log10(1.7), num=10)
x = 10**((np.log10(bins[:-1]) + np.log10(bins[1:]))/2)

labels = ["Ref",
          "Stronger radio",
          "Quasar duty cycle",
          "Best"]

col = ["#f95c57",
       "#6ca3d9",
       "#95c967",
       "#ce6ac4"]
col = ["#525cc8",
       "#d45b46",
       "#a95ac7"]
from palettable.colorbrewer.qualitative import Dark2_8 as pallette
col=pallette.hex_colors
markers = ["D", "s", "o"]
ls=['dashdot', 'dotted', 'dashed', 'solid']
ms = 6.5
lw=2
temp_idx = 5 ## temperature index - 5 for spectral, 15 for emission-weighted, 14 for mass-weighted

best = np.loadtxt("L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_"+str(snapnum).zfill(3)+"/"+filename)
plt.plot(best[:,temp_idx], best[:,1], marker=markers[2], mfc='none', mec=col[3], ms=ms, ls='none', mew=1.2, zorder=4)
## median is plotted below
#for zoom in ["c96", "c128_fixed", "c160", "c192", "c256_fixed", "c320", "c384_fixed", "c448"]:
#==============================================================================
# for zoom in ["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c256_box_Arepo_new", "c320_box_Arepo_new", "c384_box_Arepo_new"]:
#     data = np.loadtxt("/home/nh444/data/project1/L-T_relations/LDRIntRadioEffBHVel/"+zoom+"_"+str(snapnum).zfill(3)+"/"+filename)
#     plt.plot(data[temp_idx], data[1]*67.74**2/H**2, marker='o', mfc=col[3], mec=col[3], mew=1.2, ms=ms, ls='none', zorder=4)
#==============================================================================
#==============================================================================
# #c = ["#c7673e","#78a44f","#6b87c8","#934fce","#e6ab02","#be558f","#47af8d"]
# c = col
# mec = ['none', 'k']
# for i, zoom in enumerate(["c96_box_Arepo_new", "c96_box_Arepo_new_LowSoft", "c128_box_Arepo_new", "c128_box_Arepo_new_LowSoft", "c160_box_Arepo_new", "c160_box_Arepo_new_LowSoft", "c320_box_Arepo_new", "c320_box_Arepo_new_LowSoft", "c384_box_Arepo_new", "c384_box_Arepo_new_LowSoft"]):
#     data = np.loadtxt("/home/nh444/data/project1/L-T_relations/LDRIntRadioEffBHVel/"+zoom+"_"+str(snapnum).zfill(3)+"/"+filename)
#     plt.plot(data[temp_idx], data[1]*67.74**2/H**2, marker='D', mfc=c[int(i/2)], mec=mec[(i+1)%2], mew=0.5, ms=ms, ls='none', zorder=4, label=zoom.replace("_", "\_"))
#     
#==============================================================================
#==============================================================================
# data2 = np.loadtxt("L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_0.5-10_Rasia_c4_Tvir_thresh_2.5_on_lum.txt")
# plt.plot(data2[:,temp_idx], data2[:,1], marker=markers[2], mfc='none', mec='blue', alpha=0.5, ms=ms, ls='none', mew=1.0, zorder=4)
# med, bin_edges, n = stats.binned_statistic(data2[:,temp_idx], data2[:,1], statistic='median', bins=bins)
# plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c='blue', ls=ls[3], lw=lw, dash_capstyle='round', label="Best - bubbles excl. in luminosity", zorder=4)#(APEC $T_{min}=0.01$ keV)
# for i in [2,14,28,89]:
#     plt.gca().annotate(str(i), xytext=(best[i,temp_idx], best[i,1]), xycoords='data', xy=(data2[i,temp_idx], data2[i,1]), textcoords='data', arrowprops=dict(arrowstyle="->", connectionstyle="arc3"))
# for zoom in ["c96", "c128_fixed", "c160", "c192", "c256_fixed", "c320", "c384_fixed", "c448"]:
#     data = np.loadtxt("/home/nh444/data/project1/L-T_relations/LDRIntRadioEffBHVel/"+zoom+"_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_0.5-10_Rasia_c4_Tvir_thresh_2.5_on_lum.txt")
#     plt.plot(data[temp_idx], data[1]*67.74**2/H**2, marker='D', mfc='none', mec='blue', mew=1.0, ms=ms, ls='none', zorder=4)
# 
#==============================================================================
data = np.loadtxt("L40_512_NoDutyRadioWeak_HalfThermal_FixEta_"+str(snapnum).zfill(3)+"/"+filename)
#plt.plot(data[:,temp_idx], data[:,1], marker=markers[0], mec='none', mfc=col[0], ms=ms, ls='none', mew=1.0, zorder=4)
med, bin_edges, n = stats.binned_statistic(data[:,temp_idx], data[:,1], statistic='median', bins=bins)
plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[0], ls=ls[2], lw=lw, dash_capstyle='round', label=labels[0], zorder=4)

data = np.loadtxt("L40_512_NoDutyRadioInt_HalfThermal_FixEta_"+str(snapnum).zfill(3)+"/"+filename)
#plt.plot(data[:,temp_idx], data[:,1], marker=markers[0], mec='none', mfc=col[1], ms=ms, ls='none', mew=1.0, zorder=4)
med, bin_edges, n = stats.binned_statistic(data[:,temp_idx], data[:,1], statistic='median', bins=bins)
plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[1], ls=ls[1], lw=lw, dash_capstyle='round', label=labels[1], zorder=4)

data = np.loadtxt("L40_512_LowDutyRadioWeak_HalfThermal_FixEta_"+str(snapnum).zfill(3)+"/"+filename)
#plt.plot(data[:,temp_idx], data[:,1], marker=markers[1], mec='none', mfc=col[2], ms=ms, ls='none', mew=1.0, zorder=4)
med, bin_edges, n = stats.binned_statistic(data[:,temp_idx], data[:,1], statistic='median', bins=bins)
plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[2], ls=ls[0], lw=lw, dash_capstyle='round', label=labels[2], zorder=4)

#data = np.loadtxt("L40_512_LowDutyRadioWeak_HalfThermal_FixEta_BHChangeVel_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_default2.txt")#0.3-10_fit_range
#plt.plot(data[:,temp_idx], data[:,1], marker='D', mfc='none', mec=col[2], ms=4, ls='none', mew=1.0)
#med, bin_edges, n = stats.binned_statistic(data[:,temp_idx], data[:,1], statistic='median', bins=bins)
#plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[2], lw=lw, label=r"QM duty cycle ($\delta t = 0.02$)")

data = np.loadtxt("L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_"+str(snapnum).zfill(3)+"/"+filename)
med, bin_edges, n = stats.binned_statistic(data[:,temp_idx], data[:,1], statistic='median', bins=bins)
plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[3], ls=ls[3], lw=lw, dash_capstyle='round', label=labels[3], zorder=4)#(APEC $T_{min}=0.01$ keV)

data = np.loadtxt("L40_512_LDRIntRadioEffBHVel_"+str(snapnum).zfill(3)+"/"+filename)
med, bin_edges, n = stats.binned_statistic(data[:,temp_idx], data[:,1], statistic='median', bins=bins)
plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[4], ls=ls[3], lw=lw, dash_capstyle='round', label=labels[3]+" larger softenings", zorder=4)#(APEC $T_{min}=0.01$ keV)
plt.plot(data[:,temp_idx], data[:,1], marker=markers[2], mfc='none', mec=col[4], ms=ms, ls='none', mew=1.2, zorder=4)
for zoom in ["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c256_box_Arepo_new", "c320_box_Arepo_new", "c384_box_Arepo_new"]:
    data = np.loadtxt("/home/nh444/data/project1/L-T_relations/LDRIntRadioEffBHVel/"+zoom+"_"+str(snapnum).zfill(3)+"/"+filename)
    plt.plot(data[temp_idx], data[1]*67.74**2/H**2, marker='o', mfc=col[4], mec=col[4], mew=1.2, ms=ms, ls='none', zorder=4)

#==============================================================================
# data = np.loadtxt("L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_0.5-10_none.txt")
# #plt.plot(data[:,temp_idx], data[:,1], marker=markers[2], mfc='none', mec='k', ms=2, ls='none', mew=1.2, zorder=4)
# med, bin_edges, n = stats.binned_statistic(data[:,temp_idx], data[:,1], statistic='median', bins=bins)
# plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c='k', ls=ls[3], lw=lw, dash_capstyle='round', label=labels[3]+" (no cuts, no T thresh)", zorder=4)#(APEC $T_{min}=0.01$ keV)
# for zoom in ["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c256_box_Arepo_new"]:#, "c384_box_Arepo_new"]:
#     data = np.loadtxt("/home/nh444/data/project1/L-T_relations/LDRIntRadioEffBHVel/"+zoom+"_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_0.5-10_none.txt")
#     plt.plot(data[temp_idx], data[1]*67.74**2/H**2, marker='o', mfc='k', mec='k', mew=1.2, ms=ms, ls='none', zorder=4)
#==============================================================================
    
#==============================================================================
# data = np.loadtxt("L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_0.5-10_none_Tvir_thresh_2.5.txt")
# plt.plot(data[:,temp_idx], data[:,1], marker=markers[2], mfc='none', mec='0.3', ms=4, ls='none', mew=1.2, zorder=4)
# med, bin_edges, n = stats.binned_statistic(data[:,temp_idx], data[:,1], statistic='median', bins=bins)
# plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c='0.3', ls=ls[3], lw=lw, dash_capstyle='round', label=labels[3]+" (no cuts T$<2.5 T_{200}$)", zorder=4)#(APEC $T_{min}=0.01$ keV)
# for zoom in ["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c256_box_Arepo_new", "c384_box_Arepo_new"]:
#     data = np.loadtxt("/home/nh444/data/project1/L-T_relations/LDRIntRadioEffBHVel/"+zoom+"_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_0.5-10_none_Tvir_thresh_2.5.txt")
#     plt.plot(data[temp_idx], data[1]*67.74**2/H**2, marker='o', mfc='0.3', mec='0.3', mew=1.2, ms=ms, ls='none', zorder=4)
#==============================================================================

#==============================================================================
# ## no T thresh
# data = np.loadtxt("L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_0.5-10_Rasia_1e7.txt")
# plt.plot(data[:,temp_idx], data[:,1], marker=markers[2], mfc='none', mec='r', ms=ms, ls='none', mew=1.2, zorder=4)
# med, bin_edges, n = stats.binned_statistic(data[:,temp_idx], data[:,1], statistic='median', bins=bins)
# plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c='r', ls=ls[3], lw=1.5, dash_capstyle='round', label="Best ($N=10^7$ no T thresh)", zorder=4)
# for zoom in ["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c256_box_Arepo_new", "c384_box_Arepo_new"]:
#     data = np.loadtxt("/home/nh444/data/project1/L-T_relations/LDRIntRadioEffBHVel/"+zoom+"_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_0.5-10_Rasia_1e7.txt")
#     plt.plot(data[temp_idx], data[1]*67.74**2/H**2, marker='D', mfc='r', mec='none', mew=1.2, ms=ms, ls='none', zorder=4)
# ## with T thresh
# data = np.loadtxt("L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_0.5-10_Rasia_1e7_Tvir_thresh_3.0.txt")
# plt.plot(data[:,temp_idx], data[:,1], marker=markers[2], mfc='none', mec='g', ms=ms-1, ls='none', mew=1.2, zorder=4)
# med, bin_edges, n = stats.binned_statistic(data[:,temp_idx], data[:,1], statistic='median', bins=bins)
# plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c='g', ls=ls[3], lw=1.5, dash_capstyle='round', label="Best ($N=10^7$ T $< 3.0 T_{200}$)", zorder=4)#(APEC $T_{min}=0.01$ keV)
# for zoom in ["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c256_box_Arepo_new", "c384_box_Arepo_new"]:
#     data = np.loadtxt("/home/nh444/data/project1/L-T_relations/LDRIntRadioEffBHVel/"+zoom+"_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_0.5-10_Rasia_1e7_Tvir_thresh_3.0.txt")
#     plt.plot(data[temp_idx], data[1]*67.74**2/H**2, marker='D', mfc='g', mec='none', mew=1.2, ms=ms-1, ls='none', zorder=4)
#     
#==============================================================================
#==============================================================================
# data = np.loadtxt("L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_0.5-10_Rasia_1e7_core_ex.txt")
# plt.plot(data[:,temp_idx], data[:,1], marker=markers[2], mfc='none', mec='b', ms=ms-2, ls='none', mew=1.2, zorder=4)
# med, bin_edges, n = stats.binned_statistic(data[:,temp_idx], data[:,1], statistic='median', bins=bins)
# plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c='b', ls=ls[3], lw=1.5, dash_capstyle='round', label="Best ($N=10^7$ no T thresh core excised)", zorder=4)
# for zoom in ["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c256_box_Arepo_new", "c384_box_Arepo_new"]:
#     data = np.loadtxt("/home/nh444/data/project1/L-T_relations/LDRIntRadioEffBHVel/"+zoom+"_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_0.5-10_Rasia_1e7_core_ex.txt")
#     plt.plot(data[temp_idx], data[1]*67.74**2/H**2, marker='D', mfc='b', mec='none', mew=1.2, ms=ms-2, ls='none', zorder=4)
#==============================================================================
    
#==============================================================================
# data = np.loadtxt("L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_0.5-10_Rasia_1e7_core_ex_L.txt")
# plt.plot(data[:,temp_idx], data[:,1], marker=markers[2], mfc='none', mec='purple', ms=ms-2, ls='none', mew=1.2, zorder=4)
# #plt.gca().annotate("2", xy=(data[2,temp_idx], data[2,1]), xytext=(0,0), ha='right', textcoords='offset points')
# data1 = np.loadtxt("L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_0.5-10_Rasia_1e7.txt")
# plt.gca().annotate("2", xy=(data[2,temp_idx], data[2,1]), xycoords='data', xytext=(data1[2,temp_idx], data1[2,1]), textcoords='data', arrowprops=dict(edgecolor='purple', arrowstyle="->", connectionstyle="arc3"))
# data2 = np.loadtxt("L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_0.5-10_Rasia_1e7_Tvir_thresh_3.0.txt")
# plt.gca().annotate("2", xy=(data2[2,temp_idx], data2[2,1]), xycoords='data', xytext=(data1[2,temp_idx], data1[2,1]), textcoords='data', arrowprops=dict(edgecolor='g', arrowstyle="->", connectionstyle="arc3"))
# med, bin_edges, n = stats.binned_statistic(data[:,temp_idx], data[:,1], statistic='median', bins=bins)
# plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c='purple', ls=ls[3], lw=1.5, dash_capstyle='round', label="Best ($N=10^7$ no T thresh core excised)", zorder=4)
# for zoom in ["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c256_box_Arepo_new"]:#, "c384_box_Arepo_new"]:
#     data = np.loadtxt("/home/nh444/data/project1/L-T_relations/LDRIntRadioEffBHVel/"+zoom+"_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_0.5-10_Rasia_1e7_core_ex.txt")
#     plt.plot(data[temp_idx], data[1]*67.74**2/H**2, marker='D', mfc='purple', mec='none', mew=1.2, ms=ms-2, ls='none', zorder=4)
# 
#==============================================================================
## Phase space cut comparison
'''
### 0.5-2 keV fit range for box and zooms LDRIntRadioEffBHVel with original phase space cut
colour = '#a73ed7'#purple
marker='s'
filename="L-T_data_apec_0.5-2.txt"
data = np.loadtxt("L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_"+str(snapnum).zfill(3)+"/"+filename)## 0.5-2 kev fit range
plt.plot(data[:,temp_idx], np.log10(data[:,1]), marker=marker, mec=colour, mfc='none', ms=5, ls='none', mew=1.2)
#med, bin_edges, n = stats.binned_statistic(data[:,temp_idx], data[:,1], statistic='median', bins=bins)
#plt.plot(x[np.isfinite(med)], np.log10(med[np.isfinite(med)]), c='purple', lw=lw, label=r"Best box (default cut)")#(APEC $T_{min}=0.01$ keV)
zooms = ["c96", "c128_fixed", "c160", "c192", "c256_fixed", "c320", "c384_fixed", "c448"]
for zoom in zooms:
    data = np.loadtxt("/home/nh444/data/project1/L-T_relations/LDRIntRadioEffBHVel/"+zoom+"_"+str(snapnum).zfill(3)+"/"+filename)
    plt.plot(data[temp_idx], np.log10(data[1])+np.log10(67.74*67.74/(H*H)), marker=marker, mec=colour, mfc='none', mew=1.2, ms=5, ls='none', zorder=3)#(APEC $T_{min}=0.01$ keV)

### As above but with Rasia2012 phase space cut (0.5-2 keV fit range for box and zooms LDRIntRadioEffBHVel)
colour = '#ef3a24'#red
marker='o'
data = np.loadtxt("L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_0.5-2_Rasia_c4.txt")## 0.5-2 kev fit range
plt.plot(data[:,temp_idx], np.log10(data[:,1]), marker=marker, mec=colour, mfc='none', ms=5, ls='none', mew=1.2, label="Rasia+ 2012 cut")
#med, bin_edges, n = stats.binned_statistic(data[:,temp_idx], data[:,1], statistic='median', bins=bins)
#plt.plot(x[np.isfinite(med)], np.log10(med[np.isfinite(med)]), c=colour, lw=lw, label=r"Best box (Rasia+ 2012 cut)")
for zoom in zooms:
    data = np.loadtxt("/home/nh444/data/project1/L-T_relations/LDRIntRadioEffBHVel/"+zoom+"_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_0.5-2_Rasia_c4.txt")
    plt.plot(data[temp_idx], np.log10(data[1])+np.log10(67.74*67.74/(H*H)), marker=marker, mec=colour, mfc='none', mew=1.2, ms=5, ls='none', zorder=3)#(APEC $T_{min}=0.01$ keV)
'''

c = '0.7'
zorder=0
gems = np.genfromtxt(obs_dir+"gems_project_groups_L_T_table.csv", delimiter=",")
gems_z = gems[:,19] / 299792.458 ## z ~ v/c
Ez = np.sqrt((0.3*(1+gems_z)**3 + 0.7))
mask = (gems[:,15] > 60)
plt.errorbar(gems[mask,2], 10**gems[mask,8]*(70.0*70.0/(H*H)) / Ez[mask], xerr=gems[mask,3], yerr=gems[mask,9]*2.303*10**gems[mask,8]*(70.0*70.0/(H*H)) / Ez[mask], c=c, mec='none', marker='s', ls='none', ms=4, zorder=zorder, label=r"Osmond \& Ponman 2004")#(G-sample) (groups)
#mask = (gems[:,15] < 60)
#plt.errorbar(gems[mask,2], gems[mask,8]*(70.0*70.0/(H*H)), xerr=gems[mask,3], yerr=gems[mask,9]*2.303*10**gems[mask,8]*(70.0*70.0/(H*H)), c='#5eb769', mec='none', marker='s', ls='none', ms=4, label=r"Osmond \& Ponman 2004 galactic haloes (H-sample)")

#ACCEPT = np.genfromtxt(obs_dir+"accept_main_table.csv", delimiter=",", dtype=None)
#ACCEPT = ACCEPT[ACCEPT[:,5].argsort()] ## sort by increasing temperature
#mask = (ACCEPT['f6'] > 0.0) & (ACCEPT['f5'] > 0.0) #exclude luminosities and temp equal to zero
#plt.errorbar(ACCEPT['f5'][mask], ACCEPT['f6'][mask]*1e44*(70.0*70.0/(H*H)), mfc='red', mec='none', marker='D', ls='none', ms=3, label="ACCEPT Archive")
## Annotate points with object names
#for i in range(0, len(ACCEPT['f0'])):
    #if mask[i]: plt.gca().annotate(ACCEPT['f0'][i], xy=(ACCEPT['f5'][i], ACCEPT['f6'][i]*1e44*(70.0*70.0/(H*H))), xytext=(6,0), textcoords='offset points', fontsize=6)

Zou = np.genfromtxt(obs_dir+"ZouMaughan2016.csv", delimiter=",")
Ez = np.sqrt((0.3*(1+Zou[:,1])**3 + 0.7))
plt.errorbar(Zou[:,3], Zou[:,6]*1e43/Ez*(70.0*70.0/(H*H)), xerr=[abs(Zou[:,5]), Zou[:,4]], yerr=Zou[:,7], c=c, mec='none', marker='o', ls='none', ms=4, zorder=zorder, label="Zou+ 2016")
#Zou_fit = lambda T: (np.asarray(T) / 2.0)**3.28 * 1e43 * 3.18
#plt.plot([0.1, 20], Zou_fit([0.1, 20]), c='#fa79a5', ls='dashdot', label="Zou+ 2016 non-bias-corrected best-fit", zorder=3)
#Zou_fit_cor = lambda T: (np.asarray(T) / 2.0)**3.29 * 1e43 * 2.58
#plt.errorbar([0.1, 20], Zou_fit_cor([0.1, 20]), c='#fa79a5', ls='dashed', label="Zou+ 2016 bias-corrected best-fit", zorder=3)

#Sun = np.genfromtxt("/data/vault/nh444/ObsData/L-T/Sun2009.csv", delimiter=",")
#mask = np.isfinite(Sun[:,6])
## This uses the L-T directly from Horner 2001, not from the data table
#plt.errorbar(Sun[mask,9], 10**Sun[mask,8]*(50.0*50.0/(H*H)), xerr=Sun[mask,10], c='darkblue', mec='none', marker='D', ls='none', ms=4, label=r"Sun+ 2009 sample \& Horner 2001 L-T", zorder=3)##e60000
## This uses the L-T directly from the paper -
#plt.errorbar(Sun[mask,9], 10**Sun[mask,6]*(Sun[mask,7])*(50.0*50.0/(H*H)), xerr=Sun[mask,10], c='red', mec='none', marker='D', ls='none', ms=4, label="Sun+ 2009 sample & Horner 2001 L-T", zorder=3)
#plt.errorbar(Sun[:,1], Sun[:,4]*44.0*(72.0*72.0/(H*H)), xerr=[abs(Sun[:,3]), Sun[:,2]], c='#ff00bf', mec='none', marker='D', ls='none', ms=4, label="Sun+ 2009 sample & Zhang+ 2011 luminosities", zorder=3)

#Vikh = np.genfromtxt("/data/vault/nh444/ObsData/profiles/Vikhlinin_profiles/Vikhlinin2006_maintable.csv", delimiter=",")
#mask = np.isfinite(Vikh[:,19])
#plt.errorbar(Vikh[mask,21], 10**Vikh[mask,23]*(50.0*50.0/(H*H)), xerr=Vikh[mask,22], c=c, mec='none', marker='D', ls='none', ms=4, label=r"Vikhlinin+ 2006 sample \& Horner 2001 L-T", zorder=3)
## This uses the L-T directly from Horner 2001, not from the data table
#plt.errorbar(Vikh[mask,21], 10**Vikh[mask,19]*(Vikh[mask,20])*(50.0*50.0/(H*H)), xerr=Vikh[mask,22], c='green', mec='none', marker='D', ls='none', ms=4, label="Vikhlinin+ 2006 sample & Horner 2001 L-T", zorder=3)
#plt.errorbar(Vikh[:,3], (Vikh[:,13]*1e44*(72.0*72.0/(H*H)), xerr=Vikh[:,4], yerr=Vikh[:,14], c='#006080', mec='none', marker='D', ls='none', ms=4, label="Vikhlinin+ 2006 sample & Zhang+ 2011 luminosities", zorder=3)

### variable core excision radius for temperature but luminosities are not core-excised
Bhara = np.genfromtxt(obs_dir+"Bharadwaj2015.csv", delimiter=",")
Ez = np.sqrt((0.27*(1+Bhara[:,8])**3 + 0.73))
## I'm not totally certain these luminosities are within r500 but I think they are (see Bharadwaj+ 2015 paper)
plt.errorbar(Bhara[:,1], Bhara[:,4]*1e43*(71.0*71.0/(H*H)) / Ez, xerr=[abs(Bhara[:,3]), Bhara[:,2]], yerr=Bhara[:,5]*1e43*(71.0*71.0/(H*H)) / Ez, c=c, mec='none', marker='v', ls='none', ms=5, zorder=zorder, label="Bharadwaj+ 2015")
#Bhara_fit = lambda T: 5e43 * 10**0.08 * (np.asarray(T) / 3.0)**3.2
#plt.plot([0.1, 20], (Bhara_fit([0.1, 20])), c='#9f9fff', ls='dashed', label="Bharadwaj+ 2015 best-fit (bias-corrected)", zorder=3)
#Bhara_fit_uncor = lambda T: 5e43 * 10**-0.01 * (np.asarray(T) / 3.0)**2.17
#plt.plot([0.1, 20], (Bhara_fit_uncor([0.1, 20])), c='#9f9fff', ls='dotted', label="Bharadwaj+ 2015 best-fit (no bias correction)", zorder=3)
#Bhara_fit_SCC = lambda T: 5e43 * 10**0.3 * (np.asarray(T) / 3.0)**3.6
#plt.errorbar([0.1, 20], (Bhara_fit_SCC([0.1, 20])), ls='dashed', label="Bharadwaj+ 2015 best-fit (SCC)", zorder=3)
#Bhara_fit_NSCC = lambda T: 5e43 * 10**-0.17 * (np.asarray(T) / 3.0)**2.52
#plt.errorbar([0.1, 20], (Bhara_fit_NSCC([0.1, 20])), ls='dashed', label="Bharadwaj+ 2015 best-fit (NSCC)", zorder=3)
#Bhara_fit_CRS = lambda T: 5e43 * 10**0.16 * (np.asarray(T) / 3.0)**3.6
#plt.errorbar([0.1, 20], (Bhara_fit_CRS([0.1, 20])), ls='dashed', label="Bharadwaj+ 2015 best-fit (CRS)", zorder=3)
#Bhara_fit_NCRS = lambda T: 5e43 * 10**0.09 * (np.asarray(T) / 3.0)**3.2
#plt.errorbar([0.1, 20], (Bhara_fit_NCRS([0.1, 20])), ls='dashed', label="Bharadwaj+ 2015 best-fit (NCRS)", zorder=3)

horner = np.genfromtxt(obs_dir+"horner_L_T_table.txt")
Ez = np.sqrt((0.3*(1+horner[:,10])**3 + 0.7)) ## It's not clear what omega they use, could be omega_m = 1 --- they assume a flat universe since they have q0=0.5
plt.errorbar(horner[:,4], 10**horner[:,12]*(50.0*50.0/(H*H)) / Ez, xerr=[horner[:,4]-horner[:,5],horner[:,6]-horner[:,4]], c=c, mec='none', marker='D', ls='none', ms=4, zorder=zorder, label="Horner 2001")

rexcess = np.genfromtxt(obs_dir+"REXCESS_table_Pratt2009.csv", delimiter=",", dtype=None)
CC = rexcess['f22']
disturbed = rexcess['f23']
plt.errorbar(rexcess['f4'], rexcess['f7'] * 1e44 * 70.0*70.0/(H*H), xerr=[np.absolute(rexcess['f6']), rexcess['f5']], yerr=[np.absolute(rexcess['f9']) * 1e44 * 70.0*70.0/(H*H), rexcess['f8'] * 1e44 * 70.0*70.0/(H*H)], c=c, mec='none', marker='o', ls='none', ms=4, label="Pratt+ 2009")

#anderson = np.genfromtxt(obs_dir+"Anderson2014_lum.csv", delimiter=",")
#plt.errorbar(anderson[:,10], anderson[:,11]+np.log10(70.4*70.4/(H*H)), yerr=anderson[:,12]+np.log10(70.4*70.4/(H*H)), c='#d34044', mec='none', marker='^', ls='none', ms=4, label="ROSAT LBGs (Anderson et al. 2014)")

plt.xlim(0.2, 20)
plt.ylim(1e40,1e46)
plt.xscale('log')
plt.yscale('log')
plt.ylabel("L$_{500}$ E(z)$^{-1}$ [erg s$^{-1}$]", fontsize=22)#^{\mathrm{bol}}
plt.xlabel(r"T$_{500}$ [keV]", fontsize=22)

handles, labels = plt.gca().axes.get_legend_handles_labels()
n = 10
leg1 = plt.legend(handles[:n], labels[:n], loc='lower right', frameon=False, numpoints=1, ncol=1, fontsize='small')
ax = plt.gca().add_artist(leg1)
plt.legend(handles[n:], labels[n:], loc='upper left', frameon=False, borderaxespad=1, numpoints=1, ncol=1, fontsize='small')
#plt.legend(loc='upper left', frameon=False, borderaxespad=1, numpoints=1, fontsize=12, ncol=1)

plt.gca().xaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))

plt.tight_layout(pad=0.45)
#plt.tight_layout()
plt.savefig(outname)#, bbox_inches='tight')
print "Saved to", outname
