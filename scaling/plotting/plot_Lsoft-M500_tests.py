import matplotlib.pyplot as plt
#import matplotlib.ticker as ticker
import numpy as np
import scipy.stats as stats
from os import chdir
chdir('/home/nh444/data/project1/L-T_relations')

outname = "Lsoft-M500_relation.pdf"

snapnum = 25
h = 0.679

filename = "L-T_data_apec_0.5-7_Rasia_c4_Tvir_thresh_2.5_L0.1-2.4.txt"

obs_dir = "/home/nh444/vault/ObsData/L-M/"

fig = plt.figure(figsize=(7,7))
Mmin, Mmax = 10**12.5, 10**15

labels = ["Ref",
          "Stronger radio",
          "Quasar duty cycle",
          "Best"]

from palettable.colorbrewer.qualitative import Dark2_8 as pallette
col=pallette.hex_colors
markers = ["D", "s", "o"]
ls=['dashdot', 'dotted', 'dashed', 'solid']
ms = 6.5
lw=2.5
mew=1.2

## Median profiles
bins = np.logspace(11, 13.5, num=25)
x = 10**((np.log10(bins[:-1]) + np.log10(bins[1:]))/2.0)

data = np.loadtxt("L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_"+str(snapnum).zfill(3)+"/"+filename)
plt.plot(data[:,12] * 1e10 / h, data[:,1], marker=markers[2], mfc='none', mec=col[3], ms=ms, ls='none', mew=mew, zorder=4)
## median is plotted below
#for zoom in ["c96", "c128_fixed", "c160", "c192", "c256_fixed", "c320", "c384_fixed", "c448"]:
for zoom in ["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c256_box_Arepo_new", "c384_box_Arepo_new"]:
    data = np.loadtxt("/home/nh444/data/project1/L-T_relations/LDRIntRadioEffBHVel/"+zoom+"_"+str(snapnum).zfill(3)+"/"+filename)
    plt.plot(data[12] * 1e10 / h, data[1], marker=markers[2], mfc=col[3], mec=col[3], mew=mew, ms=ms, ls='none', zorder=4)

#==============================================================================
# data = np.loadtxt("L40_512_NoDutyRadioWeak_HalfThermal_FixEta_"+str(snapnum).zfill(3)+"/"+filename)#0.3-10_fit_range
# #plt.plot(data[:,12] * 1e10 / h, data[:,1], marker=markers[0], mec='none', mfc=col[0], ms=ms, ls='none', mew=mew, zorder=4)
# med, bin_edges, n = stats.binned_statistic(data[:,12] * 1e10 / h, data[:,1], statistic='median', bins=bins)
# plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[0], ls=ls[2], lw=lw, dash_capstyle='round', label=labels[0], zorder=4)
# 
# data = np.loadtxt("L40_512_NoDutyRadioInt_HalfThermal_FixEta_"+str(snapnum).zfill(3)+"/"+filename)#0.3-10_fit_range
# #plt.plot(data[:,12] * 1e10 / h, data[:,1], marker=markers[0], mec='none', mfc=col[1], ms=ms, ls='none', mew=mew, zorder=4)
# med, bin_edges, n = stats.binned_statistic(data[:,12] * 1e10 / h, data[:,1], statistic='median', bins=bins)
# plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[1], ls=ls[1], lw=lw, dash_capstyle='round', label=labels[1], zorder=4)
# 
# data = np.loadtxt("L40_512_LowDutyRadioWeak_HalfThermal_FixEta_"+str(snapnum).zfill(3)+"/"+filename)#0.3-10_fit_range
# #plt.plot(data[:,12] * 1e10 / h, data[:,1], marker=markers[1], mec='none', mfc=col[2], ms=ms, ls='none', mew=mew, zorder=4)
# med, bin_edges, n = stats.binned_statistic(data[:,12] * 1e10 / h, data[:,1], statistic='median', bins=bins)
# plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[2], ls=ls[0], lw=lw, dash_capstyle='round', label=labels[2], zorder=4)
#==============================================================================

#data = np.loadtxt("L40_512_LowDutyRadioWeak_HalfThermal_FixEta_BHChangeVel_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_default2.txt")#0.3-10_fit_range
#plt.plot(data[:,12] * 1e10 / h, data[:,1], marker='D', mfc='none', mec=col[2], ms=4, ls='none', mew=mew)
#med, bin_edges, n = stats.binned_statistic(data[:,12] * 1e10 / h, data[:,1], statistic='median', bins=bins)
#plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[2], lw=lw, label=r"QM duty cycle ($\delta t = 0.02$)")

data = np.loadtxt("L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_"+str(snapnum).zfill(3)+"/"+filename)#0.3-10_fit_range
med, bin_edges, n = stats.binned_statistic(data[:,12] * 1e10 / h, data[:,1], statistic='median', bins=bins)
plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[3], ls=ls[3], lw=lw, dash_capstyle='round', label=labels[3], zorder=4)#(APEC $T_{min}=0.01$ keV)

## no cuts with T200 threshold
data = np.loadtxt("L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_0.5-7_none_Tvir_thresh_2.5_L0.1-2.4.txt")
plt.plot(data[:,12] * 1e10 / h, data[:,1], marker=markers[2], mfc='none', mec='0.3', ms=ms, ls='none', mew=mew, zorder=4)
med, bin_edges, n = stats.binned_statistic(data[:,12] * 1e10 / h, data[:,1], statistic='median', bins=bins)
plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c='0.3', ls='dashed', lw=lw, dash_capstyle='round', label=labels[3]+" (no cuts T$<2.5 T_{200}$)", zorder=4)#(APEC $T_{min}=0.01$ keV)
for zoom in ["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c256_box_Arepo_new", "c384_box_Arepo_new"]:
    data = np.loadtxt("/home/nh444/data/project1/L-T_relations/LDRIntRadioEffBHVel/"+zoom+"_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_0.5-7_none_Tvir_thresh_2.5_L0.1-2.4.txt")
    plt.plot(data[12] * 1e10 / h, data[1], marker=markers[2], mfc='0.3', mec='0.3', mew=mew, ms=ms, ls='none', zorder=4)
    

c = '0.7'
c2 = '0.4'

COSMOS = np.genfromtxt(obs_dir+"COSMOS_L-M_Leauthaud2010.csv", delimiter=",")
Ez = np.sqrt((0.258*(1+COSMOS[:,8])**3 + 0.742)) ## Note: luminosities are already E(z)^-1
plt.errorbar(COSMOS[:,15]*Ez*1e13*0.72/h, COSMOS[:,2]*1e42*(0.72*0.72/(h*h)), xerr=[abs(COSMOS[:,17])*Ez*1e13*0.72/h, COSMOS[:,16]*Ez*1e13*0.72/h], yerr=COSMOS[:,3]*1e42*(0.72*0.72/(h*h)), c=c2, mec=c2, mfc='none', marker='s', ls='none', ms=6, label="COSMOS (Leauthaud+ 2010)")

Lov = np.genfromtxt(obs_dir+"Lovisari2015.csv", delimiter=",")##fairly sure these luminosities are within r500
Ez = np.sqrt((0.27*(1+Lov[:,1])**3 + 0.73))
plt.errorbar(Lov[:,7]*1e13*0.7/h*Ez, Lov[:,21]*1e43*(0.7*0.7/(h*h))/Ez, xerr=Lov[:,8]*1e13*0.7/h*Ez, yerr=Lov[:,22]*1e43*(0.7*0.7/(h*h))/Ez, c=c, mec='none', marker="^", ls='none', ms=7, label="Lovisari+ 2015")
Lov_fit = lambda M: 1.66 * np.log10(M/(5e13*0.7/h)) - 0.03 + np.log10(1e43) + np.log10(0.7*0.7/(h*h))
plt.errorbar([Mmin, Mmax], [10**Lov_fit(Mmin), 10**Lov_fit(Mmax)], c=c, ls='solid', lw=2, label="Lovisari+ 2015 BC")
#Lov_fit = lambda M: 1.39 * np.log10(M/(5e13*0.7/h)) - 0.12 + np.log10(1e43) + np.log10(0.7*0.7/(h*h))
#plt.errorbar([Mmin, Mmax], [10**Lov_fit(Mmin), 10**Lov_fit(Mmax)], c='r', ls='dashed', lw=2, label="Lovisari+ 2015 BC fit (all)")

eck = np.genfromtxt(obs_dir+"Eckmiller2011.csv", delimiter=",")
## It's not clear if their luminosities are within r500 or not
Ez = np.sqrt((0.3*(1+eck[:,1])**3 + 0.7))
plt.errorbar(eck[:,14]*1e13*0.7/h*Ez, eck[:,2]*1e44*(0.7*0.7/(h*h))/Ez, xerr=eck[:,15]*1e13*0.7/h*Ez, yerr=eck[:,3]/100.0*eck[:,2]*1e44*(0.7*0.7/(h*h))/Ez, c=c, mec='none', marker='D', ls='none', ms=5, label="Eckmiller+ 2011")

REX = np.genfromtxt(obs_dir+"Pratt_2009_L_soft.csv", delimiter=",") ## their R500 (M500) values come from a scaling relation but they plot and fit L-M in Pratt 2009 anyway
Ez = np.sqrt((0.3*(1+REX[:,1])**3 + 0.7))
plt.errorbar(REX[:,2]*1e14*Ez*0.7/h, REX[:,9]*1e44/Ez*(0.7*0.7/(h*h)), yerr=REX[:,10]*1e44/Ez*(0.7*0.7/(h*h)), c=c, mec='none', marker='s', ls='none', ms=5, label="Pratt+ 2009")

Giles = np.genfromtxt(obs_dir+"Giles2015.csv", delimiter=",") ## luminosities within r500 and hydrostatic masses
Ez = np.sqrt((0.282*(1+Giles[:,1])**3 + 0.718))
plt.errorbar(Giles[:,9]*1e14*0.697/h, Giles[:,5]*1e44/Ez*(0.697*0.697/(h*h)), yerr=Giles[:,6]*1e44/Ez*(0.697*0.697/(h*h)), c=c, mec='none', marker='s', ls='none', ms=5, label="Giles+ 2015")
Giles_fit = lambda M: 0.52 * 1e45 * (0.7*0.7/(h*h)) * (M / (1e15*0.7/h))**1.92
plt.errorbar([Mmin, Mmax], [Giles_fit(Mmin), Giles_fit(Mmax)], c=c, ls='dashed', lw=2, label="Giles+ 2015 BC")
Giles_fit = lambda M: 1.15 * 1e45 * (0.7*0.7/(h*h)) * (M / (1e15*0.7/h))**1.34
plt.errorbar([Mmin, Mmax], [Giles_fit(Mmin), Giles_fit(Mmax)], c=c, ls='dashed', lw=2, label="Giles+ 2015")

HIF = np.genfromtxt(obs_dir+"HIFLUGCS.csv", delimiter=",")
Ez = np.sqrt((1.0*(1+HIF[:,1])**3))
plt.errorbar(HIF[:,10]*1e14*0.5/h*Ez, HIF[:,2]*1e44*(0.5*0.5/(h*h))/Ez, xerr=[abs(HIF[:,12]*1e14*0.5/h*Ez), HIF[:,11]*1e14*0.5/h*Ez], c=c, mec='none', marker='o', ls='none', ms=5, label="HIFLUGCS")#"Reiprich \& Boehringer 2002"

#Arnaud_fit = lambda M: 10**0.247 * 1e44*0.7*0.7/(h*h) * (M / (3e14*0.7/h))**1.64
#plt.errorbar([Mmin, Mmax], [Arnaud_fit(Mmin), Arnaud_fit(Mmax)], c='r', ls='solid', lw=2, label="Arnaud+ 2010 BC")

plt.xlim(Mmin, Mmax)
plt.ylim(1e40,1e46)
plt.xscale('log')
plt.yscale('log')
plt.ylabel("L$_{500}^{0.1-2.4 \mathrm{keV}}$ E(z)$^{-1}$ [erg s$^{-1}$]", fontsize=22)
plt.xlabel("M$_{500}$ E(z) [$M_{\odot}$]", fontsize=22)#, labelpad=10)#, fontsize=16)
plt.gca().tick_params(axis='x', which='major', pad=7)

handles, labels = plt.gca().axes.get_legend_handles_labels()
n = 2
leg1 = plt.legend(handles[:n], labels[:n], loc='lower right', frameon=False, numpoints=1, ncol=1)
ax = plt.gca().add_artist(leg1)
plt.legend(handles[n:], labels[n:], loc='upper left', frameon=False, borderaxespad=1, numpoints=1, ncol=1)
#plt.legend(loc='lower right', frameon=False, numpoints=1, ncol=1)#, borderaxespad=1

plt.tight_layout(pad=0.45)
plt.savefig(outname)#, bbox_inches='tight')
print "Saved to", outname
