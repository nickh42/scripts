#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 22 16:56:10 2017

@author: nh444
"""
import numpy as np
import matplotlib.pyplot as plt

fig = plt.figure(figsize=(7,7))
plt.xscale('log')
plt.yscale('log')
plt.xlabel("Measured WL mass [10$^{14}$ M$_{\odot}$]")
plt.ylabel("Mass from WL M-T relation [10$^{14}$ M$_{\odot}$]")
xmin, xmax = 1e13, 1e15
plt.xlim(xmin, xmax)
plt.ylim(xmin, xmax)
plt.errorbar([xmin, xmax], [xmin, xmax], ls='dashed', c='0.5')
c = '0.5'

obs_dir = "/home/nh444/vault/ObsData/L-M/"
XXL = np.genfromtxt(obs_dir+"XXL_L-M_WL.csv", delimiter=",")
ind = np.where(XXL[:,1] <= 100)[0] ## redshift cut
Ez = np.sqrt((0.28*(1+XXL[ind,1])**3 + 0.72))
uplims = (XXL[ind,10]==0) | (XXL[ind,10]>XXL[ind,9]) ## points as upper limits
XXL[ind[uplims],11] *= 0.3 ## make the upper limit arrows smaller
plt.errorbar(XXL[ind,9]*1e14, XXL[ind,24]*1e14, xerr=[np.absolute(XXL[ind,11]*1e14), XXL[ind,10]*1e14], yerr=[XXL[ind,26]*1e14, XXL[ind,25]*1e14], xuplims=uplims, c=c, mec=c, mfc='none', marker="o", ls='none', ms=6, mew=1.2, label="XXL")

fig.savefig("/home/nh444/data/project1/L-T_relations/"+"XXL_mass_comparison.pdf")