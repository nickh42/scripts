# Define which axes names are temperatures.
temperatures = set(["T500", "T300kpc", "T500mw", "T500ew", "T500sl", "T500cex",
                    "T500mwcex"])
# Define which axes names are luminosities.
luminosities = set(["lum", "lum_cex", "lum_brem", "lum_tab"])


def main():
    import numpy as np

    outdir = "/data/curie4/nh444/project1/L-T_relations/"
    #outdir = "/home/nh444/Documents/Fable_evol/"

    basedir = "/data/curie4/nh444/project1/L-T_relations/"
    simdirs = [#"L40_512_LDRIntRadioEff_noBH",
               #"L40_512_NoDutyRadioWeak",
               #"L40_512_NoDutyRadioInt",
               #"L40_512_DutyRadioWeak",
               #"L40_512_LDRIntRadioEffBHVel",
               #"L40_512_LDRHighRadioEff",
               "L40_512_MDRIntRadioEff",
               ]
    labels = [#"No BHs",
              #"Weak radio",
              #"Stronger radio",
              #"Quasar duty cycle",
              #"$\delta t = 10$ Myr",#"Combined",
              #"Longer radio duty",
              "fiducial",
              ]
    #labels = [None]*10
    zoomdirs = [#"/data/curie4/nh444/project1/L-T_relations/LDRIntRadioEffBHVel/",
                #"",
                "/data/curie4/nh444/project1/L-T_relations/MDRInt/"]
    zooms = [["c96_MDRInt", "c128_MDRInt", "c160_MDRInt", "c192_MDRInt",
              "c224_MDRInt", "c256_MDRInt", "c288_MDRInt",
              "c320_MDRInt",
              "c352_MDRInt",
              "c384_MDRInt", "c448_MDRInt", "c480_MDRInt",
              "c512_MDRInt",
              "c112_MDRInt", "c144_MDRInt", "c176_MDRInt", "c208_MDRInt",
              "c240_MDRInt", "c272_MDRInt", "c304_MDRInt", "c336_MDRInt",
              "c368_MDRInt", "c400_MDRInt", "c410_MDRInt", "c432_MDRInt",
              "c464_MDRInt",
              "c496_MDRInt_SUBF",
              ]]
    #simdirs = ["MDRInt/c448_MDRInt"]
    #zoomdirs = ["",""]
    #zooms = [[]]
    do_fit = True  # fit the simulation data with a power-law

    projected = False  # if True, plot projected aperture values, otherwise 3D spherical
    # cosmology to scale to
    h_scale = 0.6774
    omega_m = 0.3089
    omega_l = 0.6911

    figsize = (7, 7)
    col = ["#c7673e", "#78a44f", "#6b87c8",
           "#934fce", "#e6ab02", "#be558f", "#47af8d"]
    # dark blue, orange, light blue, dark red
    col = ['#24439b', '#ED7600', '#50BFD7', '#B94400']
    hlightcol = '#8292BF'
    ls = ['solid']*len(simdirs)
    markers = ['D', 's', 'o', '^', '>', 'v', '<']
    ms = 8
    lw = 3
    mew = 1.8
    binwidth = 0.2  # dex for mass in Msun

    kwargs = {'projected': projected,
              'zoomdirs': zoomdirs, 'zooms': zooms,
              'h_scale': h_scale, 'omega_m': omega_m, 'omega_l': omega_l,
              'basedir': basedir, 'outdir': outdir, 'figsize': figsize,
              'mec': col, 'hlightcol': hlightcol,
              'markers': markers, 'ls': ls, 'ms': ms, 'lw': lw, 'mew': mew,
              'binwidth': binwidth,
              'do_fit': do_fit, 'M500min_fit': 1e14, 'M_power_fit': 0.5,
              'fit_lw': 3, 'fit_alpha': 1.0}

    # sim metallicity comparison
#    simdirs = ["L40_512_MDRIntRadioEff"]
#    filenames = ["L-T_0.5-10_Z0.3.hdf5", "L-T_0.5-10_Zsim.hdf5"]
#    labels = ["$Z=0.3\,Z_{\odot}$", "$Z=Z_{\mathrm{sim}}$"]
#    outname = "L-T_MDRInt_metallicity.pdf"
#    plot_relation(simdirs, filenames, ["T500", "lum"], snapnum=25, projected=projected, axlabels=axlabels, labels=labels, zoomdirs=zoomdirs, zooms=zooms, h_scale=h_scale, omega_m=omega_m, omega_l=omega_l, basedir=basedir, outname=outname, outdir=outdir, figsize=figsize, idxScat=idxScat, idxMed=idxMed, medcol=medcol, mec=medcol, markers=markers, ls=ls, ms=ms, lw=lw, mew=mew, zorder=zorder, binwidth=binwidth)

    # no BHs comparison
#    simdirs = ["L40_512_LDRIntRadioEff_noBH", "L40_512_MDRIntRadioEff"]
#    labels = ["no BHs", "fiducial"]
#    filenames = ["L-T_0.5-10_z0.0_exp1e9_Tvir4.hdf5"]
#    filenames = ["L-T_0.2-12_z0.0_Athena_exp1e8_Tvir4_metals_wabs.hdf5"]
#    kwargs['zoomdirs'] = ["", ""]
#    outname = "M-T_z0.0_noBH.pdf"
#    plot_relation(simdirs, filenames, ["T500mw", "mass"], plot_obs=False,
#                  use_non_centrals=False,
#                  #axeslims=[[1e12, 3e15], [1e40, 1e46]],
#                  #plot_scat=True,
#                  plotlabel="$z=0.0$", labels=labels, outname=outname,
#                  verbose=True, **kwargs)

#    # various redshifts
#    for z in [1.2, 1.8]:
##        filenames = ["L-T_0.5-10_z"+"{:.1f}".format(z)+"_exp1e9_Tvir4_metals_wabs.hdf5"]
#        filenames = ["L-T_0.2-12_z"+"{:.1f}".format(z)+"_Athena_exp1e8_Tvir4_metals_wabs.hdf5"]
#        outname = "L-T_z"+"{:.1f}".format(z)+".pdf"
#        plot_relation(simdirs, filenames, ["gasmass", "lum"], z=z,
#                      # axeslims=[[0.5, 10], None],
#                      plotlabel="$z="+"{:.1f}".format(z)+"$", labels=labels,
#                      outname=outname, **kwargs)

#    # different redshifts on one plot
#    z = [0.4]
#    filenames = ["L-T_0.2-12_z"+"{:.1f}".format(zz)+"_Athena_exp1e8_Tvir4_metals_wabs.hdf5" for zz in z]
#    labels = ["$z = " + "{:.1f}".format(zz) + "$" for zz in z]
#    kwargs['hlightcol'] = None
#    #kwargs['zooms'] = [["c480_MDRInt", "c512_MDRInt"]]
#    plot_relation(simdirs, filenames, ["mass", "T500mw"],
#                  plot_obs=False, labels=labels, **kwargs)

#    # Multiple file comparison
#    z = 2.0
#    filenames = ["L-T_0.3-10_z"+"{:.1f}".format(z)+"_exp1e9_Tvir4.hdf5",
#                 "L-T_0.3-10_z"+"{:.1f}".format(z)+"_exp1e10_Tvir4.hdf5",
#                 "L-T_0.5-10_z"+"{:.1f}".format(z)+"_exp1e11_Tvir4_outliers.hdf5",
#                 "L-T_0.1-10_z"+"{:.1f}".format(z)+"_exp1e11_Tvir4_outliers.hdf5",
#                 "L-T_0.3-10_z"+"{:.1f}".format(z)+"_exp1e11_Tvir4_outliers.hdf5",
#                 "L-T_0.4-10_z"+"{:.1f}".format(z)+"_exp1e11_Tvir4_outliers.hdf5",
#                 ]
#    labels = ["$10^9$ s", "$10^{10}$ s", "$10^{11}$ s",
#              ]
#    plot_relation(simdirs, filenames, ["mass", "lum"], z=z,
#                  #axeslims=[[1e12, 3e15], [1e40, 1e46]],
#                  use_non_centrals=False,
#                  #LabelGrps=[29, 38, 63, 85, 107, 123, 157],
#                  plotlabel="$z="+"{:.1f}".format(z)+"$", labels=labels,
#                  **kwargs)

# =============================================================================
# #    # Compare spectroscopic and mass-weighted temperatures.
#     for z in [0.0, 0.2, 0.4]:
# #        kwargs['hlightcol'] = None
# #        filenames = ["L-T_0.5-10_z"+"{:.1f}".format(z)+"_exp1e9_Tvir4_metals_wabs.hdf5",
# #                     "L-T_0.5-10_z"+"{:.1f}".format(z)+"_Chandra_exp1e4_Tvir4_metals_wabs.hdf5"]
# #        labels = ["$10^6$ s", "$10^4$ s"]
# #        filenames = ["L-T_0.3-10_z"+"{:.1f}".format(z)+"_exp1e9_Tvir4.hdf5",
# #                     "L-T_0.5-10_z"+"{:.1f}".format(z)+"_exp1e9_Tvir4.hdf5",
# #                     "L-T_0.7-10_z"+"{:.1f}".format(z)+"_exp1e9_Tvir4.hdf5"]
# #        labels = None
#         filenames = ["L-T_0.2-12_z" + "{:.1f}".format(z) + "_Athena_exp1e8_Tvir4_metals_wabs.hdf5"]
# #        filenames = ["L-T_0.2-12_z" + "{:.1f}".format(z) + "_Athena_exp1e8_Tvir4_metals_wabs_source_frame.hdf5"]
# #        simdirs = ["L40_512_LDRIntRadioEff_noBH", "L40_512_MDRIntRadioEff"]
# #        labels = ["no BHs", "fiducial"]
# #        kwargs['zoomdirs'] = ["", ""]
#         plot_relation(simdirs, filenames, ["T500", "T500mw"],
#                       axeslims=[[0.1, 10], [0.1, 10]],
#                       #LabelZooms=range(26), LabelGrps=[0],
#                       plotlabel="$z="+"{:.1f}".format(z)+"$",
#                       labels=labels, **kwargs)
# =============================================================================

#    # Plot ratio of spectroscopic and mass-weighted temperature
#    z = [0.0, 1.0, 1.8]
#    #filenames = ["L-T_0.5-10_z"+"{:.1f}".format(zz)+"_exp1e9_Tvir4.hdf5" for zz in z]
#    filenames = ["L-T_0.2-12_z" + "{:.1f}".format(zz) + "_Athena_exp1e8_Tvir4_metals_wabs.hdf5" for zz in z]
#    labels = ["$z = " + "{:.1f}".format(zz) + "$" for zz in z]
#    plot_ratio([r"$\mathrm{T_{mw}}$ [keV]",
#                r"$\mathrm{(T_{spec} - T_{mw}) / T_{mw}}$"],
#               None,#np.logspace(np.log10(0.5), np.log10(5), num=8),
#               simdirs, filenames, ["T500mw", "T500"], labels=labels,
#               axeslims=[[0.5, 10], [-0.5, 0.5]], **kwargs)

#    # Compare 0.3 solar to simulation metallicities
#    for z in [0.2, 0.6, 1.0, 1.4, 1.8]:
#        filenames = ["L-T_0.5-10_z"+"{:.1f}".format(z)+"_exp1e9_Tvir4.hdf5",
#                     "L-T_0.5-10_z"+"{:.1f}".format(z)+"_exp1e9_Tvir4_metals.hdf5"]
#        labels = ["0.3 solar", "sim metals"]
#        plot_relation(simdirs, filenames, ["T500", "lum"], z=z,
#                      LabelGrps=range(10), ArrowGrps=range(10),
#                      labels=labels, **kwargs)

#    # Compare responses.
#    for z in [1.0, 1.4, 1.8]:
#        filenames = ["L-T_0.5-10_z"+"{:.1f}".format(z)+"_exp1e9_Tvir4_metals.hdf5",
#                     "L-T_0.5-10_z"+"{:.1f}".format(z)+"_exp1e9_Tvir4_metals_acisi_cy20.hdf5"]
#        labels = ["Fiducial", "ACIS-I Cycle 20"]
#        plot_relation(simdirs, filenames, ["T500", "lum"], z=z,
#                      LabelGrps=range(10), ArrowGrps=range(10),
#                      labels=labels, **kwargs)

#    # Compare 3D vs 2D
#    z = 2.0
#    fname = "L-T_0.5-10_z"+"{:.1f}".format(z)+"_exp1e9_Tvir4.hdf5"
#    import copy
#    kwargs1 = copy.deepcopy(kwargs)
#    kwargs1['projected'] = False
#    # kwargs1['use_non_centrals'] = False
#    kwargs1['hlightcol'] = None
#    kwargs1['z'] = z
#    kwargs2 = copy.deepcopy(kwargs)
#    kwargs2['projected'] = True
#    # kwargs2['use_non_centrals'] = False
#    kwargs2['hlightcol'] = None
#    kwargs2['mec'] = kwargs2['mec'][1:]
#    kwargs2['z'] = z
#
#    compare_relations(simdirs, ["mass", "L"],
#                      fname, kwargs1, fname, kwargs2, plot_ratio=True)


    # tabulated luminosity vs spec luminosity, emission vs mass-weighted temp
#    z = 0.2
#    filenames = ["L-T_0.5-10_z"+"{:.1f}".format(z)+"_exp1e9_Tvir4.hdf5"]
#    outname = "Lt-Lspec_z"+"{:.1f}".format(z)+".pdf"
#    plot_relation(simdirs, filenames, ["lum", "lum_tab"], z=z,
#                  use_non_centrals=False, #hlightzooms=True,
#                  plotlabel="$z="+"{:.1f}".format(z)+"$",
#                  outname=outname, **kwargs)
#    outname = "Tew-Tmw_z"+"{:.1f}".format(z)+".pdf"
#    plot_relation(simdirs, filenames, ["T500mw", "T500ew"], z=z,
#                  axeslims=[[0.01, 10], [0.01, 10]],
#                  use_non_centrals=False, #hlightzooms=True,
#                  plotlabel="$z="+"{:.1f}".format(z)+"$",
#                  outname=outname, **kwargs)
#    for axes in (["mass", "lum"],
#                 ["mass", "lum_tab"],
#                 ["T500mw", "mass"],
#                 ["T500ew", "mass"]):
#        plot_relation(simdirs, filenames, axes, z=z,
#                      use_non_centrals=False, hlightzooms=True,
#                      plotlabel="$z="+"{:.1f}".format(z)+"$", labels=labels,
#                      outname=outname, **kwargs)


#    # YX--mass
#    filenames = ["L-T_0.5-10_z2.0_exp1e9_Tvir4.hdf5"]
#    outname = "YX-M_z2.0.pdf"
#    plot_relation(simdirs, filenames, ["mass", "YXmw"], z=2.0,
#                  axeslims=[[1e12, 3e15], [1e9, 1e14]],
#                  LabelGrps=range(0, 300),
#                  plotlabel="$z=2.0$", labels=labels,
#                  outname=outname, **kwargs)

#    plot_relation_multi(simdirs, [0.2, 0.4, 0.6, 0.8], ["mass", "lum"], kwargs,
#                        "L-M_vs_z.pdf", inprefix="L-T_0.5-10", insuffix="_exp1e9_Tvir4")

#    kwargs['axeslims'] = [[1e12, 4e15], None]
#    #kwargs['outdir'] = "/data/vault/nh444/VaultDocs/meetings/scat_vs_mass/"
#    plot_rel = False  # plot the relations as well as the scatter
#    kwargs['axeslims'][1] = [1e40, 1e46]
#    plot_scatter(simdirs, [0.0, 0.6, 1.2], ["mass", "lum"], kwargs,
#                 "scatter_L-M.pdf", suffix="L-M", plot_rel=plot_rel,
#                 inprefix="L-T_0.5-10", insuffix="_exp1e9_Tvir4")
#    kwargs['axeslims'][1] = [1e9, 3e15]
#    plot_scatter(simdirs, [0.0, 0.6, 1.2], ["mass", "YX"], kwargs,
#                 "scatter_YX-M.pdf", suffix="YX-M", plot_rel=plot_rel,
#                 inprefix="L-T_0.5-10", insuffix="_exp1e9_Tvir4")
#    kwargs['axeslims'][1] = [1e10, 3e14]
#    plot_scatter(simdirs, [0.0, 0.6, 1.2], ["mass", "gasmass"], kwargs,
#                 "scatter_Mgas-M.pdf", suffix="Mgas-M", plot_rel=plot_rel,
#                 inprefix="L-T_0.5-10", insuffix="_exp1e9_Tvir4")

#    z = 0.4
#    filenames = ["SZ-M500_z"+"{:.2f}".format(z)+"_SPT.hdf5"]
#    outname = "SZ-M500_z"+"{:.1f}".format(z)+"_SPT.pdf"
#    kwargs['basedir'] = "/data/curie4/nh444/project1/SZ/"
#    kwargs['zoomdirs'] = ["/data/curie4/nh444/project1/SZ/MDRInt/"]
#    plot_relation(simdirs, filenames, ["mass", "SZ"], z=z, plotlabel="$z="+"{:.1f}".format(z)+"$",
#                  SZ_obs={'Planck15': False, 'PlanckLBG':False, 'Wang':True, 'SPT': True},
#                  plot_scat=True, axeslims=[[1e12, 1e15], None], M500min_plot=2e12, do_plot=False,
#                  labels=labels, outname=outname, **kwargs)

#    kwargs['basedir'] = "/data/curie4/nh444/project1/SZ/"
#    kwargs['zoomdirs'] = ["/data/curie4/nh444/project1/SZ/MDRInt/"]
#    kwargs['SZ_obs'] = {'Planck15': False,
#                        'PlanckLBG': False, 'Wang': False, 'SPT': True}
#    plot_relation_multi(simdirs, [0.2, 0.4, 0.6, 0.8], ["mass", "SZ"], kwargs,
#                    "SZ-M500_SPT_vs_z.pdf", inprefix="SZ-M500", insuffix="_SPT")
#    kwargs['axeslims'][1] = [1e-9, 1e-4]
#    plot_scatter(simdirs, [0.2, 0.6, 1.0], ["mass", "SZ"], kwargs,
#                 "scatter_SZ-M500_SPT.pdf", suffix="Y-M", plot_rel=plot_rel,
#                 inprefix="SZ-M500", insuffix="_SPT")
#    kwargs['SZ_obs'] = {'Planck15': True,
#                        'PlanckLBG': False, 'Wang': True, 'SPT': False}
#    kwargs['axeslims'][1] = [1e-9, 1e-3]
#    plot_scatter(simdirs, [0.0, 0.6, 1.2], ["mass", "SZ"], kwargs,
#                 "scatter_Y-M_Planck.pdf", suffix="Y-M", plot_rel=plot_rel,
#                 inprefix="SZ-M500", insuffix="_5r500")
#    kwargs['axeslims'][1] = None

    # ----- SZ relations ----- #
    kwargs['basedir'] = "/data/curie4/nh444/project1/SZ/"
    kwargs['zoomdirs'] = ["/data/curie4/nh444/project1/SZ/MDRInt/"]

    # Compare Y(5r500) and Y(r500)
    kwargs['projected'] = False  # projected aperture, otherwise 3D spherical.
    kwargs['labels'] = ["FABLE"]
    if kwargs['projected']:
        insuffix_X = "_r500_proj"
    else:
        insuffix_X = "_r500"
    fname_X = ["SZ-M500_z0.00"+insuffix_X+".hdf5"]
    if kwargs['projected']:
        insuffix_Y = "_5r500_proj"
    else:
        insuffix_Y = "_5r500"
    fname_Y = ["SZ-M500_z0.00"+insuffix_Y+".hdf5"]
    plot_relation_from_different_files(simdirs, ["Y500", "Y5r500"], fname_X, fname_Y, kwargs)


def plot_scatter(simdirs, redshifts, axes, kwargs, outname,
                 inprefix="L-T_0.5-10", insuffix="",
                 suffix="", plot_rel=False,
                 blkwht=False, outdir=None):
    """
    Plot intrinsic scatter for given redshifts
    Args:
        redshifts: iterable of redshifts
        kwargs: dictionary of keyword arg to send to plot_relation()
        outname: name of output file
        suffix: subscript on y-label
        plot_rel: plot the individual relations in addition to the scatter
    """
    import matplotlib.pyplot as plt
    import os

    col = ['#24439b', '#ED7600', '#50BFD7']

    # need to make a copy of kwargs dictionary so we don't edit the original
    import copy
    my_kwargs = copy.deepcopy(kwargs)

    if outdir is None:
        try:
            outdir = my_kwargs['outdir']
        except KeyError:
            outdir = "/data/curie4/nh444/project1/L-T_relations/"

    fig, ax = plt.subplots(figsize=(7, 7))
    ax.set_ylabel(r"$\sigma_{\mathrm{" + str(suffix) + "}}$")
    if blkwht:
        fig.patch.set_facecolor('0.2')
    for i, zi in enumerate(redshifts):
        my_kwargs['mec'] = col[i:i+len(simdirs)]
        for idx in range(len(simdirs)):
            if 'labels' not in kwargs:
                my_kwargs['labels'] = (["$z="+"{:.1f}".format(zi)+"$"] *
                                       len(simdirs))
            else:
                my_kwargs['labels'][idx] = (str(kwargs['labels'][idx]) +
                                            " ($z="+"{:.1f}".format(zi)+"$)")
        if len(suffix) > 1:
            my_kwargs['outname'] = suffix + "_z"+"{:.1f}".format(zi) + ".pdf"
        else:
            my_kwargs['outname'] = "relation_z" + "{:.1f}".format(zi) + ".pdf"
        my_kwargs['hlightcol'] = col[i]
        filename = inprefix+"_z{:.1f}".format(zi)+insuffix+".hdf5"
        # test if this file exists for the first simdir
        if not os.path.exists(my_kwargs['basedir']+simdirs[0]+"/"+filename):
            # if not, try using 2 decimal places for the redshift
            filename = inprefix+"_z{:.2f}".format(zi)+insuffix+".hdf5"
        plot_relation(simdirs, [filename], axes, do_plot=plot_rel,
                      fit_col=['k'] if len(simdirs) == 1 else None,
                      plotlabel="$z="+"{:.1f}".format(zi)+"$",
                      plot_scat=True, axscat=ax, scat_kwargs={'zorder': i},
                      z=zi, **my_kwargs)

        ax.legend(loc='best', borderaxespad=1.0,
                  fontsize='small', frameon=False)

    if blkwht:
        fig.savefig(outdir+outname, transparent=True, bbox_inches='tight')
    else:
        fig.savefig(outdir+outname, bbox_inches='tight')
    print "Saved to", outdir+outname


def plot_ratio(axlabels, bins, *args, **kwargs):
    """Plot the ratio of two quantities as a function of the first. """
    import matplotlib.pyplot as plt
    import copy
    if bins is not None:
        from scipy.stats import binned_statistic

    # Get the axes limits if given.
    if 'axeslims' in kwargs:
        axeslims = kwargs['axeslims']
        if axeslims is None:
            axeslims = [None, None]
    else:
        axeslims = [None, None]

    fig, ax = plt.subplots(figsize=(7, 7))
    ax.set_xscale('log')
    # Horizontal line of unity
    ax.axhline(0.0, ls='--', lw=3, c='0.5', alpha=0.8)
    # Set the axes limits.
    if axeslims[0] is not None:
        ax.set_xlim(axeslims[0][0], axeslims[0][1])
    if axeslims[1] is not None:
        ax.set_ylim(axeslims[1][0], axeslims[1][1])
    # Set the axes labels.
    if axlabels is not None:
        ax.set_xlabel(axlabels[0])
        ax.set_ylabel(axlabels[1])

    my_kwargs = copy.deepcopy(kwargs)
    my_kwargs['do_plot'] = False
    my_kwargs['do_fit'] = False

    results = plot_relation(*args, **my_kwargs)

    for i, key in enumerate(results):
        result = results['result_{:d}'.format(i)]
        x = result['X']
        y = (result['Y'] - result['X']) / result['X']
        if bins is None:
            ax.plot(x, y,
                    marker='o', ms=8, ls='none', alpha=0.8,
                    label=my_kwargs['labels'][i], zorder=4)
        else:
            med, _, _ = binned_statistic(x, y, bins=bins, statistic='median')
            ax.plot((bins[1:]*bins[:-1])**0.5, med, lw=3,
                    label=my_kwargs['labels'][i])
    ax.legend(loc='best', frameon=False)
    plt.tight_layout()


def compare_relations(simdirs, axes, fname1, kwargs1, fname2, kwargs2,
                      plot_ratio=False,
                      outname=None, outdir=None):
    """Compare two files with different kwargs for plot_relation().

    For example, use this function to compare relations with and without
    projected quantities.
    """
    import matplotlib.pyplot as plt
    import numpy as np

    if outdir is None:
        try:
            outdir = kwargs1['outdir']
        except KeyError:
            outdir = "/data/curie4/nh444/project1/L-T_relations/"

    fig, ax = plt.subplots(figsize=(7, 7))

    if not isinstance(fname1, (list, tuple)):
        fname1 = [fname1]
    if not isinstance(fname2, (list, tuple)):
        fname2 = [fname2]

    res1 = plot_relation(simdirs, fname1, axes, ax=ax, **kwargs1)
    res2 = plot_relation(simdirs, fname2, axes, ax=ax, **kwargs2)

    ax.legend(loc='upper left', fontsize='small', borderaxespad=1)
    if outname is not None:
        fig.savefig(outdir+outname, bbox_inches='tight')
        print "Plot saved to", outdir+outname

    if plot_ratio:
        if fname1 != fname2:
            raise ValueError("For plot_ratio, fname1 and fname2 should "
                             "be the same.")
        if not np.all(res1['result_0']['M'] == res2['result_0']['M']):
            raise ValueError("The mass arrays are not the same.")
        fig, ax = plt.subplots(figsize=(7, 7))
        ax.set_xlabel(r"M$_{500}$ [M$_{\odot}$]")
        ax.set_ylabel("$F_1 / F_2$")
        ax.set_xscale('log')
        ax.scatter(res1['M'], res1['result_0']['Y'] / res2['result_0']['Y'])


def plot_relation_from_different_files(simdirs, axes, fname_X, fname_Y,
                                       kwargs, outdir=None):
    """Plot the relation between two quantities stored in different files.

    Arguments:
        fname_X: Path of file that contains the X quantity.
        fname_Y: Path of file that contains the Y quantity.
        kwargs: Keyowrd arguments passed to plot_relation().
    """
    import matplotlib.pyplot as plt
    import numpy as np
    from scipy.stats import binned_statistic

    if outdir is None:
        outdir = "/data/curie4/nh444/project1/L-T_relations/"
    # need to make a copy of kwargs dictionary so we don't edit the original
    import copy
    my_kwargs = copy.deepcopy(kwargs)
    my_kwargs['do_plot'] = False

    if not isinstance(fname_X, (list, tuple)):
        fname_X = [fname_X]
    if not isinstance(fname_Y, (list, tuple)):
        fname_Y = [fname_Y]

    res_X = plot_relation(simdirs, fname_X, ["mass", axes[0]], **my_kwargs)
    res_Y = plot_relation(simdirs, fname_Y, ["mass", axes[1]], **my_kwargs)
    if not np.all(res_X['result_0']['M'] == res_Y['result_0']['M']):
        raise ValueError("The mass arrays are not the same.")
    fig, ax = plt.subplots(figsize=(7, 7))
    ax.set_xlabel(r"M$_{500}$ [M$_{\odot}$]")
    ax.set_ylabel(axes[1] + "/" + axes[0])
    ax.set_xscale('log')
    M = res_X['result_0']['M']
    ratio = res_Y['result_0']['Y'] / res_X['result_0']['Y']
    ax.scatter(M, ratio)
    Mbins = np.logspace(np.log10(np.min(M)), np.log10(np.max(M)), num=10)
    med, _, _ = binned_statistic(M, ratio, statistic='median', bins=Mbins)
    ax.plot((Mbins[:-1]*Mbins[1:])**0.5, med, lw=3)
    print "mean/median ratio for M>1e14 Msun = {:.2f} / {:.2f}".format(
            np.mean(ratio[M > 1e14]), np.median(ratio[M > 1e14]))
    print "mean/median ratio for M>3e14 Msun = {:.2f} / {:.2f}".format(
            np.mean(ratio[M > 3e14]), np.median(ratio[M > 3e14]))
    print "mean/median ratio for M>5e14 Msun = {:.2f} / {:.2f}".format(
            np.mean(ratio[M > 5e14]), np.median(ratio[M > 5e14]))
    print "mean/median ratio for 1e13<M<5e13 Msun = {:.2f} / {:.2f}".format(
            np.mean(ratio[(M>1e13) & (M<5e13)]), np.median(ratio[(M>1e13) & (M<5e13)]))
    ax.axhline(1.796)


def plot_relation_multi(simdirs, redshifts, axes, kwargs, outname,
                        inprefix="L-T_0.5-10", insuffix="",
                        blkwht=False, outdir=None):
    """
    Plot a grid of scaling relations, one for each redshift in list 'redshifts'
    Args:
        redshifts: iterable of redshifts
        kwargs: dictionary of keyword arg to send to plot_relation()
    """
    import matplotlib.pyplot as plt
    import matplotlib.gridspec as gridspec
    import os
    ncol = 2

    # need to make a copy of kwargs dictionary so we don't edit the original
    import copy
    my_kwargs = copy.deepcopy(kwargs)

    if outdir is None:
        try:
            outdir = my_kwargs['outdir']
        except KeyError:
            outdir = "/data/curie4/nh444/project1/L-T_relations/"

    nrow = int(0.5 + float(len(redshifts)) / ncol)
    print "ncol =", ncol, "nrow =", nrow
    fig = plt.figure(figsize=(ncol*6, nrow*6))
    if blkwht:
        fig.patch.set_facecolor('0.2')
    gs = gridspec.GridSpec(nrow, ncol, wspace=0.0, hspace=0.0)
    for i, zi in enumerate(redshifts):
        ax = plt.subplot(gs[i])
        plotlabel = "$z="+"{:.1f}".format(zi)+"$"
        filename = inprefix+"_z{:.1f}".format(zi)+insuffix+".hdf5"
        if not os.path.exists(my_kwargs['basedir']+simdirs[0]+"/"+filename):
            filename = inprefix+"_z{:.2f}".format(zi)+insuffix+".hdf5"
        plot_relation(simdirs, [filename], axes, ax=ax,
                      z_comp=zi, plotlabel_upleft=plotlabel, **my_kwargs)
        # Remove overlapping axis and tick labels:
        if i % ncol != 0:  # not in first column
            ax.set_yticklabels([])  # remove all y tick labels
            ax.yaxis.label.set_visible(False)  # hide axes label
        elif i >= ncol:  # in first column but not in first row
            # remove last tick label
            ax.get_yticklabels()[-3].set_visible(False)
        # elif i < (nrow-1)*ncol: ## in first column but not in the last row
            # ax.get_yticklabels()[1].set_visible(False)
        if i < (nrow-1)*ncol:  # not the last row
            ax.set_xticklabels([])
            ax.xaxis.label.set_visible(False)  # hide axes label
        handles, labels = ax.axes.get_legend_handles_labels()
        ax.legend(loc='lower right', handletextpad=0.2,
                  borderaxespad=1.0 if len(labels) == 1 else 0.6,
                  fontsize='small', frameon=False)

    if blkwht:
        fig.savefig(outdir+outname, transparent=True, bbox_inches='tight')
        print "Saved to", outdir+outname
    else:
        fig.savefig(outdir+outname, bbox_inches='tight')
        print "Saved to", outdir+outname


def plot_relation_redshift(simdirs, axes, kwargs, plot_fits=False,
                           do_plot=True, param_kwargs={},
                           inprefix="L-T_0.5-10", insuffix="",
                           outprefix=None, outsuffix="",
                           M500min_fit=None,
                           minz=0.0, minsnap=None, maxz=2.0, zstep=0.2,
                           outdir=None, verbose=False):
    """Plot a scaling relation at various redshifts.

    The plots are saved to separate files, unless do_fit==True in which case
    the best-fitting relations are put on the same plot.
    Args:
        plot_fits: plot the best-fitting relation at each redshift on
                   one plot as well as the best-fitting parameters as a
                   function of redshift.
        do_plot: if not plot_fits, tells plot_relation whether to plot
                 the individual relations.
        param_kwargs: keyword args to function plot_norm_slope_vs_z
        insuffix: suffix added to input filename
        outprefix: prefix of output file, set to None to not save the plots
        outsuffix: string to append to output filenames
        M500min_fit: minimum M500 to use in fit.
                     Overrides M500min_fit given in kwargs.
        minz, maxz, zstep: defines the min, max and step in redshift
        minsnap: the snapshot number corresponding to minz for appending
                 to the input directory.
                 If None, then don't use the snapshot number.
        Ntrials: number of bootstrap resamples
        outdir: output directory for saving fit parameters
    """
    import numpy as np
    import os
    if plot_fits:
        import matplotlib.pyplot as plt
    if outprefix is None:
        outname = None
    if outdir is None:
        outdir = "/data/curie4/nh444/project1/L-T_relations/"

    # estimate errors on fit parameters via bootstrap resampling
    # with replacement.
    bootstrap = True

    # need to make a copy of kwargs dictionary so we don't edit the original.
    import copy
    my_kwargs = copy.deepcopy(kwargs)
    if M500min_fit is not None:
        my_kwargs['M500min_fit'] = M500min_fit
    if plot_fits:
        do_plot = True
        fig, ax = plt.subplots(figsize=(7, 7))
        my_kwargs['idxScat'] = []
        my_kwargs['idxMed'] = []
        my_kwargs['plot_obs'] = False
        my_kwargs['do_fit'] = True
        my_kwargs['fit_lw'] = 3
        my_kwargs['fit_alpha'] = 0.7
        assert outprefix is not None, "need outprefix if plot_fits==True"
    else:
        ax = None

    z_list = np.arange(minz, maxz, zstep)
    slope = np.zeros_like(z_list)
    logC = np.zeros_like(z_list)
    scat = np.zeros_like(z_list)
    slope_err, logC_err, scat_err = [], [], []
    Nfit = np.zeros(len(z_list), dtype=int)

    #from palettable.colorbrewer.sequential import YlGnBu_9_r as pallette
    # cmax = 0.6 ## increase to include lighter colours
    #colour_idx = np.linspace(cmax, 0, len(z_list))
    from palettable.colorbrewer.diverging import RdBu_11_r as pallette
    colour_idx = np.concatenate((np.linspace(0.3, 0.0, len(z_list)/2+1), np.linspace(1.0, 0.7, len(z_list)/2)))
    cmap = pallette.mpl_colormap
    col = cmap(colour_idx)

    for i, z in enumerate(z_list):
        filename = inprefix+"_z"+"{:.1f}".format(z)+insuffix+".hdf5"
        if not os.path.exists(my_kwargs['basedir']+simdirs[0]+"/"+filename):
            filename = inprefix+"_z"+"{:.2f}".format(z)+insuffix+".hdf5"
        if minsnap is not None:
            snapnum = minsnap - i
        else:
            snapnum = None
        if outprefix is not None:
            outname = outprefix+"_z"+"{:.1f}".format(z)+".pdf"
        if plot_fits:  # change colour
            my_kwargs['mec'] = [col[i]]
            my_kwargs['labels'] = ["$"+"{:.1f}".format(z)+"$"]
        result = plot_relation(simdirs, [filename], axes, ax=ax,
                               plotlabel=None if plot_fits else "$z="+"{:.1f}".format(z)+"$",
                               snapnum=snapnum, outname=outname,
                               bootstrap=bootstrap, do_plot=do_plot,
                               **my_kwargs)
        cur_slope = result['result_0']['slope']
        cur_slope_err = result['result_0']['slope_err']
        cur_logC = result['result_0']['logC']
        cur_logC_err = result['result_0']['logC_err']
        cur_scat = result['result_0']['scat']
        cur_scat_err = result['result_0']['scat_err']
        Nfit[i] = result['result_0']['num']
        if cur_slope is not None and cur_logC is not None:
            slope[i] = cur_slope
            logC[i] = cur_logC
            scat[i] = cur_scat
        print "cur_slope_err =", cur_slope_err
        if i == 0 and (cur_slope_err is None or cur_logC_err is None or
                       cur_scat_err is None):
            raise RuntimeError("There were not enough objects for a fit "
                               "at z={:.3f} and I cannot guess the "
                               "correct format for the error array.".format(z))
        if cur_slope_err is None:
            # Append an array of zeroes of the same shape
            # as the previous error array.
            slope_err.append(np.zeros_like(slope_err[-1]))
        else:
            slope_err.append(cur_slope_err)
        if cur_logC_err is None:
            # Append an array of zeroes of the same shape
            # as the previous error array.
            logC_err.append(np.zeros_like(logC_err[-1]))
        else:
            logC_err.append(cur_logC_err)
        if cur_scat_err is None:
            # Append an array of zeroes of the same shape
            # as the previous error array.
            scat_err.append(np.zeros_like(scat_err[-1]))
        else:
            scat_err.append(cur_scat_err)

    if my_kwargs['do_fit']:  # save best-fit parameters
        # Convert list of arrays to 2D arrays
        slope_err = np.array(slope_err).T
        logC_err = np.array(logC_err).T
        scat_err = np.array(scat_err).T

        outfile = outdir+"fit_params_"+axes[0]+"_"+axes[1]+outsuffix+".txt"
        # Concatenate arrays into 2D array for saving to file
        print "slope_err =", slope_err
        dat = np.c_[z_list, slope, slope_err[0, :], slope_err[1, :], logC,
                    logC_err[0, :], logC_err[1, :], scat, scat_err[0, :], scat_err[1, :], Nfit]
        # Add any extra error bars
        if np.shape(slope_err)[0] > 2:
            nextra = (np.shape(slope_err)[0] / 2)-1
            print "Got", nextra, "extra errors"
            for j in range(1, nextra+1):
                dat = np.column_stack((dat, slope_err[2*j, :], slope_err[2*j+1, :],
                                       logC_err[2*j, :], logC_err[2*j+1, :], scat_err[2*j, :], scat_err[2*j+1, :]))
        np.savetxt(outfile, dat, header="z, slope, slope_err_low, slope_err_high, logC, logC_err_low, logC_err_high, scat, scat_err_low, scat_err_high, num_fit")
        print "Parameters output to", outfile
        if plot_fits:
            from scaling.plotting.plot_params import plot_params_vs_z
            plot_params_vs_z(outdir, axes, outprefix,
                                 insuffixes=[outsuffix],
                                 outsuffix=outsuffix,
                                 outdir=my_kwargs['outdir'], **param_kwargs)

    if plot_fits:
        ax.legend(loc='best', fontsize='small', frameon=False,
                  borderaxespad=1, title="Redshift", ncol=2)
        fig.savefig(my_kwargs['outdir']+outprefix +
                    "_vs_z"+outsuffix+".pdf", bbox_inches='tight')
        print "Plot saved to", my_kwargs['outdir'] + \
            outprefix+"_vs_z"+outsuffix+".pdf"


def plot_fit_method_dependence_params(
        simdirs, insuffix, kwargs, inprefix="L-T_0.5-10",
        axes=["T500mw", "lum"], param_kwargs={}, plot=True,
        minz=0.0, minsnap=None, maxz=2.0, zstep=0.2,
        outdir="/home/nh444/data/project1/L-T_relations/"):
    """
    Plot the best-fitting relation for different lower halo mass limits
    """

    methods = ["bces_orthog", "bces", "linmix"]
    suffixes = ["_"+method for method in methods]
    labels = ["BCES orthogonal", r"BCES(L$|$T$_{\mathrm{mw}}$)", "Kelly 2007"]

    datadir = "/home/nh444/data/project1/L-T_relations/"

    import copy
    my_kwargs = copy.deepcopy(kwargs)
    my_kwargs['do_fit'] = True
    for i, method in enumerate(methods):
        my_kwargs['fit_method'] = method
        plot_relation_redshift(simdirs, axes, my_kwargs,
                               minz=minz, minsnap=minsnap, maxz=maxz, zstep=zstep,
                               plot_fits=False, do_plot=False,
                               insuffix=insuffix, outsuffix=suffixes[i],
                               inprefix=inprefix,
                               outdir=datadir)
    if plot:
        from scaling.plotting.plot_params import plot_params_vs_z
        outsuffix = axes[0]+"_"+axes[1]+"_method_dependence"
        plot_params_vs_z(datadir, axes, "", outsuffix=outsuffix,
                         insuffixes=suffixes, labels=labels,
                         outdir=outdir, **param_kwargs)
    

def plot_fit_method_dependence(simdirs, filenames, kwargs, z=0,
                               axes=["T500mw", "lum"],
                               plotlabel=None,
                               outname=None,
                               outdir="/home/nh444/data/project1/L-T_relations/"):
    """
    Plot the best-fitting relation for different fitting methods
    """
    import matplotlib.pyplot as plt

    if outname is None:
        outname = axes[0]+"_"+axes[1]+"_fit_dependence.pdf"

    fig, ax = plt.subplots(figsize=(7, 7))

    # col = ["#c7673e","#78a44f","#6b87c8","#934fce","#e6ab02","#be558f","#47af8d"]
    #methods = ["lm", "bces", "bcesxy", "bces_orthog", "linmix"]
    #labels = ["least-squares", r"BCES(L$|$T)", r"BCES(T$|$L)", "BCES orthogonal", "Kelly 2007"]
    # dark blue, orange, light blue, dark red
    col = ['#24439b', '#ED7600', '#50BFD7', '#B94400']
    methods = ["bces_orthog", "bces", "linmix"]
    labels = ["BCES orthogonal", r"BCES(L$|$T$_{\mathrm{mw}}$)", "Kelly 2007"]

    import copy
    my_kwargs = copy.deepcopy(kwargs)
    my_kwargs['do_fit'] = True
    my_kwargs['plot_obs'] = False
    my_kwargs['fit_alpha'] = 1.0
    if 'fit_method' in my_kwargs.keys():
        del my_kwargs['fit_method']
    if 'labels' in my_kwargs.keys():
        del my_kwargs['labels']
    for i, method in enumerate(methods):
        my_kwargs['mec'] = col[i:]
        if i > 0:
            my_kwargs['plot_obs'] = False
        my_kwargs['idxScat'] = [0] if i == 0 else []
        plot_relation(simdirs, filenames, axes, ax=ax, z_comp=z,
                      fit_method=method,
                      plotlabel=plotlabel if i == 0 else None,
                      labels=[labels[i]], outname=outname, **my_kwargs)

    ax.legend(loc='upper left', fontsize='small', borderaxespad=1)
    fig.savefig(outdir+outname, bbox_inches='tight')
    print "Plot saved to", outdir+outname


def plot_fit_mass_evol_dependence_params(
        simdirs, insuffix, kwargs, inprefix="L-T_0.5-10",
        axes=["T500mw", "lum"], param_kwargs={}, plot=True,
        minz=0.0, minsnap=None, maxz=2.0, zstep=0.2,
        outdir="/home/nh444/data/project1/L-T_relations/"):
    """
    Plot the best-fitting relation for different choices of the redshift
    evolution of the lower halo mass limit.
    """

    M_power_fit_list = [0.0, 0.5, 1.0]
    suffixes = ["_Ez"+"{:.1f}".format(i) for i in M_power_fit_list]
    labels = [r"$\mathrm{M > M_{min}}$",
              r"$\mathrm{M} > E(z)^{-0.5} \; \mathrm{M_{min}}$",
              r"$\mathrm{M} > E(z)^{-1} \; \mathrm{M_{min}}$"]
#    labels = [r"$M_{500} > E(z)^{-"+"{:.1f}".format(i) +
#              r"}$ M$_{\mathrm{500, min}}$" for i in M_power_fit_list]
    datadir = "/home/nh444/data/project1/L-T_relations/"

    import copy
    my_kwargs = copy.deepcopy(kwargs)
    my_kwargs['do_fit'] = True
    for i, M_power_fit in enumerate(M_power_fit_list):
        my_kwargs['M_power_fit'] = M_power_fit
        plot_relation_redshift(simdirs, axes, my_kwargs,
                               inprefix=inprefix,
                               plot_fits=False, do_plot=False,
                               minz=minz, minsnap=minsnap, maxz=maxz,
                               zstep=zstep,
                               insuffix=insuffix, outsuffix=suffixes[i],
                               outdir=datadir)

    if plot:
        from scaling.plotting.plot_params import plot_params_vs_z
        outsuffix = axes[0]+"_"+axes[1]+"_mass_evol_dependence"
        plot_params_vs_z(datadir, axes, "", outsuffix=outsuffix,
                         insuffixes=suffixes, labels=labels,
                         outdir=outdir, **param_kwargs)


def plot_fit_mass_dependence_params(simdirs, insuffix, kwargs, param_kwargs={},
                                    inprefix="L-T_0.5-10",
                                    axes=["T500mw", "lum"], plot=True,
                                    minz=0.0, minsnap=None, maxz=2.0, zstep=0.2,
                                    outdir="/home/nh444/data/project1/L-T_relations/"):
    """Plot the best-fitting relation for different lower halo mass limits."""

    M500min_list = [3e13, 6e13, 1e14]
    suffixes = ["_3e13", "_6e13", "_1e14"]
    labels = [
        r"M$_{500} > 3 \times 10^{13} \; E(z)^{"+"-{:.1f}".format(kwargs['M_power_fit'])+r"}$ M$_{\odot}$",
        r"M$_{500} > 6 \times 10^{13} \; E(z)^{"+"-{:.1f}".format(kwargs['M_power_fit'])+r"}$ M$_{\odot}$",
        r"M$_{500} > 10^{14} \; E(z)^{"+"-{:.1f}".format(kwargs['M_power_fit'])+r"}$ M$_{\odot}$"]

    datadir = "/home/nh444/data/project1/L-T_relations/"

    import copy
    my_kwargs = copy.deepcopy(kwargs)
    my_kwargs['do_fit'] = True
    # Calculate and save the best-fitting parameters at eahc redshift.
    for i, M500min in enumerate(M500min_list):
        my_kwargs['M500min_fit'] = M500min
        plot_relation_redshift(simdirs, axes, my_kwargs,
                               minz=minz, minsnap=minsnap, maxz=maxz, zstep=zstep,
                               plot_fits=False, do_plot=False,
                               inprefix=inprefix, insuffix=insuffix,
                               outsuffix=suffixes[i],
                               outdir=datadir)

    if plot:
        # Plot the best-fitting parameters versus redshift.
        from scaling.plotting.plot_params import plot_params_vs_z
        outsuffix = axes[0]+"_"+axes[1]+"_mass_dependence"
        plot_params_vs_z(datadir, axes, "", outsuffix=outsuffix,
                         insuffixes=suffixes, labels=labels,
                         outdir=outdir, **param_kwargs)


def plot_fit_mass_dependence(simdirs, filenames, kwargs, z,
                             axes=["T500mw", "lum"], plotlabel=None,
                             outname="L-Tmw_mass_dependence.pdf",
                             outdir="/home/nh444/data/project1/L-T_relations/",
                             h_scale=0.6774, omega_m=0.3089, omega_l=0.6911):
    """Plot the best-fitting relation for different lower halo mass limits. """
    import matplotlib.pyplot as plt

    # Define E(z), the dimensionless Hubble parameter (E(z) = H(z) / H0)
    def E(zz): return (omega_m*(1+zz)**3 + omega_l)**0.5

    fig, ax = plt.subplots(figsize=(7, 7))

    # dark blue, orange, light blue, dark red
    col = ['#24439b', '#ED7600', '#50BFD7', '#B94400']

    M500min_list = [3e13, 6e13, 1e14]
    labels = [
        r"M$_{500} > 3 \times 10^{13} \; E(z)^{"+"-{:.1f}".format(kwargs['M_power_fit'])+r"}$ M$_{\odot}$",
        r"M$_{500} > 6 \times 10^{13} \; E(z)^{"+"-{:.1f}".format(kwargs['M_power_fit'])+r"}$ M$_{\odot}$",
        r"M$_{500} > 10^{14} \; E(z)^{"+"-{:.1f}".format(kwargs['M_power_fit'])+r"}$ M$_{\odot}$"]
    import copy
    my_kwargs = copy.deepcopy(kwargs)
    my_kwargs['do_fit'] = True
    my_kwargs['fit_alpha'] = 0.7
    #my_kwargs['hlightfit'] = False
    my_kwargs['hlightcol'] = None
    if 'M500min_fit' in my_kwargs.keys():
        del my_kwargs['M500min_fit']
    if 'M500min_plot' in my_kwargs.keys():
        del my_kwargs['M500min_plot']
    if 'labels' in my_kwargs.keys():
        del my_kwargs['labels']
    for i, M500min in enumerate(M500min_list):
        my_kwargs['mec'] = col[i:]
        if i > 0:
            my_kwargs['plot_obs'] = False
        result = plot_relation(simdirs, filenames, axes, ax=ax, z_comp=z,
                               M500min_plot=M500min / E(z)**my_kwargs['M_power_fit'],
                               M500min_fit=M500min,
                               plotlabel=plotlabel if i == 0 else None,
                               labels=[labels[i]], outname=outname,
                               **my_kwargs)
        print M500min, result['result_0']['num']

    ax.legend(loc='upper left', fontsize='small', borderaxespad=1)
    fig.savefig(outdir+outname, bbox_inches='tight')
    print "Plot saved to", outdir+outname


def plot_relation(simdirs, filenames, axes, zoomdirs=None, zooms=[[]],
                  projected=True, snapnum=None,
                  M500min_plot=0, use_non_centrals=True,
                  do_plot=True, ax=None, figsize=(7, 7), labels=None,
                  basedir="/home/nh444/data/project1/L-T_relations/",
                  obs_dir="/data/vault/nh444/ObsData/",
                  plot_obs=True, plot_sims=False, z_comp=0.0,
                  xray_obs={}, SZ_obs={},
                  outdir="/home/nh444/data/project1/L-T_relations/",
                  outname=None,
                  h_scale=0.6774, omega_m=0.3089, omega_l=0.6911,
                  axeslims=None, axlabels=None,
                  idxScat=None, idxMed=[],
                  ArrowGrps=[], LabelGrps=[], LabelZooms=[],
                  hlightzooms=False, hlightfit=True, hlightcol=None,
                  plotlabel=None, plotlabel_upleft=None,
                  leg_kwargs={}, leg_len=None,
                  markers=None, mec=None, mfc=None, ms=6.5, mew=1.2, errlw=1.2,
                  binwidth=0.1, bin_ax=0,
                  ls=None, medcol=None, lw=3,
                  skip_missing_files=True,
                  do_fit=False, fit_method="bces_orthog",
                  fit_to_median=False,
                  bootstrap=False, Ntrials=10000,
                  M500min_fit=0, M_power_fit=0.5,
                  fit_col=None, fit_lw=4, fit_alpha=0.4,
                  plot_scat=False, axscat=None, scat_kwargs={},
                  verbose=False):
    """
    Plot a given scaling relation from files produced by get_L-T.py
    Args:
        simdirs: a list of names of the simulation directories within basedir
        filenames: a list of file names for the L-T data
        axes: a list of two strings defining the x and y axes
                these can include "T500", "lum", "mass", "gasmass"
        zoomdirs: list of directories containing L-T data for zooms
        zooms: list of names of zoom directories for each zoomdir
        projected: if true, use values in the projected aperture, otherwise 3D
        snapnum: snapshot number appended to simdir if given
        M500min_plot: minimum halo mass M500 to plot
        use_non_centrals: use FoF groups other than group 0 from zooms
        do_plot: Plot the relation otherwise just return the fit parameters
        ax: pyplot axes instance, set to None to create new figure
        figsize: tuple giving figure size in inches
        labels: list of strings, one for each simdir and filename
        basedir: location of the simdirs
        obs_dir: location of the observational data
        plot_obs: plot observational data
        plot_sims: plot simulation data
        z_comp: desired redshift of observational/simulation data.
        xray_obs: X-ray observations to compare to. Dictionary of boolean
                  values. For possible keys see plot_obs_data()
        SZ_obs: SZ observations to compare to. Dictionary of boolean values.
                For possible keys see SZ_scaling.plot_SZ_obs()
        outdir: directory to save output plot
        outname: output filename of plot, if None do not save the plot
        h_scale: hubble parameter to scale to
        omega_m, omega_l: omega matter and omega lambda
        axeslims: 2-list of 2-lists giving limits of x- and y-axes
        axlabels: axis labels, set to None to use default labels
        idxScat: indices of simdirs to plot points for
        idxMed: indices of simdirs to plot the median relation for
        ArrowGrps: FoF groups to plot arrows for between the 1st and 2nd files
        LabelGrps: FoF groups to plot label for, showing the group number
        LabelZooms: Indices of zooms to label. Only group 0 is labelled.
        hlightzooms: highlight zooms vs simdirs using different symbols
        hlightfit: distinguish between points used in fit and others
        hlightcol: If not None, this is the colour used to highlight
                   groups other than group 0 in zooms
        plotlabel: if not None, adds this string to bottom right of plot
        plotlabel_upleft: if not None, adds this string to upper left of plot
        leg_kwargs: dictionary of keyword arguments passed to ax.legend().
        leg_len: The maximum number of legend entries in the upper left legend.
                 Other entries are placed in a separate legend at the lower
                 right of the figure. If None, place all entries upper left.
        markers: list of markers
        mec: list of colours for marker edges
        mfc: list of colours for marker faces
        ms: marker size
        mew: marker edge width
        errlw: line width of error bars.
        binwidth: width of bins for the median relation in dex
        bin_ax: index of axis in which to bin the median relation (0 or 1)
        ls: linestyle of median lines
        medcol: list of colours for median lines
        lw: linewidth of median lines
        skip_missing_files: Don't raise an error if a zoom is missing
        do_fit: If true, perform fit on scaling relation and plot
        fit_method: fitting method, see fit_relation.py
        fit_to_median: Fit to the median in each bin of width 'binwidth' rather
                       than the individual points.
        M500min_fit: minimum halo mass (Msun) to include in fit
        M_power_fit: minimum halo mass is M500min_fit / E(z)**M_power_fit
        fit_col: iterable of colours for best-fit line
        fit_lw: linewidth of best-fit line
        fit_alpha: alpha value (Transparency) of best-fit line
        plot_scat: plot intrinsic scatter versus x-axis in log bins
        axscat: pyplot axes object for intrinsic scatter plot
        verbose: print debug messages
    """
    import matplotlib.pyplot as plt
    import matplotlib.ticker as ticker
    import numpy as np
    import scipy.stats as stats
    import os

    if len(simdirs) <= 0:
        raise ValueError("simdirs is an empty list.")
    if not isinstance(filenames, (list, tuple)):
        raise TypeError("filenames must be a list or tuple.")
    if not isinstance(axes, (list, tuple)):
        raise TypeError("axes must be a list or tuple of length 2.")
    if len(axes) != 2:
        raise ValueError("axes must be a list or tuple of length 2.")
    if M500min_plot > M500min_fit:
        raise ValueError("M500min_plot is larger than M500min_fit.")
    if labels is None:
        labels = [None]*len(simdirs)*len(filenames)
    if axeslims is None:
        axeslims = [None, None]
    if axlabels is None:
        axlabels = [None, None]
    if idxScat is None:
        idxScat = range(len(simdirs))

    # Define some aliases for the axes names.
    for i in range(2):
        if axes[i] in ("mass", "M", "M500"):
            axes[i] = "mass"
        if axes[i] in ("lum", "luminosity", "L"):
            axes[i] = "lum"
        if axes[i] in ("T500", "temp", "temperature", "T"):
            # Use the spectroscopic temperature by default.
            axes[i] = "T500"
        if axes[i] in ("gasmass", "Mgas", "gas"):
            axes[i] = "gasmass"

    # Define E(z), the dimensionless Hubble parameter (E(z) = H(z) / H0)
    def E(z): return (omega_m*(1+z)**3 + omega_l)**0.5
    # for each relation get the power of E(z) multiplying the y-axis
    Epow = get_Epow()
    # Set y axis label with proper SS scaling (unless given)
    if axlabels[1] is None:
        if axes[1] == "mass" and axes[0] in temperatures:
            axlabels[1] = r"M$_{500}$ E(z) [M$_{\odot}$]"
        if axes[1] == "gasmass" and axes[0] == "mass":
            axlabels[1] = r"M$_{\mathrm{gas},500}$ [M$_{\odot}$]"
        elif axes[1] in luminosities and axes[0] in temperatures:
            axlabels[1] = (r"L$_{500}^{\mathrm{bol}}$ E(z)$^{-1}$ "
                           "[erg s$^{-1}$]")
        elif axes[1] in luminosities and axes[0] == "mass":
            axlabels[1] = (r"L$_{500}^{\mathrm{bol}}$ E(z)$^{-7/3}$ "
                           "[erg s$^{-1}$]")
        elif axes[1] in luminosities and axes[0] == "gasmass":
            axlabels[1] = (r"L$_{500}^{\mathrm{bol}}$ E(z)$^{-7/3}$ "
                           "[erg s$^{-1}$]")
        elif axes[1] == "YX" and axes[0] == "mass":
            axlabels[1] = (r"Y$_{\mathrm{X},500}$ E(z)$^{-2/3}$ "
                           r"[M$_{\odot}$ keV]")
        elif axes[1] == "YXmw" and axes[0] == "mass":
            axlabels[1] = (r"Y$_{\mathrm{X,500,mw}}$ E(z)$^{-2/3}$ "
                           r"[M$_{\odot}$ keV]")
        elif axes[1] == "Y500" and axes[0] == "mass":
            axlabels[1] = (r"$\mathrm{E(z)^{-2/3} \, Y_{500} \, D_A^2}$ "
                           "[Mpc$^2$]")
        elif axes[1] == "Y5r500" and axes[0] == "mass":
            axlabels[1] = (r"$\mathrm{E(z)^{-2/3} \, Y_{5r_{500}} \, D_A^2}$ "
                           "[Mpc$^2$]")
        elif axes[1] == "YSPT" and axes[0] == "mass":
            axlabels[1] = (r"$\mathrm{E(z)^{-2/3} \, Y^{0'.75} \, D_A^2}$ "
                           "[Mpc$^2$]")
        elif axes[1] == "SZ" and axes[0] == "mass":
            axlabels[1] = r"$\mathrm{E(z)^{-2/3} Y}$"
    (axeslims, axlabels, axname,
     axidx, axfac, Ezpows) = get_axparams(axes, axeslims, axlabels,
                                          projected=projected,
                                          Epow=Epow, h_scale=h_scale)

    if do_fit:
        # Define range in which to fit
        if axeslims[0] is not None:
            x0 = axeslims[0][0]
            x1 = axeslims[0][1]
        if axeslims[1] is not None:
            y0 = axeslims[1][0]
            y1 = axeslims[1][1]
        # Create the powerlaw function for a given pivot point X0
        from scaling.fit_relation import fit_powerlaw
        from scaling.fit_relation import make_powerlaw
        if fit_to_median:
            from scaling.fit_relation import estimate_scatter
        # get the pivot point and initial guesses for slope and norm
        X0, inislope, inilogC = get_fit_params(axes)
        powerlaw = make_powerlaw(np.log10(X0))

    if medcol is None:
        from palettable.colorbrewer.qualitative import Dark2_8 as pallette
        medcol = pallette.hex_colors
    if markers is None:
        markers = ["D", "s", "o"]
    if ls is None:
        ls = ['dashdot', 'dotted', 'dashed', 'solid']
    if zoomdirs is None:
        zoomdirs = [""]*len(simdirs)
    if mec is None:
        from palettable.colorbrewer.qualitative import Dark2_8 as pallette
        mec = pallette.hex_colors
    if mfc is None:
        mfc = ['none']*len(simdirs)*len(filenames)
    if fit_col is None:
        fit_col = mec
    if hlightfit:
        notfit_alpha = 1.0
    if not isinstance(medcol, (list, tuple)):
        raise TypeError("medcol must be a list or tuple.")
    if not isinstance(markers, (list, tuple)):
        raise TypeError("markers must be a list or tuple.")
    if not isinstance(markers, (list, tuple)):
        raise TypeError("markers must be a list or tuple.")
    if not isinstance(mec, (list, tuple)):
        raise TypeError("mec must be a list or tuple.")
    if not isinstance(mfc, (list, tuple)):
        raise TypeError("mfc must be a list or tuple.")
    if not isinstance(fit_col, (list, tuple)):
        raise TypeError("fit_col must be a list or tuple.")
    # define zorder of points so that observational data do not overlap
    zorder = 100

    if ax is None and do_plot:
        have_ax = False
        fig, ax = plt.subplots(figsize=figsize)
    elif do_plot:
        have_ax = True

    if do_plot:
        # Set the axes properties.
        ax.set_xscale('log')
        if axeslims[0] is not None:
            ax.set_xlim(axeslims[0][0], axeslims[0][1])
        if axeslims[1] is not None:
            ax.set_ylim(axeslims[1][0], axeslims[1][1])
        ax.set_yscale('log')
        ax.set_xlabel(axlabels[0])
        ax.set_ylabel(axlabels[1])

    if plot_scat:
        # combine specified kwargs with defaults
        scat_kwargs.update({'marker': 'o', 'ms': 10, 'lw': 3,
                            'markeredgewidth': 3})

        from scaling.fit_relation import estimate_scatter
        if axscat is None:
            save_scat = True
            figscat, axscat = plt.subplots(figsize=(7, 7))
        else:
            save_scat = False
        axscat.set_xlabel(axlabels[0])
        axscat.set_xscale('log')
        axscat.set_xlim(axeslims[0][0], axeslims[0][1])

    # Observations
    if plot_obs and do_plot:
        plot_obs_data(ax, axes, z_comp, ms=ms, errlw=errlw, lw=fit_lw, Epow=Epow,
                      xray_obs=xray_obs, SZ_obs=SZ_obs,
                      omega_m=omega_m, omega_l=omega_l, h_scale=h_scale,
                      obs_dir=obs_dir)

    # Median profiles
    if len(idxMed) > 0:
        assert bin_ax in [0, 1], "bin_ax must be either 0 or 1"
        # always bin by halo mass if it's on the y-axis
        if axes[1] == "mass" and bin_ax != 1:
            print "Binning by halo mass instead of", axes[0]
            bin_ax = 1
        if axes[bin_ax] in temperatures:
            xmin, xmax = [0.01, 100]
        elif axes[bin_ax] in luminosities:
            xmin, xmax = [1e38, 1e48]
        elif axes[bin_ax] == "mass":
            xmin, xmax = [1e12, 1e16]
        elif axes[bin_ax] == "Mgas":
            xmin, xmax = [1e10, 1e15]
        else:
            raise ValueError(
                "Unrecognised axes for binning: "+str(axes[bin_ax]))
        num = int((np.log10(xmax) - np.log10(xmin)) / binwidth)
        bins = np.logspace(np.log10(xmin), np.log10(xmax), num=num)
        x = 10**((np.log10(bins[:-1]) + np.log10(bins[1:]))/2)

    if len(LabelZooms) > 0:
        # Create empty lists to append zoom labels and positions to
        # so that we can add the labels together at the very end.
        LabelZooms_label = []
        LabelZooms_X = []
        LabelZooms_Y = []

    # Store results in a dictionary and return it.
    results = {}
    for simidx, simdir in enumerate(simdirs):
        for fileidx, filename in enumerate(filenames):
            idx = fileidx + simidx*(len(filenames))
            label = labels[idx]

            if snapnum is not None:
                infile = (basedir + simdirs[simidx] + "_" +
                          str(snapnum).zfill(3) + "/" + filename)
            else:
                infile = basedir+simdirs[simidx] + "/" + filename
            if not os.path.exists(infile):
                raise IOError("Could not find path "+infile)

            X, Y, M500, grps, metadata = load_data(
                infile, axes, axname, axidx, axfac,
                projected=projected, h_scale=h_scale)
            redshift = metadata['redshift']
            X *= E(redshift)**Ezpows[0]
            Y *= E(redshift)**Ezpows[1]
            ind = np.where(M500 >= M500min_plot)[0]
            X, Y, M, grps = X[ind], Y[ind], M500[ind], grps[ind]
            if simidx in idxScat and do_plot:
                if hlightfit and do_fit:
                    # Plot solid markers for objects included
                    # in fit and open otherwise
                    notfit = (M * E(redshift)**M_power_fit < M500min_fit)
                    ax.plot(X[notfit], Y[notfit], marker=markers[idx],
                            alpha=notfit_alpha,
                            mfc='none', mec=mec[idx], ms=ms, ls='none',
                            mew=mew, zorder=zorder)
                    ax.plot(X[~notfit], Y[~notfit], marker=markers[idx],
                            mfc=mec[idx], mec=mec[idx], ms=ms, ls='none',
                            mew=mew, zorder=zorder)
                else:
                    # if not do_fit else None)
                    ax.plot(X, Y, marker=markers[idx], mfc=mfc[idx],
                            mec=mec[idx], ms=ms, ls='none', mew=mew,
                            zorder=zorder, label=label)
                    ax.plot(X, Y, marker=markers[idx], mfc=mfc[idx],
                            mec=mec[idx], ms=ms, ls='none', mew=mew,
                            zorder=zorder, label=label)
                if len(ArrowGrps) > 0:
                    # get the indices corresponding to the groups
                    # given by ArrowGrps.
                    indArrow = [i for i, grp in enumerate(grps)
                                if grp in ArrowGrps]
                    if fileidx == 0:
                        # Save the data for the first file
                        X2, Y2 = X[indArrow], Y[indArrow]
                        grps2 = grps[indArrow]
                    else:
                        # For other files, plot arrow between datapoints
                        # in this file and the first file that correspond
                        # to the same group
                        for idxArrow in indArrow:
                            # if same group was in first file
                            if grps[idxArrow] in grps2:
                                idxArrow2 = np.where(grps2 == grps[idxArrow])[0]
                                ax.annotate("",
                                xy=(X[idxArrow], Y[idxArrow]), xycoords='data',
                                xytext=(X2[idxArrow2], Y2[idxArrow2]),
                                        textcoords='data',
                                        arrowprops=dict(fc=mfc[idx] if len(filenames) > 2 else 'k', ec=mec[idx] if len(filenames) > 2 else 'k',
                                                        width=0.5, headwidth=6, headlength=5, shrink=0.07, fill=True))  # width, headwidth, headlength are in pts
                if len(LabelGrps) > 0:
                    # get the indices corresponding to the groups in LabelGrps.
                    indLabel = [i for i, grp in enumerate(grps)
                                if grp in LabelGrps]
                    for idxLabel in indLabel:
                        ax.annotate(str(grps[idxLabel]),
                                    xy=(X[idxLabel], Y[idxLabel]),
                                    xytext=(6, 0), textcoords='offset points',
                                    color=mec[idx] if len(filenames) > 1 else 'k',
                                    fontsize=10)
            if zoomdirs[simidx] is not "":
                for zidx, zoom in enumerate(zooms[simidx]):
                    if snapnum is not None:
                        zinfile = zoomdirs[simidx]+zoom+"_"+str(snapnum).zfill(3)+"/"+filename
                    else:
                        zinfile = zoomdirs[simidx]+zoom+"/"+filename
                    if os.path.exists(zinfile):
                        Xz, Yz, M500, grps, metadata = load_data(
                                zinfile, axes, axname, axidx, axfac,
                                projected=projected, h_scale=h_scale)
                        if abs(metadata['redshift'] - redshift) > 0.01:
                            raise ValueError("Redshift of zoom " + str(zoom) +
                                             "does not match redshift " +
                                             "z={:.1f}".format(redshift))
                        Xz *= E(redshift)**Ezpows[0]
                        Yz *= E(redshift)**Ezpows[1]
                        if use_non_centrals:
                            # get groups other than group zero
                            ind = np.where(M500 >= M500min_plot)[0]
                        else:
                            # group zero only
                            ind = np.where(grps.astype(int) == 0)[0]
                        Xz, Yz, M500, grps = Xz[ind], Yz[ind], M500[ind], grps[ind]
                        grp0 = (grps == 0)
                        if len(ind) > 0:
                            X = np.concatenate((X, Xz))
                            Y = np.concatenate((Y, Yz))
                            M = np.concatenate((M, M500))
                            if simidx in idxScat and do_plot:
                                if hlightfit and do_fit:
                                    # Plot solid markers for objects included
                                    # in fit and open otherwise 
                                    notfit = (M500 * E(redshift)**M_power_fit < M500min_fit)
                                    if hlightcol is None:
                                        ax.plot(Xz[notfit], Yz[notfit], marker=markers[idx], alpha=notfit_alpha,
                                                mfc='none', mec=mec[idx], ms=ms, ls='none', mew=mew, zorder=zorder)
                                        ax.plot(Xz[~notfit], Yz[~notfit], marker=markers[idx],
                                                mfc=mec[idx], mec=mec[idx], ms=ms, ls='none', mew=mew, zorder=zorder)
                                    else:
                                        # Plot groups>0 in given colour
                                        # not group 0 and not fit
                                        mask = np.array([all(tup) for tup in zip(notfit, ~grp0)], dtype=bool)
                                        ax.plot(Xz[mask], Yz[mask], marker=markers[idx], alpha=notfit_alpha,
                                                mfc='none', mec=hlightcol, ms=ms, ls='none', mew=mew, zorder=zorder-1)
                                        # not group 0 and fit
                                        mask = np.array([all(tup) for tup in zip(~notfit, ~grp0)], dtype=bool)
                                        ax.plot(Xz[mask], Yz[mask], marker=markers[idx],
                                                mfc=hlightcol, mec=hlightcol, ms=ms, ls='none', mew=mew, zorder=zorder-1)
                                        # group 0 if not fit
                                        mask = np.array([all(tup) for tup in zip(notfit, grp0)], dtype=bool)
                                        ax.plot(Xz[mask], Yz[mask], marker=markers[idx], alpha=notfit_alpha,
                                                mfc='none', mec=mec[idx], ms=ms, ls='none', mew=mew, zorder=zorder)
                                        # group 0 if fit
                                        mask = np.array([all(tup) for tup in zip(~notfit, grp0)], dtype=bool)
                                        ax.plot(Xz[mask], Yz[mask], marker=markers[idx],
                                                mfc=mec[idx], mec=mec[idx], ms=ms, ls='none', mew=mew, zorder=zorder)
                                elif hlightcol is not None:
                                    # Plot groups>0 in given colour
                                    ax.plot(Xz[~grp0], Yz[~grp0], marker=markers[idx],
                                            mfc=hlightcol, mec=hlightcol, ms=ms, ls='none', mew=mew, zorder=zorder-1)
                                    ax.plot(Xz[grp0], Yz[grp0], marker=markers[idx], mfc=mec[idx]
                                            if hlightzooms else mfc[idx], mec=mec[idx], ms=ms, ls='none', mew=mew, zorder=zorder)
                                else:
                                    ax.plot(Xz, Yz, marker=markers[idx], mfc=mec[idx] if hlightzooms else mfc[idx], mec=mec[idx], ms=ms, ls='none', mew=mew, zorder=zorder)
                                if zidx in LabelZooms:
                                    LabelZooms_label.append(
                                            str(zoom).replace("_", r"\_"))
                                    LabelZooms_X.append(Xz[grp0])
                                    LabelZooms_Y.append(Yz[grp0])
                    elif skip_missing_files:
                        if verbose:
                            print "Could not find file", zinfile
                        if snapnum is not None:
                            print "Skipping zoom", zoom, "for snapshot", snapnum
                        else:
                            print "Skipping zoom", zoom, "for redshift", redshift
                    else:
                        raise IOError("Could not find file "+str(zinfile))
            if simidx in idxMed and do_plot:
                if bin_ax == 0:
                    med, bin_edges, n = stats.binned_statistic(
                        X, Y, statistic='median', bins=bins)
                    ax.plot(x[np.isfinite(med)], med[np.isfinite(med)],
                            c=medcol[idx], ls=ls[idx], lw=lw,
                            dash_capstyle='round', label=label)
                elif bin_ax == 1:
                    med, bin_edges, n = stats.binned_statistic(
                            Y, X, statistic='median', bins=bins)
                    ax.plot(med[np.isfinite(med)], x[np.isfinite(med)],
                            c=medcol[idx], ls=ls[idx], lw=lw,
                            dash_capstyle='round', label=label)
                label = None  # don't want two legend entries

            if do_fit:
                if M500min_fit > 0:  # fit to objects above this mass limit
                    assert len(M) == len(X), "M and X are not the same length."
                    ind = np.where(M * E(redshift)**M_power_fit >= M500min_fit)[0]
                    #ind = ind[[4, 7, 4, 4, 7, 7, 3, 4]]
                    #ind = ind[[3, 1, 0, 4, 4, 2, 3, 7]]
                    #ind = ind[[0,1,2,4,6,7,12,14,15,16]]
                    #ax.plot(X[ind], Y[ind], marker='o', mfc='none', mec='r', ms=ms*2, ls='none', mew=mew, zorder=zorder)
                    if verbose:
                        print "Median mass of fit sample = {:.3e}".format(np.median(M[ind]))
                        print "Median "+str(axes[0])+" of fit sample = {:.3e}".format(np.median(X[ind]))
                        print "Median "+str(axes[1])+" of fit sample = {:.3e}".format(np.median(Y[ind]))
                else:  # fit within plotted ranges
                    ind = np.where((X >= x0) & (X <= x1) &
                                   (Y > y0) & (Y < y1))[0]
                num = len(ind)
                if len(ind) > 1:
                    if fit_to_median:
                        med_fit_bins, med_fit = get_median_in_bins(
                                X[ind], Y[ind], M[ind], axes, bin_ax,
                                binwidth=binwidth,
                                M500min=M500min_fit / E(redshift)**M_power_fit)
                        med_fit_x = 10**((np.log10(med_fit_bins[:-1]) +
                                          np.log10(med_fit_bins[1:]))/2)
                        if do_plot:
                            ax.errorbar(med_fit_x, med_fit, ls='none', marker='o',
                                        ms=10, label="median", zorder=100)
                        (slope, slope_err, logC, logC_err,
                         scat, scat_err) = fit_powerlaw(
                            med_fit_x[np.isfinite(med_fit)],
                            med_fit[np.isfinite(med_fit)],
                            X0, inislope, inilogC,
                            bootstrap=bootstrap, method=fit_method,
                            Ntrials=Ntrials, verbose=verbose)
                        # Calculate intrinsic scatter from individual points.
                        scat = estimate_scatter(np.log10(X[ind] / X0),
                                                np.log10(Y[ind]), slope, logC)
                    else:
                        (slope, slope_err, logC, logC_err,
                         scat, scat_err) = fit_powerlaw(
                            X[ind], Y[ind], X0, inislope, inilogC,
                            bootstrap=bootstrap, method=fit_method,
                            Ntrials=Ntrials, verbose=verbose)
                    if do_plot:
                        # Dashed line for extrapolation.
                        xmin, xmax = axeslims[0][0], axeslims[0][1]
                        ax.plot([xmin, xmax],
                                [10**powerlaw(np.log10(xmin), slope, logC),
                                10**powerlaw(np.log10(xmax), slope, logC)],
                                c=fit_col[idx], lw=fit_lw, alpha=fit_alpha,
                                ls='--', zorder=zorder+1)
                        # Solid line in fit region.
                        xmin, xmax = np.min(X[ind]), np.max(X[ind])
                        ax.plot([xmin, xmax],
                                [10**powerlaw(np.log10(xmin), slope, logC),
                                10**powerlaw(np.log10(xmax), slope, logC)],
                                c=fit_col[idx], lw=fit_lw, alpha=fit_alpha,
                                ls='-', zorder=zorder+1, label=label)
                    if verbose:
                        print "slope = {:.3f} [{:.3f}, {:.3f}]".format(
                                slope, slope_err[0], slope_err[1])
                        print "logC = {:.3f} [{:.3f}, {:.3f}]".format(
                                logC, logC_err[0], logC_err[1])
                        print "scat = {:.3f} [{:.3f}, {:.3f}]".format(
                                scat, scat_err[0], scat_err[1])
                else:
                    slope, slope_err, logC, logC_err, scat, scat_err = [None]*6

                if plot_scat:
                    if axes[0] == "mass":
                        xmin, xmax = 1e12, 1e16
                        binwidth = 0.4
                        # pivot point of broken power law
                        Xc = M500min_fit / E(redshift)**M_power_fit
                        # indicate lower mass limit of fit
                        axscat.axvline(M500min_fit / E(redshift)**M_power_fit,
                                       ls='-', lw=3, alpha=0.5, c=mec[idx])
                    elif axes[0] in temperatures:
                        xmin, xmax = 0.1, 10
                        binwidth = 0.3
                        # pivot point of broken power law
                        if axes[1] in luminosities:
                            Xc = 0.5
                        elif axes[1] == "mass":
                            Xc = 0.5
                        else:
                            Xc = 0.5
                        axscat.xaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))
                    else:
                        raise NotImplementedError(
                            "axes[0] = "+str(axes[0])+" unsupported for plot_scat")
                    xbins = np.logspace(np.log10(xmin), np.log10(xmax), num=(np.log10(xmax) - np.log10(xmin))/binwidth + 1)
                    xbins2 = np.logspace(np.log10(xmin), np.log10(xmax), num=100)

                    if axes[1] in luminosities:
                        # Exclude objects with luminosity < 10^39 erg/s
                        # as these are unrealistic spectral fits.
                        X = X[Y > 1e39]
                        Y = Y[Y > 1e39]

                    # define arrays to hold scatter and error relative to fit
                    scatm_fit = np.zeros(xbins.shape[0] - 1)
                    scatm_fit_err = np.zeros((xbins.shape[0] - 1, 2))
                    # also for scatter relative to the mean, i.e. st. deviation
                    scatm_mean = np.zeros(xbins.shape[0] - 1)
                    #scatm_mean_err = np.zeros((xbins.shape[0] - 1, 2))
                    # and scatter relative to quadratic fit
                    #scatm_quad = np.zeros(xbins.shape[0] - 1)
                    # and scatter relative to broken power law fit
                    scatm_bpl = np.zeros(xbins.shape[0] - 1)
                    nbin = np.zeros(xbins.shape[0] - 1)

                    import bootstrap
                    import functools
                    func = functools.partial(estimate_scatter,
                                             slope=slope, logC=logC)

                    # standard deviation function ddof degress of freedom
                    std = functools.partial(np.std, ddof=2)

                    # get quadratic fit in log-space
                    # f = np.poly1d(np.polyfit(np.log10(X/X0), np.log10(Y), 2))
                    # plot the fit
                    # ax.plot(xbins, 10**f(np.log10(xbins/X0)), c='r', lw=fit_lw, alpha=fit_alpha)

                    # get broken power law fit
                    # Create the powerlaw function for a given pivot point Xc
                    from scaling.fit_relation import make_broken_powerlaw
                    broken_powerlaw = make_broken_powerlaw(np.log10(Xc))
                    from scipy.optimize import curve_fit
                    popt, pcov = curve_fit(
                            broken_powerlaw, np.log10(X.astype(np.float64)),
                            np.log10(Y.astype(np.float64)),
                            p0=[inislope, inislope, inilogC])
                    # plot initial guess
                    # ax.plot(xbins2, 10**broken_powerlaw(np.log10(xbins2), inislope, inislope, inilogC), c='g', ls='--', lw=fit_lw, alpha=fit_alpha)
                    # plot best-fitting broken power law
                    ax.plot(xbins2, 10**broken_powerlaw(np.log10(xbins2), *popt),
                            c=fit_col[idx], lw=fit_lw, ls='--', alpha=fit_alpha)

                    for i, xbin in enumerate(xbins[:-1]):
                        binind = np.where((X >= xbins[i]) &
                                          (X < xbins[i+1]))[0]
                        scatm_fit[i] = func(np.log10(X[binind])-np.log10(X0),
                                            np.log10(Y[binind]))
                        scatm_mean[i] = std(np.log10(Y[binind]))
                        if len(binind) > 2:
                            # scatter relative to quadratic fit
                            # scatm_quad[i] = (np.sum((np.log10(Y[binind]) - f(np.log10(X[binind]/X0)))**2) / float(len(binind)-2))**0.5
                            # scatter relative to broken power law fit
                            # scatm_bpl[i] = (np.sum((np.log10(Y[binind]) - broken_powerlaw(np.log10(X[binind]), *popt))**2) / float(len(binind)-2))**0.5
                            
                            # scatter relative to mean with the slope of the
                            # broken power law fit factored out
                            # I subtract from Y the differential of the
                            # best-fit relative to its midpoint in the bin
                            scatm_bpl[i] = (std(np.log10(Y[binind]) -
                                            (broken_powerlaw(np.log10(X[binind]), *popt) -
                                             broken_powerlaw((np.log10(xbins[i])+np.log10(xbins[i+1]))/2.0, *popt))))
                        # estimate uncertainty in scatter as the
                        # confidence interval 1-alpha
                        alpha = 0.05

                        # estimate 95% confidence interval on scatter
                        # (of the population) assuming normality such that
                        # the (sample) scatter has a chi-squared distribution
                        # n = len(binind)
                        # scatm_err[i, 0] = scatm[i] - np.sqrt((n - 1) * scatm[i]**2 / chi2.isf(alpha/2.0, n-1))
                        # scatm_err[i, 1] = np.sqrt((n - 1) * scatm[i]**2 / chi2.isf(1.0-alpha/2.0, n-1)) - scatm[i]

                        # estimate errors from bootstrapping
                        if len(binind) >= 3:
                            if verbose:
                                print "Bootstrapping..."
                            # relative to fit
                            result = bootstrap.ci(
                                        (np.log10(X[binind]/X0),
                                         np.log10(Y[binind])),
                                        func, alpha=alpha,
                                        n_samples=10000,
                                        method='basic', output='errorbar')
                            scatm_fit_err[i, 0] = result[0]
                            scatm_fit_err[i, 1] = result[1]
                            # relative to mean
                            #result = bootstrap.ci((np.log10(Y[binind])), std, alpha=alpha, n_samples=10000, method='basic', output='errorbar')
                            #scatm_mean_err[i, 0] = result[0]
                            #scatm_mean_err[i, 1] = result[1]

                        nbin[i] = len(binind)
                        if verbose and nbin[i] > 0:
                            print ("scatm[{:d}] =".format(i), scatm_fit[i],
                                   "N =", nbin[i])
                    bincent = np.sqrt(xbins[1:]*xbins[:-1])
                    binind = np.where(nbin >= 3)[0]
                    # plot scatter relative to fit
                    axscat.errorbar(bincent[binind], scatm_fit[binind],
                                    yerr=[scatm_fit_err[binind, 0],
                                          scatm_fit_err[binind, 1]],
                                    mec=mec[idx], mfc=mec[idx], c=mec[idx],
                                    label=labels[idx], **scat_kwargs)
                    # plot scatter relative to mean
                    #axscat.errorbar(bincent[binind], scatm_mean[binind],
                                    #yerr=[scatm_mean_err[binind, 0],
                                          #scatm_mean_err[binind, 1]],
                                    #mec=mec[idx], mfc='none', c=mec[idx],
                                    #ls='--', **scat_kwargs)
                    # plot scatter relative to broken power law fit
                    axscat.errorbar(bincent[binind], scatm_bpl[binind],
                                    mec=mec[idx], mfc='none', c=mec[idx],
                                    ls='--', **scat_kwargs)
                    # dotted line for best-fitting scatter
                    axscat.axhline(scat, c=mec[idx], ls='dotted', lw=3)

                    if save_scat:
                        axscat.legend(loc='best', frameon=True)
                        figscat.savefig(outdir+"scatter_" +
                                        str(axes[0])+"_"+str(axes[1])+".pdf")
                        print "Saved scatter plot to", outdir + \
                            "scatter_"+str(axes[0])+"_"+str(axes[1])+".pdf"
            # Store results in a dictionary.
            result = {'X': X, 'Y': Y, 'M': M}
            if do_fit:
                # Add fit parameters.
                result['slope'] = slope
                result['slope_err'] = slope_err
                result['logC'] = logC
                result['logC_err'] = logC_err
                result['scat'] = scat
                result['scat_err'] = scat_err
                result['num'] = num
            # Store this dictionary in the 'results' dictionary.
            results['result_{:d}'.format(int(idx))] = result

    # Simulations
    if plot_sims and do_plot:
        plot_sims_data("MACSIS", ax, axes, axeslims, z_comp, lw=fit_lw,
                       zorder=zorder-2, h_scale=h_scale)
        plot_sims_data("Truong", ax, axes, axeslims, z_comp, lw=fit_lw,
                       zorder=zorder-2, h_scale=h_scale)

    if do_plot:
        # Plot a line of unity if the axes are comparable.
        if (set(axes).issubset(temperatures) or
                set(axes).issubset(luminosities)):
            ax.plot(ax.get_xlim(), ax.get_xlim(), c='r', alpha=0.5,
                    ls='--', lw=3, zorder=9999)
        if len(LabelZooms) > 0:
            # Plot the gathered labels for the zooms.
            for i, zlabel in enumerate(LabelZooms_label):
                ax.annotate(zlabel, xy=(LabelZooms_X[i], LabelZooms_Y[i]),
                            xytext=(8, 0), textcoords='offset points',
                            color='k',
                            fontsize=10, zorder=999)
                ax.plot(LabelZooms_X[i], LabelZooms_Y[i], marker='o',
                        ms=ms*1.5, c='k', mfc='none', mew=mew, zorder=999)

        if plotlabel is not None:
            ax.text(0.9, 0.1, plotlabel, horizontalalignment='right',
                    verticalalignment='center', transform=ax.transAxes)
        if plotlabel_upleft is not None:
            ax.text(0.1, 0.9, plotlabel_upleft, horizontalalignment='left',
                    verticalalignment='top', transform=ax.transAxes)

#        # Don't use the default scientific format for axis labels
#        # if the axis is temperature
#        if axes[0] in temperatures:
#            ax.xaxis.set_major_formatter(
#                    ticker.FuncFormatter(lambda y, pos: ('{{:.{:1d}f}}'.format(
#                            int(np.maximum(-np.log10(y), 0)))).format(y)))
#        if axes[1] in temperatures:
#            ax.yaxis.set_major_formatter(
#                    ticker.FuncFormatter(lambda y, pos: ('{{:.{:1d}f}}'.format(
#                            int(np.maximum(-np.log10(y), 0)))).format(y)))

        if not have_ax:
            # Add legend(s).
            # define default ax.legend() kwargs.
            default_leg_kwargs = {'loc': 'upper left', 'borderaxespad': 1,
                                  'fontsize': 'medium'}
            # fill in any missing keys in leg_kwargs with the defaults.
            for key in default_leg_kwargs:
                if key not in leg_kwargs:
                    leg_kwargs[key] = default_leg_kwargs[key]
            if leg_len is None:
                # Place all legend entries in one legend.
                ax.legend(**leg_kwargs)
            else:
                # Two legends. One upper left and the other lower right.
                del leg_kwargs['loc']
                handles, labels = ax.get_legend_handles_labels()
                leg = ax.legend(handles[:leg_len], labels[:leg_len],
                                loc='upper left', **leg_kwargs)
                ax.add_artist(leg)
                ax.legend(handles[leg_len:], labels[leg_len:],
                          loc='lower right', **leg_kwargs)

            plt.tight_layout(pad=0.45)
            if outname is not None:
                fig.savefig(outdir+outname)
                print "Saved to", outdir+outname

    return results


def plot_obs_data(ax, axes, z,
                  xray_obs={}, SZ_obs={},
                  label_redshift=False,
                  ms=8, mew=1.6, errlw=1.4, lw=3, Epow={},
                  omega_m=0.3089, omega_l=0.6911, h_scale=0.6774,
                  obs_dir="/data/vault/nh444/ObsData/"):
    """ Plot observational data for a given scaling relation.

    Arguments:
        ax (pyplot.axes object): axes to plot data to.
        axes (2-list of strings): scaling relation to plot.
        xray_obs (dictionary): X-ray observations to compare to.
                               See defaults below.
        SZ_obs (dictionary): SZ observations to compare to.
                             See defaults in SZ_scaling.plot_SZ_obs
        label_redshift (bool): put median redshift of observational
                               data in legend label
        ms (float): marker size.
        mew (float): marker edge width for open symbols.
        errlw (float): Line width of error bars.
        lw: Line width of best-fit lines.
        Epow (dictionary): power of E(z) to multiply y-axis value.
        omega_m, omega_l, h_scale: cosmology to scale to.
        obs_dir (string): path of base directory containing data.
    """
    import numpy as np

    def E(z): return (omega_m*(1+z)**3 + omega_l)**0.5

    # default dictionary for xray_obs with all entries True
    xray_obs_def = {'Mahdavi13': True,
                    'Eckert16': True,
                    'Giles16': True,
                    'Lieu16': True,
                    'Reichert11': True,
                    'Fassbender11': True,
                    'Hilton12': True,
                    'Clerc14': True,
                    'Maughan08': True,
                    'Mantz16': True,
                    'Bartalucci17': True,
                    'Dietrich19': True,
                    }
    # make a copy of the given dictionary so that we don't modify the original
    import copy
    my_xray_obs = copy.deepcopy(xray_obs)
    # fill in any missing keys
    for key in xray_obs_def:
        if key not in my_xray_obs:
            my_xray_obs[key] = xray_obs_def[key]

    c1 = '0.5'#'0.6'  # X-ray data only
    c2 = 'k'#'0.3'  # lensing data
    if errlw < 0.01:  # no error bars
        cap = 0  # error bar cap size
    else:
        cap = ms * 0.6  # error bar cap size

    #M200toM500 = 1.0/1.5  # factor to convert M200 to M500 where necessary

    minz = z - 0.1  # 0.0

    if my_xray_obs['Mahdavi13']:
        # Load Mahdavi+2013 data and select based on redshift
        Mah = np.genfromtxt(obs_dir+"Mahdavi2013.csv", delimiter=",")
        if z > 0.59 and z < 0.61:
            Mahind = np.where(Mah[:, 1] > 0.46)[0]
        elif z > 0.39 and z < 0.41:
            Mahind = np.where(Mah[:, 1] > 0.28)[0]
        else:
            Mahind = np.where(Mah[:, 1] > minz)[0]
        if len(Mahind) == 0:
            my_xray_obs['Mahdavi13'] = False
        if label_redshift:
            label = (r"Mahdavi+ 2013 ($z_{\mathrm{med}}=" +
                     "{:.2f}".format(np.median(Mah[Mahind, 1]))+"$)")
        else:
            label = "Mahdavi+ 2013"
        Mah_kwargs = {'c': c2, 'mec': c2, 'mfc': 'none', 'marker': "D",
                      'ls': 'none', 'ms': ms*0.9, 'mew': mew, 'capsize': cap,
                      'lw': errlw, 'label': label}

    if my_xray_obs['Eckert16'] or my_xray_obs['Giles16']:
        # Load XXL-100-GC data and select by redshift
        # bolometric lum within r500 and masses estimated from
        # M-T relation from Lieu 2016 for weak lensing masses
        # (I convert their quoted r500 to M500)
        XXL = np.genfromtxt(
            obs_dir+"XXL_xray_properties.csv", delimiter=",")
        if z > 0.59 and z < 0.61:
            XXLind = np.where(XXL[:, 1] > 0.41)[0]  # median 0.609
        elif z > 0.39 and z < 0.41:  # z=0.4
            # median 0.392
            XXLind = np.where((XXL[:, 1] > 0.3) & (XXL[:, 1] < 0.5))[0]
        else:
            XXLind = np.where(XXL[:, 1] > minz)[0]
        if len(XXLind) == 0:
            my_xray_obs['Eckert16'] = False
            my_xray_obs['Giles16'] = False
        elif label_redshift:
            XXL_label_suffix = (r" ($z_{\mathrm{med}}=" +
                                "{:.2f}".format(np.median(XXL[XXLind, 1])) +
                                "$)")
        else:
            XXL_label_suffix = ""
    if (my_xray_obs['Eckert16'] or my_xray_obs['Giles16'] or
            my_xray_obs['Lieu16']):
        # Lieu+ 2016 share the same symbols since they
        # are a subsample of the XXL-100-GC clusters
        XXL_kwargs = {'c': c2, 'mec': c2, 'mfc': 'none', 'marker': 'o',
                      'ls': 'none', 'ms': ms, 'mew': mew, 'capsize': cap,
                      'lw': errlw}

    if my_xray_obs['Reichert11']:
        # Load Reichert+2011 data
        Rei = np.genfromtxt(obs_dir+"L-T/"+"Reichert_2011.csv", delimiter=",")
        if z > 0.59 and z < 0.61:
            Reind = np.where(Rei[:, 1] >= 0.28)[0]  # median 0.6
        elif z > 0.39 and z < 0.41:
            # median 0.406
            Reind = np.where((Rei[:, 1] > 0.3) & (Rei[:, 1] < 0.5))[0]
        elif z > 0.99 and z < 1.01:
            # median 0.994
            Reind = np.where((Rei[:, 1] > 0.89) & (Rei[:, 1] < 1.11))[0]
        else:
            Reind = np.where(Rei[:, 1] > minz)[0]
        if len(Reind) == 0:
            my_xray_obs['Reichert11'] = False
        if label_redshift:
            label = (r"Reichert+ 2011 ($z_{\mathrm{med}}=" +
                     "{:.2f}".format(np.median(Rei[Reind, 1]))+"$)")
        else:
            label = "Reichert+ 2011"
        Rei_kwargs = {'c': c1, 'mec': 'none', 'mfc': c1, 'marker': 'o',
                      'ls': 'none', 'ms': ms*1.1, 'capsize': cap,
                      'lw': errlw, 'label': label}

    if my_xray_obs['Fassbender11']:
        # Fassbender 2011 (z ~ 1)
        Fass = np.genfromtxt(obs_dir+"high-z_clusters/"+"Fassbender_11.csv",
                             delimiter=",")
        # They convert M500 to M200 by the factor 1.54
        M200toM500_Fass = 1.0 / 1.54
        if z > 0.95 and z < 1.05:
            if "mass" in axes:
                # clusters with measured masses and z>1
                # Fassind = [4, 7, 15, 18]
                Fassind = [15, 18]  # clusters with measured masses and z~1
            else:
                # median 0.9875
                Fassind = np.where((Fass[:, 1] > 0.9) &
                                   (Fass[:, 1] < 1.1))
            if label_redshift:
                label = (r"Fassbender+ 2011 ($z_{\mathrm{med}}=" +
                         "{:.2f}".format(np.median(Fass[Fassind, 1]))+"$)")
            else:
                label = "Fassbender+ 2011"
            Fass_kwargs = {'c': c1, 'mec': 'none', 'mfc': c1, 'marker': 'v',
                           'ls': 'none', 'ms': ms*1.3, 'capsize': cap,
                           'lw': errlw, 'label': label}
        else:
            my_xray_obs['Fassbender11'] = False

    if my_xray_obs['Bartalucci17']:
        Bart = np.genfromtxt(obs_dir+"high-z_clusters/"+"Bartalucci_17.csv",
                             delimiter=",")
        if z > 0.9:
            Bartind = np.where(Bart[:, 1] > 0.9)[0]  # All five clusters
            if label_redshift:
                label = (r"Bartalucci+ 2017 ($z_{\mathrm{med}}=" +
                         "{:.2f}".format(np.median(Bart[Bartind, 1]))+"$)")
            else:
                label = "Bartalucci+ 2017"
            Bart_kwargs = {'c': c1, 'mec': 'none', 'mfc': c1, 'marker': 'd',
                           'ls': 'none', 'ms': ms*1.3, 'capsize': cap,
                           'lw': errlw, 'label': label}
        else:
            my_xray_obs['Bartalucci17'] = False

    if my_xray_obs['Maughan08']:
        Mau = np.genfromtxt(obs_dir+"Maughan2008.csv", delimiter=",")
        if z > 0.59 and z < 0.61:
            # median z = 0.59
            Mauind = np.where((Mau[:, 1] >= 0.55) & (Mau[:, 1] <= 0.7))[0]
        elif z > 0.39 and z < 0.41:
            # median z = 0.4
            Mauind = np.where((Mau[:, 1] >= 0.3) & (Mau[:, 1] <= 0.5))[0]
        elif z > 0.99 and z < 1.01:
            # median z = 0.96
            Mauind = np.where((Mau[:, 1] >= 0.83))[0]
        else:
            Mauind = np.where(Mau[:, 1] > minz)[0]
        if len(Mauind) == 0:
            my_xray_obs['Maughan08'] = False
        if label_redshift:
            label = (r"Maughan+ 2008 ($z_{\mathrm{med}}=" +
                     "{:.2f}".format(np.median(Mau[Mauind, 1]))+"$)")
        else:
            label = "Maughan+ 2008"
        Maughan_kwargs = {'c': c1, 'mec': 'none', 'marker': 's', 'ls': 'none',
                          'ms': ms, 'capsize': cap, 'lw': errlw,
                          'label': label}

    # M500-T500
    if axes[0] in ["T500", "T500mw", "T500ew", "T500sl"] and axes[1] == "mass":
        if my_xray_obs['Maughan08']:
            ax.errorbar(Mau[Mauind, 4],
                        Mau[Mauind, 3] * 1e14 * 0.7 / h_scale *
                        E(Mau[Mauind, 1])**Epow['M-T'],
                        xerr=[np.abs(Mau[Mauind, 6]), Mau[Mauind, 5]],
                        zorder=3, **Maughan_kwargs)
        if my_xray_obs['Fassbender11']:
            ax.errorbar(Fass[Fassind, 15],
                        Fass[Fassind, 7] * 1e14 * M200toM500_Fass * 0.7 / h_scale *
                        E(Fass[Fassind, 1])**Epow['M-T'],
                        xerr=[np.abs(Fass[Fassind, 17]), Fass[Fassind, 16]],
                        yerr=[np.abs(Fass[Fassind, 9]) * 1e14 * M200toM500_Fass *
                              0.7 / h_scale * E(Fass[Fassind, 1])**Epow['M-T'],
                              Fass[Fassind, 8] * 1e14 * M200toM500_Fass * 0.7 /
                              h_scale * E(Fass[Fassind, 1])**Epow['M-T']],
                        zorder=3, **Fass_kwargs)
        if my_xray_obs['Reichert11']:
            ax.errorbar(Rei[Reind, 3],
                        Rei[Reind, 9] * 1e14 * 0.7 / h_scale *
                        E(Rei[Reind, 1])**Epow['M-T'],
                        xerr=[np.abs(Rei[Reind, 5]), Rei[Reind, 4]],
                        yerr=[np.abs(Rei[Reind, 11] * 1e14 * 0.7 / h_scale) *
                              E(Rei[Reind, 1])**Epow['M-T'],
                              Rei[Reind, 10] * 1e14 * 0.7 / h_scale *
                              E(Rei[Reind, 1])**Epow['M-T']],
                        zorder=4, **Rei_kwargs)
        if my_xray_obs['Mahdavi13']:
            # need to check that array is not empty, else plt.errorbar crashes
            # see https://github.com/matplotlib/matplotlib/issues/9699/
            ax.errorbar(Mah[Mahind, 5],
                        Mah[Mahind, 11]*1e14*(0.7/h_scale)*E(Mah[Mahind, 1])**Epow['M-T'],
                        xerr=Mah[Mahind, 6],
                        yerr=Mah[Mahind, 12]*1e14*(0.7/h_scale)*E(Mah[Mahind, 1]**Epow['M-T']),
                        zorder=5, **Mah_kwargs)

    # M500-T_300kpc
    if (axes[0] in ["T300kpc", "T500", "T500mw", "T500ew", "T500sl"] and
            axes[1] == "mass"):
        if my_xray_obs['Lieu16']:
            Lieu = np.genfromtxt(obs_dir+"M-T/"+"XXL_Lieu2016.csv",
                                 delimiter=",")  # 0-300 kpc
            if z > 0.59 and z < 0.61:
                ind = np.where(Lieu[:, 1] > 0.43)[0]
            elif z > 0.39 and z < 0.41:
                ind = np.where(Lieu[:, 1] > 0.255)[0]
            else:
                ind = np.where(Lieu[:, 1] > minz)[0]
        if len(ind) > 0 and my_xray_obs['Lieu16']:
            uplims = (Lieu[ind, 10] == 0)  # points as upper limits
            # make the upper limit arrows smaller.
            Lieu[ind[uplims], 11] *= 0.3
            ax.errorbar(Lieu[ind, 2],
                        Lieu[ind, 9]*1e14*0.7/h_scale*E(Lieu[ind, 1])**Epow['M-T'],
                        xerr=[np.absolute(Lieu[ind, 4]), Lieu[ind, 3]],
                        yerr=[np.absolute(Lieu[ind, 11]*1e14*0.7/h_scale*E(Lieu[ind, 1])**Epow['M-T']),
                              Lieu[ind, 10]*1e14*0.7/h_scale*E(Lieu[ind, 1])**Epow['M-T']],
                        uplims=uplims if errlw > 0 else None, zorder=3,
                        label="Lieu+ 2016", **XXL_kwargs)
            if not (errlw > 0):
                # overplot the upper limit arrows.
                ax.errorbar(Lieu[ind[uplims], 2],
                            Lieu[ind[uplims], 9] * 1e14 * 0.7 / h_scale *
                            E(Lieu[ind[uplims], 1])**Epow['M-T'],
                            yerr=[np.absolute(Lieu[ind[uplims], 11]) *
                                  1e14 * 0.7 / h_scale *
                                  E(Lieu[ind[uplims], 1])**Epow['M-T'],
                                  Lieu[ind[uplims], 10] *
                                  1e14 * 0.7 / h_scale *
                                  E(Lieu[ind[uplims], 1])**Epow['M-T']],
                            uplims=True, zorder=3, lw=mew, c=c2, ls='none')

    # bolometric L500 vs T500
    if (axes[0] in ["T500", "T500mw", "T500ew", "T500sl"] and
            axes[1] in luminosities):
        if z < 0.1:
            from scaling.plotting.plotObsDat import L_T
            L_T(ax, core_excised=False, ms=ms, for_paper=True, h=h_scale)
        else:
            if my_xray_obs['Maughan08']:
                ax.errorbar(Mau[Mauind, 4],
                            Mau[Mauind, 7] * 1e44 * (0.7 / h_scale)**2*E(Mau[Mauind, 1])**Epow['L-T'],
                            xerr=[np.abs(Mau[Mauind, 6]), Mau[Mauind, 5]],
                            yerr=Mau[Mauind, 8] * 1e44 * (0.7 / h_scale)**2*E(Mau[Mauind, 1])**Epow['L-T'],
                            zorder=3, **Maughan_kwargs)
            if my_xray_obs['Fassbender11']:
                ax.errorbar(Fass[Fassind, 15],
                            Fass[Fassind, 13]*1e44*E(Fass[Fassind, 1])**Epow['L-T']*(0.7**2 / h_scale**2),
                            xerr=[np.abs(Fass[Fassind, 17]), Fass[Fassind, 16]],
                            yerr=Fass[Fassind, 14]*1e44*E(Fass[Fassind, 1])**Epow['L-T']*(0.7**2 / h_scale**2),
                            zorder=3, **Fass_kwargs)
            if my_xray_obs['Reichert11']:
                ax.errorbar(Rei[Reind, 3],
                            Rei[Reind, 6]*1e44*E(Rei[Reind, 1])**Epow['L-T']*(0.7**2 / h_scale**2),
                            xerr=[np.abs(Rei[Reind, 5]), Rei[Reind, 4]],
                            yerr=[np.abs(Rei[Reind, 8])*1e44*E(Rei[Reind, 1])**Epow['L-T']*(0.7**2 / h_scale**2),
                                  Rei[Reind, 7]*1e44*E(Rei[Reind, 1])**Epow['L-T']*(0.7**2 / h_scale**2)],
                            zorder=4, **Rei_kwargs)

            if my_xray_obs['Hilton12']:
                Hilt = np.genfromtxt(obs_dir+"L-T/"+"Hilton_2012.csv",
                                     delimiter=",")
                if z > 0.59 and z < 0.61:
                    ind = np.where(Hilt[:, 1] > 0.42)[0]  # median 0.59
                elif z > 0.39 and z < 0.41:
                    # median 0.41
                    ind = np.where((Hilt[:, 1] >= 0.3) &
                                   (Hilt[:, 1] < 0.51))[0]
                elif z > 0.99 and z < 1.01:
                    # median 1.00
                    ind = np.where((Hilt[:, 1] > 0.9) &
                                   (Hilt[:, 1] < 1.14))[0]
                else:
                    ind = np.where(Hilt[:, 1] > minz)[0]
                if label_redshift:
                    label = ("Hilton+ 2012 ($z_{\mathrm{med}}=" +
                             "{:.2f}".format(np.median(Hilt[ind, 1]))+"$)")
                else:
                    label = "Hilton+ 2012"
                if len(ind) > 0:
                    ax.errorbar(Hilt[ind, 5],
                                Hilt[ind, 2]*1e44*E(Hilt[ind, 1])**Epow['L-T']*(0.7**2 / h_scale**2),
                                xerr=[Hilt[ind, 6], Hilt[ind, 7]],
                                yerr=[Hilt[ind, 3]*1e44*E(Hilt[ind, 1])**Epow['L-T']*(0.7**2 / h_scale**2),
                                      Hilt[ind, 4]*1e44*E(Hilt[ind, 1])**Epow['L-T']*(0.7**2 / h_scale**2)],
                                c=c1, mec='none', mfc=c1, marker='D',
                                ls='none', ms=ms, capsize=cap, lw=errlw,
                                zorder=5, label=label)
                
            if my_xray_obs['Clerc14']:
                Clerc = np.genfromtxt(obs_dir+"L-T/"+"Clerc_2014.csv",
                                      delimiter=",")
                if z > 0.59 and z < 0.61:
                    ind = np.where(Clerc[:, 3] > 0.31)[0]  # median 0.61
                elif z > 0.39 and z < 0.41:
                    ind = np.where(Clerc[:, 3] > 0.2)[0]  # median 0.43
                else:
                    ind = np.where(Clerc[:, 3] > minz)[0]
                if len(ind) > 0:
                    ax.errorbar(Clerc[ind, 9],
                                Clerc[ind, 13]*1e43*E(Clerc[ind, 3])**Epow['L-T']*(0.7**2 / h_scale**2),
                                xerr=[np.abs(Clerc[ind, 11]), Clerc[ind, 10]],
                                yerr=Clerc[ind, 14]*1e43*E(Clerc[ind, 3])**Epow['L-T']*(0.7**2 / h_scale**2),
                                c=c1, mec='none', mfc=c1, marker='s',
                                ls='none', ms=ms, capsize=cap, lw=errlw,
                                zorder=6, label="Clerc+ 2014")

            if my_xray_obs['Mahdavi13']:
                ax.errorbar(Mah[Mahind,5],
                            Mah[Mahind,2]*1e45*(0.7/h_scale)**2 / E(Mah[Mahind,1])**Epow['L-T'],
                            xerr=Mah[Mahind,6],
                            yerr=Mah[Mahind,3]*1e45*(0.7/h_scale)**2 / E(Mah[Mahind,1])**Epow['L-T'],
                            zorder=7, **Mah_kwargs)
            if my_xray_obs['Giles16']:
                ## WARNING: The XXL temperatures are measured within 300 kpc (although Giles+16 find little difference with T500)
                ax.errorbar(XXL[XXLind,2],
                            XXL[XXLind,24]*1e43*E(XXL[XXLind,1])**Epow['L-T']*(0.7**2 / h_scale**2),
                            xerr=[np.abs(XXL[XXLind,4]), XXL[XXLind,3]],
                            yerr=XXL[XXLind,25]*1e43*E(XXL[XXLind,1])**Epow['L-T']*(0.7**2 / h_scale**2),
                            zorder=8, label="Giles+ 2016"+XXL_label_suffix, **XXL_kwargs)

    # bolometric L500-M500
    if axes[0] == "mass" and axes[1] in luminosities:
        if my_xray_obs['Maughan08']:
            ax.errorbar(Mau[Mauind, 3] * 1e14 * 0.7 / h_scale,
                        Mau[Mauind, 7] * 1e44 * (0.7 / h_scale)**2*E(Mau[Mauind, 1])**Epow['L-M'],
                        yerr=Mau[Mauind, 8] * 1e44 * (0.7 / h_scale)**2*E(Mau[Mauind, 1])**Epow['L-M'],
                        zorder=3, **Maughan_kwargs)
        if my_xray_obs['Fassbender11']:
            ax.errorbar(Fass[Fassind, 7]*1e14*M200toM500_Fass*0.7/h_scale,
                        Fass[Fassind, 13]*1e44*E(Fass[Fassind, 1])**Epow['L-M']*(0.7**2 / h_scale**2),
                        xerr=[np.abs(Fass[Fassind, 9])*1e14*M200toM500_Fass*0.7/h_scale,
                              Fass[Fassind, 8]*1e14*M200toM500_Fass*0.7/h_scale],
                        yerr=Fass[Fassind, 14]*1e44*E(Fass[Fassind, 1])**Epow['L-M']*(0.7**2 / h_scale**2),
                        zorder=3, **Fass_kwargs)
        if my_xray_obs['Reichert11']:
            ax.errorbar(Rei[Reind,9]*1e14*0.7/h_scale,
                        Rei[Reind,6]*1e44*E(Rei[Reind,1])**Epow['L-M']*(0.7**2 / h_scale**2),
                        xerr=[np.abs(Rei[Reind,11]*1e14*0.7/h_scale),
                              Rei[Reind,10]*1e14*0.7/h_scale],
                        yerr=[np.abs(Rei[Reind,8])*1e44*E(Rei[Reind,1])**Epow['L-M']*(0.7**2 / h_scale**2),
                              Rei[Reind,7]*1e44*E(Rei[Reind,1])**Epow['L-M']*(0.7**2 / h_scale**2)],
                        zorder=4, **Rei_kwargs)
        if my_xray_obs['Mahdavi13']:
            ax.errorbar(Mah[Mahind,11]*1e14*(0.7/h_scale),
                        Mah[Mahind,2]*1e45*(0.7/h_scale)**2*E(Mah[Mahind,1])**Epow['L-M'],
                        xerr=Mah[Mahind,12]*1e14*(0.7/h_scale),
                        yerr=Mah[Mahind,3]*1e45*(0.7/h_scale)**2*E(Mah[Mahind,1])**Epow['L-M'],
                        zorder=5, **Mah_kwargs)
        if my_xray_obs['Giles16']:
            ax.errorbar(XXL[XXLind,8]*1e14*0.7/h_scale,
                        XXL[XXLind,24]*1e43*E(XXL[XXLind,1])**Epow['L-M']*(0.7**2 / h_scale**2),
                        xerr=[XXL[XXLind,10]*1e14*0.7/h_scale,
                              XXL[XXLind,9]*1e14*0.7/h_scale],
                        yerr=XXL[XXLind,25]*1e43*E(XXL[XXLind,1])**Epow['L-M']*(0.7**2 / h_scale**2),
                        zorder=6, label="Giles+ 2016"+XXL_label_suffix, **XXL_kwargs)

    # L500-Mgas,500
    if axes[0] == "gasmass" and axes[1] in luminosities:              
        if my_xray_obs['Maughan08']:
            ax.errorbar(Mau[Mauind, 13] * 1e13 * 0.7 / h_scale,
                        Mau[Mauind, 7] * 1e44 * (0.7 / h_scale)**2*E(Mau[Mauind, 1])**Epow['L-M'],
                        xerr=[np.abs(Mau[Mauind, 15] * 1e13 * 0.7 / h_scale),
                              Mau[Mauind, 14] * 1e13 * 0.7 / h_scale],
                        yerr=Mau[Mauind, 8] * 1e44 * (0.7 / h_scale)**2*E(Mau[Mauind, 1])**Epow['L-M'],
                        zorder=3, **Maughan_kwargs)
        if my_xray_obs['Mahdavi13']:
            ax.errorbar(Mah[Mahind,14]*1e14*(0.7/h_scale),
                        Mah[Mahind,2]*1e45*(0.7/h_scale)**2*E(Mah[Mahind,1])**Epow['L-M'],
                        xerr=Mah[Mahind,15]*1e14*(0.7/h_scale),
                        yerr=Mah[Mahind,3]*1e45*(0.7/h_scale)**2*E(Mah[Mahind,1])**Epow['L-M'],
                        zorder=4, **Mah_kwargs)
        if my_xray_obs['Giles16']:
            ax.errorbar(XXL[XXLind,11]*0.7/h_scale,
                        XXL[XXLind,24]*1e43*E(XXL[XXLind,1])**Epow['L-M']*(0.7**2 / h_scale**2),
                        xerr=[np.abs(XXL[XXLind,13])*0.7/h_scale,
                              XXL[XXLind,12]*0.7/h_scale],
                        yerr=XXL[XXLind,25]*1e43*E(XXL[XXLind,1])**Epow['L-M']*(0.7**2 / h_scale**2),
                        zorder=5, label="Giles+ 2016"+XXL_label_suffix, **XXL_kwargs)

    # Mgas-M500
    if axes[0] == "mass" and axes[1] == "gasmass":
        if my_xray_obs['Maughan08']:
            ax.errorbar(Mau[Mauind, 3] * 1e14 * 0.7 / h_scale,
                        Mau[Mauind, 13] * 1e13 * 0.7 / h_scale * E(Mau[Mauind, 1])**Epow['Mgas-M'],
                        yerr=[np.absolute(Mau[Mauind, 15] * 1e13 * 0.7 / h_scale * E(Mau[Mauind, 1])**Epow['Mgas-M']),
                              Mau[Mauind, 14] * 1e13 * 0.7 / h_scale * E(Mau[Mauind, 1])**Epow['Mgas-M']],
                        zorder=3, **Maughan_kwargs)
        if my_xray_obs['Bartalucci17']:
            ax.errorbar(Bart[Bartind, 14]*1e14*(0.7/h_scale),
                        Bart[Bartind, 11]*1e13*(0.7/h_scale)*E(Bart[Bartind, 1])**Epow['Mgas-M'],
                        xerr=[np.abs(Bart[Bartind, 16]*1e14*(0.7/h_scale)), Bart[Bartind, 15]*1e14*(0.7/h_scale)],
                        yerr=[np.abs(Bart[Bartind, 13]*1e13*(0.7/h_scale)*E(Bart[Bartind, 1])**Epow['Mgas-M']),
                              Bart[Bartind, 12]*1e13*(0.7/h_scale)*E(Bart[Bartind, 1])**Epow['Mgas-M']],
                        zorder=4, **Bart_kwargs)
        if my_xray_obs['Mahdavi13']:
            ax.errorbar(Mah[Mahind, 11]*1e14*(0.7/h_scale),
                        Mah[Mahind, 14]*1e14*(0.7/h_scale)*E(Mah[Mahind, 1])**Epow['Mgas-M'],
                        xerr=Mah[Mahind, 12]*1e14*(0.7/h_scale),
                        yerr=Mah[Mahind, 15]*1e14*(0.7/h_scale)*E(Mah[Mahind, 1])**Epow['Mgas-M'],
                        zorder=5, **Mah_kwargs)
        if my_xray_obs['Eckert16']:
            ax.errorbar(XXL[XXLind, 8] * 1e14 * 0.7 / h_scale,
                        XXL[XXLind, 11] * 0.7 / h_scale * E(XXL[XXLind, 1])**Epow['Mgas-M'],
                        xerr=[XXL[XXLind, 10] * 1e14 * 0.7 / h_scale,
                              XXL[XXLind, 9] * 1e14 * 0.7 / h_scale],
                              yerr=[np.abs(XXL[XXLind,13])*0.7/h_scale*E(XXL[XXLind,1])**Epow['Mgas-M'],
                                    XXL[XXLind,12]*0.7/h_scale*E(XXL[XXLind,1])**Epow['Mgas-M']],
                        zorder=6, label="Eckert+ 2016"+XXL_label_suffix, **XXL_kwargs)

    # YX-M500 or M500-YX
    if (axes[0] == "mass" and axes[1] in ["YX", "YXmw"] or
            axes[1] == "mass" and axes[0] in ["YX", "YXmw"]):
        if my_xray_obs['Dietrich19'] and z > 0.9:
            Die = np.genfromtxt(obs_dir+"YX-M/Dietrich2019_YX-M.csv",
                                delimiter=",")
            ind = np.where((Die[:, 2] > 0.8) & (Die[:, 2] < 1.2))
            # Not sure if h=0.7 or 0.738 but changing it only shifts
            # the points along the relation slightly.
            M = Die[ind, 5] * 1e14 * 0.7 / h_scale
            YX = Die[ind, 9] * 0.7 / h_scale
            if axes[0] == "mass":
                ax.errorbar(M, YX * E(Die[ind, 2])**Epow['YX-M'],
                            c=c2, mec=c2, mfc='none', marker='H', ls='none',
                            ms=ms, mew=mew, capsize=cap, lw=errlw, zorder=5,
                            label="Dietrich+ 19")
        if my_xray_obs['Maughan08']:
            # YX data with measured masses are given in Maughan+ 2007
            Maughan07 = np.genfromtxt(obs_dir+"YX-M/Maughan2007_table2.csv",
                                      delimiter=",")
            if z > 0.39 and z < 0.41:
                ind = np.where((Maughan07[:, 1] > 0.4) &
                               (Maughan07[:, 1] < 0.47))[0]
            else:
                ind = np.where(Maughan07[:, 1] > minz)[0]
            if len(ind) > 0:
                M = Maughan07[ind, 6] * 1e14 * 0.7 / h_scale
                Merr = Maughan07[ind, 7] * 1e14 * 0.7 / h_scale
                YX = Maughan07[ind, 4] * 1e14 * 0.7 / h_scale
                YXerr = Maughan07[ind, 5] * 1e14 * 0.7 / h_scale
                if axes[0] == "mass":
                    ax.errorbar(M, YX * E(Maughan07[ind, 1])**Epow['YX-M'],
                                xerr=Merr,
                                yerr=YXerr * E(Maughan07[ind, 1])**Epow['YX-M'],
                                zorder=3, **Maughan_kwargs)
                elif axes[1] == "mass":
                    ax.errorbar(YX, M * E(Maughan07[ind, 1])**Epow['M-YX'],
                                xerr=YXerr,
                                yerr=Merr * E(Maughan07[ind, 1])**Epow['M-YX'],
                                zorder=3, **Maughan_kwargs)
        # Can't use Bartalucci17 as their masses are estimated
        # from a local YX-M relation.
        # if my_xray_obs['Bartalucci17']:
            # M = Bart[Bartind, 14]*1e14*(0.7/h_scale)
            # Merr = np.array([np.abs(Bart[Bartind, 16]*1e14*(0.7/h_scale)),
            #                 Bart[Bartind, 15]*1e14*(0.7/h_scale)])
            # YX = Bart[Bartind, 5]*1e14*(0.7/h_scale)
            # YXerr = np.array([np.abs(Bart[Bartind, 7]*1e14*(0.7/h_scale)),
            #                  Bart[Bartind, 6]*1e14*(0.7/h_scale)])
            # if axes[0] == "mass":
            #    ax.errorbar(M, YX*E(Bart[Bartind, 1])**Epow['YX-M'],
            #                xerr=Merr, yerr=YXerr*E(Bart[Bartind, 1])**Epow['YX-M'],
            #                zorder=4, **Bart_kwargs)
            # elif axes[1] == "mass":
            #    ax.errorbar(YX, M*E(Bart[Bartind, 1])**Epow['M-YX'],
            #                xerr=YXerr, yerr=Merr*E(Bart[Bartind, 1])**Epow['M-YX'],
            #                zorder=4, **Bart_kwargs)
        if my_xray_obs['Mantz16']:
            WtG = np.genfromtxt(obs_dir+"Mantz_2016_WtG.csv", delimiter=",")
            # only weak lensing masses
            WtG = WtG[np.isfinite(WtG[:, 10])]
            if z > 0.39 and z < 0.41:
                ind = np.where(WtG[:, 1] > 0.35)[0]  # z=(0.35, 0.45) med~0.4
            else:
                ind = np.where(WtG[:, 1] > minz)[0]
            if label_redshift:
                label = (r"Mantz+ 2016 ($z_{\mathrm{med}}=" +
                         "{:.2f}".format(np.median(WtG[ind, 1]))+"$)")
            else:
                label = "Mantz+ 2016"
            if len(ind) > 0:
                M = WtG[ind, 10] * 1e15 * 0.7/h_scale
                Merr = np.array([np.abs(WtG[ind, 12] * 1e15 * 0.7/h_scale),
                                 WtG[ind, 11] * 1e15 * 0.7/h_scale])
                YX = WtG[ind, 4] * 1e14 * (0.7/h_scale) * WtG[ind, 6]
                YXerr = np.asarray(YX * np.sqrt(
                        (WtG[ind, 5] / WtG[ind, 4])**2 +
                        (WtG[ind, 7] / WtG[ind, 6])**2))
                if axes[0] == "mass":
                    x, y, xerr, yerr = (M, YX * E(WtG[ind, 1])**Epow['YX-M'],
                                        Merr,
                                        YXerr * E(WtG[ind, 1])**Epow['YX-M'])
                elif axes[1] == "mass":
                    x, y, xerr, yerr = (YX, M * E(WtG[ind, 1])**Epow['M-YX'],
                                        YXerr,
                                        Merr * E(WtG[ind, 1])**Epow['M-YX'])
                ax.errorbar(x, y, xerr=xerr, yerr=yerr,
                            c=c2, mec=c2, mfc='none', marker='s', ls='none',
                            ms=ms, mew=mew, capsize=cap, lw=errlw, zorder=5,
                            label=label)

    # SZ-M500
    if axes[0] == "mass" and axes[1] in ["Y500", "Y5r500", "YSPT", "SZ"]:
        from scaling.SZ_scaling2 import plot_SZ_obs
        plot_SZ_obs(ax, z, mew=1.2, ms=ms, lw=lw, compare=SZ_obs,
                    plot_5r500=False if axes[1] == "Y500" else True,
                    h_scale=h_scale, omega_m=omega_m)


def get_sim_relation(name, axnames, X0=None,
                     obs_dir="/data/vault/nh444/ObsData/scal_rel_evol/",
                     h_scale=0.6774):
    """ Get parameters of the best-fitting scaling relation for a simulation.

    Arguments:
        name: the simulation, currently one of ("Truong", "MACSIS").
        axnames: x and y axes, e.g. ["mass", "lum"] for L500-M500 relation.
        X0: Desired pivot point.
            If None then the pivot point is the one used in the fitting.
        obs_dir: path of the parameter data.
        h_scale: value of little h to scale to.
    """
    import os
    import numpy as np

    fname = str(axnames[1]) + "-" + str(axnames[0]) + ".csv"

    if name == "Truong":
        path = obs_dir + "Truong_2018/"
        if "Y500" in axnames:
            label = "Planelles+ 17"
        else:
            label = "Truong+ 18"
    elif name == "MACSIS":
        path = obs_dir + "MACSIS/"
        label = "MACSIS"
    else:
        raise ValueError("name of comparison data unknown: " + str(name))
    flip = False  # whether we need to flip the best-fit relation
    if not os.path.exists(path + fname):
        fname = str(axnames[0]) + "-" + str(axnames[1]) + ".csv"
        flip = True
        if not os.path.exists(path + fname):
            print("Could not find "+str(name)+" data for axnames", axnames)
    if os.path.exists(path + fname):
        dat = np.genfromtxt(path + fname, delimiter=",")
        dat_z = dat[:, 0]
        axname_X = axnames[1 if flip else 0]
        axname_Y = axnames[0 if flip else 1]

        # slope
        if name == "Truong":
            dat_slope = dat[:, 20]
            dat_slope_errlow = np.abs(dat[:, 22])
            dat_slope_errhigh = dat[:, 21]
        elif name == "MACSIS":
            dat_slope = dat[:, 4]
            dat_slope_errlow = np.abs(dat[:, 6])
            dat_slope_errhigh = dat[:, 5]
        if flip:
            dat_slope_errlow = (dat_slope_errlow / dat_slope) / dat_slope
            dat_slope_errhigh = (dat_slope_errhigh / dat_slope) / dat_slope
            dat_slope = 1.0 / dat_slope

        # normalisation - assumes self-similar redshift scaling
        if name == "Truong":
            dat_h = 0.72  # little h assumed in Truong+18 + Planelles+17.
            dat_norm = dat[:, 17]  # logC
            dat_norm_errlow = np.abs(dat[:, 19])
            dat_norm_errhigh = dat[:, 18]
            if axname_Y == "lum":
                # convert 10^44 erg/s to erg/s
                dat_norm += 44.0
            # get pivot point
            if axname_X == "gasmass":
                dat_X0 = 1e13 / h_scale  # <-- 10^13 Msun/h
            elif axname_X in temperatures:
                dat_X0 = 3  # <-- 3 keV
            elif axname_X in ("YX", "YXmw"):
                dat_X0 = 3e13 / h_scale  # <-- 3 x 10^13 Msun/h keV
            elif axname_X == "mass":
                if axname_Y == "Y500":
                    # M0 = 5x10^14 Msun as in Planelles+17.
                    # Rescale M0 to h=h_scale so that, given a mass M with
                    # h=h_scale, M/M0 is the same as if M and M0 had h=0.72
                    dat_X0 = 5e14 * (dat_h / h_scale)
                else:
                    dat_X0 = 1e14 / h_scale  # <-- 10^14 Msun/h
            else:
                raise ValueError("Unknown pivot point for Truong+18 "
                                 "with axes"+str(axname_X))
        elif name == "MACSIS":
            dat_h = 0.6777  # little h they assume.
            dat_norm = dat[:, 1]  # logC
            dat_norm_errlow = np.abs(dat[:, 3])
            dat_norm_errhigh = dat[:, 2]
            # get pivot point
            if axname_X == "mass":
                # Rescale M0 to h=h_scale so that, given a mass M with
                # h=h_scale, M/M0 is the same as if M and M0 had h=0.6777
                dat_X0 = 4e14 * (dat_h / h_scale)  # 4 x 10^14 Msun
            elif axname_X in temperatures:
                dat_X0 = 6.0  # 6 keV
            else:
                raise ValueError("Unknown pivot point for MACSIS "
                                 "with axes"+str(axname_X))
        # Rescale normalisation to little h = h_scale.
        if axname_Y in ("lum", "lum_cex"):
            dat_norm += 2.0 * np.log10(dat_h / h_scale)
        elif axname_Y in ("YX", "YXmw", "mass", "gasmass"):
            dat_norm += np.log10(dat_h / h_scale)
        elif axname_Y == "Y500":
            # Y500 has no h-dependence itself but it is scaled by DA^2,
            # which has h^-2 dependence.
            dat_norm += 2.0 * np.log10(dat_h / h_scale)
        elif axname_Y not in temperatures:
            raise ValueError("H0 scaling for axname_Y " +
                             str(axname_Y) + " is unknown.")

        # Rescale normalisation to the given pivot point X0.
        if X0 is None:
            # The pivot point remains unchanged so that the normalisation
            # corresponds to the pivot point used in the fit.
            print ("WARNING: Pivot point not given for simulation relation. "
                   "Using the pivot point used in the fit.")
            X0 = dat_X0
        else:
            # Get the normalisation at the desired pivot point X0
            # estimate uncertainty on norm incl. average error on the slope
            dat_slope_err = (dat_slope_errlow + dat_slope_errhigh) / 2.0
            if flip:
                # Note that dat_slope has already been inverted.
                norm = np.log10(dat_X0) + dat_slope * (np.log10(X0) - dat_norm)
                norm_errlow = (dat_slope * (np.log10(X0) - dat_norm) *
                               np.sqrt((dat_norm_errlow / dat_norm)**2 +
                                       (dat_slope_err / dat_slope)**2))
                norm_errhigh = (dat_slope * (np.log10(X0) - dat_norm) *
                                np.sqrt((dat_norm_errhigh / dat_norm)**2 +
                                        (dat_slope_err / dat_slope)**2))
            else:
                norm = dat_norm + dat_slope * np.log10(X0 / dat_X0)
                norm_errlow = np.sqrt(dat_norm_errlow**2 +
                                      (dat_slope_err *
                                       np.log10(X0 / dat_X0))**2)
                norm_errhigh = np.sqrt(dat_norm_errhigh**2 +
                                       (dat_slope_err *
                                        np.log10(X0 / dat_X0))**2)
            dat_norm = norm
            dat_norm_errlow = norm_errlow
            dat_norm_errhigh = norm_errhigh

        # scatter
        if name == "Truong":
            # scatter is given for both X and Y
            if flip:
                dat_scat = dat[:, 24]
                dat_scat_errlow = dat[:, 26]
                dat_scat_errhigh = dat[:, 26]
            else:
                dat_scat = dat[:, 23]
                dat_scat_errlow = dat[:, 25]
                dat_scat_errhigh = dat[:, 25]
        elif name == "MACSIS" and not flip:
            # scatter is given for Y only so n/a if flip is True.
            dat_scat = dat[:, 7]
            dat_scat_errlow = np.abs(dat[:, 9])
            dat_scat_errhigh = dat[:, 8]
        else:
            # Set to None to indicate that it should be ignored.
            dat_scat = None
            dat_scat_errlow = None
            dat_scat_errhigh = None

        # Combine results into a dictionary.
        result = {'z': dat_z, 'label': label,
                  'slope': dat_slope,
                  'slope_errlow': dat_slope_errlow,
                  'slope_errhigh': dat_slope_errhigh,
                  'norm': dat_norm, 'norm_errlow': dat_norm_errlow,
                  'norm_errhigh': dat_norm_errhigh,
                  'scat': dat_scat, 'scat_errlow': dat_scat_errlow,
                  'scat_errhigh': dat_scat_errhigh,
                  'X0': X0, 'h_scale': h_scale}
        return result
    else:
        return None


def plot_sims_data(name, ax, axnames, axeslims, z, lw=4,
                   zorder=3, h_scale=0.6774):
    """ Plot best-fit relations from other simulations. """

    import numpy as np

    if name == "Truong":
        c = '#1D91C0'  # colour
    elif name == "MACSIS":
        c = '#ED7600'  # colour

    simdat = get_sim_relation(name, axnames, h_scale=h_scale)

    if simdat is not None:
        zind = np.where(np.abs(simdat['z'] - z) < 0.1)[0]
        dat_slope = simdat['slope'][zind[0]]
        dat_logC = simdat['norm'][zind[0]]

        X1, X2 = axeslims[0][0], axeslims[0][1]
        Y1 = 10**(dat_logC + dat_slope *
                  np.log10(axeslims[0][0] / simdat['X0']))
        Y2 = 10**(dat_logC + dat_slope *
                  np.log10(axeslims[0][1] / simdat['X0']))
        ax.plot([X1, X2], [Y1, Y2], c=c, lw=lw, zorder=zorder,
                label=simdat['label'])


def load_data(infile, axes, axname, axidx, axfac,
              projected=False, h_scale=0.6774):
    """ Load scaling relation data.

    Call the appropriate function to load the data using the filename suffix.
    Args:
        infile: the full path to the file
        projected: Only required for YX parameter. If True, use the
                   projected temperature, otherwise spherical.
    """
    import scaling.SZ_scaling2 as SZ
    import os
    filename = os.path.split(infile)[1]

    if filename[:3] == "L-T" and filename[-5:] == ".hdf5":
        # Xray data in hdf5 format
        X, Y, M500, grps, metadata = load_xray_hdf5(
            infile, axes, axname, axfac, projected=projected, h_scale=h_scale)
    elif (filename[:2] in ["Y500", "Y5r500", "YSPT", "SZ"] and
          filename[-5:] == ".hdf5"):
        # SZ data in hdf5 format
        X, Y, M500, grps, metadata = SZ.load_SZ_hdf5(infile, h_scale=h_scale)
    elif filename[:3] == "L-T" and filename[-4:] == ".txt":
        # X-ray data in txt
        X, Y, M500, grps = load_xray_txt(infile, axidx, axfac, h_scale=h_scale)
        metadata = {}
    elif (filename[:2] in ["Y500", "Y5r500", "YSPT", "SZ"] and
          filename[-4:] == ".txt"):
        # SZ data in txt
        X, Y, M500, grps = SZ.load_SZ_txt(
            infile, h_scale=h_scale)
        metadata = {}
    else:
        raise ValueError("filetype not recognised.")

    return X, Y, M500, grps, metadata


def load_xray_hdf5(infile, axes, axname, axfac,
                   projected=False, h_scale=0.6774):
    """
    Load the X-ray data in hdf5 format output by get_L-T.py in xray_python
    Args:
        projected: Only required for YX parameter. If True, use the
                   projected temperature, otherwise spherical.
    """
    assert infile[-5:] == ".hdf5", "file must be .hdf5"
    import h5py
    from copy import deepcopy

    with h5py.File(infile) as f:
        M500 = f['M500'].value[:, 0] * 1e10 / h_scale
        grps = f['groups'].value[:, 0]
        metadata = deepcopy(dict(f.attrs))
        # Calculate YX from gas mass and core-excised temperature.
        if "YX" in axes or "YXmw" in axes:
            if projected:
                prefix = "projected"
            else:
                prefix = "spherical"
            if prefix+"_cex" not in f.keys():
                raise KeyError("The following file does not have " +
                               prefix+"_cex:"+str(infile))
            if axes[0] == "YX":
                X = (f[prefix+"_cex/temp_spec"].value[:, 0] *
                     f["Mgas"].value[:, 0] * axfac[0])
                Y = f[axname[1]].value[:, 0] * axfac[1]
            elif axes[1] == "YX":
                X = f[axname[0]].value[:, 0] * axfac[0]
                Y = (f[prefix+"_cex/temp_spec"].value[:, 0] *
                     f["Mgas"].value[:, 0] * axfac[1])
            elif axes[0] == "YXmw":
                X = (f[prefix+"_cex/temp_mw"].value[:, 0] *
                     f["Mgas"].value[:, 0] * axfac[0])
                Y = f[axname[1]].value[:, 0] * axfac[1]
            elif axes[1] == "YXmw":
                X = f[axname[0]].value[:, 0] * axfac[0]
                Y = (f[prefix+"_cex/temp_mw"].value[:, 0] *
                     f["Mgas"].value[:, 0] * axfac[1])
        else:
            try:
                X = f[axname[0]].value[:, 0] * axfac[0]
            except KeyError:
                raise KeyError("The following file does not contain an " +
                               "object named " + str(axname[0]) +
                               ":\n"+str(infile))
            try:
                Y = f[axname[1]].value[:, 0] * axfac[1]
            except KeyError:
                raise KeyError("The following file does not contain an " +
                               "object named " + str(axname[1]) +
                               ":\n"+str(infile))
        # Scale luminosity to h_scale since it's not h-less
        if axes[0] in luminosities:
            X *= f.attrs['hubble']**2.0 / h_scale**2.0
        if axes[1] in luminosities:
            Y *= f.attrs['hubble']**2.0 / h_scale**2.0

    return X, Y, M500, grps.astype(int), metadata


def load_xray_txt(infile, axidx, axfac, h_scale=0.6774):
    import numpy as np

    data = np.loadtxt(infile)
    M500 = data[:, 12] * 1e10 / h_scale
    grps = data[:, 0]
    X = data[:, axidx[0]]*axfac[0]
    Y = data[:, axidx[1]] * axfac[1]

    return X, Y, M500, grps.astype(int)


def get_median_in_bins(X, Y, M500, axes, bin_ax, binwidth=0.1, M500min=1e12):
    """Return the median relation in bins.

    Arguments:
        M500min: Lower bin edge of lowest mass bin in Msun.
    """
    import numpy as np
    from scipy.stats import binned_statistic

    assert bin_ax in [0, 1], "bin_ax must be either 0 or 1"
    dat = (X, Y)
    if axes[bin_ax] == "mass":
        xmin, xmax = [M500min, 1e16]
    elif axes[bin_ax] in temperatures:
        # Use the (second) lowest temperature in the sample for the lowest bin.
        # (Often the lowest temperature is anomalous).
        xmin, xmax = [np.sort(dat[bin_ax])[1], 100]
    else:
        raise ValueError(
            "Unrecognised axes for binning: "+str(axes[bin_ax]))
    # Get number of bins rounded up.
    num = int((np.log10(xmax) - np.log10(xmin)) / binwidth) + 1
    # Ensure that the binwidth is exact by extending the upper limit to be
    # the lower limit plus a multiple of the bin width.
    bins = np.logspace(np.log10(xmin), np.log10(xmin) + (binwidth * num),
                       num=num+1)
    if bin_ax == 0:
        med, _, _ = binned_statistic(X, Y, statistic='median', bins=bins)
    elif bin_ax == 1:
        med, _, _ = binned_statistic(Y, X, statistic='median', bins=bins)

    return bins, med


def get_fit_params(axes):
    """Get parameters for a power law fit.

    Arguments:
        axes: List of axes names.
    Returns:
        X0: Pivot point for fit.
        inislope: Initial guess for slope.
        inilogC: Initial guess for normalisation.
    """
    inislope = None  # initial guess for slope
    inilogC = None  # intitial guess for normalisation
    if axes[0] in temperatures:
        X0 = 3.0  # Temperature pivot point in keV.
        if axes[1] in luminosities:
            inislope = 2.0
            inilogC = 44.0
        elif axes[1] == "mass":
            inislope = 3.0/2.0
            inilogC = 14.0
        elif axes[1] in temperatures:
            inislope = 1.0
            inilogC = X0
    elif axes[0] == "mass":
        X0 = 2e14
        if axes[1] in luminosities:
            inislope = 4.0/3.0
            inilogC = 44.0
        elif axes[1] == "gasmass":
            inislope = 1.0
            inilogC = 13.0
        elif axes[1] in ["YX", "YXmw"]:
            inislope = 5.0/3.0
            inilogC = 13.3
        elif axes[1] in ["Y500", "Y5r500", "YSPT", "SZ"]:
            inislope = 5.0/3.0
            inilogC = -5.5
        elif axes[1] in temperatures:
            inislope = 2.0/3.0
            inilogC = 0.1
        elif axes[1] == "abund":
            inislope = 0.0
            inilogC = 0.1
    elif axes[0] in ["YX", "YXmw"]:
        X0 = 1e13
        if axes[1] == "mass":
            inislope = 3.0/5.0
            inilogC = 14.0
    elif axes[0] == "gasmass":
        X0 = 2e13
        if axes[1] in luminosities:
            inislope = 4.0/3.0
            inilogC = 44.0
        elif axes[1] == "mass":
            inislope = 1.0
            inilogC = 14.0
    elif axes[0] in luminosities:
        X0 = 1e44
        if axes[1] == "mass":
            inislope = 3.0/4.0
            inilogC = 14.0
    else:
        raise NotImplementedError(
            "default fit parameters unknown for axes[0]="+str(axes[0]))
        raise NotImplementedError("default fit parameters unknown "
                                  "for axes[0]="+str(axes[0]))
    if inislope is None or inilogC is None:
        raise NotImplementedError("default fit parameters unknown "
                                  "for axes[1]="+str(axes[1]))

    return X0, inislope, inilogC


def get_axparams(axes, axeslims, axlabels, projected=True, Epow={},
                 h_scale=0.6774):
    """
    Get the parameters of the axes e.g. labels.
    Args:
        axes: a list of two strings defining the x and y axes.
              These can include:
                  "mass": Total mass M500.
                  "gasmass": Gas mass within r500.
                  "T500": spectroscopic temperature within r500.
                  "T300kpc": spectroscopic temperature within 300 kpc.
                  "T500mw": mass-weighted temperature within r500.
                  "T500ew": emission-weighted temperature within r500.
                  "T500sl": spectroscopic-like temperature within r500.
                  "T500cex": core-excised spectroscopic temperature.
                  "T500mwcex": core-excised mass-weighted temperature.
                  "lum": spectroscopic X-ray luminosity
                  "lum_cex": core-excised spectroscopic X-ray luminosity
                  "lum_brem": bremsstrahlung X-ray luminosity
                  "lum_tab": tabulated X-ray luminosity
        axeslims: limits of axes (list of 2-lists).
        axlabels: list of axis labels, set to [None,None] to use default labels
        projected: if true, use values in the projected aperture, otherwise 3D.
        L_power: luminosity is divided by E(z) to this power.
        M_power: mass (total and gas) is multiplied by E(z) to this power.
    Returns:
        axeslims: axes limits. list of 2-lists.
        axlabels: 2-list of axes labels.
        axname: 2-list of hdf5 dataset names.
        axidx: 2-list of column indices for txt files.
        axfac: 2-list of factors to multiply quantity.
        Ezpows: 2-list of powers of E(z) to scale each quantity.
    """
    # def E(z): return (omega_m*(1+z)**3 + omega_l)**0.5

    # name of hdf5 dataset for the 2 properties given in axes argument
    axname = [None, None]
    # column index in .txt file for the 2 properties specified in axes argument
    axidx = [None, None]
    axfac = [1.0, 1.0]  # factors to multiply quantity from file
    Ezpows = [0.0, 0.0]

    assert len(axes) == 2, "axes must be list of length 2"

    for i in range(2):
        if axes[i] in temperatures:
            # Spectroscopic temperature.
            if axes[i] in ("T500", "T300kpc"):
                if projected:
                    axname[i] = "projected/temp_spec"
                    axidx[i] = 5
                else:
                    axname[i] = "spherical/temp_spec"
                    axidx[i] = 22
            # Core-excised spectroscopic temperature.
            elif axes[i] == "T500cex":
                if projected:
                    axname[i] = "projected_cex/temp_spec"
                else:
                    axname[i] = "spherical_cex/temp_spec"
            # For mass- and emission-weighted temperature we'll always use
            # the spherical average, not projected.
            elif axes[i] == "T500mw":
                axname[i] = "spherical/temp_mw"
            elif axes[i] == "T500mwcex":
                axname[i] = "spherical_cex/temp_mw"
            elif axes[i] == "T500ew":
                axname[i] = "spherical/temp_ew"
            elif axes[i] == "T500sl":
                # Similarly for the spectroscopic-like temperature
                axname[i] = "spherical/temp_sl"
            axfac[i] = 1.0
            if axeslims[i] is None:
                if axes[i] in ["T500", "T300kpc", "T500cex"]:
                    axeslims[i] = [0.5, 10.0]
                elif axes[i] in ["T500mw", "T500ew", "T500sl", "T500mwcex"]:
                    axeslims[i] = [0.5, 10.0]
            if axlabels[i] is None:
                if axes[i] == "T500":
                    axlabels[i] = r"T$_{500}$ [keV]"
                elif axes[i] == "T300kpc":
                    axlabels[i] = r"T$_{300 \mathrm{kpc}}$ [keV]"
                elif axes[i] == "T500mw":
                    axlabels[i] = r"T$_{\mathrm{500, mw}}$ [keV]"
                elif axes[i] == "T500ew":
                    axlabels[i] = r"T$_{\mathrm{500, ew}}$ [keV]"
                elif axes[i] == "T500sl":
                    axlabels[i] = r"T$_{\mathrm{500, sl}}$ [keV]"
                elif axes[i] == "T500cex":
                    axlabels[i] = r"T$_{\rm 500, cex}$ [keV]"
                elif axes[i] == "T500mwcex":
                    axlabels[i] = r"T$_{\rm 500, mw, cex}$ [keV]"
        elif axes[i] in luminosities:
            if projected:
                if axes[i] == "lum":
                    axname[i] = "projected/luminosity"
                # Core-excised luminosity.
                elif axes[i] == "lum_cex":
                    axname[i] = "projected_cex/luminosity"
                elif axes[i] == "lum_brem":
                    axname[i] = "projected/lum_brem"
                elif axes[i] == "lum_tab":
                    axname[i] = "projected/lum_tab"
                else:
                    raise ValueError("HDF5 dataset name unknown for axes " +
                                     str(axes[i]))
                axidx[i] = 25
            else:
                if axes[i] == "lum":
                    axname[i] = "spherical/luminosity"
                # Core-excised luminosity (projected only).
                elif axes[i] == "lum_cex":
                    axname[i] = "spherical_cex/luminosity"
                elif axes[i] == "lum_brem":
                    axname[i] = "spherical/lum_brem"
                elif axes[i] == "lum_tab":
                    axname[i] = "spherical/lum_tab"
                else:
                    raise ValueError("HDF5 dataset name unknown for axes " +
                                     str(axes[i]))
                axidx[i] = 1
            if i == 1:  # luminosity is dependent variable
                if axes[0] == "mass":
                    Ezpows[i] = Epow['L-M']
                elif axes[0] == "gasmass":
                    Ezpows[i] = Epow['L-Mgas']
                elif axes[0] in temperatures:
                    Ezpows[i] = Epow['L-T']
                else:
                    raise ValueError(
                        "Multiplication factor for luminosity vs " +
                        str(axes[0])+" is unknown.")
            if axeslims[i] is None:
                axeslims[i] = [3e41, 1e46]
            if axlabels[i] is None:
                if axes[i] == "lum":
                    axlabels[i] = r"L$_{500}^{\mathrm{bol}}$ [erg s$^{-1}$]"
                elif axes[i] == "lum_cex":
                    axlabels[i] = (r"L$_{\rm 500, ce}^{\mathrm{bol}}$ "
                                   "[erg s$^{-1}$]")
                elif axes[i] == "lum_brem":
                    axlabels[i] = r"L$_{\mathrm{500, brem}}$ [erg s$^{-1}$]"
                elif axes[i] == "lum_tab":
                    axlabels[i] = (r"L$_{\mathrm{500, tab}}^{\mathrm{bol}}$ "
                                   "[erg s$^{-1}$]")
        elif axes[i] == "mass":
            axname[i] = "M_delta"
            axidx[i] = 12
            axfac[i] = 1e10 / h_scale
            if i == 1:  # dependent variable - SS scaling
                if axes[0] in temperatures:
                    Ezpows[i] = Epow['M-T']
                elif axes[0] in ["YX", "YXmw"]:
                    Ezpows[i] = Epow['M-YX']
                else:
                    raise ValueError(
                        "Multiplication factor for mass vs " +
                        str(axes[0])+" is unknown.")
            if axeslims[i] is None:
                axeslims[i] = [1e13, 3e15]
            if axlabels[i] is None:
                axlabels[i] = r"M$_{500}$ [M$_{\odot}$]"
        elif axes[i] == "gasmass":
            axname[i] = "Mgas"
            axidx[i] = 13
            axfac[i] = 1e10 / h_scale
            if i == 1:  # dependent variable - SS scaling
                if axes[0] == "mass":
                    Ezpows[i] = Epow['Mgas-M']
                elif axes[0] in temperatures:
                    Ezpows[i] = Epow['Mgas-T']
                else:
                    raise ValueError(
                        "Multiplication factor for gasmass vs " +
                        str(axes[0])+" is unknown.")
            if axeslims[i] is None:
                axeslims[i] = [5e11, 3e14]
            if axlabels[i] is None:
                axlabels[i] = r"M$_{\mathrm{gas},500}$ [M$_{\odot}$]"
        elif axes[i] in ["YX", "YXmw"]:
            axidx[i] = 22
            axfac[i] = 1e10 / h_scale
            if i == 1:  # dependent variable - SS scaling
                if axes[0] == "mass":
                    Ezpows[i] = Epow['YX-M']
                else:
                    raise ValueError(
                        "Multiplication factor for YX vs " +
                        str(axes[0])+" is unknown.")
            if axeslims[i] is None:
                axeslims[i] = [3e11, 5e15]
            if axlabels[i] is None:
                axlabels[i] = r"Y$_{\mathrm{X},500}$ [M$_{\odot}$ keV]"
        elif axes[i] in ["Y500", "Y5r500", "YSPT", "SZ"]:
            if i == 1:  # dependent variable - SS scaling
                if axes[0] == "mass":
                    Ezpows[i] = Epow['Y-M']
                else:
                    raise ValueError(
                        "Multiplication factor for " + str(axes[i]) + " vs " +
                        str(axes[0])+" is unknown.")
            if axeslims[i] is None:
                if axes[i] == "Y500":
                    axeslims[i] = [5e-8, 1e-3]
                elif axes[i] == "Y5r500":
                    axeslims[i] = [5e-8, 1e-3]
                elif axes[i] == "YSPT":
                    axeslims[i] = [1e-8, 1e-4]
            if axlabels[i] is None:
                if axes[i] == "Y500":
                    axlabels[i] = (r"$\mathrm{E(z)^{-2/3} Y_{500} D_A^2}$ "
                                   "[Mpc$^2$]")
                elif axes[i] == "Y5r500":
                    axlabels[i] = (r"$\mathrm{E(z)^{-2/3} Y_{5r_{500}} D_A^2}$"
                                   " [Mpc$^2$]")
                elif axes[i] == "YSPT":
                    axlabels[i] = (r"$\mathrm{E(z)^{-2/3} Y^{0'.75} D_A^2}$ "
                                   "[Mpc$^2$]")
                else:
                    axlabels[i] = r"$\mathrm{E(z)^{-2/3} Y}$"
        elif axes[i] == "abund":
            if projected:
                axname[i] = "projected/abundance"
            else:
                axname[i] = "spherical/abundance"
            axfac[i] = 1.0
            if axlabels[i] is None:
                axlabels[i] = r"Abundance"
        else:
            raise ValueError("axes["+str(i)+"] = "+axes[i]+" not recognised.")

    return axeslims, axlabels, axname, axidx, axfac, Ezpows


def get_Epow():
    # for each relation define the power of E(z) multiplying the y-axis to
    # take out the self-similar evolution.
    # e.g. the self-similar relation between luminosity and temperature is
    # L \propto E(z) T^2
    # so Epow['L-T'] is -1.0 (the negative of the exponent in the equation.)
    Epow = {'M-T': 1.0, 'L-M': -7.0/3.0, 'L-Mgas': -7.0/3.0,
            'L-T': -1.0, 'YX-M': -2.0/3.0, 'M-YX': 2.0/5.0, 'Y-M': -2.0/3.0,
            'Mgas-M': 0.0, 'M-Mgas': 0.0, 'Mgas-T': 0.0}
    return Epow


if __name__ == "__main__":
    main()
