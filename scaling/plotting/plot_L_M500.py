
def main():
    outdir = "/home/nh444/data/project1/L-T_relations/"
    basedir="/home/nh444/data/project1/L-T_relations/"
    simdirs = [#"L40_512_LDRIntRadioEff_noBH",
               #"L40_512_NoDutyRadioWeak",
               #"L40_512_NoDutyRadioInt",
               #"L40_512_DutyRadioWeak",
               "L40_512_LDRIntRadioEffBHVel",
               "L40_512_LDRHighRadioEff",
               "L40_512_MDRIntRadioEff",
               ]
    labels = [#"No BHs",
              #"Weak radio",
              #"Stronger radio",
              #"Quasar duty cycle",
              "Fiducial",#"Combined",
              "Longer radio duty",
              "Longer quasar duty",
              ]
    filename = "L-T_data_apec_0.5-10_default_Tvir_thresh_4.0.txt"
    snapnum = 25
    h = 0.679
    zoomdirs = ["/data/curie4/nh444/project1/L-T_relations/LDRIntRadioEffBHVel/"]
    zooms = []#["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c256_box_Arepo_new", "c320_box_Arepo_new", "c384_box_Arepo_new"]
    idxScat = range(len(simdirs))
    idxMed = []#range(len(simdirs))
    
    figsize=(7,7)
    col = ["#c7673e","#78a44f","#6b87c8","#934fce","#e6ab02","#be558f","#47af8d"]
    ls=['solid']*len(simdirs)
    markers = ["D", "s", "^", "v", "o", "o", "o"]
    ms=6.5
    lw=3
    mew=1.2
    binwidth = 0.2 ## dex for mass in Msun
    
    outname = "L-M500_relation_models.pdf"
    plot_L_M500(simdirs, snapnum, filename, labels=labels, zoomdirs=zoomdirs, zooms=zooms, h=h, basedir=basedir, outname=outname, outdir=outdir, figsize=figsize, idxScat=idxScat, idxMed=idxMed, col=col, markers=markers, ls=ls, ms=ms, lw=lw, mew=mew, binwidth=binwidth)

def plot_L_M500(simdirs, snapnum, filename, labels=None, zoomdirs=None, zooms=[],
                projected=True, h=0.679, basedir="/home/nh444/data/project1/L-T_relations/",
                outname="L-M500_relation.pdf", outdir="/home/nh444/data/project1/L-T_relations/",
                figsize=(7,7), idxScat=[], idxMed=[], col=None, markers=None, hlightzooms=False,
                ls=None, ms=6.5, lw=2, mew=1.2, binwidth=0.2, blkwht=False):
    import matplotlib.pyplot as plt
    import numpy as np
    import scipy.stats as stats
    
    assert len(simdirs) > 0, "simdirs is an empty list"
    
    if projected:
        lum_idx = 25 ## 1 for 3D luminosity, 25 for projected
    else:
        lum_idx = 1
    
    if col is None:
        from palettable.colorbrewer.qualitative import Dark2_8 as pallette
        col=pallette.hex_colors
    if markers is None:
        markers = ["D", "s", "o"]
    if ls is None:
        ls=['dashdot', 'dotted', 'dashed', 'solid']
    if zoomdirs is None:
        zoomdirs = [""]*len(simdirs)
    
    fig = plt.figure(figsize=figsize)
    #### OBERVATIONS ####
    #c = '0.7'
    #c2 = '0.5'
    from scaling.plotting.plotObsDat import L_M500
    L_M500(plt.gca(), h=h, ms=ms)
    
    ## Median profiles
    xmin, xmax = [11, 13.5]
    num = int((xmax - xmin) / binwidth)
    bins = np.logspace(xmin, xmax, num=num)
    x = 10**((np.log10(bins[:-1]) + np.log10(bins[1:]))/2)
    
    for simidx, simdir in enumerate(simdirs):
        data = np.loadtxt(basedir+simdirs[simidx]+"_"+str(snapnum).zfill(3)+"/"+filename)
        label = labels[simidx]
        
        if zoomdirs[simidx] is not "":
            for zoom in zooms[simidx]:
                zdata = np.loadtxt(zoomdirs[simidx]+zoom+"_"+str(snapnum).zfill(3)+"/"+filename)
                data = np.vstack((data, zdata))
                if hlightzooms:
                    plt.plot(zdata[12] * 1e10 / h, zdata[lum_idx], marker=markers[simidx], mfc=col[simidx], mec=col[simidx], ms=ms, ls='none', mew=mew)
        if simidx in idxMed:
            med, bin_edges, n = stats.binned_statistic(data[:,12] * 1e10 / h, data[:,lum_idx], statistic='median', bins=bins)
            plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[simidx], ls=ls[simidx], lw=lw, dash_capstyle='round', label=label)
            label = None ## don't want two legend entries
        if simidx in idxScat:
            plt.plot(data[:,12] * 1e10 / h, data[:,lum_idx], marker=markers[simidx], mfc='none', mec=col[simidx], ms=ms, ls='none', mew=mew, label=label)
    
    Mmin, Mmax = 3e12, 2e15
    plt.xlim(Mmin, Mmax)
    plt.ylim(1e41,1e46)
    plt.xscale('log')
    plt.yscale('log')
    plt.ylabel("L$_{500}^{\mathrm{bol}}$ E(z)$^{-1}$ [erg s$^{-1}$]")
    plt.xlabel("M$_{500}$ E(z) [$M_{\odot}$]")#, labelpad=10)#, fontsize=16)
    plt.gca().tick_params(axis='x', which='major', pad=7)
    
    plt.tight_layout(pad=0.45)
    from scaling.plotting.plotObsDat import add_legends
    if blkwht:
        add_legends(plt.gca(), simdirs, labels, textcol='white')
        fig.patch.set_facecolor('0.2')
        ax = plt.gca()
        ax.set_axis_bgcolor('0.2')
        ax.xaxis.label.set_color('white')
        ax.tick_params(axis='x', which='both', colors='white')
        ax.yaxis.label.set_color('white')
        ax.tick_params(axis='y', which='both', colors='white')
        import matplotlib.spines
        for child in ax.get_children():
            if isinstance(child, matplotlib.spines.Spine):
                child.set_color('white')
        fig.savefig(outdir+outname, bbox_inches='tight', transparent=True)
    else:
        add_legends(plt.gca(), simdirs, labels)
        fig.savefig(outdir+outname)#, bbox_inches='tight')
    print "Saved to", outdir+outname

if __name__ == "__main__":
    main()