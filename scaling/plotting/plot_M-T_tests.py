import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np
import scipy.stats as stats
from os import chdir
chdir('/home/nh444/data/project1/L-T_relations')

outname = "M-T_relation.pdf"
obs_dir = "/home/nh444/vault/ObsData/M-T/"

snapnum = 25
h = 0.679 ## h value to scale to
filename = "L-T_data_apec_0.5-2_Rasia_c4.txt"
filename = "L-T_data_apec_0.5-10_Rasia_c4_Tvir_thresh_2.5.txt"
filename = "L-T_data_apec_0.5-10_Rasia_1e7_Tvir_thresh_3.0.txt"

fig = plt.figure(figsize=(10,10))
plt.xscale('log')
Tmin, Tmax = 0.3, 10.0
plt.xlim(Tmin, Tmax)
plt.ylim(5e12, 4e15)
plt.yscale('log')
plt.xlabel(r"T$_{500}$ [keV]", fontsize=22)
plt.ylabel("M$_{500}$ E(z) [$M_{\odot}$]", fontsize=22)

labels = ["Ref",
          "Stronger radio",
          "Quasar duty cycle",
          "Best"]

from palettable.colorbrewer.qualitative import Dark2_8 as pallette
col=pallette.hex_colors
markers = ["D", "s", "o"]
ls=['dashdot', 'dotted', 'dashed', 'solid']
#ls=['solid']*4
ms = 6.5
lw=3
mew=1.2
temp_idx = 5 ## temperature index - 5 for spectral, 15 for emission-weighted, 14 for mass-weighted
## Median profiles
bins = np.logspace(-1.2, 1.0, num=22)
#bins = np.logspace(np.log10(0.16), np.log10(1.7), num=10)
x = 10**((np.log10(bins[:-1]) + np.log10(bins[1:]))/2)

#data = np.loadtxt("L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_"+str(snapnum).zfill(3)+"/"+filename)
#plt.plot(data[:,temp_idx], data[:,12] * 1e10 / h, marker=markers[2], mfc='none', mec=col[3], ms=ms, ls='none', mew=1.2, zorder=4)

## median is plotted last
#==============================================================================
# for zoom in ["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c256_box_Arepo_new", "c384_box_Arepo_new"]:
#     data = np.loadtxt("/home/nh444/data/project1/L-T_relations/LDRIntRadioEffBHVel/"+zoom+"_"+str(snapnum).zfill(3)+"/"+filename)
#     plt.plot(data[temp_idx], data[12] * 1e10 / h, marker='o', mfc='none', mec=col[3], mew=1.2, ms=ms, ls='none', zorder=4)
#==============================================================================
    
c = ["#c7673e","#78a44f","#6b87c8","#934fce","#e6ab02","#be558f","#47af8d"]
for i, zoom in enumerate(["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c256_box_Arepo_new", "c320_box_Arepo_new", "c384_box_Arepo_new"]):
    data = np.loadtxt("/home/nh444/data/project1/L-T_relations/LDRIntRadioEffBHVel/"+zoom+"_"+str(snapnum).zfill(3)+"/"+filename)
    plt.plot(data[temp_idx], data[12] * 1e10 / h, marker='D', mfc=c[i], mec=c[i], mew=0.5, ms=8, ls='none', zorder=4, label=zoom.replace("_", "\_"))
for i, zoom in enumerate(["c96_box_Arepo_new_LowSoft","c128_box_Arepo_new_LowSoft","c160_box_Arepo_new_LowSoft","c256_box_Arepo_new_LowSoft","c320_box_Arepo_new_LowSoft","c384_box_Arepo_new_LowSoft"]):
    data = np.loadtxt("/home/nh444/data/project1/L-T_relations/LDRIntRadioEffBHVel/"+zoom+"_"+str(snapnum).zfill(3)+"/"+filename)
    plt.plot(data[temp_idx], data[12] * 1e10 / h, marker='D', mfc='none', mec=c[i], mew=2.0, ms=8, ls='none', zorder=4, label=zoom.replace("_", "\_"))

#==============================================================================
# ## Core-excised temperature
# data = np.loadtxt("L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_0.5-10_Rasia_c4_Tvir_thresh_2.5_core_ex.txt")
# plt.plot(data[:,temp_idx], data[:,12] * 1e10 / h, marker='D', mfc='none', mec='purple', ms=ms, ls='none', mew=1.2, zorder=4)
# ## median is plotted last
# for zoom in ["c96", "c128_fixed", "c160", "c192", "c256_fixed", "c320", "c384_fixed", "c448"]:
#     data = np.loadtxt("/home/nh444/data/project1/L-T_relations/LDRIntRadioEffBHVel/"+zoom+"_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_0.5-10_Rasia_c4_Tvir_thresh_2.5_core_ex.txt")
#     plt.plot(data[temp_idx], data[12] * 1e10 / h, marker='D', mfc='none', mec='purple', mew=1.2, ms=ms, ls='none', zorder=4)
#==============================================================================
data = np.loadtxt("L40_512_NoDutyRadioWeak_HalfThermal_FixEta_"+str(snapnum).zfill(3)+"/"+filename)
#plt.plot(data[:,temp_idx], data[:,12] * 1e10 / h, marker=markers[0], mec='none', mfc=col[0], ms=ms, ls='none', mew=1.0, zorder=4)
med, bin_edges, n = stats.binned_statistic(data[:,temp_idx], data[:,12] * 1e10 / h, statistic='median', bins=bins)
plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[0], ls=ls[0], lw=1.5, dash_capstyle='round', label="Ref", zorder=4)

data = np.loadtxt("L40_512_NoDutyRadioInt_HalfThermal_FixEta_"+str(snapnum).zfill(3)+"/"+filename)
#plt.plot(data[:,temp_idx], data[:,12] * 1e10 / h, marker=markers[0], mec='none', mfc=col[1], ms=ms, ls='none', mew=1.0, zorder=4)
med, bin_edges, n = stats.binned_statistic(data[:,temp_idx], data[:,12] * 1e10 / h, statistic='median', bins=bins)
plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[1], ls=ls[1], lw=1.5, dash_capstyle='round', label="Stronger radio", zorder=4)

data = np.loadtxt("L40_512_LowDutyRadioWeak_HalfThermal_FixEta_"+str(snapnum).zfill(3)+"/"+filename)
#plt.plot(data[:,temp_idx], data[:,12] * 1e10 / h, marker=markers[1], mec='none', mfc=col[2], ms=ms, ls='none', mew=1.0, zorder=4)
med, bin_edges, n = stats.binned_statistic(data[:,temp_idx], data[:,12] * 1e10 / h, statistic='median', bins=bins)
plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[2], ls=ls[2], lw=1.5, dash_capstyle='round', label="Quasar duty cycle", zorder=4)

data = np.loadtxt("L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_"+str(snapnum).zfill(3)+"/"+filename)
med, bin_edges, n = stats.binned_statistic(data[:,temp_idx], data[:,12] * 1e10 / h, statistic='median', bins=bins)
plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[3], ls=ls[3], lw=1.5, dash_capstyle='round', label="Best", zorder=4)#(APEC $T_{min}=0.01$ keV)

#==============================================================================
# ## Core-excised temperature
# data = np.loadtxt("L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_0.5-10_Rasia_c4_Tvir_thresh_2.5_core_ex.txt")
# med, bin_edges, n = stats.binned_statistic(data[:,temp_idx], data[:,12] * 1e10 / h, statistic='median', bins=bins)
# plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c='purple', ls=ls[3], lw=1.5, dash_capstyle='round', label="Best ($r > 0.15 r_{500}$)", zorder=4)#(APEC $T_{min}=0.01$ keV)
# 
#==============================================================================

#==============================================================================
# ## no cuts or T thresh
# data = np.loadtxt("L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_0.5-10_none.txt")
# plt.plot(data[:,temp_idx], data[:,12] * 1e10 / h, marker=markers[2], mfc='none', mec='k', ms=ms, ls='none', mew=1.2, zorder=4)
# med, bin_edges, n = stats.binned_statistic(data[:,temp_idx], data[:,12] * 1e10 / h, statistic='median', bins=bins)
# plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c='k', ls=ls[3], lw=1.5, dash_capstyle='round', label="Best (no cuts or temp. thresh.)", zorder=4)#(APEC $T_{min}=0.01$ keV)
# #for zoom in ["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c256_box_Arepo_new", "c384_box_Arepo_new"]:
# #    data = np.loadtxt("/home/nh444/data/project1/L-T_relations/LDRIntRadioEffBHVel/"+zoom+"_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_0.5-10_none.txt")
# #    plt.plot(data[temp_idx], data[12] * 1e10 / h, marker='o', mfc='none', mec='k', mew=1.2, ms=ms, ls='none', zorder=4)
#    
# data = np.loadtxt("L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_0.5-10_none_Tvir_thresh_2.5.txt")
# plt.plot(data[:,temp_idx], data[:,12] * 1e10 / h, marker=markers[2], mfc='none', mec='b', alpha=0.5, ms=ms, ls='none', mew=1.2, zorder=4)
# med, bin_edges, n = stats.binned_statistic(data[:,temp_idx], data[:,12] * 1e10 / h, statistic='median', bins=bins)
# plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c='b', alpha=0.5, ls=ls[3], lw=1.5, dash_capstyle='round', label="Best (no cuts + temp. thresh.)", zorder=4)#(APEC $T_{min}=0.01$ keV)
# #for zoom in ["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c256_box_Arepo_new", "c384_box_Arepo_new"]:
# #    data = np.loadtxt("/home/nh444/data/project1/L-T_relations/LDRIntRadioEffBHVel/"+zoom+"_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_0.5-10_none_Tvir_thresh_2.5.txt")
# #    plt.plot(data[temp_idx], data[12] * 1e10 / h, marker='o', mfc='none', mec='k', mew=1.2, ms=ms, ls='none', zorder=4)
# 
# data = np.loadtxt("L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_0.5-10_none_core_ex.txt")
# plt.plot(data[:,temp_idx], data[:,12] * 1e10 / h, marker=markers[2], mfc='none', mec='g', alpha=0.5, ms=ms, ls='none', mew=1.2, zorder=4)
# med, bin_edges, n = stats.binned_statistic(data[:,temp_idx], data[:,12] * 1e10 / h, statistic='median', bins=bins)
# plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c='g', alpha=0.8, ls=ls[3], lw=1.5, dash_capstyle='round', label="Best (no cuts or temp. thresh. core excised)", zorder=4)#(APEC $T_{min}=0.01$ keV)
# 
#==============================================================================

#==============================================================================
#####  T vir thresh tests  #####
# ## no T thresh
# data = np.loadtxt("L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_0.5-10_Rasia_1e7.txt")
# plt.plot(data[:,temp_idx], data[:,12] * 1e10 / h, marker=markers[2], mfc='none', mec='r', ms=ms, ls='none', mew=1.2, zorder=4)
# med, bin_edges, n = stats.binned_statistic(data[:,temp_idx], data[:,12] * 1e10 / h, statistic='median', bins=bins)
# plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c='r', ls=ls[3], lw=1.5, dash_capstyle='round', label="Best ($N=10^7$ no temp. thresh.)", zorder=4)
# for zoom in ["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c256_box_Arepo_new", "c320_box_Arepo_new", "c384_box_Arepo_new"]:
#     data = np.loadtxt("/home/nh444/data/project1/L-T_relations/LDRIntRadioEffBHVel/"+zoom+"_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_0.5-10_Rasia_1e7.txt")
#     plt.plot(data[temp_idx], data[12] * 1e10 / h, marker='D', mfc='r', mec='none', mew=1.2, ms=ms, ls='none', zorder=4)
# 
# data = np.loadtxt("L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_0.5-10_Rasia_1e7_Tvir_thresh_3.0.txt")
# plt.plot(data[:,temp_idx], data[:,12] * 1e10 / h, marker=markers[2], mfc='none', mec='g', ms=ms, ls='none', mew=1.2, zorder=4)
# med, bin_edges, n = stats.binned_statistic(data[:,temp_idx], data[:,12] * 1e10 / h, statistic='median', bins=bins)
# plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c='g', ls=ls[3], lw=1.5, dash_capstyle='round', label="Best ($N=10^7$ T $< 3.0 T_{200}$)", zorder=4)#(APEC $T_{min}=0.01$ keV)
# for zoom in ["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c256_box_Arepo_new", "c320_box_Arepo_new", "c384_box_Arepo_new"]:
#     data = np.loadtxt("/home/nh444/data/project1/L-T_relations/LDRIntRadioEffBHVel/"+zoom+"_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_0.5-10_Rasia_1e7_Tvir_thresh_3.0.txt")
#     plt.plot(data[temp_idx], data[12] * 1e10 / h, marker='D', mfc='g', mec='none', mew=1.2, ms=ms-2, ls='none', zorder=4)
#     
# data = np.loadtxt("L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_0.5-10_Rasia_1e7_core_ex.txt")
# plt.plot(data[:,temp_idx], data[:,12] * 1e10 / h, marker=markers[2], mfc='none', mec='b', ms=ms, ls='none', mew=1.2, zorder=4)
# med, bin_edges, n = stats.binned_statistic(data[:,temp_idx], data[:,12] * 1e10 / h, statistic='median', bins=bins)
# plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c='b', ls=ls[3], lw=1.5, dash_capstyle='round', label="Best ($N=10^7$ no temp. thresh. core excised)", zorder=4)
# for zoom in ["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c256_box_Arepo_new", "c320_box_Arepo_new", "c384_box_Arepo_new"]:
#     data = np.loadtxt("/home/nh444/data/project1/L-T_relations/LDRIntRadioEffBHVel/"+zoom+"_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_0.5-10_Rasia_1e7_core_ex.txt")
#     plt.plot(data[temp_idx], data[12] * 1e10 / h, marker='D', mfc='b', mec='none', mew=1.2, ms=ms, ls='none', zorder=4)
#     
# data = np.loadtxt("/home/nh444/data/project1/L-T_relations/L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_025/L-T_data_apec_0.5-10_Rasia_1e7_grp2.txt")
# plt.plot(data[temp_idx], data[12] * 1e10 / h, marker='o', mec='none', label="$N=10^7$", mew=1.2, ms=ms, ls='none', zorder=4)
# data = np.loadtxt("/home/nh444/data/project1/L-T_relations/L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_025/L-T_data_apec_0.5-10_Rasia_1e7_Tvir_thresh_2.5_grp2.txt")
# plt.plot(data[temp_idx], data[12] * 1e10 / h, marker='o', mec='none', label="T $< 2.5 T_{200}$ $N=10^7$", mew=1.2, ms=ms, ls='none', zorder=4)
# data = np.loadtxt("/home/nh444/data/project1/L-T_relations/L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_025/L-T_data_apec_0.5-10_Rasia_1e7_Tvir_thresh_3.0_grp2.txt")
# plt.plot(data[temp_idx], data[12] * 1e10 / h, marker='o', mec='none', label="T $< 3.0 T_{200}$ $N=10^7$", mew=1.2, ms=ms, ls='none', zorder=4)
# data = np.loadtxt("/home/nh444/data/project1/L-T_relations/L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_025/L-T_data_apec_0.5-10_Rasia_1e7_Tvir_thresh_3.5_grp2.txt")
# plt.plot(data[temp_idx], data[12] * 1e10 / h, marker='o', mec='none', label="T $< 3.5 T_{200}$ $N=10^7$", mew=1.2, ms=ms, ls='none', zorder=4)
# data = np.loadtxt("/home/nh444/data/project1/L-T_relations/L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_025/L-T_data_apec_0.5-10_Rasia_1e7_Tvir_thresh_4.5_grp2.txt")
# plt.plot(data[temp_idx], data[12] * 1e10 / h, marker='o', mec='none', label="T $< 4.5 T_{200}$ $N=10^7$", mew=1.2, ms=ms, ls='none', zorder=4)
#==============================================================================

### Observations ###
ms=4
c='0.7'
c2='0.4'##lensing

XXL = np.genfromtxt(obs_dir+"XXL_Lieu2016.csv", delimiter=",") ## 0-300 kpc
E = lambda z: (0.28*(1+z)**3 + 0.72)**0.5
#uplims = np.isnan(XXL[:,10])
uplims = (XXL[:,10]==0) ## points as upper limits
XXL[uplims,11] *= 0.3 ## make the upper limit arrows smaller
plt.errorbar(XXL[:,2], XXL[:,9]*1e14*0.7/h*E(XXL[:,1]), xerr=[np.absolute(XXL[:,4]), XXL[:,3]], yerr=[np.absolute(XXL[:,11]*1e14*0.7/h*E(XXL[:,1])), XXL[:,10]*1e14*0.7/h*E(XXL[:,1])], uplims=uplims, c=c2, mec=c2, mfc='none', marker="o", ls='none', ms=6, mew=1.2, label="XXL M$_{500}$-T$_{300\mathrm{kpc}}$ (Lieu+ 2016)")
#XXL2 = np.loadtxt(obs_dir+"XXL_Lieu2016_from_plot.txt") ## taken from plot rather than table in Lieu 2016
#plt.errorbar(XXL2[:,0], np.log10(XXL2[:,1]*0.7/h), marker='o', ls='none')
XXL_fit = lambda T: 13.56 + 1.78 * np.log10(T) + np.log10(0.7/h) ## = log(E(z) * M500)
plt.errorbar([Tmin, Tmax], [10**XXL_fit(Tmin), 10**XXL_fit(Tmax)], c=c2, ls='dotted', dash_capstyle='round', lw=1.5, label="XXL M$_{500}$-T$_{300\mathrm{kpc}}$ fit")

COSMOS_fit = lambda T: 14.0 + 0.39 + 1.71 * np.log10(T/3.0) + np.log10(0.7/h) ## = log( E(z) * M500 ) ## Kettula 2013 [0.1 - 0.5] r500
plt.errorbar([Tmin, Tmax], [10**COSMOS_fit(Tmin), 10**COSMOS_fit(Tmax)], c=c2, ls='dashed', lw=1.5, label="COSMOS M$_{500}$-T$_{\mathrm{X}}$ (Kettula+ 2013)")


eck = np.genfromtxt(obs_dir+"Eckmiller2011-M500-T.csv", delimiter=",") ## X-ray hydrostatic masses, T with some core excised (Variable radius)
Ez = np.sqrt((0.3*(1+eck[:,1])**3 + 0.7))
plt.errorbar(eck[:,2], eck[:,17]*1e13*(0.7/h)*Ez, xerr=[abs(eck[:,4]), eck[:,3]], yerr=[abs(eck[:,19])*1e13*(0.7/h)*Ez, eck[:,18]*1e13*(0.7/h)*Ez], c=c, mec='none', marker='D', linestyle='none', ms=ms, label="Eckmiller+ 2011")##d34044
#eck_fit = lambda T: 1e14 * (0.7/h) * 10**(1.68 * np.log10(T / 3.0) + 0.15) ## M500 - T (no Ez)
#plt.errorbar([Tmin, Tmax], [eck_fit(Tmin), eck_fit(Tmax)], c=c, ls='solid', lw=1.5, label="Eckmiller+ 2011 fit")

sun = np.genfromtxt(obs_dir+"Sun2009_Mgas.txt") ## X-ray hydrostatic masses, [0.15-1] r500 temp
Ez = np.sqrt((0.24*(1+sun[:,1])**3 + 0.76))
plt.errorbar(sun[:,11], sun[:,2]*1e13*(0.73/h)*Ez, xerr=[abs(sun[:,13]), sun[:,12]], yerr=[abs(sun[:,4])*1e13*(0.73/h)*Ez, sun[:,3]*1e13*(0.73/h)*Ez], c=c, mec='none', marker='v', linestyle='none', ms=ms, label="Sun+ 2009")
#sun_fit = lambda T: 1.27 * 1e14 / h * (T / 3.0)**1.67 ## = E(z) * M500
#plt.errorbar([Tmin, Tmax], [sun_fit(Tmin), sun_fit(Tmax)], c=c, lw=1.5, label="Sun+ 2009")

Lov = np.genfromtxt(obs_dir+"Lovisari2015.csv", delimiter=",") ## X-ray hydrostatic masses, same core excision method as Eckmiller 2011
Ez = np.sqrt((0.27*(1+Lov[:,1])**3 + 0.73))
plt.errorbar(Lov[:,2], Lov[:,7]*1e13*0.7/h*Ez, xerr=Lov[:,3], yerr=Lov[:,8]*1e13*0.7/h*Ez, c=c, mec='none', marker="^", ls='none', ms=ms, label="Lovisari+ 2015")
#Lov_fit = lambda T: (1.71 * np.log10(T/2.0) + 0.2) + np.log10(5e13) + np.log10(0.7/h)
#plt.errorbar([Tmin, Tmax], [10**Lov_fit(Tmin), 10**Lov_fit(Tmax)], c=c, ls='solid', lw=1.5, label="Lovisari+ 2015 groups+clusters")
Lov_fit = lambda T: (1.65 * np.log10(T/2.0) + 0.19) + np.log10(5e13) + np.log10(0.7/h)
plt.errorbar([Tmin, Tmax], [10**Lov_fit(Tmin), 10**Lov_fit(Tmax)], c=c, ls='solid', lw=1.5, label="Lovisari+ 2015 best-fit")

#Zou = np.genfromtxt(obs_dir+"Zou2016.txt") ## I don't really trust their R500 (M500) values coz they come from a scaling relation
#Ez = np.sqrt((0.3*(1+Zou[:,1])**3 + 0.7))
#plt.errorbar(Zou[:,3], Zou[:,12]*1e13*(0.7/h)*Ez, xerr=[abs(Zou[:,5]), Zou[:,4]], c=c, mec='none', marker='s', linestyle='none', ms=ms, label="Zou+ 2016")

REX = np.genfromtxt(obs_dir+"REXCESS_M-T.csv", delimiter=",") ## their R500 (M500) values come from a scaling relation but they plot and fit L-M in Pratt 2009 anyway
Ez = np.sqrt((0.3*(1+REX[:,1])**3 + 0.7))
### Core included [0-1] r500
plt.errorbar(REX[:,4], REX[:,10]*1e14*(0.7/h)*Ez, xerr=[abs(REX[:,6]), REX[:,5]], c=c, mec='none', marker='o', ls='none', ms=ms, label="Pratt+ 2009")
### Core excluded [0.15-1] r500
plt.errorbar(REX[:,7], REX[:,10]*1e14*(0.7/h)*Ez, xerr=[abs(REX[:,9]), REX[:,8]], c=c, mec='none', marker='o', ls='none', ms=ms, label="Pratt+ 2009")

"""
The following don't necessarily plot E(z) times M500
"""
#Ket_fit = lambda T: 14.0 + 0.34 + 1.48 * np.log10(T/3.0) + np.log10(0.7/h) ## = log( E(z) * M500 ) ## Kettula 2013
#plt.errorbar([Tmin, Tmax], [Ket_fit(Tmin), Ket_fit(Tmax)], c=c, ls='dashdot', lw=1.5, label="COSMOS+CCCP+160SD2")# M$_{500}$-T$_{\mathrm{X}}$")

# macsis_fit = lambda M: 10**0.68 * (M / 4e14)**0.58 ## assumes redshift zero
# plt.plot([macsis_fit(10**logM500min), macsis_fit(10**logM500max)], [logM500min, logM500max], label="MACSIS + BAHAMAS best-fit (M$_{500,\mathrm{spec}}$-T$_{500,\mathrm{spec}}^{\mathrm{ce}}$)", lw=1.5, c='darkblue')
# #plt.plot([macsis_fit(10**logM500min), macsis_fit(10**logM500max)], [logM500min-np.log10(0.65), logM500max-np.log10(0.65)], lw=1.5, c='darkblue', ls='dashed')
# #plt.plot([macsis_fit(10**logM500min), macsis_fit(10**logM500max)], [logM500min-np.log10(0.8), logM500max-np.log10(0.8)], lw=1.5, c='darkblue', ls='dashed')

# Planelles_fit_sl = lambda M: 5.02 * (M / (5e14/h))**0.54 ## assuming redshift zero
# plt.plot([Planelles_fit_sl(10**logM500min), Planelles_fit_sl(10**logM500max)], [logM500min, logM500max], label=r"Planelles+ 2014 best-fit (M$_{500,\mathrm{true}}$-T$_{500,\mathrm{sl}}$)", lw=1.5, c='green')
# Planelles_fit_mw = lambda M: 5.23 * (M / (5e14/h))**0.55 ## assuming redshift zero
# plt.plot([Planelles_fit_mw(10**logM500min), Planelles_fit_mw(10**logM500max)], [logM500min, logM500max], label=r"Planelles+ 2014 best-fit (M$_{500,\mathrm{true}}$-T$_{500,\mathrm{mw}}$)", lw=1.5, c='green', ls='dashed')

# Truong_fit_sl = lambda T: 14.298 + 1.675 * np.log10(T/3.0)## = log(E(z) * M500) 
# plt.plot([Tmin, Tmax], [Truong_fit_sl(Tmin), Truong_fit_sl(Tmax)], label=r"Truong+ 2016 best-fit (M$_{500,\mathrm{true}}$-T$_{500,\mathrm{sl}}$)", lw=1.5, c='orange')
# Truong_fit_mw = lambda T: 14.301 + 1.603 * np.log10(T/3.0)## = log(E(z) * M500) 
# plt.plot([Tmin, Tmax], [Truong_fit_mw(Tmin), Truong_fit_mw(Tmax)], label=r"Truong+ 2016 best-fit (M$_{500,\mathrm{true}}$-T$_{500,\mathrm{mw}}$)", lw=1.5, c='orange', ls='dashed')

# OWLS_fit = lambda M: 10**0.28 * (M / 1e14)**0.577 ## assuming redshift zero
# plt.plot([OWLS_fit(10**logM500min), OWLS_fit(10**logM500max)], [logM500min, logM500max], label=r"cosmo-OWLS best-fit (M$_{500,\mathrm{true}}$-T$_{500,\mathrm{spec}}^{\mathrm{ce}}$)", lw=1.5, c='darkred')

# MUSIC_fit = lambda M: 1.11096e-09 * M**0.65 ## E(z) M500 BCES Orthogonal
# #MUSIC_fit = lambda M: 1.045069e-10 * M**0.72 ## E(z) M500 BCES Bisector
# ## BCES orthogonal: intercept 1.11096e-09 and slope 0.65
# ## BCES bisector: intercept 1.045069e-10 and slope 0.72
# plt.plot([MUSIC_fit(10**logM500min), MUSIC_fit(10**logM500max)], [logM500min, logM500max], lw=1.5, c='pink', ls='dotted', dash_capstyle='round')
# plt.plot([MUSIC_fit(1e14), MUSIC_fit(10**logM500max)], [np.log10(1e14), logM500max], label=r"MUSIC best-fit (M$_{500,\mathrm{true}}$-T$_{500,\mathrm{spec}}$)", lw=1.5, c='pink')

# Fabjan_fit_mw = lambda T: 14.427 + 1.615 * np.log10(T/3.0)## = log(E(z) * M500) 
# plt.plot([Tmin, Tmax], [Fabjan_fit_mw(Tmin), Fabjan_fit_mw(Tmax)], label=r"Fabjan+ 2011 CSF-M-W (M$_{500,\mathrm{true}}$-T$_{500,\mathrm{mw}}^{\mathrm{ce}}$)", lw=1.5, c='black', ls='solid')

#vikh_fit = lambda T: 2.89e14 / h * (T / 5)**1.58
#plt.plot([Tmin, Tmax], [np.log10(vikh_fit(Tmin)), np.log10(vikh_fit(Tmax))], c=c, ls='dashdot', lw=1.5, label="Vikhlinin+ 2006")

'''
## Plot gadget
data = np.loadtxt("gadget/prop_file_concat.txt")
T = data[:,15]
M500 = data[:,21] * 1e10 / h
med, bin_edges, n = stats.binned_statistic(T, np.log10(M500), statistic='median', bins=bins)
mask = np.isfinite(med)
#plt.plot(x[mask], med[mask], c="#d45b46", label="LDRIntRadioEffBHVel T$_{\mathrm{spec}}$($r>0.15r_{500}$)", lw=1.5)
plt.plot(T, np.log10(M500), marker='D', mec='none', mfc="#d45b46", ms=5, mew=1.2, ls='none', label="Gadget runs (M$_{500,\mathrm{true}}$-T$_{500,\mathrm{spec}}$)")
'''

handles, labels = plt.gca().axes.get_legend_handles_labels()
n = 12
leg1 = plt.legend(handles[:n], labels[:n], loc='lower right', frameon=False, numpoints=1, fontsize='x-small', ncol=1)
ax = plt.gca().add_artist(leg1)
plt.legend(handles[n:], labels[n:], loc='upper left', frameon=False, borderaxespad=1, numpoints=1, fontsize='x-small', ncol=1)

#leg = plt.legend(loc='upper left', frameon=False, borderaxespad=1, numpoints=1, fontsize=12, ncol=1)
plt.gca().xaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))

plt.tight_layout(pad=0.45)
plt.savefig(outname)#, bbox_inches='tight', transparent=True)
print "Saved to", outname

