def main():
    outdir = "/home/nh444/Documents/paper/"
    basedir="/home/nh444/data/project1/L-T_relations/"
    simdirs = ["L40_512_NoDutyRadioWeak_HalfThermal_FixEta",
               "L40_512_NoDutyRadioInt_HalfThermal_FixEta",
               "L40_512_LowDutyRadioWeak_HalfThermal_FixEta",
               "L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff",
               ]
    labels = ["Ref",
              "Stronger radio",
              "Quasar duty cycle",
              "Combined"]
    snapnum = 25
    h = 0.679
    filename = "L-T_data_apec_0.5-10_Rasia_1e7_Tvir_thresh_3.0.txt"
    zoomdirs = ["/data/curie4/nh444/project1/L-T_relations/LDRIntRadioEffBHVel/"]
    zooms = [["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c256_box_Arepo_new", "c320_box_Arepo_new", "c384_box_Arepo_new"]]
    
    figsize=(7,7)
    from palettable.colorbrewer.qualitative import Dark2_8 as pallette
    col=pallette.hex_colors
    markers = ["D", "s", "^", "o"]
    ls=['dashdot', 'dotted', 'dashed', 'solid']
    ms=6.5
    lw=3
    mew=1.2
    binwidth = 0.2 ## dex for mass in Msun
    idxScat = [3]
    idxMed = [0,1,2,3]
    
    filename = "L-T_data_apec_0.5-7_Rasia_c4_Tvir_thresh_2.5_L0.1-2.4.txt"
    outname = "Lsoft-Mgas_relation_models.pdf"
    plot_Lsoft_Mgas(simdirs, snapnum, filename, labels=labels, zoomdirs=zoomdirs, zooms=zooms, h=h, basedir=basedir, outname=outname, outdir=outdir, figsize=figsize, idxScat=idxScat, idxMed=idxMed, col=col, markers=markers, ls=ls, ms=ms, lw=lw, mew=mew, binwidth=binwidth)

def plot_Lsoft_Mgas(simdirs, snapnum, filename, labels=None, zoomdirs=None, zooms=[],
                    projected=True, h=0.679, basedir="/home/nh444/data/project1/L-T_relations/",
                    outname="Lsoft-Mgas_relation.pdf", outdir="/home/nh444/data/project1/L-T_relations/",
                    figsize=(7,7), idxScat=[], idxMed=[], hlightzooms=False,
                    col=None, markers=None, ls=None, ms=6.5,
                    lw=2, mew=1.2, binwidth=0.2, blkwht=False):
    import matplotlib.pyplot as plt
    import numpy as np
    import scipy.stats as stats
    
    assert len(simdirs) > 0, "simdirs is an empty list"
    
    if projected:
        lum_idx = 25 ## 1 for 3D luminosity, 25 for projected
    else:
        lum_idx = 1
    
    if col is None:
        from palettable.colorbrewer.qualitative import Dark2_8 as pallette
        col=pallette.hex_colors
    if markers is None:
        markers = ["D", "s", "o"]
    if ls is None:
        ls=['dashdot', 'dotted', 'dashed', 'solid']
    if zoomdirs is None:
        zoomdirs = [""]*len(simdirs)
    
    fig = plt.figure(figsize=figsize)
    #c = '0.7'
    from scaling.plotting.plotObsDat import Lsoft_Mgas
    Lsoft_Mgas(plt.gca(), h=h, ms=ms)
    
    ## Median profiles
    xmin, xmax = [11, 14.5]
    num = int((xmax - xmin) / binwidth)
    bins = np.logspace(xmin, xmax, num=num)
    x = 10**((np.log10(bins[:-1]) + np.log10(bins[1:]))/2)
    
    for simidx, simdir in enumerate(simdirs):
        data = np.loadtxt(basedir+simdirs[simidx]+"_"+str(snapnum).zfill(3)+"/"+filename)
        label = labels[simidx]
        
        if zoomdirs[simidx] is not "":
            for zoom in zooms[simidx]:
                zdata = np.loadtxt(zoomdirs[simidx]+zoom+"_"+str(snapnum).zfill(3)+"/"+filename)
                data = np.vstack((data, zdata))
                if hlightzooms:
                    plt.plot(zdata[13] * 1e10 / h, zdata[lum_idx], marker=markers[simidx], mfc=col[simidx], mec=col[simidx], ms=ms, ls='none', mew=mew)    
        
        if simidx in idxMed:
            med, bin_edges, n = stats.binned_statistic(data[:,13] * 1e10 / h, data[:,lum_idx], statistic='median', bins=bins)
            plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[simidx], ls=ls[simidx], lw=lw, dash_capstyle='round', label=label)
            label = None ## don't want two legend entries
        if simidx in idxScat:
            plt.plot(data[:,13] * 1e10 / h, data[:,lum_idx], marker=markers[simidx], mfc='none', mec=col[simidx], ms=ms, ls='none', mew=mew, label=label)

    
    plt.xlim(10**11, 3e14)
    plt.ylim(1e41,1e46)
    plt.xscale('log')
    plt.yscale('log')
    plt.ylabel("L$_{500}^{0.1-2.4 \mathrm{keV}}$ E(z)$^{-1}$ [erg s$^{-1}$]")
    plt.xlabel("M$_{\mathrm{gas},500}$ E(z) [$M_{\odot}$]")
    plt.gca().tick_params(axis='x', which='major', pad=7)
    
    plt.tight_layout(pad=0.45)
    from scaling.plotting.plotObsDat import add_legends
    if blkwht:
        add_legends(plt.gca(), simdirs, labels, textcol='white')
        fig.patch.set_facecolor('0.2')
        ax = plt.gca()
        ax.set_axis_bgcolor('0.2')
        ax.xaxis.label.set_color('white')
        ax.tick_params(axis='x', which='both', colors='white')
        ax.yaxis.label.set_color('white')
        ax.tick_params(axis='y', which='both', colors='white')
        import matplotlib.spines
        for child in ax.get_children():
            if isinstance(child, matplotlib.spines.Spine):
                child.set_color('white')
        fig.savefig(outdir+outname, bbox_inches='tight', transparent=True)
    else:
        add_legends(plt.gca(), simdirs, labels)
        fig.savefig(outdir+outname)#, bbox_inches='tight')
    print "Saved to", outdir+outname

if __name__ == "__main__":
    main()
