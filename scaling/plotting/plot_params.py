#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Jul  5 12:10:22 2018
Plot scaling relation parameters versus redshift
@author: nh444
"""
from __future__ import print_function


def main():
    import palettable

    indir = "/data/curie4/nh444/project1/L-T_relations/"
    #outdir = "/home/nh444/Documents/Fable_evol/"
    #outdir="/data/vault/nh444/VaultDocs/meetings/Fable_scal_rel_fitting/"
    outdir = "/data/curie4/nh444/project1/L-T_relations/"

#    axnames = ["mass", "gasmass"]
#    suffix = "Mgas-M"
#    plot_params_vs_z(indir, axnames, suffix, outdir=outdir,
#                         min_num=10, show_num=True,
#                         #col=['#218380', '#8F2D56', '#FFBC42'],
#                         #col=['#BD1550', '#E97F02', '#8A9B0F'],
#                         #col=['#068587', '#4FB99F', '#F2B134'],
#                         col=['#FD6041', '#FEAA3A', '#2DA4A8'],
#                         #col=palettable.cartocolors.qualitative.Vivid_3.hex_colors,
#                         insuffixes=["_3e13", "_6e13", "_1e14"])

#    axnames = ["T500", "mass"]
#    suffix = "M-T"
#    plot_params_vs_z(indir, axnames, suffix, outdir=outdir,
#                     add_titles=True, #plot_norm=False, plot_scatter=False, comp_to_sims=False, comp_to_obs=False,
#                     axnames2=["T500mw", "mass"])

#    axnames = ["T500", "lum"]
#    suffix = "L-T"
#    plot_params_vs_z(indir, axnames, suffix, outdir=outdir,
#                     Epow=1.64,
#                     axnames2=["T500mw", "lum"])

#    axnames = ["mass", "lum"]
#    suffix = "L-M"
#    plot_params_vs_z(indir, axnames, suffix, Epow=1.73, outdir=outdir)

#    axnames = ["mass", "YX"]
#    suffix = "YX-M"
#    plot_params_vs_z(indir, axnames, suffix,
#                     axnames2=["mass", "YXmw"], outdir=outdir)

    axnames = ["mass", "Y500"]
    suffix = "Y-M"
    plot_params_vs_z(indir, axnames, suffix, normalise=False, outdir=outdir)

#    axnames = ["mass", "gasmass"]
#    suffix = "Mgas-M"
#    plot_params_vs_z(indir, axnames, suffix, outdir=outdir)

#    axnames = ["T500mw", "mass"]
#    suffix = "M-Tmw"
#    plot_params_vs_z(indir, axnames, suffix, outdir=outdir)

#    axnames = ["mass", "SZ"]
#    suffix = "Y-M"
#    plot_params_vs_z(indir, axnames, suffix, outdir=outdir)

#    axnames = ["T500", "lum"]
#    suffix = "L-Tmw"
#    plot_params_vs_z(indir, axnames, suffix, outdir=outdir,
#                     axnames2=["T500mw", "lum"], Epow=1.64)

#    axnames = ["T500", "mass"]
#    suffix = "M-T"
#    plot_params_vs_z(indir, axnames, suffix, outdir=outdir)

#    axnames = ["T500mw", "lum"]
#    suffix = "L-Tmw"
#    plot_params_vs_z(indir, axnames, suffix, ylims_slope=[1.9, 3.6], outdir=outdir)

#    axnames = ["mass", "lum"]
#    outsuffix = "L-M_fits"
#    insuffixes = ["_lm", "_bces_orth", "_bces_orth", "_linmix"]
#    labels = ["least-squares", r"BCES(Y$|$X)", "BCES orthogonal", "Kelly 2007"]
#    plot_params_vs_z(indir, axnames, outsuffix, insuffixes=insuffixes, labels=labels,
#                         ylims_slope=[0.0, 4.0],  ylims_norm=[43, 46],
#                         outdir=outdir)

#    axnames = ["mass", "lum"]
#    outsuffix = "L-M_fits_3e13"
#    insuffixes = ["_lm_3e13", "_bces_3e13", "_bces_ortho_3e13", "_linmix_3e13"]
#    labels = ["least-squares", r"BCES(Y$|$X)", "BCES orthogonal", "Kelly 2007"]
#    plot_params_vs_z(indir, axnames, outsuffix, insuffixes=insuffixes, labels=labels,
#                         ylims_slope=[0.0, 4.0],  ylims_norm=[43, 46],
#                         outdir=outdir)

#    axnames = ["mass", "lum"]
#    outsuffix = "L-M_fits"
#    insuffixes = ["_lm_3e13", "_linmix_3e13"]
#    labels = ["least-squares", "Kelly 2007"]
#    plot_params_vs_z(indir, axnames, outsuffix, insuffixes=insuffixes, labels=labels,
#                         outdir=outdir)

#    axnames = ["T500mw", "lum"]
#    outsuffix = "L-Tmw_fits_5e12"
#    insuffixes = ["_lm", "_bces", "_bces_XY", "_bces_orthog", "_linmix"]
#    labels = ["least-squares", r"BCES(L$|$T)", r"BCES(T$|$L)", "BCES orthogonal", "Kelly 2007"]
#    plot_params_vs_z(indir, axnames, outsuffix, insuffixes=insuffixes, labels=labels,
#                         outdir=outdir)
#    axnames = ["T500mw", "lum"]
#    outsuffix = "L-Tmw_fits_3e13"
#    insuffixes = ["_lm_3e13", "_bces_3e13", "_bcesXY_3e13", "_bces_orthog_3e13", "_linmix_3e13"]
#    labels = ["least-squares", r"BCES(L$|$T)", r"BCES(T$|$L)", "BCES orthogonal", "Kelly 2007"]
#    plot_params_vs_z(indir, axnames, outsuffix, insuffixes=insuffixes, labels=labels,
#                         ylims_slope=[1.1, 4.3],
#                         ylims_norm=[44.2, 45.4],
#                         ylims_scat=[0, 0.4],
#                         outdir=outdir)

#    axnames = ["mass", "lum"]
#    outsuffix = "L-M_mass_dependence"
#    insuffixes = ["_lm_5e12", #"_lm_1e13",
#                  "_lm_3e13", #"_lm_5e13",
#                  "_lm_1e14"]
#    labels = [r"M$_{500} E(z) > 5 \times 10^{12}$ M$_{\odot}$", #r"M$_{500} E(z) > 10^{13}$ M$_{\odot}$",
#              r"M$_{500} E(z) > 3 \times 10^{13}$ M$_{\odot}$", #r"M$_{500} E(z) > 5 \times 10^{13}$ M$_{\odot}$",
#              r"M$_{500} E(z) > 10^{14}$ M$_{\odot}$"]
#    plot_params_vs_z(indir, axnames, outsuffix, insuffixes=insuffixes, labels=labels,
#                         outdir=outdir)

#    axnames = ["T500mw", "lum"]
#    outsuffix = "L-Tmw_mass_dependence"
#    insuffixes = ["_5e12",
#                  "_3e13",
#                  "_1e14"]
#    labels = [r"M$_{500} E(z) > 5 \times 10^{12}$ M$_{\odot}$",
#              r"M$_{500} E(z) > 3 \times 10^{13}$ M$_{\odot}$",
#              r"M$_{500} E(z) > 10^{14}$ M$_{\odot}$"]
#    plot_params_vs_z(indir, axnames, outsuffix, insuffixes=insuffixes, labels=labels,
#                         outdir=outdir, ylims_slope=[1.1, 4.3])


def plot_params_vs_z(indir, axnames, suffix, outsuffix="", insuffixes=None,
                     plot_slope=True, plot_norm=True, plot_scatter=True,
                     axnames2=None, labels=None, subscript=None, col=None,
                     ylims_slope=None, ylims_norm=None, ylims_scat=None,
                     normalise=True, Epow=None,
                     comp_to_obs=True, comp_to_sims=True,
                     CI=True, shaded_err=False, show_num=False, min_num=0,
                     tspacing={},
                     leg_kwargs={'slope': {}, 'norm': {}, 'scat': {}},
                     n_split=None, add_titles=False,
                     outdir="/data/curie4/nh444/project1/L-T_relations/",
                     for_paper=True, h_scale=0.6774, omega_m=0.3089):
    """ Plot best-fit parameters versus redshift.

    Plot the slope and/or normalisation and/or intrinsic scatter of
    the best-fitting scaling relation of axnames[1] versus axnames[0].
    Args:
        indir: directory containing file with best-fit parameters.
        axnames: x- and y-axis quantities e.g. ["mass", "lum"] for L-M relation
        suffix: suffix on output filenames.
        outsuffix: second suffix on output filenames.
        insuffixes: list of suffixes to input files containing the
                    fit parameters.
        plot_slope: (bool) Plot slope versus redshift.
        plot_norm: (bool) Plot normalisation versus redshift.
        plot_scatter: (bool) Plot intrinsic scatter versus redshift.
        axnames2: An optional second set of quantities to plot as a dashed line
        labels: legend labels for each insuffix.
        subscript: subscript on axes labels.
        col: list of colours to use.
        ylims_slope: y-axis limits of the slope. Set to None for default.
        ylims_norm: y-axis limits of the normalisation.
                    Set to None for default.
        ylims_scat: y-axis limit for the scatter. None for default.
        normalise: normalise the norm (logC) to the lowest redshift value.
        Epow: Power of E(z) to overplot on normalisation versus z.
        comp_to_obs: Plot observational constraints if available.
        comp_to_sims: Compare to simulations constraints.
        CI: the saved 'errors' are confidence intervals and are plotted
            as a shaded region. Otherwise, the errors are standard
            errors and are plotted as error bars.
        shaded_err: if not CI, plot shaded region corresponding to
                    error on first redshift bin.
        show_num: label points with number of objects used in the fit.
        min_num: redshift bins with fewer than this many objects are shown
                 as dashed rather than solid lines.
        tspacing: desired tick spacing. Expects a dictionary with possible
                  keys 'slope', 'norm' and 'scat' whose values are the
                  desired tick spacing.
        leg_kwargs: dict of dicts containing kwargs for ax.legend.
        n_split: If not None, the legend is split into two with the first
                 'n_split' going into the left-hand legend and the rest in
                 the right-hand legend. leg_kwargs applies to both legends,
                 except for the location argument.
        add_titles: Add titles 'slope', 'normalisation' and 'intrinsic scatter'
                    to the top of each figure.
        outdir: output directory.
        h_scale: value of little h to scale comparison data to.
                 (affects normalisation plots only).
        omega_m: Matter density parameter. Only used if len(Epows) > 0.
    """
    import matplotlib.pyplot as plt
    from matplotlib import container
    import matplotlib.ticker as ticker
    import numpy as np

    # Make the ticks bigger.
    plt.rcParams['xtick.major.size'] = 10
    plt.rcParams['ytick.major.size'] = 10
    plt.rcParams['xtick.minor.size'] = 5
    plt.rcParams['ytick.minor.size'] = 5

    if ylims_slope is None:
        ylims_slope = [None, None]
    if ylims_norm is None:
        ylims_norm = [None, None]
    if ylims_scat is None:
        ylims_scat = [None, None]
    if insuffixes is None:
        insuffixes = [""]
    if labels is None:
        if comp_to_sims and len(insuffixes) == 1:
            labels = ["FABLE"]
        else:
            labels = [None]*len(insuffixes)
    print("labels =", labels)

    if col is None:
        if len(insuffixes) > 3:
            col = ["#c7673e", "#78a44f", "#6b87c8", "#934fce", "#e6ab02",
                   "#be558f", "#47af8d"]
        else:
            # dark blue, orange, light blue
            col = ['#24439b', '#ED7600', '#50BFD7']
    if len(insuffixes) == 1:
        capsize = 0  # no caps on error bars
    else:
        capsize = 6
    # kwargs to plt.errorbar for the markers.
    # The colours aren't included since they vary.
    mkwargs = {'marker': 'o', 'ms': 9, 'lw': 3,
               'capsize': capsize, 'mew': 3}
    # kwargs for line joining the markers.
    lkwargs = {'lw': 3, 'zorder': 2}
    # kwargs for second set of axes.
    kwargs_2 = {'lw': 3.5, 'ls': '--',
                'dash_capstyle': 'round', 'dashes': (3, 3)}
    SS_kwargs = {'ls': 'dashed', 'c': '0.7', 'lw': 4, 'zorder': 0}
    Epow_kwargs = {'ls': 'dotted', 'c': '0.7', 'lw': 4, 'zorder': 0}
    figsize = (7, 7)

    # get pivot point used for fitting in plot_relation
    from scaling.plotting.plot_relation import get_fit_params
    X0_fit, _, _ = get_fit_params(axnames)
    # get powerlaw function with given pivot point
    from scaling.fit_relation import make_powerlaw
    powerlaw = make_powerlaw(np.log10(X0_fit))

    # get self-similar slope and evolution and pivot point for normalisation
    X0, SS_slope, Epow_SS = get_SS_and_pivot(axnames)
    if axnames2 is not None:
        # Check that these are the same for axnames2.
        X0_2, SS_slope_2, Epow_SS_2 = get_SS_and_pivot(axnames2)
        assert X0_2 == X0
        assert SS_slope_2 == SS_slope
        assert Epow_SS_2 == Epow_SS

    if subscript is None:
        subscript = get_subscript(axnames)
    if axnames2 is not None:
        # Use the subscript as the label.
        label_2 = get_subscript(axnames2)
        label_2 = r"$\mathrm{" + str(label_2) + "}$"  # math mode

    if show_num:
        txt_kwargs = {'textcoords': 'offset points',
                      'horizontalalignment': 'center', 'fontsize': 16}
    fill_kwargs = {'facecolor': '0.5', 'edgecolor': 'none', 'alpha': 0.15}
    fill_kwargs = {'facecolor': '#727D9C', 'edgecolor': 'none', 'alpha': 0.21}

    # Create the figures.
    if plot_slope:
        fig_slope, ax_slope = plt.subplots(figsize=figsize)
    if plot_norm:
        fig_norm, ax_norm = plt.subplots(figsize=figsize)
    if plot_scatter:
        fig_scat, ax_scat = plt.subplots(figsize=figsize)
    zmin, zmax = -0.2, 2.2  # minimum and maximum redshift.

    if plot_norm and Epow is not None:
        # Plot curves showing the evolution of the normalisation expected
        # when it evolves as E(z) to the power of Epow.

        if not normalise:
            raise NotImplementedError("The Epows option is not currently "
                                      "implemented for normalise=False.")

        # First define E(z) assuming flat Lambda-CDM universe.
        def E(z): return (omega_m*(1+z)**3 + (1.0 - omega_m))**0.5

        zbins = np.linspace(0.0, zmax, 100)
        # coefficient is (Epow + Epow_SS) not (Epow - Epow_SS) because Epow_SS
        # is the negative of the self-similar exponent of E(z).
        ax_norm.plot(zbins, np.log10(E(zbins)**(Epow + Epow_SS)),
                     **Epow_kwargs)

    for idx, insuffix in enumerate(insuffixes):
        # Load the fit parameters.
        dat = np.loadtxt(indir + "fit_params_" + axnames[0] + "_" +
                         axnames[1] + insuffix + ".txt")
        # and for the second set of axes names if given.
        if axnames2 is not None:
            dat_2 = np.loadtxt(indir + "fit_params_" + axnames2[0] + "_" +
                               axnames2[1] + insuffix + ".txt")
        # first 11 columns are the redshift, best-fit parameters
        # and the first set of errors (if there's a second set of errors
        # then these are loaded below.)
        (z, slope, slope_err_low, slope_err_high,
         logC0, logC0_err_low, logC0_err_high,
         scat, scat_err_low, scat_err_high, Nfit) = np.transpose(dat[:, :11])
        if axnames2 is not None:
            # We won't use the errors for the second set of axes names.
            (z_2, slope_2, _, _, logC0_2, _, _, scat_2,
             _, _, _) = np.transpose(dat_2[:, :11])

        # get the log normalisation at the given pivot point X0
        logC = powerlaw(np.log10(X0), slope=slope, logC=logC0)
#        # assume the error stays the same
#        logC_err_low = logC0_err_low
#        logC_err_high = logC0_err_high
        # Calculate uncertainty using the average error in the slope.
        slope_err_avg = (slope_err_low + slope_err_high) / 2.0
        logC_err_low = np.sqrt((logC0_err_low)**2 +
                               (slope_err_avg * np.log10(X0 / X0_fit))**2)
        logC_err_high = np.sqrt((logC0_err_high)**2 +
                                (slope_err_avg * np.log10(X0 / X0_fit))**2)
        if normalise:
            # subtract lowest redshift value
            logC -= logC[0]
            logC_err_low -= logC[0]
            logC_err_high -= logC[0]
        if axnames2 is not None:
            logC_2 = powerlaw(np.log10(X0), slope=slope_2, logC=logC0_2)
            if normalise:
                logC_2 -= logC_2[0]
        if dat.shape[1] > 11:
            # get second set of errors
            nextra = (dat.shape[1]-11)/6
            print("Found", nextra, "extra error(s)")
            (slope_err_low2, slope_err_high2,
             logC_err_low2, logC_err_high2,
             scat_err_low2, scat_err_high2) = np.transpose(dat[:, 11:17])
            # Calculate uncertainty using the average error in the slope.
            slope_err_avg2 = (slope_err_low2 + slope_err_high2) / 2.0
            logC_err_low2 = np.sqrt((logC_err_low2)**2 +
                                    (slope_err_avg2 *
                                     np.log10(X0 / X0_fit))**2)
            logC_err_high2 = np.sqrt((logC_err_high2)**2 +
                                     (slope_err_avg2 *
                                      np.log10(X0 / X0_fit))**2)
            if normalise:
                logC_err_low2 -= logC[0]
                logC_err_high2 -= logC[0]
        else:
            nextra = 0
        # Get redshift bins with at least 'min_num' objects.
        numind = (Nfit >= min_num)
        if CI and len(insuffixes) == 1:
            # Plot confidence intervals as a shaded region.
            if len(insuffixes) > 1:
                fill_kwargs['facecolor'] = col[idx]
            if plot_slope:
                ax_slope.fill_between(z, slope-slope_err_low,
                                      slope+slope_err_high, **fill_kwargs)
            if plot_norm:
                ax_norm.fill_between(z, logC-logC_err_low,
                                     logC+logC_err_high, **fill_kwargs)
            if plot_scatter:
                ax_scat.fill_between(z, scat-scat_err_low, scat+scat_err_high,
                                     **fill_kwargs)
            if nextra > 0:
                if plot_slope:
                    ax_slope.fill_between(z, slope-slope_err_low2,
                                          slope+slope_err_high2, **fill_kwargs)
                if plot_norm:
                    ax_norm.fill_between(z, logC-logC_err_low2,
                                         logC+logC_err_high2, **fill_kwargs)
                if plot_scatter:
                    ax_scat.fill_between(z, scat-scat_err_low2,
                                         scat+scat_err_high2, **fill_kwargs)
        # Plot the lines joining all markers first, then the markers
        # depending whether error bars or shaded regions are wanted.
        if plot_slope:
            ax_slope.errorbar(z, slope, c=col[idx], ls='--', **lkwargs)
            ax_slope.errorbar(z[numind], slope[numind], c=col[idx], **lkwargs)
        if plot_norm:
            ax_norm.errorbar(z, logC, c=col[idx], ls='--', **lkwargs)
            ax_norm.errorbar(z[numind], logC[numind], c=col[idx], **lkwargs)
        if plot_scatter:
            ax_scat.errorbar(z, scat, c=col[idx], ls='--', **lkwargs)
            ax_scat.errorbar(z[numind], scat[numind], c=col[idx], **lkwargs)

        if CI and len(insuffixes) == 1:
            # Plot the confidence intervals as a shaded region.
            # Note that zorder is defined so that errorbars overlap properly.

            # Plot redshift bins with N > min_num as solid markers.
            if plot_slope:
                ax_slope.errorbar(z[numind], slope[numind],
                                  c=col[idx], mec=col[idx],
                                  label=labels[idx], zorder=3, **mkwargs)
            if plot_norm:
                ax_norm.errorbar(z[numind], logC[numind], c=col[idx],
                                 mec=col[idx], label=labels[idx],
                                 zorder=3, **mkwargs)
            if plot_scatter:
                ax_scat.errorbar(z[numind], scat[numind], c=col[idx],
                                 mec=col[idx], label=labels[idx],
                                 zorder=3, **mkwargs)
            # Plot other redshift bins (N > min_num) as open markers.
            if plot_slope:
                ax_slope.errorbar(z[~numind], slope[~numind], mfc='none',
                                  mec=col[idx], c=col[idx], ls='none',
                                  zorder=3, **mkwargs)
            if plot_norm:
                ax_norm.errorbar(z[~numind], logC[~numind], mfc='none',
                                 mec=col[idx], c=col[idx], ls='none',
                                 zorder=3, **mkwargs)
            if plot_scatter:
                ax_scat.errorbar(z[~numind], scat[~numind], mfc='none',
                                 mec=col[idx], c=col[idx], ls='none',
                                 zorder=3, **mkwargs)
        else:
            # Plot confidence intervals as error bars.
            # zorder is defined so that errorbars overlap properly.
            zorder = idx + 3
            # Plot redshift bins with N > min_num as solid markers.
            if plot_slope:
                ax_slope.errorbar(z[numind], slope[numind],
                                  yerr=[slope_err_low[numind],
                                        slope_err_high[numind]],
                                  c=col[idx], mec=col[idx], label=labels[idx],
                                  zorder=zorder, **mkwargs)
            if plot_norm:
                ax_norm.errorbar(z[numind], logC[numind],
                                 yerr=[logC_err_low[numind],
                                       logC_err_high[numind]],
                                 c=col[idx], mec=col[idx], label=labels[idx],
                                 zorder=zorder, **mkwargs)
            if plot_scatter:
                ax_scat.errorbar(z[numind], scat[numind],
                                 yerr=[scat_err_low[numind],
                                       scat_err_high[numind]],
                                 c=col[idx], mec=col[idx], label=labels[idx],
                                 zorder=zorder, **mkwargs)
            # Plot other redshift bins (N > min_num) as open markers.
            if plot_slope:
                ax_slope.errorbar(z[~numind], slope[~numind],
                                  # yerr=[slope_err_low[~numind],
                                  #      slope_err_high[~numind]],
                                  c=col[idx], mec=col[idx], mfc='none',
                                  ls='none', zorder=zorder, **mkwargs)
            if plot_norm:
                ax_norm.errorbar(z[~numind], logC[~numind],
                                 # yerr=[logC_err_low[~numind],
                                 #      logC_err_high[~numind]],
                                 c=col[idx], mec=col[idx], mfc='none',
                                 ls='none', zorder=zorder, **mkwargs)
            if plot_scatter:
                ax_scat.errorbar(z[~numind], scat[~numind],
                                 # yerr=[scat_err_low[~numind],
                                 #      scat_err_high[~numind]],
                                 c=col[idx], mec=col[idx], mfc='none',
                                 ls='none', zorder=zorder, **mkwargs)
            if shaded_err and len(insuffixes) == 1:
                # Draw shaded region across plot for first error bar.
                if plot_slope:
                    ax_slope.fill_between([z[0], z[-1]],
                                          slope[0]-slope_err_low[0],
                                          slope[0]+slope_err_high[0],
                                          **fill_kwargs)
                if plot_norm:
                    ax_norm.fill_between([z[0], z[-1]],
                                         logC[0]-logC_err_low[0],
                                         logC[0]+logC_err_high[0],
                                         **fill_kwargs)
                if plot_scatter:
                    ax_scat.fill_between([z[0], z[-1]],
                                         scat[0]-scat_err_low[0],
                                         scat[0]+scat_err_high[0],
                                         **fill_kwargs)
        if plot_slope and idx == 0 and SS_slope is not None:
            # add dashed line for self-similar slope.
            ax_slope.axhline(SS_slope, **SS_kwargs)
        if plot_norm and ((normalise and idx == 0) or len(insuffixes) == 1):
            # add horizontal line corresponding to norm at first redshift.
            ax_norm.axhline(logC[0], **SS_kwargs)

        if axnames2 is not None:
            # Plot the second set of axes names.
            if plot_slope:
                handle_2, = ax_slope.plot(z_2, slope_2, c=col[idx],
                                          zorder=3, **kwargs_2)
            if plot_norm:
                ax_norm.plot(z_2, logC_2, c=col[idx], zorder=3, **kwargs_2)
            if plot_scatter:
                ax_scat.plot(z_2, scat_2, c=col[idx], zorder=3, **kwargs_2)
        if show_num:
            # Label points with the size of the sample at each redshift.
            for j, num in enumerate(Nfit):
                if len(insuffixes) == 1:
                    if plot_slope:
                        ax_slope.annotate("{:d}".format(int(num)),
                                          xy=(z[j], slope[j]+slope_err_high[j]),
                                          xytext=(0, 8), color=col[idx],
                                          **txt_kwargs)
                    if plot_norm:
                        ax_norm.annotate("{:d}".format(int(num)),
                                         xy=(z[j], logC[j]+logC_err_high[j]),
                                         xytext=(0, 8), color=col[idx],
                                         **txt_kwargs)
                    if plot_scatter:
                        ax_scat.annotate("{:d}".format(int(num)),
                                         xy=(z[j], scat[j]+scat_err_high[j]),
                                         xytext=(0, 8), color=col[idx],
                                         **txt_kwargs)
                elif plot_slope:
                    ax_slope.annotate("{:d}".format(int(num)),
                                      xy=(z[j], min(slope-slope_err_low)),
                                      xytext=(0, -24-(24*idx)),
                                      color=col[idx], **txt_kwargs)

    have_data = 0
    if comp_to_sims and len(insuffixes) == 1:
        if not (plot_slope and plot_norm and plot_scatter):
            raise NotImplementedError("comp_to_sims currently only available "
                                      "when plotting all parameters.")
        # The following is colorbrewer2.sequential.YlGnBu_9_r
        simcol = [# '#081D58',
                  # '#253494',
                  # '#225EA8',
                  '#1D91C0',
                  # '#41B6C4',
                  '#7FCDBB',
                  # '#C7E9B4',
                  # '#EDF8B1',
                  # '#FFFFD9',
                  ]
        simcol = ['#50BFD7', '#ED7600']  # light blue, orange
        have_data += plot_other_sims("MACSIS", axnames,
                                     ax_slope, ax_norm, ax_scat, X0,
                                     normalise=normalise,
                                     h_scale=h_scale,
                                     col=simcol[1], plot_kwargs=mkwargs)
        have_data += plot_other_sims("Truong", axnames,
                                     ax_slope, ax_norm, ax_scat, X0,
                                     normalise=normalise,
                                     h_scale=h_scale,
                                     col=simcol[0], plot_kwargs=mkwargs)
    if comp_to_obs:
        if not (plot_slope and plot_norm and plot_scatter):
            raise NotImplementedError("comp_to_obs currently only available "
                                      "when plotting all parameters.")
        have_data += plot_obs(axnames, ax_slope, ax_norm, ax_scat,
                              X0=X0, normalise=normalise, h_scale=h_scale)

    # set y limits if given:
    if ylims_slope[0] is not None and plot_slope:
        ax_slope.set_ylim(bottom=ylims_slope[0])
    if ylims_slope[1] is not None and plot_slope:
        ax_slope.set_ylim(top=ylims_slope[1])
    if ylims_norm[0] is not None and plot_norm:
        ax_norm.set_ylim(bottom=ylims_norm[0])
    if ylims_norm[1] is not None and plot_norm:
        ax_norm.set_ylim(top=ylims_norm[1])
    if ylims_scat[0] is not None and plot_scatter:
        ax_scat.set_ylim(bottom=ylims_scat[0])
    if ylims_scat[1] is not None and plot_scatter:
        ax_scat.set_ylim(top=ylims_scat[1])

    # Set major tick spacing.
    if 'slope' in tspacing and plot_slope:
        ax_slope.yaxis.set_major_locator(ticker.MultipleLocator(tspacing['slope']))
    if 'norm' in tspacing and plot_norm:
        ax_norm.yaxis.set_major_locator(ticker.MultipleLocator(tspacing['norm']))
    if 'scat' in tspacing and plot_scatter:
        ax_scat.yaxis.set_major_locator(ticker.MultipleLocator(tspacing['scat']))
    # Put a minor tick between each pair of major ticks.
    for ax in (ax_slope, ax_norm, ax_scat):
        ax.yaxis.set_minor_locator(ticker.AutoMinorLocator(2))

    # write 'self-similar' underneath line for SS slope
    if SS_slope is not None and plot_slope:
        # get transform to convert data coords to axes coords
        data_to_ax = (ax_slope.transAxes +
                      ax_slope.transData.inverted()).inverted()
        ax_slope.text(0.97, data_to_ax.transform([0, SS_slope])[1]-0.018,
                      "self-similar",
                      color=SS_kwargs['c'],
                      fontsize='small',
                      transform=ax_slope.transAxes,
                      horizontalalignment='right',
                      verticalalignment='top')

    if plot_slope:
        ax_slope.set_ylabel(r"$\beta_{\mathrm{"+subscript+"}}$")
    if normalise and plot_norm:
        ax_norm.set_ylabel(r"$\mathrm{log}_{10}"
                           r"(\mathrm{A}_{\mathrm{"+subscript+"}} / "
                           r"\mathrm{A}_{\mathrm{"+subscript+"}}^{z=0})$")
    elif plot_norm:
        ax_norm.set_ylabel(r"$\mathrm{log}_{10}"
                           r"(\mathrm{A}_{\mathrm{"+subscript+"}}$)")
    if plot_scatter:
        ax_scat.set_ylabel(r"$\sigma_{\mathrm{"+subscript+"}}$")

    if plot_slope:
        ax_slope.set_xlabel("Redshift")
        ax_slope.set_xlim(zmin, zmax)
    if plot_norm:
        ax_norm.set_xlabel("Redshift")
        ax_norm.set_xlim(zmin, zmax)
    if plot_slope:
        ax_scat.set_xlabel("Redshift")
        ax_scat.set_xlim(zmin, zmax)

    # Now plot the axes legends.
    # First define default kwargs for ax.legend
    def_leg_kwargs = {'slope': {'loc': 'upper left', 'fontsize': 'small',
                                'borderaxespad': 2},
                      'norm': {'loc': 'upper left', 'fontsize': 'small',
                               'borderaxespad': 2},
                      'scat': {'loc': 'best', 'fontsize': 'small',
                               'borderaxespad': 2}}
    # make a copy of the given dictionary so that we don't modify the original
    import copy
    my_leg_kwargs = copy.deepcopy(leg_kwargs)
    # fill in any missing dicts/keys from the defaults
    for axis in def_leg_kwargs:
        if axis not in my_leg_kwargs:
            my_leg_kwargs[axis] = def_leg_kwargs[axis]
        else:
            for key in def_leg_kwargs[axis]:
                if key not in my_leg_kwargs[axis]:
                    my_leg_kwargs[axis][key] = def_leg_kwargs[axis][key]
    # To plot just the symbol and not the error bars we need to modify the
    # handles by taking only the first artist (the symbol) if the handle
    # is an error bar.
    if plot_slope:  # First we'll do the slope legend.
        handles, my_labels = ax_slope.get_legend_handles_labels()
        handles = [h[0] if isinstance(h, container.ErrorbarContainer) else
                   h for h in handles]
        if axnames2 is not None:
            # Add legend entry for second set of axes names.
            handles.append(handle_2)
            my_labels.append(label_2)
        if n_split is None:
            ax_slope.legend(handles, my_labels, **my_leg_kwargs['slope'])
        else:
            my_leg_kwargs['slope']['loc'] = 'lower left'
            leg = ax_slope.legend(handles[:n_split], my_labels[:n_split],
                            **my_leg_kwargs['slope'])
            ax_slope.add_artist(leg)
            my_leg_kwargs['slope']['loc'] = 'lower right'
            ax_slope.legend(handles[n_split:], my_labels[n_split:],
                            **my_leg_kwargs['slope'])

    if not for_paper:  # If for_paper, only add legend to the slope plot.
        if plot_norm:
            # Similarly for the normalisation plot.
            handles, my_labels = ax_norm.get_legend_handles_labels()
            handles = [h[0] if isinstance(h, container.ErrorbarContainer) else
                       h for h in handles]
            if axnames2 is not None:
                handles.append(handle_2)
                my_labels.append(label_2)
            ax_norm.legend(handles, my_labels, **my_leg_kwargs['norm'])
        if plot_scatter:
            # and for the scatter plot.
            handles, my_labels = ax_scat.get_legend_handles_labels()
            handles = [h[0] if isinstance(h, container.ErrorbarContainer) else
                       h for h in handles]
            if axnames2 is not None:
                handles.append(handle_2)
                my_labels.append(label_2)
            ax_scat.legend(handles, my_labels, **my_leg_kwargs['scat'])

    if add_titles:
        titlepad = 15
        ax_slope.set_title("Slope", pad=titlepad)
        ax_norm.set_title("Normalisation", pad=titlepad)
        ax_scat.set_title("Intrinsic scatter", pad=titlepad)

    # Save the figures.
    if plot_slope:
        fig_slope.tight_layout(pad=0.4)
        fig_slope.savefig(outdir+"slope_vs_z_"+suffix+outsuffix+".pdf")
        print("Saved to", outdir+"slope_vs_z_"+suffix+outsuffix+".pdf")
        # plt.close(fig_slope)
    if plot_norm:
        fig_norm.tight_layout(pad=0.4)
        fig_norm.savefig(outdir+"norm_vs_z_"+suffix+outsuffix+".pdf")
        print("Saved to", outdir+"norm_vs_z_"+suffix+outsuffix+".pdf")
        # plt.close(fig_norm)
    if plot_scatter:
        fig_scat.tight_layout(pad=0.4)
        fig_scat.savefig(outdir+"scat_vs_z_"+suffix+outsuffix+".pdf")
        print("Saved to", outdir+"scat_vs_z_"+suffix+outsuffix+".pdf")
        # plt.close(fig_scat)


def get_SS_and_pivot(axnames):
    """Get self-similar slope and evolution and the pivot point."""
    from scaling.plotting.plot_relation import get_Epow
    Epow_dict = get_Epow()  # dictionary of coefficients of E(z)
    Epow_SS = None  # the power of E(z) multiplying the y-axis of the relation.
    SS_slope = None
    X0 = None
    from scaling.plotting.plot_relation import temperatures, luminosities
    if axnames[0] in temperatures:
        X0 = 3.0  # temperature pivot point to rescale normalisation to.
        if axnames[1] in luminosities:
            SS_slope = 2.0
            Epow_SS = Epow_dict['L-T']
        elif axnames[1] == "mass":
            SS_slope = 3.0/2.0
            Epow_SS = Epow_dict['M-T']
    elif axnames[0] == "mass":
        X0 = 2e14  # mass pivot point to rescale normalisation to.
        if axnames[1] in luminosities:
            SS_slope = 4.0/3.0
            Epow_SS = Epow_dict['L-M']
        elif axnames[1] == "gasmass":
            SS_slope = 1.0
            Epow_SS = Epow_dict['Mgas-M']
        elif axnames[1] in ("YX", "YXmw"):
            SS_slope = 5.0/3.0
            Epow_SS = Epow_dict['YX-M']
        elif axnames[1] in ["Y500", "Y5r500", "YSPT", "SZ"]:
            SS_slope = 5.0/3.0
            Epow_SS = Epow_dict['Y-M']
    elif axnames[0] in ["YX", "YXmw"]:
        X0 = 3e14  # YX pivot point to rescale normalisation to.
        if axnames[1] == "mass":
            SS_slope = 3.0/5.0
            Epow_SS = Epow_dict['M-YX']
    elif axnames[0] == "gasmass":
        X0 = 2e13  # gas mass pivot point to rescale normalisation to.
        if axnames[1] in luminosities:
            SS_slope = 4.0/3.0
            Epow_SS = Epow_dict['L-Mgas']
        elif axnames[1] == "mass":
            SS_slope = 1.0
            Epow_SS = Epow_dict['M-Mgas']
    if Epow_SS is None:
        print("Self-similar evolution unknown for axnames[0] =", axnames[0])
    if SS_slope is None:
        print("Self-similar slope unknown for axnames[0] =", axnames[0])
    if X0 is None:
        raise ValueError("Pivot point unknown for axnames " +
                         str(axnames[0]) + " and " + str(axnames[1]))

    return X0, SS_slope, Epow_SS


def plot_obs(axnames, ax_slope, ax_norm, ax_scat,
             X0=None, normalise=False, h_scale=0.679):
    """ Overplot observational estimates of slope, normalisation and scatter

    For the given relation and axes objects, overplot errorbars
    corresponding to observational estimates (and 1-sigma errors)
    The normalisation is scaled to the same pivot point, given by X0
    Arguments:
        axnames ([x-axis, y-axis]): axis names
        ax_slope, ax_norm, ax_scat (pyplot axes objects)
        X0 (float): pivot point to scale normalisation to.
                    None (default) sets X0 to the pivot point of the fit.
        normalise (bool): normalise the normalisation to the z=0 value
        h_scale (float): value of little h to scale observations to
    """
    import numpy as np

    c1 = '0.6'  # hydrostatic masses
    c2 = '0.3'  # weak lensing masses
    ms = 12
    plot_kwargs = {'mec': 'none', 'lw': 3, 'ls': 'none',
                   'capsize': 0}

    have_data = False  # whether we plotted anything or not

    if X0 is None:
        # get pivot point used for fitting in plot_relation
        from scaling.plotting.plot_relation import get_fit_params
        X0, _, _ = get_fit_params(axnames)

    hpow = 2.0  # power of (h_obs / h_scale) in YSZ

    if axnames[1] == "Y500":
        have_data = True

#        # Marrone et al. 2012
#        z = 0.2
#        A, Aerr = 0.367, 0.099
#        B , Berr = 0.44, 0.12
#        covAB = -0.012
#        ax_slope.errorbar(z, 1.0/B,
#                          #yerr=Berr/B**2,
#                          marker='o', c=c2,
#                          label="Marrone+12", ms=ms,
#                          **plot_kwargs)
#
#        # calculate corresponding Y500 * DA^2 * E(z)^-2/3 in Mpc^2
#        # from their best-fitting M500-Y500 relation
#        if not normalise:
#            Y0 = np.log10(1e-5 * 10**((np.log10(X0 * (0.73 / h_scale) / 1e14) - A) / B))
#            print("Y0 =", Y0)
#            ax_norm.errorbar(z, Y0,
#                             marker='o', c=c2,
#                             label="Marrone+12", ms=ms,
#                             **plot_kwargs)

        # Andersson et al. 2011
        z = 0.74  # median but large spread (0.3-1.1)
        A, Aerr = 14.06, 0.1  # spherical Y500
        B, Berr = 1.67, 0.29
        Mpiv = 3e14  # their pivot point
        scat, scaterr = 0.09, 0.04  # scatter
        ax_slope.errorbar(z, B,
                          yerr=Aerr,
                          marker='o', ms=ms, c=c1, label="Andersson+11",
                          **plot_kwargs)

        ax_scat.errorbar(z, scat, yerr=scaterr,
                         marker='o', ms=ms, c=c1, label="Andersson+11",
                         **plot_kwargs)
        if not normalise:
            def Anders_bestfit(M):
                # given mass in Msun return Y500 E(z)^-2/3 DA^2 in Mpc^2
                from scipy.constants import m_p  # in kg
                from scipy.constants import m_e  # in kg
                from scipy.constants import c
                from scipy.constants import e
                from scipy.constants import parsec as pc  # in m
                thomson = 6.6524587158e-29  # thomson cross-section m^2
                mu_e = 1.165  # mean molecular weight per free electron.
                Msun = 1.9884430e30  # solar mass in kg
                # get factor to convert Msun keV to Mpc^2
                fac = (Msun * 1.0e3 * e / (mu_e * m_p * m_e * c**2 / thomson) /
                       (1.0e6 * pc)**2)
                # factor of h^2 from DA^2 and factor h^B from mass^B
                return (fac * 10**A * (0.702 / h_scale)**hpow *
                        (np.asarray(M) * h_scale / 0.702 / 3e14)**B)
            Y0 = np.log10(Anders_bestfit(X0))
            print("Y0 =", Y0)
            Y0err = np.sqrt(Aerr**2 + (Berr * np.log10(X0 / Mpiv))**2)
            ax_norm.errorbar(z, Y0, yerr=Y0err,
                             marker='o', ms=ms, c=c1, label="Andersson+11",
                             **plot_kwargs)

        # Planck "baseline" relation from Table 1 of Planck XX 2014
        z = 0.152  # median z of Planck ESZ cluster sample
        # which makes up 58 of 71 clusters in the sample
        ax_slope.errorbar(z, 1.79, yerr=0.08,
                          marker='d', ms=ms, c=c1, label="Planck 14",
                          **plot_kwargs)

        ax_scat.errorbar(z, 0.063, yerr=0.011,
                         marker='d', ms=ms, c=c1, label="Planck 14",
                         **plot_kwargs)
        if not normalise:
            def Planck_bestfit(M):
                # given M500 in Msun return Y500 * E(z)^-2/3 * DA^2 in Mpc^2
                b = 0.0
                return (1e-4 * (0.7 / h_scale)**hpow * 10**-0.186 *
                        (np.asarray(M) * (h_scale / 0.7) *
                         (1.0 - b) / 6e14)**1.79)
            Y0 = np.log10(Planck_bestfit(X0))
            Mpiv = 6e14  # their pivot point mass
            Y0err = np.sqrt(0.02**2 + (0.08 * np.log10(X0 / Mpiv))**2)
            ax_norm.errorbar(z, Y0, yerr=Y0err,
                             marker='d', ms=ms, c=c1, label="Planck 14",
                             **plot_kwargs)

        # Wang et al. 2016
        z = 0.11  # approx median based on Fig. 1 in Planck XI 2013
        ax_slope.errorbar(z, [1.61], yerr=[[0.18], [0.14]],
                          marker='s', ms=ms*0.8, c=c2, label="Wang+16",
                          **plot_kwargs)

        if not normalise:
            def Wang_bestfit(M):
                # given M500 in Msun return Y500 * E(z)^-2/3 * DA^2 in Mpc^2

                # factor to convert Y500 at 500 Mpc (in arcmin^2) to Mpc^2
                fac = ((500.0 / (180.0/np.pi*60.0))**2.0 *
                       (0.704 / h_scale)**hpow)
                return 2.31e-5 * fac * (M / (10**13.5 * 0.704/h_scale))**1.61
            Y0 = np.log10(Wang_bestfit(X0))
            Y0err = np.sqrt(np.abs(np.log10(2.31e-5) - np.log10([1.93e-5, 2.67e-5]))**2 +
                            (0.16 * np.log10(X0 / 10**13.5))**2)
            ax_norm.errorbar(z, [Y0],
                             yerr=[Y0err],
                             marker='s', ms=ms*0.8, c=c2, label="Wang+16",
                             **plot_kwargs)

        # Nagarajan 2018
        z = 0.2847  # median redshift of sample
        A, Aerr = 0.97, np.array([0.21, 0.18])  # normalisation
        B, Berr = 1.67, np.array([[0.19], [0.16]])  # slope
        ax_slope.errorbar(z, np.asarray([B]),
                          yerr=Berr,
                          marker='D', ms=ms*0.8, c=c2, label="Nagarajan+18",
                          **plot_kwargs)

        ax_scat.errorbar(
            [z],
            # including 20% scatter in measured mass vs true mass:
            [0.19/np.log(10)], yerr=np.array([[0.09/np.log(10)],
                                             [0.14/np.log(10)]]),
            # ignoring the scatter in measured mass vs true mass:
            # [0.36/np.log(10)], yerr=[[0.12/np.log(10), 0.13/np.log(10)]],
            marker='D', ms=ms*0.8, c=c2, label="Nagarajan+18",
            **plot_kwargs)
        if not normalise:
            def Nagarajan_bestfit(M):
                # return Y500 E(z)^-2/3 DA^2 in Mpc^2
                return (A * 7.93e-5 * (0.7 / h_scale)**hpow *
                        (np.asarray(M) * h_scale / 0.7 / 7.084e14)**B)
            Y0 = np.log10(Nagarajan_bestfit(X0))
            Y0err = np.sqrt(Aerr**2 +
                            (0.26 * np.log10(X0 / 7.084e14))**2)
            ax_norm.errorbar(z, [Y0], yerr=[Y0err],
                             marker='D', ms=ms*0.8, c=c2, label="Nagarajan+18",
                             **plot_kwargs)

    return have_data


def plot_other_sims(name, axnames, ax_slope, ax_norm, ax_scat, X0,
                    normalise=True, col='r', plot_kwargs={}, h_scale=0.679):
    """ Plot parameters from other simulations.

    Arguments:
        name: the simulation, currently one of ("Truong", "MACSIS").
        axnames: x and y axes, e.g. ["mass", "lum"] for L500-M500 relation.
        ax_slope, ax_norm, ax_scat: pyplot axes objects for the slope,
                                    normalisation and scatter vs redshift.
        X0: desired pivot point.
        normalise: plot the normalisation relative to the z=0 value.
        col: colour of the line.
        plot_kwargs: other keyword arguments for plt.plot.
        h_scale: value of little h to scale to.
    Returns:
        have_data: True if data was found and plotted, otherwise False.
    """

    from scaling.plotting.plot_relation import get_sim_relation
    simdat = get_sim_relation(name, axnames, X0=X0, h_scale=h_scale)

    if simdat is None:
        have_data = False
    else:
        have_data = True
        if simdat['slope'] is not None:
            ax_slope.errorbar(simdat['z'], simdat['slope'],
                              yerr=[simdat['slope_errlow'],
                                    simdat['slope_errhigh']],
                              label=simdat['label'], c=col, zorder=1,
                              **plot_kwargs)
        if simdat['norm'] is not None:
            if normalise:
                if abs(simdat['z'][0]) > 0.1:
                    raise ValueError("First redshift in table is not zero.")
                simdat['norm'] -= simdat['norm'][0]  # subtract z=0 value
                simdat['norm_errlow'] -= simdat['norm'][0]
                simdat['norm_errhigh'] -= simdat['norm'][0]
            ax_norm.errorbar(simdat['z'], simdat['norm'],
                             yerr=[simdat['norm_errlow'],
                                   simdat['norm_errhigh']],
                             label=simdat['label'], c=col, zorder=1,
                             **plot_kwargs)
        if simdat['scat'] is not None:
            print(name, "simdat['scat_errlow'] =", simdat['scat_errlow'])
            ax_scat.errorbar(simdat['z'], simdat['scat'],
                             yerr=[simdat['scat_errlow'],
                                   simdat['scat_errhigh']],
                             c=col, zorder=1, label=simdat['label'],
                             **plot_kwargs)

    return have_data


def get_subscript(axnames):
    import warnings

    subscripts = [None, None]
    for i in range(2):
        if axnames[i] == "T500":
            subscripts[i] = "T_{s}"
        elif axnames[i] == "T300kpc":
            subscripts[i] = "T_{300 kpc}"
        elif axnames[i] == "T500mw":
            subscripts[i] = "T_{mw}"
        elif axnames[i] == "T500ew":
            subscripts[i] = "T_{ew}"
        elif axnames[i] == "T500cex":
            subscripts[i] = "T_{s, cex}"
        elif axnames[i] == "T500mwcex":
            subscripts[i] = "T_{mw, cex}"
        elif axnames[i] == "mass":
            subscripts[i] = "M"
        elif axnames[i] == "gasmass":
            subscripts[i] = "M_{gas}"
        elif axnames[i] == "lum":
            subscripts[i] = "L"
        elif axnames[i] == "lum_cex":
            subscripts[i] = "L_{cex}"
        elif axnames[i] == "YX":
            subscripts[i] = "Y_X"
        elif axnames[i] == "YXmw":
            subscripts[i] = "Y_{X, mw}"
        elif axnames[i] == "Y500":
            subscripts[i] = "Y_{500}"
        elif axnames[i] == "Y5r500":
            subscripts[i] = "Y_{5r500}"
        elif axnames[i] == "YSPT":
            subscripts[i] = r"Y_{\mathrm{SPT}}"
        elif axnames[i] == "SZ":
            subscripts[i] = "Y"
        else:
            subscripts[i] = ""
            warnings.warn("subscript unknown for axnames " +
                          str(axnames[0]) + " and " + str(axnames[1]))
    print("subscripts =", subscripts)
    return "-".join(subscripts[::-1])


if __name__ == "__main__":
    main()
