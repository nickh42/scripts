def main():
    outdir = "/home/nh444/Documents/paper/"
    basedir="/home/nh444/data/project1/L-T_relations/"
    simdirs = ["L40_512_NoDutyRadioWeak_HalfThermal_FixEta",
               "L40_512_NoDutyRadioInt_HalfThermal_FixEta",
               "L40_512_LowDutyRadioWeak_HalfThermal_FixEta",
               "L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff",
               ]
    labels = ["Ref",
              "Stronger radio",
              "Quasar duty cycle",
              "Combined"]
    snapnum = 25
    h = 0.679
    filename = "L-T_data_apec_0.5-10_Rasia_1e7_Tvir_thresh_3.0.txt"
    zoomdirs = ["/data/curie4/nh444/project1/L-T_relations/LDRIntRadioEffBHVel/"]
    zooms = []#["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c256_box_Arepo_new", "c320_box_Arepo_new", "c384_box_Arepo_new"]
    
    figsize=(7,7)
    from palettable.colorbrewer.qualitative import Dark2_8 as pallette
    col=pallette.hex_colors
    markers = ["D", "s", "^", "o"]
    ls=['dashdot', 'dotted', 'dashed', 'solid']
    ms=6.5
    lw=3
    mew=1.2
    binwidth = 0.2 ## dex for mass in Msun
    idxScat = [3]
    idxMed = [0,1,2,3]
    
    filename = "L-T_data_apec_0.5-7_Rasia_c4_Tvir_thresh_2.5_L0.1-2.4.txt"
    outname = "Lsoft-M500_relation_models.pdf"
    plot_Lsoft_M500(simdirs, snapnum, filename, labels=labels, zoomdirs=zoomdirs, zooms=zooms, h=h, basedir=basedir, outname=outname, outdir=outdir, figsize=figsize, idxScat=idxScat, idxMed=idxMed, col=col, markers=markers, ls=ls, ms=ms, lw=lw, mew=mew, binwidth=binwidth)

def plot_Lsoft_M500(simdirs, snapnum, filename, labels=None, zoomdirs=None, zooms=[],
                    projected=True, h=0.679, basedir="/home/nh444/data/project1/L-T_relations/",
                    outname="Lsoft-M500_relation.pdf", outdir="/home/nh444/data/project1/L-T_relations/",
                    figsize=(7,7), idxScat=[], idxMed=[], hlightzooms=False,
                    col=None, markers=None, ls=None, ms=6.5,
                    lw=2, mew=1.2, binwidth=0.2, blkwht=False):
    import matplotlib.pyplot as plt
    import numpy as np
    import scipy.stats as stats
    
    assert len(simdirs) > 0, "simdirs is an empty list"
    
    if projected:
        lum_idx = 25 ## 1 for 3D luminosity, 25 for projected
    else:
        lum_idx = 1
    
    if col is None:
        from palettable.colorbrewer.qualitative import Dark2_8 as pallette
        col=pallette.hex_colors
    if markers is None:
        markers = ["D", "s", "o"]
    if ls is None:
        ls=['dashdot', 'dotted', 'dashed', 'solid']
    if zoomdirs is None:
        zoomdirs = [""]*len(simdirs)
    
    fig, ax = plt.subplots(figsize=figsize)
    #c = '0.7'
    #c2 = '0.4'
    from scaling.plotting.plotObsDat import Lsoft_M500
    Lsoft_M500(ax, h=h, ms=ms)
    
    ## Median profiles
    xmin, xmax = [11, 13.5]
    num = int((xmax - xmin) / binwidth)
    bins = np.logspace(xmin, xmax, num=num)
    x = 10**((np.log10(bins[:-1]) + np.log10(bins[1:]))/2)
    
    for simidx, simdir in enumerate(simdirs):
        data = np.loadtxt(basedir+simdirs[simidx]+"_"+str(snapnum).zfill(3)+"/"+filename)
        label = labels[simidx]
        
        if zoomdirs[simidx] is not "":
            for zoom in zooms[simidx]:
                zdata = np.loadtxt(zoomdirs[simidx]+zoom+"_"+str(snapnum).zfill(3)+"/"+filename)
                data = np.vstack((data, zdata))
                if hlightzooms:
                    ax.plot(zdata[12] * 1e10 / h, zdata[lum_idx], marker=markers[simidx], mfc=col[simidx], mec=col[simidx], ms=ms, ls='none', mew=mew)

        if simidx in idxMed:
            med, bin_edges, n = stats.binned_statistic(data[:,12] * 1e10 / h, data[:,lum_idx], statistic='median', bins=bins)
            ax.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[simidx], ls=ls[simidx], lw=lw, dash_capstyle='round', label=label)
            label = None ## don't want two legend entries
        if simidx in idxScat:
            ax.plot(data[:,12] * 1e10 / h, data[:,lum_idx], marker=markers[simidx], mfc='none', mec=col[simidx], ms=ms, ls='none', mew=mew, label=label)
    
    ax.set_xlim(3e12, 2e15)
    ax.set_ylim(1e41,1e46)
    ax.set_xscale('log')
    ax.set_yscale('log')
    ax.set_ylabel("L$_{500}^{0.1-2.4 \mathrm{keV}}$ E(z)$^{-1}$ [erg s$^{-1}$]")
    ax.set_xlabel("M$_{500}$ E(z) [$M_{\odot}$]")#, labelpad=10)#, fontsize=16)
    ax.tick_params(axis='x', which='major', pad=7)
    
    #from scaling.plotting.plotObsDat import add_legends
    #add_legends(ax, simdirs, labels, pad=0.8)
    #n = (len(simdirs) - sum(label is None for label in labels)) ## no. of sims plotted
#==============================================================================
#     n = -2 ## puts last two legend labels at lower right
#     n = 6
#     handles, labels = ax.axes.get_legend_handles_labels()
#     leg1 = ax.legend(handles[:n], labels[:n], loc='upper left', frameon=False, borderpad=0.4, borderaxespad=1, numpoints=1, ncol=1)
#     ax.add_artist(leg1)
#     ax.legend(handles[n:], labels[n:], loc='lower right', frameon=False, borderaxespad=0.8, numpoints=1, ncol=1)
#     
#==============================================================================
    plt.tight_layout(pad=0.45)
    from scaling.plotting.plotObsDat import add_legends
    if blkwht:
        add_legends(plt.gca(), simdirs, labels, textcol='white')
        fig.patch.set_facecolor('0.2')
        ax = plt.gca()
        ax.set_axis_bgcolor('0.2')
        ax.xaxis.label.set_color('white')
        ax.tick_params(axis='x', which='both', colors='white')
        ax.yaxis.label.set_color('white')
        ax.tick_params(axis='y', which='both', colors='white')
        import matplotlib.spines
        for child in ax.get_children():
            if isinstance(child, matplotlib.spines.Spine):
                child.set_color('white')
        fig.savefig(outdir+outname, bbox_inches='tight', transparent=True)
    else:
        add_legends(plt.gca(), simdirs, labels)
        fig.savefig(outdir+outname)#, bbox_inches='tight')
    print "Saved to", outdir+outname

if __name__ == "__main__":
    main()
