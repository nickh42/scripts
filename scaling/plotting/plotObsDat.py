#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 26 13:08:18 2017
Functions which take a matplotlib axes and plot observational data
@author: nh444
"""
import numpy as np
#import matplotlib.pyplot as plt

default_col = '0.7'
default_col2 = '0.4'
## for black background:
default_col = '0.5'
default_col2 = '0.9'
ms2cap = 0.6

def add_legends(ax, simdirs, labels, simcol=None, textcol=None, nohandle=False,
                sims_only=False, nfiles=1, pad=1, frameon=False, fontsize=None,
                simfontsize='medium', obsfontsize='medium', verbose=False):
    """
    Adds legends to the given matplotlib axes -- one for the sims specified
    by simdirs and (if not sims_only) another for everything else
    'simdirs' and 'labels' are not actually used, they're needed to determine
    which of the legend handles are sims
    If col is not None, the sims labels will not have markers but the font colour
    will match 
    """
    if fontsize is not None:
        simfontsize = obsfontsize = fontsize
    n = (len(simdirs) - sum(label is None for label in labels)) * nfiles ## no. of sims plotted
    if simcol is not None:
        assert len(simcol) >= n, "need more colours"
    handles, labels = ax.axes.get_legend_handles_labels()
    if verbose:
        print "labels =", labels
    if sims_only:
        if nohandle:
            leg1 = ax.legend(handles[:n], labels[:n], loc='upper left', markerscale=0, handlelength=0, handletextpad=0, frameon=frameon, borderpad=0.5, borderaxespad=pad, numpoints=1, ncol=1, fontsize=simfontsize)
        else:
            leg1 = ax.legend(handles[:n], labels[:n], loc='upper left', frameon=frameon, borderpad=0.5, borderaxespad=pad, numpoints=1, ncol=1, fontsize=simfontsize)
        for idx, text in enumerate(leg1.get_texts()): ##set text colour
            if simcol is not None:
                text.set_color(simcol[idx])
            elif textcol is not None:
                text.set_color(textcol)
    else:
        if nohandle:
            leg1 = ax.legend(handles[:n], labels[:n], loc='lower right', markerscale=0, handlelength=0, handletextpad=0, frameon=frameon, borderpad=0.5, borderaxespad=1, numpoints=1, ncol=1, fontsize=simfontsize)
        else:
            leg1 = ax.legend(handles[:n], labels[:n], loc='lower right', frameon=frameon, borderpad=0.5, borderaxespad=1, numpoints=1, ncol=1, fontsize=simfontsize)
        for idx, text in enumerate(leg1.get_texts()): ##set text colour
            if simcol is not None:
                text.set_color(simcol[idx])
            elif textcol is not None:
                text.set_color(textcol)
        ax.add_artist(leg1)
        leg2 = ax.legend(handles[n:], labels[n:], loc='upper left', frameon=False, borderaxespad=pad, numpoints=1, ncol=1, fontsize=obsfontsize)
        if textcol is not None:
            for text in leg2.get_texts():
                text.set_color(textcol)
    #ax.legend(loc='lower right', frameon=False, numpoints=1, ncol=1)#, borderaxespad=1

def gas_frac(ax, c=None, c2=None, ms=6, lwerr=1, for_paper=True, h=0.679):
    """
    plots gas mass fraction inside r500 against M500 for observational data
    ms is marker size
    lwerr is thickness of error bars
    """
    if c is None: c = default_col
    if c2 is None: c2 = default_col2
    cap = ms*ms2cap ## cap size of error bars
    
    obs_dir = "/data/vault/nh444/ObsData/mass_fracs/"
    
    Vikh = np.genfromtxt(obs_dir+"Vikhlinin2006.csv", delimiter=",")
    Gastal = np.genfromtxt(obs_dir+"gastaldello_M500.txt")
    #Arn = np.genfromtxt(obs_dir+"Arnaud2007_gas_fracs.csv", delimiter=",")
    Mau = np.genfromtxt(obs_dir+"Maughan2008.csv", delimiter=",")
    Sun = np.genfromtxt(obs_dir+"sun_group_x-ray_properties.txt", usecols=np.arange(0,24))
    Gio = np.genfromtxt(obs_dir+"Giodini09_baryon_fracs.txt")
    REX = np.genfromtxt(obs_dir+"REXCESS_Croston2008.csv", delimiter=",")
    Sand = np.genfromtxt(obs_dir+"Sanderson_2013_gas_fracs.csv", delimiter=",")
    Gonz = np.genfromtxt(obs_dir+"Gonzalez_2013_gas_fracs.txt")
    Lov = np.genfromtxt(obs_dir+"Lovisari2015.csv", delimiter=",")
    #eck = np.genfromtxt(obs_dir+"Eckmiller2011.csv", delimiter=",")
    #Mah = np.genfromtxt(obs_dir+"Mahdavi2013.csv", delimiter=",")
    
    #XXL = np.genfromtxt(obs_dir+"XXL.csv", delimiter=",")
    
    ax.errorbar(Vikh[:,17]*1e14*0.72/h, Vikh[:,23], xerr=Vikh[:,18], yerr=Vikh[:,24], c=c, mec='none', marker='^', ls='none', lw=lwerr, capthick=lwerr, ms=ms*1.4, capsize=cap, label="Vikhlinin+ 2006")#z < 0.23 #ff471a
    mask = (np.arange(len(Gastal[:,0]))!=12)
    ax.errorbar(Gastal[mask,0]*0.7/h, Gastal[mask,6], xerr=[Gastal[mask,1],Gastal[mask,2]], yerr=[Gastal[mask,7],Gastal[mask,8]], c=c, mec='none', marker='>', ls='none', lw=lwerr, capthick=lwerr, ms=ms*1.4, capsize=cap, label="Gastaldello+ 2007")# z< 0.08 #cc99ff
    #ax.errorbar(Arn[:,5]*1e14*0.7/h, Arn[:,14], xerr=[np.absolute(Arn[:,7])*1e14*0.7/h, Arn[:,6]*1e14*0.7/h], yerr=[np.absolute(Arn[:,16]), Arn[:,15]], c=c, mec='none', marker='v', ls='none', lw=lwerr, capthick=lwerr, ms=ms*1.6, capsize=cap, label="Arnaud+ 2007")
    ax.errorbar(Mau[:,3]*1e14*0.7/h, Mau[:,13]/10/Mau[:,3], yerr=[np.absolute(Mau[:,15]/10/Mau[:,3]), Mau[:,14]/10/Mau[:,3]], c=c, mec='none', marker='v', ls='none', lw=lwerr, capthick=lwerr, ms=ms*1.4, capsize=cap, label="Maughan+ 2008")
    ax.errorbar(Gio[:,0]*0.72/h, Gio[:,5], xerr=[Gio[:,1], Gio[:,2]], yerr=Gio[:,6], c=c, mec='none', marker="<", ls='none', lw=lwerr, capthick=lwerr, ms=ms*1.4, capsize=cap, label="Giodini+ 2009")
    ax.errorbar(Sun[:,13]*1e13*0.73/h, Sun[:,16], xerr=[Sun[:,3],Sun[:,2]], yerr=[Sun[:,18],Sun[:,17]], c=c, mec='none', marker='D', ls='none', lw=lwerr, capthick=lwerr, ms=ms, capsize=cap, label="Sun+ 2009")# 0.012 <z< 0.12 # '#90AC28' #ffa64d
    #ax.errorbar(Sun[:,13]*1e13*0.73/h, Sun[:,16], xerr=[Sun[:,3],Sun[:,2]], yerr=[Sun[:,18],Sun[:,17]], c=c, mec=c, mfc='white', zorder=0, marker='D', ls='none', ms=ms, capsize=cap, label="Sun+ 2009")# 0.012 <z< 0.12 # '#90AC28' #ffa64d
    if not for_paper: ax.errorbar(Sun[:,13]*1e13*0.73/h, Sun[:,16], xerr=[Sun[:,3],Sun[:,2]], yerr=[Sun[:,18],Sun[:,17]], c=c2, mec='none', marker='d', ls='none', lw=lwerr, ms=ms*1.4, capthick=lwerr, capsize=cap, label="Sun+ 2009")
    ax.errorbar(REX[:,8]*1e14*0.7/h, REX[:,9], yerr=[REX[:,10], REX[:,11]], c=c, mec='none', marker='s', ls='none', lw=lwerr, capthick=lwerr, ms=ms*1.2, capsize=cap, label="Croston+ 2008")
    ax.errorbar(Sand[:,3]*1e14*0.7/h, Sand[:,11], xerr=Sand[:,4], yerr=Sand[:,12], c=c, mec='none', marker='d', ls='none', lw=lwerr, capthick=lwerr, ms=ms*1.4, capsize=cap, label="Sanderson+ 2013")
    ax.errorbar(Gonz[:,2]*1e14*0.702/h, Gonz[:,4], xerr=Gonz[:,3]*1e14*0.702/h, yerr=Gonz[:,5], c=c, mec='none', marker='o', ls='none', lw=lwerr, capthick=lwerr, ms=ms*1.1, capsize=cap, label="Gonzalez+ 2013")
    ax.errorbar(Lov[:,7]*1e13*0.7/h, Lov[:,11], xerr=Lov[:,8]*1e13*0.7/h, yerr=Lov[:,12], c=c, mec='none', marker='*', ls='none', lw=lwerr, capthick=lwerr, ms=ms*2.0, capsize=cap, label="Lovisari+ 2015")
    #ax.errorbar(eck[:,14]*1e13*0.7/h, eck[:,26]*(0.7/h)**1.5, xerr=eck[:,15]*1e13*0.7/h, yerr=eck[:,27]*(0.7/h)**1.5, c=c, mec='none', marker='>', ls='none', lw=lwerr, capthick=lwerr, ms=ms*1.4, capsize=cap, label="Eckmiller+ 2011")
    #ax.errorbar(Mah[:,17]*1e14*(0.7/h), Mah[:,14] / Mah[:,17], xerr=Mah[:,18]*1e14*(0.7/h), yerr=Mah[:,14]/Mah[:,17]*((Mah[:,15]/Mah[:,14])**2+(Mah[:,18]/Mah[:,17])**2)**0.5, c='r', mec='none', marker=">", ls='none', lw=lwerr, capthick=lwerr, ms=ms*1.4, capsize=cap, label="Mahdavi+ 2013")
    
    #ax.errorbar(XXL[:,8]*1e14*0.7/h, XXL[:,17], xerr=[XXL[:,10]*1e14*0.7/h, XXL[:,9]*1e14*0.7/h], yerr=[np.abs(XXL[:,19]), XXL[:,18]], c=c2, mec=c2, mfc='none', marker="o", ls='none', ms=ms, mew=1.2, capsize=cap, label="XXL")
    
    ## Illustris
    Ill =  np.genfromtxt(obs_dir+"Illustris_gas_fracs.txt")
    ax.errorbar(Ill[:,0], Ill[:,1], c='0.5', dash_capstyle='round', dashes=(4,6), lw=2.5, label="Illustris")
    
    #BAH = np.genfromtxt(obs_dir+"BAHAMAS_gas_fracs.csv", delimiter=",")
    #ax.errorbar(10**BAH[:,0]*0.7/h, BAH[:,1], c='cyan', dash_capstyle='round', dashes=(4,6), lw=2.5, label="BAHAMAS, true")
    #ax.errorbar(10**BAH[:,0]*0.7/h, BAH[:,2], c='r', dash_capstyle='round', dashes=(4,6), lw=2.5, label="BAHAMAS, x-ray")

    ## Cosmic baryon fraction
    #ax.axhline(0.0483/0.3065, ls='dashed', c=c) ## 0.0483/0.3065 for box
    
def Lsoft_M500(ax, c=None, c2=None, ms=6.5, h=0.679):
    if c is None: c = default_col
    if c2 is None: c2 = default_col2
    cap = ms*ms2cap ## cap size of error bars
    
    obs_dir = "/home/nh444/vault/ObsData/L-M/"
       
    Lov = np.genfromtxt(obs_dir+"Lovisari2015.csv", delimiter=",")##fairly sure these luminosities are within r500
    Ez = np.sqrt((0.27*(1+Lov[:,1])**3 + 0.73))
    ax.errorbar(Lov[:,7]*1e13*0.7/h*Ez, Lov[:,21]*1e43*(0.7*0.7/(h*h))/Ez, xerr=Lov[:,8]*1e13*0.7/h*Ez, yerr=Lov[:,22]*1e43*(0.7*0.7/(h*h))/Ez, c=c, mec='none', marker="^", ls='none', ms=ms*1.4, capsize=cap, label="Lovisari+ 2015")
    #Lov_fit = lambda M: 1.66 * np.log10(M/(5e13*0.7/h)) - 0.03 + np.log10(1e43) + np.log10(0.7*0.7/(h*h))
    #ax.errorbar([Mmin, Mmax], [10**Lov_fit(Mmin), 10**Lov_fit(Mmax)], c=c, ls='solid', lw=2, label="Lovisari+ 2015 BC")
    #Lov_fit = lambda M: 1.39 * np.log10(M/(5e13*0.7/h)) - 0.12 + np.log10(1e43) + np.log10(0.7*0.7/(h*h))
    #ax.errorbar([Mmin, Mmax], [10**Lov_fit(Mmin), 10**Lov_fit(Mmax)], c='r', ls='dashed', lw=2, label="Lovisari+ 2015 BC fit (all)")
    
    eck = np.genfromtxt(obs_dir+"Eckmiller2011.csv", delimiter=",")
    ## It's not clear if their luminosities are within r500 or not
    Ez = np.sqrt((0.3*(1+eck[:,1])**3 + 0.7))
    ax.errorbar(eck[:,14]*1e13*0.7/h*Ez, eck[:,2]*1e44*(0.7*0.7/(h*h))/Ez, xerr=eck[:,15]*1e13*0.7/h*Ez, yerr=eck[:,3]/100.0*eck[:,2]*1e44*(0.7*0.7/(h*h))/Ez, c=c, mec='none', marker='D', ls='none', ms=ms, capsize=cap, label="Eckmiller+ 2011")
    
    REX = np.genfromtxt(obs_dir+"Pratt_2009_L_soft.csv", delimiter=",") ## their R500 (M500) values come from a scaling relation but they plot and fit L-M in Pratt 2009 anyway
    Ez = np.sqrt((0.3*(1+REX[:,1])**3 + 0.7))
    ax.errorbar(REX[:,2]*1e14*Ez*0.7/h, REX[:,9]*1e44/Ez*(0.7*0.7/(h*h)), yerr=REX[:,10]*1e44/Ez*(0.7*0.7/(h*h)), c=c, mec='none', marker='s', ls='none', ms=ms*1.2, capsize=cap, label="Pratt+ 2009")
    
    Giles = np.genfromtxt(obs_dir+"Giles2015.csv", delimiter=",") ## luminosities within r500 and hydrostatic masses
    Ez = np.sqrt((0.282*(1+Giles[:,1])**3 + 0.718))
    ax.errorbar(Giles[:,9]*1e14*0.697/h, Giles[:,5]*1e44/Ez*(0.697*0.697/(h*h)), xerr=[np.abs(Giles[:,11]*1e14*0.697/h), Giles[:,10]*1e14*0.697/h], yerr=Giles[:,6]*1e44/Ez*(0.697*0.697/(h*h)), c=c, mec='none', marker='v', ls='none', ms=ms*1.4, capsize=cap, label="Giles+ 2017")
    #Giles_fit = lambda M: 0.52 * 1e45 * (0.7*0.7/(h*h)) * (M / (1e15*0.7/h))**1.92
    #ax.errorbar([Mmin, Mmax], [Giles_fit(Mmin), Giles_fit(Mmax)], c=c, ls='dashed', lw=2, label="Giles+ 2017 BC")
    #Giles_fit = lambda M: 1.15 * 1e45 * (0.7*0.7/(h*h)) * (M / (1e15*0.7/h))**1.34
    #ax.errorbar([Mmin, Mmax], [Giles_fit(Mmin), Giles_fit(Mmax)], c=c, ls='dashed', lw=2, label="Giles+ 2017")
    
    ## These luminosities aren't measured within r500, they are estimates of the total luminosity of the (background subtracted) cluster emission within the field of view (I think)
    #HIF = np.genfromtxt(obs_dir+"Reiprich2002_HIFLUGCS.csv", delimiter=",")
    #Ez = np.sqrt((1.0*(1+HIF[:,1])**3))
    #ax.errorbar(HIF[:,12]*1e14*0.5/h*Ez, HIF[:,2]*1e44*(0.5*0.5/(h*h))/Ez, xerr=[abs(HIF[:,14]*1e14*0.5/h*Ez), HIF[:,13]*1e14*0.5/h*Ez], yerr=HIF[:,3]*1e44*(0.5*0.5/(h*h))/Ez, c=c, mec='none', marker='o', ls='none', ms=5, capsize=cap, label="HIFLUGCS")#"Reiprich \& Boehringer 2002"
    
    #Arnaud_fit = lambda M: 10**0.247 * 1e44*0.7*0.7/(h*h) * (M / (3e14*0.7/h))**1.64
    #ax.errorbar([Mmin, Mmax], [Arnaud_fit(Mmin), Arnaud_fit(Mmax)], c='r', ls='solid', lw=2, label="Arnaud+ 2010 BC")
    
    ### Lensing:
    COSMOS = np.genfromtxt(obs_dir+"COSMOS_L-M_Leauthaud2010.csv", delimiter=",")
    Ez = np.sqrt((0.258*(1+COSMOS[:,8])**3 + 0.742)) ## Note: luminosities are already E(z)^-1
    ax.errorbar(COSMOS[:,15]*Ez*1e13*0.72/h, COSMOS[:,2]*1e42*(0.72*0.72/(h*h)), xerr=[abs(COSMOS[:,17])*Ez*1e13*0.72/h, COSMOS[:,16]*Ez*1e13*0.72/h], yerr=COSMOS[:,3]*1e42*(0.72*0.72/(h*h)), c=c2, mec=c2, mfc='none', marker='s', ls='none', ms=ms*1.2, capsize=cap, label="Leauthaud+ 2010")

    WtG = np.genfromtxt(obs_dir+"Mantz_2016_WtG.csv", delimiter=",")
    ind = np.where(np.isfinite(WtG[:,10]) & (WtG[:,1] < 100))[0] ## only weak lensing masses plus redshift cut
    Ez = np.sqrt((0.3*(1+WtG[ind,1])**3 + 0.7))
    ax.errorbar(WtG[ind,10]*1e15*Ez*0.7/h, WtG[ind,8]*1e44/Ez*0.7*0.7/(h*h), xerr=[np.abs(WtG[ind,12]*1e15*Ez*0.7/h), WtG[ind,11]*1e15*Ez*0.7/h], yerr=WtG[ind,9]*1e44/Ez*0.7*0.7/(h*h), c=c2, mec=c2, mfc='none', marker='D', ls='none', ms=ms, capsize=cap, label="Mantz+ 2016")
    
def Lsoft_Mgas(ax, c=None, c2=None, ms=6.5, h=0.679):
    if c is None: c = default_col
    if c2 is None: c2 = default_col2
    cap = ms*ms2cap ## cap size of error bars
    
    obs_dir = "/home/nh444/vault/ObsData/L-M/"
    
    Lov = np.genfromtxt(obs_dir+"Lovisari2015.csv", delimiter=",")##fairly sure these luminosities are within r500
    Ez = np.sqrt((0.27*(1+Lov[:,1])**3 + 0.73))
    ax.errorbar(Lov[:,9]*1e12*0.7/h*Ez, Lov[:,21]*1e43*(0.7*0.7/(h*h))/Ez, xerr=Lov[:,10]*1e12*0.7/h*Ez, yerr=Lov[:,22]*1e43*(0.7*0.7/(h*h))/Ez, c=c, mec='none', marker="^", ls='none', ms=ms*1.4, capsize=cap, label="Lovisari+ 2015")
    #Lov_fit = lambda M: 1.02 * np.log10(M/(5e12*(0.7/h)**2.5)) + 0.16 + np.log10(1e43) + np.log10(0.7*0.7/(h*h))
    #ax.errorbar([Mmin, Mmax], [10**Lov_fit(Mmin), 10**Lov_fit(Mmax)], c=c, ls='solid', lw=2, label="Lovisari+ 2015" (groups))
    #Lov_fit = lambda M: 1.18 * np.log10(M/(5e12*(0.7/h)**2.5)) + 0.26 + np.log10(1e43) + np.log10(0.7*0.7/(h*h))
    #ax.errorbar([Mmin, Mmax], [10**Lov_fit(Mmin), 10**Lov_fit(Mmax)], c=c, ls='dashed', lw=2, label="Lovisari+ 2015")##groups + HIFLUGCS
    
    eck = np.genfromtxt(obs_dir+"Eckmiller2011.csv", delimiter=",")
    ## It's not clear if their luminosities are within r500 or not
    Ez = np.sqrt((0.3*(1+eck[:,1])**3 + 0.7))
    ax.errorbar(eck[:,22]*1e13*0.7/h*Ez, eck[:,2]*1e44*(0.7*0.7/(h*h))/Ez, xerr=eck[:,23]*1e13*0.7/h*Ez, yerr=eck[:,3]/100.0*eck[:,2]*1e44*(0.7*0.7/(h*h))/Ez, c=c, mec='none', marker='D', ls='none', ms=ms, capsize=cap, label="Eckmiller+ 2011")
    
    REX = np.genfromtxt(obs_dir+"Pratt_2009_L_soft.csv", delimiter=",")
    Ez = np.sqrt((0.3*(1+REX[:,1])**3 + 0.7))
    ax.errorbar(10**REX[:,4]*Ez*0.7/h, REX[:,9]*1e44/Ez*(0.7*0.7/(h*h)), xerr=2.303*REX[:,5]*10**REX[:,4]*Ez*0.7/h, yerr=REX[:,10]*1e44/Ez*(0.7*0.7/(h*h)), c=c, mec='none', marker='s', ls='none', ms=ms*1.2, capsize=cap, label="Pratt+ 2009")
    
    WtG = np.genfromtxt(obs_dir+"Mantz_2016_WtG.csv", delimiter=",")
    ind = np.where(WtG[:,1] < 100)[0] ## redshift cut
    Ez = np.sqrt((0.3*(1+WtG[ind,1])**3 + 0.7))
    ax.errorbar(WtG[ind,4]*1e14*Ez*0.7/h, WtG[ind,8]*1e44/Ez*0.7*0.7/(h*h), xerr=WtG[ind,5]*1e14*Ez*0.7/h, yerr=WtG[ind,9]*1e44/Ez*0.7*0.7/(h*h), c=c, mec='none', marker='v', ls='none', ms=ms*1.4, capsize=cap, label="Mantz+ 2016")
    
    ## I shouldn't plot this since Reiprich+2002 and Zhang+2011 don't use the same r500 and they measure very different luminosities for the same objects
    ## Reiprich+2002 mention a forthcoming paper with gas masses - find it!
    #HIF = np.genfromtxt(obs_dir+"HIFLUGCS+Zhang11.csv", delimiter=",") ## Luminosity in soft band from Reiprich and Bohringer 2002 and Mgas from Zhang+2011
    #Ez1 = np.sqrt((1.0*(1+HIF[:,1])**3)) ## Reiprich and Bohringer 2002 (L)
    #Ez2 = np.sqrt((0.3*(1+HIF[:,1])**3 + 0.7)) ## Zhang 2011 (Mgas)
    #ax.errorbar(HIF[:,29]*0.7/h*Ez2, HIF[:,2]*1e44*(0.5*0.5/(h*h))/Ez1, xerr=HIF[:,30]*0.7/h*Ez2, c=c, mec='none', marker='o', ls='none', ms=5, capsize=cap, label="HIFLUGCS")#"Reiprich \& Boehringer 2002"
    
    #Arnaud_fit = lambda M: 10**0.247 * 1e44*0.7*0.7/(h*h) * (M / (3e14*0.7/h))**1.64
    #ax.errorbar([Mmin, Mmax], [Arnaud_fit(Mmin), Arnaud_fit(Mmax)], c='r', ls='solid', lw=2, label="Arnaud+ 2010 BC")

def L_M500(ax, c=None, c2=None, ms=6.5, h=0.679):
    if c is None: c = default_col
    if c2 is None: c2 = default_col2
    cap = ms*ms2cap ## cap size of error bars
        
    obs_dir = "/home/nh444/vault/ObsData/L-M/"
    L_power = 1.0#4.0/3.0 ## luminosity is divided by E(z) to this power 
    M_power = 1.0 ## mass is multiplied by E(z) to this power
    ## self similar scaling is L_power+M_power = 7.0/3.0 as in Giles+2015 and Pratt+2009
    ## so use 7/3 and 0 or 4/3 and 1 but remember to change the labels
    ## or ignore the dependence on the energy band and just use L_power=1 like for soft band
        
#==============================================================================
#     ## This plots the 38 of 100 XXL brightest clusters which have a weak lensing mass measurement
#     ## but the luminosities are from Giles+2016 who use an r500 for the aperture that is derived from the best-fit M-T and not the actual measured r500
#     ## so the luminosities and masses are not measured within the same aperture (difference in r500 is up to ~30%)
#     XXL = np.genfromtxt(obs_dir+"XXL_L-M_WL.csv", delimiter=",") ## bolometric lum within r500 (Giles 2016) and weak lensing masses (Lieu 2016)
#     ## note however that this means the luminosity is then derived within a different r500 to that corresponding to M500
#     ind = np.where(XXL[:,1] <= 100)[0] ## redshift cut
#     Ez = np.sqrt((0.28*(1+XXL[ind,1])**3 + 0.72))
#     uplims = (XXL[ind,10]==0) ## points as upper limits
#     XXL[ind[uplims],11] *= 0.3 ## make the upper limit arrows smaller
#     ax.errorbar(XXL[ind,9]*1e14*Ez**M_power*0.7/h, XXL[ind,20]*1e43/Ez**L_power*(0.7*0.7/(h*h)), xerr=[np.absolute(XXL[ind,11]*1e14*Ez**M_power*0.7/h), XXL[ind,10]*1e14*Ez**M_power*0.7/h], yerr=XXL[ind,21]*1e43/Ez**L_power*(0.7*0.7/(h*h)), xuplims=uplims, c=c2, mec=c2, mfc='none', marker="o", ls='none', ms=6, mew=1.2, capsize=cap, label="XXL")
#     
#==============================================================================
    REX = np.genfromtxt(obs_dir+"REXCESS_L-M.csv", delimiter=",") ## their R500 (M500) values come from a scaling relation but they plot and fit L-M in Pratt 2009 anyway
    # Here I'm plotting the lum within r500 but they also give core excised lum in Pratt 2009
    Ez = np.sqrt((0.3*(1+REX[:,1])**3 + 0.7))
    ax.errorbar(REX[:,5]*1e14*Ez**M_power*(0.7/h), REX[:,7]*1e44/Ez**L_power*(0.7*0.7/(h*h)), yerr=[abs(REX[:,9])*1e44/Ez**L_power*(0.7*0.7/(h*h)), REX[:,8]*1e44/Ez**L_power*(0.7*0.7/(h*h))], c=c, mec='none', marker='s', ls='none', ms=ms*1.2, capsize=cap, label="Pratt+ 2009")
    #REX_fit = lambda M: 1.38 * 1e44 *(0.7*0.7/(h*h)) * (M / (2e14*0.7/h))**2.08 ## Malmquist bias corrected
    #ax.errorbar([Mmin, Mmax], [REX_fit(Mmin), REX_fit(Mmax)], c=c, ls='dashed', lw=2, capsize=cap, label="Pratt+ 2009 BC")

    Giles = np.genfromtxt(obs_dir+"Giles2015.csv", delimiter=",") ## 0.15<z<=0.3 luminosities within r500 and hydrostatic masses
    Ez = np.sqrt((0.282*(1+Giles[:,1])**3 + 0.718))
    ax.errorbar(Giles[:,9]*1e14*Ez**M_power*0.697/h, Giles[:,7]*1e44/Ez**L_power*(0.697*0.697/(h*h)), xerr=[np.abs(Giles[:,11]*1e14*Ez**M_power*0.697/h), Giles[:,10]*1e14*Ez**M_power*0.697/h], yerr=Giles[:,8]*1e44/Ez**L_power*(0.697*0.697/(h*h)), c=c, mec='none', marker='v', ls='none', ms=ms*1.4, capsize=cap, label="Giles+ 2017")
    #Giles_fit = lambda M: 1.45 * 1e45 * (0.7*0.7/(h*h)) * (M / (1e15*0.7/h))**2.22
    #ax.errorbar([Mmin, Mmax], [Giles_fit(Mmin), Giles_fit(Mmax)], c=c, ls='solid', lw=2, label="Giles+ 2017 BC")
    
    #Zou = np.genfromtxt(obs_dir+"ZouMaughan2016.csv", delimiter=",") ## I don't really trust their R500 (M500) values coz they come from a scaling relation
    ## Note that these masses were derived from a scaling relation (YX-M500 from Vikhlinin 2009) - see Zou 2016 paper
    #Ez = np.sqrt((0.3*(1+Zou[:,1])**3 + 0.7))
    #ax.errorbar(Zou[:,12]*1e13*0.7/h, Zou[:,6]*1e43/Ez**power*(0.7*0.7/(h*h)), yerr=Zou[:,7]*1e43/Ez**power*(0.7*0.7/(h*h)), c=c, mec='none', marker='o', ls='none', ms=5, capsize=cap, label="Zou+ 2016")
    #bias = 0.7
    #ax.errorbar(Zou[:,12]*1e13*0.7/h / bias, Zou[:,6]*1e43)+np.log10(0.7*0.7/(h*h), yerr=Zou[:,7], c='blue', mec='blue', mfc='none', marker='o', ls='none', ms=5, capsize=cap, label="Zou+ 2016 (1-b)={:.1f}".format(bias))
        
    ## These are massive clusters with lensing or hydro masses
    #Mah = np.genfromtxt(obs_dir+"Mahdavi2013.csv", delimiter=",")
    #Ez = np.sqrt((0.3*(1+Mah[:,1])**3 + 0.7))
    ## lensing masses:
    #ax.errorbar(Mah[:,11]*1e14*(0.7/h), Mah[:,2]*1e45/Ez**power*(0.7*0.7/(h*h)), xerr=Mah[:,12]*1e14*(0.7/h)*Ez, yerr=Mah[:,3]*1e45/Ez**power*(0.7*0.7/(h*h)), c=c2, mec=c2, mfc='none', marker="o", ls='none', ms=6, mew=1.2, capsize=cap, label="Mahdavi+ 2013")
    ## hydro masses:
    #ax.errorbar(Mah[:,17]*1e14*(0.7/h), Mah[:,2]*1e45/Ez**power*(0.7*0.7/(h*h)), xerr=Mah[:,18]*1e14*(0.7/h)*Ez, yerr=Mah[:,3]*1e45/Ez**power*(0.7*0.7/(h*h)), c=c, mfc=c, mec='none', marker="D", ls='none', ms=5, capsize=cap, label="Mahdavi+ 2013")
    
    ## This uses masses (from tabulated r500) derived from the Lieu+16 M-T relation derived from 38 of the 100 XXL brightest clusters with weak lensing masses
    XXL = np.genfromtxt(obs_dir+"XXL_xray_properties.csv", delimiter=",") ## bolometric lum within r500 and masses estimated from M-T relation from Lieu 2016 for weak lensing masses (I convert r500 to M500)
    ind = np.where(XXL[:,1] <= 100)[0] ## redshift cut
    Ez = np.sqrt((0.28*(1+XXL[ind,1])**3 + 0.72))
    ax.errorbar(XXL[ind,8]*1e14*Ez**M_power*0.7/h, XXL[ind,24]*1e43*Ez**L_power*(0.7*0.7/(h*h)), xerr=[XXL[ind,10]*1e14*Ez**M_power*0.7/h, XXL[ind,9]*1e14*Ez**M_power*0.7/h], yerr=XXL[ind,25]*1e43*Ez**L_power*(0.7*0.7/(h*h)), c=c2, mec=c2, mfc='none', marker='o', ls='none', ms=ms, capsize=cap, label="Giles+ 2016")

    
def L_Mgas(ax, c=None, c2=None, ms=6.5, h=0.679):
    if c is None: c = default_col
    if c2 is None: c2 = default_col2
    cap = ms*ms2cap ## cap size of error bars
        
    obs_dir = "/home/nh444/vault/ObsData/L-M/"
        
    REX = np.genfromtxt(obs_dir+"REXCESS_L-M.csv", delimiter=",")
    Ez = np.sqrt((0.3*(1+REX[:,1])**3 + 0.7))
    ax.errorbar(10**REX[:,3]*(Ez), REX[:,7]*1e44/Ez*(0.7*0.7/(h*h)), xerr=10**REX[:,4], yerr=[abs(REX[:,9])*1e44/Ez*(0.7*0.7/(h*h)), REX[:,8]*1e44/Ez*(0.7*0.7/(h*h))], c=c, mec='none', marker='s', ls='none', ms=ms*1.2, capsize=cap, label="Pratt+ 2009")
    
    Zhang = np.genfromtxt(obs_dir+"Zhang_2011.csv", delimiter=",")
    Ez = np.sqrt((0.3*(1+Zhang[:,5])**3 + 0.7))
    ax.errorbar(Zhang[:,7]*Ez, Zhang[:,12]/Ez*(0.7*0.7/(h*h)), xerr=Zhang[:,8], yerr=Zhang[:,13]/Ez*(0.7*0.7/(h*h)), c=c, mec='none', marker='o', ls='none', ms=ms*1.2, capsize=cap, label="Zhang+ 2011")
    #Zhang_fit = lambda Mgas: 45.14 + 1.26*np.log10(np.asarray(Mgas)/1e14) ## L/Ez vs Mgas*Ez - I used BCES Orthogonal coz it looks like it fits slightly better
    #ax.errorbar([1e10,1e16], 10**Zhang_fit([1e10,1e16]), c=c, ls='solid')#, capsize=cap, label="Zhang+ 2011 best-fit")#d34044

    Mah = np.genfromtxt(obs_dir+"Mahdavi2013.csv", delimiter=",")
    Ez = np.sqrt((0.3*(1+Mah[:,1])**3 + 0.7))
    ax.errorbar(Mah[:,14]*1e14*(0.7/h)*Ez, Mah[:,2]*1e45/Ez*(0.7*0.7/(h*h)), xerr=Mah[:,15]*1e14*(0.7/h)*Ez, yerr=Mah[:,3]*1e45/Ez*(0.7*0.7/(h*h)), c=c, mfc=c, mec='none', marker="v", ls='none', ms=ms*1.4, capsize=cap, label="Mahdavi+ 2013")
    
#==============================================================================
#     XXL = np.genfromtxt(obs_dir+"XXL_xray_properties.csv", delimiter=",") ## bolometric lum within r500 and masses estimated from M-T relation from Lieu 2016 for weak lensing masses (I convert r500 to M500)
#     ind = np.where(XXL[:,1] <= 0.3)[0] ## redshift cut
#     Ez = np.sqrt((0.28*(1+XXL[ind,1])**3 + 0.72))
#     ax.errorbar(XXL[ind,11]*0.7/h, XXL[ind,24]*1e43/Ez*(0.7*0.7/(h*h)), xerr=[np.abs(XXL[ind,13]*0.7/h), XXL[ind,12]*0.7/h], yerr=XXL[ind,25]*1e43/Ez*(0.7*0.7/(h*h)), c=c2, mec=c2, mfc='none', marker='s', ls='none', ms=5, capsize=cap, label="XXL")
# 
#==============================================================================
    
def L_T(ax, core_excised=False, c=None, c2=None, ms=6.5, for_paper=True, h=0.679):
    if c is None: c = default_col
    if c2 is None: c2 = default_col2
    cap = ms*ms2cap ## cap size of error bars
    
    obs_dir = "/home/nh444/vault/ObsData/L-T/"
    H = h * 100.0
    #####################   CORE INCLUDED   #####################
    if not core_excised:
        gems = np.genfromtxt(obs_dir+"gems_project_groups_L_T_table.csv", delimiter=",")
        gems_z = gems[:,19] / 299792.458 ## z ~ v/c
        Ez = np.sqrt((0.3*(1+gems_z)**3 + 0.7))
        mask = (gems[:,15] >= 60) ## they define groups as systems with extraction radius greater than 60 kpc, as opposed to individual galaxies
        ## column 8 is bol lum in r500
        ax.errorbar(gems[mask,2], 10**gems[mask,8]*(70.0*70.0/(H*H)) / Ez[mask], xerr=gems[mask,3], yerr=gems[mask,9]*2.303*10**gems[mask,8]*(70.0*70.0/(H*H)) / Ez[mask], c=c, mec='none', marker='^', ls='none', ms=ms*1.4, capsize=cap, label=r"Osmond \& Ponman 2004")#(G-sample) (groups)
        #mask = (gems[:,15] < 60)
        #ax.errorbar(gems[mask,2], gems[mask,8]*(70.0*70.0/(H*H)), xerr=gems[mask,3], yerr=gems[mask,9]*2.303*10**gems[mask,8]*(70.0*70.0/(H*H)), c='#5eb769', mec='none', marker='v', ls='none', ms=6, capsize=cap, label=r"Osmond \& Ponman 2004 galactic haloes (H-sample)")
        ## Same data but extracted from Sun et al. 2012 Fig. 1 (it matches)
        #osm = np.genfromtxt(obs_dir+"Osmond_Ponman04_L-T.csv", delimiter=",") ## T500, L500/Ez from Sun2012
        #ax.errorbar(osm[:,0], osm[:,1]*1e44*(0.73/h)**2, c=c2, mec=c2, mfc='none', marker='^', ls='none', ms=ms*1.1, capsize=cap, label="Sun+ 2009")

        
        #horner = np.genfromtxt(obs_dir+"horner_L_T_table.txt")
        #Ez = np.sqrt((0.3*(1+horner[:,10])**3 + 0.7)) ## It's not clear what omega they use, could be omega_m = 1 --- they assume a flat universe since they have q0=0.5
        #ax.errorbar(horner[:,4], 10**horner[:,12]*(50.0*50.0/(H*H)) / Ez, xerr=[horner[:,4]-horner[:,5],horner[:,6]-horner[:,4]], c=c, mec='none', marker='D', ls='none', ms=4, capsize=cap, label="Horner 2001")
        
        ### [0 - 1] r500 for temp and lum
        Maughan = np.genfromtxt(obs_dir+"Maughan_2012.csv", delimiter=",")
        Ez = np.sqrt((0.3*(1+Maughan[:,1])**3 + 0.7))
        ax.errorbar(Maughan[:,3], Maughan[:,6]*1e44/Ez*(70.0*70.0/(H*H)), xerr=[abs(Maughan[:,5]), Maughan[:,4]], yerr=Maughan[:,7]*1e44/Ez*(70.0*70.0/(H*H)), c=c, mec='none', marker='v', ls='none', ms=ms*1.4, capsize=cap, label="Maughan+ 2012")

        #Mah = np.genfromtxt(obs_dir+"Mahdavi2013.csv", delimiter=",")
        #Ez = np.sqrt((0.3*(1+Mah[:,1])**3 + 0.7))
        #ax.errorbar(Mah[:,5], Mah[:,2]*1e45/Ez*(0.7*0.7/(h*h)), xerr=Mah[:,6], yerr=Mah[:,3]*1e45/Ez*(0.7*0.7/(h*h)), c=c, mfc=c, mec='none', marker="D", ls='none', ms=4, capsize=cap, label="Mahdavi+ 2013")
        
        Zou = np.genfromtxt(obs_dir+"ZouMaughan2016.csv", delimiter=",")
        Ez = np.sqrt((0.3*(1+Zou[:,1])**3 + 0.7))
        ax.errorbar(Zou[:,3], Zou[:,6]*1e43/Ez*(70.0*70.0/(H*H)), xerr=[abs(Zou[:,5]), Zou[:,4]], yerr=Zou[:,7]*1e43/Ez*(70.0*70.0/(H*H)), c=c, mec='none', marker='o', ls='none', ms=ms*1.1, capsize=cap, label="Zou+ 2016")
        ##### divide lum by Ez!! #####
        #Zou_fit = lambda T: (np.asarray(T) / 2.0)**3.28 * 1e43 * 3.18
        #ax.plot([0.1, 20], Zou_fit([0.1, 20]), c='#fa79a5', ls='dashdot', label="Zou+ 2016 non-bias-corrected best-fit", zorder=3)
        #Zou_fit_cor = lambda T: (np.asarray(T) / 2.0)**3.29 * 1e43 * 2.58
        #ax.errorbar([0.1, 20], Zou_fit_cor([0.1, 20]), c='#fa79a5', ls='dashed', label="Zou+ 2016 bias-corrected best-fit", zorder=3)
        
        rexcess = np.genfromtxt(obs_dir+"REXCESS_table_Pratt2009.csv", delimiter=",", dtype=None)
        #CC = rexcess['f22']
        #disturbed = rexcess['f23']
        Ez = np.sqrt((0.3*(1+rexcess['f1'])**3 + 0.7))
        ax.errorbar(rexcess['f4'], rexcess['f7'] * 1e44 * 70.0*70.0/(H*H) / Ez, xerr=[np.absolute(rexcess['f6']), rexcess['f5']], yerr=[np.absolute(rexcess['f9']) * 1e44 * 70.0*70.0/(H*H) / Ez, rexcess['f8'] * 1e44 * 70.0*70.0/(H*H) / Ez], c=c, mec='none', marker='s', ls='none', ms=ms*1.1, capsize=cap, label="Pratt+ 2009")
        
        sun = np.genfromtxt(obs_dir+"Sun09_L-T.csv", delimiter=",")
        ## NB: temperature is 3D but core excised (0.15-1 r500)
        ax.errorbar(sun[:,5], sun[:,8]*1e44*(0.73/h)**2, xerr=[np.abs(sun[:,6]), sun[:,7]], yerr=[np.abs(sun[:,9])*1e44*(0.73/h)**2, sun[:,10]*1e44*(0.73/h)**2], c=c, mec=c, mew=1.4, mfc='white', zorder=0, marker='D', ls='none', ms=ms*0.8, capsize=cap*0.5, label="Sun 2012") # "Sun+ 2009"
        
        ## XXL measure temperatures within 300 kpc so probably shouldn't compare to T500
        #XXL = np.genfromtxt(obs_dir+"Giles_2016_XXL.csv", delimiter=",") ## bolometric lum within r500 and masses estimated from M-T relation from Lieu 2016 for weak lensing masses (I convert r500 to M500)
        #Ez = np.sqrt((0.28*(1+XXL[:,1])**3 + 0.72))
        #ax.errorbar(XXL[:,5], XXL[:,12]*1e43/Ez*(0.7*0.7/(h*h)), xerr=[abs(XXL[:,7]), XXL[:,6]], yerr=XXL[:,13]*1e43/Ez*(0.7*0.7/(h*h)), c=c, mec='none', marker='s', ls='none', ms=4, capsize=cap, label="Giles+ 2016")
        
    #####################   CORE EXCISED   #####################
    if core_excised:       
        ### not really suitable coz they use a variable core excision radius for temperature but luminosities are not core-excised
        #Bhara = np.genfromtxt(obs_dir+"Bharadwaj2015.csv", delimiter=",")
        #Ez = np.sqrt((0.27*(1+Bhara[:,8])**3 + 0.73))
        ## I'm not totally certain these luminosities are within r500 but I think they are (see Bharadwaj+ 2015 paper)
        #ax.errorbar(Bhara[:,1], Bhara[:,4]*1e43*(71.0*71.0/(H*H)) / Ez, xerr=[abs(Bhara[:,3]), Bhara[:,2]], yerr=Bhara[:,5]*1e43*(71.0*71.0/(H*H)) / Ez, c=c, mec='none', marker='v', ls='none', ms=5, capsize=cap, label="Bharadwaj+ 2015")
        
        ### [0.15 - 1] r500 for temp and lum
        Maughan = np.genfromtxt(obs_dir+"Maughan_2012.csv", delimiter=",")
        Ez = np.sqrt((0.3*(1+Maughan[:,1])**3 + 0.7))
        ax.errorbar(Maughan[:,8], Maughan[:,11]*1e44/Ez*(70.0*70.0/(H*H)), xerr=[abs(Maughan[:,10]), Maughan[:,9]], yerr=Maughan[:,12]*1e44/Ez*(70.0*70.0/(H*H)), c=c, mec='none', marker='v', ls='none', ms=ms*1.4, capsize=cap, label="Maughan+ 2012")
        
        ##### divide lum by Ez!! #####
        #Bhara_fit = lambda T: 5e43 * 10**0.08 * (np.asarray(T) / 3.0)**3.2
        #ax.plot([0.1, 20], (Bhara_fit([0.1, 20])), c='#9f9fff', ls='dashed', label="Bharadwaj+ 2015 best-fit (bias-corrected)", zorder=3)
        #Bhara_fit_uncor = lambda T: 5e43 * 10**-0.01 * (np.asarray(T) / 3.0)**2.17
        #ax.plot([0.1, 20], (Bhara_fit_uncor([0.1, 20])), c='#9f9fff', ls='dotted', label="Bharadwaj+ 2015 best-fit (no bias correction)", zorder=3)
        #Bhara_fit_SCC = lambda T: 5e43 * 10**0.3 * (np.asarray(T) / 3.0)**3.6
        #ax.errorbar([0.1, 20], (Bhara_fit_SCC([0.1, 20])), ls='dashed', label="Bharadwaj+ 2015 best-fit (SCC)", zorder=3)
        #Bhara_fit_NSCC = lambda T: 5e43 * 10**-0.17 * (np.asarray(T) / 3.0)**2.52
        #ax.errorbar([0.1, 20], (Bhara_fit_NSCC([0.1, 20])), ls='dashed', label="Bharadwaj+ 2015 best-fit (NSCC)", zorder=3)
        #Bhara_fit_CRS = lambda T: 5e43 * 10**0.16 * (np.asarray(T) / 3.0)**3.6
        #ax.errorbar([0.1, 20], (Bhara_fit_CRS([0.1, 20])), ls='dashed', label="Bharadwaj+ 2015 best-fit (CRS)", zorder=3)
        #Bhara_fit_NCRS = lambda T: 5e43 * 10**0.09 * (np.asarray(T) / 3.0)**3.2
        #ax.errorbar([0.1, 20], (Bhara_fit_NCRS([0.1, 20])), ls='dashed', label="Bharadwaj+ 2015 best-fit (NCRS)", zorder=3)
        
        ### [0.15 - 1] r500 for temp and lum
        Zou = np.genfromtxt(obs_dir+"ZouMaughan2016.csv", delimiter=",")
        Ez = np.sqrt((0.3*(1+Zou[:,1])**3 + 0.7))
        ax.errorbar(Zou[:,13], Zou[:,16]*1e43/Ez*(70.0*70.0/(H*H)), xerr=[abs(Zou[:,15]), Zou[:,14]], yerr=Zou[:,17]*1e43/Ez*(70.0*70.0/(H*H)), c=c, mec='none', marker='o', ls='none', ms=ms*1.1, capsize=cap, label="Zou+ 2016")
        
        ### [0.15 - 1] r500 for temp and lum
        rexcess = np.genfromtxt(obs_dir+"REXCESS_table_Pratt2009.csv", delimiter=",", dtype=None)
        Ez = np.sqrt((0.3*(1+rexcess['f1'])**3 + 0.7))
        ax.errorbar(rexcess['f10'], rexcess['f13'] * 1e44 * 70.0*70.0/(H*H) / Ez, xerr=[np.absolute(rexcess['f6']), rexcess['f5']], yerr=[np.absolute(rexcess['f9']) * 1e44 * 70.0*70.0/(H*H) / Ez, rexcess['f8'] * 1e44 * 70.0*70.0/(H*H) / Ez], c=c, mec='none', marker='s', ls='none', ms=ms*1.1, capsize=cap, label="Pratt+ 2009")
        
        #sun = np.genfromtxt(obs_dir+"Sun09_L-T.csv", delimiter=",")
        ## This is the 3D temperature so might be biased high compared to other studies
        #ax.errorbar(sun[:,5], sun[:,11]*1e44*(0.73/h)**2, xerr=[np.abs(sun[:,6]), sun[:,7]], yerr=[np.abs(sun[:,12])*1e44*(0.73/h)**2, sun[:,13]*1e44*(0.73/h)**2], c=c, mec=c, mew=1.4, mfc='white', zorder=0, marker='D', ls='none', ms=ms*0.9, capsize=cap*0.5, label="Sun+ 2009")

        
def M_T(ax, Tmin, Tmax, core_excised=False, XXL_aper=False, COSMOS_aper=False, c=None, c2=None, ms=6.5, h=0.679):
    if c is None: c = default_col
    if c2 is None: c2 = default_col2
    cap = ms*ms2cap ## cap size of error bars
    
    obs_dir = "/home/nh444/vault/ObsData/M-T/"
    
    if core_excised:
        ### Core excluded ###
        if COSMOS_aper: ##0.1-0.5 r500 temp, weak lensing masses
            COSMOS = np.genfromtxt(obs_dir+"COSMOS_Kettula2013.csv", delimiter=",") ## I don't really trust their R500 (M500) values coz they come from a scaling relation
            Ez = np.sqrt((0.28*(1+COSMOS[:,25])**3 + 0.72))
            ax.errorbar(COSMOS[:,16], COSMOS[:,1]*1e14*(0.7/h)*Ez, xerr=[abs(COSMOS[:,18]), COSMOS[:,17]], yerr=[abs(COSMOS[:,3])*1e14*(0.7/h)*Ez, COSMOS[:,2]*1e14*(0.7/h)*Ez], c=c2, mec=c2, mfc='none', marker='s', ls='none', ms=ms, capsize=cap, label="Kettula+ 2013")
            COSMOS_fit = lambda T: 14.0 + 0.39 + 1.71 * np.log10(T/3.0) + np.log10(0.7/h) ## = log( E(z) * M500 ) ## Kettula 2013 [0.1 - 0.5] r500
            ax.errorbar([Tmin, Tmax], [10**COSMOS_fit(Tmin), 10**COSMOS_fit(Tmax)], c=c2, ls='solid', dash_capstyle='round', lw=1.5, label="COSMOS")#, label="COSMOS M$_{500}$-T$_{\mathrm{X}}$ (Kettula+ 2013)")
            COSMOS_fit = lambda T: 14.0 + 0.34 + 1.48 * np.log10(T/3.0) + np.log10(0.7/h) ## = log( E(z) * M500 ) ## Kettula 2013 [0.1 - 0.5] r500
            ax.errorbar([Tmin, Tmax], [10**COSMOS_fit(Tmin), 10**COSMOS_fit(Tmax)], c=c2, ls='dashed', dash_capstyle='round', lw=1.5, label="COSMOS+CCCP+160SD")#, label="COSMOS M$_{500}$-T$_{\mathrm{X}}$ (Kettula+ 2013)")
        else:
            eck = np.genfromtxt(obs_dir+"Eckmiller2011-M500-T.csv", delimiter=",") ## X-ray hydrostatic masses, T with some core excised (Variable radius)
            Ez = np.sqrt((0.3*(1+eck[:,1])**3 + 0.7))
            ax.errorbar(eck[:,2], eck[:,17]*1e13*(0.7/h)*Ez, xerr=[abs(eck[:,4]), eck[:,3]], yerr=[abs(eck[:,19])*1e13*(0.7/h)*Ez, eck[:,18]*1e13*(0.7/h)*Ez], c=c, mec='none', marker='D', ls='none', ms=ms, capsize=cap, label="Eckmiller+ 2011")##d34044
            #eck_fit = lambda T: 1e14 * (0.7/h) * 10**(1.68 * np.log10(T / 3.0) + 0.15) ## M500 - T (no Ez)
            #ax.errorbar([Tmin, Tmax], [eck_fit(Tmin), eck_fit(Tmax)], c=c, ls='solid', lw=1.5, label="Eckmiller+ 2011 fit")
            
            sun = np.genfromtxt(obs_dir+"Sun2009_Mgas.txt") ## X-ray hydrostatic masses, [0.15-1] r500 temp
            Ez = np.sqrt((0.24*(1+sun[:,1])**3 + 0.76))
            ax.errorbar(sun[:,11], sun[:,2]*1e13*(0.73/h)*Ez, xerr=[abs(sun[:,13]), sun[:,12]], yerr=[abs(sun[:,4])*1e13*(0.73/h)*Ez, sun[:,3]*1e13*(0.73/h)*Ez], c=c, mec='none', marker='v', ls='none', ms=ms*1.4, capsize=cap, label="Sun+ 2009")
            #sun_fit = lambda T: 1.27 * 1e14 / h * (T / 3.0)**1.67 ## = E(z) * M500
            #ax.errorbar([Tmin, Tmax], [sun_fit(Tmin), sun_fit(Tmax)], c=c, lw=1.5, label="Sun+ 2009")
            
            Lov = np.genfromtxt(obs_dir+"Lovisari2015.csv", delimiter=",") ## X-ray hydrostatic masses, same core excision method as Eckmiller 2011
            Ez = np.sqrt((0.27*(1+Lov[:,1])**3 + 0.73))
            ax.errorbar(Lov[:,2], Lov[:,7]*1e13*0.7/h*Ez, xerr=Lov[:,3], yerr=Lov[:,8]*1e13*0.7/h*Ez, c=c, mec='none', marker="^", ls='none', ms=ms*1.4, capsize=cap, label="Lovisari+ 2015")
            #Lov_fit = lambda T: (1.71 * np.log10(T/2.0) + 0.2) + np.log10(5e13) + np.log10(0.7/h)
            #ax.errorbar([Tmin, Tmax], [10**Lov_fit(Tmin), 10**Lov_fit(Tmax)], c=c, ls='solid', lw=1.5, label="Lovisari+ 2015 groups+clusters")
            Lov_fit = lambda T: (1.65 * np.log10(T/2.0) + 0.19) + np.log10(5e13) + np.log10(0.7/h)
            ax.errorbar([Tmin, Tmax], [10**Lov_fit(Tmin), 10**Lov_fit(Tmax)], c=c, ls='solid', lw=1.5,)# label="Lovisari+ 2015 best-fit")
            
            #Zou = np.genfromtxt(obs_dir+"Zou2016.txt") ## I don't really trust their R500 (M500) values coz they come from a scaling relation
            #Ez = np.sqrt((0.3*(1+Zou[:,1])**3 + 0.7))
            #ax.errorbar(Zou[:,3], Zou[:,12]*1e13*(0.7/h)*Ez, xerr=[abs(Zou[:,5]), Zou[:,4]], c=c, mec='none', marker='s', ls='none', ms=ms, capsize=cap, label="Zou+ 2016")
            
            REX = np.genfromtxt(obs_dir+"REXCESS_M-T.csv", delimiter=",")
            Ez = np.sqrt((0.3*(1+REX[:,1])**3 + 0.7))
            ### Core excluded [0.15-1] r500
            ax.errorbar(REX[:,7], REX[:,10]*1e14*(0.7/h)*Ez, xerr=[abs(REX[:,9]), REX[:,8]], c=c, mec='none', marker='s', ls='none', ms=ms*1.1, capsize=cap, label="Pratt+ 2009")
            
            ### Could also compare to Mantz+2016 for WtG weak lensing masses and temp in 0.15-1 r500

    
    if not core_excised:### Core included ###
        if XXL_aper: ## 300 kpc aperture, although Giles+16 show there's no systematic difference with T inside r500
            XXL = np.genfromtxt(obs_dir+"XXL_Lieu2016.csv", delimiter=",") ## 0-300 kpc
            E = lambda z: (0.28*(1+z)**3 + 0.72)**0.5
            #uplims = np.isnan(XXL[:,10])
            uplims = (XXL[:,10]==0) ## points as upper limits
            XXL[uplims,11] *= 0.3 ## make the upper limit arrows smaller
            ax.errorbar(XXL[:,2], XXL[:,9]*1e14*0.7/h*E(XXL[:,1]), xerr=[np.absolute(XXL[:,4]), XXL[:,3]], yerr=[np.absolute(XXL[:,11]*1e14*0.7/h*E(XXL[:,1])), XXL[:,10]*1e14*0.7/h*E(XXL[:,1])], uplims=uplims, c=c2, mec=c2, mfc='none', marker="o", ls='none', ms=ms, mew=1.2, capsize=cap, label="Lieu+ 2016")#M$_{500}$-T$_{300\mathrm{kpc}}$
            #XXL2 = np.loadtxt(obs_dir+"XXL_Lieu2016_from_plot.txt") ## taken from plot rather than table in Lieu 2016
            #ax.errorbar(XXL2[:,0], np.log10(XXL2[:,1]*0.7/h), marker='o', ls='none')
            XXL_fit = lambda T: 13.56 + 1.78 * np.log10(T) + np.log10(0.7/h) ## = log(E(z) * M500)
            ax.errorbar([Tmin, Tmax], [10**XXL_fit(Tmin), 10**XXL_fit(Tmax)], c=c2, ls='solid', dash_capstyle='round', lw=1.5, label="XXL only")#, label="XXL M$_{500}$-T$_{300\mathrm{kpc}}$ fit")
            XXL_fit = lambda T: 13.57 + 1.67 * np.log10(T) + np.log10(0.7/h) ## = log(E(z) * M500)
            ax.errorbar([Tmin, Tmax], [10**XXL_fit(Tmin), 10**XXL_fit(Tmax)], c=c2, ls='dashed', dash_capstyle='round', lw=1.5, label="XXL+COSMOS+CCCP")#, label="XXL M$_{500}$-T$_{300\mathrm{kpc}}$ fit")
    
            ## COSMOS (10 groups) with T measured in same 300 kpc aperture, taken from fig 5 in Lieu+16
            #COS = np.genfromtxt(obs_dir+"Lieu2016_COSMOS_300kpc.csv", delimiter=",")
            #ax.errorbar(COS[:,0], COS[:,3]*0.7/h, xerr=[COS[:,2], COS[:,1]], yerr=[COS[:,5], COS[:,4]], c='r', mec=c2, mfc='none', marker="s", ls='none', ms=6, mew=1.2) ## M500 E(z) for h=0.7
            
            ## Mahdavi measures temperatures in r500 but Giles+2016 show there's no systematic difference compared to T in 300 kpc
            #Mah = np.genfromtxt(obs_dir+"Mahdavi2013.csv", delimiter=",")
            #Ez = np.sqrt((0.3*(1+Mah[:,1])**3 + 0.7))
            #ax.errorbar(Mah[:,5], Mah[:,11]*1e14*(0.7/h)*Ez, xerr=Mah[:,6], yerr=Mah[:,12]*1e14*(0.7/h)*Ez, c=c2, mec=c2, mfc='none', marker="o", ls='none', ms=6, mew=1.2, capsize=cap, label="Mahdavi+ 2013")
        else:
            Lov = np.genfromtxt(obs_dir+"Lovisari2015.csv", delimiter=",") ## X-ray hydrostatic masses, same core excision method as Eckmiller 2011
            Ez = np.sqrt((0.27*(1+Lov[:,1])**3 + 0.73))
            ax.errorbar(Lov[:,2], Lov[:,7]*1e13*0.7/h*Ez, xerr=Lov[:,3], yerr=Lov[:,8]*1e13*0.7/h*Ez, c=c, mec='none', marker="^", ls='none', ms=ms*1.4, capsize=cap, label="Lovisari+ 2015")
            #Lov_fit = lambda T: (1.65 * np.log10(T/2.0) + 0.19) + np.log10(5e13) + np.log10(0.7/h)
            #ax.errorbar([Tmin, Tmax], [10**Lov_fit(Tmin), 10**Lov_fit(Tmax)], c=c, ls='solid', lw=1.5,)# label="Lovisari+ 2015 best-fit")
            #Lov_fit = lambda T: (1.71 * np.log10(T/2.0) + 0.2) + np.log10(5e13) + np.log10(0.7/h)
            #ax.errorbar([Tmin, Tmax], [10**Lov_fit(Tmin), 10**Lov_fit(Tmax)], c=c, ls='dashed', lw=1.5)#, label="Lovisari+ 2015 groups+clusters")
            #b = 0.4
            #ax.errorbar([Tmin, Tmax], [10**Lov_fit(Tmin) / (1-b), 10**Lov_fit(Tmax) / (1-b)], c=c, ls='dashed', lw=1.5)
            
            ## Eckmiller+11 and Lovisari+15 exclude cores from some objects but the radius is variable
            ## and CEAGLE compare to them even though they don't excise the core
            eck = np.genfromtxt(obs_dir+"Eckmiller2011-M500-T.csv", delimiter=",") ## X-ray hydrostatic masses, T with some core excised (Variable radius)
            Ez = np.sqrt((0.3*(1+eck[:,1])**3 + 0.7))
            ax.errorbar(eck[:,2], eck[:,17]*1e13*(0.7/h)*Ez, xerr=[abs(eck[:,4]), eck[:,3]], yerr=[abs(eck[:,19])*1e13*(0.7/h)*Ez, eck[:,18]*1e13*(0.7/h)*Ez], c=c, mec='none', marker='D', ls='none', ms=ms, capsize=cap, label="Eckmiller+ 2011")##d34044
            #eck_fit = lambda T: 1e14 * (0.7/h) * 10**(1.68 * np.log10(T / 3.0) + 0.15) ## M500 - T (no Ez)
            #ax.errorbar([Tmin, Tmax], [eck_fit(Tmin), eck_fit(Tmax)], c=c, ls='solid', lw=1.5, label="Eckmiller+ 2011 fit")
            
            REX = np.genfromtxt(obs_dir+"REXCESS_M-T.csv", delimiter=",") ## their R500 (M500) values come from a scaling relation but they plot and fit L-M in Pratt 2009 anyway
            Ez = np.sqrt((0.3*(1+REX[:,1])**3 + 0.7))
            ### Core included [0-1] r500
            ax.errorbar(REX[:,4], REX[:,10]*1e14*(0.7/h)*Ez, xerr=[abs(REX[:,6]), REX[:,5]], c=c, mec='none', marker='s', ls='none', ms=ms*1.1, capsize=cap, label="Pratt+ 2009")
            
            ## These are massive clusters with lensing or hydro masses
            Mah = np.genfromtxt(obs_dir+"Mahdavi2013.csv", delimiter=",")
            Ez = np.sqrt((0.3*(1+Mah[:,1])**3 + 0.7))
            ## lensing masses:
            #ax.errorbar(Mah[:,5], Mah[:,11]*1e14*(0.7/h)*Ez, xerr=Mah[:,6], yerr=Mah[:,12]*1e14*(0.7/h)*Ez, c=c2, mec=c2, mfc='none', marker="o", ls='none', ms=6, mew=1.2, capsize=cap, label="Mahdavi+ 2013")
            ## hydro masses:
            uplims = (np.abs(Mah[:,19]) >= Mah[:,17]) ## where the -ve error is larger than the value (there's one at idx=35)
            Mah[uplims,19] = 1.2 ## sets size of arrow on upper limit
            ax.errorbar(Mah[:,5], Mah[:,17]*1e14*(0.7/h)*Ez, xerr=Mah[:,6], yerr=[np.abs(Mah[:,19]*1e14*(0.7/h)*Ez), Mah[:,18]*1e14*(0.7/h)*Ez], uplims=uplims, c=c, mfc=c, mec='none', marker="v", ls='none', ms=ms*1.4, capsize=cap, label="Mahdavi+ 2013")

    ###  The following don't necessarily plot E(z) times M500 ###
    
    #Ket_fit = lambda T: 14.0 + 0.34 + 1.48 * np.log10(T/3.0) + np.log10(0.7/h) ## = log( E(z) * M500 ) ## Kettula 2013
    #ax.errorbar([Tmin, Tmax], [Ket_fit(Tmin), Ket_fit(Tmax)], c=c, ls='dashdot', lw=1.5, label="COSMOS+CCCP+160SD2")# M$_{500}$-T$_{\mathrm{X}}$")
    
    # macsis_fit = lambda M: 10**0.68 * (M / 4e14)**0.58 ## assumes redshift zero
    # ax.plot([macsis_fit(10**logM500min), macsis_fit(10**logM500max)], [logM500min, logM500max], label="MACSIS + BAHAMAS best-fit (M$_{500,\mathrm{spec}}$-T$_{500,\mathrm{spec}}^{\mathrm{ce}}$)", lw=1.5, c='darkblue')
    # #ax.plot([macsis_fit(10**logM500min), macsis_fit(10**logM500max)], [logM500min-np.log10(0.65), logM500max-np.log10(0.65)], lw=1.5, c='darkblue', ls='dashed')
    # #ax.plot([macsis_fit(10**logM500min), macsis_fit(10**logM500max)], [logM500min-np.log10(0.8), logM500max-np.log10(0.8)], lw=1.5, c='darkblue', ls='dashed')
    
    # Planelles_fit_sl = lambda M: 5.02 * (M / (5e14/h))**0.54 ## assuming redshift zero
    # ax.plot([Planelles_fit_sl(10**logM500min), Planelles_fit_sl(10**logM500max)], [logM500min, logM500max], label=r"Planelles+ 2014 best-fit (M$_{500,\mathrm{true}}$-T$_{500,\mathrm{sl}}$)", lw=1.5, c='green')
    # Planelles_fit_mw = lambda M: 5.23 * (M / (5e14/h))**0.55 ## assuming redshift zero
    # ax.plot([Planelles_fit_mw(10**logM500min), Planelles_fit_mw(10**logM500max)], [logM500min, logM500max], label=r"Planelles+ 2014 best-fit (M$_{500,\mathrm{true}}$-T$_{500,\mathrm{mw}}$)", lw=1.5, c='green', ls='dashed')
    
    # Truong_fit_sl = lambda T: 14.298 + 1.675 * np.log10(T/3.0)## = log(E(z) * M500) 
    # ax.plot([Tmin, Tmax], [Truong_fit_sl(Tmin), Truong_fit_sl(Tmax)], label=r"Truong+ 2016 best-fit (M$_{500,\mathrm{true}}$-T$_{500,\mathrm{sl}}$)", lw=1.5, c='orange')
    # Truong_fit_mw = lambda T: 14.301 + 1.603 * np.log10(T/3.0)## = log(E(z) * M500) 
    # ax.plot([Tmin, Tmax], [Truong_fit_mw(Tmin), Truong_fit_mw(Tmax)], label=r"Truong+ 2016 best-fit (M$_{500,\mathrm{true}}$-T$_{500,\mathrm{mw}}$)", lw=1.5, c='orange', ls='dashed')
    
    # OWLS_fit = lambda M: 10**0.28 * (M / 1e14)**0.577 ## assuming redshift zero
    # ax.plot([OWLS_fit(10**logM500min), OWLS_fit(10**logM500max)], [logM500min, logM500max], label=r"cosmo-OWLS best-fit (M$_{500,\mathrm{true}}$-T$_{500,\mathrm{spec}}^{\mathrm{ce}}$)", lw=1.5, c='darkred')
    
    # MUSIC_fit = lambda M: 1.11096e-09 * M**0.65 ## E(z) M500 BCES Orthogonal
    # #MUSIC_fit = lambda M: 1.045069e-10 * M**0.72 ## E(z) M500 BCES Bisector
    # ## BCES orthogonal: intercept 1.11096e-09 and slope 0.65
    # ## BCES bisector: intercept 1.045069e-10 and slope 0.72
    # ax.plot([MUSIC_fit(10**logM500min), MUSIC_fit(10**logM500max)], [logM500min, logM500max], lw=1.5, c='pink', ls='dotted', dash_capstyle='round')
    # ax.plot([MUSIC_fit(1e14), MUSIC_fit(10**logM500max)], [np.log10(1e14), logM500max], label=r"MUSIC best-fit (M$_{500,\mathrm{true}}$-T$_{500,\mathrm{spec}}$)", lw=1.5, c='pink')
    
    # Fabjan_fit_mw = lambda T: 14.427 + 1.615 * np.log10(T/3.0)## = log(E(z) * M500) 
    # ax.plot([Tmin, Tmax], [Fabjan_fit_mw(Tmin), Fabjan_fit_mw(Tmax)], label=r"Fabjan+ 2011 CSF-M-W (M$_{500,\mathrm{true}}$-T$_{500,\mathrm{mw}}^{\mathrm{ce}}$)", lw=1.5, c='black', ls='solid')
    
    #vikh_fit = lambda T: 2.89e14 / h * (T / 5)**1.58
    #ax.plot([Tmin, Tmax], [np.log10(vikh_fit(Tmin)), np.log10(vikh_fit(Tmax))], c=c, ls='dashdot', lw=1.5, label="Vikhlinin+ 2006")
    