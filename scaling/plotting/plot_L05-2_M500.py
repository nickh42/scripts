import numpy as np
import matplotlib.pyplot as plt
default_col = '0.7'
default_col2 = '0.4'
ms2cap = 0.6

def main():
    outdir = "/home/nh444/Documents/paper/"
    basedir="/home/nh444/data/project1/L-T_relations/"
    snapnum = 25
    h = 0.679
    idxScat = []
    idxMed = []
    
    simdirs = ["L40_512_LDRIntRadioEffBHVel", "L40_512_MDRIntRadioEff"]
    labels = ["$\delta t = 10$ Myr", "$\delta t = 25$ Myr"]
    simdirs = ["L40_512_MDRIntRadioEff"]
    labels = [None]
    outname = "L05-2-M500_relation.pdf"
    idxScat = range(len(simdirs))
    idxMed = []
    zoomdirs = [#"","","","",
            "/data/curie4/nh444/project1/L-T_relations/LDRIntRadioEffBHVel/",
            "/data/curie4/nh444/project1/L-T_relations/MDRInt/"] ## empty string if no zooms
    zooms = [#["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c192_box_Arepo_new", "c256_box_Arepo_new", "c320_box_Arepo_new", "c384_box_Arepo_new", "c448_box_Arepo_new"],
             ["c96_box_Arepo_new", "c128_box_Arepo_new", "c192_box_Arepo_new", "c256_box_Arepo_new", "c384_box_Arepo_new"],
             ["c96_MDRInt", "c128_MDRInt", "c192_MDRInt", "c256_MDRInt", "c384_MDRInt"]]
    filename = "L-T_data_apec_0.5-10_L0.5-2.0_default_Tvir_thresh_4.0.txt"
    
    figsize=(7,7)
    #from palettable.colorbrewer.qualitative import Dark2_8 as pallette
    #col=pallette.hex_colors
    #col = ["#c7673e","#78a44f","#6b87c8","#934fce","#e6ab02","#be558f","#47af8d"]
    col = ['#24439b', '#ED7600'] ## blue orange
    markers = ["D", "s", "^", "o"]
    ls=['dashdot', 'dotted', 'dashed', 'solid']
    ms=8
    lw=3
    mew=1.8
    binwidth = 0.2 ## dex for mass in Msun
    
    plot_L052_M500(simdirs, snapnum, filename, projected=False, labels=labels, zoomdirs=zoomdirs, zooms=zooms, h=h, basedir=basedir, outname=outname, outdir=outdir, figsize=figsize, idxScat=idxScat, idxMed=idxMed, col=col, markers=markers, ls=ls, ms=ms, lw=lw, mew=mew, binwidth=binwidth)

def plot_L052_M500(simdirs, snapnum, filename, labels=None, zoomdirs=None, zooms=[], projected=False, h=0.679, basedir="/home/nh444/data/project1/L-T_relations/", outname="Lsoft-M500_relation.pdf", outdir="/home/nh444/data/project1/L-T_relations/", figsize=(7,7), idxScat=[], idxMed=[], col=None, markers=None, ls=None, ms=6.5, lw=2, mew=1.2, binwidth=0.2):
    import matplotlib.pyplot as plt
    import numpy as np
    import scipy.stats as stats
    
    assert len(simdirs) > 0, "simdirs is an empty list"
    
    if projected:
        lum_idx = 25 ## 1 for 3D luminosity, 25 for projected
    else:
        lum_idx = 1
    
    if col is None:
        from palettable.colorbrewer.qualitative import Dark2_8 as pallette
        col=pallette.hex_colors
    if markers is None:
        markers = ["D", "s", "o"]
    if ls is None:
        ls=['dashdot', 'dotted', 'dashed', 'solid']
    if zoomdirs is None:
        zoomdirs = [""]*len(simdirs)
    
    fig, ax = plt.subplots(figsize=figsize)
    L052_M500(ax, h=h, ms=ms)
    
    ## Median profiles
    xmin, xmax = [11, 13.5]
    num = int((xmax - xmin) / binwidth)
    bins = np.logspace(xmin, xmax, num=num)
    x = 10**((np.log10(bins[:-1]) + np.log10(bins[1:]))/2)
    
    for simidx, simdir in enumerate(simdirs):
        data = np.loadtxt(basedir+simdirs[simidx]+"_"+str(snapnum).zfill(3)+"/"+filename)
        label = labels[simidx]
        
        if zoomdirs[simidx] is not "":
            for zoom in zooms[simidx]:
                zdata = np.loadtxt(zoomdirs[simidx]+zoom+"_"+str(snapnum).zfill(3)+"/"+filename)
                data = np.vstack((data, zdata))
                #ax.plot(data[12] * 1e10 / h, data[lum_idx], marker=markers[0], mfc='none', mec=col[0], mew=mew, ms=ms, ls='none')

        if simidx in idxMed:
            med, bin_edges, n = stats.binned_statistic(data[:,12] * 1e10 / h, data[:,lum_idx], statistic='median', bins=bins)
            ax.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[simidx], ls=ls[simidx], lw=lw, dash_capstyle='round', label=label)
            label = None ## don't want two legend entries
        if simidx in idxScat:
            ax.plot(data[:,12] * 1e10 / h, data[:,lum_idx], marker=markers[simidx], mfc='none', mec=col[simidx], ms=ms, ls='none', mew=mew, label=label)
    
    ax.set_xlim(3e12, 1e15)
    ax.set_ylim(1e40,1e45)
    ax.set_xscale('log')
    ax.set_yscale('log')
    ax.set_ylabel("L$_{500}^{0.5-2.0 \mathrm{keV}}$ [erg s$^{-1}$]")
    ax.set_xlabel("M$_{500}$ [$M_{\odot}$]")#, labelpad=10)#, fontsize=16)
    ax.tick_params(axis='x', which='major', pad=7)
    
    from scaling.plotting.plotObsDat import add_legends
    add_legends(ax, simdirs, labels, col=col, nfiles=1, pad=0.5, fontsize='small', frameon=False)

    #from scaling.plotting.plotObsDat import add_legends
    #add_legends(ax, simdirs, labels, pad=0.8)
    #n = (len(simdirs) - sum(label is None for label in labels)) ## no. of sims plotted
#==============================================================================
#     n = -2 ## puts last two legend labels at lower right
#     handles, labels = ax.axes.get_legend_handles_labels()
#     leg1 = ax.legend(handles[:n], labels[:n], loc='upper left', frameon=False, borderpad=0.4, borderaxespad=1, numpoints=1, ncol=1)
#     ax.add_artist(leg1)
#     ax.legend(handles[n:], labels[n:], loc='lower right', frameon=False, borderaxespad=0.8, numpoints=1, ncol=1)
#     
#==============================================================================
    plt.tight_layout(pad=0.45)
    fig.savefig(outdir+outname)#, bbox_inches='tight')
    print "Saved to", outdir+outname
    
def L052_M500(ax, c=None, c2=None, ms=6.5, h=0.679):
    if c is None: c = default_col
    if c2 is None: c2 = default_col2
    cap = ms*ms2cap ## cap size of error bars
    
    obs_dir = "/home/nh444/vault/ObsData/L-M/"
    
    LBGs = np.genfromtxt(obs_dir+"Anderson2014_lum.csv", delimiter=",")
    
    ## bootstrap errors
#    plt.errorbar(10**LBGs[:,2]*(0.704/h),
#                10**LBGs[:,11]*(0.704**2 / h**2),
#                yerr=[(10**LBGs[:,11] - 10**(LBGs[:,11] - LBGs[:,13]))*(0.704**2 / h**2), (10**(LBGs[:,11] + LBGs[:,13]) - 10**LBGs[:,11])*(0.704**2 / h**2)],
#                capsize=cap, c=c, mfc=c, mec='none', marker='o', ls='none',
#                ms=ms*1.4, lw=1.5, label="Anderson+ 2015")
    
#==============================================================================
#     ## Multiply amplitude by 40% to approximate relation from Wang+16 recalibrated by WL
#     plt.errorbar(10**LBGs[:,2]*(0.704/h),
#                 10**LBGs[:,11]*(0.704**2 / h**2)*1.4,
#                 yerr=[(10**LBGs[:,11] - 10**(LBGs[:,11] - LBGs[:,13]))*(0.704**2 / h**2)*1.4, (10**(LBGs[:,11] + LBGs[:,13]) - 10**LBGs[:,11])*(0.704**2 / h**2)*1.4],
#                 capsize=cap, c=c2, mfc=c2, mec='none', marker='o', ls='none',
#                 ms=ms*1.4, lw=1.5, label="$\sim$Wang+ 2016")
#==============================================================================
    
    plt.errorbar(10**LBGs[:,17]*(0.704/h),
                10**LBGs[:,11]*(0.704**2 / h**2),
                xerr=[(10**(LBGs[:,17])-10**(LBGs[:,17]-LBGs[:,18]))*(0.704/h), (10**(LBGs[:,17]+LBGs[:,18])-10**(LBGs[:,17]))*(0.704/h)],
                yerr=[(10**LBGs[:,11] - 10**(LBGs[:,11] - LBGs[:,13]))*(0.704**2 / h**2), (10**(LBGs[:,11] + LBGs[:,13]) - 10**LBGs[:,11])*(0.704**2 / h**2)],
                capsize=cap, c=c2, mfc=c2, mec='none', marker='o', ls='none',
                ms=ms*1.4, lw=1.5, label="Anderson+ 2015\nWang+ 2016")
    
    And = np.genfromtxt(obs_dir+"Andreon_2016.csv", delimiter=",")
    plt.errorbar(10**And[:,4]*(0.7/h), 10**And[:,12]*(0.7/h)**2.0, xerr=[(10**And[:,4]-10**(And[:,4]-And[:,5]))*(0.7/h), (10**(And[:,4]+And[:,5])-10**And[:,4])*(0.7/h)], yerr=[(10**And[:,12]-10**(And[:,12]-And[:,13]))*(0.7/h)**2.0, (10**(And[:,12]+And[:,13])-10**And[:,12])*(0.7/h)**2.0], capsize=cap, c=c, mfc=c, mec='none', marker='D', ls='none', ms=ms, label="Andreon+ 2016 (caustic)")
    plt.errorbar(10**And[:,7]*(0.7/h), 10**And[:,12]*(0.7/h)**2.0, xerr=[(10**And[:,7]-10**(And[:,7]-And[:,8]))*(0.7/h), (10**(And[:,7]+And[:,8])-10**And[:,7])*(0.7/h)], yerr=[(10**And[:,12]-10**(And[:,12]-And[:,13]))*(0.7/h)**2.0, (10**(And[:,12]+And[:,13])-10**And[:,12])*(0.7/h)**2.0], capsize=cap, c=c, mec=c, mfc='none', marker='D', ls='none', ms=ms, label="Andreon+ 2016 (vel. disp.)")

if __name__ == "__main__":
    main()
