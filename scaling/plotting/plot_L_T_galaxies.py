def main():
    outdir = "/home/nh444/data/project1/L-T_relations/"
    basedir="/home/nh444/data/project1/L-T_relations/"  
        
    simdirs = ["L40_512_MDRIntRadioEff",
               ]
    filenames = ["L-T_data_apec_0.5-10_5halfmassrad_default_Tvir_thresh_4.0.txt",
                 "L-T_data_apec_0.5-10_T50kpc_default_Tvir_thresh_4.0.txt",
                 #"L-T_data_apec_0.5-10_default_Tvir_thresh_4.0.txt",
                 ]
    labels = ["$<5 r_{\star, 0.5}$", "$<50$ kpc", "$<r_{500}$"]

    idxScat = range(len(simdirs)*len(filenames))
    idxMed = []
    zoomdirs = ["/data/curie4/nh444/project1/L-T_relations/MDRInt/"]
    zooms = [[]]#[["c96_MDRInt", "c128_MDRInt", "c192_MDRInt", "c256_MDRInt", "c384_MDRInt", "c448_MDRInt"]]
       
    snapnum = 25
    h = 0.679

    figsize=(7,7)
    col = ["#c7673e","#78a44f","#6b87c8","#934fce","#e6ab02","#be558f","#47af8d"]
    mec = col#['none']*len(simdirs)*len(filenames)
    mfc = ['none']*len(simdirs)*len(filenames)
    ls=['solid']*len(simdirs)*len(filenames)
    markers = ['D', 's', 'o', '^', '>', 'v', '<']
    ms=8
    lw=3
    mew=1.8
    binwidth = 0.2 ## dex for mass in Msun 

    binwidth = 0.1
    
    projected = False
    for_paper = False
    frameon = True
    outname = "L-T_gal_MDRInt.pdf"
    #plot_L_T(simdirs, snapnum, filenames, projected=projected, for_paper=for_paper, frameon=frameon, labels=labels, zoomdirs=zoomdirs, zooms=zooms, h=h, basedir=basedir, outname=outname, outdir=outdir, figsize=figsize, idxScat=idxScat, idxMed=idxMed, col=col, mec=mec, mfc=mfc, markers=markers, ls=ls, ms=ms, lw=lw, mew=mew, binwidth=binwidth)
    outname = "L-Mgas_gal_MDRInt.pdf"
    plot_L_Mgas(simdirs, snapnum, filenames, projected=projected, for_paper=for_paper, frameon=frameon, labels=labels, zoomdirs=zoomdirs, zooms=zooms, h=h, basedir=basedir, outname=outname, outdir=outdir, figsize=figsize, idxScat=idxScat, idxMed=idxMed, col=col, mec=mec, mfc=mfc, markers=markers, ls=ls, ms=ms, lw=lw, mew=mew, binwidth=binwidth)

def plot_L_T(simdirs, snapnum, filenames, projected=True, h=0.679,
             basedir="/home/nh444/data/project1/L-T_relations/",
             zoomdirs=None, zooms=[],
             figsize=(7,7), idxScat=[], idxMed=[], indArrow=[],
             labels=None, frameon=False, for_paper=True,
             outname="L-T_relation.pdf", outdir="/home/nh444/data/project1/L-T_relations/",
             col=None, mec=None, mfc=None, markers=None, mew=1.2, ms=6.5,
             ls=None, lw=2, binwidth=0.1):
    import matplotlib.pyplot as plt
    import matplotlib.ticker as ticker
    import numpy as np
    import scipy.stats as stats
    
    assert len(simdirs) > 0, "simdirs is an empty list"
    if not isinstance(filenames, (list, tuple)):## if not a list, convert to list
        filenames = [filenames]
    
    if projected:
        temp_idx = 5 ## temperature index - 5 for spectral projected, 22 for spectral 3D, 14 for mass-weighted projected, 15 for em-weighted projected
        lum_idx = 25 ## 1 for 3D luminosity, 25 for projected
    else: ## 3-D
        temp_idx = 22 ## temperature index - 5 for spectral projected, 22 for spectral 3D, 14 for mass-weighted projected, 15 for em-weighted projected
        lum_idx = 1 ## 1 for 3D luminosity, 25 for projected
    
    if col is None:
        from palettable.colorbrewer.qualitative import Dark2_8 as pallette
        col=pallette.hex_colors
    if mec is None:
        mec = col
    if mfc is None:
        mfc = ['none']*len(simdirs)*len(filenames)
    if markers is None:
        markers = ['D', 's', 'o', '^', '>', 'v', '<']
    if ls is None:
        ls=['dashdot', 'dotted', 'dashed', 'solid']
    if zoomdirs is None:
        zoomdirs = [""]*len(simdirs)
    
    fig = plt.figure(figsize=figsize)
    
    ## Observational data
    bab = np.genfromtxt("/data/vault/nh444/ObsData/L-T/"+"Babyk_2018.csv", delimiter=",")
    ind = np.where((bab[:,6] < 0.5) & (bab[:,7] < 0.5))[0] ## not BCG or cD
    plt.errorbar(bab[ind,14], bab[ind,18]*1e40*(0.7/h)**2, xerr=bab[ind,15], yerr=bab[ind,19]*1e40*(0.7/h)**2, c='0.7', mec='none', marker='o', ls='none', ms=ms, label="Babyk+ 2018")
    ind = np.where(bab[:,6] > 0.0)[0] ## BCGs
    plt.errorbar(bab[ind,14], bab[ind,18]*1e40*(0.7/h)**2, xerr=bab[ind,15], yerr=bab[ind,19]*1e40*(0.7/h)**2, c='0.3', mec='none', marker='D', ls='none', ms=ms, label="BCGs")
    ind = np.where(bab[:,7] > 0.0)[0] ## cDs
    plt.errorbar(bab[ind,14], bab[ind,18]*1e40*(0.7/h)**2, xerr=bab[ind,15], yerr=bab[ind,19]*1e40*(0.7/h)**2, c='darkblue', mec='none', marker='D', ls='none', ms=ms, label="cD")

    ## Median profiles
    xmin, xmax = [np.log10(0.16), np.log10(1.7)]
    num = int((xmax - xmin) / binwidth)
    bins = np.logspace(xmin, xmax, num=num)
    x = 10**((np.log10(bins[:-1]) + np.log10(bins[1:]))/2)
    
    for simidx, simdir in enumerate(simdirs):
        for fileidx, filename in enumerate(filenames):
            idx = fileidx + simidx*(len(filenames))
            data = np.loadtxt(basedir+simdirs[simidx]+"_"+str(snapnum).zfill(3)+"/"+filename)
            label = labels[idx]
            
            if zoomdirs[simidx] is not "":
                for zoom in zooms[simidx]:
                    zdata = np.loadtxt(zoomdirs[simidx]+zoom+"_"+str(snapnum).zfill(3)+"/"+filename)
                    data = np.vstack((data, zdata))
                    #plt.plot(data[temp_idx], data[lum_idx], marker=markers[fileidx], mec=mec[fileidx], mfc=mfc[fileidx], mew=mew, ms=ms, ls='none')
            
            if idx in idxMed:
                med, bin_edges, n = stats.binned_statistic(data[:,temp_idx], data[:,lum_idx], statistic='median', bins=bins)
                plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[idx], ls=ls[idx], lw=lw, dash_capstyle='round', label=label)
                label = None ## don't want two legend entries
            if idx in idxScat:
                plt.plot(data[:,temp_idx], data[:,lum_idx], marker=markers[idx], mfc=mfc[idx], mec=mec[idx], ms=ms, ls='none', mew=mew, label=label)
                if len(indArrow) > 0:
                    if fileidx == 0:
                        data1 = data
                    else:
                        for i in indArrow:
                            #plt.gca().arrow(data1[i,temp_idx], data1[i,lum_idx], data[i,temp_idx]-data1[i,temp_idx], data[i,lum_idx]-data1[i,lum_idx], width=1.0, zorder=4)
                            plt.gca().annotate("",
                            xy=(data1[i,temp_idx], data1[i,lum_idx]), xycoords='data',
                            xytext=(data[i,temp_idx], data[i,lum_idx]), textcoords='data',
                            arrowprops=dict(fc=col[idx], ec=None, #arrowstyle='->',
                            width=0.5, headwidth=6, headlength=5, shrink=0.07, fill=True))##width, headwidth, headlength are in pts
                            #connectionstyle="arc3"))
   
    plt.xlim(0.1, 4)
    plt.ylim(1e38,1e44)
    plt.xscale('log')
    plt.yscale('log')
    plt.ylabel("Luminosity [erg s$^{-1}$]")
    plt.xlabel("kT [keV]")
    
    from scaling.plotting.plotObsDat import add_legends
    add_legends(plt.gca(), simdirs, labels, col=col, nfiles=len(filenames), frameon=frameon)
    
    plt.gca().xaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))
    
    plt.tight_layout(pad=0.45)
    fig.savefig(outdir+outname)#, bbox_inches='tight')
    print "Saved to", outdir+outname
    
def plot_L_Mgas(simdirs, snapnum, filenames, projected=True, h=0.679,
             basedir="/home/nh444/data/project1/L-T_relations/",
             zoomdirs=None, zooms=[],
             figsize=(7,7), idxScat=[], idxMed=[],
             labels=None, frameon=False, for_paper=True,
             outname="L-T_relation.pdf", outdir="/home/nh444/data/project1/L-T_relations/",
             col=None, mec=None, mfc=None, markers=None, mew=1.2, ms=6.5,
             ls=None, lw=2, binwidth=0.1):
    import matplotlib.pyplot as plt
    import numpy as np
    import scipy.stats as stats
    
    assert len(simdirs) > 0, "simdirs is an empty list"
    if not isinstance(filenames, (list, tuple)):## if not a list, convert to list
        filenames = [filenames]
    
    if projected:
        lum_idx = 25 ## 1 for 3D luminosity, 25 for projected
    else: ## 3-D
        lum_idx = 1 ## 1 for 3D luminosity, 25 for projected
    
    if col is None:
        from palettable.colorbrewer.qualitative import Dark2_8 as pallette
        col=pallette.hex_colors
    if mec is None:
        mec = col
    if mfc is None:
        mfc = ['none']*len(simdirs)*len(filenames)
    if markers is None:
        markers = ['D', 's', 'o', '^', '>', 'v', '<']
    if ls is None:
        ls=['dashdot', 'dotted', 'dashed', 'solid']
    if zoomdirs is None:
        zoomdirs = [""]*len(simdirs)
    
    fig = plt.figure(figsize=figsize)
    
    ## Observational data
    #from scaling.plotting.plotObsDat import L_Mgas
    #L_Mgas(plt.gca(), ms=ms, h=h)
    bab = np.genfromtxt("/data/vault/nh444/ObsData/L-T/"+"Babyk_2018.csv", delimiter=",")
    ind = np.where((bab[:,6] < 0.5) & (bab[:,7] < 0.5))[0] ## not BCG or cD
    plt.errorbar(bab[ind,35]*1e11*0.7/h, bab[ind,18]*1e40*(0.7/h)**2, xerr=bab[ind,36]*1e11*0.7/h, yerr=bab[ind,19]*1e40*(0.7/h)**2, c='0.7', mec='none', marker='o', ls='none', ms=ms, label="Babyk+ 2018")
    ind = np.where(bab[:,6] > 0.0)[0] ## BCGs
    plt.errorbar(bab[ind,35]*1e11*0.7/h, bab[ind,18]*1e40*(0.7/h)**2, xerr=bab[ind,36]*1e11*0.7/h, yerr=bab[ind,19]*1e40*(0.7/h)**2, c='0.3', mec='none', marker='D', ls='none', ms=ms, label="BCGs")
    ind = np.where(bab[:,7] > 0.0)[0] ## cDs
    plt.errorbar(bab[ind,35]*1e11*0.7/h, bab[ind,18]*1e40*(0.7/h)**2, xerr=bab[ind,36]*1e11*0.7/h, yerr=bab[ind,19]*1e40*(0.7/h)**2, c='darkblue', mec='none', marker='D', ls='none', ms=ms, label="cD")

    ## Median profiles
    xmin, xmax = [np.log10(0.16), np.log10(1.7)]
    num = int((xmax - xmin) / binwidth)
    bins = np.logspace(xmin, xmax, num=num)
    x = 10**((np.log10(bins[:-1]) + np.log10(bins[1:]))/2)
    
    for simidx, simdir in enumerate(simdirs):
        for fileidx, filename in enumerate(filenames):
            idx = fileidx + simidx*(len(filenames))
            data = np.loadtxt(basedir+simdirs[simidx]+"_"+str(snapnum).zfill(3)+"/"+filename)
            label = labels[idx]
            
            if zoomdirs[simidx] is not "":
                for zoom in zooms[simidx]:
                    zdata = np.loadtxt(zoomdirs[simidx]+zoom+"_"+str(snapnum).zfill(3)+"/"+filename)
                    data = np.vstack((data, zdata))
                    #plt.plot(data[13] * 1e10 / h, data[lum_idx], marker=markers[fileidx], mec=mec[fileidx], mfc=mfc[fileidx], mew=mew, ms=ms, ls='none')
            
            if idx in idxMed:
                med, bin_edges, n = stats.binned_statistic(data[:,13] * 1e10 / h, data[:,lum_idx], statistic='median', bins=bins)
                plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[idx], ls=ls[idx], lw=lw, dash_capstyle='round', label=label)
                label = None ## don't want two legend entries
            if idx in idxScat:
                plt.plot(data[:,13] * 1e10 / h, data[:,lum_idx], marker=markers[idx], mfc=mfc[idx], mec=mec[idx], ms=ms, ls='none', mew=mew, label=label)
    
    plt.xlim(5e8, 1e13)
    plt.ylim(1e38,1e44)
    plt.xscale('log')
    plt.yscale('log')
    plt.ylabel("Luminosity [erg s$^{-1}$]")
    plt.xlabel("Gas mass [$M_{\odot}$]")
    
    from scaling.plotting.plotObsDat import add_legends
    add_legends(plt.gca(), simdirs, labels, col=col, nfiles=len(filenames), frameon=frameon)
       
    plt.tight_layout(pad=0.45)
    fig.savefig(outdir+outname)#, bbox_inches='tight')
    print "Saved to", outdir+outname

if __name__ == "__main__":
    main()