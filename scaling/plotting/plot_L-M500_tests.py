import matplotlib.pyplot as plt
#import matplotlib.ticker as ticker
import numpy as np
import scipy.stats as stats
from os import chdir
chdir('/home/nh444/data/project1/L-T_relations')

outname = "L-M500_relation_paper.pdf"

snapnum = 25
h = 0.679
filename = "L-T_data_apec_0.5-2_Rasia_c4.txt"
filename = "L-T_data_apec_0.5-10_Rasia_c4_Tvir_thresh_2.5.txt"

obs_dir = "/home/nh444/vault/ObsData/L-M/"

fig = plt.figure(figsize=(7,7))
Mmin, Mmax = 10**12.5, 10**15

labels = ["Ref",
          "Stronger radio",
          "Quasar duty cycle",
          "Best"]

from palettable.colorbrewer.qualitative import Dark2_8 as pallette
col=pallette.hex_colors
markers = ["D", "s", "o"]
ls=['dashdot', 'dotted', 'dashed', 'solid']
ms = 6.5
lw=2
mew=1.2

## Median profiles
bins = np.logspace(11, 13.5, num=25)
x = 10**((np.log10(bins[:-1]) + np.log10(bins[1:]))/2.0)

data = np.loadtxt("L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_"+str(snapnum).zfill(3)+"/"+filename)
plt.plot(data[:,12] * 1e10 / h, data[:,1], marker=markers[2], mfc='none', mec=col[3], ms=ms, linestyle='none', mew=mew, zorder=4)
## median is plotted below
for zoom in ["c96", "c128_fixed", "c160", "c192", "c256_fixed", "c320", "c384_fixed", "c448"]:
    data = np.loadtxt("/home/nh444/data/project1/L-T_relations/LDRIntRadioEffBHVel/"+zoom+"_"+str(snapnum).zfill(3)+"/"+filename)
    plt.plot(data[12] * 1e10 / h, data[1], marker='o', mfc='none', mec=col[3], mew=mew, ms=ms, linestyle='none', zorder=4)

data = np.loadtxt("L40_512_NoDutyRadioWeak_HalfThermal_FixEta_"+str(snapnum).zfill(3)+"/"+filename)#0.3-10_fit_range
#plt.plot(data[:,12] * 1e10 / h, data[:,1], marker=markers[0], mec='none', mfc=col[0], ms=ms, linestyle='none', mew=mew, zorder=4)
med, bin_edges, n = stats.binned_statistic(data[:,12] * 1e10 / h, data[:,1], statistic='median', bins=bins)
plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[0], ls=ls[2], lw=lw, dash_capstyle='round', label=labels[0], zorder=4)

data = np.loadtxt("L40_512_NoDutyRadioInt_HalfThermal_FixEta_"+str(snapnum).zfill(3)+"/"+filename)#0.3-10_fit_range
#plt.plot(data[:,12] * 1e10 / h, data[:,1], marker=markers[0], mec='none', mfc=col[1], ms=ms, linestyle='none', mew=mew, zorder=4)
med, bin_edges, n = stats.binned_statistic(data[:,12] * 1e10 / h, data[:,1], statistic='median', bins=bins)
plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[1], ls=ls[1], lw=lw, dash_capstyle='round', label=labels[1], zorder=4)

data = np.loadtxt("L40_512_LowDutyRadioWeak_HalfThermal_FixEta_"+str(snapnum).zfill(3)+"/"+filename)#0.3-10_fit_range
#plt.plot(data[:,12] * 1e10 / h, data[:,1], marker=markers[1], mec='none', mfc=col[2], ms=ms, linestyle='none', mew=mew, zorder=4)
med, bin_edges, n = stats.binned_statistic(data[:,12] * 1e10 / h, data[:,1], statistic='median', bins=bins)
plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[2], ls=ls[0], lw=lw, dash_capstyle='round', label=labels[2], zorder=4)

#data = np.loadtxt("L40_512_LowDutyRadioWeak_HalfThermal_FixEta_BHChangeVel_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_default2.txt")#0.3-10_fit_range
#plt.plot(data[:,12] * 1e10 / h, data[:,1], marker='D', mfc='none', mec=col[2], ms=4, linestyle='none', mew=mew)
#med, bin_edges, n = stats.binned_statistic(data[:,12] * 1e10 / h, data[:,1], statistic='median', bins=bins)
#plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[2], lw=lw, label=r"QM duty cycle ($\delta t = 0.02$)")

data = np.loadtxt("L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_"+str(snapnum).zfill(3)+"/"+filename)#0.3-10_fit_range
med, bin_edges, n = stats.binned_statistic(data[:,12] * 1e10 / h, data[:,1], statistic='median', bins=bins)
plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[3], ls=ls[3], lw=lw, dash_capstyle='round', label=labels[3], zorder=4)#(APEC $T_{min}=0.01$ keV)

c = '0.7'

Zou = np.genfromtxt(obs_dir+"ZouMaughan2016.csv", delimiter=",")
## Note that these masses were derived from a scaling relation (YX-M500 from Vikhlinin 2009) - see Zou 2016 paper
Ez = np.sqrt((0.3*(1+Zou[:,1])**3 + 0.7))
plt.errorbar(Zou[:,12]*1e13*0.7/h, Zou[:,6]*1e43/Ez*(0.7*0.7/(h*h)), yerr=Zou[:,7]*1e43/Ez*(0.7*0.7/(h*h)), c=c, mec='none', marker='o', linestyle='none', ms=5, label="Zou+ 2016")
bias = 0.7
#plt.errorbar(Zou[:,12]*1e13*0.7/h / bias, Zou[:,6]*1e43)+np.log10(0.7*0.7/(h*h), yerr=Zou[:,7], c='blue', mec='blue', mfc='none', marker='o', linestyle='none', ms=5, label="Zou+ 2016 (1-b)={:.1f}".format(bias))

REX = np.genfromtxt(obs_dir+"REXCESS_L-M.csv", delimiter=",")
Ez = np.sqrt((0.3*(1+REX[:,1])**3 + 0.7))
plt.errorbar(REX[:,5]*1e14*(Ez*0.7/h), REX[:,7]*1e44/Ez*(0.7*0.7/(h*h)), xerr=REX[:,6]*1e14*(Ez*0.7/h), yerr=[abs(REX[:,9])*1e44/Ez*(0.7*0.7/(h*h)), REX[:,8]*1e44/Ez*(0.7*0.7/(h*h))], c=c, mec='none', marker='s', ls='none', ms=4, label="Pratt+ 2009")

#Osmond = np.genfromtxt(obs_dir+"Osmond_Ponman_2004.csv", delimiter=",")
#mask = (Osmond[:,17] > 60)## G-sample
#Ez = np.sqrt((0.3*(1+Osmond[mask,1]/3e5)**3 + 0.7))
#plt.errorbar(Osmond[mask,3]*1e13*0.7/h, 10**Osmond[mask,10]*(0.7*0.7/h/h/Ez), yerr=Osmond[mask,11]*2.303*10**Osmond[mask,10]*(0.7*0.7/h/h/Ez), c=c, mec='none', marker='s', ls='none', ms=4, label=r"Osmond \& Ponman 2004 groups (G-sample)")

plt.xlim(Mmin, Mmax)
plt.ylim(1e40,1e46)
plt.xscale('log')
plt.yscale('log')
plt.ylabel("L$_{500}$ E(z)$^{-1}$ [erg s$^{-1}$]", fontsize=22)
plt.xlabel("M$_{500}$ E(z) [$M_{\odot}$]", fontsize=22)#, labelpad=10)#, fontsize=16)
plt.gca().tick_params(axis='x', which='major', pad=7)

handles, labels = plt.gca().axes.get_legend_handles_labels()
n = 4
leg1 = plt.legend(handles[:n], labels[:n], loc='lower right', frameon=False, numpoints=1, ncol=1)
ax = plt.gca().add_artist(leg1)
plt.legend(handles[n:], labels[n:], loc='upper left', frameon=False, borderaxespad=1, numpoints=1, ncol=1)
#plt.legend(loc='lower right', frameon=False, numpoints=1, ncol=1)#, borderaxespad=1

plt.tight_layout(pad=0.45)
plt.savefig(outname)#, bbox_inches='tight')
print "Saved to", outname
