#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 27 11:47:49 2018

@author: nh444
"""

import scaling.plotting.plot_relation as pr
outdir = "/data/curie4/nh444/project1/L-T_relations/"

basedir = "/data/curie4/nh444/project1/L-T_relations/"
simdirs = ["L40_512_MDRIntRadioEff",
           ]
labels = ["fiducial",
          ]
zoomdirs = ["/data/curie4/nh444/project1/L-T_relations/MDRInt/"]
zooms = [["c96_MDRInt", "c128_MDRInt", "c192_MDRInt", "c224_MDRInt",
          "c256_MDRInt", "c288_MDRInt", "c320_MDRInt",
          "c352_MDRInt", "c384_MDRInt", #"c410_MDRInt",
          "c448_MDRInt",
          "c480_MDRInt", "c512_MDRInt"
          ]]
idxScat = range(len(simdirs))
idxMed = []
do_fit = True  # fit the simulation data with a power-law

projected = False  # if True, plot projected aperture values, otherwise 3D spherical
# cosmology to scale to
h_scale = 0.679
omega_m = 0.3065
omega_l = 0.6935

figsize = (7, 7)
col = ["#c7673e","#78a44f","#6b87c8","#934fce","#e6ab02","#be558f","#47af8d"]
col = ['#24439b', '#ED7600', '#50BFD7', '#B94400'] ## dark blue, orange, light blue, dark red
hlightcol = '#8292BF'
ls = ['solid']*len(simdirs)
markers = ['D', 's', 'o', '^', '>', 'v', '<']
ms = 8
lw = 3
mew = 1.8
binwidth = 0.2 ## dex for mass in Msun
frameon = False

kwargs = {'projected':projected,
          'frameon':frameon, 'zoomdirs':zoomdirs, 'zooms':zooms,
          'h_scale':h_scale, 'omega_m':omega_m, 'omega_l':omega_l,
          'basedir':basedir, 'outdir':outdir, 'figsize':figsize,
          'idxScat':idxScat, 'idxMed':idxMed, 'mec':col, 'hlightcol':hlightcol,
          'markers':markers, 'ls':ls, 'ms':ms, 'lw':lw, 'mew':mew,
          'binwidth':binwidth, 'plot_obs':True,
          'do_fit':do_fit, 'M500min_fit':3e13, 'fit_lw':3, 'fit_alpha':1.0}


plot_rel = True ## plot the relations at each redshift as well as the scatter
kwargs['axeslims'] = [[1e12, 4e15], None]
kwargs['axeslims'][1] = [1e40, 1e46]
pr.plot_scatter(simdirs, [0.0, 1.0, 2.0], ["mass", "lum"], kwargs,
             "scatter_L-M.pdf", suffix="L-M", plot_rel=plot_rel,
             inprefix="L-T_0.5-10", insuffix="_exp1e9_Tvir4")
#pr.plot_scatter(simdirs, [0.0], ["mass", "lum_brem"], kwargs,
#             "scatter_Lb-M.pdf", suffix="Lb-M", plot_rel=plot_rel,
#             inprefix="L-T_0.5-10", insuffix="_exp1e9_Tvir4")
#pr.plot_scatter(simdirs, [0.0], ["mass", "lum_tab"], kwargs,
#                "scatter_Lt-M.pdf", suffix="Lt-M", plot_rel=plot_rel,
#                inprefix="L-T_0.5-10", insuffix="_exp1e9_Tvir4")
#kwargs['axeslims'][1] = None
#pr.plot_scatter(simdirs, [0.0], ["mass", "gasmass"], kwargs,
#                "scatter_Mgas-M.pdf", suffix="Mgas-M", plot_rel=plot_rel,
#                inprefix="L-T_0.5-10", insuffix="_exp1e9_Tvir4")
#pr.plot_scatter(simdirs, [0.0], ["mass", "T500mw"], kwargs,
#                "scatter_T500mw-M.pdf", suffix="T500mw-M", plot_rel=plot_rel,
#                inprefix="L-T_0.5-10", insuffix="_exp1e9_Tvir4")
