import matplotlib.pyplot as plt
#import matplotlib.ticker as ticker
import numpy as np
import scipy.stats as stats
from os import chdir
chdir('/home/nh444/data/project1/L-T_relations')

outname = "L-Mgas_relation_paper.pdf"

snapnum = 25
h = 0.679
filename = "L-T_data_apec_0.5-2_Rasia_c4.txt"
filename = "L-T_data_apec_0.5-10_Rasia_c4_Tvir_thresh_2.5.txt"
filename = "L-T_data_apec_0.5-10_Rasia_1e7_Tvir_thresh_3.0.txt"
#filename = "L-T_data_apec_0.5-10_Rasia_1e7.txt"

fig = plt.figure(figsize=(7,7))
Mmin, Mmax = 10**11, 10**14.5

labels = ["Ref",
          "Stronger radio",
          "Quasar duty cycle",
          "Combined"]

from palettable.colorbrewer.qualitative import Dark2_8 as pallette
col=pallette.hex_colors
markers = ["D", "s", "o"]
ls=['dashdot', 'dotted', 'dashed', 'solid']
ms = 6.5
lw=2
mew=1.2

## Median profiles
bins = np.logspace(11, 12.5, num=20)
x = 10**((np.log10(bins[:-1]) + np.log10(bins[1:]))/2.0)

data1 = np.loadtxt("L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_"+str(snapnum).zfill(3)+"/"+filename)
plt.plot(data1[:,13] * 1e10 / h, data1[:,1]*0.679**2/h**2, marker=markers[2], mfc='none', mec=col[3], ms=ms, linestyle='none', mew=mew, zorder=4)
## median is plotted below
#==============================================================================
# for zoom in ["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c256_box_Arepo_new", "c320_box_Arepo_new", "c384_box_Arepo_new"]:
#     data = np.loadtxt("/home/nh444/data/project1/L-T_relations/LDRIntRadioEffBHVel/"+zoom+"_"+str(snapnum).zfill(3)+"/"+filename)
#     data[1] = data[1] * 0.6774**2 / 0.679**2 ## convert to same h coz gonna use zooms in median
#     data1 = np.vstack((data1, data))
#     plt.plot(data[13] * 1e10 / h, data[1]*0.679**2/h**2, marker='o', mfc='none', mec=col[3], mew=mew, ms=ms, linestyle='none', zorder=4)
# 
#==============================================================================
data = np.loadtxt("L40_512_NoDutyRadioWeak_HalfThermal_FixEta_"+str(snapnum).zfill(3)+"/"+filename)#0.3-10_fit_range
#plt.plot(data[:,13] * 1e10 / h, data[:,1]*0.679**2/h**2, marker=markers[0], mec='none', mfc=col[0], ms=ms, linestyle='none', mew=mew, zorder=4)
med, bin_edges, n = stats.binned_statistic(data[:,13] * 1e10 / h, data[:,1]*0.679**2/h**2, statistic='median', bins=bins)
plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[0], ls=ls[2], lw=lw, dash_capstyle='round', label=labels[0], zorder=4)

data = np.loadtxt("L40_512_NoDutyRadioInt_HalfThermal_FixEta_"+str(snapnum).zfill(3)+"/"+filename)#0.3-10_fit_range
#plt.plot(data[:,13] * 1e10 / h, data[:,1]*0.679**2/h**2, marker=markers[0], mec='none', mfc=col[1], ms=ms, linestyle='none', mew=mew, zorder=4)
med, bin_edges, n = stats.binned_statistic(data[:,13] * 1e10 / h, data[:,1]*0.679**2/h**2, statistic='median', bins=bins)
plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[1], ls=ls[1], lw=lw, dash_capstyle='round', label=labels[1], zorder=4)

data = np.loadtxt("L40_512_LowDutyRadioWeak_HalfThermal_FixEta_"+str(snapnum).zfill(3)+"/"+filename)#0.3-10_fit_range
#plt.plot(data[:,13] * 1e10 / h, data[:,1]*0.679**2/h**2, marker=markers[1], mec='none', mfc=col[2], ms=ms, linestyle='none', mew=mew, zorder=4)
med, bin_edges, n = stats.binned_statistic(data[:,13] * 1e10 / h, data[:,1]*0.679**2/h**2, statistic='median', bins=bins)
plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[2], ls=ls[0], lw=lw, dash_capstyle='round', label=labels[2], zorder=4)

#data = np.loadtxt("L40_512_LowDutyRadioWeak_HalfThermal_FixEta_BHChangeVel_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_default2.txt")#0.3-10_fit_range
#plt.plot(data[:,13] * 1e10 / h, data[:,1]*0.679**2/h**2, marker='D', mfc='none', mec=col[2], ms=4, linestyle='none', mew=mew)
#med, bin_edges, n = stats.binned_statistic(data[:,13] * 1e10 / h, data[:,1]*0.679**2/h**2, statistic='median', bins=bins)
#plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[2], lw=lw, label=r"QM duty cycle ($\delta t = 0.02$)")

med, bin_edges, n = stats.binned_statistic(data1[:,13] * 1e10 / h, data1[:,1]*0.679**2/h**2, statistic='median', bins=bins)
plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[3], ls=ls[3], lw=lw, dash_capstyle='round', label=labels[3], zorder=4)#(APEC $T_{min}=0.01$ keV)

c = '0.7'
from scaling.plotting.plotObsDat import L_Mgas
L_Mgas(plt.gca(), c=c, h=h)

plt.xlim(Mmin, Mmax)
plt.ylim(1e41, 1e46)
plt.xscale('log')
plt.yscale('log')
plt.ylabel("L$_{500}$ E(z)$^{-1}$ [erg s$^{-1}$]", fontsize=22)
plt.xlabel("M$_{\mathrm{gas},500}$ E(z) [$M_{\odot}$]", fontsize=22)
plt.gca().tick_params(axis='x', which='major', pad=7)

handles, labels = plt.gca().axes.get_legend_handles_labels()
n = 4
leg1 = plt.legend(handles[:n], labels[:n], loc='lower right', frameon=False, numpoints=1, ncol=1)
ax = plt.gca().add_artist(leg1)
plt.legend(handles[n:], labels[n:], loc='upper left', frameon=False, borderaxespad=1, numpoints=1, ncol=1)
#plt.legend(loc='upper left', frameon=False, borderaxespad=1, numpoints=1, ncol=1)

plt.tight_layout(pad=0.45)
plt.savefig(outname)#, bbox_inches='tight')
print "Saved to", outname
