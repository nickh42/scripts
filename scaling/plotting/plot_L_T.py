def main():
    outdir = "/home/nh444/data/project1/L-T_relations/"
    basedir="/home/nh444/data/project1/L-T_relations/"  
    
    zoomdirs = []#"/data/curie4/nh444/project1/L-T_relations/LDRIntRadioEffBHVel/"
    zooms = [[]]#["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c256_box_Arepo_new", "c320_box_Arepo_new", "c384_box_Arepo_new", "c448_box_Arepo_new"]
    core_excised = False
    
    simdirs = ["L40_512_LDRIntRadioEffBHVel"]
    filenames = ["L-T_data_apec_0.5-10_default_Tvir_thresh_4.0.txt", "L-T_data_apec_0.5-10_default_Tvir_thresh_4.0_z0.01.txt", "L-T_data_apec_0.5-10_default_Tvir_thresh_4.0_z0.0001.txt"]
    labels = ["$z=0.001$", "$z=0.01$", "$z=0.0001$"]
    filenames = ["L-T_data_apec_0.5-10_default_Tvir_thresh_4.0.txt",
             "L-T_data_apec_0.03-10_default_Tvir_thresh_4.0.txt",
             #"L-T_data_apec_0.5-10_default_Tvir_thresh_4.0_c07.txt", # ACIS-I
             #"L-T_data_apec_0.5-10_default_Tvir_thresh_4.0_c07_ACIS-S.txt",
             #"L-T_data_apec_0.03-10_default_Tvir_thresh_4.0_c07.txt",
             #"L-T_data_apec_0.5-10_default_Tvir_thresh_4.0_c20_ACIS-S.txt",
             #"L-T_data_apec_0.5-10_default_Tvir_thresh_4.0_c20_ACIS-I.txt",
             #"L-T_data_apec_0.03-10_default_Tvir_thresh_4.0_c20_ACIS-S.txt",
             #"L-T_data_apec_0.5-10_Rasia2012_Tvir_thresh_4.0.txt",
             ]
    labels = ["default [0.5-10 keV fit]",
              "default [0.03-10 keV fit]",
              #"ACIS-I C07 [0.5-10 keV fit]",
              #"ACIS-S C07 [0.5-10 keV fit]",
              #"ACIS-I C07 [0.03-10 keV fit]",
              #"ACIS-S C20 [0.5-10 keV fit]",
              #"ACIS-I C20 [0.5-10 keV fit]",
              #"ACIS-S C20 [0.03-10 keV fit]",
              ]
    outname = "L-T_response_comp.pdf"
    idxScat = range(len(filenames)*len(filenames))
    idxMed = []
    
    ### Metals comparison -- 0.3 Zsun or sim metals
    simdirs = ["L40_512_LDRIntRadioEffBHVel"]
    filenames = ["L-T_data_apec_0.5-10_default_Tvir_thresh_4.0.txt", "L-T_data_apec_0.5-10_default_Tvir_thresh_4.0_sim_metals.txt"]
    labels = ["$Z = 0.3 Z_{\odot}$", "$Z = Z_{\mathrm{sim}}$"]
    outname = "L-T_sim_metals.pdf"
    idxScat = range(len(simdirs)*len(filenames))
    idxMed = []
    zoomdirs = [""]*len(simdirs)
    
#==============================================================================
#     ### subhaloes comparison -- with and without subhaloes
#     simdirs = ["L40_512_LDRIntRadioEffBHVel"]
#     filenames = ["L-T_data_apec_0.5-10_default_Tvir_thresh_4.0.txt", "L-T_data_apec_0.5-10_default_Tvir_thresh_4.0_main_halo.txt"]
#     labels = ["with subhaloes", "without subhaloes"]
#     outname = "L-T_subhaloes.pdf"
#     idxScat = range(len(simdirs)*len(filenames))
#     idxMed = []
#==============================================================================
    
#==============================================================================
#     ### projection axis comparison
#     simdirs = ["L40_512_MDRIntRadioEff"]
#     filenames = ["L-T_data_apec_0.5-10_default_Tvir_thresh_4.0_x.txt", "L-T_data_apec_0.5-10_default_Tvir_thresh_4.0_y.txt", "L-T_data_apec_0.5-10_default_Tvir_thresh_4.0_z.txt"]
#     labels = ["x", "y", "z"]
#     outname = "L-T_projection_axis.pdf"
#     idxScat = range(len(simdirs)*len(filenames))
#     idxMed = []
#==============================================================================
    
#==============================================================================
#     ### Models comparison
#     simdirs = [#"L40_512_LDRIntRadioEff_noBH",
#                #"L40_512_NoDutyRadioWeak",
#                #"L40_512_NoDutyRadioInt",
#                #"L40_512_DutyRadioWeak",
#                "L40_512_LDRIntRadioEffBHVel",
#                #"L40_512_LDRHighRadioEff",
#                "L40_512_MDRIntRadioEff",
#                ]
#     labels = [#"No BHs",
#               #"Weak radio",
#               #"Stronger radio",
#               #"Quasar duty cycle",
#               "$\delta t = 10$ Myr",#"Combined",
#               #"Longer radio duty",
#               "$\delta t = 25$ Myr",
#               ]
#     filenames = ["L-T_data_apec_0.5-10_default_Tvir_thresh_4.0.txt"]
#     core_excised = False
#     #filenames = ["L-T_data_apec_0.5-10_LT0.15-1.0r500_default_Tvir_thresh_4.0.txt"]
#     #core_excised = True
#     outname = "L-T_models_3D.pdf"
#     idxScat = range(len(simdirs)*len(filenames))
#     idxMed = []
#     #zoomdir = "/data/curie4/nh444/project1/L-T_relations/LDRIntRadioEffBHVel/"
#     #zooms = ["c96_box_Arepo_new", "c128_box_Arepo_new", "c192_box_Arepo_new", "c256_box_Arepo_new", "c384_box_Arepo_new"]
#     #zoomdir2 = "/data/curie4/nh444/project1/L-T_relations/MDRInt/"
#     #zooms2 = ["c96_MDRInt", "c128_MDRInt", "c192_MDRInt", "c256_MDRInt", "c384_MDRInt"]
#==============================================================================
    
#==============================================================================
#     ### Temp thresh comparison
#     simdirs = ["L40_512_MDRIntRadioEff"]
#     filenames = ["L-T_data_apec_0.5-10_default_Tvir_thresh_4.0.txt", "L-T_data_apec_0.5-10_default.txt"]
#     labels = [r"T $<$ 4 T$_{200}$", "no temp thresh.",]
#     outname = "L-T_MDRInt_temp_thresh.pdf"
#==============================================================================
    
#==============================================================================
#     ## fit range comparison
#     simdirs = ["L40_512_MDRIntRadioEff"]
#     filenames = ["L-T_data_apec_0.5-10_default_Tvir_thresh_4.0.txt",
#                  "L-T_data_apec_0.7-10_default_Tvir_thresh_4.0.txt",
#                  "L-T_data_apec_0.7-2_default_Tvir_thresh_4.0.txt"]
#     labels = ["[0.5-10] keV", "[0.7-10] keV", "[0.7-2] keV"]
#     outname = "L-T_MDRInt_fit_range.pdf"
#==============================================================================
    
#==============================================================================
#     ## halfmassrad
#     simdirs = ["L40_512_MDRIntRadioEff"]
#     filenames = ["L-T_data_apec_0.5-10_T50kpc_default_Tvir_thresh_4.0.txt",
#                  "L-T_data_apec_0.5-10_5halfmassrad_default_Tvir_thresh_4.0.txt"]
#     labels = ["50 kpc", "$5 r_{\star, 0.5}$"]
#     outname = "L-T_MDRInt_halfmassrad.pdf"
#==============================================================================
    
    ## projection axis comparison
    simdirs = ["L40_512_MDRIntRadioEff"]
    filenames = ["L-T_data_apec_0.5-10_default.txt", "L-T_data_apec_0.5-10_default_zaxis0.txt", "L-T_data_apec_0.5-10_default_zaxis2.txt"]
    labels = ["zaxis=1", "zaxis=0", "zaxis=2"]
    outname = "L-T_MDRInt_zaxis.pdf"
    core_excised = False
    idxScat = range(len(simdirs)*len(filenames))
    idxMed = []
    zoomdirs = ["/data/curie4/nh444/project1/L-T_relations/MDRInt/"]
    zooms = [[]]
    #zooms = [["c96_MDRInt", "c128_MDRInt", "c192_MDRInt", "c256_MDRInt", "c320_MDRInt", "c384_MDRInt", "c448_MDRInt"]]

    
    snapnum = 25
    h = 0.679

    figsize=(7,7)
    #from palettable.colorbrewer.qualitative import Dark2_8 as pallette
    #col=pallette.hex_colors
    #ls=['dashdot', 'dotted', 'dashed', 'solid']
    col = ["#c7673e","#78a44f","#6b87c8","#934fce","#e6ab02","#be558f","#47af8d"]
    #col = ['#24439b', '#ED7600'] ## blue orange
    mec = col#['none']*len(simdirs)*len(filenames)
    mfc = ['none']*len(simdirs)*len(filenames)
    ls=['solid']*len(simdirs)*len(filenames)
    markers = ['D', 's', 'o', '^', '>', 'v', '<']
    ms=8
    lw=3
    mew=1.8
    binwidth = 0.2 ## dex for mass in Msun 

    binwidth = 0.1
#==============================================================================
#     if core_excised:
#         outname = "L-T_relation_models_cex.pdf"
#         #filename = "L-T_data_apec_0.5-10_Rasia_1e7_core_ex_L.txt"
#     else:
#         #filename = "L-T_data_apec_0.5-10_Rasia_1e7_Tvir_thresh_3.0.txt"
#         outname = "L-T_relation_models_cin.pdf"
#==============================================================================
    indArrow = []#[238]
    plot_L_T(simdirs, snapnum, filenames, indArrow=indArrow, projected=True, for_paper=False, frameon=False, core_excised=core_excised, labels=labels, zoomdirs=zoomdirs, zooms=zooms, h=h, basedir=basedir, outname=outname, outdir=outdir, figsize=figsize, idxScat=idxScat, idxMed=idxMed, col=col, mec=mec, mfc=mfc, markers=markers, ls=ls, ms=ms, lw=lw, mew=mew, binwidth=binwidth)

def plot_L_T(simdirs, snapnum, filenames, core_excised=False, projected=True, h=0.679,
             basedir="/home/nh444/data/project1/L-T_relations/",
             zoomdirs=None, zooms=[],
             figsize=(7,7), idxScat=[], idxMed=[], indArrow=[], hlightzooms=False,
             labels=None, frameon=False, for_paper=True,
             outname="L-T_relation.pdf", outdir="/home/nh444/data/project1/L-T_relations/",
             col=None, mec=None, mfc=None, markers=None, mew=1.2, ms=6.5,
             ls=None, lw=2, binwidth=0.1, blkwht=False):
    import matplotlib.pyplot as plt
    import matplotlib.ticker as ticker
    import numpy as np
    import scipy.stats as stats
    
    assert len(simdirs) > 0, "simdirs is an empty list"
    if not isinstance(filenames, (list, tuple)):## if not a list, convert to list
        filenames = [filenames]
    
    if projected:
        temp_idx = 5 ## temperature index - 5 for spectral projected, 22 for spectral 3D, 14 for mass-weighted projected, 15 for em-weighted projected
        lum_idx = 25 ## 1 for 3D luminosity, 25 for projected
    else: ## 3-D
        temp_idx = 22 ## temperature index - 5 for spectral projected, 22 for spectral 3D, 14 for mass-weighted projected, 15 for em-weighted projected
        lum_idx = 1 ## 1 for 3D luminosity, 25 for projected
    
    if col is None:
        from palettable.colorbrewer.qualitative import Dark2_8 as pallette
        col=pallette.hex_colors
    if mec is None:
        mec = col
    if mfc is None:
        mfc = ['none']*len(simdirs)*len(filenames)
    if markers is None:
        markers = ['D', 's', 'o', '^', '>', 'v', '<']
    if ls is None:
        ls=['dashdot', 'dotted', 'dashed', 'solid']
    if zoomdirs is None:
        zoomdirs = [""]*len(simdirs)
    
    fig = plt.figure(figsize=figsize)
    from scaling.plotting.plotObsDat import L_T
    L_T(plt.gca(), core_excised=core_excised, ms=ms, for_paper=for_paper, h=h)
    
    ## Median profiles
    xmin, xmax = [np.log10(0.16), np.log10(1.7)]
    num = int((xmax - xmin) / binwidth)
    bins = np.logspace(xmin, xmax, num=num)
    x = 10**((np.log10(bins[:-1]) + np.log10(bins[1:]))/2)
    
    for simidx, simdir in enumerate(simdirs):
        for fileidx, filename in enumerate(filenames):
            idx = fileidx + simidx*(len(filenames))
            data = np.loadtxt(basedir+simdirs[simidx]+"_"+str(snapnum).zfill(3)+"/"+filename)
            label = labels[idx]
            
            if zoomdirs[simidx] is not "":
                for zoom in zooms[simidx]:
                    zdata = np.loadtxt(zoomdirs[simidx]+zoom+"_"+str(snapnum).zfill(3)+"/"+filename)
                    data = np.vstack((data, zdata))
                    if hlightzooms:
                        plt.plot(zdata[temp_idx], zdata[lum_idx], marker=markers[idx], mfc=mec[idx], mec=mec[idx], ms=ms, ls='none', mew=mew)
            
            if idx in idxMed:
                med, bin_edges, n = stats.binned_statistic(data[:,temp_idx], data[:,lum_idx], statistic='median', bins=bins)
                plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[idx], ls=ls[idx], lw=lw, dash_capstyle='round', label=label)
                label = None ## don't want two legend entries
            if idx in idxScat:
                plt.plot(data[:,temp_idx], data[:,lum_idx], marker=markers[idx], mfc=mfc[idx], mec=mec[idx], ms=ms, ls='none', mew=mew, label=label)
                if len(indArrow) > 0:
                    print "Showing arrows for groups:"
                    print data[indArrow,0]
                    if fileidx == 0:
                        data1 = data
                    else:
                        for i in indArrow:
                            #plt.gca().arrow(data1[i,temp_idx], data1[i,lum_idx], data[i,temp_idx]-data1[i,temp_idx], data[i,lum_idx]-data1[i,lum_idx], width=1.0, zorder=4)
                            plt.gca().annotate("",
                            xy=(data1[i,temp_idx], data1[i,lum_idx]), xycoords='data',
                            xytext=(data[i,temp_idx], data[i,lum_idx]), textcoords='data',
                            arrowprops=dict(fc=col[idx], ec=None, #arrowstyle='->',
                            width=0.5, headwidth=6, headlength=5, shrink=0.07, fill=True))##width, headwidth, headlength are in pts
                            #connectionstyle="arc3"))



    ### This is just to show where Sun 2009 and Vikhlinin 2006 lie on L-T and uses L-T values form Horner 2001 for the same groups
    #Sun = np.genfromtxt("/data/vault/nh444/ObsData/L-T/Sun2009.csv", delimiter=",")
    #mask = np.isfinite(Sun[:,6])
    ## This uses the L-T directly from Horner 2001, not from the data table
    ##### divide lum by Ez!! #####
    #plt.errorbar(Sun[mask,9], 10**Sun[mask,8]*(50.0*50.0/(H*H)), xerr=Sun[mask,10], c='darkblue', mec='none', marker='D', ls='none', ms=4, label=r"Sun+ 2009 sample \& Horner 2001 L-T", zorder=3)##e60000
    ## This uses the L-T directly from the paper -
    ##### divide lum by Ez!! #####
    #plt.errorbar(Sun[mask,9], 10**Sun[mask,6]*(Sun[mask,7])*(50.0*50.0/(H*H)), xerr=Sun[mask,10], c='red', mec='none', marker='D', ls='none', ms=4, label="Sun+ 2009 sample & Horner 2001 L-T", zorder=3)
    #plt.errorbar(Sun[:,1], Sun[:,4]*44.0*(72.0*72.0/(H*H)), xerr=[abs(Sun[:,3]), Sun[:,2]], c='#ff00bf', mec='none', marker='D', ls='none', ms=4, label="Sun+ 2009 sample & Zhang+ 2011 luminosities", zorder=3)
    #Vikh = np.genfromtxt("/data/vault/nh444/ObsData/profiles/Vikhlinin_profiles/Vikhlinin2006_maintable.csv", delimiter=",")
    #mask = np.isfinite(Vikh[:,19])
    ##### divide lum by Ez!! #####
    #plt.errorbar(Vikh[mask,21], 10**Vikh[mask,23]*(50.0*50.0/(H*H)), xerr=Vikh[mask,22], c=c, mec='none', marker='D', ls='none', ms=4, label=r"Vikhlinin+ 2006 sample \& Horner 2001 L-T", zorder=3)
    ## This uses the L-T directly from Horner 2001, not from the data table
    ##### divide lum by Ez!! #####
    #plt.errorbar(Vikh[mask,21], 10**Vikh[mask,19]*(Vikh[mask,20])*(50.0*50.0/(H*H)), xerr=Vikh[mask,22], c='green', mec='none', marker='D', ls='none', ms=4, label="Vikhlinin+ 2006 sample & Horner 2001 L-T", zorder=3)
    #plt.errorbar(Vikh[:,3], (Vikh[:,13]*1e44*(72.0*72.0/(H*H)), xerr=Vikh[:,4], yerr=Vikh[:,14], c='#006080', mec='none', marker='D', ls='none', ms=4, label="Vikhlinin+ 2006 sample & Zhang+ 2011 luminosities", zorder=3)
    
    plt.xlim(0.3, 10)
    plt.ylim(1e41,1e46)
    plt.xscale('log')
    plt.yscale('log')
    if core_excised:
        plt.ylabel("L$_{500, \mathrm{ce}}^{\mathrm{bol}}$ E(z)$^{-1}$ [erg s$^{-1}$]")#^{\mathrm{bol}}
        plt.xlabel(r"T$_{500, \mathrm{ce}}$ [keV]")
    else:
        plt.ylabel("L$_{500}^{\mathrm{bol}}$ E(z)$^{-1}$ [erg s$^{-1}$]")#^{\mathrm{bol}}
        plt.xlabel(r"T$_{500}$ [keV]")
        
    plt.gca().xaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))
    plt.tight_layout(pad=0.45)
    from scaling.plotting.plotObsDat import add_legends
    if blkwht:
        add_legends(plt.gca(), simdirs, labels, textcol='white', nfiles=len(filenames))
        fig.patch.set_facecolor('0.2')
        ax = plt.gca()
        ax.set_axis_bgcolor('0.2')
        ax.xaxis.label.set_color('white')
        ax.tick_params(axis='x', which='both', colors='white')
        ax.yaxis.label.set_color('white')
        ax.tick_params(axis='y', which='both', colors='white')
        import matplotlib.spines
        for child in ax.get_children():
            if isinstance(child, matplotlib.spines.Spine):
                child.set_color('white')
        fig.savefig(outdir+outname, bbox_inches='tight', transparent=True)
    else:
        add_legends(plt.gca(), simdirs, labels, nfiles=len(filenames))
        fig.savefig(outdir+outname)#, bbox_inches='tight')
    print "Saved to", outdir+outname

if __name__ == "__main__":
    main()