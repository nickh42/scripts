def main():
    outdir = "/data/curie4/nh444/project1/L-T_relations/"
    basedir= "/data/curie4/nh444/project1/L-T_relations/"
    simdirs = ["L40_512_NoDutyRadioWeak_HalfThermal_FixEta",
               "L40_512_NoDutyRadioInt_HalfThermal_FixEta",
               "L40_512_LowDutyRadioWeak_HalfThermal_FixEta",
               "L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff",
               ]
    labels = ["Ref",
              "Stronger radio",
              "Quasar duty cycle",
              "Combined"]
    
    basedir = "/data/curie4/nh444/project1/L-T_relations/"
    simdirs = [#"L40_512_LDRIntRadioEff_noBH",
               #"L40_512_NoDutyRadioWeak",
               #"L40_512_NoDutyRadioInt",
               #"L40_512_DutyRadioWeak",
               "L40_512_LDRIntRadioEffBHVel",
               #"L40_512_LDRHighRadioEff",
               "L40_512_MDRIntRadioEff",
               ]
    labels = [#"No BHs",
              #"Weak radio",
              #"Stronger radio",
              #"Quasar duty cycle",
              "$\delta t = 10$ Myr",#"Combined",
              #"Longer radio duty",
              "$\delta t = 25$ Myr",
              ]
    zoomdirs = ["/data/curie4/nh444/project1/L-T_relations/LDRIntRadioEffBHVel/",
                "/data/curie4/nh444/project1/L-T_relations/MDRInt/"]
    zooms = [#["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c192_box_Arepo_new", "c256_box_Arepo_new", "c320_box_Arepo_new", "c384_box_Arepo_new", "c448_box_Arepo_new"],
             ["c96_box_Arepo_new", "c128_box_Arepo_new", "c192_box_Arepo_new", "c256_box_Arepo_new", "c384_box_Arepo_new"],
             ["c96_MDRInt", "c128_MDRInt", "c192_MDRInt", "c256_MDRInt", "c384_MDRInt"]]
    idxScat = range(len(simdirs))
    idxMed = []#range(len(simdirs))
    
#==============================================================================
#     simdirs = ["L40_512_LDRIntRadioEffBHVel"]
#     filenames = ["L-T_data_apec_0.5-10_default_Tvir_thresh_4.0.txt",
#              #"L-T_data_apec_0.03-10_default_Tvir_thresh_4.0.txt",
#              #"L-T_data_apec_0.5-10_default_Tvir_thresh_4.0_c07.txt", # ACIS-I
#              #"L-T_data_apec_0.5-10_default_Tvir_thresh_4.0_c07_ACIS-S.txt",
#              #"L-T_data_apec_0.03-10_default_Tvir_thresh_4.0_c07.txt",
#              "L-T_data_apec_0.5-10_default_Tvir_thresh_4.0_c20_ACIS-S.txt",
#              "L-T_data_apec_0.5-10_default_Tvir_thresh_4.0_c20_ACIS-I.txt",
#              #"L-T_data_apec_0.03-10_default_Tvir_thresh_4.0_c20_ACIS-S.txt",
#              #"L-T_data_apec_0.5-10_Rasia2012_Tvir_thresh_4.0.txt",
#              ]
#     labels = ["default [0.5-10 keV fit]",
#               #"default [0.03-10 keV fit]",
#               #"ACIS-I C07 [0.5-10 keV fit]",
#               #"ACIS-S C07 [0.5-10 keV fit]",
#               #"ACIS-I C07 [0.03-10 keV fit]",
#               "ACIS-S C20 [0.5-10 keV fit]",
#               "ACIS-I C20 [0.5-10 keV fit]",
#               #"ACIS-S C20 [0.03-10 keV fit]",
#               ]
#     idxScat = range(len(filenames))
#     idxMed = []
#==============================================================================
       
    snapnum = 25
    h = 0.679
    
    figsize=(7,7)
    #col = ["#c7673e","#78a44f","#6b87c8","#934fce","#e6ab02","#be558f","#47af8d"]
    col = ['#24439b', '#ED7600'] ## blue orange
    ls=['solid']*len(simdirs)
    markers = ['D', 's', 'o', '^', '>', 'v', '<']
    ms=8
    lw=3
    mew=1.8
    binwidth = 0.2 ## dex for mass in Msun
    frameon = True

#==============================================================================
#     core_excised = False
#     binwidth = 0.1
#     if core_excised:
#         outname = "M-T_relation_models_cex.pdf"
#         filename = "L-T_data_apec_0.5-10_Rasia_1e7_core_ex_L.txt"
#     else:
#         outname = "M-T_relation_models_cin.pdf"
#         filename = "L-T_data_apec_0.5-10_Rasia_1e7_Tvir_thresh_3.0.txt"
#     plot_M_T(simdirs, snapnum, filename, core_excised=core_excised, labels=labels, zoomdir=zoomdir, zooms=zooms, h=h, basedir=basedir, outname=outname, outdir=outdir, figsize=figsize, idxScat=idxScat, idxMed=idxMed, col=col, markers=markers, ls=ls, ms=ms, lw=lw, mew=mew, binwidth=binwidth)
#     
#==============================================================================
    ## Lensing masses:
    core_excised = False
    binwidth = 0.1
    outname = "M-T_relation_XXL_models.pdf"
    filename = "L-T_data_apec_0.5-10_T300kpc_"+"default_Tvir_thresh_4.0"+".txt"
    XXL_aper = True
    from scaling.plotting.plot_M_T import plot_M_T
    plot_M_T(simdirs, snapnum, filename, projected=True, core_excised=core_excised, XXL_aper=XXL_aper, labels=labels, frameon=frameon, zoomdirs=zoomdirs, zooms=zooms, h=h, basedir=basedir, outname=outname, outdir=outdir, figsize=figsize, idxScat=idxScat, idxMed=idxMed, col=col, markers=markers, ls=ls, ms=ms, lw=lw, mew=mew, binwidth=binwidth)
    
    ## X-ray masses:
    core_excised = False
    binwidth = 0.1
    outname = "M-T_relation_xray_models.pdf"
    filename = "L-T_data_apec_0.5-10_default_Tvir_thresh_4.0.txt"
    XXL_aper = False
    plot_M_T(simdirs, snapnum, filename, core_excised=core_excised, XXL_aper=XXL_aper, labels=labels, frameon=frameon, zoomdirs=zoomdirs, zooms=zooms, h=h, basedir=basedir, outname=outname, outdir=outdir, figsize=figsize, idxScat=idxScat, idxMed=idxMed, col=col, markers=markers, ls=ls, ms=ms, lw=lw, mew=mew, binwidth=binwidth)


def plot_M_T(simdirs, snapnum, filenames, projected=True,
             core_excised=False, XXL_aper=False, COSMOS_aper=False,
             labels=None, frameon=False, plotlabel=None,
             zoomdirs=None, zooms=[], h=0.679,
             basedir="/home/nh444/data/project1/L-T_relations/",
             outname="M-T_relation.pdf", outdir="/home/nh444/data/project1/L-T_relations/",
             figsize=(7,7), idxScat=[], idxMed=[], hlightzooms=False,
             col=None, markers=None, ls=None, ms=6.5,
             lw=2, mew=1.2, binwidth=0.1):
    import matplotlib.pyplot as plt
    import matplotlib.ticker as ticker
    import numpy as np
    import scipy.stats as stats
    
    assert len(simdirs) > 0, "simdirs is an empty list"
    if not isinstance(filenames, (list, tuple)):## if not a list, convert to list
        filenames = [filenames]
    
    # temp_mw, temp_emw, temp_bw, temp_ew, temp_mw_3D, temp_emw_3D, temp_bw_3D, temp_ew_3D, temp_3D
    # 14,      15,       16,      17,      18,         19,          20,         21,         22,
    if projected:
        temp_idx = 5 ## temperature index - 5 for spectral projected, 22 for spectral 3D, 15 for emission-weighted, 14 for mass-weighted
    else:
        temp_idx = 22
    
    if col is None:
        from palettable.colorbrewer.qualitative import Dark2_8 as pallette
        col=pallette.hex_colors
    if markers is None:
        markers = ["D", "s", "o"]
    if ls is None:
        ls=['dashdot', 'dotted', 'dashed', 'solid']
    
    fig = plt.figure(figsize=figsize)
    ### Observations ###
    #c='0.7'
    #c2='0.5'##lensing
    Tmin, Tmax = 0.4, 10.0
    from scaling.plotting.plotObsDat import M_T
    M_T(plt.gca(), Tmin, Tmax, core_excised=core_excised, COSMOS_aper=COSMOS_aper, XXL_aper=XXL_aper, ms=ms, h=h)
    
    ## Median profiles
    xmin, xmax = [np.log10(0.16), np.log10(1.7)]
    num = int((xmax - xmin) / binwidth)
    bins = np.logspace(xmin, xmax, num=num)
    x = 10**((np.log10(bins[:-1]) + np.log10(bins[1:]))/2)
    
    for simidx, simdir in enumerate(simdirs):
        for fileidx, filename in enumerate(filenames):
            idx = fileidx + simidx*(len(filenames))
            data = np.loadtxt(basedir+simdirs[simidx]+"_"+str(snapnum).zfill(3)+"/"+filename)
            label = labels[idx]
            
            if zoomdirs[simidx] is not "":
                for zoom in zooms[simidx]:
                    zdata = np.loadtxt(zoomdirs[simidx]+zoom+"_"+str(snapnum).zfill(3)+"/"+filename)
                    data = np.vstack((data, zdata))
                    if hlightzooms:
                        plt.plot(zdata[temp_idx], zdata[12] * 1e10 / h, marker=markers[idx], mfc=col[idx], mec=col[idx], ms=ms, ls='none', mew=mew)
            if idx in idxMed:
                med, bin_edges, n = stats.binned_statistic(data[:,temp_idx], data[:,12] * 1e10 / h, statistic='median', bins=bins)
                plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[idx], ls=ls[idx], lw=lw, dash_capstyle='round', label=label)
                label = None ## don't want two legend entries
            if idx in idxScat:
                plt.plot(data[:,temp_idx], data[:,12] * 1e10 / h, marker=markers[idx], mfc='none', mec=col[idx], ms=ms, ls='none', mew=mew, label=label)
   
    plt.xscale('log')
    plt.xlim(Tmin, Tmax)
    plt.ylim(5e12, 4e15)
    plt.yscale('log')
    if core_excised:
        if COSMOS_aper:
            plt.xlabel(r"T$_{0.1-0.5 r_{500}}$ [keV]")
        else:
            plt.xlabel(r"T$_{500, \mathrm{ce}}$ [keV]")
    else:
        if XXL_aper:
            plt.xlabel(r"T$_{300 \mathrm{kpc}}$ [keV]")
        else:
            plt.xlabel(r"T$_{500}$ [keV]")
    plt.ylabel("M$_{500}$ E(z) [$M_{\odot}$]")

    from scaling.plotting.plotObsDat import add_legends
    add_legends(plt.gca(), simdirs, labels, col=col, frameon=frameon)
    
    if plotlabel is not None:
        plt.text(0.9, 0.1, plotlabel, horizontalalignment='right',
                 verticalalignment='center', transform=plt.gca().transAxes)
    
    #leg = plt.legend(loc='upper left', frameon=False, borderaxespad=1, numpoints=1, fontsize=12, ncol=1)
    plt.gca().xaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))
    
    plt.tight_layout(pad=0.45)
    fig.savefig(outdir+outname)#, bbox_inches='tight', transparent=True)
    print "Saved to", outdir+outname

if __name__ == "__main__":
    main()