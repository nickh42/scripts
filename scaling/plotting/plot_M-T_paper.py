import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np
import scipy.stats as stats
from os import chdir
chdir('/home/nh444/data/project1/L-T_relations')

core_excised = False

if core_excised:
    outname = "M-T_relation_paper_cex.pdf"
    filename = "L-T_data_apec_0.5-10_Rasia_1e7_core_ex_L.txt"
else:
    outname = "M-T_relation_paper_cin.pdf"
    filename = "L-T_data_apec_0.5-10_Rasia_1e7_Tvir_thresh_3.0.txt"
    
#filename = "L-T_data_apec_0.5-2_Rasia_c4.txt"
#filename = "L-T_data_apec_0.5-10_Rasia_c4_Tvir_thresh_2.5.txt"

snapnum = 25
h = 0.679 ## h value to scale to


fig = plt.figure(figsize=(7,7))
plt.xscale('log')
Tmin, Tmax = 0.3, 10.0
plt.xlim(Tmin, Tmax)
plt.ylim(5e12, 4e15)
plt.yscale('log')
if core_excised:
    plt.xlabel(r"T$_{500, \mathrm{ce}}$ [keV]", fontsize=22)
    plt.ylabel("M$_{500, \mathrm{ce}}$ E(z) [$M_{\odot}$]", fontsize=22)
else:
    plt.xlabel(r"T$_{500}$ [keV]", fontsize=22)
    plt.ylabel("M$_{500}$ E(z) [$M_{\odot}$]", fontsize=22)

labels = ["Ref",
          "Stronger radio",
          "Quasar duty cycle",
          "Combined"]

from palettable.colorbrewer.qualitative import Dark2_8 as pallette
col=pallette.hex_colors
markers = ["D", "s", "o"]
ls=['dashdot', 'dotted', 'dashed', 'solid']
#ls=['solid']*4
ms = 6.5
lw=3
mew=1.2
temp_idx = 5 ## temperature index - 5 for spectral, 15 for emission-weighted, 14 for mass-weighted
## Median profiles
bins = np.logspace(-1.2, np.log10(2.0), num=22)## 16 or 22
x = 10**((np.log10(bins[:-1]) + np.log10(bins[1:]))/2)

data1 = np.loadtxt("L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_"+str(snapnum).zfill(3)+"/"+filename)
plt.plot(data1[:,temp_idx], data1[:,12] * 1e10 / h, marker=markers[2], mfc='none', mec=col[3], ms=ms, ls='none', mew=1.2, zorder=4)
## median is plotted last
#==============================================================================
# for zoom in ["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c256_box_Arepo_new", "c320_box_Arepo_new", "c384_box_Arepo_new"]:
#     data = np.loadtxt("/home/nh444/data/project1/L-T_relations/LDRIntRadioEffBHVel/"+zoom+"_"+str(snapnum).zfill(3)+"/"+filename)
#     data1 = np.vstack((data1, data))
#     plt.plot(data[temp_idx], data[12] * 1e10 / h, marker='o', mfc='none', mec=col[3], mew=1.2, ms=ms, ls='none', zorder=4)
#==============================================================================
    
data = np.loadtxt("L40_512_NoDutyRadioWeak_HalfThermal_FixEta_"+str(snapnum).zfill(3)+"/"+filename)
#plt.plot(data[:,temp_idx], data[:,12] * 1e10 / h, marker=markers[0], mec='none', mfc=col[0], ms=ms, ls='none', mew=1.0, zorder=4)
med, bin_edges, n = stats.binned_statistic(data[:,temp_idx], data[:,12] * 1e10 / h, statistic='median', bins=bins)
plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[0], ls=ls[0], lw=1.5, dash_capstyle='round', label="Ref", zorder=4)

data = np.loadtxt("L40_512_NoDutyRadioInt_HalfThermal_FixEta_"+str(snapnum).zfill(3)+"/"+filename)
#plt.plot(data[:,temp_idx], data[:,12] * 1e10 / h, marker=markers[0], mec='none', mfc=col[1], ms=ms, ls='none', mew=1.0, zorder=4)
med, bin_edges, n = stats.binned_statistic(data[:,temp_idx], data[:,12] * 1e10 / h, statistic='median', bins=bins)
plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[1], ls=ls[1], lw=1.5, dash_capstyle='round', label="Stronger radio", zorder=4)

data = np.loadtxt("L40_512_LowDutyRadioWeak_HalfThermal_FixEta_"+str(snapnum).zfill(3)+"/"+filename)
#plt.plot(data[:,temp_idx], data[:,12] * 1e10 / h, marker=markers[1], mec='none', mfc=col[2], ms=ms, ls='none', mew=1.0, zorder=4)
med, bin_edges, n = stats.binned_statistic(data[:,temp_idx], data[:,12] * 1e10 / h, statistic='median', bins=bins)
plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[2], ls=ls[2], lw=1.5, dash_capstyle='round', label="Quasar duty cycle", zorder=4)

med, bin_edges, n = stats.binned_statistic(data1[:,temp_idx], data1[:,12] * 1e10 / h, statistic='median', bins=bins)
plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[3], ls=ls[3], lw=1.5, dash_capstyle='round', label="Combined", zorder=4)#(APEC $T_{min}=0.01$ keV)

### Observations ###
c='0.7'
c2='0.5'##lensing
from scaling.plotting.plotObsDat import M_T
M_T(plt.gca(), Tmin, Tmax, core_excised=core_excised, c=c, c2=c2, h=h)

handles, labels = plt.gca().axes.get_legend_handles_labels()
n = 4
leg1 = plt.legend(handles[:n], labels[:n], loc='lower right', frameon=False, numpoints=1, fontsize='x-small', ncol=1)
ax = plt.gca().add_artist(leg1)
plt.legend(handles[n:], labels[n:], loc='upper left', frameon=False, borderaxespad=1, numpoints=1, fontsize='x-small', ncol=1)

#leg = plt.legend(loc='upper left', frameon=False, borderaxespad=1, numpoints=1, fontsize=12, ncol=1)
plt.gca().xaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))

plt.tight_layout(pad=0.45)
plt.savefig(outname)#, bbox_inches='tight', transparent=True)
print "Saved to", outname

