#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  2 12:28:38 2016

@author: nh444
"""
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as stats
from os import chdir
chdir('/home/nh444/data/project1/L-T_relations')

labels = ["Ref",
          "Stronger radio",
          "Quasar duty cycle",
          "Combined"]
snapnum = 25
obs_dir = "/data/vault/nh444/ObsData/mass_fracs/"
outfilename="gas_mass_fractions_LowHigh_softening.pdf"
#filename = "L-T_data_apec_0.5-2_Rasia_c4.txt"
filename = "L-T_data_apec_0.5-10_Rasia_c4_Tvir_thresh_2.5.txt"
#filename = "L-T_data_apec_0.5-10_none_Tvir_thresh_2.5.txt"
filename = "L-T_data_apec_0.5-10_Rasia_1e7_Tvir_thresh_3.0.txt"
#filename = "L-T_data_apec_0.5-10_Rasia_1e7.txt"
h = 0.679

fig = plt.figure(figsize=(8,8))#width,height

## For median profiles
xmin, xmax = [3e12,7e13]
num = int((np.log10(xmax) - np.log10(xmin/10.0)) / 0.2)
bins = np.logspace(np.log10(xmin/10.0), np.log10(xmax), num=num)
x = 10**((np.log10(bins[:-1]) + np.log10(bins[1:]))/2)

from palettable.colorbrewer.qualitative import Dark2_8 as pallette
col=pallette.hex_colors
markers = ["D", "s", "o"]
ls=['dashdot', 'dotted', 'dashed', 'solid']
ms = 6.5
lw=2
mew=1.2

data = np.loadtxt("L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_"+str(snapnum).zfill(3)+"/"+filename)
plt.plot(data[:,12] * 1e10 / h, data[:,13]/data[:,12], marker=markers[2], mfc='none', mec=col[3], ms=ms, linestyle='none', mew=mew, zorder=4)
#median is plotted below
#for zoom in ["c96", "c128_fixed", "c160", "c192", "c256_fixed", "c320", "c384_fixed", "c448"]:
#==============================================================================
# for zoom in ["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c256_box_Arepo_new", "c384_box_Arepo_new"]:
#     data = np.loadtxt("/home/nh444/data/project1/L-T_relations/LDRIntRadioEffBHVel/"+zoom+"_"+str(snapnum).zfill(3)+"/"+filename)
#     plt.plot(data[12] * 1e10 / h, data[13]/data[12], marker='o', mfc=col[3], mec=col[3], mew=mew, ms=ms, linestyle='none', zorder=4)
#==============================================================================
c = ["#c7673e","#78a44f","#6b87c8","#934fce","#e6ab02","#be558f","#47af8d"]
mec = ['none', 'k']
#==============================================================================
# for i, zoom in enumerate(["c96_box_Arepo_new", "c96_box_Arepo_new_LowSoft", "c128_box_Arepo_new", "c128_box_Arepo_new_LowSoft", "c160_box_Arepo_new", "c160_box_Arepo_new_LowSoft", "c256_box_Arepo_new", "c256_box_Arepo_new_LowSoft", "c320_box_Arepo_new", "c320_box_Arepo_new_LowSoft", "c384_box_Arepo_new", "c384_box_Arepo_new_LowSoft"]):
#     data = np.loadtxt("/home/nh444/data/project1/L-T_relations/LDRIntRadioEffBHVel/"+zoom+"_"+str(snapnum).zfill(3)+"/"+filename)
#     plt.plot(data[12] * 1e10 / h, data[13]/data[12], marker='D', mfc=c[int(i/2)], mec=mec[(i+1)%2], mew=0.9, ms=8, linestyle='none', zorder=4, label=zoom.replace("_","\_"))
#==============================================================================
#==============================================================================
# for i, zoom in enumerate(["c96_box_Arepo_new", "c96_box_Arepo_new_BHVel", "c96_box_Arepo_new_LowSoft", "c96_box_Arepo_new_LowSoft_BHVel", "c96_box_Arepo_new_LowSoft_BHVel_Ngb"]):#, "c128_box_Arepo_new", "c160_box_Arepo_new", "c256_box_Arepo_new", "c320_box_Arepo_new", "c384_box_Arepo_new"]):
#     data = np.loadtxt("/home/nh444/data/project1/L-T_relations/LDRIntRadioEffBHVel/"+zoom+"_"+str(snapnum).zfill(3)+"/"+filename)
#     plt.plot(data[12] * 1e10 / h, data[13]/data[12], marker='D', mfc=c[i], mec=c[i], mew=0.9, ms=8, linestyle='none', zorder=4, label=zoom.replace("_","\_"))
#==============================================================================
#==============================================================================
# for i, zoom in enumerate(["c96_box_Arepo_new_LowSoft", "c96_box_Arepo_new_LowSoft_BHVel", "c96_box_Arepo_new_LowSoft_BHVel_Ngb"]):#"c128_box_Arepo_new_LowSoft","c160_box_Arepo_new_LowSoft","c256_box_Arepo_new_LowSoft","c320_box_Arepo_new_LowSoft","c384_box_Arepo_new_LowSoft"]):
#     data = np.loadtxt("/home/nh444/data/project1/L-T_relations/LDRIntRadioEffBHVel/"+zoom+"_"+str(snapnum).zfill(3)+"/"+filename)
#     plt.plot(data[12] * 1e10 / h, data[13]/data[12], marker='D', mfc='none', mec=c[i], mew=2.0, ms=8, linestyle='none', zorder=4, label=zoom.replace("_","\_"))
# 
#==============================================================================

data = np.loadtxt("L40_512_NoDutyRadioWeak_HalfThermal_FixEta_"+str(snapnum).zfill(3)+"/"+filename)#0.3-10_fit_range
#plt.plot(data[:,12] * 1e10 / h, data[:,13]/data[:,12], marker=markers[0], mec='none', mfc=col[0], ms=ms, linestyle='none', mew=mew, zorder=4)
med, bin_edges, n = stats.binned_statistic(data[:,12] * 1e10 / h, data[:,13]/data[:,12], statistic='median', bins=bins)
plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[0], ls=ls[2], lw=lw, dash_capstyle='round', label=labels[0], zorder=4)

data = np.loadtxt("L40_512_NoDutyRadioInt_HalfThermal_FixEta_"+str(snapnum).zfill(3)+"/"+filename)#0.3-10_fit_range
#plt.plot(data[:,12] * 1e10 / h, data[:,13]/data[:,12], marker=markers[0], mec='none', mfc=col[1], ms=ms, linestyle='none', mew=mew, zorder=4)
med, bin_edges, n = stats.binned_statistic(data[:,12] * 1e10 / h, data[:,13]/data[:,12], statistic='median', bins=bins)
plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[1], ls=ls[1], lw=lw, dash_capstyle='round', label=labels[1], zorder=4)

data = np.loadtxt("L40_512_LowDutyRadioWeak_HalfThermal_FixEta_"+str(snapnum).zfill(3)+"/"+filename)#0.3-10_fit_range
#plt.plot(data[:,12] * 1e10 / h, data[:,13]/data[:,12], marker=markers[1], mec='none', mfc=col[2], ms=ms, linestyle='none', mew=mew, zorder=4)
med, bin_edges, n = stats.binned_statistic(data[:,12] * 1e10 / h, data[:,13]/data[:,12], statistic='median', bins=bins)
plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[2], ls=ls[0], lw=lw, dash_capstyle='round', label=labels[2], zorder=4)

data = np.loadtxt("L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_"+str(snapnum).zfill(3)+"/"+filename)#0.3-10_fit_range
med, bin_edges, n = stats.binned_statistic(data[:,12] * 1e10 / h, data[:,13]/data[:,12], statistic='median', bins=bins)
plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[3], ls=ls[3], lw=lw, dash_capstyle='round', label=labels[3], zorder=4)#(APEC $T_{min}=0.01$ keV)

## box with larger softenings
data = np.loadtxt("L40_512_LDRIntRadioEffBHVel_"+str(snapnum).zfill(3)+"/"+filename)
plt.plot(data[:,12] * 1e10 / h, data[:,13]/data[:,12], marker=markers[2], mfc='none', mec=col[4], ms=ms, linestyle='none', mew=mew, zorder=4)
med, bin_edges, n = stats.binned_statistic(data[:,12] * 1e10 / h, data[:,13]/data[:,12], statistic='median', bins=bins)
plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[4], ls=ls[3], lw=lw, dash_capstyle='round', label=labels[3]+' (larger softening)', zorder=4)
for zoom in ["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c256_box_Arepo_new", "c320_box_Arepo_new", "c384_box_Arepo_new"]:
    data = np.loadtxt("/home/nh444/data/project1/L-T_relations/LDRIntRadioEffBHVel/"+zoom+"_"+str(snapnum).zfill(3)+"/"+filename)
    plt.plot(data[12] * 1e10 / h, data[13]/data[12], marker='D', mfc='none', mec=col[4], mew=1.5, ms=ms, linestyle='none', zorder=4)
    
#==============================================================================
# ## without cuts or T thresh
# data = np.loadtxt("L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_0.5-10_none.txt")
# med, bin_edges, n = stats.binned_statistic(data[:,12] * 1e10 / h, data[:,13]/data[:,12], statistic='median', bins=bins)
# plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c='k', lw=lw, ls='dashed', dash_capstyle='round', label=labels[3]+" (no cuts, no T thresh)", zorder=4)#(APEC $T_{min}=0.01$ keV)
# for zoom in ["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c256_box_Arepo_new", "c320_box_Arepo_new", "c384_box_Arepo_new"]:
#     data = np.loadtxt("/home/nh444/data/project1/L-T_relations/LDRIntRadioEffBHVel/"+zoom+"_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_0.5-10_none.txt")
#     plt.plot(data[12] * 1e10 / h, data[13]/data[12], marker='o', mfc='k', mec='k', mew=mew, ms=ms, linestyle='none', zorder=4)
# plt.plot(9.14924712135e+14, 0.154307, marker='o', mfc='k', mec='k', mew=mew, ms=ms, linestyle='none', zorder=4)
# 
# ## with cuts but no T thresh
# data = np.loadtxt("L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_0.5-10_Rasia_1e7.txt")
# plt.plot(data[:,12] * 1e10 / h, data[:,13]/data[:,12], marker=markers[2], mfc='none', mec=col[3], ms=ms, linestyle='none', mew=mew, zorder=4)
# med, bin_edges, n = stats.binned_statistic(data[:,12] * 1e10 / h, data[:,13]/data[:,12], statistic='median', bins=bins)
# plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[3], ls=ls[3], lw=lw, dash_capstyle='round', label=labels[3]+" ($N=10^7$, no T thresh)", zorder=4)#(APEC $T_{min}=0.01$ keV)
# for zoom in ["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c256_box_Arepo_new", "c320_box_Arepo_new", "c384_box_Arepo_new"]:
#     data = np.loadtxt("/home/nh444/data/project1/L-T_relations/LDRIntRadioEffBHVel/"+zoom+"_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_0.5-10_Rasia_1e7.txt")
#     plt.plot(data[12] * 1e10 / h, data[13]/data[12], marker='o', mfc=col[3], mec=col[3], mew=mew, ms=ms, linestyle='none', zorder=4)
# plt.plot(61977.0*1e10/0.6774, 0.154055, marker='o', mfc=col[3], mec=col[3], mew=mew, ms=ms, linestyle='none', zorder=4)
#==============================================================================
#==============================================================================
# ## without cuts with T thresh
# data = np.loadtxt("L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_0.5-10_none_Tvir_thresh_2.5.txt")
# med, bin_edges, n = stats.binned_statistic(data[:,12] * 1e10 / h, data[:,13]/data[:,12], statistic='median', bins=bins)
# plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c='0.3', lw=lw, ls='dashed', dash_capstyle='round', label=labels[3]+" (no cuts, with T thresh)", zorder=4)#(APEC $T_{min}=0.01$ keV)
# for zoom in ["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c256_box_Arepo_new", "c384_box_Arepo_new"]:
#     data = np.loadtxt("/home/nh444/data/project1/L-T_relations/LDRIntRadioEffBHVel/"+zoom+"_"+str(snapnum).zfill(3)+"/"+"L-T_data_apec_0.5-10_none_Tvir_thresh_2.5.txt")
#     plt.plot(data[12] * 1e10 / h, data[13]/data[12], marker='o', mfc='none', mec='0.3', mew=mew, ms=ms, linestyle='none', zorder=4)
# 
#==============================================================================

lw = 2
Vikh = np.genfromtxt(obs_dir+"Vikhlinin2006.csv", delimiter=",")
Sun = np.genfromtxt(obs_dir+"sun_group_x-ray_properties.txt", usecols=np.arange(0,24))
Gastal = np.genfromtxt(obs_dir+"gastaldello_M500.txt")
Gio = np.genfromtxt(obs_dir+"Giodini09_baryon_fracs.txt")
Arn = np.genfromtxt(obs_dir+"Arnaud2007_gas_fracs.csv", delimiter=",")
Ill =  np.genfromtxt(obs_dir+"Illustris_gas_fracs.txt")
Gonz = np.genfromtxt(obs_dir+"Gonzalez_2013_gas_fracs.txt")
Lov = np.genfromtxt(obs_dir+"Lovisari2015.csv", delimiter=",")
eck = np.genfromtxt(obs_dir+"Eckmiller2011.csv", delimiter=",")


c = '0.7'
plt.errorbar(Vikh[:,17]*1e14*0.72/h, Vikh[:,23], xerr=Vikh[:,18], yerr=Vikh[:,24], c=c, mec='none', marker='s', linestyle='none', ms=5, label="Vikhlinin et al. 2006")#z < 0.23 #ff471a
mask = (np.arange(len(Gastal[:,0]))!=12)
plt.errorbar(Gastal[mask,0]*0.7/h, Gastal[mask,6], xerr=[Gastal[mask,1],Gastal[mask,2]], yerr=[Gastal[mask,7],Gastal[mask,8]], c=c, mec='none', marker='D', linestyle='none', ms=5, label="Gastaldello et al. 2007")# z< 0.08 #cc99ff
plt.errorbar(Arn[:,5]*1e14*0.7/h, Arn[:,14], xerr=[np.absolute(Arn[:,7])*1e14*0.7/h, Arn[:,6]*1e14*0.7/h], yerr=[np.absolute(Arn[:,16]), Arn[:,15]], c=c, mec='none', marker='v', linestyle='none', ms=8, label="Arnaud et al. 2007")
plt.errorbar(Sun[:,13]*1e13*0.73/h, Sun[:,16], xerr=[Sun[:,3],Sun[:,2]], yerr=[Sun[:,18],Sun[:,17]], c=c, mec='none', marker='<', linestyle='none', ms=7, label="Sun et al. 2008")# 0.012 <z< 0.12 # '#90AC28' #ffa64d
plt.errorbar(Gio[:,0]*0.72/h, Gio[:,5], xerr=[Gio[:,1], Gio[:,2]], yerr=Gio[:,6], c=c, mec='none', marker='o', linestyle='none', ms=5, label="Giodini et al. 2009")
plt.errorbar(Gonz[:,2]*1e14*0.702/h, Gonz[:,4], xerr=Gonz[:,3]*1e14*0.702/h, yerr=Gonz[:,5], c=c, mec='none', marker='*', ls='none', ms=8, label="Gonzalez et al. 2013")
plt.errorbar(Lov[:,7]*1e13*0.7/h, Lov[:,11], xerr=Lov[:,8]*1e13*0.7/h, yerr=Lov[:,12], c=c, mec='none', marker="^", ls='none', ms=7, label="Lovisari+ 2015")
#plt.errorbar(eck[:,14]*1e13*0.7/h, eck[:,26]*(0.7/h)**1.5, xerr=eck[:,15]*1e13*0.7/h, yerr=eck[:,27]*(0.7/h)**1.5, c=c, mec='none', marker='>', ls='none', ms=7, label="Eckmiller+ 2011")
plt.errorbar(Ill[:,0], Ill[:,1], c=c, ls='dotted', dash_capstyle='round', dashes=(1,6), lw=lw, label="Illustris")

plt.ylabel(r"M$_{\mathrm{gas}}$/M$_{500}$")#, fontsize=16)
plt.xlabel(r"M$_{500}$ [M$_{\odot}$]")#, fontsize=16)
plt.xlim(xmin, 2e15)
plt.ylim(0.0, 0.2)
plt.xscale('log')
#plt.yscale('log')

handles, labels = plt.gca().axes.get_legend_handles_labels()
n = 5
leg1 = plt.legend(handles[:n], labels[:n], loc='lower right', frameon=False, numpoints=1, fontsize='x-small', ncol=1)
ax = plt.gca().add_artist(leg1)
plt.legend(handles[n:], labels[n:], loc='upper left', frameon=False, borderaxespad=1, numpoints=1, fontsize='x-small', ncol=2)
#plt.legend(loc='upper left', frameon=False, borderaxespad=1, numpoints=1, ncol=2)
## Set axis labels to decimal format not scientific when using log scale
#plt.gca().yaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))
#plt.setp(plt.gca().get_xticklabels(), fontsize=16)
#plt.setp(plt.gca().get_yticklabels(), fontsize=16)

plt.tight_layout(pad=0.45)
plt.savefig(outfilename)#, bbox_inches='tight')
print "Plot saved to", outfilename
