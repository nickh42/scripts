import sys
import numpy as np
import matplotlib.pyplot as plt

if len(sys.argv) > 1:
    filename = sys.argv[1]
else:
    filename = None

fig = plt.figure(figsize=(10,8))
fig.patch.set_facecolor('white')

if filename is not None:
    data = np.genfromtxt(filename, skip_header=1).reshape((-1, 6))
    
    Zbins = np.unique(data[:,2])
    #binlabels = np.digitize(data[:,2], Zbins)
    #EMbins = [data[binlabels == i, 5].sum() for i in range(1, len(Zbins))]
    #plt.step(Zbins[:-1], EMbins, where='post')
    plt.hist(data[:,2], bins=Zbins, weights=data[:,5])
    plt.xlabel("Abundance")
    plt.ylabel("Norm (total)")
    plt.show()

else:
    for i in range(6, 10):
        filename = "./group_"+str(i)+"/xspec_model.txt"
        data = np.genfromtxt(filename, skip_header=1).reshape((-1, 6))
        Zbins = np.unique(data[:,2])
        plt.hist(data[:,2], bins=Zbins, weights=data[:,5], label="group_"+str(i), histtype='step')
    plt.xlabel("Metallicity (Z$_{\odot}$)")
    plt.ylabel("Total norm")
    plt.xlim(0.0, 0.9)
    plt.legend(loc='upper left', frameon=False, borderaxespad=1, numpoints=1, fontsize=12)
    plt.savefig("metallicity_EM_distribution.pdf")
