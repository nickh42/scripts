#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 12 15:03:42 2016
This simply compares the spectral temperatures output by get_L-T.py and xray.py to weighted temperatures
@author: nh444
"""
import numpy as np
import matplotlib.pyplot as plt

indir = "/data/curie4/nh444/project1/L-T_relations/"
dirname = "L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff_025"
#filename = "L-T_data_apec_default2.txt"
filename = "L-T_data_apec_0.5-2.txt"

data = np.loadtxt(indir+dirname+"/"+filename)
Tspec = data[:,5]
Tmw = data[:,14] ## mass-weighted
Tbw = data[:,15] ## bremsstrahlung emission-weighted
Tew = data[:,16] ## total emission-weighted
abund = data[:,8]

fig, ax = plt.subplots()

ax.plot(Tmw, Tspec / Tmw, '+', c='green', ls='none', label="Mass-weighted")
ax.plot(Tbw, Tspec / Tbw, 'x', c='#ff3e00', ls='none', label="Bremsstrahlung Emission-weighted")
ax.plot(Tew, Tspec / Tew, '*', c='red', mec='none', ls='none', label="Total Emission-weighted")
ax.axhline(y=1.0, c='0.5', ls='dashed')

ax.set_xlabel("Weighted temperature")
#ax.set_xlabel("Abundance")
ax.set_ylabel("Spectral Temperature / Weighted Temperature")
#ax.set_xlim(0.3, 10)
#ax.set_ylim(0.5, 1.5)
ax.set_xscale('log')
ax.legend(frameon=False, numpoints=1)
ax.set_title(dirname.replace("_", "\_"))
plt.show()
