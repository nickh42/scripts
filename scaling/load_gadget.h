#include <sstream>
#include <cassert>
#include <iostream>

struct my_vector
{
  float x,y,z;
};

extern int swapstate;

size_t my_fread(void *ptr, size_t size, size_t nmemb, FILE * stream);

void swap_Nbyte(char *data,int n,int m);

int find_block_format1(FILE *fd,char *label);

int find_block(FILE *fd,char *label);

int read_gadget_head(int *npart,double *massarr,double *time,double *redshift,FILE *fd);

int read_gadget_head_full(int *npart,double *massarr,double *time,double *redshift,
                          int *sfr, int *feedback, int *nall, int *cooling, int *filenum,   
                          double *boxsize, double *omega_m, double *omega_l, double *hubble,FILE *fd);

int read_gadget_float(float *data,char *label,FILE *fd);

int read_gadget_float3(float *data,char *label,FILE *fd);

//-------------- Routine to read from snapshot that is split on multiple files -------------

template <class datatype>
int read_gadget_multi (datatype *&data_inout, std::string label, std::string filename)
{
  float *data = (float*) data_inout;
  FILE *fd;  

  if(!(fd = fopen(filename.c_str(),"rb")))
  {
    if(!(fd = fopen((filename+".0").c_str(),"rb")))
    {
      std::cout << "ERROR: Cant open file " << filename << " or file "<< filename+".0" << "!" << std::endl;
      exit(2);
    }
  }

  //header data
  int npart[6],nall[6]; 
  int cooling,sfr,feedback,filenum_first,filenum;
  double masstab[6],redshift,time;
  double boxsize,omega_m,omega_l,hubble;

  read_gadget_head_full(npart,masstab,&time,&redshift,&sfr,&feedback,nall,
                        &cooling,&filenum_first,&boxsize,&omega_m,&omega_l,&hubble,fd);
  fclose(fd);

  bool data_for_type[6];
  for (int i=0; i<6; ++i) data_for_type[i] = false;
  int floats_per_particle = 1;
 
  if (label == "POS ") {for (int i=0; i<6; ++i) data_for_type[i] = true; floats_per_particle = 3;}
  else if (label == "VEL ") {for (int i=0; i<6; ++i) data_for_type[i] = true; floats_per_particle = 3;}
  else if (label == "ID  ") for (int i=0; i<6; ++i) data_for_type[i] = true;
  else if (label == "MASS") {for (int i=0; i<6; ++i) if (masstab[i]==0 && nall[i]>0) data_for_type[i] = true;} 
  else if (label == "RHO ") data_for_type[0] = true;
  else if (label == "HSML") data_for_type[0] = true;
  else if (label == "U   ") data_for_type[0] = true;
  else if (label == "NE  ") data_for_type[0] = true;
  else if (label == "Z   ") {data_for_type[0] = true; data_for_type[4] = true;}
  else if (label == "TEMP") data_for_type[0] = true;
  else if (label == "MHI ") data_for_type[0] = true;
  else {std::cout << "ERROR: unknown block label \"" << label << "\" in read_gadget_float_multi!" << std::endl; exit(2);}

  int data_size = 0;
  int data_pos[6];
  int data_end[6];
  for (int i=0; i<6; ++i) 
  {
    data_pos[i] = data_size;
    if (data_for_type[i]) data_size += nall[i]*floats_per_particle;
    data_end[i] = data_size;
  }

  delete[] data;
  data = new float[data_size];

  std::stringstream strs;
  char curfilename[256];
  int floats_read = 0;
  float *curdata = 0;

  for (int curfile=0; curfile<filenum_first; ++curfile)
  {

    strs << filename;
    if (filenum_first > 1) strs << "." << curfile;
    strs << std::endl;
    strs.getline(curfilename,256); 
  
    if(!(fd = fopen(curfilename,"rb")))
    {
      std::cout << "ERROR: Cant open file " << curfilename << "!" << std::endl;
      exit(2);
    }

    read_gadget_head_full(npart,masstab,&time,&redshift,&sfr,&feedback,nall,
                          &cooling,&filenum,&boxsize,&omega_m,&omega_l,&hubble,fd);
   
    int curdata_size = 0;
    for (int i=0; i<6; ++i) if (data_for_type[i]) curdata_size += npart[i]*floats_per_particle;
    curdata = new float[curdata_size];   

    floats_read += read_gadget_float(curdata,(char*)label.c_str(), fd);

    int curdatapos = 0;
    for (int i=0; i<6; ++i)
      if (data_for_type[i])
      {
        for (int j=0; j<floats_per_particle*npart[i]; ++j)
        {
          data[data_pos[i]] = curdata[curdatapos];
          ++data_pos[i];
          ++curdatapos;
        }
      }
    assert(curdatapos==curdata_size);

    delete[] curdata;
    curdata = 0;
    fclose(fd);
  }
  
  for (int i=0; i<6; ++i) assert(data_pos[i]==data_end[i]);
  assert(floats_read==data_size);

  data_inout = (datatype*) data;

  return data_size;
}
