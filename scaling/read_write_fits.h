/* write float images
filename - name of to image file to be created
width - width of the image in pixels
height - height of the image in pixels
array - pointer to the first element of the array to be written into the image
*/

#include <fitsio.h>
#include <typeinfo>

class image_info{

public:
  int width;
  int height;
  int type;
  bool error;
  
  image_info (){width = 0; height = 0; type = 0; error = true;}
};

template <class datatype, class filetype>
int write_fits (std::string filename, const int width, const int height, datatype *array, 
                float firstpixx = 0, float firstpixy = 0, float incperpixx = 0, float incperpixy = 0)
{
    fitsfile *fptr;
    int status = 0;
    long fpixel = 1;
    const long naxis = 2;
    long naxes[naxis] = {width,height}; 
    long nelements = naxes[0] * naxes[1];
    int imagetype, datawritetype;
    filetype *data;
       
    if (typeid(filetype) == typeid(float)) {imagetype = FLOAT_IMG; datawritetype = TFLOAT;}
    if (typeid(filetype) == typeid(double)) {imagetype = DOUBLE_IMG; datawritetype = TDOUBLE;}
    if (typeid(filetype) == typeid(int)) {imagetype = LONG_IMG; datawritetype = TLONG;}
    
    if (typeid(datatype) != typeid(filetype)) 
    {
      data = new filetype[width*height];
      for (int j=0; j<width*height; ++j) data[j] = filetype(array[j]);
    }
    else data = (filetype*) array;
    
    // create new file 
    fits_create_file(&fptr, filename.c_str(), &status);
    // create the primary array image 
    fits_create_img(fptr, imagetype, naxis, naxes, &status);
    // write coordinate system if given
    if (incperpixx != 0 && incperpixy != 0)
    {
      float crpix=1;
      fits_update_key(fptr, TFLOAT, (char*)"CRPIX1", &crpix, (char*)"x index of reference pixel", &status);
      fits_update_key(fptr, TFLOAT, (char*)"CRPIX2", &crpix, (char*)"x index of reference pixel", &status);
      fits_update_key(fptr, TFLOAT, (char*)"CRVAL1", &firstpixx, (char*)"x coordinate of reference pixel", &status);
      fits_update_key(fptr, TFLOAT, (char*)"CRVAL2", &firstpixy, (char*)"y coordinate of reference pixel", &status);
      fits_update_key(fptr, TFLOAT, (char*)"CDELT1", &incperpixx, (char*)"x coordinate change per pixel", &status);
      fits_update_key(fptr, TFLOAT, (char*)"CDELT2", &incperpixy, (char*)"y coordinate change per pixel", &status);
    }
    // Write the array to the image
    fits_write_img(fptr, datawritetype, fpixel, nelements, data, &status);
    // close the file 
    fits_close_file(fptr, &status);
    // print out any error messages     
    fits_report_error(stderr, status);
    
    if (typeid(datatype) != typeid(filetype)) delete[] data; 
      
    return(status);
}

template <class datatype, class filetype>
datatype *read_data (fitsfile *fptr, int width, int height, int datareadtype)
{
  int status = 0;
  long fpixel[2]={1,1};
  filetype *inarray = new filetype[width*height];
  datatype *outarray;
  
  std::cout << "axe 1 : " << width << std::endl;
  std::cout << "axe 2 : " << height << std::endl;
  
  fits_read_pix(fptr, datareadtype, fpixel, width*height, 0, inarray, 0, &status);
  if (typeid(datatype)!=typeid(filetype))
  {
    std::cout << "converting FITS data from " << typeid(filetype).name() << " to " << typeid(datatype).name() << std::endl;
    outarray = new datatype[width*height];
    for (int j=0; j<width*height; ++j) outarray[j] = (datatype) inarray[j];
    delete[] inarray;
  }
  else outarray = (datatype*) inarray;
  return(outarray);
}

template <class datatype>
image_info read_fits (std::string filename, datatype *&outarray)
{
    fitsfile *fptr;       
    int status = 0, bitpix, naxis;
    long naxes[2];
    int datareadtype;
    image_info info;
    
    if (!fits_open_file(&fptr, filename.c_str(), READONLY, &status))
    {
      std::cout << "loading " << filename << std::endl;
      if (!fits_get_img_param(fptr, 2, &bitpix, &naxis, naxes, &status))
        {
          if (naxis != 2) std::cout << "Error: only 2D images are supported" << std::endl;
          else
          {
            info.width = naxes[0];
            info.height = naxes[1];
            info.type = bitpix;
            if (bitpix == -32) 
            {
              datareadtype = TFLOAT;
              outarray = read_data<datatype,float>(fptr,naxes[0],naxes[1],datareadtype); 
              info.error = false;
            }
            else if (bitpix == -64)
            {
              datareadtype = TDOUBLE;
              outarray = read_data<datatype,double>(fptr,naxes[0],naxes[1],datareadtype);
              info.error = false;
            }
            else if (bitpix == 32)
            {
              datareadtype = TLONG;
              outarray = read_data<datatype,int>(fptr,naxes[0],naxes[1],datareadtype);
              info.error = false;
            }   
            else
            {
              std::cout << "ERROR: unsupported image type" << std::endl;
              outarray = 0;
            }
          }
        }
      else
      {
        std::cout << "ERROR: could not get image parameters" << std::endl;
        outarray = 0;      
      }
    }
    else 
    {
      std::cout << "ERROR: unable to open FITS file" << std::endl;
      outarray = 0;
    }
    return(info);
}
