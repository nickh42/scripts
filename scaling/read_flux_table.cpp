#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <fstream>
#include <cassert>

#include "read_key.h"
#include "read_flux_table.h"

using namespace std;

channel_data::channel_data (const char *filename, int num)
{
  ifstream fluxfile (filename);
  
  read_key<double>(fluxfile,"redshift",redshift);
  if (!read_key<double>(fluxfile,"nHwabs",nHwabs,false)) nHwabs = 0;
  read_key<int>(fluxfile,"abund. num.",abundnum);
  read_key<double>(fluxfile,"min. abund.",minabund);
  read_key<double>(fluxfile,"max. abund.",maxabund);
  read_key<int>(fluxfile,"temp. num.",tempnum);
  read_key<double>(fluxfile,"min. temp.",mintemp);
  read_key<double>(fluxfile,"max. temp.",maxtemp);
  
  char buffer[256];
  find_pos(fluxfile,"ch. num. , ch. min. , ch. max. , abund. num., abund. , temp. num. , temp. , flux");
  fluxfile.getline(buffer,256);
  fluxfile >> channel;
  while (channel!=num && !fluxfile.eof()) {fluxfile.getline(buffer,256); fluxfile >> channel;}
  
  if (channel==num)
  {
    fluxfile >> chmin >> chmax;
    
    data = new double[abundnum*tempnum];
    int cur_abund, cur_temp, cur_channel;
    double doublebuff;
    
    find_linestart(fluxfile);
    
    for (int i_abund=0; i_abund<abundnum; ++i_abund)
    for (int i_temp=0; i_temp<tempnum; ++i_temp)
    {
      fluxfile >> cur_channel >> doublebuff >> doublebuff >> cur_abund >> doublebuff >> cur_temp >> doublebuff;
      assert(cur_channel==channel && cur_abund==i_abund && cur_temp==i_temp);
      fluxfile >> data[i_abund*tempnum+i_temp];//this last column should be the flux column
    }  
  }
}

double channel_data::getflux(double abund, double temp, bool &inside_range)
{
  int lower_abund, upper_abund, lower_temp, upper_temp;
  double flux, d_abund=0, d_temp=0;
  
  if (temp>=mintemp && temp<=maxtemp && abund>=minabund && abund<=maxabund) inside_range = true;
  else 
  {
    inside_range = false;
    //cout << "temperature = " << temp << ", abundance = " << abund << endl;
  }
  
  if (temp<=mintemp) {lower_temp=0; upper_temp=0;}
  else if (temp>=maxtemp) {lower_temp=tempnum-1; upper_temp=tempnum-1;}
  else
  {
    lower_temp = int((temp-mintemp)/((maxtemp-mintemp)/(tempnum-1)));
    d_temp = (temp-mintemp)/((maxtemp-mintemp)/(tempnum-1)) - lower_temp;
    upper_temp = lower_temp;
    if (lower_temp<tempnum-1) ++upper_temp;
  }
  
  if (abund<=minabund) {lower_abund=0; upper_abund=0;}
  else if (abund>=maxabund) {lower_abund=abundnum-1; upper_abund=abundnum-1;}
  else
  {
    lower_abund = int((abund-minabund)/((maxabund-minabund)/(abundnum-1)));
    d_abund = (abund-minabund)/((maxabund-minabund)/(abundnum-1)) - lower_abund;
    upper_abund = lower_abund;
    if (lower_abund<abundnum-1) ++upper_abund;
  }
  
  //cout << lower_abund << "," << upper_abund << "  " << lower_temp << "," << upper_temp << endl;
  
  flux = data[lower_abund*tempnum+lower_temp]*(1-d_abund)*(1-d_temp)+
         data[upper_abund*tempnum+lower_temp]*d_abund*(1-d_temp)+
         data[lower_abund*tempnum+upper_temp]*(1-d_abund)*d_temp+
         data[upper_abund*tempnum+upper_temp]*d_abund*d_temp;
         
  return(flux);
}

void channel_data::show()
{
  cout << "------------------------------------" << endl;
  cout << "channel data parameters" << endl << endl;
  cout << "channel = " << channel << endl;
  cout << "channel min. = " << chmin << endl;
  cout << "channel max. = " << chmax << endl;
  cout << "redshift = " << redshift << endl;
  cout << "nHwabs = " << nHwabs << endl;
  cout << "tempnum = " << tempnum << endl;
  cout << "min. temp. = " << mintemp << endl;
  cout << "max. temp. = " << maxtemp << endl;
  cout << "abundnum = " << abundnum << endl;
  cout << "min. abund. = " << minabund << endl;
  cout << "max. abund. = " << maxabund << endl;
  cout << "------------------------------------" << endl;
}

/*int main()
{
  ifstream fluxfile("fluxTable_mekal_wabs.d");
  
  //double x;
  //if (read_key<double>(fluxfile,"response matrix",x)) cout << x << endl;
  //else cout << "ERROR" << endl;

  find_pos(fluxfile,"d. num",false);
  find_linestart(fluxfile);
  char buffer[32];
  fluxfile.getline(buffer,32);
  cout << buffer << endl;
}*/

/*int main()
{
  channel_data data("fluxTable_mekal_wabs.d",0);
  data.show();
  cout << data.getflux(0.35,7) << endl;
}*/
