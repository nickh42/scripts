"""
Compute, save and plot SZ signal within R500 as a function of halo mass
"""

def main():
    simdir = "/data/emergence4/deboras/Elliptical_ZoomIns/"
    sims = ["L40_512_NoDutyRadioWeak_HalfThermal_FixEta",
            "L40_512_NoDutyRadioInt_HalfThermal_FixEta",
            "L40_512_LowDutyRadioWeak_HalfThermal_FixEta",
            "L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff",
            ]
    labels = ["Ref",
              "Stronger radio",
              "Quasar duty cycle",
              "Combined"]
    simdir = "/data/curie4/nh444/project1/boxes/"
    sims = [#"L40_512_NoDutyRadioWeak",
               #"L40_512_NoDutyRadioInt",
               #"L40_512_DutyRadioWeak",
               "L40_512_LDRIntRadioEffBHVel",
               ]
    labels = [#"Weak radio",
              #"Stronger radio",
              #"Quasar duty cycle",
              "Combined"]
    
    sims = [#"L40_512_NoDutyRadioWeak",
               #"L40_512_NoDutyRadioInt",
               #"L40_512_DutyRadioWeak",
               #"L40_512_LDRIntRadioEffBHVel",
               "L40_512_MDRIntRadioEff",
               ]
    labels = [#"Weak radio",
              #"Stronger radio",
              #"Quasar duty cycle",
              #"$\delta t = 10$ Myr",
              "$\delta t = 25$ Myr",
              ]

    zoombasedir = "/home/nh444/data/project1/zoom_ins/"
    zoomdirs = [#"","","",
                #"LDRIntRadioEffBHVel/",
                "MDRInt/",
                ] ## one for each in sims
    zooms = [#["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c192_box_Arepo_new", "c256_box_Arepo_new", "c320_box_Arepo_new", "c384_box_Arepo_new", "c448_box_Arepo_new"],
             #["c96_box_Arepo_new", "c128_box_Arepo_new", "c192_box_Arepo_new", "c256_box_Arepo_new", "c384_box_Arepo_new"],
             ["c96_MDRInt", "c128_MDRInt", "c192_MDRInt", "c256_MDRInt",
              "c320_MDRInt", "c384_MDRInt", "c448_MDRInt","c512_MDRInt",]
              ]
    
    snapnum = 25

    h_scale = 0.679 #value of h to scale output values and observation values to
    cylinder = False ## calculate SZ in cylinder of radius 5r500, otherwise spherical aperture of radius r500 or 5r500 if use5r500=True
    use5r500 = True ## if True and not cylinder, calculate SZ in spherical 5r500, otherwise spherical r500
    plot_5r500 = use5r500
    use_fof_rad = False
    #temp_thresh = 4.0
    #filter_type = "default"
    #suffix = "_def_Tvir4.0" ## appends to output SZ_M500 files (needs underscore)
    temp_thresh = -1
    filter_type = "none"
    suffix = "" ## appends to output SZ_M500 files (needs underscore)
    col = ['#24439b', '#ED7600'] ## blue orange
    mew=1.8
    ms=8
    
    #save_SZ_M500(simdir, sims, snapnum, cylinder=cylinder, use5r500=use5r500, temp_thresh=temp_thresh, filter_type=filter_type, use_fof_rad=use_fof_rad, suffix=suffix, h_scale=h_scale)
#    for i, zoomdir in enumerate(zoomdirs):
#        if zoomdir is not "":
#            save_SZ_M500(zoombasedir+zoomdir, zooms[i], snapnum, N=2, highres_only=True, cylinder=cylinder, use5r500=use5r500, outdir="/data/curie4/nh444/project1/SZ/"+zoomdir, suffix=suffix, temp_thresh=temp_thresh, filter_type=filter_type, use_fof_rad=use_fof_rad, h_scale=h_scale, mpc_units=True)
    plot_SZ_M500(sims, snapnum, scat_idx=[0,1], plot_med=True, for_paper=True, zoomdirs=zoomdirs, zooms=zooms, cylindrical=cylinder, plot_5r500=plot_5r500, suffix=suffix, labels=labels, col=col, mew=mew, ms=ms, h_scale=h_scale)

def save_SZ_M500(simdir, sims, snapnum, cylinder=True, use5r500=False, use_fof_rad=True, highres_only=False, main_halo_only=False, temp_thresh=-1, filter_type="Rasia", outdir="/data/curie4/nh444/project1/SZ/", suffix="", logM500min = 12.0, N=0, h_scale=0.7, mpc_units=False):
    ## If cylinder is true, SZ signal is found for cylinder of radius 5*R500 and converted to spherical R500 by constant conversion factor 1.796 (see Le Brun 2015 or Arnaud 2010)
    ## If use_fof_rad, uses parent FoF group R500 for each subhalo, otherwise R500 is recalculated
    ## logM500min = 12.0 ## log10 of minimum M500 (Msun) to compute Y500 for
    ## if N>0, will use only first N FoF groups e.g. N=1 for zooms
    import numpy as np
    import snap
    import os
    from astropy.cosmology import FlatLambdaCDM
    
    from scipy.constants import m_p as mproton ## in kg
    from scipy.constants import m_e as melectron ## in kg
    from scipy.constants import parsec as pc # in m
    from scipy.constants import pi
    mproton_in_g = mproton * 1000.0
    melectron_in_g = melectron * 1000.0
    Eelectron_in_kev = 510.99891 ## rest mass energy of electron in keV
    thomson = 6.6524587158e-29 #thomson cross-section m^2
    UnitMass_in_g = 10.0**10 * 1.9884430e33
    Xh = 0.76 # hydrogen mass fraction

    for sim in sims:
        print "\nStarting sim", sim
        s = snap.snapshot(simdir+sim+"/", snapnum, mpc_units=mpc_units)
        if N>0:
            M500min = 0.0
            Ntot = N
        else:
            M500min = 10**logM500min
            Ntot = len(np.where(s.cat.group_m_crit500*1.0e10 / h_scale > M500min)[0])
            print Ntot, "halos found above M500", M500min
            print "Loading masses..."
        if use_fof_rad:
            s.load_gasmass()
        else:
            s.load_mass() ## need all masses to calculate M500
        print "Loading positions..."
        if use_fof_rad:
            s.load_gaspos()
        else:
            s.load_pos() ## need all positions to calculate R500
        print "Calculating temperatures..."
        s.get_temp(del_ne=False)
        if main_halo_only:
            print "Getting hostsubhalos..."
            s.get_hostsubhalos()
        cm = FlatLambdaCDM(H0 = s.header.hubble*100.0, Om0=s.header.omega_m)
        ## Get gas particles
        if use_fof_rad:
            gasmass = s.gasmass
            gaspos = s.gaspos
            if main_halo_only:
                hostsubhalos = s.hostsubhalos[s.species_start[0]:s.species_end[0]]
                assert len(hostsubhalos) == len(gasmass)
        else:
            gasmass = s.mass[s.species_start[0]:s.species_end[0]] ## gas mass
            gaspos = s.pos[s.species_start[0]:s.species_end[0]] ## gas positions
            if main_halo_only:
                hostsubhalos = s.hostsubhalos[s.species_start[0]:s.species_end[0]]
        
        subhalos = []
        M500_list = []
        Y500_list = []
        j = 0
        if N>0:
            ngroups = N
        else:
            ngroups = s.cat.ngroups
        for group in range(0, ngroups):
            ## assume that if the group M500 is below the minimum then the subhalo will be as well
            ## This will almost always be the case unless the main halo is somehow hugely offset
            if s.cat.group_m_crit500[group]*1.0e10 / h_scale > M500min:
                subnum = s.cat.group_firstsub[group]##Planck LBGs are meant to be mostly central galaxies of their halos so only consider main halos
                if use_fof_rad:
                    M500 = s.cat.group_m_crit500[group]
                    R500 = s.cat.group_r_crit500[group] ##This is not guaranteed to be the same R500 as that of the main subhalo but probably close
                else:
                    M500, R500 = s.get_so_mass_rad(subnum, 500.0, ref="crit", subhalo=True, use_sysexit=False)
                ## will be -1,-1 if could not be found
                if M500*1.0e10 / h_scale > M500min:
                    if cylinder: ##cylinder with radius 5*R500 along z a distance 10*R500
                        rad = np.sqrt((s.rel_pos(gaspos[:,0], s.cat.sub_pos[subnum,0]))**2 + (s.rel_pos(gaspos[:,1], s.cat.sub_pos[subnum,1]))**2)
                        ind = np.where((rad < 5.0 * R500) & (np.abs(s.rel_pos(gaspos[:,2], s.cat.sub_pos[subnum,2])) < 10.0 * R500))[0]
                    elif use5r500: ## Get particles within spherical 5*R500
                        rad = s.nearest_dist_3D(gaspos, s.cat.sub_pos[subnum])
                        ind = np.where(rad < 5.0*R500)[0]
                    else: ## Get particles within spherical R500
                        rad = s.nearest_dist_3D(gaspos, s.cat.sub_pos[subnum])
                        ind = np.where(rad < R500)[0]
                    print "in radius =     ", len(ind)
                    if highres_only:
                        s.load_highresgasmass()
                        ind = np.intersect1d(ind, np.where(s.hrgm > 0.5*gasmass)[0], assume_unique=True)
                    if main_halo_only: ## Get gas particles bound to the main subhalo only
                        ind = np.intersect1d(ind, np.where(hostsubhalos == subnum)[0])
                        print "in main halo =  ", len(ind)
                    if temp_thresh > 0:
                        ind = np.intersect1d(ind, np.where(s.temp < temp_thresh*s.cat.group_T_crit200[group])[0], assume_unique=True)
                        print "in temp thresh =", len(ind)
                    if filter_type is not "none":
                        import scaling.xray_python.xray as xray
                        if not hasattr(s, "gasrho"):
                            s.load_gasrho()
                        ind = np.intersect1d(ind, xray.xray_source(s.header.redshift, s.header.hubble, s.header.omega_m).filter_phase_space(s.temp, s.gasrho, filter_type=filter_type, M500=M500, R500=R500/(1+s.header.redshift)), assume_unique=True)
                        print "in filter =     ", len(ind)
                    m = gasmass[ind]
                    NE = s.ne[ind] #electron abundance
                    temp = s.temp[ind] #gas kT in kev
                    electrons = m * UnitMass_in_g / (mproton_in_g + melectron_in_g) * Xh * NE / h_scale#s.header.hubble ## number of electrons per particle
                    #ne = rho * UnitMass_in_g / (mproton_in_g + melectron_in_g) * Xh * NE * s.header.hubble / (1.0e3*pc*100.0)**3.0
                    PV = electrons * temp #temp=kT, assume electron temp same as gas temp (equilibrium)
                    ## Mass-weighted sum
                    Y = np.sum(PV) * thomson / Eelectron_in_kev ## now in m^2
                    ## (Planck XI divide by DA^2 to get in arcmin^2)
                    print Y / (pc * 1e6)**2, "Mpc^2"
                    ## Scale to z=0 and DA=500 Mpc/arcmin
                    Y *= cm.efunc(s.header.redshift)**(-1.0) / (500.0*1.0e6*pc/(180.0/pi*60.0))**2.0
                    #if cylinder: Y /= 1.796 ##conversion factor from cylinder as used in Planck paper (see also Le Brun 2015)
                    subhalos.append(subnum)
                    M500_list.append(M500*1.0e10 / h_scale)#s.header.hubble)
                    Y500_list.append(Y)
                    print Y, "arcmin^2", j, "of", Ntot
                    j += 1              

        del s
        if not os.path.exists(outdir+sim+"/"):
            os.mkdir(outdir+sim+"/")
        header = "use_fof_rad = "+str(use_fof_rad)+" main_halo_only = "+str(main_halo_only)+" temp_thresh = "+str(temp_thresh)+" h = "+str(h_scale)
        if cylinder: np.savetxt(outdir+sim+"/SZ_M500_snap"+str(snapnum)+suffix+"_cylinder.txt", np.c_[subhalos, M500_list, Y500_list], header=header+"\nsubhalo M500(Msun) Y(r500)(arcmin2)")
        elif use5r500: np.savetxt(outdir+sim+"/SZ_M500_snap"+str(snapnum)+suffix+"_sphere_5r500.txt", np.c_[subhalos, M500_list, Y500_list], header=header+"\nsubhalo M500(Msun) Y(5r500)(arcmin2)")
        else: np.savetxt(outdir+sim+"/SZ_M500_snap"+str(snapnum)+suffix+"_sphere.txt", np.c_[subhalos, M500_list, Y500_list], header=header+"\nsubhalo M500(Msun) Y(r500)(arcmin2)")

def plot_SZ_M500(sims, snapnum, indir="/data/curie4/nh444/project1/SZ/", suffix="",
                 outdir="/data/curie4/nh444/project1/SZ/", outfilename=None,
                 zoomdirs=None, zooms=[],
                 scat_idx=[], plot_med=True, compare=False,
                 cylindrical=True, plot_5r500=True,
                 logM500min=12.0, logmass=False, h_scale=0.7,
                 labels=None, for_paper=False, figsize=(7,7),
                 col=None, ls=None, mew=1.2, ms=6.5,
                 Wang=True, blkwht=False):
    ## plot_5r500 - plot SZ within 5r500 instead of just r500
    import numpy as np
    import matplotlib.pyplot as plt
    obs_dir = "/home/nh444/vault/ObsData/SZ/"
    
    fig = plt.figure(figsize=figsize)
    #fig.patch.set_facecolor('white')
    if col is None:
        #col = ["#b776e6","#6dd269","#ec5998","#c6c744","#e3793d"]
        from palettable.colorbrewer.qualitative import Dark2_8 as pallette
        col=pallette.hex_colors
    if ls is None:
        #ls=['dashed', 'dotted', 'dashdot', 'solid']+['solid']*(len(sims)-4)
        ls = ['solid']*len(sims)
    if zoomdirs is None:
        zoomdirs = [""]*len(sims)
    lw=2.5
    cap = ms*0.5 ## cap size of error bars
    
    #axislabelsize = 18
    logM500max = 15.1
    deltalogM500 = 0.2
    if labels is None: labels=[""]*len(sims)
    
    plt.yscale('log')
    if logmass:
        plt.xlim(logM500min, logM500max)
        plt.xlabel(r"log$_{10}$(M$_{500}$ [M$_{\odot}$])")
    else:
        plt.xscale('log')
        plt.xlim(10**logM500min, 10**logM500max)
        plt.xlabel(r"M$_{500}$ [M$_{\odot}$]")
    #plt.gca().set_xticks(np.arange(np.ceil(xmin), xmax, 1))
    #plt.gca().set_xticks(np.arange(xmin, xmax, 0.1), minor=True)
    if plot_5r500:
        plt.ylabel(r"$\mathrm{\tilde{Y}_{5r_{500}}}$ [arcmin$^2$]")
    else: 
        plt.ylabel(r"$\tilde{Y}(<r_{500})$ [arcmin$^2$]")
    #plt.ylabel(r"log$_{10}$($Y_{500}$ E$^{-2/3}$($z$) (D$_A$($z$) / 500 Mpc)$^2$ [arcmin$^2$])", fontsize=axislabelsize)

    #plt.gca().yaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: '10$^{{{}}}$'.format(int('{:e}'.format(y).split('e')[1]))))
    
    if plot_5r500:
        fac= 1.796 ## conversion factor from tabulated fluxes to flux within 5r500
    else:
        fac = 1.0
    ## Observations
    LBGs = np.genfromtxt(obs_dir+"planck_SZ_M500_LBGs.txt")
    if blkwht:
        c = '0.5' ## Planck
        c2 = '0.7' ## Wang+16
    else:
        c = '0.5' ## Planck
        c2 = '0.3' ## Wang+16

    if logmass:
        plt.errorbar(LBGs[:,0]+np.log10(0.704/h_scale),
                    LBGs[:,3]*fac*1e-6*(h_scale/0.704),
                    xerr=[LBGs[:,1]+np.log10(0.704/h_scale), LBGs[:,2]+np.log10(0.704/h_scale)],
                    yerr=LBGs[:,5]*fac*1e-6*(h_scale/0.704), ## bootstrap errors
                    c=c, mfc=c, mec='none', marker='o', ls='none',
                    ms=ms*1.4, mew=mew, lw=1.5, label="Planck XI 2013\n Wang+ 2016")
    else:
        ### statistical errors (just error bars, no symbols)
        #plt.errorbar(10**LBGs[:,6]*(0.704/h_scale), LBGs[:,3]*fac*1e-6*(h_scale/0.704),
                    #yerr=LBGs[:,4]*fac*1e-6*(h_scale/0.704), capsize=cap, c=c, ls='none')
        ## bootstrap errors
        plt.errorbar(10**LBGs[:,6]*(0.704/h_scale),
                    LBGs[:,3]*fac*1e-6*(h_scale/0.704),
                    yerr=LBGs[:,5]*fac*1e-6*(h_scale/0.704), ## bootstrap errors
                    capsize=cap, c=c, mfc=c, mec=c, marker='o', ls='none',
                    ms=ms*1.4, mew=mew, lw=1.5, label="Planck XI 2013")
        if Wang:
            """
            Note that I assume h=0.704 for the Wang data even though they use h=0.673
            because (according to email from Wenting Wang) they simply apply a correction
            factor to the halo masses derived by Planck XI 2013 and so the halo masses
            are in the same units as Planck XI, which used h=0.704
            """
            ### statistical errors (just error bars, no symbols)
            #plt.errorbar(10**LBGs[:,0]*(0.704/h_scale), LBGs[:,3]*fac*1e-6*(h_scale/0.704),
                        #yerr=LBGs[:,4]*fac*1e-6*(h_scale/0.704), capsize=cap, c=c2, ls='none')
            ## bootstrap errors
            plt.errorbar(10**LBGs[:,0]*(0.704/h_scale),
                        LBGs[:,3]*fac*1e-6*(h_scale/0.704),
                        xerr=[(10**(LBGs[:,0])-10**(LBGs[:,0]-LBGs[:,1]))*(0.704/h_scale), (10**(LBGs[:,0]+LBGs[:,2])-10**(LBGs[:,0]))*(0.704/h_scale)],
                        yerr=LBGs[:,5]*fac*1e-6*(h_scale/0.704), ## bootstrap errors
                        capsize=cap, c=c2, mfc='none', mec=c2, marker='o', ls='none',
                        ms=ms*1.4, mew=mew, lw=1.5, label="Wang+ 2016") #"Planck XI 2013\n Wang+ 2016"
        ## Planck 2015 from CEAGLE - these are X-RAY MASSES so mass bias
        ## I expect the units are Mpc^2 so I scale to 500 Mpc^2 for units in arcmin^2
        #dat = np.loadtxt("/data/vault/nh444/ObsData/SZ/Planck_2015_from_Barnes17.csv", delimiter=",")
        #plt.errorbar(dat[:,0], dat[:,1] / (500.0/(180.0/np.pi*60.0))**2.0, marker='D', mfc='k', ms=6, ls='none')
    
    #MCXC = np.genfromtxt(obs_dir+"planck_SZ_M500_MCXC.txt")
    #plt.errorbar(MCXC[:,0]+np.log10(0.704/h_scale),
                 #MCXC[:,1]+np.log10(fac*(h_scale/0.704)), yerr=[MCXC[:,2]+np.log10(fac*(h_scale/0.704)), MCXC[:,3]+np.log10((h_scale/0.704))], c='g', mec='g', mfc='none', marker='D', ls='none', ms=5, mew=mew, lw=1.5, label="Planck 2013 MCXC X-ray subsample")

    ## Best-fit for planck data
    planck_bestfit = lambda x: 0.73e-3 * (x / (3.0e14 * 0.704 / h_scale))**(5.0/3.0)
    xmin, xmax = plt.gca().get_xlim()
    if logmass:
        plt.errorbar([xmin, xmax], [planck_bestfit(10**xmin)*fac*(h_scale/0.704), planck_bestfit(10**xmax)*fac*(h_scale/0.704)], c=c, lw=lw, dashes=(9,7), dash_capstyle='round', ls='dashed')#, label="Best-fit to Planck data")
    else:
        plt.errorbar([xmin, xmax], [planck_bestfit(xmin)*fac*(h_scale/0.704), planck_bestfit(xmax)*fac*(h_scale/0.704)], c=c, lw=lw, dashes=(9,7), dash_capstyle='round', ls='dashed')#, label="Best-fit to Planck data")

    if Wang:
        ## Best-fit from Wang+2016 (weak lensing calibrated masses)
        Wang_bestfit = lambda x: 2.31e-5 * (x / (10**13.5 * 0.704/h_scale))**1.61
        xmin, xmax = plt.gca().get_xlim()
        if logmass:
            plt.errorbar([xmin, xmax], [Wang_bestfit(10**xmin)*fac*(h_scale/0.704), Wang_bestfit(10**xmax)*fac*(h_scale/0.704)], c=c2, lw=lw, dashes=(9,7), dash_capstyle='round', ls='dashed')
        else:
            plt.errorbar([xmin, xmax], [Wang_bestfit(xmin)*fac*(h_scale/0.704), Wang_bestfit(xmax)*fac*(h_scale/0.704)], c=c2, lw=lw, dashes=(1,4), dash_capstyle='round', ls='dashed')


    ## If compare is true, plots cylinder value and spherical value of Y500
    from scipy.stats import binned_statistic
    i = 0
    for j, sim in enumerate(sims):
        if cylindrical:
            data = np.loadtxt(indir+sim+"/SZ_M500_snap"+str(snapnum)+suffix+"_cylinder.txt")
            if plot_5r500:
                Y = data[:,2]
            else:
                Y = data[:,2] / 1.796
        elif plot_5r500:
            data = np.loadtxt(indir+sim+"/SZ_M500_snap"+str(snapnum)+suffix+"_sphere_5r500.txt")
            Y = data[:,2]
        else:
            data = np.loadtxt(indir+sim+"/SZ_M500_snap"+str(snapnum)+suffix+"_sphere.txt")
            Y = data[:,2]
        M500 = data[:,1]
        
        if j in scat_idx:
            if logmass:
                plt.errorbar(np.log10(M500), Y, marker='D', mec=col[i], mfc='none', mew=mew, ms=ms, ls='none')
            else:
                plt.errorbar(M500, Y, marker='D', mec=col[i], mfc='none', mew=mew, ms=ms, ls='none')
               
        if zoomdirs[j] is not "":
            for zoom in zooms[j]:
                if cylindrical:
                    data = np.loadtxt(indir+zoomdirs[j]+zoom+"/SZ_M500_snap"+str(snapnum)+suffix+"_cylinder.txt")
                    if plot_5r500:
                        Yz = data[0,2]
                    else:
                        Yz = data[0,2] / 1.796
                elif plot_5r500:
                    print zoomdirs[j]
                    print zoom
                    data = np.loadtxt(indir+zoomdirs[j]+zoom+"/SZ_M500_snap"+str(snapnum)+suffix+"_sphere_5r500.txt")
                    Yz = data[0,2]
                else:
                    data = np.loadtxt(indir+zoomdirs[j]+zoom+"/SZ_M500_snap"+str(snapnum)+suffix+"_sphere.txt")
                    Yz = data[0,2]
                if j in scat_idx:
                    if logmass:
                        plt.errorbar(np.log10(data[0,1]), Yz, marker='D', mec=col[i], mfc=col[i], mew=mew, ms=ms, ls='none')
                    else:
                        plt.errorbar(data[0,1], Yz, marker='D', mec=col[i], mfc=col[i], mew=mew, ms=ms, ls='none')
                M500 = np.append(M500, data[0,1])
                Y = np.append(Y, Yz)
                    

        if plot_med or not j in scat_idx:
            if logmass:
                bins = np.linspace(logM500min-deltalogM500, logM500max, num=np.ceil((logM500max - logM500min)/deltalogM500))
                x = (bins[1:]+bins[:-1])/2.0
                means, bin_edges, n = binned_statistic(np.log10(M500), Y, bins=bins, statistic='mean')
                num, bin_edges, n = binned_statistic(np.log10(M500), Y, bins=bins, statistic='count')
            else:
                bins = np.logspace(logM500min-deltalogM500, logM500max, num=np.ceil((logM500max - logM500min)/deltalogM500))
                x = 10**((np.log10(bins[:-1]) + np.log10(bins[1:]))/2.0)
                means, bin_edges, n = binned_statistic(M500, Y, bins=bins, statistic='mean')
                num, bin_edges, n = binned_statistic(M500, Y, bins=bins, statistic='count')
        
            mask = (num > 2)
            plt.plot(x[mask], means[mask], '-', c=col[i], lw=lw, label=labels[j], ls=ls[j], dash_capstyle='round')
            if len(sims) == 1 and not j in scat_idx:## 1-sigma contours
                if logmass:
                    binlabels = np.digitize(np.log10(M500), bins, right=False)
                else:
                    binlabels = np.digitize(M500, bins, right=False)
                err1 = np.asarray([np.percentile(Y[binlabels == k], 16.0) if len(Y[binlabels==k])>0 else np.nan for k in range(1, len(bins))])
                err2 = np.asarray([np.percentile(Y[binlabels == k], 84.0) if len(Y[binlabels==k])>0 else np.nan for k in range(1, len(bins))])
                plt.plot(x[mask], err1[mask], '--', c=col[i], lw=2) 
                plt.plot(x[mask], err2[mask], '--', c=col[i], lw=2)
        #plt.errorbar((bin_edges[1:]+bin_edges[:-1])/2.0, means, xerr=deltalogM500/2, yerr=[np.abs(means - err1),np.abs(means - err2)], marker='D', ecolor=col[i], mec=col[i], mfc='none', mew=mew, ms=ms, ls='none', label=labels[j])
        #counts, bin_edges, n = binned_statistic(np.log10(M500), Y, bins=bins, statistic='count')
        #plt.errorbar((bin_edges[1:]+bin_edges[:-1])/2.0, means, xerr=deltalogM500/2, yerr=means/np.sqrt(counts), marker='D', ecolor=col[i], mec=col[i], mfc='none', mew=mew, ms=ms, ls='none', label=labels[j])
        i += 1
        if compare and not plot_5r500:
            if not cylindrical:
                data = np.loadtxt(indir+sim+"/SZ_M500_snap"+str(snapnum)+"_cylinder.txt")
                label = labels[j]+" (Cylinder 5r$_{500}$)"
            else:
                data = np.loadtxt(indir+sim+"/SZ_M500_snap"+str(snapnum)+".txt")
                label = labels[j]+" (Sphere r$_{500}$)"
            if logmass:
                means, bin_edges, n = binned_statistic(np.log10(data[:,1]), data[:,2], bins=bins, statistic='mean')
            else:
                means, bin_edges, n = binned_statistic(data[:,1], data[:,2], bins=bins, statistic='mean')
            if len(sims) > 2 and logmass:
                plt.errorbar(x, means, xerr=deltalogM500/2, marker='D', ecolor=col[i], mec=col[i], mfc='none', mew=mew, ms=ms, ls='none', label=label)
            else:
                counts, bin_edges, n = binned_statistic(np.log10(data[:,1]), data[:,2], bins=bins, statistic='count')
                plt.errorbar(x, means, xerr=deltalogM500/2, yerr=means/np.sqrt(counts), marker='D', ecolor=col[i], mec=col[i], mfc='none', mew=mew, ms=ms, ls='none', label=label)
            i += 1

    handles, labels = plt.gca().axes.get_legend_handles_labels()
    if plot_med: n = len(sims)
    else: n = 0
    leg1 = plt.legend(handles[:n], labels[:n], loc='lower right', frameon=False, borderaxespad=1, numpoints=1, ncol=1)
    plt.gca().add_artist(leg1)
    leg2 = plt.legend(handles[n:], labels[n:], loc='upper left', frameon=False, borderaxespad=1, numpoints=1, ncol=1)
    if outfilename is None:
        if for_paper: outfilename = "SZ_vs_M500_snap"+str(snapnum)+"_paper.pdf"
        else: outfilename = "SZ_vs_M500_snap"+str(snapnum)+".pdf"
        
    if blkwht:
        fig.patch.set_facecolor('0.2')
        ax = plt.gca()
        ax.set_axis_bgcolor('0.2')
        ax.xaxis.label.set_color('white')
        ax.tick_params(axis='x', which='both', colors='white')
        ax.yaxis.label.set_color('white')
        ax.tick_params(axis='y', which='both', colors='white')
        for text in leg1.get_texts():
            text.set_color('white')
        for text in leg2.get_texts():
            text.set_color('white')
        import matplotlib.spines
        for child in ax.get_children():
            if isinstance(child, matplotlib.spines.Spine):
                child.set_color('white')
        plt.savefig(outdir+outfilename.replace(".pdf", ".png"), bbox_inches='tight', transparent=True)
    else:
        plt.savefig(outdir+outfilename, bbox_inches='tight')
        print "Plot saved to", indir+outfilename

if __name__ == "__main__":
    main()
